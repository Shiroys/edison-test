var ms = require('milliseconds');

FiltersFactory = function(model) {
    if (!(this instanceof FiltersFactory)) {
        return new FiltersFactory(model);
    }
    this.model = model

}

var today = function() {
    var today = new Date()
    today.setHours(0)
    return today;
}

var startWeek = function() {
    var d = new Date();
    d.setHours(0);
    var day = d.getDay();
    var diff = d.getDate() - day + (day == 0 ? -6:1);
    return new Date(d.setDate(diff));
}

var dateInter = function(inter) {
    return (new Date(inter.date.intervention)).getTime()
}

FiltersFactory.prototype.__getFilterBy = function(key, value) {
    var _this = this;
    var rtn;
    _this.list[_this.model].forEach(function(e, k) {
        if (e[key] === value) {
            rtn = e
            return false;
        }
    })
    return rtn;
}

FiltersFactory.prototype.getFilterByName = function(name) {
    return this.__getFilterBy('short_name', name);
}


FiltersFactory.prototype.getFilterByUrl = function(url) {
    return this.__getFilterBy('url', url);
}

FiltersFactory.prototype.getAllFilters = function(model) {
    return this.list[model ||  this.model];
}


FiltersFactory.prototype.filter = function(inter) {
    var _this = this;
    this.fltr = {};
    _this.list[_this.model].forEach(function(e, k) {
        if (!e.noCache && e.fn && typeof e.fn === 'function' && e.fn(inter)) {
            _this.fltr[e.short_name] = 1;
        }
    })
    return _this.fltr;
}


FiltersFactory.prototype.list = {
    artisan: [{
        short_name: 'a_map',
        long_name: 'Map Star',
        url: 'map',
        match: {
            'star': true
        }
    },{
        short_name: 'a_fact',
        long_name: 'Demande Facturier',
        url: 'needFacturier',
        match: {
            'demandeFacturier.status': "PENDING"
        }
    }, {
	short_name: 'a_hist',
	long_name: 'Historique d \'envoi',
	url: 'histEnvoi',
	match: {
	    'historique.pack': {$gt: []}
	}
    }, {
        short_name: 'a_pal1',
        long_name: 'Palier 1',
        group: '$login.management',
        url: 'pa1',
        match: {
            'subStatus': 'NEW',
            blocked: true
        },
    }, {
        short_name: 'a_pal2',
        long_name: 'Palier 2',
        group: '$login.management',
        url: 'pa2',
        match: {
            'subStatus': 'FORM',
            blocked: true
        },
    }, {
        short_name: 'a_pal3',
        long_name: 'Palier 3',
        group: '$login.management',
        url: 'pa3',
        match: {
            'subStatus': 'CONF',
            blocked: true
        },
    }, {
        short_name: 'a_pal4',
        long_name: 'Palier 4',
        group: '$login.management',
        url: 'pa4',
        match: {
            'subStatus': 'REG',
            blocked: true
        },
    }, {
        short_name: 'a_all',
        long_name: 'Tous les Artisans',
        url: '',
        match: {},
        stats: false,
        noCache: true,
    }, {
        short_name: 'a_arc',
        long_name: 'archivés',
        url: 'archives',
        match: {
            status: 'ARC'
        },
    }, {
        short_name: 'a_pot',
        long_name: 'potentiel',
        url: 'potentiel',
        match: {
            status: 'POT'
        },
    }, {
        short_name: 'a_dosc',
        long_name: 'Dossier Complet',
        url: 'dossierComplet',
        match: {
            $and: [{
                'document.cni.ok': true
            }, {
                'document.contrat.ok': true
            }, {
                'document.kbis.ok': true
            }]
        },
    }, {
        short_name: 'a_tut',
        long_name: 'sous-tutelle',
        url: 'artSousTutelle',
        match: {
            'status': {
                $ne: 'ARC'
            },
            'tutelle': true
        },
    }],
    devis: [{
        short_name: 'd_all',
        long_name: 'Tous les devis',
        url: '',
        stats: false,
        match: {},
        noCache: true,
    }, {
        short_name: 'd_tall',
        long_name: "Devis d'aujourd'hui",
        url: 'ajd',
        match: function() {
            return {
                'date.ajout': {
                    $gt: today()
                }
            }
        },
    }, {
        short_name: 'd_ann',
        long_name: "Annulés",
        url: 'annules',
        match: {
            status: "ANN"
        },
    }, {
        short_name: 'd_aev',
        long_name: "A envoyer",
        url: 'aEnvoyer',
        match: function() {
            return {
                status: "AEV"
            }
        },
    }, {
        short_name: 'd_att',
        long_name: "En attente",
        url: 'enAttente',
        match: function() {
            return {
                status: "ATT"
            }
        },
    }],
    intervention: [{
        short_name: 'i_all',
        long_name: 'Toutes les inters',
        url: '',
        stats: false,
        match: {},
        noCache: true,
    }, {
        short_name: 'i_pall',
        long_name: 'Interventions Partenariat Elad',
        url: 'partenariat',
        match: function () {
	    return {
		partenariat: true,
	    }
	},
    }, {
        short_name: 'i_tall',
        long_name: "Aujourd'hui",
        url: 'ajd',
        match: function() {
            return {
                'date.ajout': {
                    $gt: today()
                }
            }
        },
    }, {
        short_name: 'i_tenv',
        long_name: 'Envoyé',
        group: '$login.ajout',
        url: 'envoyeAjd',
        match: function() {
            return {
                'status': 'ENC',
                'date.ajout': {
                    $gt: today()
                }
            }
        },
    }, {
        short_name: 'i_tann',
        long_name: "Annulé Aujourd'hui",
        group: '$login.annulation',
        url: 'annuleAjd',
        match: function() {
            return {
                'status': 'ANN',
                'date.annulation': {
                    $gt: today()
                }
            }
        },
    }, {
        short_name: 'i_avr',
        long_name: 'A vérifier',
        url: 'aVerifier',
        match: function() {
            return {
                status: 'ENC',
                'date.intervention': { $lt: new Date(Date.now() + ms.hours(1)) }
            }
        },
    }, {
        short_name: 'i_avru',
        long_name: 'A vérifier Urgence',
        url: 'aVerifierUrgence',
        match: function() {
            return {
                status: 'ENC',
                urgence: true
            }
        },
    }, {
        short_name: 'i_apr',
        long_name: 'A programmer',
        url: 'aProgrammer',
        match: function () {
    	    return {
        		status: 'APR',
        		'date.ajout': { $gt: startWeek() }
    	    }
        },
    }, {
        short_name: 'i_iar',
        long_name: 'Intervenant à régler',
        url: 'iar',
        match: function() {
            return {
                "compta.paiement.effectue": false,
                "compta.reglement.recu": true,
                "compta.paiement.dette": false,
                "compta.paiement.ready": false,
                "status": {
                    $ne: 'ANN'
                }
            }
        },
        stats: true,
    }, {
        short_name: 'i_sarl',
        long_name: 'Relance sous-traitant',
        url: 'relanceArtisan',
        match: function() {
            return {
                status: 'VRF',
                reglementSurPlace: true,
                'compta.reglement.recu': false,
                'date.intervention': {
                    $lt: new Date(Date.now() - ms.weeks(2)),
                }
            }
        },
        stats: true,
    }, {
        short_name: 'i_chq',
        long_name: 'Relance Cheques',
        url: 'relanceCheques',
        sortBy: {
            di: 'desc'
        },
        match: function() {
            return {
                status: 'VRF',
                reglementSurPlace: true,
                'compta.reglement.recu': false,
                'compta.paiement.effectue': false,
                'artisan.subStatus': 'NEW'
            }
        },
        stats: true,
    }, {
        short_name: 'i_carl',
        long_name: 'Client à relancer',
        url: 'relanceClient',
        sortBy: {
            l: 'asc'
        },
        match: function() {
            return {
                status: 'VRF',
		'facture.payeur': {
		    $ne: 'GRN'
		},
                'compta.reglement.recu': false,
                reglementSurPlace: false,
            }
        },
    }, {
        short_name: 'i_rlgc',
        long_name: 'Relance de Grands Comptes',
        url: 'grandComptes',
        sortBy: {
            l: 'asc'
        },
        match: function() {
            return {
		'facture.payeur': 'GRN',
            }
        },
    }, {
	short_name: 'i_lsav',
	long_name: 'Liste SAV',
	url: 'SAV',
	sortBy: {
	    l: 'asc'
	},
	match: function() {
	    return {
		"status": "SAV",
	    }
	},
    },{
        short_name: 'i_fall',
        long_name: 'Toutes mes factures',
        url: 'factureHistorique',
        match: function() {
            return {
                status: 'VRF',
                reglementSurPlace: false,
            }
        },
    }, {
        short_name: 'i_fact',
        long_name: 'Facture à envoyer',
        url: 'factureaEnvoyer',
        sortBy: {
            di: 'desc'
        },
        match: function() {
            return {
                status: 'ENC',
                reglementSurPlace: false
            }
        },
    }, {
        short_name: 'i_sav',
        long_name: 'S.A.V En Cours',
        url: 'savEnCours',
        match: {
            'sav.status': 'ENC'
        },
    }, {
	short_name: 'i_lit',
	long_name: 'Tous les litiges',
	url: 'AllLitge',
	match: {
	    'recouvrement.level': 2
	},

    },{
        short_name: 'i_litEnc',
        long_name: 'Mes litiges',
        url: 'Mylitige',
        match: {
	    'recouvrement.level': 2
        },

    }, {
        short_name: 'i_mar',
        long_name: 'MARKET',
        url: 'market',
        stats: true,
        sortBy: {
            di: 'desc'
        },
        match: {
            aDemarcher: true,
            status: 'APR',
            'artisan.id': {
                $exists: false
            }
        },
    }, {
        short_name: 'i_pan',
        long_name: 'PANIER',
        url: 'panier',
        group: '$login.demarchage',
        match: {
            aDemarcher: true,
            status: {
                $in: ['APR']
            },
            'login.demarchage': {
                $exists: true
            }
        },
    }, {
        short_name: 'i_hist',
        long_name: 'Historique',
        stats: false,
        url: 'historique',
        group: '$login.demarchage',
        match: {
            aDemarcher: true,

        },
    }, {
        short_name: 'i_tut',
        long_name: 'Sous Tutelles',
        url: 'tutelle',
        match: {
            'artisan.subStatus': 'TUT',
            'compta.reglement.recu': false,
            status: {
                $in: ['APR', 'ENC', 'VRF']
            }
        },
    }, {
        short_name: 'i_ax',
        long_name: 'Axialis',
        url: 'axialis',
        match: {
            'newOs': true,
            'appels.1': {
                $exists: true
            }
        },
    }, {
        short_name: 'i_conv',
        long_name: 'Conversations',
        url: 'conversation',
        match: {
            'conversations.0': {
                $exists: true
            }
        },
    }, {
        short_name: 'i_axno',
        long_name: 'Axialis NoContact',
        url: 'axialisNoContact',
        match: {
            'newOs': true,
            'appels': {
                $size: 0
            }
        },
    },{
        short_name: 'i_nwno',
        long_name: 'New nocontact',
        url: 'newNoContact',
	sortBy: {
	    di: 'desc'
	},
        match: {
            'newOs': true,
            'appels': {
                $size: 0
            },
            'status':'ENC',
            'artisan.subStatus': 'NEW'
        },
    }, {
        short_name: 'i_newapr',
        long_name: 'News à envoyer',
        url: 'newsaEnvoyer',
        sortBy: {
            di: 'desc'
        },
        match: {
            'status': 'APR',
            'artisan.subStatus': 'NEW'
        },
    }, {
        short_name: 'i_potapr',
        long_name: 'Potentiel à env.',
        url: 'potaEnvoyer',
	sortBy: {
	    di: 'desc'
	},
        match: {
            'status': 'APR',
            'artisan.subStatus': {
                $in: ['POT', 'HOT']
            }
        },
    }, {
        short_name: 'i_pavrf',
        long_name: 'Verifs News',
        url: 'newVerif',
	sortBy: {
	    di: 'desc'
	},
        match: {
            'login.envoi': {
                $exists: true
            },
            'artisan.subStatus': {
                $in: ['NEW', 'POT', 'HOT']
            },
            status: 'ENC',
            'date.intervention': {
                $lt: new Date(Date.now() + ms.hours(1))
            }
        },
    }, {
        short_name: 'i_paenv',
        long_name: 'Mes Inters Env.',
        url: 'paEnv',
        group: "$login.envoi",
	sortBy: {
	    di: 'desc'
	},
        match: {
            'login.envoi': {
                $exists: true
            },
            status: {
                $in: ['ENC']
            }
        },
    }, {
        short_name: 'i_paenvtot',
        long_name: 'Mes Envois',
        url: 'paEnvTot',
        group: "$login.envoi",
	sortBy: {
	    di: 'desc'
	},
        match: {
            'login.envoi': {
                $exists: true
            },
            status: {
                $in: ['ENC', 'VRF', 'APR', 'AVR', 'ANN']
            }
        },
    }, {
        short_name: 'i_relsst1',
        long_name: 'Relance SST 1',
        url: 'relsst1',
        match: function() {
            return {
                'status': 'VRF',
                'compta.reglement.recu': false,
                'reglementSurPlace': true,
                'date.intervention': {
                    $lt: new Date(Date.now() - ms.weeks(1)),
                    $gt: new Date(Date.now() - ms.weeks(2)),
                }
            }
        },
    }, {
	short_name: 'i_litst',
	long_name: 'Litige Status',
	url: 'litst',
	match: function() {
	    return {
		'litige.level': {
		    $in: [1, 2, 3, 4, 5]
		}
	    }
	}
    },{
        short_name: 'i_ann',
        long_name: "Annulés",
        url: 'annules',
        match: function() {
            return {
                status: "ANN",
                'date.annulation': {
                    $gt: today()
                }
            }
        },
    },{
        short_name: 'i_daf',
        long_name: "Devis à Faire",
        url: 'devisAfaire',
        match: function() {
            return {
                status: { $in: ["ENC", "AVR", "APR"] },
                needDevis: true
            }
        },
    },{
        short_name: 'i_evr',
        long_name: "En Attente de Vérification",
        url: 'attenteVerif',
        match: function() {
            return {
                status: "EVR",
                reglementSurPlace: true
            }
        },
    },{ /* Begin filter recouvrement */
        short_name: 'i_rcc',
        long_name: "Rec Client Clos",
        url: 'recClientClos',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 2, 
                },{
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 2,
                }]

            }
        },
        stats: true,
    },{
        short_name: 'i_relrec',
        long_name: "Relance recouvrement",
        url: 'relRec',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.recouvrement.priseDeContact.dateRelance": { $exists: true },
                },{
                    "compta.recouvrement.priseDeContact.dateRelance": { $exists: true },
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcp',
        long_name: "Rec Client Pris",
        url: 'recClientPris',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    reglementSurPlace: false,
                },{
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcpre',
        long_name: "Retard envoi",
        url: 'recRetEnvoi',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.recouvrement.causeRec": "retEnvoi",
                    reglementSurPlace: false,
                },{
                    "compta.recouvrement.causeRec": "retEnvoi",
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcpi',
        long_name: "Impaye",
        url: 'recImpaye',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.recouvrement.causeRec": "impaye",
                    reglementSurPlace: false,
                },{
                    "compta.recouvrement.causeRec": "impaye",
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcpcp',
        long_name: "Cheque perdu",
        url: 'recChequePerdu',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.recouvrement.causeRec": "chequePerdu",
                    reglementSurPlace: false,
                },{
                    "compta.recouvrement.causeRec": "chequePerdu",
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcppdc',
        long_name: "Pas de contact",
        url: 'recPasDeContact',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.recouvrement.causeRec": "pasDeContact",
                    reglementSurPlace: false,
                },{
                    "compta.recouvrement.causeRec": "pasDeContact",
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcpp',
        long_name: "Plaintes",
        url: 'recPlaintes',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.recouvrement.causeRec": "plaintes",
                    reglementSurPlace: false,
                },{
                    "compta.recouvrement.causeRec": "plaintes",
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcpfnr',
        long_name: "Facture non reçu",
        url: 'recFactureNonRecu',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.recouvrement.causeRec": "factNonRecu",
                    reglementSurPlace: false,
                },{
                    "compta.recouvrement.causeRec": "factNonRecu",
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcpt',
        long_name: "Tarif",
        url: 'recTarif',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.recouvrement.causeRec": "tarif",
                    reglementSurPlace: false,
                },{
                    "compta.recouvrement.causeRec": "tarif",
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcpq',
        long_name: "Qualité",
        url: 'recQualite',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.recouvrement.causeRec": "qualite",
                    reglementSurPlace: false,
                },{
                    "compta.recouvrement.causeRec": "qualite",
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcpa',
        long_name: "Autres",
        url: 'recAutres',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.recouvrement.causeRec": "autres",
                    reglementSurPlace: false,
                },{
                    "compta.recouvrement.causeRec": "autres",
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.priseEnCharge": 1,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rcnp',
        long_name: "Rec Client Non Pris",
        url: 'recClientNonPris',
        match: function() {
            return {
                $or: [{
                    status: "VRF",
                    reglementSurPlace: false,
                    tva: 10,
                    "facture.payeur": { $nin: ['GRN'] },
                    "compta.recouvrement.litige.status": { $nin: [1,2,3] },
                    "date.intervention": { $gt: new Date(Date.now() - ms.days(400)) },
                    "date.envoiFacture": { $lt: new Date(Date.now() - ms.days(30)) },
                    "compta.reglement.recu": false,
                    "compta.recouvrement.priseEnCharge": 0,
                },{
                    status: "VRF",
                    reglementSurPlace: false,
                    tva: { $in: [0, 20] },
                    "facture.payeur": { $nin: ['GRN'] },
                    "compta.recouvrement.litige.status": { $nin: [1,2,3] },
                    "date.intervention": { $gt: new Date(Date.now() - ms.days(400)) },
                    "date.envoiFacture": { $lt: new Date(Date.now() - ms.days(45))},
                    "compta.reglement.recu": false,
                    "compta.recouvrement.priseEnCharge": 0,
                },{
                    "compta.recouvrement.litige.status": 4,
                    "compta.recouvrement.litige.status": { $nin: [1,2,3] },
                    "compta.recouvrement.priseEnCharge": 0,
                }]
            }
        },
        stats: true,
    },{
        short_name: 'i_rlc',
        long_name: "Litige Clos",
        url: 'recLitClos',
        match: function() {
            return {
                "compta.recouvrement.litige.status": 3,
            }
        },
        stats: true,
    },{
        short_name: 'i_rellit',
        long_name: "Relance Litige",
        url: 'recRelLit',
        match: function() {
            return {
                "compta.recouvrement.litige.status": 2,
                "compta.recouvrement.priseDeContact.dateRelance": { $exists: true },
            }
        },
        stats: true,
    },{
        short_name: 'i_rlp',
        long_name: "Litige Pris",
        url: 'recLitPris',
        match: function() {
            return {
                "compta.recouvrement.litige.status": 2,
            }
        },
        stats: true,
    },{
        short_name: 'i_rlpp',
        long_name: "Litige Plaintes",
        url: 'recLitPlaintes',
        match: function() {
            return {
                "compta.recouvrement.litige.status": 2,
                "compta.recouvrement.causeRec": "plaintes",
            }
        },
        stats: true,
    },{
        short_name: 'i_rlpropp',
        long_name: "Litige Risque Opposition",
        url: 'recLitRisqueOpposition',
        match: function() {
            return {
                "compta.recouvrement.litige.status": 2,
                "compta.recouvrement.causeRec": "riskOpposition",
            }
        },
        stats: true,
    },{
        short_name: 'i_rlpa',
        long_name: "Litige Autres",
        url: 'recLitAutres',
        match: function() {
            return {
                "compta.recouvrement.litige.status": 2,
                "compta.recouvrement.causeRec": "autres",
            }
        },
        stats: true,
    },{
        short_name: 'i_rlpopp',
        long_name: "Opposition",
        url: 'recLitOpposition',
        match: function() {
            return {
                "compta.recouvrement.litige.status": 2,
                "compta.recouvrement.causeRec": "opposition",
            }
        },
        stats: true,
    },{
        short_name: 'i_rlpq',
        long_name: "Qualite",
        url: 'recLitQualite',
        match: function() {
            return {
                "compta.recouvrement.litige.status": 2,
                "compta.recouvrement.causeRec": "qualite",
            }
        },
        stats: true,
    },{
        short_name: 'i_rlpt',
        long_name: "Tarif",
        url: 'recLitTarif',
        match: function() {
            return {
                "compta.recouvrement.litige.status": 2,
                "compta.recouvrement.causeRec": "tarif",
            }
        },
        stats: true,
    },{
        short_name: 'i_rlnp',
        long_name: "Litige Non Pris",
        url: 'recLitNonPris',
        match: function() {
            return {
                "compta.recouvrement.litige.status": 1,
            }
        },
        stats: true,    /* End filter recouvrement */
    }]
}

module.exports = FiltersFactory;
