 module.exports = [{
     login: 'abel_c',
     oldLogin: 'abel',
     portable: '0633138868',
     ligne: "0633138868",
     email: "abel@chalier.me",
     service: 'INFORMATIQUE',
     root: true,
     pseudo:'Alain',
     nom: 'abel',
     prenom: 'chalier',
     activated: true
 }, {
     login: 'benjamin_b',
     oldLogin: 'boukris_b',
     portable: '0782903875',
     email: 'contact@edison-services.fr',
     service: 'INTERVENTION',
     ligne:"0972441663",
     root: true,
     nom: 'boukris',
     prenom: 'sacha',
     pseudo: 'Arnaud',
     activated: true

 }, {
     login: 'harald_x',
     oldLogin: 'harald',
     service: 'INTERVENTION',
     root: false,
     nom: 'xxx',
     prenom: 'harald',
     pseudo: 'Fabien',
     activated: true
 }, {
     login: 'jeremie_r',
     oldLogin: 'jeremie',
     service: 'INTERVENTION',
     root: false,
     nom: 'roudier',
     prenom: 'jeremie',
     pseudo: 'éric',
     activated: true
 }, {
     login: 'clement_b',
     oldLogin: 'laurent',
     service: 'INTERVENTION',
     root: false,
     nom: 'clement',
     pseudo: 'Laurent',
     prenom: 'xxx',
     activated: true
 }, {
     login: 'gregorie_e',
     oldLogin: 'sebastien',
     service: 'INTERVENTION',
     root: false,
     nom: 'clement',
     pseudo: 'Laurent',
     prenom: 'xxx',
     activated: true
 }, {
     login: 'tayeb_g',
     oldLogin: 'tayeb',
     service: 'INTERVENTION',
     root: false,
     nom: 'guillot',
     prenom: 'taieb',
     pseudo: 'Eric',
     activated: true
 }, {
     login: 'eliran_t',
     oldLogin: 'eliran',
     service: 'INTERVENTION',
     root: true,
     nom: 'taieb',
     prenom: 'eliran',
     pseudo: 'damien',
     activated: true
 }, {
     login: 'thomas_x',
     oldLogin: 'thomas',
     service: 'INTERVENTION',
     root: false,
     nom: 'xxx',
     pseudo: 'thomas',
     prenom: 'thomas',
     activated: false
 }, {
     login: 'vincent_q',
     oldLogin: 'vincent',
     service: 'COMPTABILITE',
     root: true,
     email: 'comptabilite@edison-services.fr',
     nom: 'queudray',
     prenom: 'vincent',
     activated: true
 }, {
     login: 'yohann_r',
     oldLogin: 'yohann',
     service: 'PARTENARIAT',
     portable: '0637375945',
     root: true,
     nom: 'rhoum',
     email: 'yohann.rhoum@edison-services.fr',
     prenom: 'yohann',
     activated: true
 }]
