module.exports = [{
    "title": "BOUCHE VMC",
    "desc": "Bouche pour VMC\n- Bouche utilisée en VMC pavillonnaire(simple flux autoréglable ou double flux)ou VMP (ventilation mécanique ponctuelle).",
    "ref": "VMC002",
    "pu": 34.93
}, {
    "title": "GAINE CUISINE",
    "desc": "GAINE SPLE ALU GSA 125 3M\n- Utilisation en VMC habitat et tertiaire.\n- Température d'utilisation de -30 à +250°C.",
    "ref": "VMC003",
    "pu": 36.93
}, {
    "title": "GAINE AUTRES",
    "desc": "GAINE SPLE ALU GSA 80 3M\n- Utilisation en VMC habitat et tertiaire.\n- Température d'utilisation de -30 à +250°C.",
    "pu": 33.21,
    "ref": "VMC004",
}, {
    "title": "SANIBROYEUR PRO WC",
    "desc": "SANIBROYEUR PRO WC",
    "pu": 539.1,
    "ref": "SAN010",
}, {
    "desc": "DISJONCTEUR DIFFERENTIEL 32 A",
    "title": "DISJONCTEUR DIFFERENTIEL 32 A",
    "pu": 32.65,
    "ref": "DIS044",
}, {
    "desc": "VIDANGE FOSSE SEPTIQUE",
    "title": "VIDANGE FOSSE SEPTIQUE",
    "pu": 156.25,
    "ref": "CON016",
}, {
    "desc": "DISJONCTEUR DIFFERENTIEL 20 A",
    "title": "DISJONCTEUR DIFFERENTIEL 20 A",
    "pu": 29.31,
    "ref": "DIS017",
}, {
    "ref": "EDI001",
    "title": "Main d'œuvre",
    "desc": "Main d'œuvre",
    "pu": 65,
}, {
    "desc": "DISJONCTEUR DIFFERENTIEL 16 A",
    "title": "DISJONCTEUR DIFFERENTIEL 16 A",
    "pu": 28.17,
    "ref": "DIS095",
}, {
    "desc": "DISJONCTEUR DIFFERENTIEL 10 A",
    "title": "DISJONCTEUR DIFFERENTIEL 10 A",
    "pu": 27.34,
    "ref": "DIS061",
}, {
    "title": "SIMPLE VITRAGE LIVRAISON",
    "ref": "LIV001",
    "desc": "DÉPOSE / LIVRAISON et TRANSPORT",
    "pu": 35,
}, {
    "title": "DOUBLE VITRAGE LIVRAISON",
    "desc": "DÉPOSE / LIVRAISON et TRANSPORT",
    "ref": "LIV002",
    "pu": 55,
}, {
    "title": "MAIN D'OEUVRE VITRERIE",
    "ref": "VIT001",
    "pu": 130,
    "desc": "MAIN D'OEUVRE\nDEPLACEMENTS",
}, {
    "title": "CYLINDRE PROFIL EURO",
    "ref": "CYL001",
    "desc": "CYLINDRE PROFIL EUROPÉEN\n30 X 40\nFOURNIS AVEC 3 CLES",
    "pu": 96.25,
}, {
    "desc": "CYLINDRE PROFIL EUROPÉEN DE SÛRETÉ\n30 X 40\nFOURNIS AVEC CLÉS\nCARTE DE PROPRIÉTÉ\nREPRODUCTION INTERDITE",
    "title": "CYLINDRE PROFIL EURO SECURITE",
    "ref": "CYL002",
    "pu": 169.1,
}, {
    "title": "LAVABOS",
    "desc": "VICTORIA LAVABO 60 BLC",
    "ref": "LAV001",
    "pu": 139.1
}, {
    "title": "COLONNE LAVABOS",
    "desc": "VICTORIA COLONNE BLC",
    "pu": 96.25,
    "ref": "LAV002",
}, {
    "ref": "EDI002",
    "title": "Déplacement",
    "desc": "Déplacement",
    "pu": 65,
}, {
    "title": "SERRURE 1 POINT APPLIQUE",
    "desc": "SERRURE 1 POINT APPLIQUE\nCYLINDRE PROFIL EUROPEEN\nHORIZONTAL / VERTICAL A FOUILLOT OU A TIRAGE\nFOURNIS AVEC CLES",
    "pu": 139.1,
    "ref": "SER001",
}, {
    "title": "SERRURE 3 POINTS APPLIQUE",
    "ref": "SER004",
    "desc": "SERRURE 3 POINTS APPLIQUE\nCYLINDRE PROFIL EUROPEEN\nHORIZONTAL / VERTICAL A FOUILLOT OU A TIRAGE\nFOURNIS AVEC CLÉS",
    "pu": 246.25,
}, {
    "desc": "Ensemble mécanisme WC 3/6L ECO\nDouble volume\nJusqu'à -50% d'économie d'eau\nModèle adaptable",
    "title": "MECANISME CHASSE WC 3/6L",
    "ref": "MEC001",
    "pu": 96.25
}, {
    "desc": "PACK WC SUSPENDU AVEC ABATTANT À FERMETURE\nRALENTIE LOVELY RIMFREE BLANC RÉF 08399600000100\nALLIA",
    "pu": 571.5,
    "title": "PACK WC SUSPENDU AVEC ABATTANT À FERMETURE",
    "ref": "536295"
}, {
    "desc": "BÂTI-SUPPORT AUTOPORTANT DUOFIX PLUS UP320 H :\n112 CM 111.333.00.5 GEBERIT",
    "title": "BÂTI-SUPPORT AUTOPORTANT DUOFIX",
    "ref": "659854",
    "pu": 329.77
}, {
    "desc": "PLAQUE BA12 POUR HABILLAGE CARRELAGE",
    "title": "PLAQUE BA12 POUR HABILLAGE CARRELAGE",
    "ref": "458956",
    "pu": 139.1
}, {
    "desc": "PEIGNE UNIPOLAIRE P+N",
    "title": "PEIGNE UNIPOLAIRE P+N",
    "pu": 12.93,
    "ref": "PEI062",
}, {
    "desc": "CABLAGE EDF 10 mm²",
    "title": "CABLAGE EDF 10 mm²",
    "pu": 28.67,
    "ref": "CAB046",
}, {
    "desc": "Dépose de l'existant et mise en service inclus",
    "title": "Dépose de l'existant et mise en service inclus",
    "pu": 0,
    "ref": "DEP017",
}, {
    "desc": "CACHE INFERIEUR LC 17",
    "title": "CACHE INFERIEUR LC 17",
    "pu": 63.21,
    "ref": "CAC079",
}, {
    "desc": "Installation d’une chaudière gaz",
    "title": "Installation d’une chaudière gaz",
    "pu": 0,
    "ref": "INS027",
}, {
    "desc": "DOSSERET DE REMPLACEMENT CHAUDIÈRE",
    "title": "DOSSERET DE REMPLACEMENT CHAUDIÈRE",
    "pu": 189.13,
    "ref": "DOS040",
}, {
    "ref": "BAL004",
    "title": "Trépied Ballon",
    "desc": "Trépied Ballon",
    "pu": 93.21,
}, {
    "desc": "PIPE ÉVACUATION FLEXIBLE POUR WC SUSPENDU",
    "pu": 113.21,
    "ref": "698545",
    "title": "PIPE ÉVACUATION FLEXIBLE POUR WC SUSPENDU",
}, {
    "desc": "SOUPAPE DE SÉCURITÉ 4BAR DN15",
    "title": "SOUPAPE DE SÉCURITÉ 4BAR DN15",
    "pu": 119.21,
    "ref": "SOU054",
}, {
    "title": "Remise en conformité électrique NF C15-100",
    "desc": "Remise en conformité électrique NF C15-100",
    "pu": 0,
    "ref": "REM078",
}, {
    "ref": "VIT002",
    "title": "Remplacement Vitrage",
    "desc": "Remplacement Vitragess",
    "pu": 297.13,
}, {
    "ref": "CAM001",
    "title": "Camion D'assainisement",
    "desc": "Camion D'assainisement",
    "pu": 696.25,
}, {
    "desc": "TRAITEMENT DES DÉCHETS",
    "title": "TRAITEMENT DES DÉCHETS",
    "pu": 69.21,
    "ref": "TRA088",
}, {
    "desc": "INTERUPTEUR DIFFERENTIEL 30 mA - 40A",
    "title": "INTERUPTEUR DIFFERENTIEL 30 mA - 40A",
    "pu": 166.25,
    "ref": "INT012",
}, {
    "desc": "TABLEAU ELECTRIQUE 1 RANGEE - 13 MODULES",
    "title": "TABLEAU ELECTRIQUE 1 RANGEE - 13 MODULES",
    "pu": 89.1,
    "ref": "TAB093",
}, {
    "desc": "APPL D E 120GT 2T 12V\nGâche pour serrure applique verticale\n- Modèle en aluminium à double empênage.\n- Tension 12 volts.",
    "title": "GACHE ELECTRIQUE",
    "pu": 155.4,
    "ref": "000418"
}, {
    "desc": "Transformateur - Chrono code: 396431\n- 12 ou 24V AC.\n- Puissance de sortie 1,25 A.",
    "title": "TRANSFO 12V",
    "pu": 94.11,
    "ref": "396431"
}, {
    "title": "CHAUDIERE GAZ A TIRAGE NATUREL",
    "desc": "CHAUDIERE GAZ A TIRAGE NATUREL\nPUISSANCE : 9.5 À 24 KW\nDÉBIT : 11.5  LITRES / MINUTE\nDIAMÈTRE ÉVACUATION : 125 MM\nMODE D’ALLUMAGE PAR VEILLEUSE\nDIMENSIONS (L X P X H ) : 400 X 385 X 865\nÉQUIPÉ DU SYSTÈME S.P.O.T.T (SÉCURITÉ GAZ)",
    "pu": 1438.52,
    "ref": "CHA047",
}, {
    "title": "CHAUFFE EAU VM BLINDEE 200L",
    "ref": "CEB200",
    "desc": "Chauffe-eau électrique mural vertical\nRésistance blindée anti-calcaire\nPuissance : 1800 W\nType de courant : monophasé\nV = 200L\nRACCORDEMENT ÉLECTRIQUE MONO 220V (HEURE CREUSE / PLEINE)\nMARQUE ATLANTIC CERTIFIE\n\nGarantie constructeur : Jusqu'à 5 ans\nGarante pièce d'origine : Jusqu'à 2 ans\nAssistance et dépannage constructeur inclus jusqu'à 2 ans\nESSAIS ET MISE EN SERVICE INCLUS",
    "pu": 432.1
}, {
    "ref": "CES200",
    "title": "CHAUFFE EAU VM STEATITE 200L",
    "desc": "Chauffe-eau électrique mural vertical\nRésistance stéatite\nPuissance : 1800 W\nType de courant : monophasé\nV = 200L\nRACCORDEMENT ÉLECTRIQUE MONO 220V (HEURE CREUSE / PLEINE)\nMARQUE ATLANTIC CERTIFIE\n\nGarantie constructeur : Jusqu'à 5 ans\nGarante pièce d'origine : Jusqu'à 5 ans\nAssistance et dépannage constructeur inclus jusqu'à 2 ans\nESSAIS ET MISE EN SERVICE INCLUS",
    "pu": 593.21
}, {
    "ref": "ZEN200",
    "title": "CHAUFFE EAU VM ZENEO 200L",
    "desc": "Chauffe-eau électrique mural vertical\nRésistance stéatite\nPuissance : 2200 W\nType de courant : monophasé\nV = 200L\nRACCORDEMENT ÉLECTRIQUE MONO 220V (HEURE CREUSE / PLEINE)\nMARQUE ATLANTIC CERTIFIE\n\nGarantie constructeur : Jusqu'à 5 ans\nGarante pièce d'origine : Jusqu'à 5 ans\nAssistance et dépannage constructeur inclus jusqu'à 2 ans\nESSAIS ET MISE EN SERVICE INCLUS",
    "pu": 696.25
}, {
    "desc": "Chauffe-eau électrique horizontale\nRésistance blindée anti-calcaire\nPuissance : 1800 W\nType de courant : monophasé\nV = 200L\nRACCORDEMENT ÉLECTRIQUE MONO 220V (HEURE CREUSE / PLEINE)\nMARQUE ATLANTIC CERTIFIE\n\nGarantie constructeur : Jusqu'à 5 ans\nGarante pièce d'origine : Jusqu'à 2 ans\nAssistance et dépannage constructeur inclus jusqu'à 2 ans\nESSAIS ET MISE EN SERVICE INCLUS",
    "title": "CHAUFFE EAU HM BLINDEE 200L",
    "ref": "CB200H",
    "pu": 696.25,
}, {
    "title": "CHAUFFE EAU HM STEATITE 200L",
    "ref": "CS200H",
    "pu": 739.1,
    "desc": "Chauffe-eau électrique horizontale\nRésistance blindée anti-calcaire\nPuissance : 1800 W\nType de courant : monophasé\nV = 200L\nRACCORDEMENT ÉLECTRIQUE MONO 220V (HEURE CREUSE / PLEINE)\nMARQUE ATLANTIC CERTIFIE\n\nGarantie constructeur : Jusqu'à 5 ans\nGarante pièce d'origine : Jusqu'à 5 ans\nAssistance et dépannage constructeur inclus jusqu'à 2 ans\nESSAIS ET MISE EN SERVICE INCLUS",
}, {
    "desc": "CYLINDRE PROFIL EUROPÉEN DE HAUTE SÛRETÉ\nCANON ANTI-EFFRACTION / ANTI-PERÇAGE\n30 X 40\nFOURNIS AVEC CLÉS\nCARTE DE PROPRIÉTÉ\nREPRODUCTION INTERDITE",
    "title": "CYLINDRE PROFIL EURO DE HAUTE SURETE",
    "pu": 269.1,
    "ref": "CYL003",
}, {
    "ref": "SER003",
    "title": "SERRURE 1 POINT APPLIQU DE HAUTE SURETE",
    "desc": "SERRURE 1 POINT APPLIQUE DE SÛRETÉ\nCYLINDRE PROFIL EUROPEEN DE HAUTE SURETE\nHORIZONTAL / VERTICAL A FOUILLOT OU A TIRAGE\nFOURNIS AVEC CLÉS BLINDE\nCARTE DE PROPRIÉTÉ / REPRODUCTION INTERDITE",
    "pu": 296.25,
}, {
    "ref": "SER005",
    "desc": "SERRURE 3 POINTS APPLIQUE DE SÛRETÉ\nCYLINDRE PROFIL EUROPEEN\nHORIZONTAL / VERTICAL A FOUILLOT OU A TIRAGE\nFOURNIS AVEC CLÉS\nCARTE DE PROPRIÉTÉ / REPRODUCTION INTERDITE",
    "title": "SERRURE 3 POINTS APPLIQUE DE SURETE",
    "pu": 396.25,
}, {
    "ref": "SER002",
    "title": "SERRURE 1 POINT APPLIQUE SECURITE",
    "desc": "SERRURE 1 POINT APPLIQUE DE SÛRETÉ\nCYLINDRE PROFIL EUROPEEN\nHORIZONTAL / VERTICAL A FOUILLOT OU A TIRAGE\nFOURNIS AVEC CLÉS\nCARTE DE PROPRIÉTÉ / REPRODUCTION INTERDITE",
    "pu": 226.25,
}, {
    "desc": "SERRURE 3 POINTS APPLIQUE DE SÛRETÉ\nCYLINDRE PROFIL EUROPEEN DE HAUTE SURETE\nHORIZONTAL / VERTICAL A FOUILLOT OU A TIRAGE\nFOURNIS AVEC CLÉS BLINDE\nCARTE DE PROPRIÉTÉ / REPRODUCTION INTERDITE",
    "ref": "SER006",
    "title": "SERRURE 3 POINTS APPLIQUE DE HAUTE SECURITE",
    "pu": 596.2,
}, {
    "ref": "SVI001",
    "title": "SIMPLE VITRAGE",
    "desc": "REMPLACEMENT D'UN VITRAGE SUITE A BRIS DE GLACE :\n\nPORTE FENÊTRE\nSIMPLE VITRAGE\nVITRAGE CLAIR\nHaut 2000 mm X Larg 1000 mm\nCHÂSSIS PVC / ALU / BOIS\n\nCOMMANDE SPÉCIALE SUR MESURE \nADAPTATION ET FIXATION SUR PLACE \nREMPLACEMENT A L'IDENTIQUE",
    "pu": 197.13,
}, {
    "ref": "DVI001",
    "title": "DOUBLE VITRAGE",
    "desc": "PORTE FENÊTRE\nDOUBLE VITRAGE\nVITRAGE CLAIR\nHaut 2000 mm X Larg 1000 mm\nCHÂSSIS PVC / ALU / BOIS\n\nCOMMANDE SPÉCIALE SUR MESURE \nADAPTATION ET FIXATION SUR PLACE \nREMPLACEMENT A L'IDENTIQUE",
    "pu": 297.13,
}, {
    "title": "FLEXIBLE FILAIRE POUR DIGICODE",
    "desc": "Flexible - Chrono code: 569833\n- Embouts en ABS avec prédécoupe.\n- Ressort en acier inox.\n- Longueur 500 mm diamètre 12 mm intérieur.",
    "ref": "569833",
    "pu": 39.54
}, {
    "title": "FERME PORTE",
    "desc": "Ferme-porte à pignon et crémaillère.\n- Avec force réglable en continue de 2 à 6.\n- Réagit en proportion de la violence d’ouverture \n- Freinage à l’ouverture réglable et débrayable.",
    "ref": "DOR001",
    "pu": 277.38,
}, {
    "desc": "Clavier à 30 codes de 3 à 8 termes.\n- 13 codes commandant le relais n°1 et n°2. \n- 1 code de mise “on/off” d’alarme (relais n°3).1 code de “contrainte” commandant les relais n°1 et n°2.1 code de programmation.\n- Programmation des codes par le clavier.",
    "ref": "481448",
    "title": "DIGICODE",
    "pu": 338.94
}, {
    "desc": "Groupe de sécurité anti-calcaire 3/4.Robinet à sphère.\nClapet démontable\nRaccordement eau froide et chauffe eau : 20/27.Echappement 26/34.7 bars.\nEntonnoir siphon",
    "title": "GROUPE DE SECURITE",
    "ref": "GRS001",
    "pu": 69.13
}, {
    "desc": "Cylindre 787 Z - Chrono code: 098953\n- Cylindre de très haute sécurité / double cylindre anti-effraction\n- Cylindre monobloc à panneton.\n- Livré avec clés et carte de reproduction.\nREPRODUCTION INTERDITE",
    "title": "FICHET 787 Z",
    "ref": "FIC787",
    "pu": 762.42
}, {
    "desc": "Radial NT+\n- Cylindre de haute sécurité européen à clé réversible Radial NT+. \n- Goupilles multidirectionnelles en acier traité et embase acier, bouclier anti-perçage en tungstène et insert en acier traité donnant une grande résistance à l’effraction par perçage.\n- Livré avec 4 clés en 32.5 x 32.5 et en 32.5 x 42.5 mm\n- Carte de propriété et panneton standard décalé et nickelé en standard. \nREPRODUCTION INTERDITE",
    "title": "RADIAL NT",
    "ref": "RADNT+",
    "pu": 304.71
}, {
    "desc": "Trident Vigie Picard\n- Serrure en applique de haute sécurité à tirage et tringles apparentes.\n- Coffre rectangulaire avec 1 demi tour réversible pour devenir poussant et 2 pênes dormants ronds.\n- Evolutive, possibilité de lui adjoindre des verrous intermédiaires et de blocages et gâche répétition à cylindre mort.\n- Livré avec 3 clés et carte de propriété.\nREPRODUCTION INTERDITE",
    "title": "VIGIE PICARD",
    "ref": "PICA01",
    "pu": 876.54
}, {
    "desc": "Super sûreté\n- Coffre en acier plié laqué vieil or.\n- Pênes en acier et fonctionnement du demi-tour à la clé.\n- Cylindre cuirassé diamètre 30 mm et pastille en acier carbonitruré.\n- Livré avec 3 clés à bille ou sans bille et carte de reproduction.",
    "title": "BRICARD BLOC SUPER SURETE",
    "ref": "BRIC01",
    "pu": 607.52
}, {
    "title": "BRICARD CYLINDRE SUPER SURETE",
    "desc": "Jeu de cylindres à bille Supersûreté\n- Jeu de cylindres pour remplacement des serrures Bricard.\n- Diamètre 30 mm avec clé inox.\n- Avec protège pompe extérieur solidaire de la serrure en laiton.\n- Livré avec 3 clés forgés avec bille en bout de clé et carte de propriété.\nREPRODUCTION INTERDITE",
    "pu": 474.3,
    "ref": "BRIC02"
}, {
    "desc": "VMC pavillonnaire pour les logements du T1 AU T7.\n- 4 piquages régulés D80mm pour sanitaires.\n- 1 piquage D125mm pour la cuisine.\n- Silencieux, LW cuisine 30dB(a).\n- Moteur 2 vitesses.\n- Livré avec cordelette de suspension.",
    "title": "VMC 4 SANITAIRES + 1 CUISINES",
    "pu": 269.86,
    "ref": "VMC001"
}, {
    "desc": "Eurosmart\nMitigeur monocommande évier.\n- Monotrou sur plage.\n- Limiteur de débit ajustable.\n- Bec tube pivotant.\n- Mousseur.\n- Flexible de raccordement souple",
    "title": "MITIGEUR EVIER",
    "pu": 296.25,
    "ref": "MIT001"
}]
