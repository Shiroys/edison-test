module.exports = [{
    "payeur": "SOC",
    "nom": "MONDIAL ASSISTANCE",
    "tel": "0123212321",
    "tel2": "",
    "email": "QSDQSD@DQSD.DE",
    "ref": "mondial_as",
    "__v": 0,
    "address": {
        "n": "12",
        "r": "RUE DU CHEVALIER DE LA BARRE",
        "cp": "75018",
        "v": "PARIS"
    }
}, {
    "nom": "EDISON SERVICES",
    "tel": "0618513199",
    "email": "CONTACT@EDISON-SERVICES.FR",
    "ref": "edison_ser",
    "__v": 0,
    "address": {
        "n": "75",
        "r": "RUE DES DAMES",
        "cp": "75017",
        "v": "PARIS"
    }
}, {
    "nom": "FRANCE LOISIRS",
    "prenom": "COMPTABILITE FOURNISSEUR",
    "email": "AGNES.GEORGIN@FRANCE-LOISIRS.COM",
    "tel": "0145686038",
    "ref": "france_loi",
    "__v": 0,
    "address": {
        "n": "123",
        "r": "RUE DE GRENELLE",
        "cp": "75007",
        "v": "PARIS"
    }
}, {
    "email": "FOURNISSEURS@BABILOU.COM",
    "tel": "0141496784",
    "nom": "EVANCIA SAS",
    "ref": "evancia_sa",
    "__v": 0,
    "address": {
        "n": "24",
        "r": "RUE DU MOULIN DES BRUYÈRES",
        "cp": "92400",
        "v": "COURBEVOIE"
    }
}, {
    "nom": "1001 CRÈCHES",
    "email": "FOURNISSEURS@BABILOU.COM",
    "tel": "0141490062",
    "ref": "1001_crech",
    "__v": 0,
    "address": {
        "n": "24",
        "r": "RUE DU MOULIN DES BRUYÈRES",
        "cp": "92400",
        "v": "COURBEVOIE"
    }
}, {
    "email": "CONTACT@BONAPART.FR",
    "nom": "CABINET BONAPART GESTION",
    "payeur": "SOC",
    "prenom": "STEVEN ZAKIN",
    "tel": "0171197565",
    "tel2": "0667086698",
    "ref": "cabinet_bo",
    "__v": 0,
    "address": {
        "n": "1",
        "r": "RUE CONTE",
        "cp": "75003",
        "v": "PARIS"
    }
}]
