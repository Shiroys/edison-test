module.exports = [{
    "title": "Devis débouchage camion haute pression",
    "text": "devis d'<u>assainissement</u> pour <u>le débouchage haute pression de votre canalisation.</u>",
    "ref": "DEVIS_DEBOUCHAGE_CHP",
    "__v": 0,
    "produits": [{
        "title": "DEBOUCHAGE CAMION HAUTE PRESSION",
        "desc": "DEBOUCHAGE CAMION HAUTE PRESSION\nCAMION HYDRO-CUREUR\nDEGRAISSAGE CANALISATION\nPRESSION  >  400 BAR\nRemarques : Nettoyage de conduites domestiques et de l’égout principal jusqu’à un Ø de 300 mm",
        "quantite": 1,
        "pu": 496.25,
        "ref": "DEB066"
    }, {
        "ref": "EDI001",
        "desc": "MAIN D'OEUVRE",
        "title": "MAIN D'OEUVRE",
        "quantite": 1,
        "pu": 85
    }, {
        "ref": "EDI002",
        "desc": "DEPLACEMENT",
        "title": "DEPLACEMENT",
        "quantite": 1,
        "pu": 65
    }]
}, {
    "title": "Devis sany-broyeur",
    "ref": "DEVIS_SANY_B",
    "text": "devis de <u>plomberie</u> pour <u>l'installation d'un système de sani-broyeur</u>",
    "__v": 0,
    "produits": [{
        "title": "SANIBROYEUR PRO WC",
        "desc": "SANIBROYEUR PRO WCEN 12050-3 - Silence\nRégime moteur spécial : 2800 (tr/min)\nRefoulements 5m horizontal\nRefoulements 100m vertical\nAlimentation 220-240 / 50\nGarantie 2 ans constructeur * sous d’une utilisation conforme à la notice\n",
        "quantite": 1,
        "pu": 539.1,
        "ref": "SAN063",
        "showDesc": true
    }, {
        "ref": "EDI001",
        "desc": "MAIN D’OEUVRE",
        "title": "MAIN D’OEUVRE",
        "quantite": 1,
        "pu": 130
    }]
}, {
    "title": "Devis de tableau électrique 2 rangée - 13 modules",
    "ref": "DEVIS_TAB_ELEC_2_13",
    "text": "devis de mise en conformité électrique NF C 15-100, nous vous proposons le remplacement  de votre tableau <strong>2 rangées - 13 modules</strong>",
    "__v": 0,
    "produits": [{
        "title": "Remise en conformité électrique NF C15-100",
        "desc": "Remise en conformité électrique NF C15-100\nIdentification des circuits électriques\nSchéma électrique\n",
        "quantite": 1,
        "pu": 0,
        "ref": "REM089"
    }, {
        "title": "TABLEAU ELECTRIQUE 1 RANGEE - 13 MODULES",
        "desc": "TABLEAU ELECTRIQUE 1 RANGEE - 13 MODULES",
        "quantite": 1,
        "pu": 139.1,
        "ref": "TAB036"
    }, {
        "desc": "INTERUPTEUR DIFFERENTIEL 30 mA - 40A",
        "title": "INTERUPTEUR DIFFERENTIEL 30 mA - 40A",
        "quantite": 2,
        "pu": 166.25,
        "ref": "INT065"
    }, {
        "desc": "DISJONCTEUR DIFFERENTIEL 32 A",
        "title": "DISJONCTEUR DIFFERENTIEL 32 A",
        "quantite": 2,
        "pu": 32.65,
        "ref": "DIS017"
    }, {
        "desc": "DISJONCTEUR DIFFERENTIEL 20 A",
        "title": "DISJONCTEUR DIFFERENTIEL 20 A",
        "quantite": 4,
        "pu": 29.31,
        "ref": "DIS036"
    }, {
        "desc": "DISJONCTEUR DIFFERENTIEL 16 A",
        "title": "DISJONCTEUR DIFFERENTIEL 16 A",
        "quantite": 8,
        "pu": 28.17,
        "ref": "DIS012"
    }, {
        "desc": "DISJONCTEUR DIFFERENTIEL 10 A",
        "title": "DISJONCTEUR DIFFERENTIEL 10 A",
        "quantite": 8,
        "pu": 27.34,
        "ref": "DIS084"
    }, {
        "desc": "PEIGNE UNIPOLAIRE P+N",
        "title": "PEIGNE UNIPOLAIRE P+N",
        "quantite": 4,
        "pu": 12.93,
        "ref": "PEI090"
    }, {
        "desc": "CABLAGE EDF 10 mm²",
        "title": "CABLAGE EDF 10 mm²",
        "quantite": 4,
        "pu": 28.67,
        "ref": "CAB088"
    }, {
        "ref": "EDI001",
        "desc": "MAIN D'OEUVRE",
        "title": "MAIN D'OEUVRE",
        "quantite": 1,
        "pu": 280
    }, {
        "desc": "Dépose de l'existant et mise en service inclus",
        "title": "Dépose de l'existant et mise en service inclus",
        "quantite": 1,
        "pu": 0,
        "ref": "DEP045"
    }]
}, {
    "title": "Devis chauffe-eau gaz ELM LEBLANC",
    "ref": "DEVIS_CHF_EAU_ELM",
    "text": "<u>devis de remplacement de votre chauffe-eau gaz</u>",
    "__v": 0,
    "produits": [{
        "title": "Installation d’un chauffe-eau gaz (eau chaude sanitaire)",
        "desc": "Installation d’un chauffe-eau gaz (eau chaude sanitaire)\nDépose complète de l’existant\nMise à la décharge de l’unité de chauffage existante\nMise en conformité des raccordements gaz\nInstallation et mise en service inclus\n",
        "quantite": 1,
        "pu": 0,
        "ref": "INS058"
    }, {
        "title": "CHAUFFE-EAU GAZ A TIRAGE NATUREL",
        "desc": "CHAUFFE-EAU GAZ A TIRAGE NATUREL\nMarque : ELM LEBLANC - ONDEA\nPuissance : 7 à 29,5 kW\nDébit : 8 à 17 litres / minute\nMode d’allumage par veilleuse\nDimensions (L x P x H ) : 425 x 334 x 655\nCapacité pour : Cuisine + Lavabos + Douche + Baignoire\nÉquipé du système S.P.O.T.T (sécurité gaz)\n",
        "quantite": 1,
        "pu": 938.52,
        "ref": "CHA035"
    }, {
        "desc": "KIT MÉLANGEUR POUR ONDEA",
        "title": "KIT MÉLANGEUR POUR ONDEA",
        "quantite": 1,
        "pu": 89.13,
        "ref": "KIT043"
    }, {
        "desc": "CACHE INFERIEUR LC 17",
        "title": "CACHE INFERIEUR LC 17",
        "quantite": 1,
        "pu": 63.21,
        "ref": "CAC033"
    }, {
        "ref": "EDI001",
        "desc": "MAIN D’OEUVRE",
        "title": "MAIN D’OEUVRE",
        "quantite": 1,
        "pu": 330
    }]
}, {
    "title": "Devis débouchage camion vidange fosse septique",
    "ref": "DEVIS_DEBOUCHAGE_CVFS",
    "text": "devis d'<u>assainissement</u> pour <u>la vidange de votre fosse septique.</u>",
    "__v": 0,
    "produits": [{
        "desc": "VIDANGE FOSSE SEPTIQUE\nCONTENANCE DE LA CUVE = 1 M3PRIX / M3",
        "title": "CONTENANCE DE LA CUVE = 1 M3",
        "quantite": 1,
        "pu": 156.25,
        "ref": "CON045"
    }, {
        "desc": "TRAITEMENT DES DÉCHETS",
        "title": "TRAITEMENT DES DÉCHETS",
        "quantite": 1,
        "pu": 69.21,
        "ref": "TRA080"
    }, {
        "ref": "EDI001",
        "desc": "MAIN D'OEUVRE",
        "title": "MAIN D'OEUVRE",
        "quantite": 1,
        "pu": 85
    }, {
        "ref": "EDI002",
        "desc": "DEPLACEMENT",
        "title": "DEPLACEMENT",
        "quantite": 1,
        "pu": 65
    }]
}, {
    "title": "Devis chaudière gaz mural ELM LEBLANC avec ECS",
    "ref": "DEVIS_CHD_GM_ELM",
    "text": "<u>devis de remplacement de votre chaudière gaz</u>",
    "__v": 0,
    "produits": [{
        "desc": "Installation d’une chaudière gaz (chauffage + eau chaude sanitaire)\nDépose complète de l’existant\nMise à la décharge de l’unité de chauffage existanteMise en conformité des raccordements gaz\nInstallation et mise en service inclus\n",
        "title": "Installation d’une chaudière gaz",
        "quantite": 1,
        "pu": 0,
        "ref": "INS078"
    }, {
        "desc": "CHAUDIERE GAZ A TIRAGE NATUREL",
        "title": "CHAUDIERE GAZ A TIRAGE NATURELPuissance : 9.5 à 24 kW\nDébit : 11.5  litres / minute\nDiamètre évacuation : 125 mm\nMode d’allumage par veilleuse\nDimensions (L x P x H ) : 400 x 385 x 865\nÉquipé du système S.P.O.T.T (sécurité gaz)",
        "quantite": 1,
        "pu": 1438.52,
        "ref": "CHA075"
    }, {
        "desc": "DOSSERET DE REMPLACEMENT CHAUDIÈRE",
        "title": "DOSSERET DE REMPLACEMENT CHAUDIÈRE",
        "quantite": 1,
        "pu": 189.13,
        "ref": "DOS068"
    }, {
        "desc": "SOUPAPE DE SÉCURITÉ 4BAR DN15",
        "title": "SOUPAPE DE SÉCURITÉ 4BAR DN15",
        "quantite": 1,
        "pu": 119.21,
        "ref": "SOU068"
    }, {
        "ref": "EDI001",
        "desc": "MAIN D’OEUVRE",
        "title": "MAIN D’OEUVRE",
        "quantite": 1,
        "pu": 380
    }]
}, {
    "title": "Chauffe eau électrique 200 L HM Blindée",
    "text": "<u>devis de remplacement de votre chauffe-eau electrique</u>",
    "ref": "CHAUFFE_EA",
    "__v": 0,
    "produits": [{
        "desc": "Chauffe-eau électrique horizontale\nRésistance blindée anti-calcaire\nPuissance : 1800 W\nType de courant : monophasé\nV = 200L\nRACCORDEMENT ÉLECTRIQUE MONO 220V (HEURE CREUSE / PLEINE)\nMARQUE ATLANTIC CERTIFIE\n\nGarantie constructeur : Jusqu'à 5 ans\nGarante pièce d'origine : Jusqu'à 2 ans\nAssistance et dépannage constructeur inclus jusqu'à 2 ans\nESSAIS ET MISE EN SERVICE INCLUS",
        "title": "CHAUFFE EAU HM BLINDEE 200L",
        "ref": "CB200H",
        "pu": 696.25,
        "__v": 0,
        "random": 0,
        "quantite": 1
    }, {
        "ref": "BAL002",
        "title": "groupe de securité",
        "desc": "Groupe de securité",
        "pu": 69.97,
        "__v": 0,
        "random": 0,
        "quantite": 1
    }, {
        "ref": "BAL003",
        "title": "Raccordement hydraulique",
        "desc": "Raccordement hydraulique",
        "pu": 16.33,
        "__v": 0,
        "random": 0,
        "quantite": 2
    }, {
        "ref": "EDI001",
        "title": "Main d'œuvre",
        "desc": "Main d'œuvre",
        "pu": 195,
        "__v": 0,
        "random": 1,
        "quantite": 1
    }]
}, {
    "title": "Chauffe eau électrique 200 L VM Blindée",
    "text": "<u>devis de remplacement de votre chauffe-eau electrique</u>",
    "ref": "MONCOMBO",
    "__v": 0,
    "produits": [{
        "__v": 0,
        "title": "CHAUFFE EAU VM BLINDEE 200L",
        "ref": "CEB200",
        "desc": "Chauffe-eau électrique mural vertical\nRésistance blindée anti-calcaire\nPuissance : 1800 W\nType de courant : monophasé\nV = 200L\nRACCORDEMENT ÉLECTRIQUE MONO 220V (HEURE CREUSE / PLEINE)\nMARQUE ATLANTIC CERTIFIE\n\nGarantie constructeur : Jusqu'à 5 ans\nGarante pièce d'origine : Jusqu'à 2 ans\nAssistance et dépannage constructeur inclus jusqu'à 2 ans\nESSAIS ET MISE EN SERVICE INCLUS",
        "pu": 432.1,
        "random": 0,
        "quantite": 1
    }, {
        "ref": "BAL002",
        "title": "groupe de securité",
        "desc": "Groupe de securité",
        "pu": 69.97,
        "__v": 0,
        "random": 0,
        "quantite": 1
    }, {
        "ref": "BAL003",
        "title": "Raccordement hydraulique",
        "desc": "Raccordement hydraulique",
        "pu": 16.33,
        "__v": 0,
        "random": 1,
        "quantite": 2
    }, {
        "ref": "EDI001",
        "title": "Main d'œuvre",
        "desc": "Main d'œuvre",
        "pu": 195,
        "__v": 0,
        "random": 0,
        "quantite": 1
    }]
}, {
    "__v": 0,
    "title": "Chauffe eau électrique 200 L VM steatite",
    "text": "<u>devis de remplacement de votre chauffe-eau électrique steatite</u>",
    "ref": "CHAUFFE_EA",
    "produits": [{
        "quantite": 1,
        "random": 1,
        "pu": 593.21,
        "desc": "Chauffe-eau électrique mural vertical\nRésistance stéatite\nPuissance : 1800 W\nType de courant : monophasé\nV = 200L\nRACCORDEMENT ÉLECTRIQUE MONO 220V (HEURE CREUSE / PLEINE)\nMARQUE ATLANTIC CERTIFIE\n\nGarantie constructeur : Jusqu'à 5 ans\nGarante pièce d'origine : Jusqu'à 5 ans\nAssistance et dépannage constructeur inclus jusqu'à 2 ans\nESSAIS ET MISE EN SERVICE INCLUS",
        "title": "CHAUFFE EAU VM STEATITE 200L",
        "ref": "CES200",
        "__v": 0,
    }, {
        "quantite": 1,
        "random": 0,
        "__v": 0,
        "pu": 69.97,
        "desc": "Groupe de securité",
        "title": "groupe de securité",
        "ref": "BAL002",
    }, {
        "quantite": 2,
        "random": 1,
        "__v": 0,
        "pu": 16.33,
        "desc": "Raccordement hydraulique",
        "title": "Raccordement hydraulique",
        "ref": "BAL003",
    }, {
        "quantite": 1,
        "random": 1,
        "__v": 0,
        "pu": 195,
        "desc": "Main d'œuvre",
        "title": "Main d'œuvre",
        "ref": "EDI001",
    }]
}, {
    "title": "Devis de tableau électrique 1 rangée - 13 modules",
    "ref": "DEVIS_TAB_ELEC_1_13",
    "text": "devis de mise en conformité électrique NF C 15-100, nous vous proposons le remplacement  de votre tableau <strong>1 rangée - 13 modules</strong>",
    "__v": 0,
    "produits": [{
        "title": "Remise en conformité électrique NF C15-100",
        "desc": "Remise en conformité électrique NF C15-100\nIdentification des circuits électriques\nSchéma électrique\n",
        "pu": 0,
        "quantite": 1,
        "ref": "REM046"
    }, {
        "desc": "TABLEAU ELECTRIQUE 1 RANGEE - 13 MODULES",
        "title": "TABLEAU ELECTRIQUE 1 RANGEE - 13 MODULES",
        "quantite": 1,
        "pu": 89.1,
        "ref": "TAB066"
    }, {
        "desc": "INTERUPTEUR DIFFERENTIEL 30 mA - 40A",
        "title": "INTERUPTEUR DIFFERENTIEL 30 mA - 40A",
        "quantite": 1,
        "pu": 166.25,
        "ref": "INT034"
    }, {
        "desc": "DISJONCTEUR DIFFERENTIEL 32 A",
        "title": "DISJONCTEUR DIFFERENTIEL 32 A",
        "quantite": 1,
        "pu": 32.65,
        "ref": "DIS015"
    }, {
        "desc": "DISJONCTEUR DIFFERENTIEL 20 A",
        "title": "DISJONCTEUR DIFFERENTIEL 20 A",
        "quantite": 2,
        "pu": 29.31,
        "ref": "DIS032"
    }, {
        "desc": "DISJONCTEUR DIFFERENTIEL 16 A",
        "title": "DISJONCTEUR DIFFERENTIEL 16 A",
        "quantite": 4,
        "pu": 28.17,
        "ref": "DIS083"
    }, {
        "desc": "DISJONCTEUR DIFFERENTIEL 10 A",
        "title": "DISJONCTEUR DIFFERENTIEL 10 A",
        "quantite": 4,
        "pu": 27.34,
        "ref": "DIS016"
    }, {
        "desc": "PEIGNE UNIPOLAIRE P+N",
        "title": "PEIGNE UNIPOLAIRE P+N",
        "quantite": 2,
        "pu": 12.93,
        "ref": "PEI023"
    }, {
        "desc": "CABLAGE EDF 10 mm²",
        "title": "CABLAGE EDF 10 mm²",
        "quantite": 2,
        "pu": 28.67,
        "ref": "CAB032"
    }, {
        "ref": "EDI001",
        "desc": "MAIN D'OEUVRE\nDépose de l'existant et mise en service inclus",
        "title": "MAIN D'OEUVRE",
        "quantite": 1,
        "pu": 280
    }]
}, {
    "title": "Devis wc supendu",
    "text": "<u>devis du wc suspendu</u>",
    "ref": "DEVIS_WC_S",
    "__v": 0,
    "produits": [{
        "__v": 0,
        "desc": "PACK WC SUSPENDU AVEC ABATTANT À FERMETURE\nRALENTIE LOVELY RIMFREE BLANC RÉF 08399600000100\nALLIA",
        "pu": 571.5,
        "title": "PACK WC SUSPENDU AVEC ABATTANT À FERMETURE",
        "ref": "536295",
        "random": 0,
        "quantite": 1,
        "showDesc": true
    }, {
        "__v": 0,
        "desc": "BÂTI-SUPPORT AUTOPORTANT DUOFIX PLUS UP320 H :\n112 CM 111.333.00.5 GEBERIT",
        "title": "BÂTI-SUPPORT AUTOPORTANT DUOFIX",
        "ref": "659854",
        "pu": 329.77,
        "random": 1,
        "quantite": 1,
        "showDesc": true
    }, {
        "__v": 0,
        "desc": "PLAQUE BA12 POUR HABILLAGE CARRELAGE",
        "title": "PLAQUE BA12 POUR HABILLAGE CARRELAGE",
        "ref": "458956",
        "pu": 139.1,
        "random": 0,
        "quantite": 1,
        "showDesc": true
    }, {
        "desc": "PIPE ÉVACUATION FLEXIBLE POUR WC SUSPENDU",
        "pu": 113.21,
        "ref": "698545",
        "title": "PIPE ÉVACUATION FLEXIBLE POUR WC SUSPENDU",
        "__v": 0,
        "random": 0,
        "quantite": 1,
        "showDesc": true
    }, {
        "ref": "EDI001",
        "title": "Main d'œuvre",
        "desc": "Main d'œuvre",
        "pu": 380,
        "__v": 0,
        "random": 1,
        "quantite": 1,
        "showDesc": true
    }]
}]
