angular.module('edison', ['chart.js', 'browserify', 'mm.iban', 'ui.slimscroll', 'ngMaterial', 'lumx', 'ngAnimate', 'xeditable', 'btford.socket-io', 'ngFileUpload', 'pickadate', 'ngRoute', 'ngResource', 'ngTable', 'ngMap'])
    .config(["$mdThemingProvider", function($mdThemingProvider) {
        "use strict";
        $mdThemingProvider.theme('default')
            .primaryPalette('indigo')
            .accentPalette('blue-grey');
    }])
    .run(["editableOptions", function(editableOptions) {
        "use strict";
        editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
    }]).run(["$templateCache", "$route", "$http", function($templateCache, $route, $http) {
        var url;
        for (var i in $route.routes) {
            if (url = $route.routes[i].templateUrl) {
                $http.get(url, {
                    cache: $templateCache
                });
            }
        }
        $http.get("/Directives/dropdown-row.html", {
            cache: $templateCache
        });

        $http.get("/Templates/artisan-categorie.html", {
            cache: $templateCache
        });
        $http.get("/Templates/info-client.html", {
            cache: $templateCache
        });
        $http.get("/Templates/info-categorie.html", {
            cache: $templateCache
        });
        $http.get("/Templates/autocomplete-map.html", {
            cache: $templateCache
        });
    }]).config(["$compileProvider", function($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|callto|mailto|file|tel):/);
    }]);

angular.module('edison').controller('MainController', ["$timeout", "LxNotificationService", "dialog", "$q", "DataProvider", "TabContainer", "$scope", "socket", "config", "$rootScope", "$location", "edisonAPI", "taskList", "$window", function($timeout, LxNotificationService, dialog, $q, DataProvider, TabContainer, $scope, socket, config, $rootScope, $location, edisonAPI, taskList, $window) {
    "use strict";
    $rootScope.app_users = app_users;
    $rootScope.displayUser = app_session
    $scope.sidebarHeight = $("#main-menu-bg").height();
    $scope.config = config;
    $scope._ = _;
    $rootScope.loadingData = true;
    $rootScope.$on('$routeChangeSuccess', function() {
        $window.scrollTo(0, 0);
        $rootScope.loadingData = false;
    });
    var _this = this;

	edisonAPI.signalement.list({selectedUser: app_session.login, type: 1}).then(function(resp) {
		$rootScope.signCount = resp.data.length;
	})

    $rootScope.toggleSidebar = function(open) {
        if ($rootScope.sideBarMode === true) {
            $rootScope.sideBarIsClosed = open;
        }
    }

    $rootScope.addIntervention = function() {
        if ($scope.user.service !== 'INTERVENTION' || (!$scope.userStats.i_avr.total || !$scope.userStats.i_avr.total || ($scope.user.maxInterAverif > $scope.userStats.i_avr.total))) {
            $location.url('/intervention')
        } else {
            LxNotificationService.error("Impossible: Vous avez dépasser votre quota d'intervention à vérifié (" + $scope.userStats.i_avr.total + ' > ' + $scope.user.maxInterAverif + ')')
        }
    }

    $rootScope.toggleSidebarMode = function(newVal) {
        $rootScope.sideBarMode = _.isUndefined(newVal) ? !$rootScope.sideBarMode : newVal;
        $rootScope.sideBarIsClosed = $rootScope.sideBarMode
    }

    var checkResize = function() {
        $rootScope.smallWin = window.innerWidth < 1350
    	$rootScope.phoneWin = window.innerWidth < 480
    	_this.phoneWin = window.innerWidth < 480
            return $rootScope.toggleSidebarMode($rootScope.smallWin);
        }
        $(window).resize(checkResize);
        checkResize();
        var now = new Date()
        $scope.dateFormat = moment().format('llll').slice(0, -5);
        $scope.dateFormat2 = moment(new Date(now.getFullYear(), now.getMonth(),1)).format('MMM YYYY');
        $scope.dateFormat3 = moment(new Date(now.getFullYear(), now.getMonth()-1,1)).format('MMM YYYY');
        $scope.dateFormat4 = moment(new Date(now.getFullYear(), now.getMonth()-2,1)).format('MMM YYYY');
        $scope.dateFormat5 = moment(new Date(now.getFullYear(), now.getMonth()-3,1)).format('MMM YYYY');
        $scope.dateFormat6 = moment(new Date(now.getFullYear(), now.getMonth()-4,1)).format('MMM YYYY');
        $scope.dateFormat7 = moment(new Date(now.getFullYear(), now.getMonth()-5,1)).format('MMM YYYY');
        $scope.dateFormat8 = moment(new Date(now.getFullYear(), now.getMonth()-6,1)).format('MMM YYYY');
        $scope.dateFormat9 = moment(new Date(now.getFullYear(), now.getMonth()-7,1)).format('MMM YYYY');
        $scope.dateFormat10 = moment(new Date(now.getFullYear(), now.getMonth()-8,1)).format('MMM YYYY');
        $scope.dateFormat11 = moment(new Date(now.getFullYear(), now.getMonth()-9,1)).format('MMM YYYY');
        $scope.dateFormat12 = moment(new Date(now.getFullYear(), now.getMonth()-10,1)).format('MMM YYYY');
        $scope.dateFormat13 = moment(new Date(now.getFullYear(), now.getMonth()-11,1)).format('MMM YYYY');
        $rootScope.options = {
            showMap: true
    };


    var getSignalementStats = function() {
        edisonAPI.signalement.stats().then(function(resp) {
            $scope.signalementStats = resp.data;
        })
    }
    getSignalementStats()

    var reloadStats = function() {
        edisonAPI.stats.telepro()
            .success(function(result) {
                $scope.userStats = _.find(result, function(e) {
                    return e.login === $rootScope.user.login;
                });
                $rootScope.interventionsStats = result;
            });

        edisonAPI.intervention.dashboardStats({
                date: moment().startOf('day').toDate()
            })
            .then(function(resp) {
                _this.statsTeleproBfm = _.sortBy(resp.data.weekStats, 'total').reverse();
            });

    };

    _this.createLitige = function() {
        dialog.createLitige(function(data) {
            edisonAPI.intervention.createLitige({ data: data, user: app_session.login }).then(function(returnEnd) {
                if (returnEnd.data == "YES")
                    LxNotificationService.success("Litige ajouté");
                else
                    LxNotificationService.error("Une erreur est survenue");
            })
        })
    }

    _this.changeStatsTeleproBfm = function(type) {
        if (!_this.indexStatsTeleproBfm)
            _this.indexStatsTeleproBfm = 0;
        if (type == 1)
        {
            if ((_this.indexStatsTeleproBfm + 5) >= _this.statsTeleproBfm.length)
                _this.indexStatsTeleproBfm = _this.indexStatsTeleproBfm;
            else
                _this.indexStatsTeleproBfm += 5;
        }
        else if (type == 2)
        {
            if ((_this.indexStatsTeleproBfm - 5) <= 0)
                _this.indexStatsTeleproBfm = 0;
            else
                _this.indexStatsTeleproBfm -= 5;
        }
    }

    $rootScope.user = window.app_session
    reloadStats();
    socket.on('filterStatsReload', _.debounce(reloadStats, _.random(0, 1000)));

    $window.notify = function() {
        LxNotificationService.notify("test", 'android', false, "red");
    }

    socket.on('notification', function(data) {

        if (data.dest === $rootScope.user.login && (data.dest !== data.origin || data.self)) {
	    if (data.sticky === true)
		LxNotificationService.notify(data.message, data.icon || 'android', true, data.color);
	    else
		LxNotificationService.notify(data.message, data.icon || 'android', false, data.color);
        }
        if (data.service && data.service === $rootScope.user.service) {
	    if (data.sticky === true)
		LxNotificationService.notify(data.message, data.icon || 'android', true, data.color);
	    else
		LxNotificationService.notify(data.message, data.icon || 'android', false, data.color);
        }
    })

    $rootScope.openTab = function(tab) {

    }

    $rootScope.closeContextMenu = function(ev) {
        $rootScope.$broadcast('closeContextMenu');
    }

    var devisDataProvider = new DataProvider('devis')
    var artisanDataProvider = new DataProvider('artisan')
    var interventionDataProvider = new DataProvider('intervention')

    this.tabContainer = TabContainer;
    $scope.$on("$locationChangeStart", function(event) {
        if (_.includes(["/intervention", '/devis', '/artisan', '/'], $location.path())) {
            return 0
        }
        TabContainer.add($location).order();
    })

    $scope.taskList = taskList;

    $scope.linkClick = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
    };

    Mousetrap.bind(['command+i', 'ctrl+i'], function() {
        dialog.declareBug(_this.tabContainer, function(err, resp) {
            edisonAPI.bug.declare(resp).then(function() {
                    LxNotificationService.error("Le Serice informatique en a été prevenu");
                })
        })
    });

    $scope.tabIconClick = function($event, tab) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.tabs.close(tab)
    };
}]);

var getIntervention = function($route, $q, edisonAPI) {
    "use strict";
    var id = $route.current.params.id;
    if ($route.current.params.d) {
	var deferred = $q.defer();
	$q.all([
	    edisonAPI.devis.get($route.current.params.d, {
		select: 'date login produits tva client prixAnnonce categorie -_id'
	    }),
	    edisonAPI.intervention.getTmp(0)
	]).then(function(result) {
	    deferred.resolve(_.merge(result[1], result[0]));
	})
	return deferred.promise;

    } else if (id.length > 10) {
	return edisonAPI.intervention.getTmp(id);
    } else {
	return edisonAPI.intervention.get(id, {
	    populate: 'sst'
	});
    }
};
getIntervention.$inject = ["$route", "$q", "edisonAPI"];

var statsTelepro = function($route, $q, edisonAPI) {
    return edisonAPI.stats.telepro()
}
statsTelepro.$inject = ["$route", "$q", "edisonAPI"];

var getDevis = function($route, $q, edisonAPI) {
    "use strict";
    var id = $route.current.params.id;
    if ($route.current.params.i) {
	return edisonAPI.intervention.get($route.current.params.i, {
	    select: 'client categorie tva -_id conversations'
	});
    } else if (id.length > 10) {
	return edisonAPI.devis.getTmp(id);
    } else {
	return edisonAPI.devis.get(id);
    }
};
getDevis.$inject = ["$route", "$q", "edisonAPI"];
var getArtisan = function($route, $q, edisonAPI) {
    "use strict";
    var id = $route.current.params.id;
    if (id.length > 10) {
	return edisonAPI.artisan.getTmp(id);
    } else {
	return edisonAPI.artisan.get(id);
    }
}
getArtisan.$inject = ["$route", "$q", "edisonAPI"];

angular.module('edison').config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
    "use strict";
    $routeProvider
	.when('/', {
	    redirectTo: '/dashboard',
	})
	.when('/intervention/list', {
	    templateUrl: "Pages/ListeInterventions/listeInterventions.html",
	    controller: "ListeInterventionController",
	    controllerAs: 'vm',
	    reloadOnSearch: false

	})
	.when('/intervention/list/:fltr', {
	    templateUrl: "Pages/ListeInterventions/listeInterventions.html",
	    controller: "ListeInterventionController",
	    controllerAs: 'vm',
	    reloadOnSearch: false

	})
	.when('/intervention', {
	    redirectTo: function(routeParams, path, params) {
		var url = params.devis ? "?d=" + params.devis : "";
		url += params.i ? (url == "" ? "?i=" + params.i : "&i=" + params.i) : "";
		url += params.di ? (url == "" ? "?di=" + params.di : "&di=" + params.di) : "";
		url += params.sa ? (url == "" ? "?sa=" + params.sa : "&sa=" + params.sa) : "";
		return '/intervention/' + Date.now() + url;
	    }
	})
	.when('/intervention/:id', {
	    templateUrl: "Pages/Intervention/intervention.html",
	    controller: "InterventionController",
	    controllerAs: "vm",
	    reloadOnSearch: false,
	    resolve: {
		interventionPrm: getIntervention,
	    }
	})
	.when('/devis/list', {
	    templateUrl: "Pages/ListeDevis/listeDevis.html",
	    controller: "ListeDevisController",
	    controllerAs: 'vm',
	    reloadOnSearch: false

	})
	.when('/devis/list/:fltr', {
	    templateUrl: "Pages/ListeDevis/listeDevis.html",
	    controller: "ListeDevisController",
	    controllerAs: "vm",
	    reloadOnSearch: false

	})
	.when('/devis', {
	    redirectTo: function(routeParams, path, params) {
		var url = params.i ? "?i=" + params.i : "";
		return '/devis/' + Date.now() + url;
	    }
	})
	.when('/devis/:id', {
	    templateUrl: "Pages/Intervention/devis.html",
	    controller: "DevisController",
	    controllerAs: "vm",
	    resolve: {
		devisPrm: getDevis,
	    }
	})
	.when('/prospect/:id', {
	    templateUrl: "Pages/Prospect/prospect.html",
	    controller: "ProspectController",
	    controllerAs: "vm",
	})
    	.when('/prospect/:addr/:cate', {
	    templateUrl: "Pages/Prospect/prospect.html",
	    controller: "ProspectController",
	    controllerAs: "vm",
	})
	.when('/artisan/contact', {
	    templateUrl: "Pages/ListeArtisan/contactArtisan.html",
	    controller: "ContactArtisanController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/artisan/:sstid/recap', {
	    templateUrl: "Pages/ListeArtisan/contactArtisan.html",
	    controller: "ContactArtisanController",
	    controllerAs: 'vm',
	    reloadOnSearch: false

	})
	.when('/artisan/list', {
	    templateUrl: "Pages/ListeArtisan/listeArtisan.html",
	    controller: "ListeArtisanController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/artisan/list/:fltr', {
	    templateUrl: "Pages/ListeArtisan/listeArtisan.html",
	    controller: "ListeArtisanController",
	    controllerAs: "vm",
	    reloadOnSearch: false
	})
	.when('/artisan', {
	    redirectTo: function() {
		return '/artisan/' + Date.now();
	    }
	})
	.when('/artisan/:id', {
	    templateUrl: "Pages/Artisan/artisan.html",
	    controller: "ArtisanController",
	    controllerAs: "vm",
	    resolve: {
		artisanPrm: getArtisan,
	    }
	})
	.when('/dashboard', {
	    controller: 'DashboardController',
	    templateUrl: "Pages/Dashboard/dashboard.html",
	    controllerAs: "vm",
	    resolve: {
		statsTelepro: statsTelepro
	    }
	})
	.when('/search/:query', {
	    templateUrl: "Pages/Search/search.html",
	    controller: "SearchController",
	    controllerAs: "vm",
	})
	.when('/compta/lpa', {
	    templateUrl: "Pages/LPA/LPA.html",
	    controller: "LpaController",
	    controllerAs: "vm",
	})
	.when('/compta/avoirs', {
	    templateUrl: "Pages/Avoirs/avoirs.html",
	    controller: "avoirsController",
	    controllerAs: "vm",
	})
	.when('/compta/archivesPaiement', {
	    templateUrl: "Pages/Archives/archives.html",
	    controller: "archivesPaiementController",
	    controllerAs: "vm",
	})
	.when('/compta/remiseCheques', {
	    templateUrl: "Pages/Archives/archives.html",
	    controller: "remiseChequesController",
	    controllerAs: "vm",
	})
	.when('/compta/archivesReglement', {
	    templateUrl: "Pages/Archives/archives.html",
	    controller: "archivesReglementController",
	    controllerAs: "vm",
	})
        .when('/compta/fournitures', {
	    templateUrl: "Pages/Tools/fournitureMonthly.html",
	    controller: "fournitureMonthly",
	    controllerAs: 'vm',
	})
        .when('/compta/chiffreGrandComptes', {
	    templateUrl: "Pages/Tools/chiffreGrandComptes.html",
	    controller: "chiffreGrandComptes",
	    controllerAs: 'vm',
	})
	.when('/tools/vcf', {
		templateUrl: "Pages/Tools/vcf.html",
		controller: "vcfController",
		controllerAs: "vm",
	})
	.when('/tools/telephoneMatch', {
	    templateUrl: "Pages/Tools/telephoneMatch.html",
	    controller: "telephoneMatch",
	    controllerAs: "vm",
	})
	.when('/tools/editProducts', {
	    templateUrl: "Pages/Tools/edit-products.html",
	    controller: "editProducts",
	    controllerAs: "vm",
	})
        .when('/tools/ovh', {
	    templateUrl: "Pages/Tools/ovhPage.html",
	    controller: "ovhController",
	    controllerAs: "vm",
	})
        .when('/tools/ovhCalls', {
	    templateUrl: "Pages/Tools/ovhCalls.html",
	    controller: "ovhCallsController",
	    controllerAs: "vm",
	})
	.when('/tools/editSignalements', {
	    templateUrl: "Pages/Tools/edit-signalements.html",
	    controller: "editSignalements",
	    controllerAs: "vm",
	})
	.when('/listeSignalements', {
	    templateUrl: "Pages/ListeSignalements/liste-signalements.html",
	    controller: "listeSignalements",
	    controllerAs: "vm",
	   // reloadOnSearch: false
	})
	.when('/tools/editComptes', {
	    templateUrl: "Pages/Tools/edit-comptes.html",
	    controller: "editComptes",
	    controllerAs: "vm",
	})
	.when('/tools/editFournisseur', {
	    templateUrl: "Pages/Tools/edit-fournisseur.html",
	    controller: "editFournisseur",
	    controllerAs: "vm",
	})
	.when('/tools/editRaison', {
	    templateUrl: "Pages/Tools/edit-raison.html",
	    controller: "editRaison",
	    controllerAs: "vm",
	})
	.when('/tools/editCombos', {
	    templateUrl: "Pages/Tools/edit-combos.html",
	    controller: "editCombos",
	    controllerAs: "vm",
	})
	.when('/tools/editUsers', {
	    templateUrl: "Pages/Tools/edit-users.html",
	    controller: "editUsers",
	    controllerAs: "vm",
	})
    	.when('/tools/editPriv', {
	    templateUrl: "Pages/Tools/edit-priv.html",
	    controller: "editPriv",
	    controllerAs: "vm",
	})
    	.when('/tools/editAcquittance', {
	    templateUrl: "Pages/Tools/edit-acquittance.html",
	    controller: "editAcquittance",
	    controllerAs: "vm",
	})
	.when('/partenariat/comissions', {
	    templateUrl: "Pages/Tools/commissions-partenariat.html",
	    controller: "commissionsPartenariat",
	    controllerAs: 'vm',
	})
	.when('/tools/comRecouvrement', {
	    templateUrl: "Pages/Tools/commissions-recouvrement.html",
	    controller: "ComRecouvrement",
	    controllerAs: 'vm',
	})
	.when('/tools/comLitiges', {
	    templateUrl: "Pages/Tools/commissions-litiges.html",
	    controller: "ComLitiges",
	    controllerAs: 'vm',
	})
	.when('/tools/commissions', {
	    templateUrl: "Pages/Tools/commissions.html",
	    controller: "CommissionsController",
	    controllerAs: "vm",
	    reloadOnSearch: false

	})
        .when('/tools/emargement', {
            templateUrl: "Pages/Utils/acquittance.html",
            controller: "acquittanceController",
            controllerAs: "vm",
        })
        .when('/tools/outilsGeneraux', {
            templateUrl: "Pages/Utils/general.html",
            controller: "GeneralController",
            controllerAs: "vm",
        })
        .when('/tools/import', {
            templateUrl: "Pages/Utils/import.html",
            controller: "ImportController",
            controllerAs: "vm",
        })
        .when('/utils/demarchage', {
            templateUrl: "Pages/Utils/demarchage.html",
            controller: "DemarchageController",
            controllerAs: "vm",
        })
	.when('/utils/mapDemarchage', {
	    templateUrl: "Pages/Utils/mapDemarchage.html",
	    controller: "MapDemarchageController",
	    controllerAs: "vm"
	})
	.when('/stats/:type', {
	    templateUrl: "Pages/Stats/stats.html",
	    controller: "StatsController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/statsnew/:type', {
	    templateUrl: "Pages/Stats/stats-new.html",
	    controller: "StatsNewController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
    	.when('/statspart/:type', {
	    templateUrl: "Pages/Stats/stats-part.html",
	    controller: "StatsPartController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/gstats/:type', {
	    templateUrl: "Pages/Stats/gstats.html",
	    controller: "GStatsController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/user/history', {
	    templateUrl: "Pages/User/historique.html",
	    controller: "userHistory",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/statistiques', {
	    templateUrl: "Pages/Stats/statsRecouvrement.html",
	    controller: "StatsRecouvrementController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.otherwise({
	    redirectTo: '/dashboard'
	});
    // use the HTML5 History API
    $locationProvider.html5Mode(true);
}]);

 angular.module('edison').directive('absenceSst', ["edisonAPI", "LxNotificationService", "user", function(edisonAPI, LxNotificationService, user) {
    "use strict";
    return {
        replace: false,
        restrict: 'E',
        templateUrl: '/Templates/absence-sst.html',
        scope: {
            data: '=',
            exit: '&',
        },
        link: function(scope, elem) {
            scope.absence = {
                start: moment().add(-1, 'hours').toDate(),
                end: moment().hour(23).minute(43).toDate()
            }
            scope.save = function() {
                edisonAPI.artisan.absence(scope.data.id, scope.absence).then(function() {
                    LxNotificationService.success("L'absence à été enregistrer");
                    (scope.exit || _.noop)();
                })
            }
        }
    }
 }]);

angular.module('edison').directive('activiteSst', ["edisonAPI", function(edisonAPI) {
    "use strict";

    return {
		restrict: 'E',
		replace: true,
		templateUrl: '/Templates/activite-sst.html',
		scope: {
		    data: "=",
		    exit: '&'
		},
		link: function(scope, element, attrs) {
		    var reload = function() {
				if (!scope.data || !scope.data.id) {
				    return 0;
				}
				edisonAPI.artisan.fullHistory(scope.data.id).then(function(resp) {
				    scope.hist = resp.data;
				})
	 	   }
	    	scope.$watch('data.id', reload)
	    	scope.check = function(sign) {
		                /*  if (sign.ok)
				    return 0;*/
				edisonAPI.signalement.check(sign._id, sign.text).then(function(resp) {
				    sign = _.merge(sign, resp.data);
				})
				scope.exit && scope.exit();
	   		}
	    	scope.comment = function() {
				edisonAPI.artisan.comment(scope.data.id, scope.comm).then(reload)
				scope.comm = ""
	    	}
		}
    };
}]);

angular.module('edison').directive('allowPattern', [allowPatternDirective]);

function allowPatternDirective() {
    return {
        restrict: "A",
        compile: function(tElement, tAttrs) {
            return function(scope, element, attrs) {
                // I handle key events
                element.bind("keypress", function(event) {
                    var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
                    var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode.

                    // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
                    if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
                        event.preventDefault();
                        return false;
                    }

                });
            };
        }
    };
}

angular.module('edison').directive('callsSst', ["edisonAPI", function(edisonAPI) {
    "use strict";

    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/Templates/calls-sst.html',
        scope: {
            data: "=",
            exit: '&',
	    ispro: '@'
        },
        link: function(scope, element, attrs) {
            var reload = function() {
                if (!scope.data || !scope.data.id) {
                    return 0;
                }
		if (scope.ispro && scope.ispro === "true")
		{
		    edisonAPI.prospect.callHistory(scope.data.id).then(function(resp) {
			scope.hist = resp.data;
		    })
		}
		else
		{
                    edisonAPI.artisan.callHistory(scope.data.id).then(function(resp) {
			scope.hist = resp.data;
                    })
		}
            }

	    scope.$watch('data.calls', reload);
            scope.$watch('data.id', reload)
            scope.checkCall = function(sign) {
                var state = !sign.ok;
		if (scope.ispro && scope.ispro === "true")
		{
		    edisonAPI.prospect.checkCall(sign.id_artisan, sign._id, sign.text, state).then(function(resp) {
			var data;
			var n;
			for (n in resp.data.calls)
			{
                            if (resp.data.calls[n]._id == sign._id)
				data = resp.data.calls[n];
			}
			sign = _.merge(sign, data);
			reload();
                    })
                    scope.exit && scope.exit();
		}
		else
		{
                    edisonAPI.artisan.checkCall(sign.id_artisan, sign._id, sign.text, state).then(function(resp) {
			var data;
			var n;
			for (n in resp.data.calls)
			{
                            if (resp.data.calls[n]._id == sign._id)
				data = resp.data.calls[n];
			}
			sign = _.merge(sign, data);
			reload();
                    })
                    scope.exit && scope.exit();
		}
            }

        }
    };
}]);

angular.module('edison').directive('capitalize', function() {
    "use strict";
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(input) {
                return typeof input === "string" ? input.toUpperCase() : "";
            });
            element.css("text-transform", "uppercase");
        }
    };
});


angular.module('edison').directive('creditcard', function() {
    "use strict";
    return {
        require: 'ngModel',
        scope: {
            inline: "=",
        },
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(input) {
                return input.replace('x', 'AAA')
            });
        }
    };
});

angular.module('edison').directive('dropdownRow', ["Devis", "productsList", "edisonAPI", "config", "$q", "$timeout", "Intervention", "dialog", function(Devis, productsList, edisonAPI, config, $q, $timeout, Intervention, dialog) {
    "use strict";

    var arr_sort = function (o1, o2) {
	return new Date(o1.date) - new Date(o2.date)
    }

    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/Directives/dropdown-row.html',
        scope: {
            model: "@",
            row: '=',
        },
        link: function(scope, element, attrs) {
            scope._ = _;
            scope.Intervention = Intervention
            scope.Devis = Devis
            scope._model = scope.model || "intervention"

            scope.expendedStyle = {
                height: 0,
                overflow: 'hidden'
            };
            scope.expendedReady = false;
            scope.data = {};
            scope.dataVerif = {};
	    	scope.numbers = {};
            scope.config = config
            if (scope._model === "intervention") {
                edisonAPI.intervention.get(scope.row.id, {
                    populate: ['sst', 'devisOrigine'].join(',')
                }).then(function(result) {
                    scope.data = result.data;
                    if (scope.data.cache.s == 9)
                    {
                        edisonAPI.intervention.verifInter({ id: scope.data.id }).then(function(dataVerif) {
                            if (dataVerif)
                                scope.checkedMaj = 1;
                            else if (!dataVerif)
                                scope.checkedMaj = 2;
                            scope.dataVerif = dataVerif.data;
                            if (scope.data.produits) {
                                scope.produits = new productsList(scope.data.produits);
                            }
                            scope.client = scope.data.client;
                            scope.address = scope.client.address;
                            edisonAPI.ovh.getNumber(result.data.client.telephone, result.data.login.ajout).then(function (res) {
                                scope.numbers = res.data;
                            })
                        })
                    }
                    else
                    {
                        if (scope.data.produits) {
                                scope.produits = new productsList(scope.data.produits);
                        }
                        scope.client = scope.data.client;
                        scope.address = scope.client.address;
                        edisonAPI.ovh.getNumber(result.data.client.telephone, result.data.login.ajout).then(function (res) {
                            scope.numbers = res.data;
                        })
                    }
                })
            }
            else if (scope._model === "devis")
            {
                var pAll = [
                    edisonAPI.devis.get(scope.row.id, {
                        populate: 'transfertId'
                    }),
                ]
                var pThen = function(result) {
                    scope.data = result[0].data;
                    scope.produits = new productsList(scope.data.produits);
                    scope.hist = scope.data.historique
                    scope.client = scope.data.client;
                    scope.address = scope.client.address;
                }
            }
            else if (scope._model === 'artisan')
            {
                scope.loadPanel = function(id) {
                    edisonAPI.artisan.getStats(id).then(function(resp) {
                        new Chartist.Pie('.ct-chart', {
                            series: [{
                                value: resp.data.envoye.total,
                                name: 'En cours',
                                className: 'ct-orange',
                                meta: 'Meta One'
                            }, {
                                value: resp.data.annule.total,
                                name: 'annulé',
                                className: 'ct-red',
                                meta: 'Meta One'
                            }, {
                                value: resp.data.paye.total,
                                name: 'payé',
                                className: 'ct-green',
                                meta: 'Meta One'
                            }]
                        }, {
                            total: resp.data.annule.total + resp.data.paye.total + resp.data.envoye.total,
                            donut: true,
                            startAngle: 270,
                            donutWidth: 62,
                        });
                        scope.stats = resp.data
                    })

                }
                scope.loadPanel(scope.row.id)
                pAll = [
                    edisonAPI.artisan.get(scope.row.id),
                    edisonAPI.artisan.getStats(scope.row.id)
                ]
                pThen = function(result) {
                    scope.data = result[0].data;
                    scope.artisan = scope.data;
                    scope.artisan.stats = result[1].data;
                    scope.address = scope.artisan.address
                }
            }

            scope.showAlert = function(ev) {
                dialog.alertDialog(scope);
            }
            scope.showNoAlert = function(ev) {
                console.log("ev", ev, "scope", scope);
                dialog.alertNoDialog(scope);
            }
            scope.showNoReglementAlert = function(ev) {
                console.log("ev", ev, "scope", scope);
                dialog.alertNoReglementDialog(scope);
            }    

            $q.all(pAll).then(pThen)
            scope.getStaticMap = function() {
                var q = "?format=jpg&width=411&height=210px&precision=0&origin=" + scope.address.lt + ", " + scope.address.lg;
                if (_.get(scope, 'data.artisan.address.lt'))
                    q += "&destination=" + scope.data.artisan.address.lt + ", " + scope.data.artisan.address.lg;
                else
                    q += "&zoom=15";
                return "/api/mapGetStatic" + q;
            }

        }
    };
}]);

angular.module('edison').directive('elastic', ["$timeout", function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, element) {
                $scope.initialHeight = $scope.initialHeight || element[0].style.height;
                var resize = function() {
                    element[0].style.height = $scope.initialHeight;
                    element[0].style.height = "" + element[0].scrollHeight + "px";
                };
                element.on("input change", resize);
                $timeout(resize, 0);
            }
        };
    }
]);
angular.module('edison').directive('ngEnter', function () {
    "use strict";
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});
angular.module('edison').directive('historiquePaiementSst', ["edisonAPI", "FlushList", function(edisonAPI, FlushList) {
    "use strict";

    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/Templates/historique-paiement-sst.html',
        scope: {
            data: "=",
            exit: '&'
        },
        link: function(scope, element, attrs) {
            var reload = function() {
                if (!scope.data || !scope.data.id) {
                    return 0;
                }
                scope.getTotal = function(arr) {
                    var rtn = 0;
                    _.each(arr, function(e) {
                        rtn += e.original.compta.paiement.historique.final
                    })
                    return _.round(rtn, 2);
                }
                edisonAPI.artisan.getCompteTiers(scope.data.id).then(function(resp) {
                    scope.historiquePaiement = _.map(resp.data, function(e) {
                        e.flushList = new FlushList(e.list, _.map(e.list, '_id'))
                        _.map(e.flushList.getList() , function(x) {
                            x.original = _.find(e.list, 'id', x.id)
                        })
                        return e;
                    })
                })
            }

            scope.$watch('data.id', reload)
            scope.check = function(sign) {
                /*  if (sign.ok)
                      return 0;*/
                edisonAPI.signalement.check(sign._id, sign.text).then(function(resp) {
                    sign = _.merge(sign, resp.data);
                })
                scope.exit && scope.exit();
            }
            scope.comment = function() {
                edisonAPI.artisan.comment(scope.data.id, scope.comm).then(reload)
                scope.comm = ""
            }
        }
    };
}]);

angular.module('edison').directive('historiqueSst', ["edisonAPI", function(edisonAPI) {
    "use strict";

    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/Templates/historique-sst.html',
        scope: {
            data: "=",
            exit: '&'
        },
        link: function(scope, element, attrs) {
            var reload = function() {
                if (!scope.data || !scope.data.id) {
                    return 0;
                }
                edisonAPI.artisan.fullHistory(scope.data.id).then(function(resp) {
                    scope.hist = resp.data;
                })
            }

            scope.$watch('data.id', reload)
            scope.check = function(sign) {
                /*  if (sign.ok)
                      return 0;*/
                edisonAPI.signalement.check(sign._id, sign.text).then(function(resp) {
                    sign = _.merge(sign, resp.data);
                })
                scope.exit && scope.exit();
            }
            scope.comment = function() {
                edisonAPI.artisan.comment(scope.data.id, scope.comm).then(reload)
                scope.comm = ""
            }
        }
    };
}]);

 angular.module('edison').directive('infoComment', ["user", function(user) {
     "use strict";
     return {
         replace: false,
         restrict: 'E',
         templateUrl: '/Templates/info-comment.html',
         scope: {
             data: '=',
         },
         link: function(scope, elem, attr) {
             scope.height = attr.height ||  216;
             scope.user = user;
             scope.addComment = function() {
                 scope.data.comments.push({
                     login: user.login,
                     text: scope.commentText,
                     date: new Date()
                 })
                 scope.commentText = "";
             }
         }
     }
 }]);

 angular.module('edison').directive('infoLitige', function() {
     "use strict";
     return {
         replace: false,
         restrict: 'E',
         templateUrl: '/Templates/info-litige.html',
         scope: {
             data: '=',
         },
         link: function(scope, elem) {
             scope.$watch('data.litige.description', function(curr, prev) {
                 if (scope.data.litige && !scope.data.litige.closed && scope.data.litige.description)
                     scope.data.litige.open = true
                 if (scope.data.litige && !scope.data.litige.description) {
                     scope.data.litige.open = false
                 }
             })
         }
     }
 });

 angular.module('edison').directive('infoPaiement', ["config", function(config) {
     "use strict";
     return {
         replace: false,
         restrict: 'E',
         templateUrl: '/Templates/info-paiement.html',
         scope: {
             data: '=',
             artisans: '='
         },
         link: function(scope, elem) {
            scope.config = config;
         }
     }
 }]);

 angular.module('edison').directive('infoSav', ["config", function(config) {
     "use strict";
     return {
         replace: false,
         restrict: 'E',
         templateUrl: '/Templates/info-sav.html',
         scope: {
             data: '=',
             artisans: '='
         },
         link: function(scope, elem) {
            scope.config = config;
         }
     }
 }]);

var Controller = function($timeout, TabContainer, FiltersFactory, user, ContextMenu, LxProgressService, LxNotificationService, edisonAPI, DataProvider, $routeParams, $location, $rootScope, $filter, config, ngTableParams, MomentIterator, openPost) {
    var _this = this;
    _this._ = _;
    _this.displayUser = app_session;
    LxProgressService.circular.show('#5fa2db', '#globalProgress');
    var currentFilter;
    var currentHash = $location.hash();
    var dataProvider = new DataProvider(_this.model, $routeParams.hashModel)
    var filtersFactory = new FiltersFactory(_this.model)
    _this.see = 0;
    if ($routeParams.fltr) {
        currentFilter = filtersFactory.getFilterByUrl($routeParams.fltr)
    }
    if ($routeParams.fltr && $routeParams.fltr === "grandComptes")
    {
	edisonAPI.compte.list().then(function (resp) {
	    _this.compteList = [];
	    for (var n = 0; n < resp.data.length; n++)
	    {
		_this.compteList.push(resp.data[n]);
	    }
	});
    }

    if ($routeParams.fltr && $routeParams.fltr === "map")
    {
	edisonAPI.artisan.starList().then(function (resp) {
	    _this.confirmedArtisans = resp.data;
	});
    }

    _this.interSwipe = function (id) {
	$location.url("/intervention/" + id);
    }

    _this.devisSwipe = function (id) {
	$location.url("/devis/" + id);
    }

    _this.changeCompte = function () {
		$location.url("/intervention/list/grandComptes?nc=" + this.payeurToShow);
		_this.payeurToShow = this.payeurToShow;
    }
    var now = new Date()
    var end = new Date(now.getFullYear(), now.getMonth()+1, 1);
    var start = moment().add(-13, 'month').toDate()
    _this.dateSelectList = MomentIterator(start, end).range('month').map(function(e) {
		return {
	            ts:e.unix(),
	            t: e.format('MMM YYYY'),
	            m: e.month() + 1,
	            y: e.year(),
	        }
    })

    _this.selectAll = function () {
		for (var n = 0; n < _this.tableParams.data.length; n++)
		{
	    	_this.tableParams.data[n].Action = true;
			_this.tableParams.data[n].ActionTshirt = true;
		}
		_this.keep();
    }

    _this.keep = function () {
		var elems = [];
		for (var n = 0; n < _this.tableParams.data.length; n++)
		{
	    	if ((_this.tableParams.data[n].Action && _this.tableParams.data[n].Action === true) || (_this.tableParams.data[n].ActionTshirt && _this.tableParams.data[n].ActionTshirt))
	   		{
				elems.push(_this.tableParams.data[n]);
			}
		}
		if (elems.length === 0)
	    	_this.see = 0;
		else
	    	_this.see = 1;
    }

    _this.ArcCheck = function () {
		_this.arc = 1;
    };
    _this.RelCheck = function () {
		_this.rel = 1;
    };
    _this.EnvCheck = function () {
		_this.env = 1;
    };
    _this.FacCheck = function () {
		_this.fac = 1;
    }
    _this.RefCheck = function () {
		_this.ref = 1;
    };
    _this.ActionArt = function () {
		var arc = _this.arc;
		var rel = _this.rel;
		var env = _this.env;
		var fac = _this.fac;
		var ref = _this.ref;
		var elems = [];
		for (var n = 0; n < _this.tableParams.data.length; n++)
		{
			if (_this.tableParams.data[n].ActionTshirt && _this.tableParams.data[n].ActionTshirt === true)
	   		{
				var check = 0;
				if (elems.length != 0)
				{
					var ind = 0;
					while (elems[ind] != null)
					{
						if (_this.tableParams.data[n].id == elems[ind].id)
						{
							elems[ind].ActionTshirt = true;
							check = 1;
							ind = -2;
						}
						ind++;
					}
					if (check == 0)
						elems.push(_this.tableParams.data[n]);
				}
				else
					elems.push(_this.tableParams.data[n]);
	    	}
			if (_this.tableParams.data[n].Action && _this.tableParams.data[n].Action === true)
			{
				var check = 0;
				if (elems.length != 0)
				{
					var ind = 0;
					while (elems[ind] != null)
					{
						if (_this.tableParams.data[n].id == elems[ind].id)
						{
							elems[ind].Action = true;
							check = 1;
							ind = -2;
						}
						ind++;
					}
					if (check == 0)
						elems.push(_this.tableParams.data[n]);
				}
				else
					elems.push(_this.tableParams.data[n]);
			}
		}
		for (var j = 0;j < elems.length;j++)
		{
	    	var artisan = elems[j];
	    	edisonAPI.artisan.get(artisan.id).then(function(resp) {
				var ind = 0;
				while (elems[ind] != null)
				{
					if (elems[ind].id == resp.data.id)
					{
						var factu = elems[ind].Action;
						var tshirt = elems[ind].ActionTshirt;
						ind = -2;
					}
					ind++;
				}
				var mes = "Bonjour Monsieur " + resp.data.representant.nom + "\n" + "\n" + "Suite à notre dernière échange téléphonique concernant une proposition de partenariat entre nos deux entreprises,\n" + "je me permet de vous rappeler que certains documents sont essentiels à notre collaboration.\n" + "\n" +"Vous trouverez donc ci-joint la déclaration de sous-traitance à remplir.\n" + "\n" +"Merci de joindre également à cette déclaration les éléments suivants :\n" + "\n" + "<strong>• Extrait KBIS ou INSEE</strong>\n" + "<strong>• Photocopie R/V de la pièce d'identité du gérant</strong>\n" + "\n" +"Vous pouvez nous transmettre ces pièces administratives par mail à :\n" + "\n" +"yohann.rhoum@edison-services.fr\n" + "\n" + "\n" + "Ou par voie postal :\n" + "\n" + "<u><b>" + "Edison Services\n" + "Service Partenariat\n" + "32 rue Fernand Pelloutier -921107ClichyS\n" + "</b></u>" + "\n" + "\n" + "Dès réception de ces documents et validation par nos services, vous recevrez par voie postal:\n" + "\n" + "• Un bloc facture Edison Services\n" + "• Un bloc devis Edison Services\n" + "• Un accès à tous nos fournisseurs\n" + "\n" +"Je reste à votre entière disposition pour toutes les questions ou les remarques que vous pourriez avoir.\n" + "\n" + "Dans l'attente d'une réponse favorable de votre part,\n" + "\n" + "Cordialement\n" + "\n" + "<b>Yohann RHOUM</b>\n" + "Service Partenariat\n" + "Tel : 09.72.45.27.10 Fax : 09.72.39.33.46\n" + "yohann.rhoum@edison-services.fr\n" + "\n" + "<b>Edison Services</b>\n" + "Dépannage - Entretien - Installation - Rénovation\n" + "Siège social : 32 rue Fernand Pelloutier, 92110,Clichys\n" + "contact@edison-services.fr - www.edison-services.fr\n"
				if (arc === 1)
				{
					resp.data.status = "ARC";
					var message = "L'artisan n " + resp.data.id +  " a été archivé";
					LxNotificationService.success(message);
		    		edisonAPI.artisan.save(resp.data).then(function(resp2){
						edisonAPI.artisan.activite(resp.data.id, "cause d'Archivage: Archivage Multiple")
		    		})
				}
				else if (env === 1)
				{
		   			edisonAPI.artisan.envoiContrat(resp.data.id, {
					text: mes,
					signe: true
		    		})
		    		var message = "Envoi de contrat pour l'artisan n " + resp.data.id;
		    		LxNotificationService.success(message);
				}
				else if (fac === 1)
				{
					if (tshirt == true && (factu == false || !factu))
					{
						window.open('/api/artisan/' + resp.data.id + '/tshirt');
						edisonAPI.artisan.sendFacturier(resp.data.id, false, false, "tshirt");
					}
					else if (factu == true && (tshirt == false || !tshirt))
					{
						window.open('/api/artisan/'+ resp.data.id + '/facturier')
		    			edisonAPI.artisan.sendFacturier(resp.data.id, "facturier", "deviseur", false);
					}
					else if (tshirt == true && factu == true)
					{
						window.open('/api/artisan/'+ resp.data.id + '/facturier')
						window.open('/api/artisan/' + resp.data.id + '/tshirt');
						edisonAPI.artisan.sendFacturier(resp.data.id, "facturier", "deviseur", "tshirt");
					}
				}
				else if (ref === 1)
				{
		    		resp.data.demandeFacturier.status = 'NO';
		    		edisonAPI.artisan.save(resp.data).then(function(resp2){
		    		})
		    		var message = "Le facturier a été refusé pour Mr." + resp.data.representant.nom
		    		LxNotificationService.success(message);
				}
				else
				{
		    		edisonAPI.artisan.envoiContrat(resp.data.id, {
					text: mes,
					signe: true,
					rappel: true
		    		})
		    		var message = "Relance de documents pour l'artisan n " + resp.data.id
		    		LxNotificationService.success(message);
				}
	    	})
		}
		_this.fac = 0;
		_this.rel = 0
		_this.env = 0;
		_this.arc = 0;
		_this.tshirt = 0;
    }

    _this.removeDemarch = function (elem) {
        edisonAPI.demarchage.remove(elem._id).then(function (resp) {
            _this.tableParams.data.forEach(function (element, i, array) {
                if (element._id === elem._id)
                    array.splice(i, 1);
            });
            var message = _.template("La démarche a bien été retirée de la liste de démarchage.")(resp.data)
            LxNotificationService.success(message);
        }, function (error) {
            LxNotificationService.error(error.data);
        });
    }

    _this.found = function (elem) {
		var dateStr = new Date();
		var dArr = [];
		dArr = _.words(dateStr), /[^- ]+/;
		dArr[1] = dateStr.getMonth() + 1;
		var datev = dArr[2] + "/" + dArr[1] + "/" + dArr[3];
		edisonAPI.demarchage.validedemarche({id: elem._id,login: user.login,date: datev}).then(function (resp) {
		    _this.tableParams.data.forEach(function (element, i, array) {
			if (resp.data._id === element._id)
			{
			    array[i].demarchedate = resp.data.demarchedate
			    array[i].demarchepar = resp.data.demarchepar
			}
		    });
		})
		edisonAPI.demarchage.map({id: elem._id}).then(function (resp) {
		    _this.tableParams.data.forEach(function (element, i, array) {
			if (resp.data._id === element._id)
			{
			    array[i].foundOne = resp.data.foundOne;
			}
		    });
		})
    }
    _this.validate = function (elem) {
		var dateStr = new Date();
		var dArr = [];
		dArr = _.words(dateStr), /[^- ]+/;
		dArr[1] = dateStr.getMonth() + 1;
		var datev = dArr[2] + "/" + dArr[1] + "/" + dArr[3];
		edisonAPI.demarchage.validate({id: elem._id,login: user.login,date: datev}).then(function (resp) {
		    _this.tableParams.data.forEach(function (element, i, array) {
			if (resp.data._id === element._id)
			{
			    array[i].validedate = resp.data.validedate
			    array[i].validepar = resp.data.validepar
			}
		    });

		})
    }

    _this.routeParamsFilter = $routeParams.fltr;
    if (_this.embedded) {
        _this.$watch('filter', function() {
            if (_.size(_this.filter)) {
                _this.customFilter = function(inter) {
                    for (var i in _this.filter) {
                        if (_this.filter[i] !== inter[i])
                            return false
                    }
                    return true
                }
                if (_this.tableParams) {
                    dataProvider.applyFilter({}, _this.tab.hash, _this.customFilter);
                    _this.tableParams.reload();
                }
            }
        })
    }
    _this.displaySubRow = function(inter) {
        return _this.expendedRow && _this.expendedRow === inter.id;
    }

    _this.smallWin = window.innerWidth < 1400
    _this.phoneWin = window.innerWidth < 480
    $(window).resize(function() {
        _this.smallWin = window.innerWidth < 1400
		_this.phoneWin = window.innerWidth < 480
    })

    _this.tab = TabContainer.getCurrentTab();
    _this.tab.hash = currentHash;
    _this.config = config;
    var title = currentFilter ? currentFilter.long_name : _this.model;
    if ($routeParams.sstid) {
        var id = parseInt($routeParams.sstid)
        _this.customFilter = function(inter) {
            return inter.ai === id;
        }
    } else {
        _this.tab.setTitle(title, currentHash);
    }
    if ($routeParams.sstids_in) {
        _this.customFilter = function(inter) {
            return _.includes($routeParams.sstids_in, inter.id);
        }
    }
    if ($routeParams.ids_in) {
        var tab = JSON.parse($routeParams.ids_in)
        _this.customFilter = function(inter) {
            return _.includes(tab, inter.id);
        }
    }

    _this.$watch(function() {
        return $location.search()
    }, _.after(2, function(nw, old) {
        _this.tableParams.filter(_.omit(nw, 'hashModel', 'page', 'sstid', 'ids_in'))
    }), true)

    _this.$watch(function() {
        return $location.hash()
    }, function(nw, old) {
        if (_this.tableParams) {
            dataProvider.applyFilter(currentFilter, nw, _this.customFilter);
            _this.tableParams.reload()
        }
    }, true)

    var actualiseUrl = function(fltrs, page) {
        $location.search('page', page !== 1 ? page : undefined);
        _.each(fltrs, function(e, k) {
            if (!e) e = undefined;
            if (e !== "hashModel") {
                $location.search(k, e);

            } else {}
        })
    }

    var sortBy = (currentFilter && currentFilter.sortBy) ||  {
        id: 'desc'
    }

    dataProvider.init(function(err, resp) {
        dataProvider.applyFilter(currentFilter, _this.tab.hash, _this.customFilter);
        var tableParameters = {
            page: $location.search()['page'] ||  1,
            total: dataProvider.filteredData.length,
            filter: _this.embedded ? {} : _.omit($location.search(), 'hashModel', 'page', 'sstid', 'ids_in'),
            sorting: sortBy,
            count: _this.limit || 100
        };
        var tableSettings = {
            total: dataProvider.filteredData,
            getData: function($defer, params) {
                var data = dataProvider.filteredData;
                if (!_this.embedded) {
                    data = $filter('tableFilter')(data, params.filter());
                }
                _this.currentFilter = _.clone(params.filter());
                params.total(data.length);
				if (_this.routeParamsFilter === "recouvrementArtn" || _this.routeParamsFilter === "recouvrementArt")
                	data = $filter('orderBy')(data, 'a');
				else
					data = $filter('orderBy')(data, params.orderBy());
                $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            },
            filterDelay: 100
        }
        _this.tableParams = new ngTableParams(tableParameters, tableSettings);
        LxProgressService.circular.hide()
    })

    var lastChange = 0;
    $rootScope.$on(_this.model.toUpperCase() + '_CACHE_LIST_CHANGE', function(event, newData) {
        if (TabContainer.getCurrentTab() && _this.tab.fullUrl === TabContainer.getCurrentTab().fullUrl) {
            dataProvider.applyFilter(currentFilter, _this.tab.hash, _this.customFilter);
	    _this.tableParams.reload();
        }
    })
    _this.contextMenu = new ContextMenu(_this.model)

    if (user.service === 'COMPTABILITE') {
        var subs = _.findIndex(_this.contextMenu.list, "title", "Appels");
        if (subs) {
            var tmp = _this.contextMenu.list[subs]
            _this.contextMenu.list.splice(subs, 1);
            _this.contextMenu.list.push(tmp);
        }
    }
    _this.rowRightClick = function($event, inter) {
        edisonAPI[_this.model].get(inter.id, {
                populate: 'sst'
            })
            .then(function(resp) {
                _this.contextMenu.setData(resp.data);
                _this.contextMenu.setPosition($event.pageX - (($routeParams.sstid ||  _this.embedded) ? 50 : 0), $event.pageY + ($routeParams.sstid ||  _this.embedded ? 0 : 200))
                _this.contextMenu.open();
            })
    }

    _this.rowClick = function($event, inter) {
        if (_this.contextMenu.active)
            return _this.contextMenu.close();
        if ($event.metaKey || $event.ctrlKey) {
            TabContainer.addTab('/' + _this.model + '/' + inter.id, {
                title: ('#' + inter.id),
                setFocus: false,
                allowDuplicates: false
            });
        } else {
            if (_this.expendedRow === inter.id) {
                _this.expendedRow = undefined;
            } else {
                _this.expendedRow = inter.id
            }
        }
    }

    _this.absence  = {};
    _this.absence.start  = "";
    _this.absence.end  = "";
    _this.select = {};
    _this.select.start = "";
    _this.select.end = "";

	_this.$watch('select.end', function() {
	    if (_this.select.start != "")
	    {
		var inter = _this.select.end.split(" ");
		var Mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
		for (i = 0;i < Mois.length;i++)
		{
		    if (inter[1] === Mois[i])
			var nbrMonth = i;
		}
		var end = new Date(inter[2],nbrMonth,inter[0]);
		edisonAPI.artisan.datefacturier({
		    start: _this.absence.start.toISOString(),
		    end: end.toISOString()
		}).then(function(resp) {
		    _this.delai = resp;
		})
	    }
	})

    _this.$watch('select.start', function() {
	if (_this.select.end != "")
	{
	    var inter = _this.select.start.split(" ");
	    var Mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
	    for (i = 0;i < Mois.length;i++)
	    {
		if (inter[1] === Mois[i])
		    var nbrMonth = i;
	    }
	    var start = new Date(inter[2],nbrMonth,inter[0]);
	    edisonAPI.artisan.datefacturier({
		start: start.toISOString(),
		end: _this.absence.end.toISOString()
	    }).then(function(resp) {
		_this.delai = resp;
	    })
	}
    })

    _this.prospectModif = function (cate, ville) {
	$location.url('/prospect/' + ville + '/' + cate);
    }
}

angular.module('edison').directive('lineupIntervention', ["$timeout", "TabContainer", "FiltersFactory", "user", "ContextMenu", "LxProgressService", "LxNotificationService", "edisonAPI", "DataProvider", "$routeParams", "$location", "$rootScope", "$filter", "config", "ngTableParams", "MomentIterator", "openPost", function($timeout, TabContainer, FiltersFactory, user, ContextMenu, LxProgressService, LxNotificationService, edisonAPI, DataProvider, $routeParams, $location, $rootScope, $filter, config, ngTableParams, MomentIterator, openPost) {
    "use strict";
    var arg = arguments;
    return {
        replace: false,
        restrict: 'E',
		templateUrl: function(){

	    	if (arg[9].fltr === "SAV")
				var Temp = '/Templates/lineup-sav.html';
	    	else if (arg[9].fltr === "litst")
				var Temp = '/Templates/lineup-litstatus.html';
			else if (arg[9].fltr === "recouvrementArt" || arg[9].fltr === "recouvrementArtn")
				var Temp = '/Templates/lineup-recouvrement.html';
			else if (arg[9].fltr === "recLitClos" || arg[9].fltr === "recLitNonPris" || arg[9].fltr === "recLitPris"
				|| arg[9].fltr === "recRelLit" || arg[9].fltr === "recLitAutres" || arg[9].fltr === "recLitOpposition"
				|| arg[9].fltr === "recLitRisqueOpposition" || arg[9].fltr === "recLitQualite"
				|| arg[9].fltr === "recLitTarif" ||  arg[9].fltr === "recLitPlaintes" || arg[9].fltr === "relRec"
				|| arg[9].fltr === "recClientClos"|| arg[9].fltr === "recClientNonPris" || arg[9].fltr === "recClientPris"
				|| arg[9].fltr === "recPasDeContact" || arg[9].fltr === "recPlaintes" || arg[9].fltr === "recFactureNonRecu"
				|| arg[9].fltr === "recRetEnvoi" || arg[9].fltr === "recImpaye" || arg[9].fltr === "recChequePerdu"
				|| arg[9].fltr === "recTarif" || arg[9].fltr === "recQualite" || arg[9].fltr === "recAutres"
        || arg[9].fltr === "recClientEncaiss"|| arg[9].fltr === "recLitigeEncaiss"
        || arg[9].fltr === "recClientEncaiss1"|| arg[9].fltr === "recLitigeEncaiss1"
        || arg[9].fltr === "recClientEncaiss2"|| arg[9].fltr === "recLitigeEncaiss2"
        || arg[9].fltr === "recClientEncaiss3"|| arg[9].fltr === "recLitigeEncaiss3"
        || arg[9].fltr === "recClientEncaiss4"|| arg[9].fltr === "recLitigeEncaiss4"
        || arg[9].fltr === "recClientEncaiss5"|| arg[9].fltr === "recLitigeEncaiss5"
        || arg[9].fltr === "recClientEncaiss6"|| arg[9].fltr === "recLitigeEncaiss6"
        || arg[9].fltr === "recClientEncaiss7"|| arg[9].fltr === "recLitigeEncaiss7"
        || arg[9].fltr === "recClientEncaiss8"|| arg[9].fltr === "recLitigeEncaiss8"
        || arg[9].fltr === "recClientEncaiss9"|| arg[9].fltr === "recLitigeEncaiss9"
        || arg[9].fltr === "recClientEncaiss10"|| arg[9].fltr === "recLitigeEncaiss10"
        || arg[9].fltr === "recClientEncaiss11"|| arg[9].fltr === "recLitigeEncaiss11"
        || arg[9].fltr === "recClientEncaiss12"|| arg[9].fltr === "recLitigeEncaiss12"
      )
				var Temp = '/Templates/lineup-recouvrementClient.html';
			else if (arg[9].fltr === "attenteVerif")
				var Temp = '/Templates/lineup-attenteVerif.html';
	    	else
				var Temp = '/Templates/lineup-intervention.html';
	    	return Temp
		},
        scope: {
            limit: '=',
            embedded: '=',
            filter: '=',
        },
        controller: ["$scope", function($scope) {
            $scope.model = 'intervention'
            Controller.apply($scope, arg)
        }]
    }
}]);

angular.module('edison').directive('lineupDevis', ["$timeout", "TabContainer", "FiltersFactory", "user", "ContextMenu", "LxProgressService", "LxNotificationService", "edisonAPI", "DataProvider", "$routeParams", "$location", "$rootScope", "$filter", "config", "ngTableParams", "MomentIterator", "openPost", function($timeout, TabContainer, FiltersFactory, user, ContextMenu, LxProgressService, LxNotificationService, edisonAPI, DataProvider, $routeParams, $location, $rootScope, $filter, config, ngTableParams, MomentIterator, openPost) {
    "use strict";
    var arg = arguments;
    return {
        replace: false,
        restrict: 'E',
        templateUrl: '/Templates/lineup-devis.html',
        scope: {
            filter: '=',
        },
        controller: ["$scope", function($scope) {
            $scope.model = 'devis'
            Controller.apply($scope, arg)
        }]
    }
}]);

angular.module('edison').directive('lineupArtisan', ["$timeout", "TabContainer", "FiltersFactory", "user", "ContextMenu", "LxProgressService", "LxNotificationService", "edisonAPI", "DataProvider", "$routeParams", "$location", "$rootScope", "$filter", "config", "ngTableParams", "MomentIterator", "openPost", function($timeout, TabContainer, FiltersFactory, user, ContextMenu, LxProgressService, LxNotificationService, edisonAPI, DataProvider, $routeParams, $location, $rootScope, $filter, config, ngTableParams, MomentIterator, openPost) {
    "use strict";
    var arg = arguments;
    return {
        replace: false,
        restrict: 'E',
        templateUrl: function(){
	    if (arg[9].fltr === "needFacturier" || arg[9].fltr === "histEnvoi")
		var Temp = '/Templates/lineup-facturier.html'
	    else
		var Temp = '/Templates/lineup-artisan.html'
	    return Temp
	},
        scope: {
	    filter: '=',
        },
        controller: ["$scope", function($scope) {
            $scope.model = 'artisan'
            Controller.apply($scope, arg)
        }]
    }
 }]);

angular.module('edison').directive('lineupDemarchage', ["$timeout", "TabContainer", "FiltersFactory", "user", "ContextMenu", "LxProgressService", "LxNotificationService", "edisonAPI", "DataProvider", "$routeParams", "$location", "$rootScope", "$filter", "config", "ngTableParams", "MomentIterator", "openPost", function($timeout, TabContainer, FiltersFactory, user, ContextMenu, LxProgressService, LxNotificationService, edisonAPI, DataProvider, $routeParams, $location, $rootScope, $filter, config, ngTableParams, MomentIterator, openPost) {
    "use strict";
    var arg = arguments;
    return {
        replace: false,
        restrict: 'E',
        templateUrl: '/Templates/lineup-demarchage.html',
        scope: {
            filter: '=',
        },
        controller: ["$scope", function($scope) {
            $scope.model = 'demarchage'
            Controller.apply($scope, arg)
        }]
    }
}]);

angular.module('edison').directive('ngRightClick', ["$parse", function($parse) {
    "use strict";
    return function(scope, element, attrs) {
        element.bind('contextmenu', function(event) {
            if (!(event.altKey ||  event.ctrlKey || event.shiftKey ||  ["INPUT", "TEXTAREA"].indexOf(event.target.nodeName) >= 0)) {
                scope.$apply(function() {
                    event.preventDefault();
                    $parse(attrs.ngRightClick)(scope, {
                        $event: event
                    });
                });
            }
        });
    };
}]);

 angular.module('edison').directive('link', ["FiltersFactory", "$rootScope", function(FiltersFactory, $rootScope) {
     "use strict";
     return {
         restrict: 'AE',
         replace: true,
         template: '<li>' +
             '      <a href="{{fullUrl}}" >' +
             '            <i ng-if="icon" class = "menu-icon fa fa-{{icon}}"> </i>' +
             '            <span ng-class="{bold : bold, textWhite: textWhite}" class="mm-text">{{title || exFltr.long_name}}</span>' +
             '            <span ng-if="total"class="label label-{{_color}}">{{total}}</span>' +
             '        </a>' +
             '      </li>',
         scope: {
             fltr: '@',
             login: '@',
             today: '@',
             icon: '@',
             title: '@',
             url: '@',
             textWhite: '@',
             model: '@',
             bold: '@',
             count: '@',
             noCounter: '@',
             color: '@',
             hashModel: '@'
         },
         link: function(scope, element, attrs) {
             var findTotal = function() {
                if (scope.count) {
                    return scope.count
                }
                 if (scope.noCounter)
                     return undefined;
                 var total = 0;
                 if (scope.login) {
                     var t = _.find($rootScope.interventionsStats, function(e) {
                         return e.login === scope.login;
                     })
                     total += _.get(t, scope.fltr + '.total', 0);
                 } else {
                     _.each($rootScope.interventionsStats, function(t) {
                         total += _.get(t, scope.fltr + '.total', 0);
                     })
                 }
                 return total;
             }
             $rootScope.$watch('interventionsStats', function() {
                 scope.total = findTotal();
             })
             scope.$watch('login', function(current, prev) {
                 scope._color = (scope.color || 'success')
                 scope._model = scope.model || 'intervention';
                 var filtersFactory = new FiltersFactory(scope._model);
                 scope.exFltr = filtersFactory.getFilterByName(scope.fltr);
                 scope.total = findTotal();
                 scope.exFltr = scope.exFltr ||  {
                     url: ''
                 };
                 scope._url = scope.exFltr.url.length ? "/" + scope.exFltr.url : scope.exFltr.url;
                 scope._login = scope.login && !scope.hashModel ? ("#" + scope.login) : '';
                 scope._hashModel = scope.hashModel ? ("?" + scope.hashModel + "=" + scope.login) : '';
                 scope.fullUrl = scope.url ||  ('/' + scope._model + '/list' + scope._url + scope._hashModel + scope._login)
             })

         }
     };
 }]);

 angular.module('edison').directive('simpleLink', ["FiltersFactory", "$rootScope", function(FiltersFactory, $rootScope) {
     "use strict";
     return {
         restrict: 'AE',
         replace: true,
         template: '<li>' +
             '      <a href="{{url}}"  target="{{target}}">' +
             '            <i ng-if="icon" class = "menu-icon fa fa-{{icon}}"> </i>' +
             '            <span class="mm-text">{{title}}</span>' +
             '        </a>' +
             '      </li>',
         scope: {
             icon: '@',
             title: '@',
             url: '@',
         },
         link: function(scope, element, attrs) {
             scope.target = (typeof attrs.extern === 'string' ? '_blank' : '')
         }
     };
 }]);


 angular.module('edison').directive('linkSeparator', function() {
     "use strict";
     return {
         restrict: 'AE',
         replace: true,
         template: '<li>' +
             '      <a>' +
             '            <i ng-if="icon" class = "menu-icon fa fa-{{icon}}"> </i>' +
             '            <strong><span class="mm-text">{{title}}</span></strong>' +
             '        </a>' +
             '      </li>',
         scope: {
             icon: '@',
             title: '@',
         },
         link: function(scope, element, attrs) {

         }
     };
 });


 angular.module('edison').service('sidebarSM', function() {

     var C = function() {
         this.display = false;
     };
     C.prototype.set = function(name, value) {
         this[name] = value;
     }
     return new C();

 });



 angular.module('edison').directive('sideBar', ["sidebarSM", function(sidebarSM) {
     "use strict";
     return {
         replace: false,
         restrict: 'E',
         templateUrl: '/Directives/side-bar.html',
         transclude: true,
         scope: {},
         link: function(scope, element, attrs) {
             scope.sidebarSM = sidebarSM;
         }
     }
 }]);

angular.module('edison').directive('dropDown', ["config", "sidebarSM", "$timeout", "$document", function(config, sidebarSM, $timeout, $document) {
     "use strict";

     var prev;

     return {
         replace: true,
         restrict: 'E',
         templateUrl: '/Directives/dropdown.html',
         transclude: true,
         scope: {
             title: '@',
             icon: '@',
             isOpen: '@',
             openDefault: '&'
         },
         link: function(scope, element, attrs) {
             scope.openDefault = scope.$eval(scope.openDefault)
             scope.isopen = scope.openDefault
             scope.toggleSidebar = function($event, $elem) {
                 var $ul = $(element).find('>ul')
                 if ($('#main-menu').width() > 200 || $event.currentTarget.parentElement.id.slice(0, 7) !== "service") {
                     if (scope.isopen) {
                         $ul.velocity({
                             height: 0
                         }, 200, function() {
                             scope.$apply(function() {
                                 scope.isopen = false;
                             })
                         });
                     } else {
                         $ul.css('height', '100%')
                         scope.isopen = true
                     }
                 } else {
		     if (prev && JSON.stringify(prev) === JSON.stringify($ul))
		     {
			 prev.html($('#mmc-ul > .mmc-wrapper').find(">*"))
			 sidebarSM.set("display", false);
			 $('#mmc-ul > .mmc-wrapper').html('');
			 prev = null;
		     }
		     else if (prev)
		     {
			 prev.html($('#mmc-ul > .mmc-wrapper').find(">*"))
			 $('#mmc-ul > .mmc-wrapper').html($ul.find('>*'));
			 prev = $ul;
		     }
		     else
		     {
			 sidebarSM.set("display", true);
			 $('#mmc-ul > .mmc-wrapper').html($ul.find('>*'));
			 prev = $ul;
		     }
                 }
             }

	     element.bind('click', function(event) {
		 if (scope.$root && scope.$root.phoneWin === true)
		     event.stopPropagation();
	     });

	     $document.bind('click', function(){
		 var $ul = $(element).find('>ul')
		 if (prev && JSON.stringify(prev) === JSON.stringify($ul))
		 {
		     prev.html($('#mmc-ul > .mmc-wrapper').find(">*"))
		     sidebarSM.set("display", false);
		     $('#mmc-ul > .mmc-wrapper').html('');
		     prev = null;
		 }
	     });
         }
     };
 }]);

angular.module('edison').directive('signalement', ["edisonAPI", "LxNotificationService", "dialog", function(edisonAPI, LxNotificationService, dialog) {
    "use strict";
	/* Directive pour faire un signalement dans une intervention */
    return {
        replace: false,
        restrict: 'E',
        templateUrl: '/Templates/signalement.html',
        scope: {
            data: '=',
            exit: '&',
        },
        link: function(scope, elem) {
            scope.setSelectedSubType = function(subType) {
                scope.selectedSubType = scope.selectedSubType === subType ? null : subType
            }
            edisonAPI.signal.list().then(function(resp) {
                scope.signalementsGrp = _.groupBy(resp.data, 'subType');
            })
            scope.hide = function(signal) {
				/* Ouverture et appel du pop up commentaires/raison */
		    	dialog.pop_up_level2_comment(signal.nom, function(resp, cancel){
					if (!resp.commentaire)
					    resp.commentaire = "Aucune"
					var check = resp.commentaire;
					/* Si on clique sur valider cancel = true */
					if (cancel)
					{
						var arrayDest = [];
						/* Creation du tableau des destinataires */
						for (var ind = 0; ind < signal.dest.users.length; ind++)
						{
							/* Tri pour le spécial */
							if (signal.dest.special && signal.dest.special.ajoutI && signal.dest.special.ajoutI === true && scope.data.login.ajout
								&& scope.data.login.ajout == signal.dest.users[ind].login)
								arrayDest.push(signal.dest.users[ind].email);
							else if (signal.dest.special && signal.dest.special.envoiI && signal.dest.special.envoiI === true && scope.data.login.envoi
								&& scope.data.login.envoi == signal.dest.users[ind].login)
								arrayDest.push(signal.dest.users[ind].email);
							else if (signal.dest.special && signal.dest.special.verifI && signal.dest.special.verifI === true && scope.data.login.verification
								&& scope.data.login.verification == signal.dest.users[ind].login)
								arrayDest.push(signal.dest.users[ind].email);
							else if (signal.dest.special && signal.dest.special.annulI && signal.dest.special.annulI === true && scope.data.login.annulation
								&& scope.data.login.annulation == signal.dest.users[ind].login)
								arrayDest.push(signal.dest.users[ind].email);
							else if (signal.dest.special && signal.dest.special.ajoutA && signal.dest.special.ajoutA === true && scope.data.artisan.login.ajout
								&& scope.data.artisan.login.ajout == signal.dest.users[ind].login)
								arrayDest.push(signal.dest.users[ind].email);
							/* Tri pour les services */
							else if ((signal.dest.service && signal.dest.service.commerc === true && signal.dest.users[ind].service == "INTERVENTION")
										|| (signal.dest.service && signal.dest.service.partena === true && signal.dest.users[ind].service == "PARTENARIAT")
										|| (signal.dest.service && signal.dest.service.compta === true && signal.dest.users[ind].service == "COMPTABILITE")
										|| (signal.dest.service && signal.dest.service.recouv === true && signal.dest.users[ind].service == "RECOUVREMENT"))
								arrayDest.push(signal.dest.users[ind].email);
							/* Tri pour les users cochés */
							else if (signal.dest.users[ind].send === true)
								arrayDest.push(signal.dest.users[ind].email);
							/* Tri pour l'émitteur */
							else if (signal.dest.users[ind].login == scope.data.login.ajout)
								arrayDest.push(signal.dest.users[ind].email);
						}
				 	   	signal.isStar = scope.data.sst.star;
						/* Appel de la fonction d'envoi du signalement */
						edisonAPI.signalement.add(_.merge(signal, {
							addBy: scope.data.login.ajout || 'Non ajoutée',
							verifBy: scope.data.login.verification || 'Non vérifiée',
							inter_id: scope.data.id || scope.data.tmpID,
							raison: check || "Raison: Aucune",
							arrayDest: arrayDest,
							sst_id: scope.data.sst && scope.data.sst.id,
							sst_nom: scope.data.sst && scope.data.sst.nomSociete,
					    })).then(function() {
							LxNotificationService.success("Le signalement a été envoyé.")
				  	  	})
					}
		    	})
                return scope.exit && scope.exit()
            }
        }
    }
 }]);

 angular.module('edison').directive('trello', ["user", "edisonAPI", function(user, edisonAPI) {
    "use strict";
    return {
        replace: false,
        restrict: 'E',
        templateUrl: '/Templates/trello.html',
        scope: {
            data: '=',
        },
        link: function(scope, elem) {

            var xmap = function(e) {
                e.checked = e.state === 'complete';
                return e;
            }
            scope.reload = function() {
                edisonAPI.tasklist.get(moment().format('DD-MM-YYYY'), user.login).then(function(resp) {
                    scope.tasklist = resp.data
                    resp.data.checkItems = resp.data.checkItems.map(xmap)
                })
            }
            scope.reload()
            scope.check = function(task) {
                task.listID = scope.tasklist.id;
                task.cardID = scope.tasklist.cardID;
                edisonAPI.tasklist.update(_.clone(task)).then(function(resp) {
                    task = xmap(resp.data)
                })
                task.checked = !task.checked;
            }
        }
    }
 }]);

angular.module('edison').factory('TabContainer', ["$location", "$window", "$q", "edisonAPI", function($location, $window, $q, edisonAPI) {
    "use strict";
    var Tab = function(args, options, prevTab) {

        if (typeof args === 'object') {
            //copy constructor
            _.each(args, function(e, k) {
                this[k] = e;
            })
        } else {
            this.prevTab = prevTab ||  {}
            this.urlFilter = options.urlFilter
            this.hash = options.hash
            this.url = args;
            this.fullUrl = this.url + ['', $.param(this.urlFilter)].join(_.isEmpty(this.urlFilter) ? '' : '?') + (this.hash ? '#' + this.hash : '')
            this.title = '';
            this.position = null;
            this.deleted = false;
            this._timestamp = Date.now();
        }
    }

    Tab.prototype.setData = function(data) {
        //slice create a copy
        this.data = data;
    }

    Tab.prototype.setTitle = function(title, subTitle) {
        this.title = title;
    }

    var TabContainer = function() {
        this._tabs = [];
        this.selectedTab = 0;
    }

    TabContainer.prototype.loadSessionTabs = function(currentUrl) {
        var _this = this;

        return $q(function(resolve, reject) {
            var currentUrlInSessionTabs = false;
            edisonAPI.request({
                fn: "getSessionData",
            }).then(function(result) {
                _this.selectedTab = result.data.selectedTab;
                for (var i = 0; i < result.data._tabs.length; i++) {
                    _this._tabs.push(new Tab(result.data._tabs[i]))
                    if (result.data._tabs[i].url === currentUrl) {
                        _this.selectedTab = i;
                        currentUrlInSessionTabs = true;
                    }
                }
                if (!currentUrlInSessionTabs) {
                    return reject();
                }
                return resolve();
            }).catch(reject);

        })

    }

    TabContainer.prototype.setFocus = function(tab) {
        this.selectedTab = (typeof tab === 'number' ? tab : tab.position);
    };

    TabContainer.prototype.createTab = function(url, options) {
        var tab = new Tab(url, options, this.getCurrentTab());
        tab.position = this._tabs.length;
        this._tabs.push(tab);
        return (tab);
    }

    TabContainer.prototype.isOpen = function(url, options) {
        var index = _.findIndex(this._tabs, function(e) {
            return !e.deleted && e.url === url &&
                (!options.hash && !e.hash || options.hash == e.hash) &&
                _.isEqual(options.urlFilter, e.urlFilter)
        });
        return (index >= 0);
    };

    TabContainer.prototype.getTab = function(url, options) {

        return _.find(this._tabs, function(e) {
            return !e.deleted && e.url === url &&
                (!options.hash && !e.hash || options.hash == e.hash) &&
                _.isEqual(options.urlFilter, e.urlFilter)
        });
    };

    TabContainer.prototype.getTabSimple = function(url, options) {

        return _.find(this._tabs, function(e) {
            return !e.deleted && e.url.split('/')[1] === url.split('/')[1]
        });
    };

    TabContainer.prototype.len = function() {
        var size = 0;

        this._tabs.forEach(function(e) {
            size += !e.deleted;
        })
        return (size);
    }

    TabContainer.prototype.getPrevTab = function(tab) {

        for (var i = tab.position - 1; i >= 0; i--) {
            if (this._tabs[i].deleted === false)
                return (this._tabs[i]);
        }
    };

    TabContainer.prototype.close = function(tab) {
        if (this.len() > 1) {
            if (this.remove(tab)) {
                $location.url(tab.prevTab.fullUrl ||  '/intervention/list')
            }
        } else {
            $location.url(tab.prevTab.fullUrl ||  '/intervention/list');
            this.noClose = true;
            //this.remove(tab);
        }
    }

    TabContainer.prototype.remove = function(tab) {
        var newTabs = [];
        var j = 0;

        if (this._tabs.length <= 1) {
            return false;
        }
        var reload = (this.selectedTab === tab.position);
        for (var i = 0; i < this._tabs.length; i++) {
            if (i !== tab.position) {
                newTabs.push(this._tabs[i]);
                newTabs[j].position = j;
                ++j;
            }
        }
        this._tabs = newTabs;

        if (this.selectedTab === tab.position && this.selectedTab !== 0) {
            this.selectedTab--;
        }
        if (this.selectedTab > tab.position) {
            this.selectedTab--;
        }
        return (reload);
    }

    TabContainer.prototype.getCurrentTab = function() {
        return this._tabs[this.selectedTab];
    }
    TabContainer.prototype.addTab = function(url, options) {
        var tab;
        var noOpen = [
            '/list',
            '/search',
            '/recap',
            'lpa',
            '/artisan/contact',
            '/tools/edit',
        ]
        if (this.noClose) {
            tab = this._tabs[0]
        } else if (_.find(noOpen, _.partial(_.includes, url)) && this.getTabSimple(url)) {
            tab = this.getTabSimple(url);
        } else if (this.noClose || this.isOpen(url, options)) {
            tab = this.getTab(url, options)
        } else {
            tab = this.createTab(url, options);
        }
        this.noClose = false;
        if (!(options && options.setFocus === false)) {
            this.setFocus(tab)
        }
        if (options && options.title) {
            tab.setTitle(options.title);
        }
    }

    return (new TabContainer());

}]);

angular.module('edison').factory('Signalement', ["edisonAPI", function(edisonAPI) {
    "use strict";

    var Signalement = function(inter) {
        this.intervention = inter;
    }

    Signalement.prototype.list = []
    return Signalement;
}]);

angular.module('edison').factory('Tab', function() {


    var Tab = function(container, location) {
        this.container = container;
        this.title = "...";
        this.path = location.path();
        this.url = location.path().split('/').slice(1)
        this.model = this.url[0]
        this.route = this.url[1]
        this.hash = location.hash();
        this.title = this.url[this.url.length - 1]
        this.date = new Date;
    }
    Tab.prototype.setTitle = function(title) {
        this.title = title
        return this;
    };

    Tab.prototype.close = function() {
        this.container.close(this);
    }

    Tab.prototype.setData = function(data) {
        this.data = data;
        return this;
    };
    return Tab;

})

angular.module('edison').factory('TabContainer', ["Tab", "$location", function(Tab, $location) {
    "use strict";

    var TabContainer = {
        __tabs: [],
        __ordered: {}
    }


    TabContainer.find = function(location) {
        var cmp = new Tab(this, location)
        return _.find(this.__tabs, function(e) {
            if (e.route === 'list' && cmp.route === 'list') {
                return cmp.model === e.model
            }
            return e.path == location.path() && e.hash == location.hash()
        })
    }

    TabContainer.ordered = function() {
        return this.__ordered;
    }
    TabContainer.close = function(tab) {
        var index = _.findIndex(this.__tabs, function(e) {
            return e.path == tab.path && e.hash == location.hash
        })
        this.__tabs.splice(index, 1);
        $location.url((this.prevTab && this.prevTab.path != this.selectedTab.path && this.prevTab.path) ||  '/intervention/list');
    }



    TabContainer.add = function(location) {
        var tab = this.find(location);
        this.prevTab = this.selectedTab
        if (!tab) {
            this.selectedTab = new Tab(this, location);
            this.__tabs.push(this.selectedTab)
        } else {
            this.selectedTab = tab
        }
        return this;
    }

    TabContainer.getCurrentTab = function() {
        return this.selectedTab;
    }
    TabContainer.order = function() {
        var _this = this;
        var models = ["intervention", "artisan", "devis", 'tools', 'compta'];
        var tmp = {};
        _.each(_this.__tabs, function(e) {
            if (_.includes(models, e.model) && e.url[1] !== 'list' && e.url[1] !== 'contact') {
                var dest = _.endsWith(e.model, 's') ? e.model : e.model + 's';
            } else {
                dest = 'Recents';
            }
            tmp[dest] = tmp[dest] || {
                title: dest,
                tabs: []
            };
            tmp[dest].tabs.push(e)
        })
        this.__ordered = tmp
        return this;
    }
    TabContainer.getOrdered = function() {
        return this.__ordered;
    }


    return TabContainer

}]);

angular.module('edison').factory('Address', function() {
  "use strict";

  var Address = function(place, copyContructor) {
    if (place.lat && place.lng) {
      this.lt = place.lat;
      this.lg = place.lng;
    } else if (copyContructor) {
      this.getAddressProprieties(place);
      this.streetAddress = true;
    } else if (this.isStreetAddress(place)) {
      this.getPlaceProprieties(place);
    } else if (this.isLocalityAddress(place)) {
      this.getPlaceLocalityProprieties(place);
    } else if (place.types[0] === 'route') {
      this.getRouteProprieties(place);
    } else if (place.types[0] === 'postal_code') {
      this.getCpProprieties(place);
    }
    if (place.geometry) {
      this.lt = place.geometry.location.lat();
      this.lg = place.geometry.location.lng();
    }
    this.latLng = this.lt + ', ' + this.lg;
  };

  Address.prototype.getCpProprieties = function(place) {
    var a = place.address_components;
    this.cp = a[0] && a[0].short_name;
    this.v = a[1] && a[1].short_name;
  }

  Address.prototype.getRouteProprieties = function(place) {
    var a = place.address_components;
    this.n = 1;
    this.r = a[1] && a[0].short_name;
    this.cp = a[5] && a[5].short_name;
    this.v = a[1] && a[1].short_name;
  }

  Address.prototype.getPlaceLocalityProprieties = function(place) {
    var a = place.address_components;
    this.cp = a[1] && a[1].short_name;
    this.v = a[0] && a[0].short_name;
  }

  Address.prototype.getPlaceProprieties = function(place) {
    var a = place.address_components;
    this.n = a[0] && a[0].short_name;
    this.r = a[1] && a[1].short_name;
    this.cp = a[6] && a[6].short_name;
    this.v = a[2] && a[2].short_name;
  }

  Address.prototype.getAddressProprieties = function(address) {
    this.n = address.n;
    this.r = address.r;
    this.cp = address.cp;
    this.v = address.v;
    this.lt = address.lt;
    this.lg = address.lg;
    this.code = address.code;
    this.etage = address.etage;
    this.batiment = address.batiment;
  }

  Address.prototype.isLocalityAddress = function(place) {
    this.localityAddress = (place.types.indexOf("locality") >= 0);
    return this.localityAddress;
  }

  Address.prototype.isStreetAddress = function(place) {
    var noStreet = ["locality", "country", "postal_code", "route", "sublocality"];
    this.streetAddress = (noStreet.indexOf(place.types[0]) < 0);
    return (this.streetAddress);
  }

  Address.prototype.toString = function() {
    return (this.n + " " + this.r + " " + this.cp + ", " + this.v + ", France")
  }

  return (function(place, copyContructor) {
    return new Address(place, copyContructor);
  })
});

angular.module('edison').factory('edisonAPI', ["$http", "$location", "Upload", function($http, $location, Upload) {
    "use strict";
    return {
	bug: {
	    declare: function(params) {
		return $http.post('/api/bug/declare', params)
	    },
	},
	raison: {
		saveRaison: function(params) {
			return $http.post('/api/raison/saveRaison', params);
		},
		list: function() {
		return $http.get('/api/raison/list');
	    },
	},
	fournisseur: {
		saveFournisseur: function(params) {
			return $http.post('/api/fournisseur/saveFournisseur', params);
		},
		list: function() {
		return $http.get('/api/fournisseur/list');
	    },
	},
	demarchage: {
        add: function(params) {
		return $http.post('/api/demarchage/add', params)
            },
	    map: function(params) {
		return $http.post('/api/demarchage/map', params);
	    },
	    validate: function(params) {
		return $http.post('/api/demarchage/validate', params);
	    },
	    validedemarche: function(params) {
		return $http.post('/api/demarchage/validedemarche', params);
	    },
        list: function() {
		return $http.get('/api/demarchage/list');
            },
        remove: function(id) {
		return $http.post('/api/demarchage/remove', {
            id: id,
		});
            },
	},
	marker: {
	    list: function (cat) {
		return $http.get('/api/marker/list', {
		    params: cat
		});
	    }
	},
	user: {
	    history: function(login) {
		return $http.get('/api/user/' + login + '/history')
	    },
	    loginuser: function(data) {
		return $http.get('/api/user/loginuser', data)
	    },
	},
	product: {
	    list: function() {
		return $http.get('/api/product/list');
	    },
	    save: function(data) {
		return $http.post('/api/product/__save', data);
	    }
	},
	signal: {
		saveDest: function(data) {
		return $http.post('/api/signal/saveDest', data);
	    },
	    list: function() {
		return $http.get('/api/signal/list');
	    },
	    update: function(data) {
		return $http.post('/api/signal/update', data);
	    },
	    save: function(data) {
		return $http.post('/api/signal/__save', data);
	    }
	},
	compte: {
	    list: function() {
		return $http.get('/api/compte/list');
	    },
	    save: function(data) {
		return $http.post('/api/compte/__save', data);
	    }
	},
	combo: {
	    list: function() {
		return $http.get('/api/combo/list');
	    },
	    save: function(data) {
		return $http.post('/api/combo/__save', data);
	    }
	},
	stats: {
	    telepro: function() {
		return $http.get('/api/stats/telepro');
	    },
	    day: function() {
		return $http.get('/api/stats/day');
	    }
	},
	users: {
	    logout: function() {
		return $http.get('/logout');
	    },
	    save: function(data) {
		return $http.post('/api/user/__save', data);
	    },
	    list: function() {
		return $http.get('/api/user/list');
	    },
	    upload: function(data) {
		return $http.post('/api/user/upload', data)
	    },
	    cancel: function (data) {
		return $http.post('/api/user/cancel', data)
	    }
	},
	bfm: {
	    get: function() {
		return $http.get('/api/bfm');
	    }
	},
	compta: {
	    getCA: function (year) {
			return $http.get('/api/intervention/getCA', {
			    params: year
			});
	    },
	    remise: function(data) {
			return $http.get('/api/intervention/remise?d=' + (data.d ||  ''));
	    },
	    lpa: function(data) {
			return $http.get('/api/intervention/lpa?d=' + (data.d ||  ''));
	    },
	    getFourniture: function(year) {
			return $http.get('/api/intervention/getFourniture', {
			    params: year
			});
	    },
	    flush: function(data) {
		return $http.post('/api/intervention/flush', data);
	    },
	    flushMail: function(data) {
		return $http.post('/api/intervention/flushMail', data);
	    },
	    listRemises: function() {
		return $http.get('/api/remise/listRemises');
	    },
	    addCheques: function() {
		return $http.get('/api/remise/addCheques');
	    },
	    validateRemise: function() {
		return $http.get('/api/remise/validateRemise');
	    },
	    archivesPaiement: function() {
		return $http.get('/api/intervention/archivePaiement');
	    },
	    archivesReglement: function() {
		return $http.get('/api/intervention/archiveReglement');
	    },
	    avoirs: function() {
		return $http.get('/api/intervention/avoirs')
	    },
	    flushAvoirs: function(data) {
		return $http.post('/api/intervention/flushAvoirs', data);
	    },

	},
	devis: {
		relanceAuto7h: function() {
		return $http.post('/api/devis/relanceAuto7h');
		},
	    getInfo: function (user) {
		return $http.get('/api/devis/getInfo', {
		    params: user
		});
	    },
	    saveTmp: function(data) {
		return $http.post('/api/devis/temporarySaving', data);
	    },
	    getTmp: function(id) {
		return $http.get('/api/devis/temporarySaving?id=' + id);
	    },
	    get: function(id, options) {
		return $http({
		    method: 'GET',
		    cache: false,
		    url: '/api/devis/' + id,
		    params: options || {}
		}).success(function(result) {
		    return result;
		});
	    },
	    save: function(params) {
		if (!params.id) {
		    return $http.post("/api/devis", params)
		} else {
		    return $http.post("/api/devis/" + params.id, params)

		}
	    },
	    envoi: function(id, options) {
		return $http.post("/api/devis/" + id + "/envoi", options);
	    },
	    annulation: function(id, causeAnnulation) {
		return $http.post("/api/devis/" + id + "/annulation");
	    },
	    list: function() {
		return $http.get('api/devis/getCacheList')
	    },
	},
	log: {
	    getWeekLog: function(options) {
		return $http.get('/api/log/getWeekLog', {
		    params: options
		});
	    },
	    getLate: function(options) {
		return $http.get('/api/log/getLate', {
		    params: options
		});
	    },
	},
	ovh: {
	    getAll: function() {
		return $http.get('/api/callovh/getAll');
	    },
	    getNumber: function(options) {
		return $http.get('/api/callovh/getNumber', {
		    params: options
		});
	    },
	    getSingle: function(options) {
		return $http.get('/api/callovh/getSingle', {
		    params: options
		});
	    },
	    getStats: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/ovh/ovhStats",
		    params: options
		});
	    },
	    getInfo: function (obj) {
		return $http.get('/api/ovh/getInfo', {
		    params: obj
		})
	    },
	    getBill: function (obj) {
		return $http.get('/api/ovh/getBill', {
		    params: obj
		})
	    },
	    getAccounts: function () {
		return $http.get('/api/ovh/getAccounts')
	    },
	    getPhone: function () {
		return $http.get('/api/ovh/getPhone')
	    },
	    getComptes: function (obj) {
		return $http.post('/api/ovh/ovhAccount', {
		    params: {
			dayStart: obj.d,
			type: obj.t,
			account: obj.a,
			line: obj.n
		    }
		})
	    },
	    demStats: function (obj) {
		return $http.post('/api/ovh/ovhDemStats', {
		    params: {
			dayStart: obj.d,
			type: obj.t,
			account: obj.a,
			line: obj.n
		    }
		})
	    },
	    getInterStatus: function (obj) {
		return $http.get('/api/ovh/interStatus', {
		    params: {
			dayStart: obj.d,
			type: obj.t,
			account: obj.a,
			line: obj.n,
			line2: obj.l
		    }
		})
	    },
	},
	intervention: {
		statsRecou: function(options) {
			return $http({
				method: 'GET',
				url: "/api/intervention/statsRecou",
				params: options
			});
		},
		saveEncaissement: function(params) {
			return $http.post('/api/intervention/saveEncaissement', params);
		},
		saveDecaissement: function(params) {
			return $http.post('/api/intervention/saveDecaissement', params);
		},
		comptaDemande: function() {
			return $http.post('/api/intervention/comptaDemande');
		},
		switchTest: function(params) {
			return $http.post('/api/intervention/switchTest', params);
		},
		getDatasOnFactureAndClientNames: function() {
			return $http.get('/api/intervention/getDatasOnFactureAndClientNames');
		},
		getDataOneInter: function(params) {
			return $http.post('/api/intervention/getDataOneInter', params);
		},
		demandeSav: function(params) {
			return $http.post('/api/intervention/demandeSav', params);
		},
		remiseCom: function(params) {
			return $http.post('/api/intervention/remiseCom', params);
		},
		lettreDesist: function(params) {
			return $http.post('/api/intervention/lettreDesist', params);
		},
		lettreOppo: function(params) {
			return $http.post('/api/intervention/lettreOppo', params);
		},
		protAcc: function(params) {
			return $http.post('/api/intervention/protAcc', params);
		},
		injTribCom: function(params) {
			return $http.post('/api/intervention/injTribCom', params);
		},
		injTribProx: function(params) {
			return $http.post('/api/intervention/injTribProx', params);
		},
		demeure: function(params) {
			return $http.post('/api/intervention/demeure', params);
		},
		envRec: function(params) {
			return $http.post('/api/intervention/envRec', params);
		},
		priseEnCharge: function(params) {
			return $http.post('/api/intervention/priseEnCharge', params);
		},
		resetRec: function(params) {
			return $http.post('/api/intervention/resetRec', params);
		},
		saveRec: function(params) {
			return $http.post('/api/intervention/saveRec', params);
		},
		createLitige: function(params) {
			return $http.post('/api/intervention/createLitige', params);
		},
		getRaisonData: function() {
			return $http.get('/api/intervention/getRaisonData');
		},
		getFournisseurData: function() {
			return $http.get('/api/intervention/getFournisseurData');
		},
		verifInter: function(params) {
			return $http.post('/api/intervention/verifInter', params);
		},
		updateVerifInter: function(params) {
			return $http.post('/api/intervention/updateVerifInter', params);
		},
		updateRefusInter: function(params) {
			return $http.post('/api/intervention/updateRefusInter', params);
		},
		sendRefus: function(params) {
			return $http.post('/api/intervention/sendRefus', params);
		},
		listInter: function(params) {
			return $http.post('/api/intervention/listInter', params);
		},
		mailAverif: function() {
			return $http.post('/api/intervention/mailAverif');
		},
		flushTrue: function() {
			return $http.post('/api/intervention/flushTrue');
		},
		transSav: function(params) {
			return $http.post('/api/intervention/transSav', params);
		},
		reloadCache: function() {
			return $http.post('/api/intervention/reloadCache');
		},
	    litigeInjonc: function(id){
		return $http.post('api/intervention/' + id + '/litigeInjonc')
	    },
	    printQ: function(id, check){
		return $http.post('api/intervention/' + id + '/printQ', check)
	    },
	    relanceAuto : function(){
			return $http.post('api/intervention/relanceAuto');
	    },
	    relanceAll : function(){
			return $http.post('api/intervention/relanceAll');
	    },
	    getLitiges : function(){
		return $http.post('api/intervention/getLitiges');
	    },
	    CountRecu: function (params){
		return $http.post('/api/intervention/CountRecu', params);
	    },
	    ComptaStats: function (params){
		return $http.post('/api/intervention/ComptaStats', params);
	    },
	    CountFacture: function (params){
		return $http.post('/api/intervention/CountFacture', params);
	    },
	    canGetFacturier: function (params){
		return $http.post('/api/intervention/canGetFacturier', params);
	    },
	    getSeuilMaxArtisan: function(params){
		return $http.post('/api/intervention/getSeuilMaxArtisan', params);
	    },
	    getSeuilRuptArtisan: function(params) {
		return $http.post('/api/intervention/getSeuilRuptArtisan', params);
	    },
	    checklimit: function(params){
		return $http.post('/api/intervention/checklimit', params);
	    },
	    validatefourn: function(params) {
		return $http.post('/api/intervention/validatefourn', params);
	    },
	    ptStats: function(options) {
		return $http.get('/api/intervention/ptStats', {
		    params: options
		});
	    },	    
	    dashboardStats: function(options) {
		return $http.get('/api/intervention/dashboardStats', {
		    params: options
		});
	    },
	    GetDayWeek: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/GetDayWeek",
		    params: options
		}); 
	    },
	    GetStatUser: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/GetStatUser",
		    params: options
		});
	    },
	    getHourlyStat: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/getHourlyStat",
		    params: options
		});
	    },
	    saveGrandsComptes: function () {
		return $http.post('/api/intervention/saveGrandsComptes');
	    },
	    getTelMatch: function(text) {
		return $http.post('/api/intervention/telMatches', text);
	    },
	    saveTmp: function(data) {
		return $http.post('/api/intervention/temporarySaving', data);
	    },
	    getTmp: function(id) {
		return $http.get('/api/intervention/temporarySaving?id=' + id);
	    },
	    demarcher: function(id) {
		return $http({
		    method: 'POST',
		    url: '/api/intervention/' + id + '/demarcher'
		})
	    },
	    favoris: function(params) {
		return $http.post('/api/intervention/favoris', params);
	    },
	    priseEnCharge: function(params) {
		return $http.post('/api/intervention/priseEnCharge', params);
	    },
	    reactivation: function(id) {
		return $http.post('api/intervention/' + id + '/reactivation')
	    },
	    list: function() {
		return $http.get('api/intervention/getCacheList')
	    },
	    get: function(id, options) {
		return $http({
		    method: 'GET',
		    cache: false,
		    url: '/api/intervention/' + id,
		    params: options || {}
		}).success(function(result) {
		    return result;
		});
	    },
	    getCB: function(id) {
		return $http.get("/api/intervention/" + id + "/CB");
	    },
	    save: function(params) {
		if (!params.id) {
		    return $http.post("/api/intervention", params)
		} else {
		    return $http.post("/api/intervention/" + params.id, params)

		}
	    },
	    getFiles: function(id) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/" + id + "/getFiles"
		});
	    },
	    verification: function(id, options) {
		return $http.post("/api/intervention/" + id + "/verification", options);
	    },
	    annulation: function(id, options) {
		return $http.post("/api/intervention/" + id + "/annulation", options);
	    },
	    envoi: function(id, options) {
		return $http.post("/api/intervention/" + id + "/envoi", options);
	    },
	    sendFacture: function(id, options) {
		return $http.post("/api/intervention/" + id + "/sendFacture", options);
	    },
	    sendFactureAcquitte: function(id, options) {
		return $http.post("/api/intervention/" + id + "/sendFactureAcquitte", options);
	    },
	    statsBen: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/statsBen",
		    params: options
		});
	    },
	    gstats: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/gstats",
		    params: options
		});
	    },
	    statsBenYear: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/statsBenYearly",
		    params: options
		});
	    },
	    commissions: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/commissions",
		    params: options
		});
	    },
	    scan: function(id) {
		return $http.post("/api/intervention/" + id + "/scan");
	    }
	},
	prospect: {
	    checkDuplicates: function (elems) {
		return $http.get('/api/prospect/checkDuplicates', {
		    params: {
			q: JSON.stringify({
			    list: elems
			})
		    }
		})
	    },
	    geoloc: function(options) {
		return $http.post('/api/prospect/geoloc');
	    },
	    geolocNotBDD: function(options) {
		return $http.post('/api/prospect/geolocNotBDD', {
		    params: {
			q: JSON.stringify({
			    list: options
			})
		    }
		});
	    },
	    Merge: function(options) {
		return $http.post('/api/prospect/Merge');
	    },
	    CP: function(options) {
		return $http.post('/api/prospect/CP');
	    },
	    getNearest: function(address, categorie) {
		return $http({
		    method: 'GET',
		    url: "/api/prospect/rank",
		    cache: false,
		    params: {
			categorie: categorie,
			lat: address.lt,
			lng: address.lg,
			limit: 150,
			maxDistance: 30
		    }
		});
	    },
	    save: function(params) {
		if (!params.id) {
		    return $http.post("/api/prospect", params)
		} else {
		    return $http.post("/api/prospect/" + params.id, params)
		}
	    },
	    get: function(id, options) {
		return $http({
		    method: 'GET',
		    cache: false,
		    url: '/api/prospect/' + id,
		    params: options || {}
		}).success(function(result) {
		    return result;
		});
	    },
	    comment: function(id, text) {
		return $http.post('/api/prospect/' + id + '/comment', {
		    text: text
		})
	    },
	    remove: function(id) {
		return $http.post('api/prospect/' + id + '/removePro')
	    },
	    callLog: function (id) {
		return $http.post('/api/prospect/' + id + '/callLog')
            },
	    checkCall: function(id_artisan, id, text, state) {
		return $http.post('/api/prospect/' + id_artisan + '/checkCall', {
                    text: text,
                    sst: id,
                    state: state
		})
            },
	    callHistory: function(id) {
		return $http.get('/api/prospect/' + id + '/callHistory');
            },
	    getStats: function(id_sst) {
		return $http({
		    method: 'GET',
		    url: "/api/prospect/" + id_sst + "/stats"
		});
	    },
	},
	artisan: {
		changeNumber: function() {
			return $http.get('/api/artisan/changeNumber');
		},
		checkExist: function (params) {
			return $http.post('/api/artisan/checkExist',params);
	    },
	    sendIns: function (id, options) {
		return $http.post('/api/artisan/' + id + '/sendIns', {
		    params: options
		});
	    },
	    CheckFacturier: function (id,params) {
		return $http.post('/api/artisan/'+ id +'/CheckFacturier',params);
	    },
	    validateArt: function () {
		return $http.get('/api/validateArt');
	    },
	    addIBAN: function (data) {
		return $http.post('/api/artisan/addIBAN', data);
	    },
	    pdfcom: function(data) {
		return $http.get('/api/artisan/pdfcom', {
		    params: data
		});
	    },
	    starList: function() {
		return $http.get('/api/artisan/starList');
	    },
	    ptStats: function(options) {
		return $http.get('/api/artisan/ptStats', {
		    params: options
		});
	    },	    
	    exist: function(email, num) {
		return $http({
		    method: 'GET',
		    cache: false,
		    url: '/api/artisan/exist',
		    params: {
			email: email,
			num: num
		    }
		});
	    },
	    tableauCom: function() {
		return $http.get('/api/artisan/getStep');
	    },
	    filesCom: function(date) {
		return $http.get('/api/artisan/getComissionFiles');
	    },
	    manage: function(id) {
		return $http.post('/api/artisan/' + id + '/manage')
	    },
	    activite: function(id, text) {
		return $http.post('/api/artisan/' + id + '/activite', {
		    text: text
		})
	    },
	    comment: function(id, text) {
		return $http.post('/api/artisan/' + id + '/comment', {
		    text: text
		})
	    },
	    absence: function(id, absence) {
		return $http.post('/api/artisan/' + id + '/absence', absence)
	    },
	    needFacturier: function(id) {
		return $http.post('/api/artisan/' + id + '/needFacturier')
	    },
	    sendFacturier: function(id, facturier, deviseur, tshirt) {
		return $http.post('/api/artisan/' + id + '/sendFacturier', {
		    facturier: facturier,
		    deviseur: deviseur,
			tshirt: tshirt,
		});
	    },
	    saveTmp: function(data) {
		return $http.post('/api/artisan/temporarySaving', data);
	    },
            callLog: function (id) {
		return $http.post('/api/artisan/' + id + '/callLog')
            },
            callHistory: function(id) {
		return $http.get('/api/artisan/' + id + '/callHistory');
            },
	    ActiviteHistory: function(id) {
		return $http.get('/api/artisan/' + id + '/ActiviteHistory');
	    },
	    fullHistory: function(id) {
		return $http.get('/api/artisan/' + id + '/fullHistory');
	    },
        checkCall: function(id_artisan, id, text, state) {
		return $http.post('/api/artisan/' + id_artisan + '/checkCall', {
                    text: text,
                    sst: id,
                    state: state
		})
        },
	    getTmp: function(id) {
		return $http.get('/api/artisan/temporarySaving?id=' + id);
	    },
	    getCompteTiers: function(id_sst) {
		return $http.get(['/api/artisan', id_sst, 'compteTiers'].join('/'));
	    },
	    envoiContrat: function(id, options) {
		return $http.post("/api/artisan/" + id + '/sendContrat', options)
	    },
	    upload: function(file, name, id) {
		return Upload.upload({
		    url: '/api/artisan/' + id + "/upload",
		    fields: {
			name: name,
			id: id
		    },
		    file: file
		})
	    },
	    getFiles: function(id) {
		return $http({
		    method: 'GET',
		    url: "/api/artisan/" + id + "/getFiles"
		});
	    },
	    extendedStats: function(id) {
		return $http.get('/api/artisan/' + id + "/extendedStats")
	    },
	    statsMonths: function(id) {
		return $http.get('/api/artisan/' + id + "/statsMonths")
	    },
	    save: function(params) {
		if (!params.id) {
		    return $http.post("/api/artisan", params)
		} else {
		    return $http.post("/api/artisan/" + params.id, params)

		}
	    },
	    list: function(options) {
		return $http.get('api/artisan/getCacheList')
	    },
	    lastInters: function(id) {
		return $http({
		    method: 'GET',
		    url: '/api/artisan/' + id + '/lastInters',
		})
	    },
	    get: function(id, options) {
		return $http({
		    method: 'GET',
		    cache: false,
		    url: '/api/artisan/' + id,
		    params: options || {}
		}).success(function(result) {
		    return result;
		});
	    },
	    getNearest: function(address, categorie) {
		return $http({
		    method: 'GET',
		    url: "/api/artisan/rank",
		    cache: false,
		    params: {
			categorie: categorie,
			lat: address.lt,
			lng: address.lg,
			limit: 150,
			maxDistance: 100
		    }
		});
	    },
	    datefacturier: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/artisan/datefacturier",
		    params: options
		});
	    },
	    getStats: function(id_sst) {
		return $http({
		    method: 'GET',
		    url: "/api/artisan/" + id_sst + "/stats"
		});
	    },
	},
	tasklist: {
	    get: function(date, login) {
		return $http.get('/api/tasklist/' + date + '/' + login);
	    },
	    check: function(listid, taskid) {
		return $http.post('/api/tasklist/' + listid + '/check/' + taskid);
	    },
	    update: function(task) {
		return $http.post('/api/tasklist/', {
		    task: task
		});
	    }
	},
	task: {
	    add: function(params) {
		return $http.post('/api/task/add', params)
	    },
	    check: function(id) {
		return $http.post('/api/task/' + id + '/check')
	    },
	    listRelevant: function(options) {
		return $http.get('/api/task/relevant', {
		    params: options
		});
	    }
	},
	signalement: {
		transfo: function(params) {
		return $http.post('/api/signalement/transfo', params)
		},
		resolve: function(params) {
		return $http.post('/api/signalement/resolve', params)
		},
	    check: function(id, text) {
		return $http.post('/api/signalement/' + id + '/check', {
		    text: text
		})
	    },
	    add: function(params) {
		return $http.post('/api/signalement/add', params)
	    },
	    list: function(params) {
		return $http.get('/api/signalement/list', {
		    params: params
		})
	    },
	    stats: function() {
		return $http.get('/api/signalement/stats')
	    }
	},
	file: {
	    uploadScans: function(file, options) {
		return Upload.upload({
		    url: '/api/document/uploadScans',
		    fields: options,
		    file: file
		})
	    },
	    upload: function(file, options) {
		return Upload.upload({
		    url: '/api/document/upload',
		    fields: options,
		    file: file
		})
	    },
	    scan: function(options) {
		return $http.post('/api/document/scan', options);
	    }
	},
	history: {
	    add: function(params) {
		return $http.post('/api/history/add', params);
	    },
	    list: function() {
		return $http.get('/api/history/listAll', {})
	    },
	},
	call: {
	    get: function(origin, link) {
		return $http({
		    method: 'GET',
		    url: '/api/calls/get',
		    params: {
			q: JSON.stringify({
			    link: link
			})
		    }
		})
	    },
	    save: function(params) {
		return $http.post('/api/calls', params);
	    },
	},
	sms: {
	    get: function(origin, link) {
		return $http({
		    method: 'GET',
		    url: '/api/sms/get',
		    params: {
			q: JSON.stringify({
			    link: link,
			    origin: origin
			})
		    }
		})
	    },
	    send: function(params) {
		return $http.post("/api/sms/send", params)
	    },

	},
	getDistance: function(origin, destination) {
	    return $http({
		method: 'GET',
		cache: true,
		url: '/api/mapGetDistance',
		params: {
		    origin: origin,
		    destination: destination
		}
	    })
	},
	request: function(options) {
	    return $http({
		method: options.method || 'GET',
		url: '/api/' + options.fn,
		params: options.data
	    });
	},
	searchPhone: function(tel) {
	    return $http({
		method: 'GET',
		url: "api/arcep/" + tel + "/search"
	    });
	},
	getUser: function() {
	    return $http({
		method: 'GET',
		cache: true,
		url: "/api/whoAmI"
	    });
	},
	searchText: function(text, options) {
	    return $http({
		method: 'GET',
		params: options,
		url: ['api', 'search', text].join('/')
	    })
	},
	bigSearch: function(text, options) {
	    return $http({
		method: 'GET',
		params: options,
		url: ['api', 'bigSearch', text].join('/')
	    })
	}
    }
}]);

angular.module('edison')
    .factory('Artisan', ["$window", "$rootScope", "user", "$location", "LxNotificationService", "LxProgressService", "dialog", "edisonAPI", "textTemplate", function($window, $rootScope, user, $location, LxNotificationService, LxProgressService, dialog, edisonAPI, textTemplate) {
        "use strict";
        var Artisan = function(data) {
            if (!(this instanceof Artisan)) {
                return new Artisan(data);
            }
            for (var k in data) {
                this[k] = data[k];
            }
        }

        var appelLocal = function(tel) {
            if (tel) {
                $window.open('callto:' + tel, '_self', false);
            }
        }

        Artisan.prototype.callTel1 = function() {
            appelLocal(this.telephone.tel1)
            edisonAPI.artisan.callLog(this.id).then(function (resp) {
                var message = _.template("L'appel a été ajouté à l'historique.")(resp.data)
                LxNotificationService.success(message);
            }, function (error) {
                LxNotificationService.error(error.data);
            });
        }
        Artisan.prototype.callTel2 = function() {
            appelLocal(this.telephone.tel2)
            edisonAPI.artisan.callLog(this.id).then(function (resp) {
                var message = _.template("L'appel a été ajouté à l'historique.")(resp.data)
                LxNotificationService.success(message);
            }, function (error) {
                LxNotificationService.error(error.data);
            });
        }

        Artisan.prototype.typeOf = function() {
            return 'Artisan';
        }

        Artisan.prototype.ouvrirFiche = function() {
            $location.url("/artisan/" + this.id);
        }
        Artisan.prototype.ouvrirRecap = function() {
            $location.url("/artisan/" + this.id + '/recap');
        }
        Artisan.prototype.facturierDeviseur = function() {
            var _this = this;
            dialog.facturierDeviseur(this, function(facturier, deviseur, tshirt) {
                edisonAPI.artisan.sendFacturier(_this.id, facturier, deviseur, tshirt);
            })
        }

        Artisan.prototype.tutelleIn = function(cb) {
            this.tutelle = true;
            Artisan(this).save();
        }
        Artisan.prototype.tutelleOut = function(cb) {
            this.tutelle = false;
            Artisan(this).save();
        }

        Artisan.prototype.deArchiver = function() {
	    var _this = this;
            _this.status = "ACT";
            Artisan(_this).save();
        }
        Artisan.prototype.archiver = function(cb) {
	    var _this = this;
	  //  this.status = "ARC";
          //  Artisan(this).save();
	    dialog.archiver(_this, function (ans, causeArchive){
		if (ans)
		{
		    if (causeArchive)
		    {
			var causeArchive = "Cause Archive : " + causeArchive;
			edisonAPI.artisan.activite(_this._id, causeArchive)
			var message = "L'artisan a été archivé"
			LxNotificationService.success(message);
			_this.status = "ARC";
			Artisan(_this).save();
		    }
		}
            })
	}
	

        Artisan.prototype.call = function(cb) {
            var _this = this;
            var now = Date.now();
            $window.open('callto:' + _this.telephone.tel1, '_self', false)
            edisonAPI.artisan.callLog(_this.id).then(function (resp) {
                var message = _.template("L'appel a été ajouté à l'historique.")(resp.data)
                LxNotificationService.success(message);
            }, function (error) {
                LxNotificationService.error(error.data);
            });
        };

        Artisan.prototype.refuseFacturier = function() {
            this.demandeFacturier.status = 'NO';
            Artisan(this).save();
        }

        Artisan.prototype.needFacturier = function() {
            if (this.demandeFacturier && moment(this.demandeFacturier.date).isAfter(moment().add(-10, 'days'))) {
                if (this.demandeFacturier.status === 'PENDING') {
                    LxNotificationService.error(moment(this.demandeFacturier.date).format("[Une demande à deja été éffectué le ]LLL"));
                }
                if (this.demandeFacturier.status === 'OK') {
                    LxNotificationService.error("Un facturier à deja été envoyé dans les 10 derniers jours");
                }
                if (this.demandeFacturier.status === 'NO') {
                    LxNotificationService.error("L'envoi d'un facturier à deja été refusé dans les 10 derniers jours");
                }
                return 0;
            }
            edisonAPI.artisan.needFacturier(this.id).then(function(resp) {
                LxNotificationService.success("Une notification a été envoyer au service partenariat");
            })
        }

        Artisan.prototype.save = function(cb) {
            var _this = this;
            edisonAPI.artisan.save(_this)
                .then(function(resp) {
                    LxNotificationService.success("Les données ont à été enregistré");
                    if (typeof cb === 'function')
                        cb(null, resp.data)
                }).catch(function(error) {
                    LxNotificationService.error(error.data);
                    if (typeof cb === 'function')
                        cb(error.data)
                });
        };


        Artisan.prototype.manager = function(cb) {
            var _this = this;
            _this.login.management = user.login;
            edisonAPI.artisan.save(_this)
                .then(function(resp) {
                    LxNotificationService.success("Vous manager désormais " + _this.nomSociete);
                    if (typeof cb === 'function')
                        cb(null, resp.data)
                }).catch(function(error) {
                    LxNotificationService.error(error.data);
                    if (typeof cb === 'function')
                        cb(error.data)
                });
        };

	Artisan.prototype.comment = function() {
            var _this = this;
            dialog.applyComment({comments: _this.comments}, function(comment) {
		edisonAPI.artisan.comment(_this.id, comment)
            });	    
        }

        Artisan.prototype.upload = function(file, name, cb) {
            var _this = this;
            if (file) {
                LxProgressService.circular.show('#5fa2db', '#fileUploadProgress');
                edisonAPI.artisan.upload(file, name, _this.id)
                    .success(function(resp) {
                        _this.document = resp.document;
                        LxProgressService.circular.hide();
                        if (typeof cb === 'function')
                            cb(null, resp);
                    }).catch(function(err) {
                        LxProgressService.circular.hide();
                        if (typeof cb === 'function')
                            cb(err);
                    })
            }
        }

        Artisan.prototype.envoiContrat = function(options, cb) {
            var _this = this;
            options = options || {};
            dialog.sendContrat({
                data: _this,
                signe: options.signe,
                text: _.template(textTemplate.mail.artisan.envoiContrat())(_this),
            }, function(options) {
                LxProgressService.circular.show('#5fa2db', '#globalProgress');
                edisonAPI.artisan.envoiContrat(_this.id, {
                    text: options.text,
                    signe: options.signe
                }).success(function(resp) {
                    LxProgressService.circular.hide()
                    if (typeof cb === 'function')
                        cb(null, resp);
                });
            });
        }
        Artisan.prototype.relanceDocuments = function(cb) {
            var _this = this;
            _this.datePlain = moment(_this.date.ajout).format('ll')
            dialog.sendContrat({
                data: _this,
                text: _.template(textTemplate.mail.artisan.relanceDocuments())(_this),
            }, function(options) {
                LxProgressService.circular.show('#5fa2db', '#globalProgress');
                edisonAPI.artisan.envoiContrat(_this.id, {
                    text: options.text,
                    signe: options.signe,
                    rappel: true
                }).success(function(resp) {
                    LxProgressService.circular.hide()
                    if (typeof cb === 'function')
                        cb(null, resp);
                });
            });
        }

        Artisan.prototype.ouvrirFiche = function() {
            $location.url("/artisan/" + this.id);
        }
        return Artisan;
    }]);

angular.module('edison').factory('ContextMenu', ["$rootScope", "$location", "edisonAPI", "user", "$window", "$timeout", "dialog", "Devis", "Intervention", "Artisan", "$routeParams", "contextMenuData", function($rootScope, $location, edisonAPI, user, $window, $timeout, dialog, Devis, Intervention, Artisan, $routeParams, contextMenuData) {
    "use strict";

    var ContextMenu = function(model) {
        var _this = this;
        this.model = model
        this.list = contextMenuData[model];
        $rootScope.$on('closeContextMenu', function() {
            return _this.active && _this.close();
        })
        this.style = {
            left: 0,
            top: 0,
            display: "none"
        }
    }

    ContextMenu.prototype.openSub = function(delay) {
        var _this = this
        this.openSubTimeout = $timeout(function() {
            _this.mouseOverCM = true
        }, delay || 0)

    }

    ContextMenu.prototype.closeSub = function() {
        if (this.openSubTimeout) {
            $timeout.cancel(this.openSubTimeout);
        }
        this.mouseOverCM = false
    }

    ContextMenu.prototype.getData = function() {
        return this.data;
    }

    ContextMenu.prototype.setData = function(data) {
        this.data = data;
    }

    ContextMenu.prototype.setPosition = function(x, y) {
        this.style.left = (x - $('#main-menu-inner').width());
        if ((y - $('.dropdown-menu__content').height() - 200) < 0)
            this.style.top = (y - ($('.dropdown-menu__content').height() / 2));
        else if (((y + $('.dropdown-menu__content').height()) > $('.page-header').height()))
            this.style.top = (y - $('.dropdown-menu__content').height() - 48);
        else
            this.style.top = (y - 48);
    }

    ContextMenu.prototype.active = false;

    ContextMenu.prototype.open = function() {
        var _this = this;
        this.closeSub()
        this.list.forEach(function(e) {
            if (e.subs) {
                _.each(e.subs, function(sub) {
                    sub.hidden = sub.hide && sub.hide(_this.data);
                })
            } else {
                e.hidden = e.hide && e.hide(_this.data, user, $routeParams.fltr);
            }
        });
        this.style.display = "block";
        this.active = true;
        return this
    }

    ContextMenu.prototype.onClose = function(callback) {
        this.onCloseCallback = callback;
    }

    ContextMenu.prototype.close = function() {
        this.style.display = "none";
        this.active = false;
        if (this.onCloseCallback) {
           this.onCloseCallback()
           this.onCloseCallback = null;
        }

    }

    ContextMenu.prototype.modelObject = {
        intervention: Intervention,
        devis: Devis,
        artisan: Artisan
    }

    ContextMenu.prototype.tooltip = function(link) {
        return _.get(this.data, link.binding, '');
    }

    ContextMenu.prototype.click = function(link) {
        if (typeof link.action === 'function') {
            return link.action(this.getData())
        } else if (typeof link.action === 'string') {
            return this.modelObject[this.model]()[link.action].bind(this.data)();
        } else {
            console.error("error here")
        }
    }

    return ContextMenu
}]);

angular.module('edison').factory('DataProvider', ["$timeout", "edisonAPI", "socket", "$rootScope", "config", function($timeout, edisonAPI, socket, $rootScope, config) {
    "use strict";
    var DataProvider = function(model, hashModel) {
        var _this = this;
        this.model = model;
        this.hashModel = hashModel || 't';
        this.rand = new Date
        socket.on(_this.socketListChange(), function(change) {
            if (_this.appliedList.indexOf(change.ts) === -1) {
                _this.appliedList.push(change.ts)
                _this.updateData(change.data);
            }
        });
    }

    DataProvider.prototype.socketListChange = function() {
        var _this = this;
        return _this.model.toUpperCase() + '_CACHE_LIST_CHANGE';
    }
    DataProvider.prototype.appliedList = [];

    DataProvider.prototype.data = {}

    DataProvider.prototype.trie = {}

    DataProvider.prototype.setData = function(data) {
        var _this = this;
        this.data[this.model] = data;
        _this.trie[this.model] = {};
        _.each(data, function(e) {
            _this.getTrie()[e.id] = e;
        })
    };

    DataProvider.prototype.init = function(cb) {
        var _this = this;
        if (_this.getData())
            return cb(_this.getData());
        edisonAPI[_this.model].list({
            cache: true
        }).success(function(resp) {
            _this.setData(resp);
            return cb(null, resp);
        })
    }

    DataProvider.prototype.rowFilterFactory = function(filter, hash) {
        var _this = this;
        if (!filter && hash) {
            return function onlyLogin(inter) {
                return inter[_this.hashModel] === hash;
            }
        }
        if (filter && hash) {
            return function loginAndFilter(inter) {
                return inter.f && inter.f[filter.short_name] === 1 && inter[_this.hashModel] === hash;
            }
        }
        if (filter && !hash) {
            return function onlyFilter(inter) {
                return inter.f && inter.f[filter.short_name] === 1;
            }
        }
    }

    DataProvider.prototype.applyFilter = function(filter, hash, customFilter) {
        this.filteredData = this.getData();
        if (this.getData() && (filter || hash || customFilter)) {
            this.filteredData = _.filter(this.getData(), customFilter || this.rowFilterFactory(filter, hash));
        }
    }

    DataProvider.prototype.flash = function(row) {
        row.flash = true;
        $timeout(function() {
            row.flash = false;
        }, 1000)
    }

    DataProvider.prototype.updateData = function(newRows) {
        var _this = this;
        if (this.getData()) {
            _.each(newRows, function(e) {
                var tmp = _this.getTrie()[e.id];
                if (tmp) {
                    _.merge(_this.getTrie()[e.id], e);
                } else {
                    _this.getData().unshift(e);
                    _this.getTrie()[e.id] = e;
                }
            })

            $rootScope.$broadcast(_this.socketListChange());
        }

    }

    DataProvider.prototype.getData = function() {
        return this.data[this.model];
    }
    DataProvider.prototype.getTrie = function() {
        return this.trie[this.model];
    }

    DataProvider.prototype.isInit = function() {
        return this.model && this.data && this.data[this.model];
    }
    return DataProvider;

}]);

angular.module('edison').factory('DateSelect', function() {
    "use strict";
    var DateSelect = function(dateStart, dateEnd) {

        var _this = this;
        var d = new Date();
        _this.start = {
            m: !dateStart ? 9 : dateStart.getMonth() + 1,
            y: !dateStart ? 2013 : dateStart.getFullYear()
        }
        _this.current = {
            m: d.getMonth() + 1,
            y: d.getFullYear()
        }

        var frenchMonths = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
        _this._list = [];
        _.times(_this.current.y - _this.start.y + 1, function(yr) {
            _.times(12, function(mth) {
                _this._list.push({
                    m: mth + 1,
                    y: _this.start.y + yr,
                    t: frenchMonths[mth] + ' ' + (_this.start.y + yr),
                    o: (_this.start.y + yr) + (mth + 1) * 0.01
                })
            })
        })
        _this._list.splice(_this.current.m - 12)
    }
    DateSelect.prototype.list = function() {
        return this._list;
    }
    return DateSelect;
});

angular.module('edison')
    .factory('Devis', ["openPost", "$window", "$rootScope", "$location", "LxNotificationService", "LxProgressService", "dialog", "edisonAPI", "textTemplate", function(openPost, $window, $rootScope, $location, LxNotificationService, LxProgressService, dialog, edisonAPI, textTemplate) {
        "use strict";
        var Devis = function(data) {
            if (!(this instanceof Devis)) {
                return new Devis(data);
            }
            for (var k in data) {
                this[k] = data[k];
            }
        }

        var appelLocal = function(tel) {
            if (tel) {
                $window.open('callto:' + tel, '_self', false);
            }
        }

        Devis.prototype.callTel1 = function() {
            appelLocal(this.client.telephone.tel1)
        }
        Devis.prototype.callTel2 = function() {
            appelLocal(this.client.telephone.tel2)
        }
        Devis.prototype.callTel3 = function() {
            appelLocal(this.client.telephone.tel3)
        }

        Devis.prototype.typeOf = function() {
            return 'Devis';
        }
        Devis.prototype.save = function(cb) {
            var _this = this;
            edisonAPI.devis.save(_this)
                .then(function(resp) {
                    var validationMessage = _.template("Les données du devis {{id}} ont à été enregistré")(resp.data);
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function')
                        cb(null, resp.data)
                }, function(error) {
                    LxNotificationService.error(error.data);
                    if (typeof cb === 'function')
                        cb("une erreur est " + error.data)
                }).catch(function(e) {
                    console.log('ERRRR:>', e)
                })
        };

        Devis.prototype.devisPreview = function() {
            if (!this.client.nom || !this.client.address.r || !this.client.address.v ||  !this.client.address.cp || !this.client.address.n) {
                return LxNotificationService.error('Les Coordonnées du devis sont incompletes');
            }
            if (!this.produits || !this.produits.length) {
                return LxNotificationService.error('Veuillez renseigner au moins 1 produits');
            }
            openPost('/api/intervention/devisPreview', {
                data: JSON.stringify(this),
                html: true
            })
        }

        Devis.prototype.sendDevis = function(cb) {
            var _this = this;
            if (!/\S+@\S+\.\S+/.test(_this.client.email)) {
                LxNotificationService.error("L'addresse email est invalide");
                return cb("err");
            }
            var preview = Devis(this).devisPreview.bind(this)
            dialog.getTextDevis(preview, {
                text: textTemplate.mail.devis.envoi.bind(_this)($rootScope.user),
                width: "900px",
                height: "700px"
            }, function(text) {
                console.log("------>Devis text func");
                LxProgressService.circular.show('#5fa2db', '#globalProgress');
                console.log("------>Devis text func 2");
                edisonAPI.devis.envoi(_this.id, { text: text }).success(function(resp) {
                    console.log("------>Devis envoi 1");
                    LxProgressService.circular.hide()
                    var validationMessage = _.template("le devis {{id}} à été envoyé")(_this);
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function') {
                        cb(null, resp);
                    }
                }).catch(function(err) {
                    console.log("------>Devis catch err");
                    LxProgressService.circular.hide()
                    var validationMessage = _.template("L'envoi du devis {{id}} à échoué\n")(_this)
                    if (err && err.data && typeof err.data === 'string')
                        validationMessage += ('\n(' + err.data + ')')
                    LxNotificationService.error(validationMessage);
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        }
        Devis.prototype.annulation = function(cb) {
            var _this = this;
            edisonAPI.devis.annulation(_this.id)
                .then(function(resp) {
                    var validationMessage = _.template("Le devis {{id}} est annulé")(resp.data)
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function')
                        cb(null, resp.data)
                });
        };

        Devis.prototype.ouvrirFiche = function() {
            $location.url("/devis/" + this.id);
        }
        Devis.prototype.transfert = function() {
            $location.url("/intervention?devis=" + this.id);
        }
        return Devis;
    }]);

angular.module('edison').factory('dialog', ["openPost", "$mdDialog", "$rootScope", "edisonAPI", "config", "$window", "$filter", "LxNotificationService", function(openPost, $mdDialog, $rootScope, edisonAPI, config, $window, $filter, LxNotificationService) {
    "use strict";

    return {
	callArtisan: function (data, cb) {
	    $mdDialog.show({
		controller: ["$scope", "$mdDialog", "config", function($scope, $mdDialog, config)
		{
		    $scope.data = data
		    $scope.tel1 = true;
		    $scope.tel2 = false;
		    $scope.unselect = function (nb) {
			if (nb == 1)
			{
			    $scope.tel1 = false;
			    $scope.tel2 = true;
			}
			else
			{
			    $scope.tel1 = true;
			    $scope.tel2 = false;
			}
		    }
		    $scope.answer = function(cancel) {
			$mdDialog.hide();
			if (!cancel) {
			    if ($scope.tel1 == true)
				return cb($scope.data.tel1);
			    else
				return cb($scope.data.tel2);
			}
		    }
		}],
		templateUrl: '/DialogTemplates/callArtisan.html',
	    })
	},

	cancelLate: function (data, cb) {
	    $mdDialog.show({
		controller: ["$scope", "$mdDialog", "config", function($scope, $mdDialog, config)
		{
		    $scope.data = data
		    $scope.answer = function(cancel) {
			$mdDialog.hide();
			if (!cancel) {
			    return cb($scope.data);
			}
		    }
		}],
		templateUrl: '/DialogTemplates/cancelLate.html',
	    });	
	},

	
	changeImage: function(link, cb) {
	    $mdDialog.show({
		controller: ["$scope", "$mdDialog", "config", function($scope, $mdDialog, config)
		{
		    $scope.link = link;
		    $scope.answer = function(cancel) {
			$mdDialog.hide();
			if (!cancel) {
			    return cb($scope.link);
			}
		    }
		}],
		templateUrl: '/DialogTemplates/changeImage.html',
	    });	
	},
	
	applyComment: function (obj, cb) {
	    $mdDialog.show({
		controller: ["$scope", "$mdDialog", "config", function($scope, $mdDialog, config)
		{
		    $scope.history = obj.comments;
		    $scope.answer = function(cancel) {
			$mdDialog.hide();
			if (!cancel) {
			    return cb($scope.comment);
			}
		    }
		}],
		templateUrl: '/DialogTemplates/comment.html',
	    });
	},
	
	prospectCall: function(elems, cb) {
	    $mdDialog.show({
		controller: ["$scope", "$mdDialog", "config", function($scope, $mdDialog, config)
		{
		    $scope.statutPro = elems.statutProspect;
		    $scope.config = config;
		    $scope.answer = function(cancel) {
			$mdDialog.hide();
			if (!cancel) {
			    return cb($scope.statutPro);
			}
		    }
		}],
		templateUrl: '/DialogTemplates/prospectCall.html',
	    });
	},
	
        declareBug: function(tabs, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", "config", function($scope, $mdDialog, config) {
                    $scope.resp = {
                        location: tabs.getCurrentTab().path,
                    }
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel) {
                            return cb(null, $scope.resp)
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/declare-bug.html',
            });
        },
		/* Fonction pour la pop up de la résolution */
		pop_up_showDest_raison : function(signal, typeT, cb)
		{
			$mdDialog.show({
				controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
					$scope.signal = signal;
					$scope.typeT = typeT;
					if (signal.text && typeT == 2)
						$scope.resolve = signal.text;
					if (typeT == 0)
					{
						edisonAPI.users.list().then(function(resp) {
							$scope.showDest = [];
							for (var ind = 0; ind < resp.data.length; ind++)
							{
								for (var index = 0; index < signal.arrayDest.length; index++)
								{
									if (resp.data[ind].email == signal.arrayDest[index])
                                    {
                                        var check = 0;
                                        for (var i = 0; i < $scope.showDest.length; i++)
                                        {
                                            if ($scope.showDest[i] == resp.data[ind].login)
                                                check++;
                                        }
                                        if (check == 0)
                                            $scope.showDest.push(resp.data[ind].login);
                                    }
								}
							}
							$scope.showDest = $filter('orderBy')($scope.showDest, 'login');
						})
					}
					$scope.answer = function(cancel, resolve) {
						$mdDialog.hide();
						if (typeT == 1 && cancel === true && resolve)
						{
							return cb (resolve, cancel);
						}
					}
				}],
				templateUrl: '/DialogTemplates/pop_up_showDest_raison.html',
			});
		},
		/* Fonction pour la pop up des destinataires dans Outils->Signalements */
		pop_up_dest: function(signal, indPl, cb)
		{
			$mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
					edisonAPI.users.list().then(function(resp) {
                        resp.data = $filter('orderBy')(resp.data, 'login');
                        $scope.signal = signal;
                        if (!$scope.signal.dest)
                            $scope.signal.dest = {};
						if (!$scope.signal.dest.special)
							$scope.signal.dest.special = {};
						if (!$scope.signal.dest.service)
							$scope.signal.dest.service = {};
                        if (!$scope.signal.dest.users)
                            $scope.signal.dest.users = resp.data;
                        else
                        {
                            edisonAPI.signal.update({ users: resp.data, signal: $scope.signal }).then(function(updatedSignal) {
                                $scope.signal = updatedSignal.data;
                            })
                        }
		    			$scope.answer = function(cancel) {
							$mdDialog.hide();
							if (cancel === true)
							{
								edisonAPI.signal.saveDest({ nom: $scope.signal.nom, signal: $scope.signal }).then(function(resp) {
                                    return cb (resp, cancel);
                                })
							}
                            else
                                return cb (null, cancel);
						}
					});
		    	}],
                templateUrl: '/DialogTemplates/pop_up_dest.html',
            });

		},
	/* Fonction pour la pop up signalement/commentaires dans une intervention */	
		pop_up_level2_comment: function(nom, cb)
        {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
		    		$scope.nomA = nom
					$scope.answer = function(cancel) {
						$mdDialog.hide();
                     	if (cancel && $scope.commentaire){
						}
							return cb ($scope,cancel);
		    		}
                }],
                templateUrl: '/DialogTemplates/pop_up_level2.html',
            });
        },

    	getPassword: function(page, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", "config", function($scope, $mdDialog, config) {

                    $scope.answer = function(cancel) {
                        if ((page == "new" && $scope.password === 'sacha42') ||
			    (page == "part" && $scope.password === 'yohann') ||
			    (page == "recouv" && $scope.password === 'edison')) {
                            $mdDialog.hide();
                            return cb(null, "OK")
			}
                    }
                }],
                templateUrl: '/DialogTemplates/get-password.html',
            });
        },

        verification: function(inter, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", "config", function DialogController($scope, $mdDialog, config) {
                    $scope.data = inter
                    $scope.config = config;
                    $scope.answer = function(cancel) {
                        $scope.window = $window;
                        $scope.inter = inter;
                        if (!cancel) {
                            if (!$scope.inter.prixFinal) {
                                LxNotificationService.error("Veuillez renseigner un prix final");
                            } else {
                                $mdDialog.hide();
                                cb(inter);
                            }
                        } else {
                            $mdDialog.hide();
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/verification.html',
            });
        },
        recouvrement: function(inter, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", "config", function DialogController($scope, $mdDialog, config) {
                    $scope.data = inter
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel) {
                            cb(inter);
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/recouvrement.html',
            });
        },
        validationReglement: function(inter, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.data = inter

                    Mousetrap.bind(['command+k', 'ctrl+k', 'command+f1', 'ctrl+f1'], function() {
                        $window.open("appurl:", '_self');
                        edisonAPI.intervention.scan(inter.id).then(function() {
                            LxNotificationService.success("Le fichier est enregistré");
                        })
                        return false;
                    });


                    $scope.answer = function(resp) {
                        $scope.data = inter;
                        $mdDialog.hide();
                        if (resp === null) {
                            return cb('nope')
                        }
                        if ($scope.data.compta.reglement.montant !== 0) {
                            $scope.data.compta.reglement.recu = resp;
                        }
                        return cb(null, $scope.data);
                    }
                }],
                templateUrl: '/DialogTemplates/validationReglement.html',
            });
        },
        encaissement: function(inter, cb) {
            $mdDialog.show({
                controller: ["$scope", "$rootScope", "$mdDialog", function DialogController($scope, $rootScope, $mdDialog) {
                    $scope.tva = inter.tva.toString();
                    $scope.dateSaisie = new Date().getTime();
                    $scope.dateEncaissement = new Date().getTime();
                    if (inter.compta.encaissement.length > 0) {
                        $scope.data = inter;
                    } else {
                        $scope.data = inter;
                        $scope.data.compta.encaissement = [{ client: inter.client.nom, agent: $rootScope.displayUser.login, tva: inter.tva.toString() }];
                    }
                    $scope.add = function() {
                        $scope.data.compta.encaissement.unshift({ client: inter.client.nom, agent: $rootScope.displayUser.login, tva: inter.tva.toString() });
                    }
                    $scope.remove = function(index) {
                        $scope.data.compta.encaissement.splice(index, 1);
                    }
                    $scope.answer = function(resp) {
                        $mdDialog.hide();
                        if (resp === true) {
                            for (var ind = 0; ind < $scope.data.compta.encaissement.length; ind++) {
                                $scope.data.compta.encaissement[ind].dateEncaissement = new Date($scope.data.compta.encaissement[ind].dateEncaissement);
                            }
                            return cb(null, $scope.data);
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/encaissement.html',
            });
        },
        decaissement: function(inter, cb) {
            $mdDialog.show({
                controller: ["$scope", "$rootScope", "$mdDialog", function DialogController($scope, $rootScope, $mdDialog) {
                    var count = 0;
                    $scope.dateSaisie = new Date();
                    $scope.tva = inter.tva.toString();
                    if (inter.compta.decaissement.length > 0) {
                        $scope.data = inter;
                    } else {
                        $scope.data = inter;
                        $scope.data.compta.decaissement = [{ client: inter.client.nom, agent: $rootScope.displayUser.login, tva: inter.tva.toString() }];
                    }
                    if (inter.compta.reglement.avoir.effectue === true) {
                        for (var ind = 0; ind < inter.compta.decaissement.length; ind++) {
                            if (inter.compta.decaissement[ind].motif == 'avoir') {
                                count++;
                            }
                        }
                        if (count == 0) {
                            $scope.data.compta.decaissement.unshift({ num: inter.compta.reglement.avoir.numeroCheque, dateSaisie: inter.compta.reglement.avoir.date,
                            montant: inter.compta.reglement.avoir.montant, tva: inter.tva.toString(), motif: 'avoir' });
                        }
                    }
                    $scope.add = function() {
                        $scope.data.compta.decaissement.unshift({ client: inter.client.nom, agent: $rootScope.displayUser.login, tva: inter.tva.toString() });
                    }
                    $scope.remove = function(index) {
                        $scope.data.compta.decaissement.splice(index, 1);
                    }
                    $scope.answer = function(resp) {
                        $mdDialog.hide();
                        if (resp === true) {
                            return cb(null, $scope.data);
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/decaissement.html',
            });
        },
        validationPaiement: function(inter, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", "textTemplate", function DialogController($scope, $mdDialog, textTemplate) {
                    $scope.data = inter
                    Mousetrap.bind(['command+k', 'ctrl+k', 'command+f1', 'ctrl+f1'], function() {
                        $window.open("appurl:", '_self');
                        edisonAPI.intervention.scan(inter.id).then(function() {
                            LxNotificationService.success("Le fichier est enregistré");
                        })
                        return false;
                    });
                    $scope.textTemplate = textTemplate;
                    $scope.data.compta.paiement.ready = true;
                    $scope.preview = function() {
                        openPost('/api/intervention/autofacture', {
                            data: JSON.stringify($scope.data),
                            html: true
                        });
                    }
                    $scope.answer = function(resp) {
                        $scope.data = inter;
                        $mdDialog.hide();
                        if (resp === null) {
                            return cb('nope')
                        }
                        return cb(null, $scope.data);
                    }
                }],
                templateUrl: '/DialogTemplates/validationPaiement.html',
            });
        },
        facturierDeviseur: function(artisan, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", "config", function DialogController($scope, $mdDialog, config) {
                    	$scope.sst = artisan
			$scope.facturier = true;
			$scope.deviseur = true;
			$scope.tshirt = true;
			$scope.doublePrint = function(id) {
				window.open('/api/artisan/' + id + '/tshirt');
				window.open('api/artisan/' + id + '/facturier');
			}
			$scope.printTshirt = function(id) {
				window.open('/api/artisan/' + id + '/tshirt');
			}
			$scope.printFact = function(id) {
				window.open('/api/artisan/' + id + '/facturier');
			}
			$scope.answer = function(choice) {
                        	$mdDialog.hide();
                        	if (choice == "facturier")
				{
                            		cb($scope.facturier, $scope.deviseur, $scope.tshirt);
				}
				else if (choice == "tshirt")
				{
					cb($scope.facturier, $scope.deviseur, $scope.tshirt);
				}
				else if (choice == "factTshirt")
				{
					cb($scope.facturier, $scope.deviseur, $scope.tshirt);
				}
                  	}
                }],
                templateUrl: '/DialogTemplates/facturierDeviseur.html',
            });
        },
        envoiFacture: function(inter, text, showAcquitte, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.text = text
                    $scope.date = new Date();
                    $scope.showAcquitte = showAcquitte;
                    $scope.acquitte = showAcquitte;
                    $scope.saveFiles = [];
                    $scope.showName = [];
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel) {
                            cb(null, $scope.text, $scope.acquitte, $scope.date, $scope.saveFiles);
                        } else {
                            cb('nope')
                        }
                    }
                    $scope.joinFile = function(elem) {
                        var $scope = this;
                        $scope.$apply(function () {
                            var name = elem.files[0].name;
                            $scope.showName.push(elem.files[0].name);
                            var fileExt = elem.files[0].name.split('.');
                            if (fileExt[fileExt.length - 1] == "json")
                                var typeFile = 'application/json';
                            else
                                var typeFile = elem.files[0].type;
                            if (fileExt[fileExt.length - 1] == "pdf")
                            {
                                var read = new FileReader(elem.files[0]);
                                read.onload = function(e) {
                                    $scope.saveFiles.push({ ContentType: typeFile, Name: elem.files[0].name, Content: e.target.result.replace('data:application/pdf;base64,', '') });
                                }
                                read.readAsDataURL(elem.files[0], 'ISO-8859-1');
                            }
                            else
                            {
                                var read = new FileReader(elem.files[0]);
                                read.onload = function(e) {
                                    $scope.saveFiles.push({ ContentType: typeFile, Name: elem.files[0].name, Content: e.target.result });
                                }
                                read.readAsText(elem.files[0], 'ISO-8859-1');
                            }
                        });
                        $scope.deletePJ = function(index) {
                            $scope.showName.splice(index, 1);
                            $scope.saveFiles.splice(index, 1);
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/envoiFacture.html',
            });
        },
        recap: function(inters) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", "config", function DialogController($scope, $mdDialog, config) {
                    $scope.inters = inters;
                    $scope.config = config
                    $scope.answer = function() {
                        $mdDialog.hide();
                    }
                }],
                templateUrl: '/DialogTemplates/recapList.html',
            });
        },
        callsList: function(sst) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.sst = sst;
                    $scope.answer = function() {
                        $mdDialog.hide();
                    }
                }],
                templateUrl: '/DialogTemplates/callsList.html',
            });
        },
        smsList: function(sst) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.sst = sst;
                    $scope.answer = function() {
                        $mdDialog.hide();
                    }
                }],
                templateUrl: '/DialogTemplates/smsList.html',
            });
        },
        choiceText: function(options, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.options = options;
                    $scope.answer = function(resp, text) {
                        $mdDialog.hide();
                        return cb(resp, text);
                    }
                }],
                templateUrl: '/DialogTemplates/choiceText.html',
            });
        },
        addProd: function(cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", "$window", function DialogController($scope, $mdDialog, $window) {
                    $scope.pu = 0;
                    $scope.window = $window
                    $scope.quantite = 1;
                    $scope.prod = {
                        quantite: 1,
                        pu: 0,
                        title: "",
                        ref: ""
                    }
                    $scope.$watch('prod', function() {
                        $scope.prod.ref = $scope.prod.ref.toUpperCase();
                        $scope.prod.title = $scope.prod.title.toUpperCase();
                        $scope.prod.desc = $scope.prod.title;
                        $scope.prod.pu = $scope.prod.pu;
                        $scope.prod.quantite = $scope.prod.quantite;
                    }, true)
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel) {
                            return cb($scope.prod);
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/getProd.html',
            });
        },
	archiver: function(artisan, cb) {
	    $mdDialog.show({
		controller: ["$scope", "config", function($scope, config) {
		    $scope.nomA = artisan.nomSociete
		    $scope.causeArchive = config.cause;
		    $scope.answer = function(resp) {
			if (resp && !$scope.causeArchive) {
			    return LxNotificationService.error("Veuillez renseigner une raison pour archiver l'artisan");
			}
			$mdDialog.hide();
			if (resp)
			{
			    edisonAPI.artisan.save(artisan);
			    return cb(true, $scope.causeArchive);
			}
			return cb(false, 'nope');
		    }
		}],
		templateUrl: '/DialogTemplates/archive.html',
	    });
	},
        getCauseAnnulation: function(inter, cb) {
            $mdDialog.show({
                controller: ["$scope", "config", function($scope, config) {
                    $scope.causeAnnulation = config.causeAnnulation;
                    inter.datePlain = moment(inter.date.intervention).format('DD/MM/YYYY')
                    $scope.textSms = _.template("L'intervention {{id}} chez {{client.civilite}} {{client.nom}} à {{client.address.v}} le {{datePlain}} a été annulé. \nMerci de ne pas intervenir. \nEdison Services")(inter)
                    $scope.answer = function(resp) {
                        if (resp && !$scope.ca) {
                            return LxNotificationService.error("Veuillez renseigner une raison d'annulation");
                        }
                        $mdDialog.hide();
                        if (resp)
                            return cb(null, $scope.ca, $scope.reinit, $scope.sendSms, $scope.textSms);
                        return cb('nope');
                    }
                }],
                templateUrl: '/DialogTemplates/causeAnnulation.html',
            });
        },
        sendContrat: function(options, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.options = options;
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel)
                            return cb($scope.options);
                    }
                }],
                templateUrl: '/DialogTemplates/sendContrat.html',
            });
        },
        getText: function(options, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.options = options;
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel)
                            return cb($scope.options.text);
                    }
                }],
                templateUrl: '/DialogTemplates/text.html',
            });
        },
        getTextDevis: function(previewFunction, options, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.options = options;
                    $scope.previewFunction = previewFunction;
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel)
                            return cb($scope.options.text);
                    }
                }],
                templateUrl: '/DialogTemplates/textDevis.html',
            });
        },
        getFileAndText: function(data, text, files, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.Math = Math
                    $scope.xfiles = _.clone(files ||  []);
                    $scope.smsText = text;
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (cancel === false) {
                            return cb(null, $scope.smsText, $scope.addedFile);
                        } else {
                            return cb('nope');
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/fileAndText.html',
            });
        },
		demandeSmartphone: function(inter, cb) {
	    	$mdDialog.show({
			controller: ["$scope", "config", function($scope, config) {
		    	edisonAPI.artisan.get(inter.artisan.id).then(function(resp){
					$scope.rep = resp.data.representant.civilite + " " + resp.data.representant.nom.charAt(0).toUpperCase() + resp.data.representant.nom.substring(1).toLowerCase() + " " + 
						resp.data.representant.prenom.charAt(0).toUpperCase() + resp.data.representant.prenom.substring(1).toLowerCase()
		    	})
		    	$scope.inter = inter
		    	$scope.answer = function(resp) {
				$mdDialog.hide();
				if (resp)
				    return cb(null, resp);
				return cb('nope');
		    	}
			}],
				templateUrl: '/DialogTemplates/demandeSmartphone.html',
	    	});
		},
		/* Pop up phone envoi intervention */
        envoiIntervention: function(data, text, cb) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.data = data;
                    $scope.smsText = text;
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (cancel === false) {
                            return cb(null, $scope.smsText, $scope.addedFile);
                        } else {
                            return cb('nope');
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/envoi.html',
            });
        },
        editProduct: {
            open: function(produit, cb) {
                $mdDialog.show({
                    controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                        $scope.produit = _.clone(produit);
                        $scope.mdDialog = $mdDialog;
                        $scope.answer = function(p) {
                            $mdDialog.hide(p);
                            return cb(p);
                        }
                    }],
                    templateUrl: '/DialogTemplates/edit.html',
                });
            }
        },
        selectSubProduct: function(elem, callback) {
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog) {
                    $scope.elem = elem
                    $scope.answer = function(cancel, item) {
                        $mdDialog.hide();
                        if (!cancel) {
                            return callback(item)
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/selectSubProduct.html',
            });
        },

        alertDialog: function(dataScope){
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog){
                    $scope.answer = function(params) {
                        $mdDialog.hide();
                        var type = 0;
                        if (dataScope.dataVerif.reglementChoice)
                            type = 1;
                        else if (dataScope.dataVerif.date)
                            type = 2;
                        else if (dataScope.dataVerif.raison)
                            type = 3;
                        if(params === true){
                            document.getElementById('popupContainer').style.display = "none";
                            edisonAPI.intervention.updateVerifInter({ data: dataScope.data, type: type}).then(function(resp) {
                            if (resp.data == "OK")
                                LxNotificationService.success("Update de l'intervention effectuée.");
                            else
                                LxNotificationService.error("Erreur update de l'intervention.");
                            })
                        }
                    };
                }],
                templateUrl: '/DialogTemplates/validateDialog.html',
            });
        },
        alertNoDialog: function(data){
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog){
                    console.log("data dialog", data);
                    $scope.answer = function(params) {
                        var refusText = document.getElementById('refusText').value;
                        $mdDialog.hide();
                        console.log(data.dataVerif);
                        var type = 0;
                        if (data.dataVerif.reglementChoice)
                            type = 1;
                        else if (data.dataVerif.date)
                            type = 2;
                        else if (data.dataVerif.raison)
                            type = 3;
                        if (refusText != "")
                        {   
                            if(params === true){
                                document.getElementById('popupContainer').style.display = "none";
                                edisonAPI.intervention.updateRefusInter({ data: data.dataVerif, type: type}).then(function(resp) {
                                    console.log("TEST")
                                    edisonAPI.intervention.sendRefus({ raisonRefus: refusText, data: data.dataVerif, type: 1}).then(function(resp) {
                                        if (resp.data == "OK")
                                                LxNotificationService.success("Mail de refus envoyé.");
                                        else
                                            LxNotificationService.error("Erreur envoi refus.");
                                    })
                                })

                            }
                        }   
                    };
                }],
                templateUrl: '/DialogTemplates/refusValidDialog.html',
            });
        },
        alertNoReglementDialog: function(data){
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog){
                    $scope.answer = function(params) {
                        $mdDialog.hide();
                        console.log(data)
                        if(params === true){
                            document.getElementById('popupContainer').style.display = "none";
                            edisonAPI.intervention.updateRefusInter({ data: data.dataVerif, type: 1}).then(function(resp) {
                                edisonAPI.intervention.sendRefus({ raisonRefus: "", data: data.data, type: 2}).then(function(resp) {
                                    if (resp.data == "OK")
                                        LxNotificationService.success("Mail de refus envoyé.");
                                    else
                                        LxNotificationService.error("Erreur envoi refus.");
                                })
                            })
                        }   
                    };
                }],
                templateUrl: '/DialogTemplates/noReglementDialog.html',
            });
        },
        priseEnCharge: function(data, cb){
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog){
                    $scope.data = data;
                    if ($scope.data.compta.recouvrement.litige.status == 0)
                        $scope.causeRec = { 'retEnvoi': false, 'chequePerdu': false, 'impaye': false, 'factNonRecu': false, 'tarif': false, 'qualite': false, 'pasDeContact': false, 'plaintes': false, 'autres': false };
                    else
                        $scope.causeRec = { 'tarif': false, 'qualite': false, 'opposition': false, 'riskOpposition': false, 'plaintes': false, 'autres': false, 'demandeSav': false };
                    if (data.compta.recouvrement.causeRec != 'none')
                        $scope.causeRec[data.compta.recouvrement.causeRec] = true;
                    $scope.societe = (data.tva == 0 || data.tva == 20) ? true : false;
                    $scope.particulier = (data.tva == 10) ? true : false;
                    if (data.devisOrigine)
                        $scope.devis = { 'yes': true, 'no': false };
                    else
                        $scope.devis = { 'yes': false, 'no': true };
                    if ($scope.devis['yes'] === true)
                        $scope.mailInter = { 'yes': true, 'no': false };
                    else
                        $scope.mailInter = { 'yes': false, 'no': false };
                    var addClient = $scope.data.client.address.n + $scope.data.client.address.r + $scope.data.client.address.cp + $scope.data.client.address.v;
                    if ($scope.data.facture)
                        var addPayeur = $scope.data.facture.address.n + $scope.data.facture.address.r + $scope.data.facture.address.cp + $scope.data.facture.address.v;
                    if (addClient == addPayeur)
                        $scope.coordIdent = { 'yes': true, 'no': false };
                    else
                        $scope.coordIdent = { 'yes': false, 'no': true };
                    $scope.changeBox = function(type, name) {
                        if (type == 0) {
                            for (var k in $scope.devis)
                                if (k != name)
                                    $scope.devis[k] = false;
                            if ($scope.devis['yes'] === true)
                                $scope.mailInter = { 'yes': true, 'no': false };
                            else
                                for (var k in $scope.mailInter)
                                    if (k != name)
                                        $scope.mailInter[k] = false;
                        } else if (type == 1) {
                            if ($scope.devis['yes'] === true)
                                $scope.mailInter = { 'yes': true, 'no': false };
                            else
                                for (var k in $scope.mailInter)
                                    if (k != name)
                                        $scope.mailInter[k] = false;
                        } else if (type == 2) {
                            for (var k in $scope.coordIdent)
                                if (k != name)
                                    $scope.coordIdent[k] = false;
                        } else if (type == 3) {
                            for (var k in $scope.causeRec)
                                if (k != name)
                                    $scope.causeRec[k] = false;
                        }
                    }
                    $scope.answer = function(params) {
                        var count = 0;;
                        for (var k in $scope.causeRec) {
                            if ($scope.causeRec[k] === true)
                                count++;
                        }
                        if (params === true) {
                            if (count != 1) {
                                LxNotificationService.error("Error: Aucunes ou plus d'une case cocher dans les causes");
                            }
                            else {
                                $mdDialog.hide();
                                for (var k in $scope.causeRec) {
                                    if ($scope.causeRec[k] === true)
                                        data.compta.recouvrement.causeRec = k;
                                }
                                if (data.compta.recouvrement.litige.status == 1)
                                    data.compta.recouvrement.litige.status = 2;
                                else
                                    data.compta.recouvrement.priseEnCharge = 1;
                                data.compta.recouvrement.devisSigne = ($scope.devis['yes'] === true) ? true : false;
                                data.compta.recouvrement.mailInter = ($scope.mailInter['yes'] === true) ? true : false;
                                data.compta.recouvrement.coordFactIdent = ($scope.coordIdent['yes'] === true) ? true : false;
                                cb (data);
                            }
                        }
                        else
                            $mdDialog.hide();
                    };
                }],
                templateUrl: '/DialogTemplates/priseEnCharge.html',
            });
        },
        priseDeContact: function(data, cb){
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog){
                    $scope.data = data;
                    if ($scope.data.compta.recouvrement.litige.status == 0)
                        $scope.causeRec = { 'retEnvoi': false, 'chequePerdu': false, 'impaye': false, 'factNonRecu': false, 'tarif': false, 'qualite': false, 'pasDeContact': false, 'plaintes': false, 'autres': false };
                    else
                        $scope.causeRec = { 'tarif': false, 'qualite': false, 'opposition': false, 'riskOpposition': false, 'plaintes': false, 'autres': false, 'demandeSav': false };
                    $scope.status = { 'enc': false, 'reg': false, 'resolu': false, 'perte': false };
                    if ($scope.data.compta.recouvrement.litige.status == 0) {
                        $scope.conclu = { 'demandeSav': false, 'remiseCom': false, 'lettreDesist': false, 'factRenv': false, 'lettreOppo': false, 'remiseEnc': false,
                        'protAcc': false, 'demeure': false, 'injTribProx': false, 'injTribCom': false, 'negoc': false, 'regClient': false };
                    } else {
                        $scope.conclu = { 'protAcc': false, 'demeure': false, 'lettreOppo': false, 'demandeSav': false, 'remiseCom': false, 'negoc': false, 'regClient': false, 'envRec': false };
                    }
                    if ($scope.data.compta.recouvrement.priseDeContact.conclu != 'none')
                        $scope.conclu[$scope.data.compta.recouvrement.priseDeContact.conclu] = true;
                    if ($scope.data.compta.recouvrement.causeRec != 'none')
                        $scope.causeRec[$scope.data.compta.recouvrement.causeRec] = true;
                    if ($scope.data.compta.recouvrement.priseDeContact.conclu == 'remiseCom' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'protAcc'
                        || $scope.data.compta.recouvrement.priseDeContact.conclu == 'factRenv' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'lettreDesist'
                        || $scope.data.compta.recouvrement.priseDeContact.conclu == 'regClient' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'remiseEnc')
                        $scope.status['reg'] = true;
                    else
                        if ($scope.data.compta.recouvrement.priseDeContact.statusRec != 'none')
                            $scope.status[$scope.data.compta.recouvrement.priseDeContact.statusRec] = true;
                    $scope.changeBox = function(type, name) {
                        if (type == 0) {
                            for (var k in $scope.causeRec)
                                if (k != name)
                                    $scope.causeRec[k] = false;
                        } else if (type == 1) {
                            if (name == 'regClient' || name == 'factRenv' || name == 'lettreDesist' || name == 'protAcc' || name === 'remiseCom' || name === 'remiseEnc') {
                                for (var k in $scope.status)
                                    if (k != 'reg')
                                        $scope.status[k] = false;
                                    else if (k == 'reg' && $scope.status[k] === false)
                                        $scope.status[k] = true;
                            }
                            else {
                                $scope.status = { 'enc': false, 'reg': false, 'resolu': false, 'perte': false };
                            }
                            for (var k in $scope.conclu)
                                if (k != name)
                                    $scope.conclu[k] = false;
                        } else if (type == 2) {
                            if ($scope.conclu['regClient'] === true || $scope.conclu['factRenv'] === true
                            || $scope.conclu['lettreDesist'] === true || $scope.conclu['protAcc'] === true
                            || $scope.conclu['remiseCom'] === true || $scope.conclu['remiseEnc'] === true) {
                                for (var k in $scope.status)
                                    if (k != 'reg')
                                        $scope.status[k] = false;
                                    else if (k == 'reg' && $scope.status[k] === false)
                                        $scope.status[k] = true;

                            } else {
                                for (var k in $scope.status)
                                    if (k != name)
                                        $scope.status[k] = false;
                            }
                        }
                    }
                    $scope.answer = function(params) {
                        var count = 0;
                        for (var k in $scope.causeRec) {
                            if ($scope.causeRec[k] === true)
                                count++;
                        }
                        var count1 = 0;
                        for (var k in $scope.conclu) {
                            if ($scope.conclu[k] === true)
                                count1++;
                        }
                        var count2 = 0;
                        if ($scope.conclu['envRec'] === false) {
                            for (var k in $scope.status) {
                                if ($scope.status[k] === true)
                                    count2++;
                            }
                        }
                        else
                            count2 = 1;
                        if (params === true) {
                            if (count != 1 || count1 != 1 || count2 != 1) {
                                LxNotificationService.error("Error: Aucunes ou plus d'une case cocher dans les causes, conclusions ou status");
                            }
                            else {
                                for (var k in $scope.causeRec) {
                                    if ($scope.causeRec[k] === true)
                                        $scope.data.compta.recouvrement.causeRec = k;
                                }
                                for (var k in $scope.conclu) {
                                    if ($scope.conclu[k] === true)
                                        $scope.data.compta.recouvrement.priseDeContact.conclu = k;
                                }
                                if ($scope.data.compta.recouvrement.priseDeContact.conclu == 'remiseCom' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'protAcc'
                                    || $scope.data.compta.recouvrement.priseDeContact.conclu == 'factRenv' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'lettreDesist'
                                    || $scope.data.compta.recouvrement.priseDeContact.conclu == 'regClient' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'remiseEnc') {
                                    $scope.data.compta.recouvrement.priseDeContact.statusRec = "reg";
                                } else if ($scope.data.compta.recouvrement.priseDeContact.conclu != 'envRec') {
                                    for (var k in $scope.status) {
                                        if ($scope.status[k] === true)
                                            $scope.data.compta.recouvrement.priseDeContact.statusRec = k;
                                    }
                                }
				var day = new Date().getDate();
				var month = new Date().getMonth() + 1;
				if (month < 10)
					month = '0' + month;
				var year = new Date().getFullYear();
                                if (!$scope.data.compta.recouvrement.priseDeContact.comment)
                                    $scope.data.compta.recouvrement.priseDeContact.comment = day + '/' + month + '/' + year + " Pas de réponse";
                                if ($scope.data.compta.recouvrement.priseDeContact.statusRec == "reg" && !data.compta.recouvrement.priseDeContact.dateRelance) {
                                    LxNotificationService.error("Error: Date de relance obligatoire pour ce status");
                                } else {
                                    $mdDialog.hide();
                                    if ($scope.data.compta.recouvrement.priseDeContact.statusRec != 'reg')
                                        delete $scope.data.compta.recouvrement.priseDeContact.dateRelance;
                                    if (($scope.data.compta.recouvrement.priseDeContact.statusRec == 'perte' || $scope.data.compta.recouvrement.priseDeContact.statusRec == 'resolu') && $scope.data.compta.recouvrement.priseEnCharge == 1) {
                                        $scope.data.compta.recouvrement.priseEnCharge = 2;
                                    } else if (($scope.data.compta.recouvrement.priseDeContact.statusRec == 'perte' || $scope.data.compta.recouvrement.priseDeContact.statusRec == 'resolu') && $scope.data.compta.recouvrement.litige.status == 2) {
                                        $scope.data.compta.recouvrement.litige.status = 3;
                                    } else if ($scope.data.compta.recouvrement.litige.status == 2) {
                                        $scope.data.compta.recouvrement.litige.status = 2;
                                    } else {
                                        $scope.data.compta.recouvrement.priseEnCharge = 1;
                                    }
                                    if (!$scope.comment || $scope.comment == "")
                                        $scope.data.compta.recouvrement.priseDeContact.comment.push(day + '/' + month + '/' + year + " Pas de réponse");
                                    else
                                        $scope.data.compta.recouvrement.priseDeContact.comment.push(day + '/' + month + '/' + year + ' ' + $scope.comment);
                                    cb ($scope.data);
                                }
                            }
                        } else {
                            $mdDialog.hide();
                        }
                    };
                    $scope.reset = function(params) {
                        if (params === true) {
                            $mdDialog.hide();
                            edisonAPI.intervention.resetRec({ data: $scope.data }).then(function(returnEnd) {
                                if (returnEnd.data == "YES") {
                                    LxNotificationService.success("Reset des données recouvrement");
                                } else {
                                    LxNotificationService.error("Une erreur est survenue");
                                }
                            })
                        }
                    }
                }],
                templateUrl: '/DialogTemplates/priseDeContact.html',
            });
        },
        statusRec: function(data, cb){
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog){
                    $scope.data = data;
                    $scope.status = { 'enc': false, 'reg': false, 'resolu': false, 'perte': false };
                    if ($scope.data.compta.recouvrement.priseDeContact.statusRec != 'none')
                        $scope.status[data.compta.recouvrement.priseDeContact.statusRec] = true;
                    $scope.changeBox = function(name) {
                        for (var k in $scope.status)
                            if (k != name)
                                $scope.status[k] = false;
                    }
                    $scope.answer = function(params) {
                        var count = 0;
                        for (var k in $scope.status) {
                            if ($scope.status[k] === true)
                                count++;
                        }
                        if (params === true) {
                            if (count != 1) {
                                LxNotificationService.error("Error: Aucunes ou plus d'une case cocher");
                            }
                            else {
                                $mdDialog.hide();
                                for (var k in $scope.status) {
                                    if ($scope.status[k] === true)
                                        data.compta.recouvrement.priseDeContact.statusRec = k;
                                }
                                if ($scope.data.compta.recouvrement.priseDeContact.statusRec != 'reg')
                                    delete $scope.data.compta.recouvrement.priseDeContact.dateRelance;
                                if (($scope.data.compta.recouvrement.priseDeContact.statusRec == 'perte' || $scope.data.compta.recouvrement.priseDeContact.statusRec == 'resolu')
                                    && $scope.data.compta.recouvrement.priseEnCharge == 1)
                                    $scope.data.compta.recouvrement.priseEnCharge = 2;
                                else if (($scope.data.compta.recouvrement.priseDeContact.statusRec == 'perte' || $scope.data.compta.recouvrement.priseDeContact.statusRec == 'resolu')
                                    && $scope.data.compta.recouvrement.litige.status == 2)
                                    $scope.data.compta.recouvrement.litige.status = 3;
                                else if ($scope.data.compta.recouvrement.litige.status == 2 || $scope.data.compta.recouvrement.litige.status == 3){
                                    $scope.data.compta.recouvrement.priseEnCharge = 0;
                                    $scope.data.compta.recouvrement.litige.status = 2;
                                }
                                else
                                    $scope.data.compta.recouvrement.priseEnCharge = 1;
                                cb ($scope.data);
                            }
                        }
                        else
                            $mdDialog.hide();
                    };
                }],
                templateUrl: '/DialogTemplates/statusRec.html',
            });
        },
        createLitige: function(cb){
            $mdDialog.show({
                controller: ["$scope", "$mdDialog", function DialogController($scope, $mdDialog){
                    console.log("Dialog litige");
                    $scope.answer = function(params) {
                        if (params === true) {
                            edisonAPI.intervention.getDataOneInter({ id: parseInt($scope.idLitige) }).then(function(resp) {
                                if (resp.data.compta.recouvrement.litige.status != 0) {
                                    LxNotificationService.error("Litige déjà éxistant");
                                } else {
                                    if (!$scope.idLitige)
                                        LxNotificationService.error("Case de l'id litige vide");
                                    else {
                                        $mdDialog.hide();
                                        cb (parseInt($scope.idLitige))
                                    }
                                }
                            })
                        }
                        else
                            $mdDialog.hide();
                    };
                }],
                templateUrl: '/DialogTemplates/createLitige.html',
            });
        }
    }
}]);

angular.module('edison').factory('fourniture', ["$rootScope", function($rootScope) {
    "use strict";

    return {
        init: function(fourniture) {
            this.fourniture = fourniture;
            if (!this.fourniture)
                this.fourniture = [];
            return this;
        },
        remove: function(index) {
            this.fourniture.splice(index, 1);
        },
        moveTop: function(index) {
            if (index !== 0) {
		fourniture.updateDate(index);
                var tmp = this.fourniture[index - 1];
                this.fourniture[index - 1] = this.fourniture[index];
                this.fourniture[index] = tmp;
            }

        },
        moveDown: function(index) {
            if (index !== this.fourniture.length - 1) {
		fourniture.updateDate(index);
                var tmp = this.fourniture[index + 1];
                this.fourniture[index + 1] = this.fourniture[index];
                this.fourniture[index] = tmp;
            }
        },
        add: function() {
            this.fourniture.push({
                bl: '0',
                title: 'Fourniture',
                quantite: 1,
                pu: 0
            });
        },
        total: function() {
            var total = 0;
            if (this.fourniture) {
                this.fourniture.forEach(function(e) {
                    total += (e.pu * e.quantite);
                })
            }
            return total
        },
        updateDate: function(index) {
            var dat = new Date();
            var d = dat.getDate().toString();
            var m = (dat.getMonth() + 1).toString();
            var min = (dat.getMinutes()).toString();
            var fulldate = $rootScope.user.pseudo + " " + dat.getHours() + "h" +
                (min[1] ? min : "0" + min[0]) + " " + (d[1] ? d : "0" + d[0]) + "/" +
                (m[1] ? m : "0" + m[0]) + "/" + dat.getFullYear()
            this.fourniture[index].date = fulldate;
        }
    }

}]);

angular.module('edison')
    .factory('Intervention', ["$location", "$window", "openPost", "LxNotificationService", "LxProgressService", "dialog", "user", "config", "edisonAPI", "Devis", "$rootScope", "$routeParams", "textTemplate", function($location, $window, openPost, LxNotificationService, LxProgressService, dialog, user, config, edisonAPI, Devis, $rootScope, $routeParams, textTemplate) {
        "use strict";

        var Intervention = function(data) {
            if (!(this instanceof Intervention)) {
                return new Intervention(data);
            }
            for (var k in data) {
                this[k] = data[k];
            }
        };

        var appelLocal = function(tel) {
            if (tel) {
                $window.open('callto:' + tel, '_self', false);
            }
        }

        Intervention.prototype.callTel1 = function() {
            appelLocal(this.client.telephone.tel1)
        }
        Intervention.prototype.callTel2 = function() {
            appelLocal(this.client.telephone.tel2)
        }
        Intervention.prototype.callTel3 = function() {
            appelLocal(this.client.telephone.tel3)
        }

        Intervention.prototype.callSst1 = function() {
            appelLocal(this.sst.telephone.tel1)
	        edisonAPI.artisan.callLog(this.sst.id).then(function (resp) {
                var message = _.template("L'appel a été ajouté à l'historique.")(resp.data)
                LxNotificationService.success(message);
            }, function (error) {
                LxNotificationService.error(error.data);
	        });
        }
        Intervention.prototype.callSst2 = function() {
            appelLocal(this.sst.telephone.tel2)
            edisonAPI.artisan.callLog(this.sst.id).then(function (resp) {
                var message = _.template("L'appel a été ajouté à l'historique.")(resp.data)
                LxNotificationService.success(message);
            }, function (error) {
                LxNotificationService.error(error.data);
            });
        }

        Intervention.prototype.callPayeur1 = function() {
            appelLocal(this.facture.tel)
        }

        Intervention.prototype.callPayeur2 = function() {
            appelLocal(this.facture.tel2)
        }

        Intervention.prototype.typeOf = function() {
            return 'Intervention';
        };
        Intervention.prototype.envoiDevis = function(cb) {
            Devis().envoi.bind(this)(cb)
        };

        Intervention.prototype.validerReglement = function(cb) {
            var _this = this;
            dialog.validationReglement(this, function(err, resp) {
                if (err) {
                    return cb && cb(err);
                }
                edisonAPI.intervention.save(_this).then(function(resp) {
                    LxNotificationService.success("L'intervention " + _this.id + " est modifié");
                }, function(err) {
                    LxNotificationService.error("Une erreur est survenu (" + err.data + ")");
                });
            })
        };

        Intervention.prototype.encaissement = function(cb) {
            var _this = this;
            dialog.encaissement(this, function(err, newInter) {
                if (err) {
                    LxNotificationService.error("Une erreur est survenu");
                } else {
                    edisonAPI.intervention.saveEncaissement({ data: newInter }).then(function(resp) {
                        LxNotificationService.success("Encaissement valider");
                    }, function(err) {
                        LxNotificationService.error("Une erreur est survenu");
                    });
                }
            })
        };

        Intervention.prototype.decaissement = function(cb) {
            var _this = this;
            dialog.decaissement(this, function(err, newInter) {
                if (err) {
                    LxNotificationService.error("Une erreur est survenu");
                } else {
                    edisonAPI.intervention.saveDecaissement({ data: newInter }).then(function(resp) {
                        LxNotificationService.success("Decaissement valider");
                    }, function(err) {
                        LxNotificationService.error("Une erreur est survenu");
                    });
                }
            })
        };

	Intervention.prototype.comment = function() {
            var _this = this;
            dialog.applyComment({comments: _this.comments}, function(comment) {
        		_this.comments.push({
                    login: user.login,
                    text: comment,
                    date: new Date()
                })
        		edisonAPI.intervention.save(_this)
            });	    
        }

        Intervention.prototype.validerPaiement = function(cb) {
            var _this = this;
            dialog.validationPaiement(this, function(err, resp) {
                if (err) {
                    return cb && cb(err);
                }
                edisonAPI.intervention.save(_this).then(function(resp) {
                    LxNotificationService.success("L'intervention " + _this.id + " est modifié");
                }, function(err) {
                    LxNotificationService.error("Une erreur est survenu (" + err.data + ")");
                });
            })
        };

        Intervention.prototype.demarcher = function(cb) {
            edisonAPI.intervention.demarcher(this.id).success(function() {
                LxNotificationService.success("Vous demarchez l'intervention");
            }, function() {
                LxNotificationService.error("Une erreur est survenu");
            })
        };

        Intervention.prototype.favoris = function(cb) {
            edisonAPI.intervention.favoris({ id: this.id }).success(function(resp) {
                if (resp == "OUI")
                    LxNotificationService.success("Vous avez mis l'intervention en favoris");
                else if (resp == "NON")
                    LxNotificationService.success("Vous avez retiré l'intervention des favoris");
            }, function() {
                LxNotificationService.error("Une erreur est survenu");
            })
        };

        Intervention.prototype.facturePreview = function() {
            openPost('/api/intervention/facturePreview', {
                data: JSON.stringify(this),
                html: true
            })
        }

        Intervention.prototype.factureAcquittePreview = function() {
            openPost('/api/intervention/factureAcquittePreview', {
                data: JSON.stringify(this),
                html: true
            })
        }


        Intervention.prototype.sendFactureAcquitte = function(cb) {
            var _this = this;
            var datePlain = moment(this.date.intervention).format('LL');
            var template = textTemplate.mail.intervention.factureAcquitte.bind(_this)(datePlain)
            var mailText = (_.template(template)(this))
            dialog.envoiFacture(_this, mailText, true, function(err, text, acquitte, date, dataFiles) {
                if (err)
                    return cb('nope')
                LxProgressService.circular.show('#5fa2db', '#globalProgress');
                edisonAPI.intervention.sendFactureAcquitte(_this.id, {
                    text: text,
                    acquitte: acquitte,
                    date: date,
                    data: _this,
                    dataFiles: dataFiles
                }).success(function(resp) {
                    LxProgressService.circular.hide();
                    var validationMessage = _.template("La facture de l'intervention {{id}} à été envoyé")(_this)
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    LxProgressService.circular.hide();
                    var validationMessage = _.template("L'envoi de la facture {{id}} à échoué\n" + "(" + err.data + ")")(_this)
                    LxNotificationService.error(validationMessage);
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        }

        Intervention.prototype.sendFacture = function(cb) {
            var _this = this;
            var datePlain = moment(this.date.intervention).format('LL');
            var template = textTemplate.mail.intervention.envoiFacture.bind(_this)(datePlain)
            var mailText = (_.template(template)(this))
            dialog.envoiFacture(_this, mailText, false, function(err, text, acquitte, date, dataFiles) {
                if (err)
                    return cb('nope')
                LxProgressService.circular.show('#5fa2db', '#globalProgress');
                edisonAPI.intervention.sendFacture(_this.id, {
                    text: text,
                    dataFiles: dataFiles
                }).success(function(resp) {
                    LxProgressService.circular.hide();
                    var validationMessage = _.template("La facture de l'intervention {{id}} à été envoyé")(_this)
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    LxProgressService.circular.hide();
                    var validationMessage = _.template("L'envoi de la facture {{id}} à échoué\n" + "(" + err.data + ")")(_this)
                    LxNotificationService.error(validationMessage);
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        }



        Intervention.prototype.ouvrirFicheV1 = function() {
            $window.open('http://electricien13003.com/alvin/5_Gestion_des_interventions/show_res_bis_2.php?id_client=' + this.id)
        }
        Intervention.prototype.autoFacture = function() {
            $window.open('/api/intervention/' + this.id + '/autoFacture')
        }
        Intervention.prototype.ouvrirFiche = function() {
            $location.url('/intervention/' + this.id)
        }
        Intervention.prototype.ouvrirRecapSST = function() {
            $location.url(['/artisan', this.artisan.id, 'recap'].join('/') + '#interventions')
        }
        Intervention.prototype.smsArtisan = function(cb) {
            var _this = this;
            var text = textTemplate.sms.intervention.demande.bind(this)(user, config);
            text = _.template(text)(this)
            dialog.getFileAndText(_this, text, [], function(err, text) {
                if (err) {
                    return cb(err)
                }
                edisonAPI.sms.send({
                    dest: _this.sst.nomSociete,
                    text: text,
                    to: _this.sst.telephone.tel1,
                }).success(function(resp) {
                    var validationMessage = _.template("Un sms a été envoyé à M. {{sst.representant.nom}}")(_this)
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    LxNotificationService.success("L'envoi du sms a échoué");
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        };

        Intervention.prototype.callClient = function(cb) {
            var _this = this;
            var now = Date.now();
            $window.open('callto:' + _this.client.telephone.tel1, '_self', false)
            dialog.choiceText({
                subTitle: _this.client.telephone.tel1,
                title: 'Nouvel Appel Client',
            }, function(response, text) {
                edisonAPI.call.save({
                    date: now,
                    to: _this.client.telephone.tel1,
                    link: _this.id,
                    origin: _this.id || _this.tmpID || 0,
                    description: text,
                    response: response
                }).success(function(resp) {
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        }
        Intervention.prototype.callArtisan = function(cb) {
            var _this = this;
            var now = Date.now();
            $window.open('callto:' + _this.artisan.telephone.tel1, '_self', false)
            dialog.choiceText({
                subTitle: _this.artisan.telephone.tel1,
                title: 'Nouvel Appel',
            }, function(response, text) {
                edisonAPI.call.save({
                    date: now,
                    to: _this.artisan.telephone.tel1,
                    link: _this.artisan.id,
                    origin: _this.id || _this.tmpID || 0,
                    description: text,
                    response: response
                }).success(function(resp) {
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        };
        Intervention.prototype.save = function(cb) {
            var _this = this;
            var fournitureSansFournisseur = _.find(this.fourniture, function(e) {
                return !e.fournisseur;
            })
            var fournitureSansPU = _.find(this.fourniture, function(e) {
                    return !e.pu;
            })
            if (fournitureSansFournisseur) {
                LxNotificationService.error("Veuillez renseigner un fournisseur");
                return cb(fournitureSansFournisseur)
            }
            if (fournitureSansPU) {
                LxNotificationService.error("Veuillez renseigner un prix pour toutes les fournitures");
                return cb(fournitureSansPU)
            }
            edisonAPI.intervention.save(_this)
                .then(function(resp) {
                    var validationMessage = _.template("Les données de l'intervention {{id}} ont à été enregistré.")(resp.data)
                    if ((_this.tmpID && _this.sst) || (_this.sst__id && _this.sst && _this.sst__id !== _this.sst.id) && !_this.sst.tutelle) {
                        validationMessage += "\n\n Un sms à été envoyé";
                    }
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function')
                        cb(null, resp.data)
                }, function(error) {
                    LxNotificationService.error(error.data);
                    if (typeof cb === 'function')
                        cb(error.data)
                });
        };

		/* Prototype envoi intervention */
        Intervention.prototype.envoi = function(cb) {
            var _this = this;
	    	var pass = 0;
            if (!Intervention(_this).isEnvoyable()) {
                return LxNotificationService.error("Vous ne pouvez pas envoyer cette intervention");
            }
	    	edisonAPI.artisan.get(_this.artisan.id).then(function(resp2){
                _this.artisan.majInternet = resp2.data.majInternet;
                var defaultText = textTemplate.sms.intervention.envoi.bind(_this)(_.find(window.app_users, 'login', _this.login.ajout));
				if (!resp2.data.smartphone || resp2.data.smartphone === "Non demandé")
				{
		    		dialog.demandeSmartphone(_this, function(err, demande) {
						if (err)
						    return cb && cb(err)
						resp2.data.smartphone = demande;
						edisonAPI.artisan.save(resp2.data)
						dialog.envoiIntervention(_this, defaultText, function(err, text, file) {
			    			if (err)
								return cb && cb(err)
				  	  		LxProgressService.circular.show('#5fa2db', '#globalProgress');
				    		edisonAPI.intervention.envoi(_this.id, {
								sms: text,
								file: file
			    			}).then(function(resp) {
								LxProgressService.circular.hide();
								var validationMessage = _.template("L'intervention est envoyé")
								LxNotificationService.success(validationMessage);
								if (typeof cb === 'function')
						    		cb(null, resp.data)
			    			}, function(error) {
								LxProgressService.circular.hide();
								LxNotificationService.error(error.data);
								if (typeof cb === 'function')
						 		   cb(error.data);
			    			});
						})
		    		})
				}
				else
				{
					/* Pop up phone envoi sms */
		    		dialog.envoiIntervention(_this, defaultText, function(err, text, file) {
						if (err)
					    	return cb && cb(err)
						LxProgressService.circular.show('#5fa2db', '#globalProgress');
						edisonAPI.intervention.envoi(_this.id, {
						    sms: text,
						    file: file
						}).then(function(resp) {
						    LxProgressService.circular.hide();
						    var validationMessage = _.template("L'intervention est envoyé")
						    LxNotificationService.success(validationMessage);
						    if (typeof cb === 'function')
								cb(null, resp.data)
						}, function(error) {
						    LxProgressService.circular.hide();
						    LxNotificationService.error(error.data);
						    if (typeof cb === 'function')
								cb(error.data);
						});
		    		})
				}
	    	})
        };
	

        Intervention.prototype.reactivation = function(cb) {
            var _this = this;
            edisonAPI.intervention.reactivation(this.id).then(function() {
                LxNotificationService.success("L'intervention " + _this.id + " est à programmer");
            })
        };

        Intervention.prototype.annulation = function(cb) {
            var _this = this;
	    _this.config = config
            dialog.getCauseAnnulation(_this, function(err, causeAnnulation, reinit, sms, textSms) {
                if (err) {
                    return typeof cb === 'function' && cb('err');
                }
		if (causeAnnulation === "PS_SST" || causeAnnulation === "PS_SST_DISPO" || causeAnnulation === "PS_BON")
		{
		    var addr = _this.client.address.cp + ' ' + _this.client.address.v;
		    var dat = new Date();
		    var day = dat.getDate().toString();
		    var m = (dat.getMonth() + 1).toString();
		    var min = (dat.getMinutes()).toString();
		    var fulldate = dat.getHours() + "h" +
			(min[1] ? min : "0" + min[0]) + " " + (day[1] ? day : "0" + day[0]) + "/" +
			(m[1] ? m : "0" + m[0]) + "/" + dat.getFullYear();
		    var obj = {
			date: fulldate,
			login: $rootScope.user.login,
			cate: config.categories[_this.categorie].order,
			ad: addr,
			status: false,
		    };
		    edisonAPI.demarchage.add(obj).then(function (resp) {
			var message = _.template("Une nouvelle adresse a été ajoutée à la liste de démarchage.")(resp.data)
			LxNotificationService.success(message);
		    }, function (error) {
			LxNotificationService.error(error.data);
		    });
		}
                edisonAPI.intervention.annulation(_this.id, {
                    causeAnnulation: causeAnnulation,
                    reinit: reinit,
                    sms: sms,
                    textSms: textSms
                })
		    .then(function(resp) {
                        var msg = "L'intervention {{id}} est annulé";
                        if (sms) {
                            msg += "\nUn sms à été envoyé au SST";
                        }
                        var validationMessage = _.template(msg)(resp.data)
                        LxNotificationService.success(validationMessage);
                        if (typeof cb === 'function') {
                            cb(null, resp.data)
                        }
                    });
            });
        };


        Intervention.prototype.envoiFactureVerif = function(cb) {
            var _this = this;

            if (!this.produits.length) {
                LxNotificationService.error("Veuillez renseigner les produits");
                return cb('nope')
            }
            _this.sendFacture(function(err) {
                if (err)
                    return cb(err);
                _this.verificationSimple(cb)
            })
        }

        Intervention.prototype.verificationSimple = function(cb) {
            var _this = this;
            LxProgressService.circular.show('#5fa2db', '#globalProgress');
            if (!Intervention(this).isVerifiable()) {
                return LxNotificationService.error("Vous ne pouvez pas verifier cette intervention");
            }
            edisonAPI.intervention.verification(_this.id)
                .then(function(resp) {
                    LxProgressService.circular.hide()
                    var validationMessage = _.template("L'intervention {{id}} est vérifié")(resp.data)
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function') {
                        cb(null, resp.data);
                    }
                }, function(error) {
                    LxProgressService.circular.hide()
                    LxNotificationService.error(error.data);
                    cb(error.data);
                })
        }

        Intervention.prototype.verification = function(cb) {
            if (!Intervention(this).isVerifiable()) {
                return LxNotificationService.error("Vous ne pouvez pas verifier cette intervention");
            }
            var _this = this;
            if (!_this.reglementSurPlace) {
                return $location.url('/intervention/' + this.id)
            }
            dialog.verification(_this, function(inter) {
                Intervention(inter).save(function(err, resp) {
                    if (!err) {
                        return Intervention(resp).verificationSimple(cb);
                    }
                });
            });
        }


        Intervention.prototype.recouvrement = function(cb) {
            var _this = this;
            dialog.recouvrement(_this, function(inter) {
		inter.recouvrement.date = new Date()
		inter.recouvrement.login = user.login
                Intervention(inter).save(function(err, resp) {
                    return (cb || _.noop)()
                });
            });
        }

        Intervention.prototype.priseDeContact = function(cb) {
            edisonAPI.intervention.getDataOneInter({ id: this.id }).then(function(dataInter) {
                dialog.priseDeContact(dataInter.data, function(data) {
                    if (data.compta.recouvrement.priseDeContact.conclu == "demandeSav")
                        edisonAPI.intervention.demandeSav({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Demande sav envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "remiseCom")
                        edisonAPI.intervention.remiseCom({ data: data }).then(function(returnEnd) {
                            if (returnEnd.data == "YES")
                                LxNotificationService.success("Prix à négocier modifié");
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "lettreDesist")
                        edisonAPI.intervention.lettreDesist({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Lettre de désistement envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "remiseEnc")
                        edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                            if (returnEnd.data == "YES")
                                LxNotificationService.success("Remise à l'encaissement");
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "injTribProx")
                        edisonAPI.intervention.injTribProx({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Injonction tribunal de proximite envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "injTribCom")
                        edisonAPI.intervention.injTribCom({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Injonction tribunal de commerce envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "protAcc")
                        edisonAPI.intervention.protAcc({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Protocole d'accord envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "demeure")
                        edisonAPI.intervention.demeure({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Mise en demeure envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "lettreOppo")
                        edisonAPI.intervention.lettreOppo({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Lettre d'opposition envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "factRenv")
                        Intervention(data).sendFacture(function(err) {
                            if (err)
                                LxNotificationService.error("Une erreur est survenue")
                            else
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Renvoi de la facture effectué");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "regClient")
                        edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                            if (returnEnd.data == "YES")
                                LxNotificationService.success("Règlement client en cours");
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "negoc")
                        edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                            if (returnEnd.data == "YES")
                                LxNotificationService.success("Négociation en cours");
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "envRec")
                        edisonAPI.intervention.envRec({ data: data }).then(function(returnEnd) {
                            if (returnEnd.data == "YES")
                                LxNotificationService.success("Envoi en recouvrement");
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                })
            })
        }

        Intervention.prototype.priseEnCharge = function(cb) {
            edisonAPI.intervention.getDataOneInter({ id: this.id }).then(function(dataInter) {
                dialog.priseEnCharge(dataInter.data, function(data) {
                    edisonAPI.intervention.priseEnCharge({ data: data }).then(function(returnPrise) {
                        if (returnPrise.data == "YES")
                            LxNotificationService.success("Vous avez pris en charge l'intervention");
                    }, function() {
                        LxNotificationService.error("Une erreur est survenu");
                    })
                })
            })
        }

        Intervention.prototype.statusRec = function(cb) {
            edisonAPI.intervention.getDataOneInter({ id: this.id }).then(function(dataInter) {
                dialog.statusRec(dataInter.data, function(data) {
                    edisonAPI.intervention.saveRec({ data: data }).then(function(returnPrise) {
                        if (returnPrise.data == "YES")
                            LxNotificationService.success("Vous avez changé le status de recouvrement");
                    }, function() {
                        LxNotificationService.error("Une erreur est survenu");
                    })
                })
            })
        }

        Intervention.prototype.fileUpload = function(file, cb) {
            var _this = this;
            if (file) {
                LxProgressService.circular.show('#5fa2db', '#fileUploadProgress');
                edisonAPI.file.upload(file, {
                    link: _this.id || _this.tmpID,
                    model: 'intervention',
                    type: 'fiche'
                }).success(function(resp) {
                    LxProgressService.circular.hide();
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    LxProgressService.circular.hide();
                    if (typeof cb === 'function')
                        cb(err);
                })
            }
        }

        Intervention.prototype.editCB = function() {
            var _this = this;
            edisonAPI.intervention.getCB(this.id).success(function(resp) {
                _this.cb = resp;
            }, function(error) {
                LxNotificationService.error(error.data);
            })
        }

        Intervention.prototype.reinitCB = function() {
            this.cb = {
                number: 0
            }
        }
        function moneyStore()
        {
            this.limit = -1;
            this.seuil = -1;
            this.lswp = -1;
            this.sswp = -1;
            this.srwp = -1;
        }
        var moneyInstance = new moneyStore();
	//new Single Instance of moneyStore, acts like a C Static Variable

        Intervention.prototype.isEnvoyable = function()
		{
            if (this.status === "SAV")
            {
                return (true);
            }
			if (moneyInstance.limit == -1)
			{
				moneyInstance.limit = 0;
				edisonAPI.intervention.checklimit({id:this.sst._id}).then(function(resp)
				{
					moneyInstance.lswp = Number(resp.data);
				});
			}
			if (moneyInstance.seuil == -1)
			{
				moneyInstance.seuil = 0;
				edisonAPI.intervention.getSeuilMaxArtisan({id:this.sst._id}).then(function(resp)
				{
					moneyInstance.sswp = Number(resp.data);
				});
				edisonAPI.intervention.getSeuilRuptArtisan({id:this.sst._id}).then(function(resp)
				{
					moneyInstance.srwp = Number(resp.data);
				});
			}
			var isAdmin = typeof user.admin == 'undefined' ? false : user.admin;
	    	var isRoot = typeof user.root == 'undefined' ? false : user.root;
	    	
			if (!this.sst || (((moneyInstance.srwp < moneyInstance.lswp) == true) && isAdmin == false) || (((moneyInstance.sswp < moneyInstance.lswp) == true) && isRoot == false))
			{
				return (false);
			}
			else if (this.sst.subStatus === 'QUA' ||  this.sst.blocked)
			{
				return user.root;
			}
			else if (this.sst.subStatus === 'NEW' || this.sst.subStatus === 'TUT' || this.sst.status === 'POT')
			{
				return user.root || user.service === 'PARTENARIAT'
			}
            else if (!this.reglementSurPlace)
            {
                return true
            }
			return _.includes(["ANN", "APR", "ENC", undefined], this.status)
		}

        Intervention.prototype.isPayable = function() {
            return (this.status === 'ENC' || this.status === 'VRF') &&
                user.root ||  user.service === 'COMPTABILITE' || user.service === 'RECOUVREMENT'
        }

        Intervention.prototype.isVerifiable = function() {
	    if (!this.artisan) {
                return false;
            }
	    if (this.status === 'SAV')
		return this.status === 'SAV'
            return this.status === 'ENC'
        }
        return Intervention;
    }]);

 angular.module('edison').directive('whenScrollEnds', function() {
        return {
            restrict: "A",
            link: function(scope, element, attrs) {
                var visibleHeight = element.height();
                var threshold = 100;

                element.scroll(function() {
                    var scrollableHeight = element.prop('scrollHeight');
                    var hiddenContentHeight = scrollableHeight - visibleHeight;

                    if (hiddenContentHeight - element.scrollTop() <= threshold) {
                        // Scroll is almost at the bottom. Loading more rows
                        scope.$apply(attrs.whenScrollEnds);
                    }
                });
            }
        };
    });
angular.module('edison').factory('Map', function() {
    "use strict";

    var Map = function() {
        this.display = false;
    }

    Map.prototype.setCenter = function(address) {
        var myLatLng = new google.maps.LatLng(address.lt, address.lg);
        this.center = address;
        if (window.map)
            window.map.setCenter(myLatLng)
    }

    Map.prototype.setZoom = function(value) {
       // throw new Error('lol')
         if (window.map)
            window.map.setZoom(value)
        this.zoom = value
    }
    Map.prototype.show = function() {
        this.display = true;
    }

    var circleList = [];
    var cate = -1;

    Map.prototype.setPoly = function (circles) {
	if (window.map)
	{
	    circles.setMap(window.map);
	    circleList = circleList.concat(circles);
	}
    }

    Map.prototype.clearPoly = function (cat) {
	if (window.map)
	{
	    if (cate == -1)
		cate = cat
	    else if (cate != cat)
	    {
		for (var n = 0; n < circleList.length; n++)
		    circleList[n].setMap(null);
		circleList = [];
		cate = cat;
	    }
	}
    }
    
    return Map;
});

angular.module('edison').factory('mapAutocomplete', ["$q", "Address", function($q, Address) {
        "use strict";
        var autocomplete = function() {
            this.service = new google.maps.places.AutocompleteService();
            this.geocoder = new google.maps.Geocoder();
        }

        autocomplete.prototype.search = function(input) {
            var deferred = $q.defer();
            this.service.getPlacePredictions({
                input: input,
                componentRestrictions: {
                    country: 'fr'
                }
            }, function(predictions) {
                if (predictions)
                    predictions.forEach(function(e) {
                        if (e.description === input)
                            predictions = null;
                    })
                deferred.resolve(predictions || []);
            });
            return deferred.promise;
        }

        autocomplete.prototype.getPlaceAddress = function(place) {
            var self = this;
            return $q(function(resolve, reject) {
                self.geocoder.geocode({
                    'address': place.description
                }, function(result, status) {
                    if (status !== google.maps.places.PlacesServiceStatus.OK)
                        return reject(status);
                    return resolve(Address(result[0]));
                });

            });
        };

        return new autocomplete();

    }
]);

angular.module('edison').factory('openPost', function() {
    "use strict";
    return function(url, data) {
        var mapForm = document.createElement("form");
        mapForm.target = "_blank";
        mapForm.method = "POST";
        mapForm.action = url;

        // Create an input
        _.each(data, function(e, i) {
                var mapInput = document.createElement("input");
                mapInput.type = "text";
                mapInput.name = i;
                mapInput.value = typeof e == 'object' ? JSON.stringify(e) : e;
                mapForm.appendChild(mapInput);
            })
            // Add the form to dom
        document.body.appendChild(mapForm);

        // Just submit
        mapForm.submit();
        mapForm.remove();
    }
});

angular.module('edison').factory('productsList', ["$q", "dialog", "openPost", "edisonAPI", function($q, dialog, openPost, edisonAPI) {
    "use strict";
    var Produit = function(produits) {
        this.produits = produits;
        var _this = this
        if (!this.ps) {
            edisonAPI.product.list().then(function(resp) {
                _this.ps = resp.data
            })
        }
        _.each(this.produits, function(e) {
            if (e.desc.toLowerCase() != e.title.toLowerCase()) {
                e.showDesc = true
            }
        })
        this.lastCall = _.now()
    }
    Produit.prototype = {
        remove: function(index) {
            this.produits.splice(index, 1);
        },
        moveTop: function(index) {
            if (index !== 0) {
                var tmp = this.produits[index - 1];
                this.produits[index - 1] = this.produits[index];
                this.produits[index] = tmp;
            }

        },
        moveDown: function(index) {
            if (index !== this.produits.length - 1) {
                var tmp = this.produits[index + 1];
                this.produits[index + 1] = this.produits[index];
                this.produits[index] = tmp;
            }
        },
        edit: function(index) {
            var _this = this;
            dialog.editProduct.open(this.produits[index], function(res) {
                _this.produits[index] = res;
            })
        },
        add: function(prod) {
            if (this.lastCall + 100 > _.now())
                return 0
            this.lastCall = _.now()
            this.searchText = '';
            prod.quantite = 1;
            this.produits.push(prod);
        },
        search: function(text) {
            var rtn = []
            for (var i = 0; i < this.ps.length; ++i) {
                if (text === this.ps[i].title)
                    return [];
                var needle = _.deburr(text).toLowerCase()

                var haystack = _.deburr(this.ps[i].title).toLowerCase();
                var haystack2 = _.deburr(this.ps[i].ref).toLowerCase();
                var haystack3 = _.deburr(this.ps[i].desc).toLowerCase();
                if (_.includes(haystack, needle) ||
                    _.includes(haystack2, needle) ||
                    _.includes(haystack3, needle)) {
                    var x = _.clone(this.ps[i])
                    x.random = _.random();
                    rtn.push(x)
                }
            }
            return rtn
        },
        /*        search: function(text) {
                    var deferred = $q.defer();
                    edisonAPI.searchProduct(text)
                        .success(function(resp) {
                            deferred.resolve(resp)
                        })
                    return deferred.promise;
                },*/
        getDetail: function(elem) {
            if (!elem)
                return 0
            var _this = this;
            dialog.selectSubProduct(elem, function(resp) {
                var rtn = {
                    showDesc: true,
                    quantite: 1,
                    ref: resp.ref,
                    title: elem.nom,
                    desc: resp.nom + '\n' + elem.description.split('-').join('\n'),
                    pu: Number(resp.prix)
                }
                _this.add(rtn)
            })
        },
        flagship: function() {
            return _.mapBy(this.produits, 'pu');
        },
        total: function() {
            var total = _.round(_.sum(this.produits, function(e)  {
                return (e.pu || 0) * (e.quantite || 0);
            }), 2)
            return total;
        },

    }

    return Produit;


}]);

angular.module('edison').factory('socket', ["socketFactory", function(socketFactory) {
	"use strict";
	return socketFactory({
		ioSocket: io.connect(location.protocol + "//" + location.hostname + ':1995')
	});
}]);

angular.module('edison').factory('taskList', ["dialog", "edisonAPI", function(dialog, edisonAPI) {
    "use strict";
    var Task = function(user) {
        edisonAPI.get({
            to: user,
            done: false
        })
    }
    Task.prototype = {
        check: function(_id) {
            edisonAPI.check({
                _id: _id
            })
        },
        add: function(params) {
            edisonAPI.add(params)

        }
    }
    return Task;


}]);

angular.module('edison').factory('user', ["$window", function($window) {
    "use strict";
    return $window.app_session;
}]);

angular.module("edison").filter('contactFilter', ["config", function(config) {
    "use strict";

    var clean = function(str) {
        return _.deburr(str).toLowerCase();
    }

    var compare = function(a, b, strictMode) {
        if (typeof a === "string") {
            return clean(a).includes(b);
        } else if (!strictMode) {
            return clean(String(a)).startsWith(b);
        } else {
            return a === parseInt(b);
        }
    }
    return function(dataContainer, input) {
        var rtn = [];
        input = clean(input);
        _.each(dataContainer, function(data) {
            if (!data.stringify)
                data.stringify = clean(JSON.stringify(data))
            if (!input || data.stringify.indexOf(input) >= 0) {
                rtn.push(data);
            } else {
            }
        })
        return rtn;
    }
}]);

angular.module('edison').filter('crlf', function() {
	"use strict";
    return function(text) {
        return text.split(/\n/g).join('<br>');
    };
});

 angular.module('edison').filter('frnbr', function() {
 	"use strict";
 	return function(num) {
 		var n = _.round((num || 0), 2).toString(),
 			p = n.indexOf('.');
 		return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function($0, i) {
 			return (p < 0 || i < p ? ($0 + ' ') : $0).replace('.', ',');
 		});
 	};
 });

angular.module('edison').filter('loginify', function() {
    "use strict";
    return function(obj) {
        if (!obj)
            return "";
        return obj.slice(0, 1).toUpperCase() + obj.slice(1, -2)
    };
});

angular.module('edison').filter('relativeDate', function() {
    "use strict";
    return function(date, smallWin) {
        var d = moment((date + 137000000) * 10000);
        var l = moment().subtract(4, 'days');
        if (d < l) {
            return smallWin ? d.format('DD/MM') : d.format('DD/MM/YY')
        } else {
            var x = d.fromNow().toString()
            if (smallWin) {
                x = x
                    .replace('quelques secondes', '')
                    .replace(' minutes', 'mn')
                    .replace(' minute', 'mn')
                    .replace(' une', '1')
                    .replace(' heures', 'H')
                    .replace(' heure', 'H')
                    .replace(' jours', 'J')
                    .replace(' jour', 'J')
                    .replace('il y a', '-')
                    .replace(' un', '1')
                    .replace('dans ', '+')
            }
            return x;
        }
        // return moment((date + 1370000000) * 1000).fromNow(no).toString()
    };
});

angular.module('edison').filter('reverse', function() {
    "use strict";
    return function(items) {
        if (!items)
            return [];
        return items.slice().reverse();
    };
});

angular.module("edison").filter('tableFilter', ["config", function(config) {
    "use strict";

    var clean = function(str) {
        return _.deburr(str).toLowerCase();
    }

    var compare = function(a, b, strictMode) {
        if (typeof a === "string") {
            return clean(a).includes(b);
        } else if (!strictMode) {
			var check = 0;
			var bSplit = b.split('+');
			if (b.includes('+') && isValid)
			{
				for (var ind = 0; ind < bSplit.length; ind++)
				{
					if (clean(a).includes(bSplit[ind]))
						check = 1;
				}
			}
			if (check == 1)
				return true;
			else if (clean(String(a)).startsWith(b))
				return true;
			else
				return false;
        } else {
            return a === parseInt(b);
        }
    }
    var compareCustom = function(key, data, input) {
        if (key === '_categorie') {
            var cell = config.categoriesHash()[data.c].long_name;
            return compare(cell, input);
        }
	if (key === '_catego') {
            var cell = config.categoriesHash()[data.cate].long_name;
            return compare(cell, input);
        }
        if (key === '_etat') {
            var cell = config.etatsHash()[data.s].long_name
            return compare(cell, input);
        }
        return true;

    }
    var compareDate = function(key, data, input) {
        var md = (data[key] + 137000000) * 10000;
        if (md > input.start.getTime() && md < input.end.getTime()) {
            return true
        }
        return false;
    }

    var parseDate = function(e) {
        if (!(/^[0-9\/]+$/).test(e) ||  _.endsWith(e, '/')) {
            return undefined;
        }
        var x = e.split('/');
        if (x.length === 1) {
            var month = parseInt(x[0]);
            var year = new Date().getFullYear();
            return {
                start: new Date(year, month - 1),
                end: new Date(year, month)
            }
        } else if (x.length === 2)  {

            if (x[1].length == 4) {
                var month = parseInt(x[0]);
                var year = parseInt(x[1]);
                return {
                    start: new Date(year, month - 1),
                    end: new Date(year, month),
                }
            }

            var day = parseInt(x[0]);
            var month = parseInt(x[1]);
            var year = new Date().getFullYear();
            return {
                start: new Date(year, month - 1, day),
                end: new Date(year, month - 1, day + 1)
            }
        }
        return undefined;
    }


    return function(dataContainer, inputs, strictMode) {
        var rtn = [];
        //console.time('fltr')
        inputs = _.mapValues(inputs, clean);
        _.each(inputs, function(e, k) {
            if (k.charAt(0) === '∆') {
                inputs[k] = parseDate(e);
            }
        })

        _.each(dataContainer, function(data) {
                if (data.id) {
                    var psh = true;
                    _.each(inputs, function(input, k) {
                        if (input && _.size(input) > 0) {
                            if (k.charAt(0) === '_') {
                                if (!compareCustom(k, data, input)) {
                                    psh = false;
                                    return false
                                }
                            } else if (k.charAt(0) === '∆') {
                                if (!compareDate(k.slice(1), data, input)) {
                                    psh = false;
                                    return false
                                }
                            } else {
                                if (!compare(data[k], input, strictMode)) {
                                    psh = false;
                                    return false
                                }
                            }
                        }
                    });
                    if (psh === true) {
                        rtn.push(data);
                    }
                }
            })
            //console.timeEnd('fltr')

        return rtn;
    }
}]);

 angular.module('edison').directive('infoAppelSst', ["mapAutocomplete", "edisonAPI", "config", function(mapAutocomplete, edisonAPI, config) {
    "use strict";
    return {
        restrict: 'E',
        templateUrl: '/Templates/info-appel-sst.html',
        scope: {
            data: "=",
        },
        link: function(scope, element, attrs) {
            scope.embedded = !!attrs.embedded
        },
    }

 }]);

 angular.module('edison').directive('infoConversation', ["mapAutocomplete", "edisonAPI", "config", function(mapAutocomplete, edisonAPI, config) {
    "use strict";
    return {
        restrict: 'E',
        templateUrl: '/Templates/info-conversation.html',
        scope: {
            data: "=",
        },
        link: function(scope, element, attrs) {
            scope.embedded = !!attrs.embedded
        },
    }

 }]);

 angular.module('edison').directive('infoFacture', ["mapAutocomplete", "edisonAPI", "config", function(mapAutocomplete, edisonAPI,config) {
     "use strict";
     return {
         restrict: 'E',
         templateUrl: '/Templates/info-facture.html',
         scope: {
             data: "=",
         },
         link: function(scope, element, attrs) {
             var model = scope.data;
             scope.config = config;
             scope.autocomplete = mapAutocomplete;
             scope.changeAddressFacture = function(place) {
                 mapAutocomplete.getPlaceAddress(place).then(function(addr) {
                     scope.data.facture = scope.data.facture ||  {}
                     scope.data.facture.address = addr;
                 });
             }
             edisonAPI.compte.list().then(function(resp) {
                 scope.grndComptes = resp.data
             })

	     	scope.comptech = function () {
		 		scope.data.facture.compte = undefined;
	     	}
			scope.changeMailed = function(mailed) {
				scope.data.facture.mailed = mailed;
			}
            scope.changePapier = function(papier) {
                scope.data.facture.papier = papier;
            }
            scope.changeGrandCompte = function() {
                 // var x = _.clone(config.compteFacturation[scope.data.facture.compte])
                 var x  = scope.data.facture.compte
                 scope.data.facture = _.find(scope.grndComptes, 'ref', scope.data.facture.compte);
                 scope.data.facture.payeur = "GRN";
                 scope.data.facture.compte = x;
             }
         },
     }

 }]);

angular.module('edison').directive('infoFourniture', ["config", "fourniture", "edisonAPI", "user", function(config, fourniture, edisonAPI, user) {
        "use strict";
        return {
            restrict: 'E',
            templateUrl: '/Templates/info-fourniture.html',
            scope: {
                data: "=",
                display: "=",
                small:"="
            },
            link: function(scope, element, attrs) {
		scope.isAuto = false;
		if (scope.data.artisan)
		{
		    edisonAPI.artisan.get(scope.data.artisan.id).then(function (resp) {
			if (resp.data.formeJuridique == "AUT")
			    scope.isAuto = true;
		    })
		}
		
		scope.validefourni = function (){
		    var dateStr = new Date();
		    var dArr = [];
		    dArr = _.words(dateStr), /[^- ]+/;
		    dArr[1] = dateStr.getMonth() + 1;
		    var datev = dArr[2] + "/" + dArr[1] + "/" + dArr[3];
		    var ids = scope.data._id
		    edisonAPI.intervention.validatefourn({id: ids, login: user.login, date: datev}).then(function (resp){
			scope.data.fourndate = resp.data.fourndate
			scope.data.fournpar = resp.data.fournpar
			scope.datefour = resp.data.fourndate
			scope.parfour = resp.data.fournpar
		    })
		}
		scope.parfour = scope.data.fournpar
		scope.datefour = scope.data.fourndate
                scope.config = config
                scope.dsp = scope.display || false
                scope.data.fourniture = scope.data.fourniture || [];
                scope.fourniture = fourniture.init(scope.data.fourniture);
            },
        }

    }
]);

angular.module('edison').directive('infoOvh', ["mapAutocomplete", "edisonAPI", "config", "$sce", function(mapAutocomplete, edisonAPI, config, $sce) {
    "use strict";
    return {
        restrict: 'E',
        templateUrl: '/Templates/info-ovh.html',
        scope: {
            data: "=",
        },
        link: function(scope, element, attrs) {
            scope.embedded = !!attrs.embedded

	    scope.trustUrl = function (url) {
		return $sce.trustAsResourceUrl(url);
	    }
        },
    }

 }]);

angular.module('edison').directive('mainNavbar', ["$q", "edisonAPI", "TabContainer", "$timeout", "$rootScope", "$location", "$window", "$document", function($q, edisonAPI, TabContainer, $timeout, $rootScope, $location, $window, $document) {
    "use strict";
    return {
        restrict: 'E',
        templateUrl: '/Templates/main-navbar.html',
        scope: {
            data: "=",
            display: "=",
            small: "="
        },
        link: function(scope, element, attrs) {
            scope.root = $rootScope;
            scope._ = _;
            scope.tabContainer = TabContainer;

            scope.select = function(model) {
                    if (scope.selectedTab == model) {
                        scope.selectedTab = null
                    } else {
                        scope.selectedTab = model
                    }
                }
                /*            $('input[type="search"]').ready(function() {
                                $timeout(function() {
                                    $('input[type="search"]').on('keyup', function(e, w) {
                                        if (e.which == 13) {
                                            if ($('ul.md-autocomplete-suggestions>li').length) {
                                                $location.url('/search/' + $(this).val())
                                                $(this).val("")
                                                $(this).blur()
                                            }
                                        }
                                    });
                                }, 10);
                            })
                */
            $rootScope.$on('closeContextMenu', function() {
                scope.selectedTab = null;
            })


            scope.search = function(text) {
                if (text.length > 2) {
                    $location.url('/search/' + text)
		    scope.collapseNav = true;
                }
            }

            scope.logout = function() {
                    edisonAPI.users.logout().then(function() {
                        $window.location.reload()
                    })
                }
                /*

                                                            $rootScope.$on('closeSearchBar', function() {
                                                                scope.searchBarSize = 100
                                                            })
                                                
                                            var searchInput = 'md-autocomplete.searchBar>md-autocomplete-wrap>input'
                                            $(searchInput).ready(function() {
                                                $timeout(function() {
                                                    $(searchInput).on('focus', function() {
                                                        scope.searchFocus = true
                                                        var selectors = ['.navbar-header', '.navbar-nav', '.dropdown-toggle.user-menu']
                                                        scope.searchBarSize = _.reduce(selectors, function(total, el) {
                                                            return total -= $(el).width();
                                                        }, $(window).width() - 70)
                                                    })
                                                    $(searchInput).on('blur', function() {
                                                        scope.searchFocus = false
                                                        scope.searchBarSize = 100
                                                    })
                                                }, 10);
                                            })
                */
            scope.changeUser = function(usr) {
                    $rootScope.displayUser = usr
                }
                /*
                            scope.searchBox = {
                                search: _.throttle(function(x) {
                                    var deferred = $q.defer();
                                    edisonAPI.searchText(x, {
                                        limit: 10,
                                        flat: true
                                    }).success(function(resp) {
                                        deferred.resolve(resp)
                                    })
                                    return deferred.promise;
                                }, 600),
                                change: function(x) {
                                    if (!x ||  !x.link)
                                        return 0;
                                    if (x) {
                                        $location.url(x.link)
                                    }
                                    $timeout(function() {
                                        $(searchInput).blur();
                                    });
                                    scope.searchText = "";
                                }
                            }*/


        },

    }

}]);

var archiveReglementController = function(edisonAPI, TabContainer, $routeParams, $location, LxProgressService) {

    var tab = TabContainer.getCurrentTab();
    var _this = this;
    _this.type = 'reglement';
    _this.title = 'Archives Reglements'
    tab.setTitle('archives RGL')
    LxProgressService.circular.show('#5fa2db', '#globalProgress');
    edisonAPI.compta.archivesReglement().success(function(resp) {
        LxProgressService.circular.hide()
        _this.data = resp
    })
    _this.moment = moment;
    _this.openLink = function(link) {
        $location.url(link)
    }
}
archiveReglementController.$inject = ["edisonAPI", "TabContainer", "$routeParams", "$location", "LxProgressService"];

angular.module('edison').controller('archivesReglementController', archiveReglementController);

var archivesPaiementController = function(edisonAPI, TabContainer, $routeParams, $location, LxProgressService) {
    var _this = this;
    var tab = TabContainer.getCurrentTab();
    _this.type = 'paiement'
    _this.title = 'Archives Paiements'
    tab.setTitle('archives PAY')
    LxProgressService.circular.show('#5fa2db', '#globalProgress');
    edisonAPI.compta.archivesPaiement().success(function(resp) {
	LxProgressService.circular.hide()
        _this.data = resp
    })
    _this.moment = moment;
    _this.openLink = function(link) {
        $location.url(link)
    }
}
archivesPaiementController.$inject = ["edisonAPI", "TabContainer", "$routeParams", "$location", "LxProgressService"];

angular.module('edison').controller('archivesPaiementController', archivesPaiementController);

var remiseChequesController = function(edisonAPI, TabContainer, $filter, $routeParams, $location, LxProgressService, LxNotificationService) {
    var _this = this;
    _this.type = 'remise';
    _this.title = 'Remise de Cheques';
    edisonAPI.compta.listRemises().then(function(resp) {
        _this.data = $filter('orderBy')(resp.data, 'dateRemise');
    })
    _this.moment = moment;
    _this.addCheques = function() {
        edisonAPI.compta.addCheques().then(function(resp) {
            if (resp.data == 'OK') {
                edisonAPI.compta.listRemises().then(function(resp) {
                    _this.data = $filter('orderBy')(resp.data, 'dateRemise');
                })
            } else {
                LxNotificationService.error("Une erreur est survenue");
            }
        })
    }
    _this.validateRemise = function() {
        edisonAPI.compta.validateRemise().then(function(resp) {
            if (resp.data == 'OK') {
                edisonAPI.compta.listRemises().then(function(resp) {
                    _this.data = $filter('orderBy')(resp.data, 'dateRemise');
                })
            } else {
                LxNotificationService.error("Une erreur est survenue");
            }
        })
    }
}
remiseChequesController.$inject = ["edisonAPI", "TabContainer", "$filter", "$routeParams", "$location", "LxProgressService", "LxNotificationService"];

angular.module('edison').controller('remiseChequesController', remiseChequesController);

 angular.module('edison').directive('artisanCategorie', ["config", function(config) {
     "use strict";
     return {
         replace: true,
         restrict: 'E',
         templateUrl: '/Templates/artisan-categorie.html',
         transclude: true,
         scope: {
             model: "=",
         },
         link: function(scope, element, attrs) {
             scope.config = config
             scope.findColor = function(categorie) {
                 var f = _.find(scope.model.categories, function(e)  {
                     return e === categorie.short_name
                 })
                 return f ? categorie.color : "white";

             }
             scope.toggleCategorie = function(categorie) {
                var f = scope.model.categories.indexOf(categorie);
                if (f >= 0) {
                    scope.model.categories.splice(f, 1);
                } else {
                    scope.model.categories.push(categorie)
                }
             }
         },
     }

 }]);

var ArtisanCtrl = function(IBAN, $timeout, $rootScope, $scope, edisonAPI, $location, $routeParams, ContextMenu, LxProgressService, LxNotificationService, TabContainer, config, dialog, artisanPrm, Artisan, user) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.dialog = dialog;
    _this.moment = moment;
    _this.user = $rootScope.user;
    _this.contextMenu = new ContextMenu('artisan')
    var tab = TabContainer.getCurrentTab();
    if (!tab.data) {
        var artisan = new Artisan(artisanPrm.data)
        tab.setData(artisan);
        if ($routeParams.id.length > 12) {
            _this.isNew = true;
            artisan.tmpID = $routeParams.id;
            tab.setTitle('SST  ' + moment((new Date(parseInt(artisan.tmpID))).toISOString()).format("HH:mm").toString());
        } else {
            tab.setTitle('SST  ' + $routeParams.id);
            if (!artisan) {
                LxNotificationService.error("Impossible de trouver les informations !");
                $location.url("/dashboard");
                TabContainer.remove(tab);
                return 0;
            }
        }
    } else {
        var artisan = tab.data;
    }
    _this.data = tab.data;
    var star = tab.data.star;
    var one = tab.data.oneShot;
    var acp = tab.data.address.cp;
    var ar = tab.data.address.r;
    var av = tab.data.address.v;
    var tel1 = tab.data.telephone.tel1
    var tel2 = tab.data.telephone.tel2
    var mail = tab.data.email
    var nom = tab.data.representant.nom
    var IBAN = tab.data.IBAN
    var nomS = tab.data.nomSociete
    var an = tab.data.address.n;    
    var cat = [];
    var doc = []
    for(var index in tab.data.document) {
	doc.push(tab.data.document[index].ok);
    }
    for (var n = 0; n < _this.data.categories.length; n++)
	   cat.push(_this.data.categories[n]);
    var pm = tab.data.pourcentage.maindOeuvre;
    _this.saveArtisan = function(options) {
        // artisan = _this.data;
        // edisonAPI.artisan.checkExist({ email: artisan.email, tel: artisan.telephone.tel1 }).then(function(check) {
        //     console.log("------>", check);
        //     if (check.data == "NO")
        //     {
        //         console.log("IN NO");
        //         LxNotificationService.error("Erreur: L'email ou le téléphone existe déjà.");
        //     }
        //     else if (check.data == "YES")
        //     {
        	    artisan.save(function(err, resp) {
                    if (err)
                    {
                        return false
                    }
                    else if (options.contrat)
                    {
                        artisan = new Artisan(resp);
                        artisan.envoiContrat.bind(resp)(options, function(err, res) {
                            if (!err) {
                                TabContainer.close(tab);
                            }
                        });
                    }
                    else
                    {
                		if (nom != _this.data.representant.nom)
                		    edisonAPI.artisan.activite(_this.data._id, "Le nom de société a été changé de " + nom + " en " + _this.data.representant.nom)
                		if (IBAN != _this.data.IBAN)
                		{
                		    var messIBAN = "Le numero d' IBAN a changé de " + IBAN + " en " + _this.data.IBAN
                		    if (IBAN === undefined)
                			var messIBAN = "Ajout d'un numero d' IBAN: " + _this.data.IBAN;
                		    edisonAPI.artisan.activite(_this.data._id, messIBAN)
                		}
                		if (nomS != _this.data.nomSociete)
                		    edisonAPI.artisan.activite(_this.data._id, "Le nom de société a été changé de " + nomS + " en " + _this.data.nomSociete)
                		if (mail != _this.data.email)
                		    edisonAPI.artisan.activite(_this.data._id, "Le mail  a changé de " + mail + " en " + _this.data.email)
                		if (tel1 != _this.data.telephone.tel1)
                		    edisonAPI.artisan.activite(_this.data._id, "Le numero de téléphone n°1 a changé de " + tel1 + " en " + _this.data.telephone.tel1)
                		if (tel2 != _this.data.telephone.tel2)
                		{
                		    var messtel = "Le numero de téléphone n°2 a changé de " + tel2 + " en " + _this.data.telephone.tel2
                		    if (tel2 === undefined)
                			var messtel = "Ajout d'un numero de telephone n°2: " + _this.data.telephone.tel2;
                		    edisonAPI.artisan.activite(_this.data._id, messtel)
                		}
                		if (pm != _this.data.pourcentage.maindOeuvre)
                		{
                		    var sent = "Le pourcentage de main d'oeuvre a été changé de " + pm + " à " + _this.data.pourcentage.maindOeuvre
                		    edisonAPI.artisan.activite(_this.data._id, sent)
                		}
                		if (one != _this.data.oneShot)
                		{
                		    if (_this.data.oneShot === false)
                			var mess = "L' artisan n' a plus le statut OneShot"
                		    else
                			var mess = "L'artisan a le statut OneShot"
                		 
                		    edisonAPI.artisan.activite(_this.data._id, mess)
                		}
                		if (star != _this.data.star)
                		{
                		 
                		    if (_this.data.star === false)
                        		var mes = "L'artisan n' a plus le statut Star";
                        	else
                        		var mes = "L'artisan a le statut Star";
                		    edisonAPI.artisan.activite(_this.data._id, mes)
                		}
                		if (acp != _this.data.address.cp || ar != _this.data.address.r || av != _this.data.address.v || an != _this.data.address.n)
                		{
                		    var sent = "L' adresse a été changé de \n" + an + " " + ar + " " + av + " " + acp + " en \n" +  _this.data.address.n + " " + _this.data.address.r + " " + _this.data.address.v + " " + _this.data.address.cp; 
                		    edisonAPI.artisan.activite(_this.data._id, sent)
                		}
                		var doc2 = []
                        var check = []
                        if (_this.data.document)
                        {
                            var documents = Object.keys(_this.data.document)
                            for(var index in _this.data.document) {
                                doc2.push(tab.data.document[index].ok);
                            }
                            for (var d = 0;doc.length > d; d++)
                            {
                                if (doc[d] != doc2[d])
                                {
                                    if (doc2[d])
                                        edisonAPI.artisan.activite(_this.data._id, documents[d] + " a été mis")
                                    else
                                        edisonAPI.artisan.activite(_this.data._id, documents[d] + " a été enlevé")
                                }
                            }
                        }
                		for (var t = 0;cat.length > t;t++)
                		{
                		    if(_this.data.categories.indexOf(cat[t]) === -1)
                		    {
                			 edisonAPI.artisan.activite(_this.data._id,_this.config.categories[cat[t]].long_name  + " a été enlevé")
                		    }
                		    else
                		    {
                			 check.push(cat[t])
                		    }
                		}
                		
                		for (var y = 0; _this.data.categories.length > y;y++)
                		{
                		    if (check.indexOf(_this.data.categories[y]) === -1)
                		    {
                			edisonAPI.artisan.activite(_this.data._id,_this.config.categories[_this.data.categories[y]].long_name  + " a été ajouté")
                		    }
                		}
                        TabContainer.close(tab);
                    }
                })
        //     }
        // })
    }

    _this.checkConflict = function(check) {
	if (check === 'one' && _this.data.star === true)
	    _this.data.star = false;
	if (check === 'star' && _this.data.oneShot === true)
	    _this.data.oneShot = false;
    }
    
    _this.onArtisanFileUpload = function(file, name) {
        LxProgressService.circular.show('#5fa2db', '#fileUploadProgress');
        edisonAPI.artisan.upload(file, name, artisan.id).then(function() {
            LxProgressService.circular.hide()
            _this.loadFilesList();
        })
    }

    _this.artisanClickTrigger = function(elem) {
        setTimeout(function() {
            angular.element("#file_" + elem + ">input").trigger('click');
        }, 0)

    }
    _this.rightClick = function($event) {
        _this.contextMenu.setPosition($event.pageX, $event.pageY)
        _this.contextMenu.setData(artisan);
        _this.contextMenu.open();
    }

    _this.validIBAN = function(iban) {
        return !iban || IBAN.isValid(iban);
    }

    _this.fileExist = function(name) {
        if (!artisan.file)
            return false;
        return _.find(artisan.file, function(e) {
            return _.startsWith(e, name)
        });
    }
    _this.smart = [{
	label:"Smartphone"
    },{
	label:"Mobile"
    },{
	label:"Non demandé"
    }]
    
    if (!_this.data.smartphone)
    {
	_this.data.smartphone = "Non demandé"
    }
    _this.sendIns = function () {
	var opt = {}
	opt.to = _this.data.email;
	opt.mail = "instructionLydia.html"
	opt.obj = "Instruction Lydia"
	var id = _this.data.id
	edisonAPI.artisan.sendIns(id, opt)
	LxNotificationService.success("Les instructions Lydia ont bien été envoyés");
    }
    _this.loadFilesList = function() {
        edisonAPI.artisan.getFiles(artisan.id).then(function(result) {
            artisan.file = result.data;
        })
    }
    if (artisan.id) {
        _this.loadFilesList();
    }
    _this.checklimit = function (){
		edisonAPI.intervention.checklimit({id: _this.data._id}).then(function(resp)
    		{
	       		_this.seuilResp = Math.round(parseFloat(resp.data));
		})
	}

	_this.checklimitRupture = function () {
		edisonAPI.intervention.checklimit({id: _this.data._id}).then(function(resp)
	    	{
        		_this.seuilRupt = Math.round(parseFloat(resp.data));
		})
	}
    
    _this.addComment = function() {

        artisan.comments.push({
            login: $rootScope.user.login,
            text: _this.commentText,
            date: new Date()
        })
        if (artisan.id) {
            edisonAPI.artisan.comment(artisan.id, _this.commentText)
        }
        _this.commentText = "";
    }
    var updateTmpArtisan = _.after(5, _.throttle(function() {
        edisonAPI.artisan.saveTmp(artisan);

	
    }, 30000))

    if (!artisan.id) {
        $scope.$watch(function() {
            return artisan;
        }, updateTmpArtisan, true)
    }
}
ArtisanCtrl.$inject = ["IBAN", "$timeout", "$rootScope", "$scope", "edisonAPI", "$location", "$routeParams", "ContextMenu", "LxProgressService", "LxNotificationService", "TabContainer", "config", "dialog", "artisanPrm", "Artisan", "user"];
angular.module('edison').controller('ArtisanController', ArtisanCtrl);

var AvoirsController = function(TabContainer, openPost, edisonAPI, $rootScope, LxProgressService, LxNotificationService, FlushList) {
    "use strict";
    var _this = this
    var tab = TabContainer.getCurrentTab();
    _this.offsetX = 1;
    _this.offsetY = -5;
    tab.setTitle('Avoirs')
    _this.loadData = function(prevChecked) {
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        edisonAPI.compta.avoirs().then(function(result) {
            $rootScope.avoirs = result.data
            LxProgressService.circular.hide()
        })
    }
    if (!$rootScope.avoirs)
        _this.loadData()

    _this.reloadAvoir = function() {
        _this.loadData()
    }

    _this.print = function(type) {
        openPost('/api/intervention/printAvoir', {
            data: $rootScope.avoirs
        });
    }

    _this.printChq = function(type) {
        var checked = [];
        var check = 0;
        for (var ind = 0; ind < $rootScope.avoirs.length; ind++)
        {
            if ($rootScope.avoirs[ind].checked === true)
            {
                check++;
                checked.push($rootScope.avoirs[ind]);
            }
        }
        if (check >= 1)
        {
            openPost('/api/intervention/printAvoirChq', {
                data: checked,
                offsetX: _this.offsetX || 0,
                offsetY: _this.offsetY || 0
            });
        }
        else
        {
            openPost('/api/intervention/printAvoirChq', {
                data: $rootScope.avoirs,
                offsetX: _this.offsetX || 0,
                offsetY: _this.offsetY || 0
            });
        }
    }

    _this.flush = function() {
        var list = _.filter($rootScope.avoirs, {
            checked: true
        })
        edisonAPI.compta.flushAvoirs(list).then(function(resp) {
            LxNotificationService.success(resp.data);
            _this.reloadAvoir()
        }).catch(function(err) {
            LxNotificationService.error(err.data);
        })
    }

}
AvoirsController.$inject = ["TabContainer", "openPost", "edisonAPI", "$rootScope", "LxProgressService", "LxNotificationService", "FlushList"];


angular.module('edison').controller('avoirsController', AvoirsController);

var ContactArtisanController = function($scope, $window, $timeout, TabContainer, LxNotificationService, LxProgressService, FiltersFactory, ContextMenu, edisonAPI, DataProvider, $routeParams, $location, $q, $rootScope, $filter, config, ngTableParams) {
    "use strict";
    var _this = this;

    _this.callSst = function(tel, id)
    {
        if (tel) {
            $window.open('callto:' + tel, '_self', false);
        }
        edisonAPI.artisan.callLog(id).then(function (resp) {
            var message = _.template("L'appel a été ajouté à l'historique.")(resp.data)
            LxNotificationService.success(message);
        }, function (error) {
            LxNotificationService.error(error.data);
        });
    }

    _this.loadPanel = function(id) {
        edisonAPI.artisan.get(id)
            .then(function(resp) {
                _this.sst = resp.data;
                _this.tab.setTitle('@' + _this.sst.nomSociete.slice(0, 10));

            })
        edisonAPI.artisan.getStats(id).then(function(resp) {
            new Chartist.Pie('.ct-chart', {
                series: [{
                    value: resp.data.envoye.total,
                    name: 'En cours',
                    className: 'ct-orange',
                    meta: 'Meta One'
                }, {
                    value: resp.data.annule.total,
                    name: 'annulé',
                    className: 'ct-red',
                    meta: 'Meta One'
                }, {
                    value: resp.data.paye.total,
                    name: 'payé',
                    className: 'ct-green',
                    meta: 'Meta One'
                }]
            }, {
                total: resp.data.annule.total + resp.data.paye.total + resp.data.envoye.total,
                donut: true,
                startAngle: 270,
                donutWidth: 62,
            });
            _this.stats = resp.data
        })

    }

    _this.reloadStats = function() {
        edisonAPI.artisan.statsMonths($routeParams.sstid).then(function(resp) {
            var series = ['Annulé', 'Payé'];
            var labels = []
            var data = [
                [],
                []
            ];
            _.each(resp.data, function(e) {
                labels.push(_.capitalize(moment([e.year, e.month - 1]).format('MMMM YYYY')))
                data[0].push(e.annule);
                data[1].push(e.paye);
            })
            _this.sstChart = {
                series: series,
                data: data,
                labels: labels,
                options: {
                    scaleBeginAtZero: true,
                },
                colours: [
                    '#F7464A', // red
                    '#46BFBD', // green

                ]
            }
        });
    }


    _this.tbz = ['informations', 'interventions', 'historique','activite' , 'signalement', 'stats', 'paiements'];
    var ind = _this.tbz.indexOf($location.hash());
    $scope.selectedIndex = ind >= 0 ? ind : 0
    _this.tab = TabContainer.getCurrentTab();

    _this.recap = $location.url().includes('recap') ? $routeParams.sstid : undefined

    if (_this.recap) {
        _this.loadPanel(_this.recap)
    } else {
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        var dataProvider = new DataProvider('artisan');
        dataProvider.init(function(err, resp) {
            _this.config = config;
            _this.moment = moment;
            if (!dataProvider.isInit()) {
                dataProvider.setData(resp);
            }
            _this.tableFilter = "";
            _this.tableLimit = 20;
            $rootScope.expendedRow = $routeParams.sstid || 45
                // if (_this.recap) {
                //     $scope.selectedIndex = 1;
                // }
            _this.tableData = dataProvider.getData()
            _this.loadPanel(_this.tableData[0].id)
            LxProgressService.circular.hide();
        });
    }

    _this.getStaticMap = function(address) {
        if (_this.sst && this.sst.address)
            return "/api/mapGetStatic?width=500&height=400&precision=0&zoom=6&origin=" + _this.sst.address.lt + ", " + _this.sst.address.lg;
    }

    _this.reloadData = function() {
        _this.tableData = $filter('contactFilter')(dataProvider.getData(), _this.tableFilter);
    }

    _this.loadMore = function() {
        _this.tableLimit += 10;
    }

    /*
        $rootScope.$watch('tableilter', _this.reloadData);
    */
    $rootScope.$on('ARTISAN_CACHE_LIST_CHANGE', function() {
        if (_this.tab.fullUrl === TabContainer.getCurrentTab().fullUrl) {
            dataProvider.applyFilter(currentFilter, _this.tab.hash);
        }
    })

    _this.contextMenu = new ContextMenu('artisan')

    _this.rowRightClick = function($event, inter) {
        _this.contextMenu.setPosition($event.pageX, $event.pageY + 200)
        edisonAPI.artisan.get(inter.id)
            .then(function(resp) {
                _this.contextMenu.setData(resp.data);
                _this.contextMenu.open();
            })
    }

    $scope.addComment = function() {
        edisonAPI.artisan.comment(_this.sst.id, $scope.commentText).then(function() {
            _this.loadPanel(_this.sst.id);
            $scope.commentText = "";
        })
    }

    _this.rowClick = function($event, inter) {
        if (_this.contextMenu.active)
            return _this.contextMenu.close();
        if ($event.metaKey || $event.ctrlKey) {
            TabContainer.addTab('/artisan/' + inter.id, {
                title: ('#' + inter.id),
                setFocus: false,
                allowDuplicates: false
            });
        } else {
            if ($rootScope.expendedRow === inter.id) {
                return 0;
            } else {
                $rootScope.expendedRow = inter.id
                _this.loadPanel(inter.id)
                $location.search('sstid', inter.id);
            }
        }
    }


    $scope.$watchCollection('[selectedIndex, expendedRow]', function(current, prev) {
        if (current && current[0] !== void(0)) {
            $location.hash(_this.tbz[current[0]]);
        }
        if (_this.tbz[current[0]] === 'stats') {
            _this.reloadStats();
        }
    })


}
ContactArtisanController.$inject = ["$scope", "$window", "$timeout", "TabContainer", "LxNotificationService", "LxProgressService", "FiltersFactory", "ContextMenu", "edisonAPI", "DataProvider", "$routeParams", "$location", "$q", "$rootScope", "$filter", "config", "ngTableParams"];
angular.module('edison').controller('ContactArtisanController', ContactArtisanController);

var DashboardController = function($rootScope, statsTelepro, dialog, user, edisonAPI, $scope, $filter, TabContainer, NgTableParams, $routeParams, $location, LxProgressService, MomentIterator, socket, edisonAPI, config) {
    var _this = this;
    $scope._ = _;
    $scope.root = $rootScope;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Stats');
    $scope.showAll = window.app_env === 'DEVELOPMENT'
    
    // Debut des Stats
    var isset = false;
    var end = new Date();
    var start = new Date(2013, 8, 1)
    _this.dateselects = MomentIterator(start, end).range('month').map(function(e) {
		return {
		    t: e.format('MMM YYYY'),
		    m: e.month() + 1,
		    y: e.year(),
		}
    }).reverse()
    _this.percstat = {};
    _this.percstats = {};
    _this.MtnMonth = "";
    _this.MtnWeek = "";
    _this.name = [];
    edisonAPI.user.loginuser().then(function(resp4){
	    for (var c = 0;c < resp4.data.length;c++)
		_this.name.push(resp4.data[c].login);
    })

    edisonAPI.devis.getInfo({login: user.login}).then(function(resp5) {
		_this.devisData = resp5.data;
    })
  
    var tmp = _this.dateselects[0];
    var dateTarget = {y: tmp.y, m: tmp.m};
    _this.yearSelect = MomentIterator(start, moment(end).add(1,'year')).range('year', {
		format: 'YYYY',
    }).map(function(e) {
		return parseInt(e)
    })
    $scope.selectedDate = _this.dateselects[0];
    $scope.selectedSubDate = _this.dateselects[0];
    $scope.selectedYear = _this.dateselects[0].y;
    
    var fetch = {
		"hour": {desc2: "Inters", desc1: "Devis"},
		"CA": {desc1: "€"},
		"nombre": {desc2: "Ajout", desc3: "Ann", desc1: "A Prog"}
    }
    
    var getSimpleChart = function(type, title, series, categories, data) {

		var min = 1;
		if (data === "hour")
		    min = 7
		var text = "Nombre";
		if (data === 'CA')
		    text = "Chiffre d'affaire"
		return {
		    chart: {
				zoomType: 'x',
				type: type
		    },
		    title: {
				text: title
		    },
		    xAxis: {
				min: min,
				categories: categories,
				tickInterval: 1,
		    },
		    yAxis: {
				min: 0,
				title: {
				    text: text
				},
		    },
		    tooltip: {
				shared: true,
				formatter: function () {
				    var ret = "";
				    if (data === "hour")
						ret = this.x + "h<br>"
				    else
						ret = "Jour " + this.x + "<br>"
				    for (var n = 0; n < this.points.length; n++)
				    {
						if (this.points[n])
						{
						    if (this.points[n].series.linkedSeries.length == 1 && this.points[n].color == "#FF0000")
							ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + " " + fetch[data].desc3 + "<br>";
						    else if (this.points[n].series.linkedSeries.length == 1)
							ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + " " + fetch[data].desc2 + "<br>";
						    else
							ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + " " + fetch[data].desc1 + "<br>";
						}
				    }
				    return ret;
				}
		    },
		    plotOptions: {
				animation: false,
				column: {
				    pointPadding: 0,
				    groupPadding: 0.04,
				    borderWidth: 0,
				    animation: false,
				},
				area: {
				    stacking: 'normal',
				    lineColor: '#666666',
				    lineWidth: 1,
				    marker: {
					lineWidth: 1,
					lineColor: '#666666'
				    }
				},
				areaspline: {
				    stacking: 'normal',
				}
		    },
		    series: series
		}
    }

    
    var getChart = function(type, title, series, categories) {

	return {
	    chart: {
			zoomType: 'x',
			type: type
	    },
	    title: {
			text: title
	    },
	    xAxis: {
		categories: categories
	    },
	    yAxis: {
		min: 0,
		max: 100,
		title: {
		    text: "Pourcentage"
		},
		plotLines: [{
		    value: $rootScope.user.limit,
		    color: 'green',
		    dashStyle: 'shortdash',
		    width: 2,
		}],
		tickInterval: 5,
	    },
	    tooltip: {
		shared: true,
		formatter: function () {
		    var ret = "";
		    if (parseInt(this.points[0].series.points.length) < 14)
		    {
				for (var n = 0; n < this.points.length; n++)
				{
				    if ($rootScope.user.admin || $scope.selectedDivider === "annulation")
				    {
						if (this.points[n])
						{
						    if (this.points[n].series.linkedSeries.length)
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%  " + _this.percstats[this.points[n].series.name].perc[this.points[n].x - 1] + "<br>";
						    else
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "% " + _this.percstats[this.points[n].series.name + "ann"].perc[this.points[n].x - 1] + "<br>";
						}
				    }
				    else
				    {
						if (this.points[n])
						{
						    if (this.points[n].series.linkedSeries.length)
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%" + "<br>";
						    else
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%" + "<br>";
						}
				    }	    
				}
		    }
		    else
		    {
				for (var n = 0; n < this.points.length; n++)
				{
				    if ($rootScope.user.root || $scope.selectedDivider === "annulation")
				    {
						if (this.points[n])
						{
						    if (this.points[n].series.linkedSeries.length)
						    {
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%  " + _this.percstat[this.points[n].series.name].perc[this.points[n].x - 1] + "<br><br>";
						    }
						    else
						    {
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%  " + _this.percstat[this.points[n].series.name + "ann"].perc[this.points[n].x - 1] + "<br>";
						    }
						}
				    }
				    else
				    {
						if (this.points[n])
						{
						    if (this.points[n].series.linkedSeries.length)
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%" + "<br>";
						    else
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%" + "<br>";
						}
				    }	    
				}
		    }
		    return ret; 
		}
	    },
	    plotOptions: {
		animation: false,
		column: {
		    pointPadding: 0,
		    groupPadding: 0.04,
		    borderWidth: 0,
		    animation: false,
		},
		area: {
		    stacking: 'normal',
		    lineColor: '#666666',
		    lineWidth: 1,
		    marker: {
			lineWidth: 1,
			lineColor: '#666666'
		    }
		},
		areaspline: {
		    stacking: 'normal',
		}
	    },
	    series: series
	}
    }
    if (user.priv === true)
    {
		_this.dividerSelect = [
		    'annulation',
		    'ajout'
		]
    }
    else
    {
		_this.dividerSelect = [
		    'chiffre',
		    'annulation',
		    'ajout'
		]
    }
    $scope.selectedDivider = 'annulation'
    if (!user.admin)	
		$scope.selectedDivider = 'chiffre'
    
    _this.typeSelect = [
	'column',
	'areaspline',
	'area',
	'line',
	'pie',
	'bar',
	'spline',
    ]
    $scope.selectedType = 'column'

    _this.subTypeSelect = [
    'Total CA',
	'CA',
	'nombre',
    ]
    $scope.selectedSubType = 'CA'

    var moveIt = function (elem)
    {
		for (var l = 0; l < elem.series.length; l++)
		{
		    elem.series[l].data.push(0);
		    var cpy;
		    var rep = 0;
		    for (var n = 0; n < elem.series[l].data.length; n++)
		    {
				cpy = elem.series[l].data[n]
				elem.series[l].data[n] = rep;
				rep = cpy;
		    }
		}
		return (elem);
    }

    var moveOneUp = function (elem, isArray)
    {
		if (isArray)
		{
		    for (var n = 0; n < elem.length; n++)
		    {
				elem[n] = moveIt(elem[n]);
		    }
		}
	/*	else
		    elem = moveIt(elem);*/
		return (elem);
    }

    var loadData = function (group)
    {
	edisonAPI.intervention.getHourlyStat(group).then(function(resp) {
	    var d = moveOneUp(resp.data, group.divider !== 'CA');
	    var serie = [];
	    if (!user.admin)
	    {
			if (group.divider != 'CA')
			{
			    for (var i = 0; i < d[0].series.length; i++)
			    {
				if (d[0].series[i].name === user.login || (d[0].series[i].obs === true && user.priv === true))
				{
				    if (d[0].series[i].name === user.login)
						_this.name = d[0].series[i].name;
				    serie.push(d[0].series[i], d[1].series[i]);
				    if (d.length === 3)
						serie.push(d[2].series[i]);
				}
			    }
			}
			else
			{
			    for (var i = 0; i < d.series.length; i++)
			    {
					if (d.series[i].name === user.login || (d.series[i].obs === true && user.priv === true))
					{
					    if (d.series[i].name === user.login)
							_this.name = d.series[i].name;
					    serie.push(d.series[i]);
					}
			    }
			}
	    }
	    else
	    {
		if (group.divider != 'CA')
		{
		    serie = d[0].series.concat(d[1].series);
		    if (d.length === 3)
				serie = serie.concat(d[2].series);
		}
		else
		    serie = d.series
	    }
	    serie.sort(function (a,b){
		if (a.name > b.name)
		    return 1;
		if (a.name < b.name)
		    return -1;
		return 0;
	    });
	    if (d.length === 3)
	    {
			for (var j = 0;j < serie.length - 1; j = j + 3)
			{
			    if (typeof serie[j].color !== "undefined" && serie[j].linkedTo === ':previous')
			    {
					var temp = serie[j];
					serie[j] = serie[j+1];
					serie[j+1] = temp;
					if (typeof serie[j].color !== "undefined")
					{
					    temp = serie[j];
					    serie[j] = serie[j+2];
					    serie[j+2] = temp;
					}
			    }
			    else if (typeof serie[j].color !== "undefined" && serie[j].linkedTo === ':previous2')
			    {
					var temp = serie[j];
					serie[j] = serie[j+2];
					serie[j+2] = temp;
					if (typeof serie[j].color !== "undefined")
					{
					    temp = serie[j];
					    serie[j] = serie[j+1];
					    serie[j+1] = temp;
					}
			    }
			    else if (typeof serie[j+1].color !== "undefined" && serie[j+1].linkedTo === ':previous2')
			    {
					var temp = serie[j+1];
					serie[j+1] = serie[j+2];
					serie[j+2] = temp;
			    }
			    serie[j+2].linkedTo = ':previous'
			}
	    }
	    else
	    {
			for (var i = 0;i < serie.length;i++)
			{
			    for (var j = 0;j < serie.length - i;j = j + 2)
			    {
					if (typeof serie[j].color !== "undefined")
					{
					    var temp = serie[j];
					    serie[j] = serie[j+1];
					    serie[j+1] = temp;
					}
			    }
			}
	    }
	    if (group.group === 'hour')
	    {
			$('#chartContainer').highcharts(getSimpleChart($scope.selectedType, d[0].title, serie, d.categories, group.divider));
	    }
	    else if (group.group === 'day')
	    {
			$('#chartContainer2').highcharts(getSimpleChart($scope.selectedType, d.title ? d.title : d[0].title, serie, d.categories, group.divider));
	    }
	})
    }
    
    var monthChange2 = function(scope)
    {
	var choice = "chiffreVRF";
	if (scope === "annulation")
	    choice = "nbrVRF";
	edisonAPI.intervention.GetStatUser({
	    month: $scope.selectedDate.m,
	    year: $scope.selectedDate.y,
	    group: 'day',
	    model: 'ca',
	    divider: scope,
	}).then(function(resp) {
	    edisonAPI.intervention.GetStatUser({
		month: $scope.selectedDate.m,
		year: $scope.selectedDate.y,
		group: 'day',
		model: 'ca',
		divider: choice
	    }).then(function(resp2) {
		var d = resp.data;
		var d2 = resp2.data;
		var serie = [];
		if(!user.admin)
		{
		    for (var i = 0;i < d.series.length;i++)
		    {
			if (d.series[i].name === user.login || (d.series[i].obs === true && user.priv === true))
			{
			    if (d.series[i].name === user.login)
				_this.name = d.series[i].name;
			    serie.push(d.series[i], d2.series[i]);
			}
		    }
		}
		else
		    serie = d.series.concat(d2.series);
		serie.sort(function (a,b){
		    if (a.name > b.name)
			return 1;
		    if (a.name < b.name)
			return -1;
		    return 0;
		});
		for (var i = 0;i < serie.length;i++)
		{
		    for (var j = 0;j < serie.length - i;j = j + 2)
		    {
			if (typeof serie[j].color !== "undefined")
			{
			    var temp = serie[j];
			    serie[j] = serie[j+1];
			    serie[j+1] = temp;
			}
		    }
		}
		if (scope === 'chiffre' && isset === false)
		{
		    edisonAPI.intervention.GetDayWeek({
			start: new Date(moment().startOf('isoweek').toDate()),
			end: new Date(),
			name: _this.name
		    }).then(function(resp3) {
			_this.MtnWeek = Math.round(resp3.data.elem) + "€";
		    })
		    isset = true;
		}
	    })
	})
    }
    var monthChange = function(scope) {
	var choice = "chiffreVRF";
	if (scope === "annulation")
	    choice = "nbrVRF";
	edisonAPI.intervention.GetStatUser({
	    month: $scope.selectedDate.m,
	    year: $scope.selectedDate.y,
	    group: 'day',
	    model: 'ca',
	    divider: scope,
	}).then(function(resp) {
	    edisonAPI.intervention.GetStatUser({
		month: $scope.selectedDate.m,
		year: $scope.selectedDate.y,
		group: 'day',
		model: 'ca',
		divider: choice
	    }).then(function(resp2) {
		var d = resp.data;
		var d2 = resp2.data;
		var serie = [];
		if(!user.admin)
		{
		    for (var i = 0;i < d.series.length;i++)
		    {
			if (d.series[i].name === user.login  || (d.series[i].obs === true && user.priv === true))
			{
			    if (d.series[i].name === user.login)
				_this.name = d.series[i].name;
			    serie.push(d.series[i], d2.series[i]);
			}
		    }
		}
		else
		    serie = d.series.concat(d2.series);
		serie.sort(function (a,b){
		    if (a.name > b.name)
				return 1;
		    if (a.name < b.name)
				return -1;
		    return 0;
		});
		for (var i = 0; i < serie.length; i++)
		{
		    for (var j = 0; j < serie.length - i; j = j + 2)
		    {
			if (typeof serie[j].color !== "undefined")
			{
			    var temp = serie[j];
			    serie[j] = serie[j+1];
			    serie[j+1] = temp;
			}
		    }
		}
		for (var n = 0; n < serie.length; n++)
		{
		    var percarr = [];
		    var perc = "";
		    for (var t = 0; t < serie[n].data.length; t++)
		    {
				if (!serie[n].linkedTo)
				{
				    perc = Math.round((serie[n].data[t] / (serie[n].data[t] +  serie[n + 1].data[t])) * 100);
				}
				else
				{
				    perc = Math.round((serie[n].data[t] / (serie[n].data[t] +  serie[n - 1].data[t])) * 100);
				}
				if (isNaN(perc))
				{
				    perc = 0;
				}
				percarr.push(perc);
			}
		    serie[n].perc = percarr;
		}
		for (var n = 0; n < serie.length; n++)
		{
		    var tmp = []
		    tmp = serie[n].perc;
		    serie[n].perc = serie[n].data;
		    serie[n].data = tmp;
		    if (!serie[n].linkedTo)
			_this.percstat[serie[n].name] = serie[n];
		    else
			_this.percstat[serie[n].name + "ann"] = serie[n];
		    for (var u = 0;u < serie[n].perc.length;u++)
		    {
			if (!serie[n].linkedTo)
			    _this.percstat[serie[n].name].perc[u] = serie[n].perc[u];
			else
			    _this.percstat[serie[n].name + "ann"].perc[u] = serie[n].perc[u];
		    }
		}
		$('#chartContainer').highcharts(getChart($scope.selectedType, d.title, serie, d.categories));
	    })
	})
    }
    var yearChange2 = function(scope) {
	var choice = "chiffreVRF";
	if (scope === "annulation")
	    choice = "nbrVRF";
	edisonAPI.intervention.GetStatUser({
	    year: $scope.selectedDate.y,
	    group: 'month',
	    model: 'ca',
	    divider: scope,
	}).then(function(resp) {
	    edisonAPI.intervention.GetStatUser({
		year: $scope.selectedDate.y,
		group: 'month',
		model: 'ca',
		divider: choice
	    }).then(function(resp2) {
		var d = resp.data;
		var d2 = resp2.data;
		var serie = [];
		if(!user.admin)
		{
		    for (var i = 0;i < d.series.length;i++)
		    {
			if (d.series[i].name === user.login || (d.series[i].obs === true && user.priv === true))
			    serie.push(d.series[i], d2.series[i]);
		    }
		}
		else
		    serie = d.series.concat(d2.series);
		serie.sort(function (a,b){
		    if (a.name > b.name)
			return 1;
		    if (a.name < b.name)
			return -1;
		    return 0;
		});
		for (var i = 0;i < serie.length;i++)
		{
		    for (var j = 0;j < serie.length - i;j = j + 2)
		    {
			if (typeof serie[j].color !== "undefined")
			{
			    var temp = serie[j];
			    serie[j] = serie[j+1];
			    serie[j+1] = temp;
			}
		    }
		}
		if (scope === 'chiffre')
		{
		    var mtn = 0;
		    for (var k = 1;k < serie.length;k = k + 2)
		    {
			mtn += Math.round(serie[k].data[$scope.selectedDate.m - 1]);
		    }
		    _this.MtnMonth = mtn + "€";    
		}
	    })
	})
    }
    var yearChange = function(scope) {
		var choice = "chiffreVRF";
		if (scope === "annulation")
		    choice = "nbrVRF";
		edisonAPI.intervention.GetStatUser({
		    year: $scope.selectedDate.y,
		    group: 'month',
		    model: 'ca',
		    divider: scope,
		}).then(function(resp) {
		    edisonAPI.intervention.GetStatUser({
			year: $scope.selectedDate.y,
			group: 'month',
			model: 'ca',
			divider: choice
		    }).then(function(resp2) {
			var d = resp.data;
			var d2 = resp2.data;
			var serie = [];
			if(!user.admin)
			{
			    for (var i = 0;i < d.series.length;i++)
			    {
				if (d.series[i].name === user.login || (d.series[i].obs === true && user.priv === true))
				    serie.push(d.series[i], d2.series[i]);
			    }
			}
			else
			    serie = d.series.concat(d2.series);
			serie.sort(function (a,b){
			    if (a.name > b.name)
				return 1;
			    if (a.name < b.name)
				return -1;
			    return 0;
			});
			for (var i = 0;i < serie.length;i++)
			{
			    for (var j = 0;j < serie.length - i;j = j + 2)
			    {
				if (typeof serie[j].color !== "undefined")
				{
				    var temp = serie[j];
				    serie[j] = serie[j+1];
				    serie[j+1] = temp;
				}
			    }
			}
			if (scope === 'chiffre')
			{
			    var mtn = 0;
			    for (var k = 1;k < serie.length;k = k + 2)
			    {
				mtn += Math.round(serie[k].data[$scope.selectedDate.m - 1]);
			    }
			    _this.MtnMonth = mtn + "€";
			}
			for (var n = 0; n < serie.length; n++)
			{
			    var percarr = [];
			    var perc = "";
			    for (var t = 0;t < serie[n].data.length;t++)
			    {
					if (!serie[n].linkedTo)
					    perc = Math.round((serie[n].data[t] / (serie[n].data[t] +  serie[n + 1].data[t])) * 100);
					else
					    perc = Math.round((serie[n].data[t] / (serie[n].data[t] +  serie[n - 1].data[t])) * 100);
					if (isNaN(perc))
					    perc = 0;
					percarr.push(perc);
			    }
			    serie[n].perc = percarr;
			}
			for (var n = 0; n < serie.length; n++)
			{
			    var tmp = []
			    tmp = serie[n].perc;
			    serie[n].perc = serie[n].data;
			    serie[n].data = tmp;
			    if (!serie[n].color)
					_this.percstats[serie[n].name] = serie[n];
			    else
					_this.percstats[serie[n].name + "ann"] = serie[n];
			    for (var u = 0;u < serie[n].perc.length;u++)
			    {
				if (!serie[n].linkedTo)
				    _this.percstats[serie[n].name].perc[u] = serie[n].perc[u];
				else
				    _this.percstats[serie[n].name + "ann"].perc[u] = serie[n].perc[u];
			    }
			    
			}
			if (scope === $scope.selectedDivider)
			{
			    $('#chartContainer2').highcharts(getChart($scope.selectedType, d.title, serie, d.categories));
			}
		    });
		})
    }
    
    $scope.$watch("selectedType", function()  {
	if ($scope.selectedDivider != 'ajout')
	{
	    monthChange2('chiffre');
	    monthChange($scope.selectedDivider);
	    yearChange2('chiffre');
	    yearChange($scope.selectedDivider);
	}
	else
	{
	    loadData({
			group: 'hour',
			divider: 'hour',
	    });
	    loadData({
			month: $scope.selectedSubDate.m,
			year: $scope.selectedSubDate.y,
			group: 'day',
			divider: $scope.selectedSubType,
	    })
	}
    });
    
    
    $scope.$watch("selectedDivider", function()  {
	if ($scope.selectedDivider != 'ajout')
	{
	    monthChange2('chiffre');
	    monthChange($scope.selectedDivider);
	    yearChange2('chiffre');
	    yearChange($scope.selectedDivider);
	}
	else
	{
	    loadData({
			group: 'hour',
			divider: 'hour',
	    });
	    loadData({
			month: $scope.selectedSubDate.m,
			year: $scope.selectedSubDate.y,
			group: 'day',
			divider: $scope.selectedSubType,
	    })
	}
    });
    
    $scope.$watch("selectedYear", function() {
		dateTarget.y = $scope.selectedYear;
		var tmp = _.find(_this.dateselects, dateTarget);
		$scope.selectedDate = tmp;
		monthChange2('chiffre');
		monthChange($scope.selectedDivider);
		yearChange2('chiffre');
		yearChange($scope.selectedDivider);
    });

    $scope.$watch("selectedSubDate", function(curr) {
		if (!$scope.selectedSubDate.m)
		    $scope.selectedSubDate.m = _this.dateselects[0].m;
		$scope.selectedSubDate.y = parseInt($scope.selectedDate.y);
		if ($scope.selectedDivider === 'ajout')
		{
		    loadData({
			month: $scope.selectedSubDate.m,
			year: parseInt($scope.selectedDate.y),
			group: 'day',
			divider: $scope.selectedSubType,
		    })
		}
    }, true);

    $scope.$watch("selectedSubType", function () {
	if ($scope.selectedDivider === 'ajout')
	{
	    loadData({
			month: $scope.selectedSubDate.m,
			year: parseInt($scope.selectedDate.y),
			group: 'day',
			divider: $scope.selectedSubType,
	    })
	}
    })
    
    $scope.$watch("selectedDate", function(curr) {
		if (!$scope.selectedDate.m)
		    $scope.selectedDate.m = _this.dateselects[0].m;
		$scope.selectedYear = parseInt($scope.selectedDate.y);
		monthChange2('chiffre');
		monthChange($scope.selectedDivider);
		yearChange2('chiffre');
		yearChange($scope.selectedDivider);
    }, true);
    
    //Fin des Stats
    _this.addTask = function() {
		edisonAPI.task.add(_this.newTask).then(_.partial(_this.reloadTask, _this.newTask.to));
    }
    
    _this.check = function(task) {
		edisonAPI.task.check(task._id).then(_.partial(_this.reloadTask, _this.newTask.to))
    }

    edisonAPI.intervention.dashboardStats({
	date: moment().startOf('day').toDate()
    })
	.then(function(resp) {
	    _this.statsTeleproBfm = _.sortBy(resp.data.weekStats, 'total').reverse()
	});

    _this.reloadTask = function(usr) {
	_this.newTask = {
	    to: usr,
	    from: user.login
	}
	edisonAPI.task.listRelevant({
	    login: usr
	}).then(function(resp) {
	    _this.taskList = resp.data;
	})
    }

    _this.reloadTask(user.login);
    
    _this.reloadDashboardStats = function(date) {
		edisonAPI.intervention.dashboardStats(date).then(function(resp) {
		    _this.tableParams = new NgTableParams({
			count: resp.data.weekStats.length,
			sorting: {
			    total: 'desc'
			}
		    }, {
			counts: [],
			data: resp.data.weekStats
		    });
		    _this.stats = resp.data
		})
    }
    _this.dateSelect = [{
	nom: 'Du jour',
	date: moment().startOf('day').toDate()
    }, {
	nom: 'De la semaine',
	date: moment().startOf('week').toDate()
    }, {
	nom: 'Du mois',
	date: moment().startOf('month').toDate()
    }, {
	nom: "De l'année",
	date: moment().startOf('year').toDate()
    }]
    _this.dateChoice = _this.dateSelect[1];
    this.reloadDashboardStats(_this.dateChoice);



    var month = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "O\
ctobre", "Novembre", "Décembre"]

    _this.partDate = {};
    _this.partDate.start = new Date();
    _this.partDate.start.setHours(0,0,0,0);
    _this.partDate.end = new Date();
    _this.partDate.end.setHours(24,0,0,0);
    $scope.partDate = {}
    $scope.partDate.start = _this.partDate.start.getDate() + " " + month[_this.partDate.start.getMonth()] + " " + _this.partDate.start.getFullYear();
    $scope.partDate.end = _this.partDate.end.getDate() + " " + month[_this.partDate.end.getMonth()] + " " + _this.partDate.end.getFullYear();

    _this.partDate2 = {};
    _this.partDate2.start = new Date();
    _this.partDate2.start.setHours(0,0,0,0);
    _this.partDate2.end = new Date();
    _this.partDate2.end.setHours(24,0,0,0);
    $scope.partDate2 = {}
    $scope.partDate2.start = _this.partDate2.start.getDate() + " " + month[_this.partDate2.start.getMonth()] + " " + _this.partDate2.start.getFullYear();
    $scope.partDate2.end = _this.partDate2.end.getDate() + " " + month[_this.partDate2.end.getMonth()] + " " + _this.partDate2.end.getFullYear();


    _this.updatePartIntStats = function ()
    {
		var d = _this.partDate2;
		edisonAPI.intervention.ptStats({array: _this.userList, start: d.start, end: d.end}).then(function (resp) {
		    var table = []
		    for (var n = 0; n < _this.userList.length; n++)
		    {
				resp.data[_this.userList[n]].login = _this.userList[n];
				table.push(resp.data[_this.userList[n]])
		    }
		    _this.prStatsInt = new NgTableParams({
				count: table.length,
		    }, {
				counts: [],
				data: table
		    });
		})
    }
    
    _this.StatsCompta = function () {
	var array2 = []
	var ajd = new Date()
	edisonAPI.intervention.CountRecu().then(function(resp2){
	    edisonAPI.intervention.ComptaStats(resp2.data).then(function(resp){
		var array = []
		for(var key in resp.data) {
		    var key2 = key
		    var  test = key2.length
		    var test1 = key2.substring(0,test - 2)
		    var test2 = test1.charAt(0).toUpperCase() + test1.substring(1).toLowerCase()
		    resp.data[key].login = "2F" + ajd.getFullYear() + "#" + key
		    resp.data[key].name = test2
		    array.push(resp.data[key])
		}
		_this.nbrStats = array
	    })
	})
	edisonAPI.intervention.getLitiges().then(function(resp3){
	    for(var key in resp3.data) {
		var key2 = key
		var  test = key2.length
		var test1 = key2.substring(0,test - 2)
		var test2 = test1.charAt(0).toUpperCase() + test1.substring(1).toLowerCase()
		resp3.data[key].name = test2
		resp3.data[key].redirect = "2F" + ajd.getFullYear() + "#" + key
		array2.push(resp3.data[key])
	    }
	    _this.nbrYearStats = array2
	})
    }
    _this.StatsCompta()
    _this.updatePartStats = function () {
		var d = _this.partDate;
		edisonAPI.artisan.ptStats({array: _this.userList, start: d.start, end: d.end}).then(function (resp) {
		    var table = []
		    for (var n = 0; n < _this.userList.length; n++)
		    {
				resp.data[_this.userList[n]].login = _this.userList[n];
				table.push(resp.data[_this.userList[n]])
		    }
		    _this.prStats = new NgTableParams({
				count: table.length,
				sorting: {
				    transf: 'desc'
				}
		    }, {
				counts: [],
				data: table
		    });
		})
    }
    
    _this.getUserList = function () {
		edisonAPI.users.list().then(function (resp) {
		    _this.userList = []
		    for (var n = 0; n < resp.data.length; n++)
		    {
				if (resp.data[n].activated === true && resp.data[n].service === 'PARTENARIAT')
				    _this.userList.push(resp.data[n].login);
		    }
		    _this.updatePartStats();
		    _this.updatePartIntStats();
		})
    }
    _this.getUserList();

    _this.paEnvoi = function (login) {
	$location.url("/intervention/list/paEnvTot?ev=" + login);
    }

    _this.artisanLink = function (login) {
	$location.url("/artisan/list#" + login);
    }

    $scope.$watch('partDate.start', function () {
		if ($scope.partDate && $scope.partDate.start)
		{
		    var split = $scope.partDate.start.split(" ");
		    _this.partDate.start = new Date(split[2], month.indexOf(split[1]), split[0]);
		    if (_this.userList)
			_this.updatePartStats();
		}
    })

    $scope.$watch('partDate.end', function () {
		if ($scope.partDate && $scope.partDate.end)
		{
		    var split = $scope.partDate.end.split(" ");
		    _this.partDate.end = new Date(split[2], month.indexOf(split[1]), split[0]);
		    if (_this.userList)
			_this.updatePartStats();
		}
    })

    $scope.$watch('partDate2.start', function () {
		if ($scope.partDate2 && $scope.partDate2.start)
		{
		    var split = $scope.partDate2.start.split(" ");
		    _this.partDate2.start = new Date(split[2], month.indexOf(split[1]), split[0]);
		    if (_this.userList)
			_this.updatePartIntStats();
		}
    })

    $scope.$watch('partDate2.end', function () {
		if ($scope.partDate2 && $scope.partDate2.end)
		{
		    var split = $scope.partDate2.end.split(" ");
		    _this.partDate2.end = new Date(split[2], month.indexOf(split[1]), split[0]);
		    if (_this.userList)
			_this.updatePartIntStats();
		}
    })
}
DashboardController.$inject = ["$rootScope", "statsTelepro", "dialog", "user", "edisonAPI", "$scope", "$filter", "TabContainer", "NgTableParams", "$routeParams", "$location", "LxProgressService", "MomentIterator", "socket", "edisonAPI", "config"];

angular.module('edison').controller('DashboardController', DashboardController);



 angular.module('edison').directive('edisonMap', ["$window", "Map", "mapAutocomplete", "Address", "edisonAPI", function($window, Map, mapAutocomplete, Address, edisonAPI) {
         "use strict";
         return {
             replace: true,
             restrict: 'E',
             templateUrl: '/Templates/autocomplete-map.html',
             scope: {
                 data: "=",
                 client: "=",
                 height: "@",
                 xmarkers: "=",
                 markerClick: '&',
                 isNew: "=",
                 firstAddress: "=",
                 showAddress: "=",
		 searchRev:"@",
		 hidesearch:"@",
		 draw:"@",
		 circle:"@",
		 ispro:"@",
		 fourn:"=",
		 searchOnly:'@',
		 bigger:"@",
		 hidemap:"@",
             },
             link: function(scope, element, attrs) {
                 scope._height = scope.height || 315;
                 scope.map = new Map();
                 scope.map.setZoom(_.get(scope, 'client.address') ? 12 : 6)
                 if (scope.isNew) {
                     scope.map.show()
                 }
                 scope.autocomplete = mapAutocomplete;

                 scope.mapShow = function() {
                     scope.mapDisplay = true
                 }

                 if (_.get(scope, 'client.address.lt')) {
                     scope.client.address = Address(scope.client.address, true); //true -> copyContructor
                     scope.map.setCenter(scope.client.address);
                 } else {
                     scope.map.setCenter(Address({
                         lat: 46.3333,
                         lng: 2.6
                     }));
                     scope.map.setZoom(5)
                 }

                 scope.changeAddress = function(place) {
                     scope.firstAddress = true;
                     mapAutocomplete.getPlaceAddress(place).then(function(addr) {
			 if (scope.searchRev && scope.searchRev == "true")
			     scope.data.artisan.address = addr;
			 else
			 {
                             scope.map.setZoom(12);
                             scope.map.setCenter(addr)
			     if (!(scope.searchOnly && scope.searchOnly == "true"))
				 scope.client.address = addr;
			 }
                     });
                 }

		 var drawCircle = function(point, radius, dir)
		 {
		     var d2r = Math.PI / 180;
		     var r2d = 180 / Math.PI;
		     var earthsradius = 3963;
		     var points = 32;

		     var rlat = (radius / earthsradius) * r2d;
		     var rlng = rlat / Math.cos(point.lat() * d2r);

		     var extp = new Array();
		     if (dir == 1)
		     {
			 var start = 0;
			 var end = points + 1;
		     }
		     else
		     {
			 var start = points + 1;
			 var end = 0;
		     }
		     for (var i=start; (dir==1 ? i < end : i > end); i=i+dir)
		     {
			 var theta = Math.PI * (i / (points/2));
			 var ey = point.lng() + (rlng * Math.cos(theta));
			 var ex = point.lat() + (rlat * Math.sin(theta));
			 extp.push(new google.maps.LatLng(ex, ey));
		     }
		     return extp;
		 }

		 scope.info = new google.maps.InfoWindow({
		     content: "Hello!"
		 })

		 scope.showInfo = function (marker, fourn)
		 {
		     scope.info.setContent("<img src='/img/glass.jpg'/>" +
					   "<div style='text-align:left'><h1>" +
					   fourn.f.nom + "</h1>" +
					   "<p>" + fourn.f.address.r + "</p>" +
					   "<p>" + fourn.f.address.v + "</p>" +
					   "<p>" + fourn.f.address.cp + "</p>" +
					   (fourn.f.telephone ? "<p>Tel: " + fourn.f.telephone + "</p></div>" : "</div>"))
		     scope.info.open(scope.map, this);
		 }
		 
		 scope.initCircles = function (sst)
		 {
		     if (scope.circle == "true" && sst.address)
		     {
			 scope.map.clearPoly(sst.cate);
			 var circles = [drawCircle(new google.maps.LatLng(sst.address.lt,
									 sst.address.lg),
						   30 * 0.62137, 1)];
			 var joinCircle = new google.maps.Polygon({
			     paths: circles,
			     strokeColor: '#ff0000',
			     strokeOpacity: 0.20,
			     strokeWeight: 0,
			     fillColor: "#FF0000",
			     fillOpaity: 0.20,
			 });
			 scope.map.setPoly(joinCircle);
			 scope.isInit = true;
		     }
		 }

                 scope.getStaticMap = function() {
                     if (!_.get(scope, 'data.client.address.lt') && !_.get(scope, 'data.address.lt'))
                         return 0
                     var q = "?width=" + Math.round($window.outerWidth * (scope.height === "small" ? 0.8 : 1.2));
                     if (scope.client && scope.client.address && scope.client.address.latLng)
                         q += ("&origin=" + scope.client.address.latLng);
                     if (scope.data.artisan && scope.data.artisan.address)
                         q += ("&destination=" + scope.data.artisan.address.lt + "," + scope.data.artisan.address.lg);
                     return "/api/mapGetStatic" + q;
                 }
                 scope.showClientMarker = function() {
                     return scope.client.address && scope.client.address.latLng;
                 }
                 scope.clickOnArtisanMarker = function(event, sst) {
                    scope.markerClick({
                        sst:sst
                    })
                 }
             }
         }
     }
 ]);

 angular.module('edison').directive('creditCard', ["config", function(config) {
     "use strict";
     return {
         replace: true,
         restrict: 'E',
         templateUrl: '/Templates/credit-card.html',
         scope: {
             model: "=",
         },
         link: function(scope, element, attrs) {
             scope.config = config
         },
     }

 }]);

angular.module('edison').directive('decaissement',
                   ["config", "Paiement", "Intervention", "textTemplate", "edisonAPI", function(config, Paiement, Intervention, textTemplate, edisonAPI) {
        "use strict";
        return {
            restrict: 'E',
            templateUrl: '/Templates/decaissement.html',
            scope: {
                data: "=",
                displayDecaissement: '@',
                dialog: '@',
                simulator: '@'
            },
            link: function(scope, element, attrs) {
                console.log("decaissement: ", scope, element, attrs);
                if (scope.displayDecaissement) {
                    scope.showDecaissement = true;
                }
                scope.changeTva = function(index) {
                    $scope.data.compta.decaissement[index].tva = scope.data.tva;
                }
            },
        }
    }]
);
var DevisCtrl = function(edisonAPI, $scope, $rootScope, $location, $routeParams, LxProgressService, LxNotificationService, TabContainer, config, dialog, devisPrm, Devis) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.dialog = dialog;
    _this.moment = moment;
    var tab = TabContainer.getCurrentTab();
    if (!tab.data) {
        var devis = new Devis(devisPrm.data)
        tab.setData(devis);
        if ($routeParams.id.length > 12) {
            _this.isNew = true;
            devis.tmpID = $routeParams.id;
            tab.setTitle('DEVIS ' + moment((new Date(parseInt(devis.tmpID))).toISOString()).format("HH:mm").toString());
        } else {
            tab.setTitle('DEVIS ' + $routeParams.id);
            if (!devis) {
                LxNotificationService.error("Impossible de trouver les informations !");
                $location.url("/dashboard");
                TabContainer.remove(tab);
                return 0;
            }
        }
    } else {
        var devis = tab.data;
    }
    _this.data = tab.data;

    var closeTab = function(err) {
        if (!err)
            TabContainer.close(tab);
    }


    _this.loadFournitures = function () {
	edisonAPI.marker.list({categorie: _this.data.categorie}).then(function (resp) {
	    _this.fournCoord = resp.data;
	})
    }

    _this.searchArtisans = function(categorie) {
        if (_.get(devis, 'client.address.lt')) {
            edisonAPI.artisan.getNearest(devis.client.address, categorie || devis.categorie)
                .success(function(result) {
                    _this.nearestArtisans = result;
		    if (_this.data.categorie || intervention.categorie)
			_this.loadFournitures()
                });
        }
    }
    _this.searchArtisans();

    _this.saveDevis = function(options) {
        if (!devis.produits ||  !devis.produits.length) {
            return LxNotificationService.error("Veuillez ajouter au moins 1 produit");
        }
        devis.save(function(err, resp) {
            if (err) {
                return false;
            } else if (options.envoi) {
                Devis(resp).sendDevis(closeTab);
            } else if (options.annulation) {
                Devis(resp).annulation(closeTab);
            } else if (options.transfert) {
                Devis(resp).transfert()
            } else {
                closeTab();
            }
        })
    }

    $scope.$watch(function() {
        return devis.client.civilite
    }, function(newVal, oldVal) {
        if (oldVal != newVal) {
            devis.tva = (newVal == 'Soc.' ? 20 : 10);
        }
    })

    var updateTmpDevis = _.after(5, _.throttle(function() {
        edisonAPI.devis.saveTmp(devis);
    }, 30000))

    if (!devis.id) {
        $scope.$watch(function() {
            return devis;
        }, updateTmpDevis, true)
    }

}
DevisCtrl.$inject = ["edisonAPI", "$scope", "$rootScope", "$location", "$routeParams", "LxProgressService", "LxNotificationService", "TabContainer", "config", "dialog", "devisPrm", "Devis"];
angular.module('edison').controller('DevisController', DevisCtrl);

angular.module('edison').directive('encaissement',
				   ["config", "Paiement", "Intervention", "textTemplate", "edisonAPI", function(config, Paiement, Intervention, textTemplate, edisonAPI) {
        "use strict";
        return {
            restrict: 'E',
            templateUrl: '/Templates/encaissement.html',
            scope: {
                data: "=",
                displayEncaissement: '@',
                dialog: '@',
                simulator: '@'
            },
            link: function(scope, element, attrs) {
                console.log("ENCAISSEMENT: ", scope, element, attrs);
                if (scope.displayEncaissement) {
                    scope.showEncaissement = true;
                }
                scope.changeTva = function(index) {
                    $scope.data.compta.encaissement[index].tva = scope.data.tva;
                }
            },
        }
    }]
);
 angular.module('edison').directive('infoCategorie', ["config", function(config) {
     "use strict";
     return {
         replace: true,
         restrict: 'E',
         templateUrl: '/Templates/info-categorie.html',
         transclude: true,
         scope: {
             model: "=",
             change: '&'
         },
         link: function(scope, element, attrs) {
             scope.config = config
             scope.callback = function(newCategorie) {
                 scope.model = newCategorie;
                 if (typeof scope.change === 'function')  {
                     scope.change({
                         newCategorie: newCategorie
                     })
                 }
             }
         },
     }

 }]);

 angular.module('edison').directive('infoClient', ["config", "edisonAPI", function(config, edisonAPI) {
     "use strict";
     return {
         replace: true,
         restrict: 'E',
         templateUrl: '/Templates/info-client.html',
         transclude: true,
         scope: {
             client: '=model',
             data: '=',
             noDetails:'@'
         },
         link: function(scope, element, attrs) {
             scope.config = config;
             scope.searchPhone = function(tel) {
                 if (tel.length > 2) {
                     edisonAPI.searchPhone(tel)
                         .success(function(tel) {
                             scope.searchedPhone = tel
                         }).catch(function() {
                             scope.searchedPhone = {};
                         })
                 }
             }
         }
     }

 }]);

angular.module('edison').directive('infoCompta',
				   ["config", "Paiement", "Intervention", "textTemplate", "edisonAPI", function(config, Paiement, Intervention, textTemplate, edisonAPI) {
        "use strict";
        return {
            restrict: 'E',
            templateUrl: '/Templates/info-compta.html',
            scope: {
                data: "=",
                displayReglement: '@',
                dialog: '@',
                displayPaiement: '@',
                simulator: '@'
            },
            link: function(scope, element, attrs) {
                scope.config = config
                scope.textTemplate = textTemplate;
                scope.Intervention = Intervention
                if (scope.displayReglement) {
                    scope.showPaiement = true
                }
                if (scope.displayPaiement) {
                    scope.showReglement = true
                }
                var reglement = scope.data.compta.reglement
                var paiement = scope.data.compta.paiement
                if (!scope.data.tva) {
                    scope.data.tva = (scope.data.client.civilite == 'Soc.' ? 20 : 10)
                }
                if (!paiement.mode) {
                    paiement.mode = _.get(scope.data.sst, 'verifRIB') ? "VIR" : "CHQ"
                }
                scope.format = function(nbr) {
                    return _.round(nbr, 2).toFixed(2);
                }
                scope.getPaiement = function(e) {
                    var x = _.cloneDeep(scope.data);
                    x.compta.paiement = _.cloneDeep(e);
                    return new Paiement(x);
                }
		edisonAPI.artisan.get(scope.data.artisan.id).then(function(resp){
		  scope.verif = resp.data.verifRIB
		})
                scope.Paiement = Paiement;
                scope.compta = new Paiement(scope.data)
                reglement.montantTTC = scope.compta.getMontantTTC()

                scope.$watchGroup(['data.compta.reglement.montantTTC',
                    'data.compta.reglement.avoir',
                    'data.tva'
                ], function(current, prev) {
                    var montant = reglement.montantTTC || 0
                    var coeff = 100 * (100 / (100 + scope.data.tva));
                    reglement.montant = Paiement().applyCoeff(reglement.montantTTC, coeff)
                    if (!paiement.base) {
                        paiement.base = _.round(reglement.montant - (reglement.avoir ||  0), 2)
                    }
                })

                var change = function(newValues, oldValues, scope) {
                //    if (!_.isEqual(newValues, oldValues)) {
                        scope.compta = new Paiement(scope.data)
                        paiement.montant = scope.compta.montantTotalTTC
                  //  }
                }
                scope.$watch('data.fourniture', change, true)

                scope.$watch('data.compta.paiement.pourcentage.deplacement', change, true)

                scope.$watch('data.compta.paiement.pourcentage.maindOeuvre', change, true)

                scope.$watchGroup(['data.compta.reglement.montant',
                    'data.compta.paiement.base',
                    'data.compta.paiement.tva',
                    'data.compta.paiement.pourcentage.deplacement',
                    'data.compta.paiement.pourcentage.maindOeuvre',
                ], change, true);
                if (!scope.data.compta.paiement.base && scope.data.compta.reglement.montant) {
                    scope.data.compta.paiement.base = scope.data.compta.reglement.montant;
                    scope.compta = new Paiement(scope.data)
                    paiement.montant = scope.compta.montantTotalTTC
                }
            },

        }

    }]
);

 angular.module('edison').directive('produits',
     ["config", "productsList", "dialog", "openPost", "LxNotificationService", "Intervention", "Devis", "edisonAPI", function(config, productsList, dialog, openPost, LxNotificationService, Intervention, Devis, edisonAPI) {
         "use strict";
         return {
             restrict: 'E',
             templateUrl: '/Templates/info-produit.html',
             scope: {
                 data: "=",
                 tva: '=',
                 display: '@',
                 model: "@",
                 embedded: "="
             },
             link: function(scope, element, attrs) {
                 var model = scope.data;
                 scope.config = config
                 model.produits = model.produits || [];
                 scope.config = config;
                 scope.produits = new productsList(model.produits);
                 edisonAPI.combo.list().then(function(resp) {
                     scope.combo = resp.data
                 })

                 scope.Intervention = Intervention;
                 scope.Devis = Devis;

                 if (!scope.data.reglementSurPlace) {
                     scope.display = true;
                 }


                 scope.$watch('data.produits', function(curr, prev) {
                     if (!_.isEqual(curr, prev)) {
                         scope.data.prixFinal = scope.produits.total()
                         scope.data.prixAnnonce = scope.produits.total()
                     }
                 }, true)

                 scope.$watch('data.combo', function(curr, prev) {
                     if (curr && !_.isEqual(curr, prev)) {
                         var prod = _.find(scope.combo, function(e) {
                             return e.ref === curr;
                         })
                         _.each(prod.produits, function(e) {
                             if (!e.ref) {
                                 e.ref = e.desc.toUpperCase().slice(0, 3) + '0' + _.random(9, 99)
                             }
                         })
                         model.comboText = prod.text;
                         model.produits = prod.produits || [];
                         scope.produits = new productsList(model.produits);
                     }
                 }, true)

                 scope.displayfact = function() {
                     return scope.data.produits.length > 0 || !scope.data.reglementSurPlace || scope.dsf;
                 }

                 scope.changeElemTitle = function(elem) {
                     if (!elem.showDesc) {
                         elem.desc = elem.title
                     }
                 }

                 scope.createProd = function() {
                     /*                     scope.produits.add({
                                              ref: 'EDIXX',
                                              desc: "",
                                              pu: 10,
                                              quantite: 1,
                                              focus: true,
                                          })*/
                     model.produits.push({
                             showDesc: false,
                             desc: '',
                             title: '',
                             pu: 0,
                             quantite: 0,
                         })
                         /*  dialog.addProd(function(resp) {
                               model.produits.push(resp)
                           });*/
                 }
                 scope.printDevis = function() {
                     openPost('/api/intervention/printDevis', {
                         data: JSON.stringify(scope.data),
                         html: true
                     })
                 }

                 scope.printFactureAcquitte = function() {
                     openPost('/api/intervention/printFactureAcquitte', {
                         data: JSON.stringify(scope.data),
                         html: true
                     })
                 }
             },
         }

     }]
 );

var InterventionCtrl = function(Description, Signalement, ContextMenu, $window, $timeout, $rootScope, $scope, $location, $routeParams, dialog, fourniture, LxNotificationService, LxProgressService, TabContainer, edisonAPI, Address, $q, mapAutocomplete, productsList, config, interventionPrm, Intervention, Map, user) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.dialog = dialog;
    _this.autocomplete = mapAutocomplete;
    _this.displayUser = app_session;
    var tab = TabContainer.getCurrentTab();
    if (!tab.data) {
        var intervention = new Intervention(interventionPrm.data)
        intervention.sst__id = intervention.sst ? intervention.sst.id : 0;
        tab.setData(intervention);
        if ($routeParams.id.length > 12) {
            _this.isNew = true;
            intervention.tmpID = $routeParams.id;
            intervention.tmpDate = moment.unix(intervention.tmpID / 1000).format('HH[h]mm')
            tab.setTitle(intervention.tmpDate);
        } else {
            if (intervention && intervention.client.nom) {
                var __title = intervention.client.civilite + intervention.client.nom
                __title = __title.slice(0, 10);
            } else {
                var __title = '#' + $routeParams.id;
            }
            tab.setTitle(__title);
            if (!intervention) {
                LxNotificationService.error("Impossible de trouver les informations !");
                $location.url("/dashboard");
                TabContainer.remove(tab);
                return 0;
            }
        }
    } else {
        var intervention = tab.data;
    }
    if ($routeParams.d) {
        intervention.devisOrigine = parseInt($routeParams.d)
        intervention.date = {
            ajout: new Date(),
            intervention: new Date(),
        }
        intervention.reglementSurPlace = true;
        intervention.modeReglement = 'CH';
        intervention.remarque = 'PAS DE REMARQUES';
    }
    if ($routeParams.i) {
		edisonAPI.intervention.get(parseInt($routeParams.i)).then(function (resp) {
	    	if (resp.data != "")
	    	{
				intervention.client = resp.data.client;
				intervention.categorie = resp.data.categorie;
				intervention.comments = [];
				intervention.facture = resp.data.facture;
				intervention.description = resp.data.description
				intervention.remarque = resp.data.remarque;
				intervention.produits = resp.data.produits;
				intervention.file = resp.data.file;
				if ($routeParams.sa)
				{
		    		intervention.status = "SAV"
		    		intervention.statusSAV = "APR"
		    		intervention.sst = resp.data.sst
		    		intervention.artisan = resp.data.artisan
		    		intervention.oldid = $routeParams.i
				}
				intervention.comments.push({
                    login: user.login,
                    text: "Duplication de l'intervention n°" + parseInt($routeParams.i),
                    date: new Date()
				})
	    	}
		})
    }
    else if ($routeParams.di) {
		edisonAPI.devis.get(parseInt($routeParams.di)).then(function (resp2) {
	    	intervention.client = resp2.data.client;
	    	intervention.categorie = resp2.data.categorie
	    	intervention.produits = resp2.data.produits;
	    	intervention.comments = [];
	    	intervention.comments.push({
				login: user.login,
				text: "Duplication du devis n°" + parseInt($routeParams.di),
				date: new Date()
	    	})
		})
    }

    if (tab.data.reglementSurPlace)
        _this.saveReglementSurPlace = tab.data.reglementSurPlace;

    _this.data = tab.data;

	_this.changeStatus = function(newStatus) {
		if (newStatus != "")
		{
			var tabStatus = ["ENC", "VRF", "APR", "AVR", "EVR", "ANN", "SAV", "DVG", "PRT", "RGL"];
			if (newStatus == "SAV")
			{
				var saveStatus = _this.data.status;
				_this.data.status = newStatus;
				_this.data.cache.s = tabStatus.indexOf(newStatus);
				_this.data.cache.sa = saveStatus;
				_this.data.statusSAV = saveStatus;
			}
			else
			{
				_this.data.status = newStatus;
				_this.data.cache.s = tabStatus.indexOf(newStatus);
				_this.data.cache.sa = "";
				_this.data.statusSAV = "";
			}
		}
	}

    _this.loadFournitures = function () {
	edisonAPI.marker.list({categorie: _this.data.categorie}).then(function (resp) {
	    _this.fournCoord = resp.data;
	})
    }

    _this.fourndatafunc = function () {
    	if (_this.data.relancecheque.level === 1)
    	{
    	    var data = "L'artisan possède toujours le règlement";
    	    _this.data.fournfile = data;
    	}
    	if(_this.data.relancecheque.level === 2)
    	{
    	    var data = "L'artisan a envoyé le règlement"
    	    _this.data.fournfile = data;
    	}
    	if (_this.data.relancecheque.level === 3)
    	{
    	    var data = "L'artisan dit ne pas avoir pris de règlement"
    	    _this.data.fournfile = data;
    	}
    	var dateStr = new Date();
    	var dArr = [];
    	dArr = _.words(dateStr), /[^- ]+/;
    	dArr[1] = dateStr.getMonth() + 1;
    	var date1 = dArr[2] + "/" + dArr[1] + "/" + dArr[3];
    	_this.data.fournpar = user.login
    	_this.data.fourndate = date1
    	if (!_this.data.fourndata)
    	    var test = []
    	else
    	    var test = _this.data.fourndata
    	test.push({login: user.login,statut: data,date: dateStr})
    	_this.data.fourndata = test
    	edisonAPI.intervention.save(resp.data)
    }
    
    _this.changeCours = function(){

    	_this.data.status = "ENC"
    	edisonAPI.intervention.save(_this.data)
        LxNotificationService.success("Intervention est passé au status \"En cours\"");
    }

    _this.checkConflict = function(check) {
    	if (check === 'opp' && _this.sspro === true)
    	    _this.sspro = false;
    	if (check === 'ssp' && _this.oppo === true)
    	    _this.oppo = false;
    }
    
    $scope.bv = {
        show: function(view) {
            if (this[view]) {
                return (this[view] = false);
            }
	    	this.comments = false
            this.historique = false;
            this.absence = false;
            this.signalement = false;
	   		this.calls = false;
            this[view] = true;
        },
        init: function() {
            this.comments = this.historique = this.absence = this.signalement = false;
            _this.searchArtisans(intervention.categorie)
            $timeout(function() {
                _this.searchArtisans(intervention.categorie)
            }, 666)
        }
    }
    var updateTitle = _.throttle(function() {
        tab.setTitle(_.template("{{typeof tmpDate == 'undefined' ? id : tmpDate}} - {{client.civilite}} {{client.nom}} ({{client.address.cp}})")(intervention));
    }, 1000)

    $scope.$watch('vm.data.client', updateTitle, true)
    updateTitle();
    _this.description = new Description(intervention);
    _this.signalement = new Signalement(intervention)
    _this.contextMenuIntervention = new ContextMenu('intervention')
    _this.contextMenuSST = new ContextMenu('artisan')
    _this.contextMenu = _this.contextMenuIntervention
    _this.contextMenu.setData(intervention);
    
    _this.callSwipe = function (sst) {
	dialog.callArtisan(sst, function(data) {
	    $window.open('tel:' + data, '_self', false);
	});
    }
    
    _this.isAbsent = function(abs) {
        if (!abs || !abs.length) {
            return false;
        }
        return moment().isAfter(abs[abs.length - 1].start) && moment().isBefore(abs[abs.length - 1].end)
    }


    _this.rowRightClick = function($event, inter) {
        if ($('.map-box').has($event.target).length) {
            var id = $event.target.getAttribute('id-sst') || _.get(intervention, 'sst.id')
            if (id) {
                _this.rightClickArtisan = id;
                edisonAPI.artisan.get(id).then(function(resp) {
                    _this.contextMenu = _this.contextMenuSST;
                    _this.contextMenu.setData(resp.data);
                    _this.contextMenu.setPosition($event.pageX, $event.pageY + 200)
                    _this.contextMenu.open().onClose(_.debounce(function(resp) {
                        _this.searchArtisans(intervention.categorie)
                    }, 500))
                })
            }

        } else if ($('.listeInterventions').has($event.target).length == 0) {
            _this.contextMenu = this.contextMenuIntervention;
            _this.contextMenu.setPosition($event.pageX, $event.pageY + 200)
            _this.contextMenu.open();
        }
    }

    Mousetrap.bind(['command+k', 'ctrl+k', 'command+f1', 'ctrl+f1'], function() {
        $window.open("appurl:", '_self');
        edisonAPI.intervention.scan(intervention.id).then(function() {
            $scope.loadFilesList();
            LxNotificationService.success("Le fichier est enregistré");
        })
        return false;
    });


    $scope.calculPrixFinal = function() {
        if (intervention.reglementSurPlace) {
            return 0;
        }
        intervention.prixFinal = 0;
        _.each(intervention.produits, function(e)  {
            intervention.prixFinal += (e.pu * e.quantite)
        })
        intervention.prixFinal = Math.round(intervention.prixFinal * 100) / 100;
    }

    $scope.addLitige = function() {
        dialog.getText({
            title: "Description du Litige",
            text: ""
        }, function(resp) {
            if (!intervention.litiges)
                intervention.litiges = [];
            intervention.litiges.push({
                date: new Date(),
                login: $rootScope.user.login,
                description: resp,
                regle: false
            })
        })
    }

    $scope.smsArtisan = function() {
        intervention.smsArtisan(function(err, resp) {
            if (!err)
                intervention.sst.sms.unshift(resp)
        })
    }


    $scope.clickTrigger = function(elem) {
        angular.element(elem).trigger('click');
    }


    $scope.$watch('vm.fileUpload', function(file) {
        if (file && file.length === 1) {
            intervention.fileUpload(file[0], function(err, resp) {
                $scope.fileUploadText = "";
                $scope.loadFilesList();
            });
        }
    })

    $scope.loadFilesList = function() {
        edisonAPI.intervention.getFiles(intervention.id || intervention.tmpID).then(function(result) {
            intervention.files = result.data;
        })
    }

    $scope.loadFilesList();

    var postSave = function(options, resp, cb) {
        /*
	if (intervention.tmpID && $routeParams.i) {
	    for (var n = 0; n < intervention.files.length; n++)
	    {
		intervention.fileUpload(intervention.files[n], function(err, resp) {
		});
	    }
	}*/
        if (options && options.envoiFacture && options.verification) {
            intervention.envoiFactureVerif(cb)
        } else if (options && options.envoiFacture) {
            intervention.sendFacture(cb)
        } else if (options && options.envoi === true) {
            resp.files = intervention.files;
            intervention.envoi.bind(resp)(cb);
        } else if (options && options.annulation) {
            intervention.annulation(cb);
        } else if (options && options.verification) {
            intervention.verificationSimple(cb);
        } else if (_this.saveReglementSurPlace && _this.saveReglementSurPlace != _this.data.reglementSurPlace && options && options.verification) {
            intervention.envoiFactureVerif(cb);
        } else {
            cb(null)
        }
    }
    

    var saveInter = function(options) {
    	if ($rootScope.user.root !== true && $rootScope.user.service === 'INTERVENTION' && _this.data.date.modifs)
    	{
    	    var count = 0;
    	    for (var n = 0; n < _this.data.date.modifs.length; n++)
    	    {
        		if (_this.data.date.modifs[n].login === $rootScope.user.login &&
        		    _this.data.date.intervention.getTime() != new Date(interDate).getTime())
        		{
        		    if (count >= 1 || _this.data.status !== 'ENC')
        		    {
            			LxNotificationService.error("Vous ne pouvez plus modifier la date d'intervention");
            			return ;
        		    }
        		    count += 1;
        		}
    	    }
    	}
    	if (_this.data.date.intervention.getTime() != new Date(interDate).getTime() &&
    	    _this.data.date.modifs)
    	    _this.data.date.modifs.push({date: new Date(), login: $rootScope.user.login});
        if (_this.data.artisan)
        {
            edisonAPI.artisan.get(_this.data.artisan.id).then(function(artisan){
        		var pack = artisan.data.historique.pack
        		if (pack.length > 0)
        		{
        			var datefact = new Date(pack[pack.length - 1].date)
        		}
        		else
        		    var datefact = new Date(2016,8,1);
                    edisonAPI.intervention.CountFacture({id:_this.data.sst.id,date:datefact}).then(function(resp2){
            		    var inter = resp2.data
            		    inter = parseInt(inter)
            		    edisonAPI.artisan.CheckFacturier(_this.data.sst.id,{count: inter}).then(function(resp){
                			if (resp === "ok")
                			{
            			        LxNotificationService.success("Une demande de facturier a été fait pour M." + _this.data.sst.representant.nom)
			                }
        		        })
        		    })
        	})
        }
        intervention.save(function(err, resp) {
	       if (!err)
           {
                /*  var files = intervention.files
                  var tmp = intervention;
                  intervention = new Intervention(resp);
                  intervention.produits = tmp.produits;
                  intervention.fourniture = tmp.fourniture;
                  intervention.files = files*/
                postSave(options, resp, function(err) {
                    if (!err) {
                        TabContainer.close(tab);
                    }
                    $scope.saveInter = saveInter;
                })
            }
            else
            {
                $scope.saveInter = saveInter;
            }
        })
    }

    $scope.saveInter = saveInter;


    var latLng = function(add) {
        return add.lt + ', ' + add.lg
    }
    _this.selectArtisan = function(sst, first) {

        if (!sst) {
            intervention.sst = intervention.artisan = null
            return false;
        }
        $q.all([
            edisonAPI.artisan.get(sst.id, {
                cache: false
            }),
            edisonAPI.artisan.getStats(sst.id, {
                cache: true
            })
        ]).then(function(result) {
            intervention.sst = intervention.artisan = result[0].data;
            intervention.sst.stats = result[1].data
            if (!first) {
                intervention.compta.paiement.pourcentage = _.clone(intervention.sst.pourcentage);
            }
            edisonAPI.getDistance(latLng(sst.address), latLng(intervention.client.address))
                .then(function(dir) {
                    intervention.sst.stats.direction = dir.data;
                })
            _this.recapFltr = {
                ai: intervention.sst.id
            }
        });
    }

    _this.showInterList = function() {
        //$scope.interList = true;
    }

    _this.sstBase = intervention.sst;
    if (intervention.sst) {
        _this.selectArtisan(intervention.sst, true);
    }

    _this.searchArtisans = function(categorie) {
        if (_.get(intervention, 'client.address.lt')) {
            edisonAPI.artisan.getNearest(intervention.client.address, categorie || intervention.categorie)
                .success(function(result) {
                    _this.nearestArtisans = result;
        		    if (_this.data.categorie || intervention.categorie)
                        _this.loadFournitures()
                });
        }
    }
    //_this.searchArtisans();
    var init = true
    $scope.$watch(function() {
        return intervention.client.address;
    }, function() {
	if (init == true)
	{
	    init = false
	    return ;
	}
        _this.searchArtisans(intervention.categorie);
    })

    _this.createLitige = function(type) {
        edisonAPI.intervention.createLitige({ type: type, data: _this.data.id, user: app_session.login }).then(function(returnEnd) {
            if (returnEnd.data == "YES")
                LxNotificationService.success("Litige ajouté");
            else
                LxNotificationService.error("Une erreur est survenue");
        })
    }

    _this.InfoRecouv = function () {
	   _this.data.recouvrement.date = new Date();
	   _this.data.recouvrement.login = user.login;
    }
    $scope.$watch(function() {
        return intervention.client.civilite;
    }, function(curr, prev) {
        if (curr !== prev && curr === 'Soc.') {
            intervention.tva = 20;
            LxNotificationService.info("La TVA à été mise a 20%");
        }
    })

    var updateTmpIntervention = _.after(5, _.throttle(function() {
        edisonAPI.intervention.saveTmp(intervention);
    }, 30000))

    if (!intervention.id) {
        $scope.$watch(function() {
            return intervention;
        }, updateTmpIntervention, true)
    }

    _this.prospectModif = function () {
	$window.open('/prospect/' + $routeParams.id);
    }
    /*
    $scope.$watch("vm.test", function(){
	var check = _this.test.split(":")
	var check2 = new Date(_this.test2)
	check2.setHours(check[0])
	check2.setMinutes(check[1])
	var inter = new Date(check2)
	var inter2 = new Date(check2) 
	var inter17 = new Date(inter.setHours(17,0,0))
	var inter24 = new Date(inter.setHours(23,59,59))
	if (inter2.getDay() === 0)
	    _this.showDimanche = true
	else
	    _this.showDimanche = false
	if (inter2.getTime() > inter17.getTime() && inter2.getTime() < inter24.getTime())
	    _this.showSoir = true
	else
	    _this.showSoir = false
	if (inter2.getDay() === 6)
	    _this.showSamedi = true
	else
	    _this.showSamedi = false
    })
    $scope.$watch("vm.test2", function(){
	var check = _this.test.split(":")
	var check2 = new Date(_this.test2)
	check2.setHours(check[0])
	check2.setMinutes(check[1])
	var inter = new Date(check2)
	var inter2 = new Date(check2)
	var inter17 = new Date(inter.setHours(17,0,0))
	var inter24 = new Date(inter.setHours(23,59,59))
	if (inter2.getDay() === 0)
	    _this.showDimanche = true
	else
	    _this.showDimanche = false
	if (inter2.getTime() > inter17.getTime() && inter2.getTime() < inter24.getTime())
	    _this.showSoir = true
	else
	    _this.showSoir = false
	if (inter2.getDay() === 6)
	    _this.showSamedi = true
	else
	    _this.showSamedi = false
    })
    var inter = new Date(_this.data.date.intervention)
    var inter2 = new Date(_this.data.date.intervention)
    var inter17 = new Date(inter.setHours(17,0,0))
    var inter24 = new Date(inter.setHours(23,59,59))
    if (inter2.getDay() === 6)
	_this.showSamedi = true
    else
	_this.showSamedi = false
    if (inter2.getDay() === 0)
	_this.showDimanche = true
    else
	_this.showDimanche = false
    if (inter2.getTime() > inter17.getTime() && inter2.getTime() < inter24.getTime())
	_this.showSoir = true
    else
	_this.showSoir = false
    */
    var checkResize = function() {
	_this.phoneWin = window.innerWidth < 660
    }
    $(window).resize(checkResize);
    _this.phoneWin = window.innerWidth < 660

    _this.addDemarchage = function ()
    {
        var addr = _this.description.inter.client.address.cp + ' ' + _this.description.inter.client.address.v;
        var dat = new Date();
        var day = dat.getDate().toString();
        var m = (dat.getMonth() + 1).toString();
        var min = (dat.getMinutes()).toString();
        var fulldate = dat.getHours() + "h" +
            (min[1] ? min : "0" + min[0]) + " " + (day[1] ? day : "0" + day[0]) + "/" +
            (m[1] ? m : "0" + m[0]) + "/" + dat.getFullYear();
        var obj = {
            date: fulldate,
            login: $rootScope.user.login,
            cate: config.categories[_this.description.inter.categorie].order,
            ad: addr,
            status: false,
        };

	_this.autocomplete.getPlaceAddress({description: obj.ad}).then(function (addrResp) {
	    obj.address = {
		lt: addrResp.lt,
		lg: addrResp.lg
	    }
	    
	    edisonAPI.demarchage.add(obj).then(function (resp) {
		if (resp.data != "not")
		    var message = _.template("Une nouvelle adresse a été ajoutée à la liste de démarchage.")(resp.data)
		else
		    var message = "L'addresse n'a pas été ajouté"
		LxNotificationService.success(message);
            }, function (error) {
		
		LxNotificationService.error(error.data);
            });
	})
	
    }

    _this.miseEnDe = function() {
	edisonAPI.intervention.litigeInjonc(_this.data.id).then(function(resp){
	})
	
    }
    
    _this.printQ = function (check) {
	var check2 = {params: check}
	if (check === "desispers")
	{
	    check2 = {
		params: check,
		montant: _this.mtnCheque,
		numero: _this.numCheque,
		emetteur: _this.emeCheque,
		date: _this.datCheque,
		nom: _this.nomBanque
	    }
	}
	edisonAPI.intervention.printQ(_this.data.id, check2).then(function(resp){
	    LxNotificationService.success("Envoyé au Dropbox");    
	})
    }
    _this.printQI = function (check) {
	
	if (_this.oppo)
	    var check2 = {params: "opposition"}
	if (_this.sspro)
	    var check2 = {params: "ssprovision"}
	edisonAPI.intervention.printQ(_this.data.id, check2).then(function(resp){
	    LxNotificationService.success("Envoyé au Dropbox");
	})
    }
    var arr_sort = function (o1, o2) {
	   return new Date(o1.date) - new Date(o2.date)
    }

    edisonAPI.ovh.getNumber(_this.data.client.telephone, _this.data.login ? _this.data.login.ajout : user.login).then(function (res) {
	_this.numbers = res.data;
    })

    var interDate = _this.data.date.intervention;
    _this.desc2 = _this.data.description;
}
InterventionCtrl.$inject = ["Description", "Signalement", "ContextMenu", "$window", "$timeout", "$rootScope", "$scope", "$location", "$routeParams", "dialog", "fourniture", "LxNotificationService", "LxProgressService", "TabContainer", "edisonAPI", "Address", "$q", "mapAutocomplete", "productsList", "config", "interventionPrm", "Intervention", "Map", "user"];

angular.module('edison').controller('InterventionController', InterventionCtrl);

var LpaController = function(user, openPost, socket, ContextMenu, $location, $window, TabContainer, edisonAPI, $rootScope, LxProgressService, LxNotificationService, FlushList) {
    "use strict";
    var _this = this
    var tab = TabContainer.getCurrentTab();
    tab.setTitle('LPA')
    _this.search = $location.search();
    _this.contextMenu = new ContextMenu('intervention')
    _this.user = user;
    _this.offsetX = 1;
    _this.offsetY = -5;
    _this.loadData = function(prevChecked) {
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        edisonAPI.compta.lpa($location.search()).then(function(result) {
            _.each(result.data, function(sst) {
		for (var i = 0;i < sst.list.length; i++)
		{ 
		    if (sst.verif)
			sst.list[i].compta.paiement.mode = "VIR"
		    else
			sst.list[i].compta.paiement.mode = "CHQ"
		}
                sst.list = new FlushList(sst.list, prevChecked);
                sst.numeroCheque = sst.list.getList()[0].numeroCheque
                if (_this.search.d) {
                    _this.checkArtisan(sst);
                }
                _this.reloadList(sst)
            })
		$rootScope.lpa = result.data
            LxProgressService.circular.hide()
        })
    }

    _this.rowRightClick = function($event, inter) {
        edisonAPI.intervention.get(inter.id, {
                populate: 'sst'
            })
            .then(function(resp) {
                _this.contextMenu.setData(resp.data);
                _this.contextMenu.setPosition($event.pageX, $event.pageY + 200)
                _this.contextMenu.open();
            })
    }
    if (!$rootScope.lpa)
        _this.loadData()
    _this.checkArtisan = function(sst) {

        sst.checked = !sst.checked
        _.each(sst.list.getList(), function(e) {
            e.checked = sst.checked;
        })
    }
    _this.updateNumeroCheque = function(index) {
        var base = $rootScope.lpa[index].numeroCheque;
        if (base) {
            for (var i = index; i < $rootScope.lpa.length; i++) {
                if ($rootScope.lpa[i].list.getList()[0].mode === 'CHQ' /*&& _.find($rootScope.lpa[i].list.getList(), 'checked', true)*/ ) {
                    $rootScope.lpa[i].numeroCheque = base++
                }
            };
        }
    }
    _this.flushMail = function() {
        var rtn = [];

        var lpa = [];
        _.each(_.cloneDeep($rootScope.lpa), function(e) {
            e.list.__list = _.filter(e.list.__list, 'checked', true);
            if (e.list.__list.length) {
                lpa.push(e);
            }
        })
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        edisonAPI.compta.flushMail(lpa).then(function(resp) {
            LxProgressService.circular.hide()
            _this.reloadLPA()
        }, function() {

            LxProgressService.circular.hide()
            _this.reloadLPA()
        })
    }
    _this.flush = function() {
        var rtn = [];
        var lpa = [];
        _.each(_.cloneDeep($rootScope.lpa), function(e) {
            e.list.__list = _.filter(e.list.__list, 'checked', true);
            if (e.list.__list.length) {
                lpa.push(e);
            }
        })
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        edisonAPI.compta.flush(lpa).then(function(resp) {
            LxProgressService.circular.hide()
                /*edisonAPI.compta.flushMail(lpa).then(function(resp) {
                      _this.reloadLPA()
                  });*/
            alert('Les éléments ont été flushés')
        }, function() {
            LxProgressService.circular.hide()
            alert('Les éléments ont été flushés')
                /*edisonAPI.compta.flushMail(lpa).then(function(resp) {
                    LxProgressService.circular.hide()
                    _this.reloadLPA()
                });*/
        })
    }

    socket.on('intervention_db_flushMail', function(data) {
        if (data === 100) {
            $rootScope.globalProgressCounter = "";
            LxProgressService.circular.hide();
            _this.reloadLPA()
        } else {
            $rootScope.globalProgressCounter = data + '%';
        }

    })

    _this.selectToggle = function(artisan, item) {
        if (this.search.d) {
            return false;
        }
        item.checked = !item.checked;
        _this.reloadList(artisan)
    }
    _this.reloadList = function(artisan) {

        artisan.total = artisan.list.getTotal()
        artisan.total = artisan.list.getTotal(true)
        artisan.total = artisan.list.getTotal()
    }
    _this.reloadLPA = function() {
        var rtn = [];
        _.each($rootScope.lpa, function(sst) {
            _.each(sst.list.getList(), function(e) {
                if (e.checked) {
                    rtn.push(e.id);
                }
            })
        })
        _this.loadData(rtn)
    }

    _this.clickTrigger = function(elem) {
        window.setTimeout(function() {
            angular.element(elem).trigger('click');
        }, 0)
    }

    _this.onFileUpload = function(file) {
        var ids = _($rootScope.lpa).map(_.partial(_.pick, _, 'numeroCheque', 'id')).value();
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        edisonAPI.file.uploadScans(file, {
            ids: ids,
            date: _this.search.d
        }).then(function(resp) {
            LxProgressService.circular.hide()
        })
    }

    _this.print = function(type) {
        openPost('/api/intervention/print', {
            type: type,
            data: $rootScope.lpa,
            offsetX: _this.offsetX || 0,
            offsetY: _this.offsetY || 0
        });
    }
}
LpaController.$inject = ["user", "openPost", "socket", "ContextMenu", "$location", "$window", "TabContainer", "edisonAPI", "$rootScope", "LxProgressService", "LxNotificationService", "FlushList"];


angular.module('edison').controller('LpaController', LpaController);

angular.module('edison').controller('ListeArtisanController', _.noop);

var ListArtisanMapController = function($rootScope, edisonAPI, $scope) {
    var _this = this;
    $scope._ = _;
    $scope.root = $rootScope;

    edisonAPI.artisan.starList().then(function (resp) {
	_this.confirmedArtisans = resp.data;
    });

}
ListArtisanMapController.$inject = ["$rootScope", "edisonAPI", "$scope"];

angular.module('edison').controller('ListArtisanMapController', ListArtisanMapController);

angular.module('edison').controller('ListeDevisController', _.noop);

angular.module('edison').controller('ListeInterventionController', _.noop);

var listeSignalements = function(TabContainer, edisonAPI, MomentIterator, $rootScope, $filter, $scope, dialog, $location, LxNotificationService, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Liste Signalements');
    //_this.activeTab = parseInt($location.search().level || 0)
	/* Initialisation des variables de base */
	var dateNow = new Date();
	_this.selectMonth = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet",
						"Août", "Septembre", "Octobre", "Novembre", "Décembre"];
	_this.selectService = ["RECOUVREMENT", "INTERVENTION", "COMPTABILITE", "PARTENARIAT"];
	_this.selectYear = MomentIterator(new Date(2016, 1 , 1), moment(new Date()).add(1, 'year')).range('year', { format: 'YYYY' }).map(function(resp) {
		return parseInt(resp);
	})
	$scope.dataUrl = $location.search();
	_this.selectedService = $scope.dataUrl.service;
	_this.selectedUser = $scope.dataUrl.user;
	_this.selectedMonth = _this.selectMonth[dateNow.getMonth()];
	_this.selectedYear = parseInt(dateNow.getFullYear());
	$scope.dataUrl.selectedUser = _this.selectedUser;
	$scope.dataUrl.selectedMonth = _this.selectMonth.indexOf(_this.selectedMonth);
	$scope.dataUrl.selectedYear = _this.selectedYear;
	$scope.dataUrl.selectedService = _this.selectedService;
	
	/* List tous les utilisateurs ainsi que les signalements de l'utilisateur loggé */
	edisonAPI.users.list().then(function(resp) {
		for (var ind = 0; ind < resp.data.length; ind++)
		{
			if (resp.data[ind].login == $scope.dataUrl.user)
			{
				if (resp.data[ind].root)
					$scope.dataUrl.root = resp.data[ind].root;
				if (resp.data[ind].admin)
					$scope.dataUrl.admin = resp.data[ind].admin;
				$scope.dataUrl.service = resp.data[ind].service;
			}
		}
		$scope.users = $filter('orderBy')(resp.data, 'login');
		$scope.users.unshift({ login: 'all', service: $scope.dataUrl.service });
    	edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
        	$scope.pl = resp.data;
    	})
	})

	/* Affichage pop up destinataires du signalement */
	_this.showDest = function(signal) {
		dialog.pop_up_showDest_raison(signal, 0, null)
	}

	/* Affichage pop up résolution signalement */
	_this.resolveSig = function(signal) {
		dialog.pop_up_showDest_raison(signal, 1, function(resp, cancel) {
			if (cancel === true && resp == "")
				LxNotificationService.error("Pas de raison donnée.");
			else if (cancel === true && resp != "")
			{
				edisonAPI.signalement.resolve({ _id: signal._id, text: resp, inter_id: signal.inter_id}).then(function(resp) {
					edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
        				$scope.pl = resp.data;
						LxNotificationService.success("Le signalement a été résolu.");	
    				})
				})	
			}
		})
	}

	/* Afficher la resolution */
	_this.showResolve = function(signal) {
		dialog.pop_up_showDest_raison(signal, 2, null)
	}

	/* Liste les signalements du nouvel utilisateur selectionner */
	_this.changeSelected = function() {
		$scope.dataUrl.selectedUser = _this.selectedUser;
		edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
        	$scope.pl = resp.data;
    	})
	}

	/* Menu change service on change */
	_this.changeService = function() {
		$scope.dataUrl.selectedService = _this.selectedService;
		edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
			$scope.pl = resp.data;
		})
	}

	/* Menu change month on change */
	_this.changeMonth = function() {
		$scope.dataUrl.selectedMonth = _this.selectMonth.indexOf(_this.selectedMonth);
		edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
			$scope.pl = resp.data;
		})
	}

	/* Menu change year on change */
	_this.changeYear = function() {
		$scope.dataUrl.selectedYear = _this.selectedYear;
		edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
			$scope.pl = resp.data;
		})
	}
}
listeSignalements.$inject = ["TabContainer", "edisonAPI", "MomentIterator", "$rootScope", "$filter", "$scope", "dialog", "$location", "LxNotificationService", "socket"];
angular.module('edison').controller('listeSignalements', listeSignalements);

var ProspectCtrl = function($window, edisonAPI, ContextMenu, $routeParams, $rootScope, $q, $location, dialog, config, Map, LxNotificationService, user, mapAutocomplete) {
    "use strict";
    var _this = this;
    _this.data = {};
    _this.contextMenuSST = new ContextMenu('artisan')
    _this.contextMenu = _this.contextMenuSST;
    _this.config = config;
    _this.hasCalled = false;
    _this.autocomplete = mapAutocomplete

    var refreshProspect = function() {
	var cont = true;
	for (var n = 0; n < _this.nearestArtisans.length && cont === true; n++)
	{
	    if (_this.nearestArtisans[n].id === _this.data.artisan.id)
	    {
		_this.nearestArtisans[n].statutProspect = _this.data.artisan.statutProspect;
		cont = false;
	    }
	}
    }

    var removeProspect = function() {
	var cont = true;
	for (var n = 0; n < _this.nearestArtisans.length && cont === true; n++)
	{
	    if (_this.nearestArtisans[n].id === _this.data.artisan.id)
	    {
		_this.nearestArtisans.splice(n, 1);
		cont = false;
	    }
	}
    }
    
    _this.callSst = function(tel, id)
    {
	edisonAPI.artisan.exist(_this.data.artisan.email, _this.data.artisan.telephone.tel1).then(function(resp) {
	    if (resp.data === "exist")
	    {
		removeProspect();
		edisonAPI.prospect.remove(_this.data.artisan.id);
		var message = _.template("L'artisan étant déjà dans la base, le prospect va être supprimé.")(resp.data)
		LxNotificationService.success(message);
		_this.data.artisan = _this.data.sst = null;
	    }
	    else
	    {
		_this.hasCalled = true;
		if (tel) {
		    $window.open('callto:' + tel, '_self', false);
		}
		if (id)
		{
		    edisonAPI.prospect.callLog(id).then(function (resp) {
			_this.data.artisan.calls = resp.data;
			var message = _.template("L'appel a été ajouté à l'historique.")(resp.data)
			LxNotificationService.success(message);
		    }, function (error) {
			LxNotificationService.error(error.data);
		    });
		}
	    }
	});
    }

    _this.showmap = window.innerWidth > 1655
    $(window).resize(function() {
	_this.showmap = window.innerWidth > 1655
    })
    
    _this.searchArtisans = function(intervention) {
	if (_.get(intervention, 'client.address.lt')) {
	    edisonAPI.prospect.getNearest(intervention.client.address, intervention.categorie)
	        .success(function(result) {
		    _this.nearestArtisans = result
		});
	}
    }

    _this.validIBAN = function(iban) {
	return !iban || IBAN.isValid(iban);
    }

    _this.addComment = function() {

	_this.data.artisan.comments.push({
	    login: $rootScope.user.login,
	    text: _this.commentText,
	    date: new Date()
	})
	if (_this.data.artisan.id) {
	    edisonAPI.prospect.comment(_this.data.artisan.id, _this.commentText)
	}
	_this.commentText = "";
    }

    var latLng = function(add) {
	return add.lt + ', ' + add.lg
    }

    var artisanSwitch = function(sst, first) {
	_this.hasCalled = false;
	if (!sst) {
	    _this.data.artisan = null
	    return false;
	}
	$q.all([
	    edisonAPI.prospect.get(sst.id, {
		cache: false
	    }),
/*	    edisonAPI.prospect.getStats(sst.id, {
		cache: true
	    })*/
	]).then(function(result) {
	    _this.data.sst = _this.data.artisan = result[0].data;
            _this.data.sst.stats = {};
	    edisonAPI.getDistance(latLng(sst.address), latLng(_this.inter.client.address))
	        .then(function(dir) {
		    _this.data.sst.stats.direction = dir.data;
		})
	    _this.recapFltr = {
		ai: _this.data.sst.id
	    }
	});
    }

    _this.selectArtisan = function(sst, first) {
	if (_this.hasCalled == true && !_this.data.artisan.statutProspect)
	{
	    dialog.prospectCall(_this.data.artisan, function (statut) {
		if (statut !== undefined)
		{
		    _this.data.artisan.statutProspect = config.statutProspect[statut];
		    edisonAPI.prospect.save(_this.data.artisan).then(function (resp) {
			if (_this.data.artisan)
			    refreshProspect();
			artisanSwitch(sst, first);
		    });
		}
	    });
	}
	else
	    artisanSwitch(sst, first);
    }

    if ($routeParams.id)
    {
	edisonAPI.intervention.get($routeParams.id).then(function (resp) {
	    _this.inter = resp.data;
	    _this.searchArtisans(_this.inter);
	});
    }
    else if ($routeParams.addr && $routeParams.cate)
    {
	_this.inter = {}
	_this.inter.client = {}
	_this.inter.client.address = {v: $routeParams.addr};
	_this.inter.categorie = $routeParams.cate;
	_this.autocomplete.getPlaceAddress({description: $routeParams.addr}).then(function (addr) {
	    _this.inter.client.address.lt = addr.lt;
	    _this.inter.client.address.lg = addr.lg;
	    _this.searchArtisans(_this.inter);
	})
    }

    _this.findColor = function(categorie) {
	if (_this.data.artisan.statutProspect && _this.data.artisan.statutProspect.short_name === categorie.short_name)
	    return (categorie.color);
	return ("white");
    }

    _this.toggleCategorie = function(categorie) {
	if (!_this.data.artisan.statusProspect ||
	    _this.data.artisan.statutProspect.categorie.short_name != categorie.short_name)
	    _this.data.artisan.statutProspect = categorie;
	edisonAPI.prospect.save(_this.data.artisan).then(function (resp) {
	    refreshProspect();
	    var message = _.template("Les changements ont été enregistrés")(resp.data)
	    LxNotificationService.success(message);
	}, function (error) {
	    LxNotificationService.error(error.data);
	});

    }

    _this.save = function () {
	edisonAPI.prospect.save(_this.data.artisan).then(function (resp) {
	    refreshProspect();
	    var message = _.template("Les changements ont été enregistrés")(resp.data)
	    LxNotificationService.success(message);
	}, function (error) {
	    LxNotificationService.error(error.data);
	});
    }

    _this.move = function () {
	var id = _this.data.artisan.id;
	delete _this.data.artisan.id;
	delete _this.data.artisan._id;
	_this.data.artisan.transferedBy = user.login
	edisonAPI.artisan.save(_this.data.artisan).then(function (resp) {
	    _this.hasCalled = false;
	    removeProspect();
	    edisonAPI.prospect.remove(id);
	    var message = _.template("La fiche artisan à bien été crée")(resp.data)
	    LxNotificationService.success(message);
	    $location.url("/artisan/" + resp.data.id.toString());
	}, function (error) {
	    _this.data.artisan.id = id;
	    _this.data.artisan._id = id;
	    LxNotificationService.error(error.data);
	});
    }

}
ProspectCtrl.$inject = ["$window", "edisonAPI", "ContextMenu", "$routeParams", "$rootScope", "$q", "$location", "dialog", "config", "Map", "LxNotificationService", "user", "mapAutocomplete"];

angular.module('edison').controller('ProspectController', ProspectCtrl);

var SearchController = function(edisonAPI, TabContainer, $routeParams, $location, LxProgressService, config) {
    var tab = TabContainer.getCurrentTab();
    tab.setTitle('Search')
    var _this = this;
    _this.config = config;
    _this.routeParams = $routeParams
    LxProgressService.circular.show('#5fa2db', '#globalProgress');
    edisonAPI.bigSearch($routeParams.query).success(function(resp) {
        LxProgressService.circular.hide()
        _this.data = resp
    })
    _this.openLink = function(link) {
        $location.url(link)
    }
    _this.open = function(url) {
        $location.url(url);
    }
}
SearchController.$inject = ["edisonAPI", "TabContainer", "$routeParams", "$location", "LxProgressService", "config"];

angular.module('edison').controller('SearchController', SearchController);

var GStatsController = function(MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope, $location, LxProgressService, socket) {
  "use strict";
  edisonAPI.intervention.gstats().then(function(resp) {
    $('#chartContainer').highcharts({
      chart: {
        type: 'column'
      },
      title: {
        text: 'Pourcentage de paiements'
      },
      xAxis: {
        categories:resp.data.categories
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total fruit consumption'
        }
      },
      tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
      },
      plotOptions: {
        column: {
          stacking: 'percent'
        }
      },
      series: resp.data.series
    })
  })
}
GStatsController.$inject = ["MomentIterator", "TabContainer", "$routeParams", "edisonAPI", "$rootScope", "$scope", "$location", "LxProgressService", "socket"];
angular.module('edison').controller('GStatsController', GStatsController);

var StatsNewController = function(dialog, MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope,
  $location, LxProgressService, socket) {
  "use strict";
  var _this = this;
  _this.tab = TabContainer.getCurrentTab();
  _this.tab.setTitle('Stats');
  $scope.showAll = window.app_env === 'DEVELOPMENT'
  if (!$scope.showAll) {
      dialog.getPassword("new", function(err, resp) {
      if (resp === "OK") {
        $scope.showAll = true
      }
    })
  }


  var end = new Date();
  var start = new Date(2013, 8, 1)
  _this.dateSelect = MomentIterator(start, end).range('month').map(function(e) {
    return {
      t: e.format('MMM YYYY'),
      m: e.month() + 1,
      y: e.year(),
    }
  }).reverse()
  var dateTarget = _.pick(_this.dateSelect[0], 'm', 'y');
  _this.yearSelect = MomentIterator(start, moment(end).add(1,'year')).range('year', {
    format: 'YYYY',
  }).map(function(e) {

    return parseInt(e)
  })



  var end2 = new Date();
  var start2 = new Date(2015, 8, 1)
  _this.dateSelect2 = MomentIterator(start2, end2).range('month').map(function(e) {
    return {
      t: e.format('MMM YYYY'),
      m: e.month() + 1,
      y: e.year(),
    }
  }).reverse()
  var dateTarget = _.pick(_this.dateSelect2[0], 'm', 'y');
  _this.yearSelect2 = MomentIterator(start, moment(end2).add(1,'year')).range('year', {
    format: 'YYYY',
  }).map(function(e) {

    return parseInt(e)
  })



  var end3 = new Date();
  var start3 = new Date(2017, 4, 1)
  _this.dateSelect3 = MomentIterator(start3, end3).range('month').map(function(e) {
    return {
      t: e.format('MMM YYYY'),
      m: e.month() + 1,
      y: e.year(),
    }
  }).reverse()
  var dateTarget = _.pick(_this.dateSelect3[0], 'm', 'y');
  _this.yearSelect3 = MomentIterator(start, moment(end3).add(1,'year')).range('year', {
    format: 'YYYY',
  }).map(function(e) {

    return parseInt(e)
  })





  var getChart = function(type, title, series, categories) {


    return {
      chart: {
        zoomType: 'x',
        type: type
      },
      title: {
        text: title
      },
      xAxis: {
        categories: categories
      },
      yAxis: {
        min: 0,
        title: {
          text: "Chiffre"
        },
      },
      tooltip: {
        shared: true,
        valueSuffix: ' €'
      },
      plotOptions: {
        animation: false,
        column: {
          pointPadding: 0,
          groupPadding: 0.04,
          borderWidth: 0,
          animation: false,
          //  stacking: 'normal',
        },
        area: {
          stacking: 'normal',
          lineColor: '#666666',
          lineWidth: 1,
          marker: {
            lineWidth: 1,
            lineColor: '#666666'
          }
        },
        areaspline: {
          stacking: 'normal',
        }
      },
      series: series



    }
  }
        _this.dividerSelect = [
        'categorie',
        'chiffre',
	      'chiffre2',
        'telepro',
        'suivi'

    ]
  $scope.selectedDivider = 'chiffre'


  _this.typeSelect = [
        'column',
        'areaspline',
        'area',
        'line',
        'pie',
        'bar',
        'spline',
    ]
  $scope.selectedType = 'column'
//edisonAPI.save_stats.saveStats()
  var monthChange = function() {
    edisonAPI.intervention.statsBen({
      month: $scope.selectedDate.m,
      year: $scope.selectedDate.y,
      group: 'day',
      model: 'ca',
      divider: $scope.selectedDivider,
    }).then(function(resp) {
	var d = resp.data;
      $('#chartContainer').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories));
    });
  }

  var yearChange = function() {
    edisonAPI.intervention.statsBen({
      year: $scope.selectedDate.y,
      group: 'month',
      model: 'ca',
      divider: $scope.selectedDivider,
    }).then(function(resp) {
      setTotal(resp.data);
      var d = resp.data;
      $('#chartContainer2').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories));
    });
  }

  var weekChange = function() {
    edisonAPI.intervention.statsBen({
      year: $scope.selectedDate.y,
      group: 'week',
      model: 'ca',
      divider: $scope.selectedDivider,
    }).then(function(resp) {
      var d = resp.data;
	$('#chartContainer3').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories));
    });
  }



    var suiviChange = function() {
    edisonAPI.intervention.statsBen({
      year0: $scope.selectedDate3.y,
      month0: $scope.selectedDate3.m,
      year: $scope.selectedDate2.y,
      group: 'month2',
      model: 'Suivi',
      divider: "suivi2",
    }).then(function(resp) {
      var d = resp.data;
  $('#chartContainer4').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories));
    });
  }


  $scope.$watch("selectedType", function()  {
    monthChange();
    yearChange();
    weekChange();

  });

  var setTotal = function(data) {
    $scope.totalYear =   {
      recu: 0,
      potentiel: 0,
    }
    _.times(data.categories.length, function(i) {
      $scope.totalYear[data.series[0].name] += data.series[0].data[i]
      $scope.totalYear[data.series[1].name] += data.series[1].data[i]
    })
  }

  $scope.$watch("selectedDivider", function()  {

    monthChange();
    yearChange();
    weekChange();

  });

  $scope.$watch("selectedYear", function() {

    yearChange();
    weekChange();

  });
  /*$scope.$watch("selectedDate2", function() {

    suiviChange();
  });*/
///////////////////////////////////////////////////////////////////////////////////////
  $scope.$watch("selectedDate2", function(curr) {
    if (!curr ||  !curr.m || !curr.y)
      return false;
    $location.search('m', curr.m);
    $location.search('y', curr.y);
    suiviChange();
  }, true);


  $scope.$watch("selectedDate3", function(curr) {
    if (!curr ||  !curr.m || !curr.y)
      return false;
    $location.search('m', curr.m);
    $location.search('y', curr.y);
    suiviChange();
  }, true);


///////////////////////////////////////////////////////////////////////////////////////////
  $scope.$watch("selectedDate", function(curr) {
    if (!curr ||  !curr.m || !curr.y)
      return false;
    $location.search('m', curr.m);
    $location.search('y', curr.y);
    monthChange(curr);
    yearChange();
    weekChange();
  }, true);
    if ($location.search().m)  {
    dateTarget.m = parseInt($location.search().m)
  }
    if ($location.search().y)  {
    dateTarget.y = parseInt($location.search().y)
  }
  $scope.selectedDate3 = _.find(_this.dateSelect3, dateTarget)
  $scope.selectedDate2 = _.find(_this.dateSelect2, dateTarget)
  $scope.selectedDate = _.find(_this.dateSelect, dateTarget)
  $scope.selectedYear = $scope.selectedDate.y.toString();
}
StatsNewController.$inject = ["dialog", "MomentIterator", "TabContainer", "$routeParams", "edisonAPI", "$rootScope", "$scope", "$location", "LxProgressService", "socket"];
angular.module('edison').controller('StatsNewController', StatsNewController);

var StatsPartController = function(dialog, MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope,
				   $location, LxProgressService, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Stats');
    $scope.showAll = window.app_env === 'DEVELOPMENT'
    if (!$scope.showAll) {
	dialog.getPassword("part", function(err, resp) {
	    if (resp === "OK") {
		$scope.showAll = true
	    }
	})
    }


    var end = new Date();
    var start = new Date(2013, 8, 1)
    _this.dateSelect = MomentIterator(start, end).range('month').map(function(e) {
	return {
	    t: e.format('MMM YYYY'),
	    m: e.month() + 1,
	    y: e.year(),
	}
    }).reverse()
    var dateTarget = _.pick(_this.dateSelect[0], 'm', 'y');
    _this.yearSelect = MomentIterator(start, moment(end).add(1,'year')).range('year', {
	format: 'YYYY',
    }).map(function(e) {
	return parseInt(e)
    })

    var getChart = function(type, title, series, categories) {


	return {
	    chart: {
		zoomType: 'x',
		type: type
	    },
	    title: {
		text: title
	    },
	    xAxis: {
		categories: categories
	    },
	    yAxis: {
		min: 0,
		title: {
		    text: "Chiffre"
		},
	    },
	    tooltip: {
		shared: true,
		valueSuffix: ' €'
	    },
	    plotOptions: {
		animation: false,
		column: {
		    pointPadding: 0,
		    groupPadding: 0.04,
		    borderWidth: 0,
		    animation: false,
		    //  stacking: 'normal',
		},
		area: {
		    stacking: 'normal',
		    lineColor: '#666666',
		    lineWidth: 1,
		    marker: {
			lineWidth: 1,
			lineColor: '#666666'
		    }
		},
		areaspline: {
		    stacking: 'normal',
		}
	    },
	    series: series
	}
    }
    _this.dividerSelect = [
        'categorie',
        'chiffre',
        'telepro'
    ]
    $scope.selectedDivider = 'chiffre'


    _this.typeSelect = [
        'column',
        'areaspline',
        'area',
        'line',
        'pie',
        'bar',
        'spline',
    ]
    $scope.selectedType = 'column'


    var monthChange = function() {
	edisonAPI.intervention.statsBen({
	    month: $scope.selectedDate.m,
	    year: $scope.selectedDate.y,
	    group: 'day',
	    model: 'ca',
	    divider: $scope.selectedDivider,
	    partenariat: true
	}).then(function(resp) {
	    var d = resp.data;
	    $('#chartContainer').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories));
	});
    }

    var yearChange = function() {
	edisonAPI.intervention.statsBen({
	    year: $scope.selectedDate.y,
	    group: 'month',
	    model: 'ca',
	    divider: $scope.selectedDivider,
	    partenariat: true
	}).then(function(resp) {
	    setTotal(resp.data);
	    var d = resp.data;
	    $('#chartContainer2').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories));
	});
    }

    var weekChange = function() {
	edisonAPI.intervention.statsBen({
	    year: $scope.selectedDate.y,
	    group: 'week',
	    model: 'ca',
	    divider: $scope.selectedDivider,
	    partenariat: true
	}).then(function(resp) {
	    var d = resp.data;
	    $('#chartContainer3').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories));
	});
    }


    $scope.$watch("selectedType", function()  {
	monthChange();
	yearChange();
	weekChange();
    });

    var setTotal = function(data) {
	$scope.totalYear =   {
	    recu: 0,
	    potentiel: 0,
	}
	_.times(data.categories.length, function(i) {
	    $scope.totalYear[data.series[0].name] += data.series[0].data[i]
	    $scope.totalYear[data.series[1].name] += data.series[1].data[i]
	})
    }

    $scope.$watch("selectedDivider", function()  {
	monthChange();
	yearChange();
	weekChange();
    });

    $scope.$watch("selectedYear", function() {

	yearChange();
	weekChange();

    });
    $scope.$watch("selectedDate", function(curr) {
	if (!curr ||  !curr.m || !curr.y)
	    return false;
	$location.search('m', curr.m);
	$location.search('y', curr.y);
	monthChange(curr);
	yearChange();

    }, true);
    if ($location.search().m)  {
	dateTarget.m = parseInt($location.search().m)
    }
    if ($location.search().y)  {
	dateTarget.y = parseInt($location.search().y)
    }
    $scope.selectedDate = _.find(_this.dateSelect, dateTarget)
    $scope.selectedYear = $scope.selectedDate.y.toString();
}
StatsPartController.$inject = ["dialog", "MomentIterator", "TabContainer", "$routeParams", "edisonAPI", "$rootScope", "$scope", "$location", "LxProgressService", "socket"];
angular.module('edison').controller('StatsPartController', StatsPartController);

var StatsController = function(DateSelect, TabContainer, $routeParams, edisonAPI, $rootScope, $scope, $location, LxProgressService, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Stats');


    var dateSelect = new DateSelect;
    _this.yearSelect = [];
    _.times(dateSelect.current.y - dateSelect.start.y + 1, function(k) {
        _this.yearSelect.push(dateSelect.start.y + k);
    })
    $scope.selectedYear = dateSelect.current.y

    $scope.$watch("selectedYear", function(curr) {
        edisonAPI.intervention.statsBen({
            y: curr
        }).then(function(resp) {
            $('#chartContainer2 > *').remove()
            var svg = dimple.newSvg("#chartContainer2", 1070, 400);
            var myChart = new dimple.chart(svg, resp.data);
            myChart.setBounds(60, 30, 1000, 300)
            var x = myChart.addCategoryAxis("x", "mth");
            var y = myChart.addMeasureAxis("y", "montant");
            y.tickFormat = ',.0f';
            myChart.addSeries("potentiel", dimple.plot.bar);
            myChart.addLegend(60, 10, 410, 20, "right");
            myChart.draw();

            $scope.totalYear = {
                potentiel: 0,
                recu: 0
            }

            _.each(resp.data, function(e) {
                $scope.totalYear[e.potentiel ? 'potentiel' : 'recu'] += e.montant
            })
        })
    });




    $scope.$watch("selectedDate", function(curr) {
        if (!curr ||  !curr.m || !curr.y)
            return false;
        $location.search('m', curr.m);
        $location.search('y', curr.y);
        edisonAPI.intervention.statsBen(curr).then(function(resp) {
            $('#chartContainer > *').remove()
            var svg = dimple.newSvg("#chartContainer", 1300, 400);
            var myChart = new dimple.chart(svg, resp.data);
            myChart.setBounds(60, 30, 1000, 300)
            var x = myChart.addCategoryAxis("x", "day");
            //x.addOrderRule("dt");
            var y = myChart.addMeasureAxis("y", "prix");
            y.tickFormat = ',.0f';
            myChart.addSeries("recu", dimple.plot.bar);
            //myChart.addPctAxis("y", "paye");
            myChart.assignColor("En Attente", "#2196F3");
            myChart.assignColor("Encaissé", "#4CAF50");
            myChart.addLegend(60, 10, 410, 20, "right");
            myChart.draw();

        })
    })
    if ($location.search().m)  {
        dateSelect.current.m = parseInt($location.search().m)
    }
    if ($location.search().y)  {
        dateSelect.current.y = parseInt($location.search().y)
    }
    _this.dateSelect = dateSelect.list()
    $scope.selectedDate = _.find(dateSelect.list(), dateSelect.current)
}
StatsController.$inject = ["DateSelect", "TabContainer", "$routeParams", "edisonAPI", "$rootScope", "$scope", "$location", "LxProgressService", "socket"];
angular.module('edison').controller('StatsController', StatsController);

var StatsRecouvrementController = function(dialog, MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope,
  $location, LxProgressService, socket) {
  "use strict";
  var _this = this;
  _this.tab = TabContainer.getCurrentTab();
  _this.tab.setTitle('Stats');
  $scope.showAll = window.app_env === 'DEVELOPMENT'
  if (!$scope.showAll) {
      dialog.getPassword("recouv", function(err, resp) {
      if (resp === "OK") {
        $scope.showAll = true
      }
    })
  }


  var end = new Date();
  var start = new Date(2016, 3, 1)
  _this.dateSelect = MomentIterator(start, end).range('month').map(function(e) {
    return {
      t: e.format('MMM YYYY'),
      m: e.month() + 1,
      y: e.year(),
    }
  }).reverse()
  var dateTarget = _.pick(_this.dateSelect[0], 'm', 'y');
  _this.yearSelect = MomentIterator(start, moment(end).add(1,'year')).range('year', {
    format: 'YYYY',
  }).map(function(e) {

    return parseInt(e)
  })

  var getChart = function(type, title, series, categories,texte, suffixe) {


    return {
      chart: {
        zoomType: 'x',
        type: type
      },
      title: {
        text: title
      },
      xAxis: {
        categories: categories
      },
      yAxis: {
        min: 0,
        title: {
          text: texte //'Chiffre'
        },
      },
      tooltip: {
        shared: true,
        valueSuffix: suffixe//' '//' €'
      },
      plotOptions: {
        animation: false,
        column: {
          pointPadding: 0,
          groupPadding: 0.04,
          borderWidth: 0,
          animation: false,
          //  stacking: 'normal',
        },
        area: {
          stacking: 'normal',
          lineColor: '#666666',
          lineWidth: 1,
          marker: {
            lineWidth: 1,
            lineColor: '#666666'
          }
        },
        areaspline: {
          stacking: 'normal',
        }
      },
      series: series



    }
  }
        _this.dividerSelect = [
        'nombre_litige',
        'nombre_recouvrement',
        'montant_litige',
        'montant_recouvrement'

    ]
  $scope.selectedDivider = 'nombre_litige'


  _this.typeSelect = [
        'column',
        'areaspline',
        'area',
        'line',
        'pie',
        'bar',
        'spline',
    ]
  $scope.selectedType = 'column'


  var graph1Change = function() {
    edisonAPI.intervention.statsRecou({
      month: $scope.selectedDate.m,
      year: $scope.selectedDate.y,
      group: 'graph1',
      model: 'graph1',
      divider: $scope.selectedDivider,
    }).then(function(resp) {
	var d = resp.data;
      $('#chartContainer').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories,d.texte,d.suffixe));
    });
  }

  var graph2Change = function() {
    edisonAPI.intervention.statsRecou({
      year: $scope.selectedDate.y,
      group: 'graph2',
      model: 'graph2',
      divider: $scope.selectedDivider,
    }).then(function(resp) {
      setTotal(resp.data);
      var d = resp.data;
      $('#chartContainer2').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories,d.texte,d.suffixe));
    });
  }

  var graph3Change = function() {
    edisonAPI.intervention.statsRecou({
      year: $scope.selectedDate.y,
      group: 'graph3',
      model: 'graph3',
      divider: $scope.selectedDivider,
    }).then(function(resp) {
      var d = resp.data;
	$('#chartContainer3').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories,d.texte,d.suffixe));
    });
  }
  var graph4Change = function() {
    edisonAPI.intervention.statsRecou({
      year: $scope.selectedDate.y,
      group: 'graph4',
      model: 'graph4',
      divider: $scope.selectedDivider,
    }).then(function(resp) {
      var d = resp.data;
  $('#chartContainer4').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories,d.texte,d.suffixe));
    });
  }
  var graph5Change = function() {
    edisonAPI.intervention.statsRecou({
      year: $scope.selectedDate.y,
      group: 'graph5',
      model: 'graph5',
      divider: $scope.selectedDivider,
    }).then(function(resp) {
      var d = resp.data;
  $('#chartContainer5').highcharts(getChart($scope.selectedType, d.title, d.series, d.categories,d.texte,d.suffixe));
    });
  }


  $scope.$watch("selectedType", function()  {
    graph1Change();
    graph2Change();
    graph3Change();
    graph4Change();
    graph5Change();
  });

  var setTotal = function(data) {
    $scope.totalYear =   {
      recu: 0,
      potentiel: 0,
    }
    _.times(data.categories.length, function(i) {
      $scope.totalYear[data.series[0].name] += data.series[0].data[i]
      $scope.totalYear[data.series[1].name] += data.series[1].data[i]
    })
  }

  $scope.$watch("selectedDivider", function()  {
    graph1Change();
    graph2Change();
    graph3Change();
    graph4Change();
    graph5Change();

  });

  $scope.$watch("selectedYear", function() {

    graph2Change();
    graph3Change();

  });
  $scope.$watch("selectedDate", function(curr) {
    if (!curr ||  !curr.m || !curr.y)
      return false;
    $location.search('m', curr.m);
    $location.search('y', curr.y);
    graph1Change(curr);
    graph2Change();
    graph3Change();
    graph4Change();
    graph5Change();

  }, true);
    if ($location.search().m)  {
    dateTarget.m = parseInt($location.search().m)
  }
    if ($location.search().y)  {
    dateTarget.y = parseInt($location.search().y)
  }
  $scope.selectedDate = _.find(_this.dateSelect, dateTarget)
  $scope.selectedYear = $scope.selectedDate.y.toString();
}
StatsRecouvrementController.$inject = ["dialog", "MomentIterator", "TabContainer", "$routeParams", "edisonAPI", "$rootScope", "$scope", "$location", "LxProgressService", "socket"];
angular.module('edison').controller('StatsRecouvrementController', StatsRecouvrementController);

var userHistory = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, socket) {
	"use strict";
	var _this = this;
	_this.tab = TabContainer.getCurrentTab();
	_this.tab.setTitle('User History');
	edisonAPI.user.history($location.search().login).then(function(resp) {
		$scope.history = resp.data
	})
	_this.xclick = function(h) {
		_this.selectedRow = (_this.selectedRow == h.date ? null : h.date);
	}


}
userHistory.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "socket"];

angular.module('edison').controller('userHistory', userHistory);

var chiffreGrandComptes = function(config, edisonAPI, MomentIterator, $scope) {
    "use strict";
    var _this = this;
    _this.config = config;

    var start = new Date("01/01/2013");
    var end = new Date();
    _this.yearSelect = MomentIterator(start, end).range('year', {
	format: 'YYYY',
    }).map(function(e) {
	return parseInt(e)
    })

    $scope.selectedYear = _this.yearSelect[_this.yearSelect.length - 1];
    
    _this.loadData = function () {
	var obj = {};
	obj.year = $scope.selectedYear;
	edisonAPI.compte.list().then(function (compte) {
	    edisonAPI.compta.getCA(obj).then(function (resp) {
		_this.years = resp.data
		_this.info = [];
		for (var n in resp.data)
		{
		    var total = 0;
		    for (var l = 0; l < resp.data[n].length; l++)
			total += resp.data[n][l]
		    var ref = n;
		    for (var l = 0; l < compte.data.length; l++)
		    {
			if (compte.data[l].ref.toUpperCase() === ref)
			    ref = compte.data[l].nom
		    }
		    _this.info.push({name: ref, total: total, ref: n});
		}
	    })
	})
    }
    
    _this.xsort = function(sb) {
	_this.sb = sb
    }

    $scope.$watch("selectedYear", function() {
	_this.loadData();
    });

}
chiffreGrandComptes.$inject = ["config", "edisonAPI", "MomentIterator", "$scope"];

angular.module('edison').filter("toArray", function (){
    return function (obj) {
	var res = [];
	angular.forEach(obj, function (val, key){
	    res.push(val);
	})
	return res;
    }
}).controller('chiffreGrandComptes', chiffreGrandComptes);

var ComLitiges = function(MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope, $location, LxProgressService, socket, config) {
    "use strict";

    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Coms.');

    _this.xcalc = function(e) {
        return _.round((e.compta.reglement.montant || e.compta.paiement.base || e.prixFinal) * 0.01, 2);
    }

    _this.getTotal = function() {
        var rtn = {
            com: 0,
            all: 0
        }
        _.each($scope.list, function(x) {
            rtn.com += _this.xcalc(x);
            rtn.all += x.compta.reglement.montant || 0
        })
        return rtn;
    }
    var end = new Date();
    var start = new Date(2013, 8, 1)
    _this.dateSelect = MomentIterator(start, end).range('month').map(function(e) {
        return {
            t:e.format('MMM YYYY'),
            m:e.month() + 1,
            y:e.year(),
        }
    }).reverse()

    var dateTarget = _.pick(_this.dateSelect[0], 'm', 'y');

    var actualise = _.debounce(function() {
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        edisonAPI.intervention.commissions(_.merge($scope.selectedDate)).then(function(resp) {
            LxProgressService.circular.hide();
            $scope.list = resp.data
	    console.log($scope.list);
            $scope.total = _this.getTotal()
        })
    }, 50)
    $scope.$watch("selectedDate", function(curr, prev) {
        $location.search('m', curr.m);
        $location.search('y', curr.y);
        actualise();
    })
    if ($location.search().m)  {
        dateTarget.m = parseInt($location.search().m)
    }
    if ($location.search().y)  {
        dateTarget.y = parseInt($location.search().y)
    }
    $scope.selectedDate = _.find(_this.dateSelect, dateTarget)
}
ComLitiges.$inject = ["MomentIterator", "TabContainer", "$routeParams", "edisonAPI", "$rootScope", "$scope", "$location", "LxProgressService", "socket", "config"];
angular.module('edison').controller('ComLitiges', ComLitiges);

var commissionsPartenariat = function(MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope,$location, LxProgressService, socket, openPost,  LxNotificationService) {
    "use strict";
  var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.dateSelectList = MomentIterator(new Date(2016, 1, 0),moment().add(1, 'months').toDate()).range('month').map(function(e) {
	return {
	    date: new Date(e),
	    name: moment(e).format("MM[/]YYYY")
	}
    })

    _this.xsort = function(sb) {
	_this.sb = sb
    }
    _this.onChanges = function () {
	if ($scope.choice)
	{
	var path = "/CommissionsPartenariat/" + $scope.choice;
	openPost('/api/document/see?name=' + path ,{
	    html: true
	})
	}
	else
	{
	    LxNotificationService.error("Veuillez mettre un fichier à telecharger");	    
	}
//	$location.path('/api/document/see?name=' + path)
    }
    
    _this.changeSelectedDate = function() {
	LxProgressService.circular.show('#5fa2db', '#globalProgress');
	edisonAPI.artisan.tableauCom(_this.selectedDate).then(function(resp) {
	    LxProgressService.circular.hide()
	    _this.data = resp.data
	    
	})
    }
    edisonAPI.artisan.filesCom().then(function(resp) {
	_this.files = resp.data
    })

    _this.validateArt = function () {
	edisonAPI.artisan.validateArt();
	LxNotificationService.success("Operation reussie");
    }
    
    _this.Csv = function (){
	var result = []
	var tab =  _this.data;
	var res = ["Société","Status","SubStatus","Nombre d'inters total","Total Paye","Intervention avec comission à payer","Comission","montant"];
	var prixtot = 0;
	var comtot = 0;
	result.push(res);
	for (var i = 0;tab.length > i ;i++)
	{
	    var inter = []
	    var nbr = Math.floor(tab[i].totalImpaye / 10)
	    if (nbr != 0)
	    {
		
		inter.push(tab[i].nomSociete)
		inter.push(tab[i].status)
		inter.push(tab[i].subStatus)
		inter.push(tab[i].nbrIntervention)
		var calc = Math.floor(tab[i].totalPaye)
		inter.push(Math.floor(tab[i].totalPaye / 10))
		inter.push(Math.floor(tab[i].nbrIntervention - calc))
		inter.push(nbr) 
		inter.push(nbr * 15)
		prixtot = prixtot + nbr * 15
		comtot = comtot + nbr
		result.push(inter)
	    }
	}
	result.push( [" "," "," "," "," "," "," "," "],["Total"," "," "," "," "," ",comtot, " "],["Montant"," "," "," "," " ," "," ",prixtot + "€"])
	edisonAPI.artisan.pdfcom(result).then(function(res){
	})
	LxNotificationService.success("Exportation Réussie");
    }
    _this.selectedDate = new Date(_this.dateSelectList[_this.dateSelectList.length - 1].date)
    _this.changeSelectedDate();
    _this.tab.setTitle('Coms.');
    //   edisonAPI.artisan.tableauCom().then(function(resp) {
    //    LxProgressService.circular.hide()
    //    _this.data = resp.data
    //  })
}
commissionsPartenariat.$inject = ["MomentIterator", "TabContainer", "$routeParams", "edisonAPI", "$rootScope", "$scope", "$location", "LxProgressService", "socket", "openPost", "LxNotificationService"];
angular.module('edison').controller('commissionsPartenariat', commissionsPartenariat);

var ComRecouvrement = function(MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope, $location, LxProgressService, socket, config) {
    "use strict";

    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Coms.');

    _this.xcalc = function(e) {
        return _.round((e.compta.reglement.montant || e.compta.paiement.base || e.prixFinal) * 0.01, 2);
    }

    _this.getTotal = function() {
        var rtn = {
            com: 0,
            all: 0
        }
        _.each($scope.list, function(x) {
            rtn.com += _this.xcalc(x);
            rtn.all += x.compta.reglement.montant || 0
        })
        return rtn;
    }
    var end = new Date();
    var start = new Date(2013, 8, 1)
    _this.dateSelect = MomentIterator(start, end).range('month').map(function(e) {
        return {
            t:e.format('MMM YYYY'),
            m:e.month() + 1,
            y:e.year(),
        }
    }).reverse()

    var dateTarget = _.pick(_this.dateSelect[0], 'm', 'y');

    var actualise = _.debounce(function() {
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        edisonAPI.intervention.commissions(_.merge($scope.selectedDate, {
            r: true
        })).then(function(resp) {
            LxProgressService.circular.hide();
            $scope.list = resp.data
	    console.log($scope.list);
            $scope.total = _this.getTotal()
        })
    }, 50)
    $scope.$watch("selectedDate", function(curr, prev) {
        $location.search('m', curr.m);
        $location.search('y', curr.y);
        actualise();
    })
    if ($location.search().m)  {
        dateTarget.m = parseInt($location.search().m)
    }
    if ($location.search().y)  {
        dateTarget.y = parseInt($location.search().y)
    }
    $scope.selectedDate = _.find(_this.dateSelect, dateTarget)
}
ComRecouvrement.$inject = ["MomentIterator", "TabContainer", "$routeParams", "edisonAPI", "$rootScope", "$scope", "$location", "LxProgressService", "socket", "config"];
angular.module('edison').controller('ComRecouvrement', ComRecouvrement);

var CommissionsController = function(MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope, $location, LxProgressService, socket) {
    "use strict";

    // harald - Grand Compte - 1 janvier 2016 => 2%

    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Coms.');

    _this.xcalc = function(e) {
        if ((new Date(e.date.ajout)).getFullYear() >= 2016 && e.facture && e.facture.payeur === 'GRN' && e.login.ajout === 'harald_x' ) {
          e.exception1 = true
          return _.round((e.compta.reglement.montant || e.compta.paiement.base || e.prixFinal) * 0.02, 2);
        }
        if ((new Date(e.date.ajout)).getFullYear() >= 2016 && e.categorie === 'VT' && (e.login.ajout === 'adrien_c' || e.login.ajout === 'gregoire_e') ) {
          e.exception2 = true
          return _.round((e.compta.reglement.montant || e.compta.paiement.base || e.prixFinal) * 0.005, 2);
        }
        return e.categorie === 'VT' ? 1.5 : _.round((e.compta.reglement.montant || e.compta.paiement.base || e.prixFinal) * 0.01, 2);
    }

    _this.getTotal = function() {
        var rtn = {
            com: 0,
            all: 0
        }
        _.each($scope.list, function(x) {
            rtn.com += _this.xcalc(x);
            rtn.all += x.compta.reglement.montant || 0
        })
        return rtn;
    }
    var end = new Date();
    var start = new Date(2013, 8, 1)
    _this.dateSelect = MomentIterator(start, end).range('month').map(function(e) {
        return {
            t:e.format('MMM YYYY'),
            m:e.month() + 1,
            y:e.year(),
        }
    }).reverse()

    var dateTarget = _.pick(_this.dateSelect[0], 'm', 'y');
    $scope.usrs = _.filter(window.app_users, 'service', 'INTERVENTION');

    $scope.selectedUser = $location.search().l ||  $scope.usrs[0].login

    var actualise = _.debounce(function() {
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        edisonAPI.intervention.commissions(_.merge($scope.selectedDate, {
            l: $scope.selectedUser
        })).then(function(resp) {
            LxProgressService.circular.hide();
            $scope.list = resp.data
            $scope.total = _this.getTotal()
        })
    }, 50)
    $scope.$watch("selectedUser", function(curr, prev) {
        $location.search('l', curr);
        actualise();
        /* */
    })
    $scope.$watch("selectedDate", function(curr, prev) {
        $location.search('m', curr.m);
        $location.search('y', curr.y);
        actualise();
    })
    if ($location.search().m)  {
        dateTarget.m = parseInt($location.search().m)
    }
    if ($location.search().y)  {
        dateTarget.y = parseInt($location.search().y)
    }
    $scope.selectedDate = _.find(_this.dateSelect, dateTarget)
}
CommissionsController.$inject = ["MomentIterator", "TabContainer", "$routeParams", "edisonAPI", "$rootScope", "$scope", "$location", "LxProgressService", "socket"];
angular.module('edison').controller('CommissionsController', CommissionsController);

var editAcquittance = function(config,  MomentIterator, TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, socket) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.log = {}

    _this.weekDays = function () {
	return (['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'])
    }

    _this.getUserList = function () {
	edisonAPI.users.list().then(function (resp) {
	    $scope.users = resp.data
	    _this.userList = resp.data
	    var d = new Date();
	    var tab = [new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0))]
	    var tab2 = [new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0))]
	    for (var n = 0; n < _this.userList.length; n++)
	    {
		if (_this.userList[n].activated === true && _this.userList[n].hours.length === 0)
		{
		    _this.userList[n].hours = tab
		}
		else if (_this.userList[n].activated === true && _this.userList[n].hoursf.length === 0)
		{
		    _this.userList[n].hoursf = tab2
		}
		else
		{
		    for (var l = 0; l < _this.userList[n].hours.length; l++)
		    {
			_this.userList[n].hours[l] = new Date(_this.userList[n].hours[l]);
			_this.userList[n].hoursf[l] = new Date(_this.userList[n].hoursf[l]);
		    }
		}
	    }
	})
    }
    
    _this.save = function() {
	edisonAPI.users.save(_this.userList).then(function() {
	    LxNotificationService.success("Les utilisateurs on été mis a jour");
	}, function(err) {
	    LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
	})
    }
    
    _this.getUserList();
}
editAcquittance.$inject = ["config", "MomentIterator", "TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "socket"];
angular.module('edison').controller('editAcquittance', editAcquittance);

var editCombos = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Combos');


    var base = {
        "id": 29300,
        "categorie": "PL",
        "description": "RECHERCHE DE PANNE ELCTRIQUE",
        "sst": 31,
        "file": [],
        "tva": 10,
        "coutFourniture": 0,
        "enDemarchage": false,
        "aDemarcher": false,
        "reglementSurPlace": false,
        "prixFinal": 0,
        "prixAnnonce": 130,
        "modeReglement": "CH",
        "fourniture": [],
        "produits": [],
        "remarque": "Pas de remarque(s)",
        "descriptionTags": [],
        "artisan": {
            "id": 31,
            "nomSociete": "SODESEN"
        },
        "savEnCours": true,
        "litigesEnCours": true,
        "litiges": [],
        "sav": [],
        "client": {
            "civilite": "M.",
            "nom": "DELORME",
            "email": "",
            "location": [
                45.7592,
                4.77779
            ],
            "address": {
                "n": "19",
                "r": "RUE DES CERISIERS",
                "v": "TASSIN-LA-DEMI-LUNE",
                "cp": "69160",
                "lt": 45.7592,
                "lg": 4.77779
            },
            "telephone": {
                "tel1": "0478346059",
                "origine": "0478346059"
            },
            "prenom": "CHRISTIAN"
        },
        "facture": {
            "civilite": "M.",
            "nom": "DELORME",
            "email": "",
            "location": [
                45.7592,
                4.77779
            ],
            "address": {
                "n": "19",
                "r": "RUE DES CERISIERS",
                "v": "TASSIN-LA-DEMI-LUNE",
                "cp": "69160",
                "lt": 45.7592,
                "lg": 4.77779
            },
            "telephone": {
                "tel1": "0478346059",
                "origine": "0478346059"
            },
            "prenom": "CHRISTIAN"
        },
        "produits": [],
        "historique": [],
        "comments": [],
        "date": {
            "intervention": "2015-09-17T11:00:00.000Z",
            "envoi": "2015-09-17T09:11:14.000Z",
            "ajout": "2015-09-17T09:11:14.000Z"
        },
        "login": {
            "ajout": "clement_b",
            "envoi": "clement_b"
        },
        "status": "ENC"
    }



    edisonAPI.combo.list().then(function(resp) {
        $scope.plSave = resp.data
        $scope.pl = _.map(resp.data, _this.extend);
    })

    _this.extend = function(e) {
        var z = _.assign(_.clone(base), e)
        return z;
    }

    _this.save = function() {
        edisonAPI.combo.save($scope.pl).then(function(resp) {
            $scope.pl = _.map(resp.data, _this.extend);
            LxNotificationService.success("Les produits on été mis a jour");
        }, function(err) {
            LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
            //  edisonAPI.combo.save($scope.plSave);
        })
    }
    _this.remove = function(obj) {
        $scope.pl.splice(_.findIndex($scope.pl, '_id', obj._id), 1);
    }

    _this.getInter = function(prods)  {
        var x = _.clone(base)
        x.produits = prods.produits;
        return x;
    }

    _this.add = function() {
        $scope.pl.push({
            produits: [],
            title: '',
            open: true,
            text: ""
        })
    }



}
editCombos.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "socket"];
angular.module('edison').controller('editCombos', editCombos);

var editComptes = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, $filter, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('grand Comptes');

    edisonAPI.compte.list().then(function(resp) {
        $scope.pl = resp.data
        $scope.pl = $filter('orderBy')($scope.pl, 'nom')
    })

    _this.save = function() {
        edisonAPI.compte.save($scope.pl).then(function(resp) {
            LxNotificationService.success("Les comptes on été mis a jour");
        }, function(err) {
            LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
        })
    }
    _this.remove = function(obj) {
        $scope.pl.splice(_.findIndex($scope.pl, '_id', obj._id), 1);
    }
}
editComptes.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "$filter", "socket"];

angular.module('edison').controller('editComptes', editComptes);

var editFournisseur = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, $filter, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Fournisseurs');

    edisonAPI.fournisseur.list().then(function(resp) {
        $scope.pl = resp.data
        $scope.pl = $filter('orderBy')($scope.pl, 'nom')
    })

    _this.save = function() {
        edisonAPI.fournisseur.saveFournisseur($scope.pl).then(function(resp) {
            LxNotificationService.success("Les fournisseurs on été mis a jour");
        }, function(err) {
            LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
        })
    }
    _this.remove = function(obj) {
        $scope.pl.splice(_.findIndex($scope.pl, '_id', obj._id), 1);
    }
}
editFournisseur.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "$filter", "socket"];

angular.module('edison').controller('editFournisseur', editFournisseur);
var editPriv = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Privileges');
    
    edisonAPI.users.list().then(function(resp) {
	$scope.usrs = resp.data
    })

    _this.save = function() {
        edisonAPI.users.save($scope.usrs).then(function() {
            LxNotificationService.success("Les utilisateurs on été mis a jour");
        }, function(err) {
            LxNotificationService.error("Une erreur est survenue (" + JSON.stringify(err.data) + ')');
        })
    }
}
editPriv.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "socket"];


angular.module('edison').controller('editPriv', editPriv);

var editProducts = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, $filter, socket, dialog) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Produits');
    $scope.startLimit = 0


    var single = function(e) {
        e.single = (_(e.desc).deburr().toLowerCase() !== _(e.title).deburr().toLowerCase())
        return e;
    }

    editProducts.prototype.$scope = $scope;

    $scope.$watch('filteredData', function () {
		if ($scope.filteredData && $scope.filteredData.length == 0 && $scope.startLimit != 0)
		    $scope.startLimit -= 50;
    })

    _this.inc = function () {
		if ($scope.startLimit >= 0)
		    $scope.startLimit += 50;
    }

    _this.dec = function () {
		if ($scope.startLimit >= 0)
		    $scope.startLimit -= 50;
		if ($scope.startLimit < 0)
		    $scope.startLimit = 0;
    }

    _this.reset = function(reset) {
		$scope.startLimit = 0;
		if (reset)
		    $scope.objSearch.scategorie = undefined;
    }

    _this.categorieList = function () {
		$scope.cat = [];
		$scope.scat = {};
		for (var n = 0; n < $scope.pl.length; n++)
		{
		    if ($scope.cat.indexOf($scope.pl[n].categorie) === -1)
			$scope.cat.push($scope.pl[n].categorie);
		    if ($scope.cat.indexOf($scope.pl[n].categorie) > -1)
		    {
			if (!$scope.scat[$scope.pl[n].categorie])
			    $scope.scat[$scope.pl[n].categorie] = [];
			if ($scope.scat[$scope.pl[n].categorie].indexOf($scope.pl[n].scategorie) === -1)
			{
			    $scope.scat[$scope.pl[n].categorie].push($scope.pl[n].scategorie);
			    $scope.scat[$scope.pl[n].categorie].sort();
			}
		    }
		}
		$scope.cat.sort();
		$scope.cat.splice(0, 1);
		$scope.objSearch = {
		    categorie: $scope.cat[0],
		}
    }


    edisonAPI.product.list().then(function(resp) {
        $scope.pl = _.map(resp.data, single);
        $scope.pl = $filter('orderBy')($scope.pl, 'title');
		_this.categorieList();
    })

    _this.remove = function(obj) {
        $scope.pl.splice(_.findIndex($scope.pl, '_id', obj._id), 1);
    }

    _this.save = function() {
        edisonAPI.product.save($scope.pl).then(function(resp) {
            $scope.pl = _.map(resp.data, single);
            LxNotificationService.success("Les produits on été mis a jour");
        }, function(err) {
            LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
        })
    }

    var csvToJSON = function (file) {
		var lines = file.split("\n");
		var result = [];
		var header = lines[0].split(";");
		header.pop();

		for (var n = 1; n < lines.length; n++)
		{
		    var obj = {};
		    var current = lines[n].split(";");

		    for (var l = 0; l < header.length; l++)
			obj[header[l]] = current[l];
		    result.push(obj);
		}

		return (result);
    }

    _this.remCat = function () {
		for (var n = 0; n < $scope.pl.length; n++)
		{
		    if ($scope.pl[n].categorie === $scope.objSearch.categorie)
			$scope.pl.splice(n--, 1);
		}
		_this.categorieList();	
    }

    var toObject = function (csv) {
		var arr = [];
		var obj;
		for (var n = 0; n < csv.length; n++)
		{
		    if (csv[n].DESIGNATION)
		    {
			arr.push({
			    categorie: csv[n].CATEGORIE,
			    scategorie: csv[n].SCATEGORIE,
			    title: csv[n].DESIGNATION,
			    desc: csv[n].DESIGNATION + " " + csv[n].MARQUE + (csv[n].DESCRIPTION != '' ? "\n" + csv[n].DESCRIPTION.replace(/#/g, '\n') : ''),
			    ref: csv[n].REF,
			    pa: csv[n].PA.replace(',', '.'),
			    pu: csv[n].PU.replace(',', '.'),
			    marge: parseInt(csv[n].PU.replace(',', '.')) - parseInt(csv[n].PA.replace(',', '.')),
			    link: csv[n].LINK,
			    single: true,
			});
		    }
		}
		return arr;
    }

    _this.editImage = function (elem) {
		dialog.changeImage(elem.link, function (resp) {
		    elem.link = resp;
		})
    }

    _this.importProducts = function() {
		var read = new FileReader();
		read.onload = function(e) {
		    var elems = toObject(csvToJSON(e.target.result));
		    $scope.pl = $scope.pl.concat(elems);
		    _this.categorieList();
		}
		read.readAsText($scope.file, 'ISO-8859-1');
    }

    /* $scope.$watch('pl', function(curr, prev) {
         if (curr && prev && !_.isEqual(prev, curr)) {
             save()
         }
     }, true)*/
     _this.newProd = function()
    {
    	$scope.pl.unshift({ single: false, open: true, categorie: $scope.objSearch.categorie});
    }
}
editProducts.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "$filter", "socket", "dialog"];

editProducts.prototype.changeFile = function (elem) {
    var $scope = this.$scope;
    $scope.$apply(function () {
	$scope.file = elem.files[0];
    });
};


angular.module('edison').controller('editProducts', editProducts);

var editRaison = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, $filter, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Raisons');

    edisonAPI.raison.list().then(function(resp) {
        $scope.pl = resp.data
        $scope.pl = $filter('orderBy')($scope.pl, 'raison')
    })

    _this.save = function() {
        edisonAPI.raison.saveRaison($scope.pl).then(function(resp) {
            LxNotificationService.success("Les Raisons ont été mise à jour");
        }, function(err) {
            LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
        })
    }
    _this.remove = function(obj) {
        $scope.pl.splice(_.findIndex($scope.pl, '_id', obj._id), 1);
    }
}
editRaison.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "$filter", "socket"];

angular.module('edison').controller('editRaison', editRaison);
var editSignalements = function(TabContainer, edisonAPI, $rootScope, dialog, $scope, $filter, $location, LxNotificationService, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Signalements');

    edisonAPI.signal.list().then(function(resp) {
		$scope.pl = $filter('orderBy')(resp.data, 'subType');
    })

	_this.dest = function(indPl, signal) {
		dialog.pop_up_dest(signal, indPl, function(resp, cancel) {
            if (cancel === true)
                $scope.pl[indPl] = resp.data;
		})

	}

    _this.remove = function(_id) {
        var i = _.findIndex($scope.pl, '_id', _id)
        $scope.pl.splice(i, 1);
    }

    _this.save = function() {
        edisonAPI.signal.save($scope.pl).then(function(resp) {
            $scope.pl = resp.data;
            LxNotificationService.success("Les produits on été mis a jour");
        }, function(err) {
            LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
        })
    }


}
editSignalements.$inject = ["TabContainer", "edisonAPI", "$rootScope", "dialog", "$scope", "$filter", "$location", "LxNotificationService", "socket"];
angular.module('edison').controller('editSignalements', editSignalements);

var editUsers = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, $filter, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Utilisateurs');

    editUsers.prototype.$scope = $scope;

    edisonAPI.users.list().then(function(resp) {
		$scope.usrs = $filter('orderBy')(resp.data, 'login');
    })

    _this.save = function() {
    	$scope.usrs = $filter('orderBy')($scope.usrs, 'login');
    	edisonAPI.users.save($scope.usrs).then(function(usersSaved) {
    		LxNotificationService.success("Les utilisateurs ont été mis à jour");
    	}, function(err) {
    		LxNotificationService.error("Une erreur est survenue (" + JSON.stringify(err.data) + ')');
    	})
    }

    _this.remove = function(obj) {
        $scope.usrs.splice(_.findIndex($scope.usrs, '_id', obj._id), 1);
    }

    $scope.changeImage = function (obj, file) {
    	var reader = new FileReader();
    	/*reader.onload = function(e) {
    	    var bytes = new Uint8Array(e.target.result);
    	    var binary = ""
    	    for (var n = 0; n < bytes.byteLength; n++)
    		binary += String.fromCharCode(bytes[n]);
    	    edisonAPI.users.upload({login: obj, file: binary});
    	}
    	reader.readAsArrayBuffer($scope.file)*/
    	reader.onloadend = function(e) {
    	    var r = this.result;
    	    var hex = [];
    	    for (var n = 0; n < this.result.length; n++)
    	    {
        		var byteStr = r.charCodeAt(n);
        		hex.push(byteStr);
    	    }
    	    edisonAPI.users.upload({login: obj, file: hex});
    	}
    	reader.readAsBinaryString($scope.file);
        }
}
editUsers.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "$filter", "socket"];

editUsers.prototype.changeFile = function (elem) {
    var $scope = this.$scope;
    $scope.$apply(function () {
	$scope.file = elem.files[0];
	$scope.changeImage(elem.name);
    });
};

angular.module('edison').controller('editUsers', editUsers);

var fournitureMonthly = function(config, edisonAPI, MomentIterator, $scope) {
    "use strict";
    var _this = this;
    _this.config = config;

    var start = new Date("01/01/2013");
    var end = new Date();
    _this.yearSelect = MomentIterator(start, end).range('year', {
	format: 'YYYY',
    }).map(function(e) {
	return parseInt(e)
    })

    $scope.selectedYear = _this.yearSelect[_this.yearSelect.length - 1];
    
    _this.loadData = function () {
	var obj = {};
	obj.year = $scope.selectedYear;
	edisonAPI.compta.getFourniture(obj).then(function (resp) {
	    _this.years = resp.data;
	})
    }
    _this.loadData();
    
    _this.xsort = function(sb) {
	_this.sb = sb
    }

    $scope.$watch("selectedYear", function() {
	_this.loadData();
    });

}
fournitureMonthly.$inject = ["config", "edisonAPI", "MomentIterator", "$scope"];

angular.module('edison').controller('fournitureMonthly', fournitureMonthly);

var ovhCallsController = function(config, edisonAPI, MomentIterator, $scope, dialog, NgTableParams, LxProgressService, $window) {
    var _this = this;
    $scope.startLimit = 0

    _this.numbersData = {}

    var arr_sort = function (o1, o2) {
	return new Date(o1.date) - new Date(o2.date)
    }

    var loadData = function (array, n)
    {
	if (n < array.length && !_this.numbersData[$scope.filteredData[n]])
	{
	    edisonAPI.ovh.getSingle({tel: $scope.filteredData[n]}).then(function(resp) {
		resp.data.convOvh.sort(arr_sort);
		_this.numbersData[$scope.filteredData[n]] = resp.data
		loadData(array, n + 1);
	    })
	}
    }

    $scope.$watch('filteredData', function () {
	if ($scope.filteredData && $scope.filteredData.length == 0 && $scope.startLimit != 0)
	    $scope.startLimit -= 10;
	if ($scope.filteredData)
	    loadData($scope.filteredData, 0);
    })

    _this.inc = function () {
	$scope.startLimit += 10;
    }

    _this.dec = function () {
	if ($scope.startLimit != 0)
	    $scope.startLimit -= 10;
    }

    edisonAPI.ovh.getAll().then(function (resp) {
	_this.numberList = []
	Object.keys(resp.data).forEach(function(key, index) {
	    _this.numberList.push(resp.data[key].number)
	})
    })
}
ovhCallsController.$inject = ["config", "edisonAPI", "MomentIterator", "$scope", "dialog", "NgTableParams", "LxProgressService", "$window"];

angular.module('edison').controller('ovhCallsController', ovhCallsController);

var ovhController = function(config, edisonAPI, MomentIterator, $scope, dialog, NgTableParams, LxProgressService, $window) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.log = {}
    _this.late = {}
    _this.total = {}
    _this.excused = {}

    var sortNb = function (a, b) {
	return (a - b);
    }

    var filter = function (array, taken) {
	var ret = {
	    anM: 0,
	    anR: 0,
	    rec: 0,
	    recU: 0,
	    man: 0,
	    manU: 0,
	    manH: 0,
	    manUH: 0
	}
	var uniqueRec = [];
	var uniqueMan = [];
	for (var n = 0; n < array.length; n++)
	{
	    if (array[n].duration > 0)
	    {
		ret.rec += 1;
		if (array[n].an === true)
		    ret.anR += 1;
		if (uniqueRec.indexOf(array[n].number) === -1)
		{
		    ret.recU += 1
		    uniqueRec.push(array[n].number)
		}
	    }
	}
	for (var n = 0; n < array.length; n++)
	{
	    var date = new Date(array[n].date);
	    if (array[n].duration === 0 && taken.indexOf(array[n].number) === -1)
	    {
		if (date.getHours() >= 7 && date.getHours() <= 18)
		    ret.man += 1;
		else
		    ret.manH += 1;
		if (array[n].an === true)
		    ret.anM += 1;
		if (uniqueMan.indexOf(array[n].number) === -1 && uniqueRec.indexOf(array[n].number) === -1)
		{
		    if (date.getHours() >= 7 && date.getHours() <= 18)
			ret.manU += 1
		    else
			ret.manUH += 1
		    uniqueMan.push(array[n].number)
		}
	    }
	}
	return (ret);
    }

    var filterMissed = function(object) {
	var taken = []
	Object.keys(object).forEach(function(key, index) {
	    for (var n = 0; n < object[key].length; n++)
	    {
		if (object[key][n].duration > 0)
		    taken.push(object[key][n].number)
	    }
	})
	return (taken);
    }

    _this.updateStats = function () {
	var table = []
	var taken = filterMissed(_this.lines.logs);
	Object.keys(_this.lines.logs).forEach(function(key, index) {
	    var obj = filter(_this.lines.logs[key], taken)
	    if (_this.interLink[key])
	    {
		_this.interLink[key].total.sort(sortNb);
		_this.interLink[key].val.sort(sortNb);
		_this.interLink[key].ann.sort(sortNb);
		_this.interLink[key].prog.sort(sortNb);
	    }
	    if (_this.devisLink[key])
	    {
		_this.devisLink[key].total.sort(sortNb);
		_this.devisLink[key].env.sort(sortNb);
		_this.devisLink[key].tran.sort(sortNb);
	    }
	    table.push({
		name: key,
		rec: obj.rec,
		recU: obj.recU,
		man: obj.man,
		manU: obj.manU,
		anR: obj.anR,
		anM: obj.anM,
		manH: obj.manH,
		manUH: obj.manUH,
		at: _this.linesFinal[key] ? _this.linesFinal[key][0] : 0,
		atN: _this.linesFinal[key] ? _this.linesFinal[key][1].toFixed(2) : 0.00,
		atL: _this.interLink[key] ? _this.interLink[key].total : [],
		osv: _this.linesFinal[key] ? _this.linesFinal[key][2] : 0,
		osvN: _this.linesFinal[key] ? _this.linesFinal[key][3].toFixed(2) : 0.00,
		osvL: _this.interLink[key] ? _this.interLink[key].val : [],
		osa: _this.linesFinal[key] ? _this.linesFinal[key][4] : 0,
		osaN: _this.linesFinal[key] ? _this.linesFinal[key][5].toFixed(2) : 0.00,
		osaL: _this.interLink[key] ? _this.interLink[key].ann : [],
		osp: _this.linesFinal[key] ? _this.linesFinal[key][6] : 0,
		ospN: _this.linesFinal[key] ? _this.linesFinal[key][7].toFixed(2) : 0.00,
		ospL: _this.interLink[key] ? _this.interLink[key].prog : [],
		dev: _this.linesFinal[key] ? _this.linesFinal[key][8] : 0,
		devN: _this.linesFinal[key] ? _this.linesFinal[key][9].toFixed(2) : 0.00,
		devL: _this.interLink[key] ? _this.devisLink[key].total : [],
		devE: _this.linesFinal[key] ? _this.linesFinal[key][10] : 0,
		devEN: _this.linesFinal[key] ? _this.linesFinal[key][11].toFixed(2) : 0.00,
		devEL: _this.interLink[key] ? _this.devisLink[key].env : [],
		devT: _this.linesFinal[key] ? _this.linesFinal[key][12] : 0,
		devTN: _this.linesFinal[key] ? _this.linesFinal[key][13].toFixed(2) : 0.00,
		devTL: _this.interLink[key] ? _this.devisLink[key].tran : [],
		total: _this.linesFinal[key] ? (_this.linesFinal[key][1] + _this.linesFinal[key][9]).toFixed(2) : 0.00
	    })
	})
	_this.prStats = new NgTableParams({
	    count: table.length,
	    sorting: {
		rec: 'desc'
	    }
	}, {
	    counts: [],
	    data: table
	});
	/*var table2 = []
	Object.keys(_this.lines2.logs).forEach(function(key, index) {
	    var obj = filter(_this.lines2.logs[key], taken)	
	    if (_this.interLink[key])
	    {
		_this.interLink[key].total.sort(sortNb);
		_this.interLink[key].val.sort(sortNb);
		_this.interLink[key].ann.sort(sortNb);
		_this.interLink[key].prog.sort(sortNb);
	    }
	    if (_this.devisLink[key])
	    {
		_this.devisLink[key].total.sort(sortNb);
		_this.devisLink[key].env.sort(sortNb);
		_this.devisLink[key].tran.sort(sortNb);
	    }    
	    table2.push({
		name: key,
		number: _this.numbers.number[key] ? _this.numbers.number[key].replace('0033', '0') : '',
		rec: obj.rec,
		recU: obj.recU,
		man: obj.man,
		manU: obj.manU,
		anR: obj.anR,
		anM: obj.anM,
		manH: obj.manH,
		manUH: obj.manUH,
		at: _this.linesFinal[key] ? _this.linesFinal[key][0] : 0,
		atN: _this.linesFinal[key] ? _this.linesFinal[key][1].toFixed(2) : 0,
		atL: _this.interLink[key] ? _this.interLink[key].total : [],
		osv: _this.linesFinal[key] ? _this.linesFinal[key][2] : 0,
		osvN: _this.linesFinal[key] ? _this.linesFinal[key][3].toFixed(2) : 0.00,
		osvL: _this.interLink[key] ? _this.interLink[key].val : [],
		osa: _this.linesFinal[key] ? _this.linesFinal[key][4] : 0,
		osaN: _this.linesFinal[key] ? _this.linesFinal[key][5].toFixed(2) : 0.00,
		osaL: _this.interLink[key] ? _this.interLink[key].ann : [],
		osp: _this.linesFinal[key] ? _this.linesFinal[key][6] : 0,
		ospN: _this.linesFinal[key] ? _this.linesFinal[key][7].toFixed(2) : 0.00,
		ospL: _this.interLink[key] ? _this.interLink[key].prog : [],
		dev: _this.linesFinal[key] ? _this.linesFinal[key][8] : 0,
		devN: _this.linesFinal[key] ? _this.linesFinal[key][9].toFixed(2) : 0.00,
		devL: _this.interLink[key] ? _this.devisLink[key].total : [],
		devE: _this.linesFinal[key] ? _this.linesFinal[key][10] : 0,
		devEN: _this.linesFinal[key] ? _this.linesFinal[key][11].toFixed(2) : 0.00,
		devEL: _this.interLink[key] ? _this.devisLink[key].env : [],
		devT: _this.linesFinal[key] ? _this.linesFinal[key][12] : 0,
		devTN: _this.linesFinal[key] ? _this.linesFinal[key][13].toFixed(2) : 0.00,
		devTL: _this.interLink[key] ? _this.devisLink[key].tran : [],
		total: _this.linesFinal[key] ? (_this.linesFinal[key][1] + _this.linesFinal[key][9]).toFixed(2) : 0.00
	    })
	})
	_this.prStats2 = new NgTableParams({
	    count: table2.length,
	    sorting: {
		rec: 'desc'
	    }
	}, {
	    counts: [],
	    data: table2
	});*/
    }


    var start = new Date("08/01/2016");
    start.setHours(0,0,0,0);
    var end = new Date();
    end.setHours(1,0,0,0);
    _this.dateSelect = MomentIterator(start, end).range('weeks', {
	format: 'DD-MM-YYYY',
    }).map(function(e) {
	return e
    })

    _this.dateSelect2 = MomentIterator(start, end).range('months', {
	format: 'DD-MM-YYYY',
    }).map(function(e) {
	return e
    })    

    $scope.selectedWeek = _this.dateSelect[_this.dateSelect.length - 1];
    $scope.selectedMonth = _this.dateSelect2[_this.dateSelect2.length - 1];

    _this.changeLink = function (mode, nb) {
	$window.open('/' + mode + '/' + nb);
    }

    _this.ovhChargeMultiple = function(type, selected) {
	_this.loadedAcc = _this.account;
	_this.loadedNum = _this.number;
	LxProgressService.circular.show('#5fa2db', '#globalProgress');
	var q = {
	    t: type,
	    d: selected,
	    a: _this.account,
	    n: _this.logins[_this.number.replace("0033", "0")] ? _this.logins[_this.number.replace("0033", "0")] : '',
	}
	edisonAPI.ovh.getComptes(q).then(function (resp) {
	    _this.lines = resp.data;
	    q.n = _this.numberInfo[_this.number]
	    edisonAPI.ovh.demStats(q).then(function (resp2) {
		_this.numbers = resp2.data;
		Object.keys(_this.numbers.logs).forEach(function (key, index) {
		    Object.keys(_this.numbers.logs[key]).forEach(function (key2, index2) {
			_this.lines.logs[key2] = []
			for (var n = 0; n < _this.numbers.logs[key][key2].length; n++)
			    _this.lines.logs[key2].push(_this.numbers.logs[key][key2][n])
		    })
		})
		q.l = _this.logins[_this.number.replace("0033", "0")] ? _this.logins[_this.number.replace("0033", "0")] : '';
		edisonAPI.ovh.getInterStatus(q).then(function (resp3) {
		    _this.linesFinal = resp3.data.count;
		    _this.interLink = resp3.data.inter;
		    _this.devisLink = resp3.data.devis;
		    _this.updateStats();
		    LxProgressService.circular.hide();
		})
	    })
	})

    }

    _this.ovhCharge = function () {
	_this.loadedAcc = _this.account;
	_this.loadedNum = _this.number;
	LxProgressService.circular.show('#5fa2db', '#globalProgress');
	var check = new Date(_this.date2);
	var check2 = new Date();
	check2.setHours(0, 0, 0, 0);
	if (check.getTime() > check2.getTime())
	{
	    return ;
	}
	var q = {
	    t: 'single',
	    d: _this.date2,
	    a: _this.account,
	    n: _this.logins[_this.number.replace("0033", "0")] ? _this.logins[_this.number.replace("0033", "0")] : '',
	}
	edisonAPI.ovh.getComptes(q).then(function (resp) {
	    _this.lines = resp.data;
	    q.n = _this.numberInfo[_this.number]
	    edisonAPI.ovh.demStats(q).then(function (resp2) {
		_this.numbers = resp2.data;
		Object.keys(_this.numbers.logs).forEach(function (key, index) {
		    Object.keys(_this.numbers.logs[key]).forEach(function (key2, index2) {
			_this.lines.logs[key2] = []
			for (var n = 0; n < _this.numbers.logs[key][key2].length; n++)
			    _this.lines.logs[key2].push(_this.numbers.logs[key][key2][n]);
		    })
		})
		q.l = _this.logins[_this.number.replace("0033", "0")] ? _this.logins[_this.number.replace("0033", "0")] : '';
		edisonAPI.ovh.getInterStatus(q).then(function (resp3) {
		    _this.linesFinal = resp3.data.count;
		    _this.interLink = resp3.data.inter;
		    _this.devisLink = resp3.data.devis;
		    _this.updateStats();
		    LxProgressService.circular.hide();
		})
	    })
	})
    }

    _this.keys = function(obj) {
	if (!obj)
	    return []
	var arr = Object.keys(obj);
	for (var n = 0; n < arr.length; n++)
	{
	    for (var l = n + 1; l < arr.length; l++)
	    {
		if (obj[arr[n]].length < obj[arr[l]].length)
		{
		    var tmp = arr[n];
		    arr[n] = arr[l]
		    arr[l] = tmp;
		}
	    }
	}
	return arr;
    }

    _this.account = "";
    _this.number = "";
    _this.loadedAcc = "";
    _this.loadedNum = "";

    var naturalCompare = function (a, b) {
	var ax = [], bx = [];

	a.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
	b.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });

	while(ax.length && bx.length) {
	    var an = ax.shift();
	    var bn = bx.shift();
	    var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
	    if(nn) return nn;
	}

	return ax.length - bx.length;
    }

    _this.resetNumber = function () {
	_this.number = "";
    }

    _this.search = function (str) {
	var results = str ? _this.fullInfo.filter(function (elem) {
	    return (elem.toLowerCase().indexOf(str.toLowerCase()) >= 0);
	}) : _this.fullInfo;
	return results;
    }

    _this.onSelect = function(num) {
	var index = -1
	if (num)
	{
	    for (var n = 0; n < _this.numberFullList.length && index == -1; n++)
	    {
		if (num.indexOf(_this.numberFullList[n]) >= 0)
		    index = n;
	    }
	    if (index != -1)
	    {
		_this.number = _this.numberFullList[index];
		_this.account = _.findKey(_this.numberList, function(o) {
		    return o.indexOf(_this.numberFullList[index]) >= 0
		})
	    }
	}
    }

    _this.counter = 0;
    _this.loading = true;

    LxProgressService.circular.show('#5fa2db', '#timedProgress');
    edisonAPI.ovh.getAccounts().then(function (resp) {
	edisonAPI.ovh.getPhone().then(function (resp2) {
	    _this.logins = resp2.data;
	    _this.accountList = resp.data.accounts;
	    _this.numberList = resp.data.numbers;
	    _this.numberInfo = {};
	    _this.numberFullList = [].concat.apply([], Object.keys(resp.data.numbers).map(function (key) {
		return resp.data.numbers[key]
	    }))
	    _this.fullInfo = _this.numberFullList.slice();

	    var keys = Object.keys(_this.numberList);
	    var total = keys.length;
	    var val = keys.splice(0, 1)[0]
	    
	    var load = function (arr) {
		var obj = {
		    key: arr,
		    val: _this.numberList[arr],
		}
		edisonAPI.ovh.getInfo(obj).then(function (resp) {
		    _this.counter = Math.round(((total - keys.length) / total) * 100)
		    _this.numberInfo = Object.assign(_this.numberInfo, resp.data);
		    val = keys.splice(0, 1)[0];
		    if (val)
			load(val)
		    else
		    {
			for (var n = 0; n < _this.fullInfo.length; n++)
			{
			    _this.fullInfo[n] = _this.numberInfo[_this.fullInfo[n]] + " " + _this.fullInfo[n]
			}
			_this.loading = false;
			LxProgressService.circular.hide();
		    }
		})
	    }

	    var load2 = function () {
		edisonAPI.ovh.getBill({ val: _this.accountList}).then(function (resp) {
		    _this.accountName = resp.data;
		})
	    }

	    load2();
	    load(val);
	    _this.accountList.sort(naturalCompare);
	})
    })



/*    $scope.$watch("selectedWeek", function() {
	_this.reload();
    });*/

}
ovhController.$inject = ["config", "edisonAPI", "MomentIterator", "$scope", "dialog", "NgTableParams", "LxProgressService", "$window"];

angular.module('edison').controller('ovhController', ovhController);

var telephoneMatch = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxProgressService, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('TelMatch');
    $scope.__txt_tel = $rootScope.__txt_tel
    $rootScope.getTelMatch = function() {
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        $rootScope.__txt_tel = $scope.__txt_tel
        edisonAPI.intervention.getTelMatch({
            q: $rootScope.__txt_tel
        }).then(_.noop, function() {
        LxProgressService.circular.hide()

        })
    }

    socket.on('intervention_db_telMatches', function(data) {
        $rootScope.globalProgressCounter = data + '%';
    })

    socket.on('telephoneMatch', function(data) {
        $rootScope.globalProgressCounter = ""
        LxProgressService.circular.hide()
        $scope.resp = data
    })

}
telephoneMatch.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxProgressService", "socket"];
angular.module('edison').controller('telephoneMatch', telephoneMatch);

var vcfController = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, socket, $window, Artisan, Intervention) {
	"use strict";
	var _this = this;
}
vcfController.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "socket", "$window", "Artisan", "Intervention"];
angular.module('edison').controller('vcfController', vcfController);

var acquittanceController = function(config, edisonAPI, MomentIterator, $scope, dialog) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.log = {}
    _this.late = {}
    _this.total = {}
    _this.excused = {}

    var start = new Date("08/01/2016");
    start.setHours(0,0,0,0);
    var end = new Date();
    end.setHours(1,0,0,0);
    _this.dateSelect = MomentIterator(start, end).range('week', {
	format: 'DD-MM-YYYY',
    }).map(function(e) {
	return e
    })

    $scope.selectedWeek = _this.dateSelect[_this.dateSelect.length - 1];
    
    _this.loadData = function () {
	var obj = {};
	obj.year = $scope.selectedYear;
	edisonAPI.compta.getFourniture(obj).then(function (resp) {
	    _this.years = resp.data;
	})
    }
    _this.loadData();

    _this.weekDays = function () {
	return (['Logins', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche', 'Retards Accumulés', 'Temps de travail'])
    }
    
    _this.cancel = function (login, day, elem, excused) {
	if (elem != 0 && excused === false)
	{
	    dialog.cancelLate({login: login, day: _this.weekDays()[day + 1], nb: day, week: $scope.selectedWeek}, function (data) {
		edisonAPI.users.cancel(data);
	    })
	}
    }

    _this.weekLog = function (login, hourse, hoursf) {
	edisonAPI.log.getLate({login: login, weekStart: $scope.selectedWeek}).then(function (resp) {
	    _this.late[login] = resp.data.late;
	    _this.excused[login] = resp.data.excused;
	    _this.total[login] = 0;
	    for (var n = 0; n < resp.data.late.length; n++)
		_this.total[login] += resp.data.late[n] > 0 ? resp.data.late[n] : 0;
	    edisonAPI.log.getWeekLog({login: login, weekStart: $scope.selectedWeek}).then(function (resp) {
		_this.log[login] = resp.data;
		var time = 0
		for (var j = 0;hourse.length > j;j++)
		{
		    var d1 = new Date(hourse[j])
		    var d1h = d1.getHours()
		    var d1m = d1.getMinutes()
		    var d2 =  new Date(hoursf[j])
		    var d2h = d2.getHours()
		    var d2m = d2.getMinutes()
		    var tot1 = (d2h - d1h) * 3600000
		    var tot2 = (d2m - d1m) *60000
		    var tot = tot1 + tot2
		    if (_this.log[login][j] !== "Abs")
		    {
			if (d1.getHours() != 0)
			    time = tot + time;
		    }
		}
		time = time - (_this.total[login] * 60000)
		var x = time / 1000
		var seconds = x % 60
		x /= 60
		var minutes = x % 60
		x /= 60
		var hours = Math.floor(x);
		time = hours + "h" + minutes + "min"
		_this.time[login] = time
	    });
	});
	
	
	
    }

    _this.reload = function () {
	if (_this.userList)
	{
	    for (var n = 0; n < _this.userList.length; n++)
		_this.weekLog(_this.userList[n].login);
	}
    }
    
    _this.getUserList = function () {
	edisonAPI.users.list().then(function (resp) {
	    _this.userList = []
	    _this.time = {}
	    for (var n = 0; n < resp.data.length; n++)
	    {
		if (resp.data[n].activated === true)
		{
		   _this.time[resp.data[n].login] = ""
		    _this.weekLog(resp.data[n].login, resp.data[n].hours, resp.data[n].hoursf);
		    _this.userList.push({login: resp.data[n].login, service: resp.data[n].service});
		    
		}
	    }
	})
    }
    _this.getUserList();

    $scope.$watch("selectedWeek", function() {
	_this.reload();
    });

}
acquittanceController.$inject = ["config", "edisonAPI", "MomentIterator", "$scope", "dialog"];

angular.module('edison').controller('acquittanceController', acquittanceController);

angular.module('edison').controller('DemarchageController', _.noop);
var general = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, socket, $window, mapAutocomplete, Artisan, Intervention, dialog, user) {
    "use strict";
    var _this = this;

    _this.saveGrandsComptes = function() {
		edisonAPI.intervention.saveGrandsComptes();
    }
    
    _this.Geolocalisation = function () {
		edisonAPI.prospect.geoloc();
    }
    _this.CP = function () {
		edisonAPI.prospect.CP();
    }
    _this.Merge = function () {
		edisonAPI.prospect.Merge();
    }

	_this.relanceAuto7h = function() {
		edisonAPI.intervention.relanceAuto();
	}

	_this.changePriseEnCharge = function() {
		edisonAPI.intervention.priseEnChargeTimer().then(function(resp) {
			console.log("------>", resp);
		})
	}

	_this.listInter = function() {
		if ($scope.fileId)
		{
			/* Read local file */
			var name = $scope.fileId.name;
			var read = new FileReader($scope.fileId);
			read.onload = function(e) {
				edisonAPI.intervention.listInter({fileId: JSON.parse(e.target.result)}).then(function(resp) {
				});
			}
			read.readAsText($scope.fileId, 'ISO-8859-1');
		}
	}

	/* Fonction de transformation de status à partir d'un fichier JSON (Outils Admin) */
	_this.statusTrans = function() {
		if ($scope.fileId)
		{
			/* Read local file */
			var name = $scope.fileId.name;
			var read = new FileReader($scope.fileId);
			read.onload = function(e) {
				/* Appel de la fonction de transformation du status /server/models/intervention/methods/intervention.transStatus.js */
				edisonAPI.intervention.transSav({fileId: JSON.parse(e.target.result), nStatus: $scope.typeSt}).then(function() {
					LxNotificationService.success("Les transformations sont finies.");
				});
			}
			read.readAsText($scope.fileId, 'ISO-8859-1');
		}
	}

	_this.switchTest = function() {
		edisonAPI.intervention.switchTest().then(function(resp) {
		});
	}

	_this.mailAverif = function() {
		edisonAPI.intervention.mailAverif().then(function(resp) {
		});
	}

	_this.getDatasOnFactureAndClientNames = function() {
		edisonAPI.intervention.getDatasOnFactureAndClientNames().then(function(resp) {
		})
	}

	_this.transfo = function(typeSig) {
		edisonAPI.signalement.transfo({typeSig: typeSig}).then(function(resp) {
		});
	}

	_this.ReloadCache = function() {
		edisonAPI.intervention.reloadCache();
	}

	_this.flushTrue = function() {
		edisonAPI.intervention.flushTrue().then(function(resp) {
		});
	}

    _this.relAll = function () {
		edisonAPI.intervention.relanceAll();
    }

    general.prototype.$scope = $scope;

    _this.importRIB = function () {
		var read = new FileReader();
		read.onload = function(e) {
	   		var elems = toObject(csvToJSON(e.target.result));
	    	edisonAPI.artisan.addIBAN(elems).then(function(resp){
	    	})
		}
		read.readAsText($scope.file, 'ISO-8859-1');
    }

	/* Prototype de sauvegarde du fichier via onchange angular */
   	general.prototype.changeFile = function(elem) {
		var $scope = this.$scope;
		$scope.$apply(function () {
	    	$scope.fileId = elem.files[0];
		});
    };
}
general.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "socket", "$window", "mapAutocomplete", "Artisan", "Intervention", "dialog", "user"];

/*
**
** This Controller serves for a large variety of
** functions linked to database manipulation
**
*/


angular.module('edison').controller('GeneralController', general);

var ImportController = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, socket, $window, mapAutocomplete, Artisan, dialog, user) {
    "use strict";
    var _this = this;

    _this.saveAllArtisan = function() {
	edisonAPI.artisan.saveAll();
    }

    var geocoder = new google.maps.Geocoder();

    ImportController.prototype.$scope = $scope;

    var csvToJSON = function (file) {
	var lines = file.split("\n");
	var result = [];
	var header = lines[0].split(";");
	header.pop();

	for (var n = 1; n < lines.length; n++)
	{
	    var obj = {};
	    var current = lines[n].split(";");

	    for (var l = 0; l < header.length; l++)
		obj[header[l]] = current[l];

	    result.push(obj);
	}

	return (result);
    }

    var convertToArtisan = function (elem) {
	var tmp = {};

	tmp.email = elem.email;
	tmp.representant = {
	    nom: elem.nom,
	    prenom: elem.prenom,
	    civilite: "M."
	};
	tmp.categories = elem.categories.split(',');
	tmp.nomSociete = elem.societe;
	tmp.telephone = {
	    tel1: (elem.tel1 ? elem.tel1.replace(/\s/g,'') : ''),
	    tel2: (elem.tel2 ? elem.tel2.replace(/\s/g,'') : ''),
	};
	tmp.infoSup = {
	    siret: elem.siret,
	    Site1: elem.site1,
	    Site2: elem.site2,
	    Site3: elem.site3
	};

	tmp.zoneChalandise = "30";
	tmp.pourcentage = {
	    fourniture: "30",
	    maindOeuvre: "30",
	    deplacement: "50",
	};

	tmp.address = {
	    cp: elem.cp,
	    n: 0,
	    r: elem.rue,
	    v: elem.ville,
	};

	return (tmp);
    }

   var parseData = function(elems) {
       var finals = [];

       for (var n = 0; n < elems.length; n++)
       {
	   var tmpList = []
	   Object.keys(elems[n]).forEach(function(key, index) {
	       tmpList.push(key);
	       tmpList.push(elems[n][key]);
	   })
	   for (var l = n+1; l < elems.length; l++)
	   {
	       if ((elems[n].tel1 && elems[l].tel1 && elems[n].tel1 === elems[l].tel1) ||
		   (elems[n].tel1 && elems[l].tel2 && elems[n].tel1 === elems[l].tel2) ||
		   (elems[n].tel2 && elems[l].tel1 && elems[n].tel2 === elems[l].tel1) ||
		   (elems[n].tel2 && elems[l].tel2 && elems[n].tel2 === elems[l].tel2))
	       {
		   var copyList = []

		   Object.keys(elems[l]).forEach(function(key, index) {
		       copyList.push(key);
		       copyList.push(elems[l][key]);
		   })
		   for (var m = 0; m < copyList.length; m += 2)
		   {
		       var i = tmpList.indexOf(copyList[m]);
		       if (tmpList[i + 1].length === 0 && copyList[m + 1].length !== 0)
		       {
			   tmpList[i + 1] = copyList[m + 1];
			   elems[n][copyList[m]] = copyList[m + 1];
		       }
		   }
		   elems.splice(l--, 1)
	       }
	   }
	   finals.push(elems[n]);
       }
       return (finals);
   }

    _this.history = function () {
	edisonAPI.history.list().then(function (resp) {
	    _this.hist = resp.data;
	})
    }

    _this.history();

    _this.geoloc = function() {
	if ($scope.fileGeoloc)
	{
	    var name = $scope.fileGeoloc.name;
	    var read = new FileReader();
	    read.onload = function(e) {
		var elems = csvToJSON(e.target.result);
		elems = parseData(elems.slice(0));
		edisonAPI.history.add({login: user.login, name: name});
//		edisonAPI.prospect.checkDuplicates(elems).then(function (resp) {
		    var finals = [];
		    for (var n = 0; n < elems.length; n++)
			finals.push(convertToArtisan(elems[n]));
		    edisonAPI.prospect.geolocNotBDD(finals);
//		})
	    }
	    read.readAsText($scope.fileGeoloc, 'ISO-8859-1');
	}
    }
}
ImportController.$inject = ["TabContainer", "edisonAPI", "$rootScope", "$scope", "$location", "LxNotificationService", "socket", "$window", "mapAutocomplete", "Artisan", "dialog", "user"];


ImportController.prototype.changeFileGeoloc = function (elem) {
    var $scope = this.$scope;
    $scope.$apply(function () {
	$scope.fileGeoloc = elem.files[0];
    });
};

angular.module('edison').controller('ImportController', ImportController);

var MapDemarchageController = function($rootScope, edisonAPI, $scope, config) {
    var _this = this;
    $scope._ = _;
    $scope.root = $rootScope;

    _this.cat = config.categoriesHash();
    
    _this.changeCat = function () {
	_this.confirmedArtisans = []
	for (var n = 0; n < _this.complete.length; n++)
	{
	    if (_this.complete[n].cate === parseInt($scope.categorie))
		_this.confirmedArtisans.push(_this.complete[n]);
	}
    }

    edisonAPI.demarchage.list().then(function (resp) {
	_this.complete = []
	for (var n = 0; n < resp.data.length; n++)
	{
	    if (resp.data[n].foundOne === false)
		_this.complete.push(resp.data[n]);
	}
    });
}
MapDemarchageController.$inject = ["$rootScope", "edisonAPI", "$scope", "config"];

angular.module('edison').controller('MapDemarchageController', MapDemarchageController);
