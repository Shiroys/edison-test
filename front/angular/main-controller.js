angular.module('edison').controller('MainController', function($timeout, LxNotificationService, dialog, $q, DataProvider, TabContainer, $scope, socket, config, $rootScope, $location, edisonAPI, taskList, $window) {
    "use strict";
    $rootScope.app_users = app_users;
    $rootScope.displayUser = app_session
    $scope.sidebarHeight = $("#main-menu-bg").height();
    $scope.config = config;
    $scope._ = _;
    $rootScope.loadingData = true;
    $rootScope.$on('$routeChangeSuccess', function() {
        $window.scrollTo(0, 0);
        $rootScope.loadingData = false;
    });
    var _this = this;

	edisonAPI.signalement.list({selectedUser: app_session.login, type: 1}).then(function(resp) {
		$rootScope.signCount = resp.data.length;
	})

    $rootScope.toggleSidebar = function(open) {
        if ($rootScope.sideBarMode === true) {
            $rootScope.sideBarIsClosed = open;
        }
    }

    $rootScope.addIntervention = function() {
        if ($scope.user.service !== 'INTERVENTION' || (!$scope.userStats.i_avr.total || !$scope.userStats.i_avr.total || ($scope.user.maxInterAverif > $scope.userStats.i_avr.total))) {
            $location.url('/intervention')
        } else {
            LxNotificationService.error("Impossible: Vous avez dépasser votre quota d'intervention à vérifié (" + $scope.userStats.i_avr.total + ' > ' + $scope.user.maxInterAverif + ')')
        }
    }

    $rootScope.toggleSidebarMode = function(newVal) {
        $rootScope.sideBarMode = _.isUndefined(newVal) ? !$rootScope.sideBarMode : newVal;
        $rootScope.sideBarIsClosed = $rootScope.sideBarMode
    }

    var checkResize = function() {
        $rootScope.smallWin = window.innerWidth < 1350
    	$rootScope.phoneWin = window.innerWidth < 480
    	_this.phoneWin = window.innerWidth < 480
            return $rootScope.toggleSidebarMode($rootScope.smallWin);
        }
        $(window).resize(checkResize);
        checkResize();
        var now = new Date()
        $scope.dateFormat = moment().format('llll').slice(0, -5);
        $scope.dateFormat2 = moment(new Date(now.getFullYear(), now.getMonth(),1)).format('MMM YYYY');
        $scope.dateFormat3 = moment(new Date(now.getFullYear(), now.getMonth()-1,1)).format('MMM YYYY');
        $scope.dateFormat4 = moment(new Date(now.getFullYear(), now.getMonth()-2,1)).format('MMM YYYY');
        $scope.dateFormat5 = moment(new Date(now.getFullYear(), now.getMonth()-3,1)).format('MMM YYYY');
        $scope.dateFormat6 = moment(new Date(now.getFullYear(), now.getMonth()-4,1)).format('MMM YYYY');
        $scope.dateFormat7 = moment(new Date(now.getFullYear(), now.getMonth()-5,1)).format('MMM YYYY');
        $scope.dateFormat8 = moment(new Date(now.getFullYear(), now.getMonth()-6,1)).format('MMM YYYY');
        $scope.dateFormat9 = moment(new Date(now.getFullYear(), now.getMonth()-7,1)).format('MMM YYYY');
        $scope.dateFormat10 = moment(new Date(now.getFullYear(), now.getMonth()-8,1)).format('MMM YYYY');
        $scope.dateFormat11 = moment(new Date(now.getFullYear(), now.getMonth()-9,1)).format('MMM YYYY');
        $scope.dateFormat12 = moment(new Date(now.getFullYear(), now.getMonth()-10,1)).format('MMM YYYY');
        $scope.dateFormat13 = moment(new Date(now.getFullYear(), now.getMonth()-11,1)).format('MMM YYYY');
        $rootScope.options = {
            showMap: true
    };


    var getSignalementStats = function() {
        edisonAPI.signalement.stats().then(function(resp) {
            $scope.signalementStats = resp.data;
        })
    }
    getSignalementStats()

    var reloadStats = function() {
        edisonAPI.stats.telepro()
            .success(function(result) {
                $scope.userStats = _.find(result, function(e) {
                    return e.login === $rootScope.user.login;
                });
                $rootScope.interventionsStats = result;
            });

        edisonAPI.intervention.dashboardStats({
                date: moment().startOf('day').toDate()
            })
            .then(function(resp) {
                _this.statsTeleproBfm = _.sortBy(resp.data.weekStats, 'total').reverse();
            });

    };

    _this.createLitige = function() {
        dialog.createLitige(function(data) {
            edisonAPI.intervention.createLitige({ data: data, user: app_session.login }).then(function(returnEnd) {
                if (returnEnd.data == "YES")
                    LxNotificationService.success("Litige ajouté");
                else
                    LxNotificationService.error("Une erreur est survenue");
            })
        })
    }

    _this.changeStatsTeleproBfm = function(type) {
        if (!_this.indexStatsTeleproBfm)
            _this.indexStatsTeleproBfm = 0;
        if (type == 1)
        {
            if ((_this.indexStatsTeleproBfm + 5) >= _this.statsTeleproBfm.length)
                _this.indexStatsTeleproBfm = _this.indexStatsTeleproBfm;
            else
                _this.indexStatsTeleproBfm += 5;
        }
        else if (type == 2)
        {
            if ((_this.indexStatsTeleproBfm - 5) <= 0)
                _this.indexStatsTeleproBfm = 0;
            else
                _this.indexStatsTeleproBfm -= 5;
        }
    }

    $rootScope.user = window.app_session
    reloadStats();
    socket.on('filterStatsReload', _.debounce(reloadStats, _.random(0, 1000)));

    $window.notify = function() {
        LxNotificationService.notify("test", 'android', false, "red");
    }

    socket.on('notification', function(data) {

        if (data.dest === $rootScope.user.login && (data.dest !== data.origin || data.self)) {
	    if (data.sticky === true)
		LxNotificationService.notify(data.message, data.icon || 'android', true, data.color);
	    else
		LxNotificationService.notify(data.message, data.icon || 'android', false, data.color);
        }
        if (data.service && data.service === $rootScope.user.service) {
	    if (data.sticky === true)
		LxNotificationService.notify(data.message, data.icon || 'android', true, data.color);
	    else
		LxNotificationService.notify(data.message, data.icon || 'android', false, data.color);
        }
    })

    $rootScope.openTab = function(tab) {

    }

    $rootScope.closeContextMenu = function(ev) {
        $rootScope.$broadcast('closeContextMenu');
    }

    var devisDataProvider = new DataProvider('devis')
    var artisanDataProvider = new DataProvider('artisan')
    var interventionDataProvider = new DataProvider('intervention')

    this.tabContainer = TabContainer;
    $scope.$on("$locationChangeStart", function(event) {
        if (_.includes(["/intervention", '/devis', '/artisan', '/'], $location.path())) {
            return 0
        }
        TabContainer.add($location).order();
    })

    $scope.taskList = taskList;

    $scope.linkClick = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
    };

    Mousetrap.bind(['command+i', 'ctrl+i'], function() {
        dialog.declareBug(_this.tabContainer, function(err, resp) {
            edisonAPI.bug.declare(resp).then(function() {
                    LxNotificationService.error("Le Serice informatique en a été prevenu");
                })
        })
    });

    $scope.tabIconClick = function($event, tab) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.tabs.close(tab)
    };
});
