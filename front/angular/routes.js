var getIntervention = function($route, $q, edisonAPI) {
    "use strict";
    var id = $route.current.params.id;
    if ($route.current.params.d) {
	var deferred = $q.defer();
	$q.all([
	    edisonAPI.devis.get($route.current.params.d, {
		select: 'date login produits tva client prixAnnonce categorie -_id'
	    }),
	    edisonAPI.intervention.getTmp(0)
	]).then(function(result) {
	    deferred.resolve(_.merge(result[1], result[0]));
	})
	return deferred.promise;

    } else if (id.length > 10) {
	return edisonAPI.intervention.getTmp(id);
    } else {
	return edisonAPI.intervention.get(id, {
	    populate: 'sst'
	});
    }
};

var statsTelepro = function($route, $q, edisonAPI) {
    return edisonAPI.stats.telepro()
}

var getDevis = function($route, $q, edisonAPI) {
    "use strict";
    var id = $route.current.params.id;
    if ($route.current.params.i) {
	return edisonAPI.intervention.get($route.current.params.i, {
	    select: 'client categorie tva -_id conversations'
	});
    } else if (id.length > 10) {
	return edisonAPI.devis.getTmp(id);
    } else {
	return edisonAPI.devis.get(id);
    }
};
var getArtisan = function($route, $q, edisonAPI) {
    "use strict";
    var id = $route.current.params.id;
    if (id.length > 10) {
	return edisonAPI.artisan.getTmp(id);
    } else {
	return edisonAPI.artisan.get(id);
    }
}

angular.module('edison').config(function($routeProvider, $locationProvider) {
    "use strict";
    $routeProvider
	.when('/', {
	    redirectTo: '/dashboard',
	})
	.when('/intervention/list', {
	    templateUrl: "Pages/ListeInterventions/listeInterventions.html",
	    controller: "ListeInterventionController",
	    controllerAs: 'vm',
	    reloadOnSearch: false

	})
	.when('/intervention/list/:fltr', {
	    templateUrl: "Pages/ListeInterventions/listeInterventions.html",
	    controller: "ListeInterventionController",
	    controllerAs: 'vm',
	    reloadOnSearch: false

	})
	.when('/intervention', {
	    redirectTo: function(routeParams, path, params) {
		var url = params.devis ? "?d=" + params.devis : "";
		url += params.i ? (url == "" ? "?i=" + params.i : "&i=" + params.i) : "";
		url += params.di ? (url == "" ? "?di=" + params.di : "&di=" + params.di) : "";
		url += params.sa ? (url == "" ? "?sa=" + params.sa : "&sa=" + params.sa) : "";
		return '/intervention/' + Date.now() + url;
	    }
	})
	.when('/intervention/:id', {
	    templateUrl: "Pages/Intervention/intervention.html",
	    controller: "InterventionController",
	    controllerAs: "vm",
	    reloadOnSearch: false,
	    resolve: {
		interventionPrm: getIntervention,
	    }
	})
	.when('/devis/list', {
	    templateUrl: "Pages/ListeDevis/listeDevis.html",
	    controller: "ListeDevisController",
	    controllerAs: 'vm',
	    reloadOnSearch: false

	})
	.when('/devis/list/:fltr', {
	    templateUrl: "Pages/ListeDevis/listeDevis.html",
	    controller: "ListeDevisController",
	    controllerAs: "vm",
	    reloadOnSearch: false

	})
	.when('/devis', {
	    redirectTo: function(routeParams, path, params) {
		var url = params.i ? "?i=" + params.i : "";
		return '/devis/' + Date.now() + url;
	    }
	})
	.when('/devis/:id', {
	    templateUrl: "Pages/Intervention/devis.html",
	    controller: "DevisController",
	    controllerAs: "vm",
	    resolve: {
		devisPrm: getDevis,
	    }
	})
	.when('/prospect/:id', {
	    templateUrl: "Pages/Prospect/prospect.html",
	    controller: "ProspectController",
	    controllerAs: "vm",
	})
    	.when('/prospect/:addr/:cate', {
	    templateUrl: "Pages/Prospect/prospect.html",
	    controller: "ProspectController",
	    controllerAs: "vm",
	})
	.when('/artisan/contact', {
	    templateUrl: "Pages/ListeArtisan/contactArtisan.html",
	    controller: "ContactArtisanController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/artisan/:sstid/recap', {
	    templateUrl: "Pages/ListeArtisan/contactArtisan.html",
	    controller: "ContactArtisanController",
	    controllerAs: 'vm',
	    reloadOnSearch: false

	})
	.when('/artisan/list', {
	    templateUrl: "Pages/ListeArtisan/listeArtisan.html",
	    controller: "ListeArtisanController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/artisan/list/:fltr', {
	    templateUrl: "Pages/ListeArtisan/listeArtisan.html",
	    controller: "ListeArtisanController",
	    controllerAs: "vm",
	    reloadOnSearch: false
	})
	.when('/artisan', {
	    redirectTo: function() {
		return '/artisan/' + Date.now();
	    }
	})
	.when('/artisan/:id', {
	    templateUrl: "Pages/Artisan/artisan.html",
	    controller: "ArtisanController",
	    controllerAs: "vm",
	    resolve: {
		artisanPrm: getArtisan,
	    }
	})
	.when('/dashboard', {
	    controller: 'DashboardController',
	    templateUrl: "Pages/Dashboard/dashboard.html",
	    controllerAs: "vm",
	    resolve: {
		statsTelepro: statsTelepro
	    }
	})
	.when('/search/:query', {
	    templateUrl: "Pages/Search/search.html",
	    controller: "SearchController",
	    controllerAs: "vm",
	})
	.when('/compta/lpa', {
	    templateUrl: "Pages/LPA/LPA.html",
	    controller: "LpaController",
	    controllerAs: "vm",
	})
	.when('/compta/avoirs', {
	    templateUrl: "Pages/Avoirs/avoirs.html",
	    controller: "avoirsController",
	    controllerAs: "vm",
	})
	.when('/compta/archivesPaiement', {
	    templateUrl: "Pages/Archives/archives.html",
	    controller: "archivesPaiementController",
	    controllerAs: "vm",
	})
	.when('/compta/remiseCheques', {
	    templateUrl: "Pages/Archives/archives.html",
	    controller: "remiseChequesController",
	    controllerAs: "vm",
	})
	.when('/compta/archivesReglement', {
	    templateUrl: "Pages/Archives/archives.html",
	    controller: "archivesReglementController",
	    controllerAs: "vm",
	})
        .when('/compta/fournitures', {
	    templateUrl: "Pages/Tools/fournitureMonthly.html",
	    controller: "fournitureMonthly",
	    controllerAs: 'vm',
	})
        .when('/compta/chiffreGrandComptes', {
	    templateUrl: "Pages/Tools/chiffreGrandComptes.html",
	    controller: "chiffreGrandComptes",
	    controllerAs: 'vm',
	})
	.when('/tools/vcf', {
		templateUrl: "Pages/Tools/vcf.html",
		controller: "vcfController",
		controllerAs: "vm",
	})
	.when('/tools/telephoneMatch', {
	    templateUrl: "Pages/Tools/telephoneMatch.html",
	    controller: "telephoneMatch",
	    controllerAs: "vm",
	})
	.when('/tools/editProducts', {
	    templateUrl: "Pages/Tools/edit-products.html",
	    controller: "editProducts",
	    controllerAs: "vm",
	})
        .when('/tools/ovh', {
	    templateUrl: "Pages/Tools/ovhPage.html",
	    controller: "ovhController",
	    controllerAs: "vm",
	})
        .when('/tools/ovhCalls', {
	    templateUrl: "Pages/Tools/ovhCalls.html",
	    controller: "ovhCallsController",
	    controllerAs: "vm",
	})
	.when('/tools/editSignalements', {
	    templateUrl: "Pages/Tools/edit-signalements.html",
	    controller: "editSignalements",
	    controllerAs: "vm",
	})
	.when('/listeSignalements', {
	    templateUrl: "Pages/ListeSignalements/liste-signalements.html",
	    controller: "listeSignalements",
	    controllerAs: "vm",
	   // reloadOnSearch: false
	})
	.when('/tools/editComptes', {
	    templateUrl: "Pages/Tools/edit-comptes.html",
	    controller: "editComptes",
	    controllerAs: "vm",
	})
	.when('/tools/editFournisseur', {
	    templateUrl: "Pages/Tools/edit-fournisseur.html",
	    controller: "editFournisseur",
	    controllerAs: "vm",
	})
	.when('/tools/editRaison', {
	    templateUrl: "Pages/Tools/edit-raison.html",
	    controller: "editRaison",
	    controllerAs: "vm",
	})
	.when('/tools/editCombos', {
	    templateUrl: "Pages/Tools/edit-combos.html",
	    controller: "editCombos",
	    controllerAs: "vm",
	})
	.when('/tools/editUsers', {
	    templateUrl: "Pages/Tools/edit-users.html",
	    controller: "editUsers",
	    controllerAs: "vm",
	})
    	.when('/tools/editPriv', {
	    templateUrl: "Pages/Tools/edit-priv.html",
	    controller: "editPriv",
	    controllerAs: "vm",
	})
    	.when('/tools/editAcquittance', {
	    templateUrl: "Pages/Tools/edit-acquittance.html",
	    controller: "editAcquittance",
	    controllerAs: "vm",
	})
	.when('/partenariat/comissions', {
	    templateUrl: "Pages/Tools/commissions-partenariat.html",
	    controller: "commissionsPartenariat",
	    controllerAs: 'vm',
	})
	.when('/tools/comRecouvrement', {
	    templateUrl: "Pages/Tools/commissions-recouvrement.html",
	    controller: "ComRecouvrement",
	    controllerAs: 'vm',
	})
	.when('/tools/comLitiges', {
	    templateUrl: "Pages/Tools/commissions-litiges.html",
	    controller: "ComLitiges",
	    controllerAs: 'vm',
	})
	.when('/tools/commissions', {
	    templateUrl: "Pages/Tools/commissions.html",
	    controller: "CommissionsController",
	    controllerAs: "vm",
	    reloadOnSearch: false

	})
        .when('/tools/emargement', {
            templateUrl: "Pages/Utils/acquittance.html",
            controller: "acquittanceController",
            controllerAs: "vm",
        })
        .when('/tools/outilsGeneraux', {
            templateUrl: "Pages/Utils/general.html",
            controller: "GeneralController",
            controllerAs: "vm",
        })
        .when('/tools/import', {
            templateUrl: "Pages/Utils/import.html",
            controller: "ImportController",
            controllerAs: "vm",
        })
        .when('/utils/demarchage', {
            templateUrl: "Pages/Utils/demarchage.html",
            controller: "DemarchageController",
            controllerAs: "vm",
        })
	.when('/utils/mapDemarchage', {
	    templateUrl: "Pages/Utils/mapDemarchage.html",
	    controller: "MapDemarchageController",
	    controllerAs: "vm"
	})
	.when('/stats/:type', {
	    templateUrl: "Pages/Stats/stats.html",
	    controller: "StatsController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/statsnew/:type', {
	    templateUrl: "Pages/Stats/stats-new.html",
	    controller: "StatsNewController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
    	.when('/statspart/:type', {
	    templateUrl: "Pages/Stats/stats-part.html",
	    controller: "StatsPartController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/gstats/:type', {
	    templateUrl: "Pages/Stats/gstats.html",
	    controller: "GStatsController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/user/history', {
	    templateUrl: "Pages/User/historique.html",
	    controller: "userHistory",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.when('/statistiques', {
	    templateUrl: "Pages/Stats/statsRecouvrement.html",
	    controller: "StatsRecouvrementController",
	    controllerAs: 'vm',
	    reloadOnSearch: false
	})
	.otherwise({
	    redirectTo: '/dashboard'
	});
    // use the HTML5 History API
    $locationProvider.html5Mode(true);
});
