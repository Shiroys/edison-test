var archiveReglementController = function(edisonAPI, TabContainer, $routeParams, $location, LxProgressService) {

    var tab = TabContainer.getCurrentTab();
    var _this = this;
    _this.type = 'reglement';
    _this.title = 'Archives Reglements'
    tab.setTitle('archives RGL')
    LxProgressService.circular.show('#5fa2db', '#globalProgress');
    edisonAPI.compta.archivesReglement().success(function(resp) {
        LxProgressService.circular.hide()
        _this.data = resp
    })
    _this.moment = moment;
    _this.openLink = function(link) {
        $location.url(link)
    }
}

angular.module('edison').controller('archivesReglementController', archiveReglementController);

var archivesPaiementController = function(edisonAPI, TabContainer, $routeParams, $location, LxProgressService) {
    var _this = this;
    var tab = TabContainer.getCurrentTab();
    _this.type = 'paiement'
    _this.title = 'Archives Paiements'
    tab.setTitle('archives PAY')
    LxProgressService.circular.show('#5fa2db', '#globalProgress');
    edisonAPI.compta.archivesPaiement().success(function(resp) {
	LxProgressService.circular.hide()
        _this.data = resp
    })
    _this.moment = moment;
    _this.openLink = function(link) {
        $location.url(link)
    }
}

angular.module('edison').controller('archivesPaiementController', archivesPaiementController);

var remiseChequesController = function(edisonAPI, TabContainer, $filter, $routeParams, $location, LxProgressService, LxNotificationService) {
    var _this = this;
    _this.type = 'remise';
    _this.title = 'Remise de Cheques';
    edisonAPI.compta.listRemises().then(function(resp) {
        _this.data = $filter('orderBy')(resp.data, 'dateRemise');
    })
    _this.moment = moment;
    _this.addCheques = function() {
        edisonAPI.compta.addCheques().then(function(resp) {
            if (resp.data == 'OK') {
                edisonAPI.compta.listRemises().then(function(resp) {
                    _this.data = $filter('orderBy')(resp.data, 'dateRemise');
                })
            } else {
                LxNotificationService.error("Une erreur est survenue");
            }
        })
    }
    _this.validateRemise = function() {
        edisonAPI.compta.validateRemise().then(function(resp) {
            if (resp.data == 'OK') {
                edisonAPI.compta.listRemises().then(function(resp) {
                    _this.data = $filter('orderBy')(resp.data, 'dateRemise');
                })
            } else {
                LxNotificationService.error("Une erreur est survenue");
            }
        })
    }
}

angular.module('edison').controller('remiseChequesController', remiseChequesController);
