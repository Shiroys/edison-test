var DashboardController = function($rootScope, statsTelepro, dialog, user, edisonAPI, $scope, $filter, TabContainer, NgTableParams, $routeParams, $location, LxProgressService, MomentIterator, socket, edisonAPI, config) {
    var _this = this;
    $scope._ = _;
    $scope.root = $rootScope;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Stats');
    $scope.showAll = window.app_env === 'DEVELOPMENT'
    
    // Debut des Stats
    var isset = false;
    var end = new Date();
    var start = new Date(2013, 8, 1)
    _this.dateselects = MomentIterator(start, end).range('month').map(function(e) {
		return {
		    t: e.format('MMM YYYY'),
		    m: e.month() + 1,
		    y: e.year(),
		}
    }).reverse()
    _this.percstat = {};
    _this.percstats = {};
    _this.MtnMonth = "";
    _this.MtnWeek = "";
    _this.name = [];
    edisonAPI.user.loginuser().then(function(resp4){
	    for (var c = 0;c < resp4.data.length;c++)
		_this.name.push(resp4.data[c].login);
    })

    edisonAPI.devis.getInfo({login: user.login}).then(function(resp5) {
		_this.devisData = resp5.data;
    })
  
    var tmp = _this.dateselects[0];
    var dateTarget = {y: tmp.y, m: tmp.m};
    _this.yearSelect = MomentIterator(start, moment(end).add(1,'year')).range('year', {
		format: 'YYYY',
    }).map(function(e) {
		return parseInt(e)
    })
    $scope.selectedDate = _this.dateselects[0];
    $scope.selectedSubDate = _this.dateselects[0];
    $scope.selectedYear = _this.dateselects[0].y;
    
    var fetch = {
		"hour": {desc2: "Inters", desc1: "Devis"},
		"CA": {desc1: "€"},
		"nombre": {desc2: "Ajout", desc3: "Ann", desc1: "A Prog"}
    }
    
    var getSimpleChart = function(type, title, series, categories, data) {

		var min = 1;
		if (data === "hour")
		    min = 7
		var text = "Nombre";
		if (data === 'CA')
		    text = "Chiffre d'affaire"
		return {
		    chart: {
				zoomType: 'x',
				type: type
		    },
		    title: {
				text: title
		    },
		    xAxis: {
				min: min,
				categories: categories,
				tickInterval: 1,
		    },
		    yAxis: {
				min: 0,
				title: {
				    text: text
				},
		    },
		    tooltip: {
				shared: true,
				formatter: function () {
				    var ret = "";
				    if (data === "hour")
						ret = this.x + "h<br>"
				    else
						ret = "Jour " + this.x + "<br>"
				    for (var n = 0; n < this.points.length; n++)
				    {
						if (this.points[n])
						{
						    if (this.points[n].series.linkedSeries.length == 1 && this.points[n].color == "#FF0000")
							ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + " " + fetch[data].desc3 + "<br>";
						    else if (this.points[n].series.linkedSeries.length == 1)
							ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + " " + fetch[data].desc2 + "<br>";
						    else
							ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + " " + fetch[data].desc1 + "<br>";
						}
				    }
				    return ret;
				}
		    },
		    plotOptions: {
				animation: false,
				column: {
				    pointPadding: 0,
				    groupPadding: 0.04,
				    borderWidth: 0,
				    animation: false,
				},
				area: {
				    stacking: 'normal',
				    lineColor: '#666666',
				    lineWidth: 1,
				    marker: {
					lineWidth: 1,
					lineColor: '#666666'
				    }
				},
				areaspline: {
				    stacking: 'normal',
				}
		    },
		    series: series
		}
    }

    
    var getChart = function(type, title, series, categories) {

	return {
	    chart: {
			zoomType: 'x',
			type: type
	    },
	    title: {
			text: title
	    },
	    xAxis: {
		categories: categories
	    },
	    yAxis: {
		min: 0,
		max: 100,
		title: {
		    text: "Pourcentage"
		},
		plotLines: [{
		    value: $rootScope.user.limit,
		    color: 'green',
		    dashStyle: 'shortdash',
		    width: 2,
		}],
		tickInterval: 5,
	    },
	    tooltip: {
		shared: true,
		formatter: function () {
		    var ret = "";
		    if (parseInt(this.points[0].series.points.length) < 14)
		    {
				for (var n = 0; n < this.points.length; n++)
				{
				    if ($rootScope.user.admin || $scope.selectedDivider === "annulation")
				    {
						if (this.points[n])
						{
						    if (this.points[n].series.linkedSeries.length)
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%  " + _this.percstats[this.points[n].series.name].perc[this.points[n].x - 1] + "<br>";
						    else
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "% " + _this.percstats[this.points[n].series.name + "ann"].perc[this.points[n].x - 1] + "<br>";
						}
				    }
				    else
				    {
						if (this.points[n])
						{
						    if (this.points[n].series.linkedSeries.length)
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%" + "<br>";
						    else
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%" + "<br>";
						}
				    }	    
				}
		    }
		    else
		    {
				for (var n = 0; n < this.points.length; n++)
				{
				    if ($rootScope.user.root || $scope.selectedDivider === "annulation")
				    {
						if (this.points[n])
						{
						    if (this.points[n].series.linkedSeries.length)
						    {
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%  " + _this.percstat[this.points[n].series.name].perc[this.points[n].x - 1] + "<br><br>";
						    }
						    else
						    {
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%  " + _this.percstat[this.points[n].series.name + "ann"].perc[this.points[n].x - 1] + "<br>";
						    }
						}
				    }
				    else
				    {
						if (this.points[n])
						{
						    if (this.points[n].series.linkedSeries.length)
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%" + "<br>";
						    else
								ret += '<span style="color:'+this.points[n].color+'">\u25CF</span>' + this.points[n].series.name + ": " + this.points[n].y + "%" + "<br>";
						}
				    }	    
				}
		    }
		    return ret; 
		}
	    },
	    plotOptions: {
		animation: false,
		column: {
		    pointPadding: 0,
		    groupPadding: 0.04,
		    borderWidth: 0,
		    animation: false,
		},
		area: {
		    stacking: 'normal',
		    lineColor: '#666666',
		    lineWidth: 1,
		    marker: {
			lineWidth: 1,
			lineColor: '#666666'
		    }
		},
		areaspline: {
		    stacking: 'normal',
		}
	    },
	    series: series
	}
    }
    if (user.priv === true)
    {
		_this.dividerSelect = [
		    'annulation',
		    'ajout'
		]
    }
    else
    {
		_this.dividerSelect = [
		    'chiffre',
		    'annulation',
		    'ajout'
		]
    }
    $scope.selectedDivider = 'annulation'
    if (!user.admin)	
		$scope.selectedDivider = 'chiffre'
    
    _this.typeSelect = [
	'column',
	'areaspline',
	'area',
	'line',
	'pie',
	'bar',
	'spline',
    ]
    $scope.selectedType = 'column'

    _this.subTypeSelect = [
    'Total CA',
	'CA',
	'nombre',
    ]
    $scope.selectedSubType = 'CA'

    var moveIt = function (elem)
    {
		for (var l = 0; l < elem.series.length; l++)
		{
		    elem.series[l].data.push(0);
		    var cpy;
		    var rep = 0;
		    for (var n = 0; n < elem.series[l].data.length; n++)
		    {
				cpy = elem.series[l].data[n]
				elem.series[l].data[n] = rep;
				rep = cpy;
		    }
		}
		return (elem);
    }

    var moveOneUp = function (elem, isArray)
    {
		if (isArray)
		{
		    for (var n = 0; n < elem.length; n++)
		    {
				elem[n] = moveIt(elem[n]);
		    }
		}
	/*	else
		    elem = moveIt(elem);*/
		return (elem);
    }

    var loadData = function (group)
    {
	edisonAPI.intervention.getHourlyStat(group).then(function(resp) {
	    var d = moveOneUp(resp.data, group.divider !== 'CA');
	    var serie = [];
	    if (!user.admin)
	    {
			if (group.divider != 'CA')
			{
			    for (var i = 0; i < d[0].series.length; i++)
			    {
				if (d[0].series[i].name === user.login || (d[0].series[i].obs === true && user.priv === true))
				{
				    if (d[0].series[i].name === user.login)
						_this.name = d[0].series[i].name;
				    serie.push(d[0].series[i], d[1].series[i]);
				    if (d.length === 3)
						serie.push(d[2].series[i]);
				}
			    }
			}
			else
			{
			    for (var i = 0; i < d.series.length; i++)
			    {
					if (d.series[i].name === user.login || (d.series[i].obs === true && user.priv === true))
					{
					    if (d.series[i].name === user.login)
							_this.name = d.series[i].name;
					    serie.push(d.series[i]);
					}
			    }
			}
	    }
	    else
	    {
		if (group.divider != 'CA')
		{
		    serie = d[0].series.concat(d[1].series);
		    if (d.length === 3)
				serie = serie.concat(d[2].series);
		}
		else
		    serie = d.series
	    }
	    serie.sort(function (a,b){
		if (a.name > b.name)
		    return 1;
		if (a.name < b.name)
		    return -1;
		return 0;
	    });
	    if (d.length === 3)
	    {
			for (var j = 0;j < serie.length - 1; j = j + 3)
			{
			    if (typeof serie[j].color !== "undefined" && serie[j].linkedTo === ':previous')
			    {
					var temp = serie[j];
					serie[j] = serie[j+1];
					serie[j+1] = temp;
					if (typeof serie[j].color !== "undefined")
					{
					    temp = serie[j];
					    serie[j] = serie[j+2];
					    serie[j+2] = temp;
					}
			    }
			    else if (typeof serie[j].color !== "undefined" && serie[j].linkedTo === ':previous2')
			    {
					var temp = serie[j];
					serie[j] = serie[j+2];
					serie[j+2] = temp;
					if (typeof serie[j].color !== "undefined")
					{
					    temp = serie[j];
					    serie[j] = serie[j+1];
					    serie[j+1] = temp;
					}
			    }
			    else if (typeof serie[j+1].color !== "undefined" && serie[j+1].linkedTo === ':previous2')
			    {
					var temp = serie[j+1];
					serie[j+1] = serie[j+2];
					serie[j+2] = temp;
			    }
			    serie[j+2].linkedTo = ':previous'
			}
	    }
	    else
	    {
			for (var i = 0;i < serie.length;i++)
			{
			    for (var j = 0;j < serie.length - i;j = j + 2)
			    {
					if (typeof serie[j].color !== "undefined")
					{
					    var temp = serie[j];
					    serie[j] = serie[j+1];
					    serie[j+1] = temp;
					}
			    }
			}
	    }
	    if (group.group === 'hour')
	    {
			$('#chartContainer').highcharts(getSimpleChart($scope.selectedType, d[0].title, serie, d.categories, group.divider));
	    }
	    else if (group.group === 'day')
	    {
			$('#chartContainer2').highcharts(getSimpleChart($scope.selectedType, d.title ? d.title : d[0].title, serie, d.categories, group.divider));
	    }
	})
    }
    
    var monthChange2 = function(scope)
    {
	var choice = "chiffreVRF";
	if (scope === "annulation")
	    choice = "nbrVRF";
	edisonAPI.intervention.GetStatUser({
	    month: $scope.selectedDate.m,
	    year: $scope.selectedDate.y,
	    group: 'day',
	    model: 'ca',
	    divider: scope,
	}).then(function(resp) {
	    edisonAPI.intervention.GetStatUser({
		month: $scope.selectedDate.m,
		year: $scope.selectedDate.y,
		group: 'day',
		model: 'ca',
		divider: choice
	    }).then(function(resp2) {
		var d = resp.data;
		var d2 = resp2.data;
		var serie = [];
		if(!user.admin)
		{
		    for (var i = 0;i < d.series.length;i++)
		    {
			if (d.series[i].name === user.login || (d.series[i].obs === true && user.priv === true))
			{
			    if (d.series[i].name === user.login)
				_this.name = d.series[i].name;
			    serie.push(d.series[i], d2.series[i]);
			}
		    }
		}
		else
		    serie = d.series.concat(d2.series);
		serie.sort(function (a,b){
		    if (a.name > b.name)
			return 1;
		    if (a.name < b.name)
			return -1;
		    return 0;
		});
		for (var i = 0;i < serie.length;i++)
		{
		    for (var j = 0;j < serie.length - i;j = j + 2)
		    {
			if (typeof serie[j].color !== "undefined")
			{
			    var temp = serie[j];
			    serie[j] = serie[j+1];
			    serie[j+1] = temp;
			}
		    }
		}
		if (scope === 'chiffre' && isset === false)
		{
		    edisonAPI.intervention.GetDayWeek({
			start: new Date(moment().startOf('isoweek').toDate()),
			end: new Date(),
			name: _this.name
		    }).then(function(resp3) {
			_this.MtnWeek = Math.round(resp3.data.elem) + "€";
		    })
		    isset = true;
		}
	    })
	})
    }
    var monthChange = function(scope) {
	var choice = "chiffreVRF";
	if (scope === "annulation")
	    choice = "nbrVRF";
	edisonAPI.intervention.GetStatUser({
	    month: $scope.selectedDate.m,
	    year: $scope.selectedDate.y,
	    group: 'day',
	    model: 'ca',
	    divider: scope,
	}).then(function(resp) {
	    edisonAPI.intervention.GetStatUser({
		month: $scope.selectedDate.m,
		year: $scope.selectedDate.y,
		group: 'day',
		model: 'ca',
		divider: choice
	    }).then(function(resp2) {
		var d = resp.data;
		var d2 = resp2.data;
		var serie = [];
		if(!user.admin)
		{
		    for (var i = 0;i < d.series.length;i++)
		    {
			if (d.series[i].name === user.login  || (d.series[i].obs === true && user.priv === true))
			{
			    if (d.series[i].name === user.login)
				_this.name = d.series[i].name;
			    serie.push(d.series[i], d2.series[i]);
			}
		    }
		}
		else
		    serie = d.series.concat(d2.series);
		serie.sort(function (a,b){
		    if (a.name > b.name)
				return 1;
		    if (a.name < b.name)
				return -1;
		    return 0;
		});
		for (var i = 0; i < serie.length; i++)
		{
		    for (var j = 0; j < serie.length - i; j = j + 2)
		    {
			if (typeof serie[j].color !== "undefined")
			{
			    var temp = serie[j];
			    serie[j] = serie[j+1];
			    serie[j+1] = temp;
			}
		    }
		}
		for (var n = 0; n < serie.length; n++)
		{
		    var percarr = [];
		    var perc = "";
		    for (var t = 0; t < serie[n].data.length; t++)
		    {
				if (!serie[n].linkedTo)
				{
				    perc = Math.round((serie[n].data[t] / (serie[n].data[t] +  serie[n + 1].data[t])) * 100);
				}
				else
				{
				    perc = Math.round((serie[n].data[t] / (serie[n].data[t] +  serie[n - 1].data[t])) * 100);
				}
				if (isNaN(perc))
				{
				    perc = 0;
				}
				percarr.push(perc);
			}
		    serie[n].perc = percarr;
		}
		for (var n = 0; n < serie.length; n++)
		{
		    var tmp = []
		    tmp = serie[n].perc;
		    serie[n].perc = serie[n].data;
		    serie[n].data = tmp;
		    if (!serie[n].linkedTo)
			_this.percstat[serie[n].name] = serie[n];
		    else
			_this.percstat[serie[n].name + "ann"] = serie[n];
		    for (var u = 0;u < serie[n].perc.length;u++)
		    {
			if (!serie[n].linkedTo)
			    _this.percstat[serie[n].name].perc[u] = serie[n].perc[u];
			else
			    _this.percstat[serie[n].name + "ann"].perc[u] = serie[n].perc[u];
		    }
		}
		$('#chartContainer').highcharts(getChart($scope.selectedType, d.title, serie, d.categories));
	    })
	})
    }
    var yearChange2 = function(scope) {
	var choice = "chiffreVRF";
	if (scope === "annulation")
	    choice = "nbrVRF";
	edisonAPI.intervention.GetStatUser({
	    year: $scope.selectedDate.y,
	    group: 'month',
	    model: 'ca',
	    divider: scope,
	}).then(function(resp) {
	    edisonAPI.intervention.GetStatUser({
		year: $scope.selectedDate.y,
		group: 'month',
		model: 'ca',
		divider: choice
	    }).then(function(resp2) {
		var d = resp.data;
		var d2 = resp2.data;
		var serie = [];
		if(!user.admin)
		{
		    for (var i = 0;i < d.series.length;i++)
		    {
			if (d.series[i].name === user.login || (d.series[i].obs === true && user.priv === true))
			    serie.push(d.series[i], d2.series[i]);
		    }
		}
		else
		    serie = d.series.concat(d2.series);
		serie.sort(function (a,b){
		    if (a.name > b.name)
			return 1;
		    if (a.name < b.name)
			return -1;
		    return 0;
		});
		for (var i = 0;i < serie.length;i++)
		{
		    for (var j = 0;j < serie.length - i;j = j + 2)
		    {
			if (typeof serie[j].color !== "undefined")
			{
			    var temp = serie[j];
			    serie[j] = serie[j+1];
			    serie[j+1] = temp;
			}
		    }
		}
		if (scope === 'chiffre')
		{
		    var mtn = 0;
		    for (var k = 1;k < serie.length;k = k + 2)
		    {
			mtn += Math.round(serie[k].data[$scope.selectedDate.m - 1]);
		    }
		    _this.MtnMonth = mtn + "€";    
		}
	    })
	})
    }
    var yearChange = function(scope) {
		var choice = "chiffreVRF";
		if (scope === "annulation")
		    choice = "nbrVRF";
		edisonAPI.intervention.GetStatUser({
		    year: $scope.selectedDate.y,
		    group: 'month',
		    model: 'ca',
		    divider: scope,
		}).then(function(resp) {
		    edisonAPI.intervention.GetStatUser({
			year: $scope.selectedDate.y,
			group: 'month',
			model: 'ca',
			divider: choice
		    }).then(function(resp2) {
			var d = resp.data;
			var d2 = resp2.data;
			var serie = [];
			if(!user.admin)
			{
			    for (var i = 0;i < d.series.length;i++)
			    {
				if (d.series[i].name === user.login || (d.series[i].obs === true && user.priv === true))
				    serie.push(d.series[i], d2.series[i]);
			    }
			}
			else
			    serie = d.series.concat(d2.series);
			serie.sort(function (a,b){
			    if (a.name > b.name)
				return 1;
			    if (a.name < b.name)
				return -1;
			    return 0;
			});
			for (var i = 0;i < serie.length;i++)
			{
			    for (var j = 0;j < serie.length - i;j = j + 2)
			    {
				if (typeof serie[j].color !== "undefined")
				{
				    var temp = serie[j];
				    serie[j] = serie[j+1];
				    serie[j+1] = temp;
				}
			    }
			}
			if (scope === 'chiffre')
			{
			    var mtn = 0;
			    for (var k = 1;k < serie.length;k = k + 2)
			    {
				mtn += Math.round(serie[k].data[$scope.selectedDate.m - 1]);
			    }
			    _this.MtnMonth = mtn + "€";
			}
			for (var n = 0; n < serie.length; n++)
			{
			    var percarr = [];
			    var perc = "";
			    for (var t = 0;t < serie[n].data.length;t++)
			    {
					if (!serie[n].linkedTo)
					    perc = Math.round((serie[n].data[t] / (serie[n].data[t] +  serie[n + 1].data[t])) * 100);
					else
					    perc = Math.round((serie[n].data[t] / (serie[n].data[t] +  serie[n - 1].data[t])) * 100);
					if (isNaN(perc))
					    perc = 0;
					percarr.push(perc);
			    }
			    serie[n].perc = percarr;
			}
			for (var n = 0; n < serie.length; n++)
			{
			    var tmp = []
			    tmp = serie[n].perc;
			    serie[n].perc = serie[n].data;
			    serie[n].data = tmp;
			    if (!serie[n].color)
					_this.percstats[serie[n].name] = serie[n];
			    else
					_this.percstats[serie[n].name + "ann"] = serie[n];
			    for (var u = 0;u < serie[n].perc.length;u++)
			    {
				if (!serie[n].linkedTo)
				    _this.percstats[serie[n].name].perc[u] = serie[n].perc[u];
				else
				    _this.percstats[serie[n].name + "ann"].perc[u] = serie[n].perc[u];
			    }
			    
			}
			if (scope === $scope.selectedDivider)
			{
			    $('#chartContainer2').highcharts(getChart($scope.selectedType, d.title, serie, d.categories));
			}
		    });
		})
    }
    
    $scope.$watch("selectedType", function()  {
	if ($scope.selectedDivider != 'ajout')
	{
	    monthChange2('chiffre');
	    monthChange($scope.selectedDivider);
	    yearChange2('chiffre');
	    yearChange($scope.selectedDivider);
	}
	else
	{
	    loadData({
			group: 'hour',
			divider: 'hour',
	    });
	    loadData({
			month: $scope.selectedSubDate.m,
			year: $scope.selectedSubDate.y,
			group: 'day',
			divider: $scope.selectedSubType,
	    })
	}
    });
    
    
    $scope.$watch("selectedDivider", function()  {
	if ($scope.selectedDivider != 'ajout')
	{
	    monthChange2('chiffre');
	    monthChange($scope.selectedDivider);
	    yearChange2('chiffre');
	    yearChange($scope.selectedDivider);
	}
	else
	{
	    loadData({
			group: 'hour',
			divider: 'hour',
	    });
	    loadData({
			month: $scope.selectedSubDate.m,
			year: $scope.selectedSubDate.y,
			group: 'day',
			divider: $scope.selectedSubType,
	    })
	}
    });
    
    $scope.$watch("selectedYear", function() {
		dateTarget.y = $scope.selectedYear;
		var tmp = _.find(_this.dateselects, dateTarget);
		$scope.selectedDate = tmp;
		monthChange2('chiffre');
		monthChange($scope.selectedDivider);
		yearChange2('chiffre');
		yearChange($scope.selectedDivider);
    });

    $scope.$watch("selectedSubDate", function(curr) {
		if (!$scope.selectedSubDate.m)
		    $scope.selectedSubDate.m = _this.dateselects[0].m;
		$scope.selectedSubDate.y = parseInt($scope.selectedDate.y);
		if ($scope.selectedDivider === 'ajout')
		{
		    loadData({
			month: $scope.selectedSubDate.m,
			year: parseInt($scope.selectedDate.y),
			group: 'day',
			divider: $scope.selectedSubType,
		    })
		}
    }, true);

    $scope.$watch("selectedSubType", function () {
	if ($scope.selectedDivider === 'ajout')
	{
	    loadData({
			month: $scope.selectedSubDate.m,
			year: parseInt($scope.selectedDate.y),
			group: 'day',
			divider: $scope.selectedSubType,
	    })
	}
    })
    
    $scope.$watch("selectedDate", function(curr) {
		if (!$scope.selectedDate.m)
		    $scope.selectedDate.m = _this.dateselects[0].m;
		$scope.selectedYear = parseInt($scope.selectedDate.y);
		monthChange2('chiffre');
		monthChange($scope.selectedDivider);
		yearChange2('chiffre');
		yearChange($scope.selectedDivider);
    }, true);
    
    //Fin des Stats
    _this.addTask = function() {
		edisonAPI.task.add(_this.newTask).then(_.partial(_this.reloadTask, _this.newTask.to));
    }
    
    _this.check = function(task) {
		edisonAPI.task.check(task._id).then(_.partial(_this.reloadTask, _this.newTask.to))
    }

    edisonAPI.intervention.dashboardStats({
	date: moment().startOf('day').toDate()
    })
	.then(function(resp) {
	    _this.statsTeleproBfm = _.sortBy(resp.data.weekStats, 'total').reverse()
	});

    _this.reloadTask = function(usr) {
	_this.newTask = {
	    to: usr,
	    from: user.login
	}
	edisonAPI.task.listRelevant({
	    login: usr
	}).then(function(resp) {
	    _this.taskList = resp.data;
	})
    }

    _this.reloadTask(user.login);
    
    _this.reloadDashboardStats = function(date) {
		edisonAPI.intervention.dashboardStats(date).then(function(resp) {
		    _this.tableParams = new NgTableParams({
			count: resp.data.weekStats.length,
			sorting: {
			    total: 'desc'
			}
		    }, {
			counts: [],
			data: resp.data.weekStats
		    });
		    _this.stats = resp.data
		})
    }
    _this.dateSelect = [{
	nom: 'Du jour',
	date: moment().startOf('day').toDate()
    }, {
	nom: 'De la semaine',
	date: moment().startOf('week').toDate()
    }, {
	nom: 'Du mois',
	date: moment().startOf('month').toDate()
    }, {
	nom: "De l'année",
	date: moment().startOf('year').toDate()
    }]
    _this.dateChoice = _this.dateSelect[1];
    this.reloadDashboardStats(_this.dateChoice);



    var month = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "O\
ctobre", "Novembre", "Décembre"]

    _this.partDate = {};
    _this.partDate.start = new Date();
    _this.partDate.start.setHours(0,0,0,0);
    _this.partDate.end = new Date();
    _this.partDate.end.setHours(24,0,0,0);
    $scope.partDate = {}
    $scope.partDate.start = _this.partDate.start.getDate() + " " + month[_this.partDate.start.getMonth()] + " " + _this.partDate.start.getFullYear();
    $scope.partDate.end = _this.partDate.end.getDate() + " " + month[_this.partDate.end.getMonth()] + " " + _this.partDate.end.getFullYear();

    _this.partDate2 = {};
    _this.partDate2.start = new Date();
    _this.partDate2.start.setHours(0,0,0,0);
    _this.partDate2.end = new Date();
    _this.partDate2.end.setHours(24,0,0,0);
    $scope.partDate2 = {}
    $scope.partDate2.start = _this.partDate2.start.getDate() + " " + month[_this.partDate2.start.getMonth()] + " " + _this.partDate2.start.getFullYear();
    $scope.partDate2.end = _this.partDate2.end.getDate() + " " + month[_this.partDate2.end.getMonth()] + " " + _this.partDate2.end.getFullYear();


    _this.updatePartIntStats = function ()
    {
		var d = _this.partDate2;
		edisonAPI.intervention.ptStats({array: _this.userList, start: d.start, end: d.end}).then(function (resp) {
		    var table = []
		    for (var n = 0; n < _this.userList.length; n++)
		    {
				resp.data[_this.userList[n]].login = _this.userList[n];
				table.push(resp.data[_this.userList[n]])
		    }
		    _this.prStatsInt = new NgTableParams({
				count: table.length,
		    }, {
				counts: [],
				data: table
		    });
		})
    }
    
    _this.StatsCompta = function () {
	var array2 = []
	var ajd = new Date()
	edisonAPI.intervention.CountRecu().then(function(resp2){
	    edisonAPI.intervention.ComptaStats(resp2.data).then(function(resp){
		var array = []
		for(var key in resp.data) {
		    var key2 = key
		    var  test = key2.length
		    var test1 = key2.substring(0,test - 2)
		    var test2 = test1.charAt(0).toUpperCase() + test1.substring(1).toLowerCase()
		    resp.data[key].login = "2F" + ajd.getFullYear() + "#" + key
		    resp.data[key].name = test2
		    array.push(resp.data[key])
		}
		_this.nbrStats = array
	    })
	})
	edisonAPI.intervention.getLitiges().then(function(resp3){
	    for(var key in resp3.data) {
		var key2 = key
		var  test = key2.length
		var test1 = key2.substring(0,test - 2)
		var test2 = test1.charAt(0).toUpperCase() + test1.substring(1).toLowerCase()
		resp3.data[key].name = test2
		resp3.data[key].redirect = "2F" + ajd.getFullYear() + "#" + key
		array2.push(resp3.data[key])
	    }
	    _this.nbrYearStats = array2
	})
    }
    _this.StatsCompta()
    _this.updatePartStats = function () {
		var d = _this.partDate;
		edisonAPI.artisan.ptStats({array: _this.userList, start: d.start, end: d.end}).then(function (resp) {
		    var table = []
		    for (var n = 0; n < _this.userList.length; n++)
		    {
				resp.data[_this.userList[n]].login = _this.userList[n];
				table.push(resp.data[_this.userList[n]])
		    }
		    _this.prStats = new NgTableParams({
				count: table.length,
				sorting: {
				    transf: 'desc'
				}
		    }, {
				counts: [],
				data: table
		    });
		})
    }
    
    _this.getUserList = function () {
		edisonAPI.users.list().then(function (resp) {
		    _this.userList = []
		    for (var n = 0; n < resp.data.length; n++)
		    {
				if (resp.data[n].activated === true && resp.data[n].service === 'PARTENARIAT')
				    _this.userList.push(resp.data[n].login);
		    }
		    _this.updatePartStats();
		    _this.updatePartIntStats();
		})
    }
    _this.getUserList();

    _this.paEnvoi = function (login) {
	$location.url("/intervention/list/paEnvTot?ev=" + login);
    }

    _this.artisanLink = function (login) {
	$location.url("/artisan/list#" + login);
    }

    $scope.$watch('partDate.start', function () {
		if ($scope.partDate && $scope.partDate.start)
		{
		    var split = $scope.partDate.start.split(" ");
		    _this.partDate.start = new Date(split[2], month.indexOf(split[1]), split[0]);
		    if (_this.userList)
			_this.updatePartStats();
		}
    })

    $scope.$watch('partDate.end', function () {
		if ($scope.partDate && $scope.partDate.end)
		{
		    var split = $scope.partDate.end.split(" ");
		    _this.partDate.end = new Date(split[2], month.indexOf(split[1]), split[0]);
		    if (_this.userList)
			_this.updatePartStats();
		}
    })

    $scope.$watch('partDate2.start', function () {
		if ($scope.partDate2 && $scope.partDate2.start)
		{
		    var split = $scope.partDate2.start.split(" ");
		    _this.partDate2.start = new Date(split[2], month.indexOf(split[1]), split[0]);
		    if (_this.userList)
			_this.updatePartIntStats();
		}
    })

    $scope.$watch('partDate2.end', function () {
		if ($scope.partDate2 && $scope.partDate2.end)
		{
		    var split = $scope.partDate2.end.split(" ");
		    _this.partDate2.end = new Date(split[2], month.indexOf(split[1]), split[0]);
		    if (_this.userList)
			_this.updatePartIntStats();
		}
    })
}

angular.module('edison').controller('DashboardController', DashboardController);

