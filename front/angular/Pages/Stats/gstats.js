var GStatsController = function(MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope, $location, LxProgressService, socket) {
  "use strict";
  edisonAPI.intervention.gstats().then(function(resp) {
    $('#chartContainer').highcharts({
      chart: {
        type: 'column'
      },
      title: {
        text: 'Pourcentage de paiements'
      },
      xAxis: {
        categories:resp.data.categories
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total fruit consumption'
        }
      },
      tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
      },
      plotOptions: {
        column: {
          stacking: 'percent'
        }
      },
      series: resp.data.series
    })
  })
}
angular.module('edison').controller('GStatsController', GStatsController);
