var ListArtisanMapController = function($rootScope, edisonAPI, $scope) {
    var _this = this;
    $scope._ = _;
    $scope.root = $rootScope;

    edisonAPI.artisan.starList().then(function (resp) {
	_this.confirmedArtisans = resp.data;
    });

}

angular.module('edison').controller('ListArtisanMapController', ListArtisanMapController);
