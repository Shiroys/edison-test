angular.module('edison').directive('encaissement',
				   function(config, Paiement, Intervention, textTemplate, edisonAPI) {
        "use strict";
        return {
            restrict: 'E',
            templateUrl: '/Templates/encaissement.html',
            scope: {
                data: "=",
                displayEncaissement: '@',
                dialog: '@',
                simulator: '@'
            },
            link: function(scope, element, attrs) {
                console.log("ENCAISSEMENT: ", scope, element, attrs);
                if (scope.displayEncaissement) {
                    scope.showEncaissement = true;
                }
                scope.changeTva = function(index) {
                    $scope.data.compta.encaissement[index].tva = scope.data.tva;
                }
            },
        }
    }
);