var InterventionCtrl = function(Description, Signalement, ContextMenu, $window, $timeout, $rootScope, $scope, $location, $routeParams, dialog, fourniture, LxNotificationService, LxProgressService, TabContainer, edisonAPI, Address, $q, mapAutocomplete, productsList, config, interventionPrm, Intervention, Map, user) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.dialog = dialog;
    _this.autocomplete = mapAutocomplete;
    _this.displayUser = app_session;
    var tab = TabContainer.getCurrentTab();
    if (!tab.data) {
        var intervention = new Intervention(interventionPrm.data)
        intervention.sst__id = intervention.sst ? intervention.sst.id : 0;
        tab.setData(intervention);
        if ($routeParams.id.length > 12) {
            _this.isNew = true;
            intervention.tmpID = $routeParams.id;
            intervention.tmpDate = moment.unix(intervention.tmpID / 1000).format('HH[h]mm')
            tab.setTitle(intervention.tmpDate);
        } else {
            if (intervention && intervention.client.nom) {
                var __title = intervention.client.civilite + intervention.client.nom
                __title = __title.slice(0, 10);
            } else {
                var __title = '#' + $routeParams.id;
            }
            tab.setTitle(__title);
            if (!intervention) {
                LxNotificationService.error("Impossible de trouver les informations !");
                $location.url("/dashboard");
                TabContainer.remove(tab);
                return 0;
            }
        }
    } else {
        var intervention = tab.data;
    }
    if ($routeParams.d) {
        intervention.devisOrigine = parseInt($routeParams.d)
        intervention.date = {
            ajout: new Date(),
            intervention: new Date(),
        }
        intervention.reglementSurPlace = true;
        intervention.modeReglement = 'CH';
        intervention.remarque = 'PAS DE REMARQUES';
    }
    if ($routeParams.i) {
		edisonAPI.intervention.get(parseInt($routeParams.i)).then(function (resp) {
	    	if (resp.data != "")
	    	{
				intervention.client = resp.data.client;
				intervention.categorie = resp.data.categorie;
				intervention.comments = [];
				intervention.facture = resp.data.facture;
				intervention.description = resp.data.description
				intervention.remarque = resp.data.remarque;
				intervention.produits = resp.data.produits;
				intervention.file = resp.data.file;
				if ($routeParams.sa)
				{
		    		intervention.status = "SAV"
		    		intervention.statusSAV = "APR"
		    		intervention.sst = resp.data.sst
		    		intervention.artisan = resp.data.artisan
		    		intervention.oldid = $routeParams.i
				}
				intervention.comments.push({
                    login: user.login,
                    text: "Duplication de l'intervention n°" + parseInt($routeParams.i),
                    date: new Date()
				})
	    	}
		})
    }
    else if ($routeParams.di) {
		edisonAPI.devis.get(parseInt($routeParams.di)).then(function (resp2) {
	    	intervention.client = resp2.data.client;
	    	intervention.categorie = resp2.data.categorie
	    	intervention.produits = resp2.data.produits;
	    	intervention.comments = [];
	    	intervention.comments.push({
				login: user.login,
				text: "Duplication du devis n°" + parseInt($routeParams.di),
				date: new Date()
	    	})
		})
    }

    if (tab.data.reglementSurPlace)
        _this.saveReglementSurPlace = tab.data.reglementSurPlace;

    _this.data = tab.data;

	_this.changeStatus = function(newStatus) {
		if (newStatus != "")
		{
			var tabStatus = ["ENC", "VRF", "APR", "AVR", "EVR", "ANN", "SAV", "DVG", "PRT", "RGL"];
			if (newStatus == "SAV")
			{
				var saveStatus = _this.data.status;
				_this.data.status = newStatus;
				_this.data.cache.s = tabStatus.indexOf(newStatus);
				_this.data.cache.sa = saveStatus;
				_this.data.statusSAV = saveStatus;
			}
			else
			{
				_this.data.status = newStatus;
				_this.data.cache.s = tabStatus.indexOf(newStatus);
				_this.data.cache.sa = "";
				_this.data.statusSAV = "";
			}
		}
	}

    _this.loadFournitures = function () {
	edisonAPI.marker.list({categorie: _this.data.categorie}).then(function (resp) {
	    _this.fournCoord = resp.data;
	})
    }

    _this.fourndatafunc = function () {
    	if (_this.data.relancecheque.level === 1)
    	{
    	    var data = "L'artisan possède toujours le règlement";
    	    _this.data.fournfile = data;
    	}
    	if(_this.data.relancecheque.level === 2)
    	{
    	    var data = "L'artisan a envoyé le règlement"
    	    _this.data.fournfile = data;
    	}
    	if (_this.data.relancecheque.level === 3)
    	{
    	    var data = "L'artisan dit ne pas avoir pris de règlement"
    	    _this.data.fournfile = data;
    	}
    	var dateStr = new Date();
    	var dArr = [];
    	dArr = _.words(dateStr), /[^- ]+/;
    	dArr[1] = dateStr.getMonth() + 1;
    	var date1 = dArr[2] + "/" + dArr[1] + "/" + dArr[3];
    	_this.data.fournpar = user.login
    	_this.data.fourndate = date1
    	if (!_this.data.fourndata)
    	    var test = []
    	else
    	    var test = _this.data.fourndata
    	test.push({login: user.login,statut: data,date: dateStr})
    	_this.data.fourndata = test
    	edisonAPI.intervention.save(resp.data)
    }
    
    _this.changeCours = function(){

    	_this.data.status = "ENC"
    	edisonAPI.intervention.save(_this.data)
        LxNotificationService.success("Intervention est passé au status \"En cours\"");
    }

    _this.checkConflict = function(check) {
    	if (check === 'opp' && _this.sspro === true)
    	    _this.sspro = false;
    	if (check === 'ssp' && _this.oppo === true)
    	    _this.oppo = false;
    }
    
    $scope.bv = {
        show: function(view) {
            if (this[view]) {
                return (this[view] = false);
            }
	    	this.comments = false
            this.historique = false;
            this.absence = false;
            this.signalement = false;
	   		this.calls = false;
            this[view] = true;
        },
        init: function() {
            this.comments = this.historique = this.absence = this.signalement = false;
            _this.searchArtisans(intervention.categorie)
            $timeout(function() {
                _this.searchArtisans(intervention.categorie)
            }, 666)
        }
    }
    var updateTitle = _.throttle(function() {
        tab.setTitle(_.template("{{typeof tmpDate == 'undefined' ? id : tmpDate}} - {{client.civilite}} {{client.nom}} ({{client.address.cp}})")(intervention));
    }, 1000)

    $scope.$watch('vm.data.client', updateTitle, true)
    updateTitle();
    _this.description = new Description(intervention);
    _this.signalement = new Signalement(intervention)
    _this.contextMenuIntervention = new ContextMenu('intervention')
    _this.contextMenuSST = new ContextMenu('artisan')
    _this.contextMenu = _this.contextMenuIntervention
    _this.contextMenu.setData(intervention);
    
    _this.callSwipe = function (sst) {
	dialog.callArtisan(sst, function(data) {
	    $window.open('tel:' + data, '_self', false);
	});
    }
    
    _this.isAbsent = function(abs) {
        if (!abs || !abs.length) {
            return false;
        }
        return moment().isAfter(abs[abs.length - 1].start) && moment().isBefore(abs[abs.length - 1].end)
    }


    _this.rowRightClick = function($event, inter) {
        if ($('.map-box').has($event.target).length) {
            var id = $event.target.getAttribute('id-sst') || _.get(intervention, 'sst.id')
            if (id) {
                _this.rightClickArtisan = id;
                edisonAPI.artisan.get(id).then(function(resp) {
                    _this.contextMenu = _this.contextMenuSST;
                    _this.contextMenu.setData(resp.data);
                    _this.contextMenu.setPosition($event.pageX, $event.pageY + 200)
                    _this.contextMenu.open().onClose(_.debounce(function(resp) {
                        _this.searchArtisans(intervention.categorie)
                    }, 500))
                })
            }

        } else if ($('.listeInterventions').has($event.target).length == 0) {
            _this.contextMenu = this.contextMenuIntervention;
            _this.contextMenu.setPosition($event.pageX, $event.pageY + 200)
            _this.contextMenu.open();
        }
    }

    Mousetrap.bind(['command+k', 'ctrl+k', 'command+f1', 'ctrl+f1'], function() {
        $window.open("appurl:", '_self');
        edisonAPI.intervention.scan(intervention.id).then(function() {
            $scope.loadFilesList();
            LxNotificationService.success("Le fichier est enregistré");
        })
        return false;
    });


    $scope.calculPrixFinal = function() {
        if (intervention.reglementSurPlace) {
            return 0;
        }
        intervention.prixFinal = 0;
        _.each(intervention.produits, function(e)  {
            intervention.prixFinal += (e.pu * e.quantite)
        })
        intervention.prixFinal = Math.round(intervention.prixFinal * 100) / 100;
    }

    $scope.addLitige = function() {
        dialog.getText({
            title: "Description du Litige",
            text: ""
        }, function(resp) {
            if (!intervention.litiges)
                intervention.litiges = [];
            intervention.litiges.push({
                date: new Date(),
                login: $rootScope.user.login,
                description: resp,
                regle: false
            })
        })
    }

    $scope.smsArtisan = function() {
        intervention.smsArtisan(function(err, resp) {
            if (!err)
                intervention.sst.sms.unshift(resp)
        })
    }


    $scope.clickTrigger = function(elem) {
        angular.element(elem).trigger('click');
    }


    $scope.$watch('vm.fileUpload', function(file) {
        if (file && file.length === 1) {
            intervention.fileUpload(file[0], function(err, resp) {
                $scope.fileUploadText = "";
                $scope.loadFilesList();
            });
        }
    })

    $scope.loadFilesList = function() {
        edisonAPI.intervention.getFiles(intervention.id || intervention.tmpID).then(function(result) {
            intervention.files = result.data;
        })
    }

    $scope.loadFilesList();

    var postSave = function(options, resp, cb) {
        /*
	if (intervention.tmpID && $routeParams.i) {
	    for (var n = 0; n < intervention.files.length; n++)
	    {
		intervention.fileUpload(intervention.files[n], function(err, resp) {
		});
	    }
	}*/
        if (options && options.envoiFacture && options.verification) {
            intervention.envoiFactureVerif(cb)
        } else if (options && options.envoiFacture) {
            intervention.sendFacture(cb)
        } else if (options && options.envoi === true) {
            resp.files = intervention.files;
            intervention.envoi.bind(resp)(cb);
        } else if (options && options.annulation) {
            intervention.annulation(cb);
        } else if (options && options.verification) {
            intervention.verificationSimple(cb);
        } else if (_this.saveReglementSurPlace && _this.saveReglementSurPlace != _this.data.reglementSurPlace && options && options.verification) {
            intervention.envoiFactureVerif(cb);
        } else {
            cb(null)
        }
    }
    

    var saveInter = function(options) {
    	if ($rootScope.user.root !== true && $rootScope.user.service === 'INTERVENTION' && _this.data.date.modifs)
    	{
    	    var count = 0;
    	    for (var n = 0; n < _this.data.date.modifs.length; n++)
    	    {
        		if (_this.data.date.modifs[n].login === $rootScope.user.login &&
        		    _this.data.date.intervention.getTime() != new Date(interDate).getTime())
        		{
        		    if (count >= 1 || _this.data.status !== 'ENC')
        		    {
            			LxNotificationService.error("Vous ne pouvez plus modifier la date d'intervention");
            			return ;
        		    }
        		    count += 1;
        		}
    	    }
    	}
    	if (_this.data.date.intervention.getTime() != new Date(interDate).getTime() &&
    	    _this.data.date.modifs)
    	    _this.data.date.modifs.push({date: new Date(), login: $rootScope.user.login});
        if (_this.data.artisan)
        {
            edisonAPI.artisan.get(_this.data.artisan.id).then(function(artisan){
        		var pack = artisan.data.historique.pack
        		if (pack.length > 0)
        		{
        			var datefact = new Date(pack[pack.length - 1].date)
        		}
        		else
        		    var datefact = new Date(2016,8,1);
                    edisonAPI.intervention.CountFacture({id:_this.data.sst.id,date:datefact}).then(function(resp2){
            		    var inter = resp2.data
            		    inter = parseInt(inter)
            		    edisonAPI.artisan.CheckFacturier(_this.data.sst.id,{count: inter}).then(function(resp){
                			if (resp === "ok")
                			{
            			        LxNotificationService.success("Une demande de facturier a été fait pour M." + _this.data.sst.representant.nom)
			                }
        		        })
        		    })
        	})
        }
        intervention.save(function(err, resp) {
	       if (!err)
           {
                /*  var files = intervention.files
                  var tmp = intervention;
                  intervention = new Intervention(resp);
                  intervention.produits = tmp.produits;
                  intervention.fourniture = tmp.fourniture;
                  intervention.files = files*/
                postSave(options, resp, function(err) {
                    if (!err) {
                        TabContainer.close(tab);
                    }
                    $scope.saveInter = saveInter;
                })
            }
            else
            {
                $scope.saveInter = saveInter;
            }
        })
    }

    $scope.saveInter = saveInter;


    var latLng = function(add) {
        return add.lt + ', ' + add.lg
    }
    _this.selectArtisan = function(sst, first) {

        if (!sst) {
            intervention.sst = intervention.artisan = null
            return false;
        }
        $q.all([
            edisonAPI.artisan.get(sst.id, {
                cache: false
            }),
            edisonAPI.artisan.getStats(sst.id, {
                cache: true
            })
        ]).then(function(result) {
            intervention.sst = intervention.artisan = result[0].data;
            intervention.sst.stats = result[1].data
            if (!first) {
                intervention.compta.paiement.pourcentage = _.clone(intervention.sst.pourcentage);
            }
            edisonAPI.getDistance(latLng(sst.address), latLng(intervention.client.address))
                .then(function(dir) {
                    intervention.sst.stats.direction = dir.data;
                })
            _this.recapFltr = {
                ai: intervention.sst.id
            }
        });
    }

    _this.showInterList = function() {
        //$scope.interList = true;
    }

    _this.sstBase = intervention.sst;
    if (intervention.sst) {
        _this.selectArtisan(intervention.sst, true);
    }

    _this.searchArtisans = function(categorie) {
        if (_.get(intervention, 'client.address.lt')) {
            edisonAPI.artisan.getNearest(intervention.client.address, categorie || intervention.categorie)
                .success(function(result) {
                    _this.nearestArtisans = result;
        		    if (_this.data.categorie || intervention.categorie)
                        _this.loadFournitures()
                });
        }
    }
    //_this.searchArtisans();
    var init = true
    $scope.$watch(function() {
        return intervention.client.address;
    }, function() {
	if (init == true)
	{
	    init = false
	    return ;
	}
        _this.searchArtisans(intervention.categorie);
    })

    _this.createLitige = function(type) {
        edisonAPI.intervention.createLitige({ type: type, data: _this.data.id, user: app_session.login }).then(function(returnEnd) {
            if (returnEnd.data == "YES")
                LxNotificationService.success("Litige ajouté");
            else
                LxNotificationService.error("Une erreur est survenue");
        })
    }

    _this.InfoRecouv = function () {
	   _this.data.recouvrement.date = new Date();
	   _this.data.recouvrement.login = user.login;
    }
    $scope.$watch(function() {
        return intervention.client.civilite;
    }, function(curr, prev) {
        if (curr !== prev && curr === 'Soc.') {
            intervention.tva = 20;
            LxNotificationService.info("La TVA à été mise a 20%");
        }
    })

    var updateTmpIntervention = _.after(5, _.throttle(function() {
        edisonAPI.intervention.saveTmp(intervention);
    }, 30000))

    if (!intervention.id) {
        $scope.$watch(function() {
            return intervention;
        }, updateTmpIntervention, true)
    }

    _this.prospectModif = function () {
	$window.open('/prospect/' + $routeParams.id);
    }
    /*
    $scope.$watch("vm.test", function(){
	var check = _this.test.split(":")
	var check2 = new Date(_this.test2)
	check2.setHours(check[0])
	check2.setMinutes(check[1])
	var inter = new Date(check2)
	var inter2 = new Date(check2) 
	var inter17 = new Date(inter.setHours(17,0,0))
	var inter24 = new Date(inter.setHours(23,59,59))
	if (inter2.getDay() === 0)
	    _this.showDimanche = true
	else
	    _this.showDimanche = false
	if (inter2.getTime() > inter17.getTime() && inter2.getTime() < inter24.getTime())
	    _this.showSoir = true
	else
	    _this.showSoir = false
	if (inter2.getDay() === 6)
	    _this.showSamedi = true
	else
	    _this.showSamedi = false
    })
    $scope.$watch("vm.test2", function(){
	var check = _this.test.split(":")
	var check2 = new Date(_this.test2)
	check2.setHours(check[0])
	check2.setMinutes(check[1])
	var inter = new Date(check2)
	var inter2 = new Date(check2)
	var inter17 = new Date(inter.setHours(17,0,0))
	var inter24 = new Date(inter.setHours(23,59,59))
	if (inter2.getDay() === 0)
	    _this.showDimanche = true
	else
	    _this.showDimanche = false
	if (inter2.getTime() > inter17.getTime() && inter2.getTime() < inter24.getTime())
	    _this.showSoir = true
	else
	    _this.showSoir = false
	if (inter2.getDay() === 6)
	    _this.showSamedi = true
	else
	    _this.showSamedi = false
    })
    var inter = new Date(_this.data.date.intervention)
    var inter2 = new Date(_this.data.date.intervention)
    var inter17 = new Date(inter.setHours(17,0,0))
    var inter24 = new Date(inter.setHours(23,59,59))
    if (inter2.getDay() === 6)
	_this.showSamedi = true
    else
	_this.showSamedi = false
    if (inter2.getDay() === 0)
	_this.showDimanche = true
    else
	_this.showDimanche = false
    if (inter2.getTime() > inter17.getTime() && inter2.getTime() < inter24.getTime())
	_this.showSoir = true
    else
	_this.showSoir = false
    */
    var checkResize = function() {
	_this.phoneWin = window.innerWidth < 660
    }
    $(window).resize(checkResize);
    _this.phoneWin = window.innerWidth < 660

    _this.addDemarchage = function ()
    {
        var addr = _this.description.inter.client.address.cp + ' ' + _this.description.inter.client.address.v;
        var dat = new Date();
        var day = dat.getDate().toString();
        var m = (dat.getMonth() + 1).toString();
        var min = (dat.getMinutes()).toString();
        var fulldate = dat.getHours() + "h" +
            (min[1] ? min : "0" + min[0]) + " " + (day[1] ? day : "0" + day[0]) + "/" +
            (m[1] ? m : "0" + m[0]) + "/" + dat.getFullYear();
        var obj = {
            date: fulldate,
            login: $rootScope.user.login,
            cate: config.categories[_this.description.inter.categorie].order,
            ad: addr,
            status: false,
        };

	_this.autocomplete.getPlaceAddress({description: obj.ad}).then(function (addrResp) {
	    obj.address = {
		lt: addrResp.lt,
		lg: addrResp.lg
	    }
	    
	    edisonAPI.demarchage.add(obj).then(function (resp) {
		if (resp.data != "not")
		    var message = _.template("Une nouvelle adresse a été ajoutée à la liste de démarchage.")(resp.data)
		else
		    var message = "L'addresse n'a pas été ajouté"
		LxNotificationService.success(message);
            }, function (error) {
		
		LxNotificationService.error(error.data);
            });
	})
	
    }

    _this.miseEnDe = function() {
	edisonAPI.intervention.litigeInjonc(_this.data.id).then(function(resp){
	})
	
    }
    
    _this.printQ = function (check) {
	var check2 = {params: check}
	if (check === "desispers")
	{
	    check2 = {
		params: check,
		montant: _this.mtnCheque,
		numero: _this.numCheque,
		emetteur: _this.emeCheque,
		date: _this.datCheque,
		nom: _this.nomBanque
	    }
	}
	edisonAPI.intervention.printQ(_this.data.id, check2).then(function(resp){
	    LxNotificationService.success("Envoyé au Dropbox");    
	})
    }
    _this.printQI = function (check) {
	
	if (_this.oppo)
	    var check2 = {params: "opposition"}
	if (_this.sspro)
	    var check2 = {params: "ssprovision"}
	edisonAPI.intervention.printQ(_this.data.id, check2).then(function(resp){
	    LxNotificationService.success("Envoyé au Dropbox");
	})
    }
    var arr_sort = function (o1, o2) {
	   return new Date(o1.date) - new Date(o2.date)
    }

    edisonAPI.ovh.getNumber(_this.data.client.telephone, _this.data.login ? _this.data.login.ajout : user.login).then(function (res) {
	_this.numbers = res.data;
    })

    var interDate = _this.data.date.intervention;
    _this.desc2 = _this.data.description;
}

angular.module('edison').controller('InterventionController', InterventionCtrl);
