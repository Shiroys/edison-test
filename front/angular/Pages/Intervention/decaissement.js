angular.module('edison').directive('decaissement',
                   function(config, Paiement, Intervention, textTemplate, edisonAPI) {
        "use strict";
        return {
            restrict: 'E',
            templateUrl: '/Templates/decaissement.html',
            scope: {
                data: "=",
                displayDecaissement: '@',
                dialog: '@',
                simulator: '@'
            },
            link: function(scope, element, attrs) {
                console.log("decaissement: ", scope, element, attrs);
                if (scope.displayDecaissement) {
                    scope.showDecaissement = true;
                }
                scope.changeTva = function(index) {
                    $scope.data.compta.decaissement[index].tva = scope.data.tva;
                }
            },
        }
    }
);