 angular.module('edison').directive('edisonMap', ['$window', 'Map', 'mapAutocomplete', 'Address',
						  function($window, Map, mapAutocomplete, Address, edisonAPI) {
         "use strict";
         return {
             replace: true,
             restrict: 'E',
             templateUrl: '/Templates/autocomplete-map.html',
             scope: {
                 data: "=",
                 client: "=",
                 height: "@",
                 xmarkers: "=",
                 markerClick: '&',
                 isNew: "=",
                 firstAddress: "=",
                 showAddress: "=",
		 searchRev:"@",
		 hidesearch:"@",
		 draw:"@",
		 circle:"@",
		 ispro:"@",
		 fourn:"=",
		 searchOnly:'@',
		 bigger:"@",
		 hidemap:"@",
             },
             link: function(scope, element, attrs) {
                 scope._height = scope.height || 315;
                 scope.map = new Map();
                 scope.map.setZoom(_.get(scope, 'client.address') ? 12 : 6)
                 if (scope.isNew) {
                     scope.map.show()
                 }
                 scope.autocomplete = mapAutocomplete;

                 scope.mapShow = function() {
                     scope.mapDisplay = true
                 }

                 if (_.get(scope, 'client.address.lt')) {
                     scope.client.address = Address(scope.client.address, true); //true -> copyContructor
                     scope.map.setCenter(scope.client.address);
                 } else {
                     scope.map.setCenter(Address({
                         lat: 46.3333,
                         lng: 2.6
                     }));
                     scope.map.setZoom(5)
                 }

                 scope.changeAddress = function(place) {
                     scope.firstAddress = true;
                     mapAutocomplete.getPlaceAddress(place).then(function(addr) {
			 if (scope.searchRev && scope.searchRev == "true")
			     scope.data.artisan.address = addr;
			 else
			 {
                             scope.map.setZoom(12);
                             scope.map.setCenter(addr)
			     if (!(scope.searchOnly && scope.searchOnly == "true"))
				 scope.client.address = addr;
			 }
                     });
                 }

		 var drawCircle = function(point, radius, dir)
		 {
		     var d2r = Math.PI / 180;
		     var r2d = 180 / Math.PI;
		     var earthsradius = 3963;
		     var points = 32;

		     var rlat = (radius / earthsradius) * r2d;
		     var rlng = rlat / Math.cos(point.lat() * d2r);

		     var extp = new Array();
		     if (dir == 1)
		     {
			 var start = 0;
			 var end = points + 1;
		     }
		     else
		     {
			 var start = points + 1;
			 var end = 0;
		     }
		     for (var i=start; (dir==1 ? i < end : i > end); i=i+dir)
		     {
			 var theta = Math.PI * (i / (points/2));
			 var ey = point.lng() + (rlng * Math.cos(theta));
			 var ex = point.lat() + (rlat * Math.sin(theta));
			 extp.push(new google.maps.LatLng(ex, ey));
		     }
		     return extp;
		 }

		 scope.info = new google.maps.InfoWindow({
		     content: "Hello!"
		 })

		 scope.showInfo = function (marker, fourn)
		 {
		     scope.info.setContent("<img src='/img/glass.jpg'/>" +
					   "<div style='text-align:left'><h1>" +
					   fourn.f.nom + "</h1>" +
					   "<p>" + fourn.f.address.r + "</p>" +
					   "<p>" + fourn.f.address.v + "</p>" +
					   "<p>" + fourn.f.address.cp + "</p>" +
					   (fourn.f.telephone ? "<p>Tel: " + fourn.f.telephone + "</p></div>" : "</div>"))
		     scope.info.open(scope.map, this);
		 }
		 
		 scope.initCircles = function (sst)
		 {
		     if (scope.circle == "true" && sst.address)
		     {
			 scope.map.clearPoly(sst.cate);
			 var circles = [drawCircle(new google.maps.LatLng(sst.address.lt,
									 sst.address.lg),
						   30 * 0.62137, 1)];
			 var joinCircle = new google.maps.Polygon({
			     paths: circles,
			     strokeColor: '#ff0000',
			     strokeOpacity: 0.20,
			     strokeWeight: 0,
			     fillColor: "#FF0000",
			     fillOpaity: 0.20,
			 });
			 scope.map.setPoly(joinCircle);
			 scope.isInit = true;
		     }
		 }

                 scope.getStaticMap = function() {
                     if (!_.get(scope, 'data.client.address.lt') && !_.get(scope, 'data.address.lt'))
                         return 0
                     var q = "?width=" + Math.round($window.outerWidth * (scope.height === "small" ? 0.8 : 1.2));
                     if (scope.client && scope.client.address && scope.client.address.latLng)
                         q += ("&origin=" + scope.client.address.latLng);
                     if (scope.data.artisan && scope.data.artisan.address)
                         q += ("&destination=" + scope.data.artisan.address.lt + "," + scope.data.artisan.address.lg);
                     return "/api/mapGetStatic" + q;
                 }
                 scope.showClientMarker = function() {
                     return scope.client.address && scope.client.address.latLng;
                 }
                 scope.clickOnArtisanMarker = function(event, sst) {
                    scope.markerClick({
                        sst:sst
                    })
                 }
             }
         }
     }
 ]);
