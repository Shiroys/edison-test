var editProducts = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, $filter, socket, dialog) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Produits');
    $scope.startLimit = 0


    var single = function(e) {
        e.single = (_(e.desc).deburr().toLowerCase() !== _(e.title).deburr().toLowerCase())
        return e;
    }

    editProducts.prototype.$scope = $scope;

    $scope.$watch('filteredData', function () {
		if ($scope.filteredData && $scope.filteredData.length == 0 && $scope.startLimit != 0)
		    $scope.startLimit -= 50;
    })

    _this.inc = function () {
		if ($scope.startLimit >= 0)
		    $scope.startLimit += 50;
    }

    _this.dec = function () {
		if ($scope.startLimit >= 0)
		    $scope.startLimit -= 50;
		if ($scope.startLimit < 0)
		    $scope.startLimit = 0;
    }

    _this.reset = function(reset) {
		$scope.startLimit = 0;
		if (reset)
		    $scope.objSearch.scategorie = undefined;
    }

    _this.categorieList = function () {
		$scope.cat = [];
		$scope.scat = {};
		for (var n = 0; n < $scope.pl.length; n++)
		{
		    if ($scope.cat.indexOf($scope.pl[n].categorie) === -1)
			$scope.cat.push($scope.pl[n].categorie);
		    if ($scope.cat.indexOf($scope.pl[n].categorie) > -1)
		    {
			if (!$scope.scat[$scope.pl[n].categorie])
			    $scope.scat[$scope.pl[n].categorie] = [];
			if ($scope.scat[$scope.pl[n].categorie].indexOf($scope.pl[n].scategorie) === -1)
			{
			    $scope.scat[$scope.pl[n].categorie].push($scope.pl[n].scategorie);
			    $scope.scat[$scope.pl[n].categorie].sort();
			}
		    }
		}
		$scope.cat.sort();
		$scope.cat.splice(0, 1);
		$scope.objSearch = {
		    categorie: $scope.cat[0],
		}
    }


    edisonAPI.product.list().then(function(resp) {
        $scope.pl = _.map(resp.data, single);
        $scope.pl = $filter('orderBy')($scope.pl, 'title');
		_this.categorieList();
    })

    _this.remove = function(obj) {
        $scope.pl.splice(_.findIndex($scope.pl, '_id', obj._id), 1);
    }

    _this.save = function() {
        edisonAPI.product.save($scope.pl).then(function(resp) {
            $scope.pl = _.map(resp.data, single);
            LxNotificationService.success("Les produits on été mis a jour");
        }, function(err) {
            LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
        })
    }

    var csvToJSON = function (file) {
		var lines = file.split("\n");
		var result = [];
		var header = lines[0].split(";");
		header.pop();

		for (var n = 1; n < lines.length; n++)
		{
		    var obj = {};
		    var current = lines[n].split(";");

		    for (var l = 0; l < header.length; l++)
			obj[header[l]] = current[l];
		    result.push(obj);
		}

		return (result);
    }

    _this.remCat = function () {
		for (var n = 0; n < $scope.pl.length; n++)
		{
		    if ($scope.pl[n].categorie === $scope.objSearch.categorie)
			$scope.pl.splice(n--, 1);
		}
		_this.categorieList();	
    }

    var toObject = function (csv) {
		var arr = [];
		var obj;
		for (var n = 0; n < csv.length; n++)
		{
		    if (csv[n].DESIGNATION)
		    {
			arr.push({
			    categorie: csv[n].CATEGORIE,
			    scategorie: csv[n].SCATEGORIE,
			    title: csv[n].DESIGNATION,
			    desc: csv[n].DESIGNATION + " " + csv[n].MARQUE + (csv[n].DESCRIPTION != '' ? "\n" + csv[n].DESCRIPTION.replace(/#/g, '\n') : ''),
			    ref: csv[n].REF,
			    pa: csv[n].PA.replace(',', '.'),
			    pu: csv[n].PU.replace(',', '.'),
			    marge: parseInt(csv[n].PU.replace(',', '.')) - parseInt(csv[n].PA.replace(',', '.')),
			    link: csv[n].LINK,
			    single: true,
			});
		    }
		}
		return arr;
    }

    _this.editImage = function (elem) {
		dialog.changeImage(elem.link, function (resp) {
		    elem.link = resp;
		})
    }

    _this.importProducts = function() {
		var read = new FileReader();
		read.onload = function(e) {
		    var elems = toObject(csvToJSON(e.target.result));
		    $scope.pl = $scope.pl.concat(elems);
		    _this.categorieList();
		}
		read.readAsText($scope.file, 'ISO-8859-1');
    }

    /* $scope.$watch('pl', function(curr, prev) {
         if (curr && prev && !_.isEqual(prev, curr)) {
             save()
         }
     }, true)*/
     _this.newProd = function()
    {
    	$scope.pl.unshift({ single: false, open: true, categorie: $scope.objSearch.categorie});
    }
}

editProducts.prototype.changeFile = function (elem) {
    var $scope = this.$scope;
    $scope.$apply(function () {
	$scope.file = elem.files[0];
    });
};


angular.module('edison').controller('editProducts', editProducts);
