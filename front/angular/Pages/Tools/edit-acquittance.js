var editAcquittance = function(config,  MomentIterator, TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, socket) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.log = {}

    _this.weekDays = function () {
	return (['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'])
    }

    _this.getUserList = function () {
	edisonAPI.users.list().then(function (resp) {
	    $scope.users = resp.data
	    _this.userList = resp.data
	    var d = new Date();
	    var tab = [new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0)),new Date(d.setHours(9,0,0,0))]
	    var tab2 = [new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0)),new Date(d.setHours(17,0,0,0))]
	    for (var n = 0; n < _this.userList.length; n++)
	    {
		if (_this.userList[n].activated === true && _this.userList[n].hours.length === 0)
		{
		    _this.userList[n].hours = tab
		}
		else if (_this.userList[n].activated === true && _this.userList[n].hoursf.length === 0)
		{
		    _this.userList[n].hoursf = tab2
		}
		else
		{
		    for (var l = 0; l < _this.userList[n].hours.length; l++)
		    {
			_this.userList[n].hours[l] = new Date(_this.userList[n].hours[l]);
			_this.userList[n].hoursf[l] = new Date(_this.userList[n].hoursf[l]);
		    }
		}
	    }
	})
    }
    
    _this.save = function() {
	edisonAPI.users.save(_this.userList).then(function() {
	    LxNotificationService.success("Les utilisateurs on été mis a jour");
	}, function(err) {
	    LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
	})
    }
    
    _this.getUserList();
}
angular.module('edison').controller('editAcquittance', editAcquittance);
