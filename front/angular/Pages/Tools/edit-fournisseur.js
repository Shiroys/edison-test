var editFournisseur = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, $filter, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Fournisseurs');

    edisonAPI.fournisseur.list().then(function(resp) {
        $scope.pl = resp.data
        $scope.pl = $filter('orderBy')($scope.pl, 'nom')
    })

    _this.save = function() {
        edisonAPI.fournisseur.saveFournisseur($scope.pl).then(function(resp) {
            LxNotificationService.success("Les fournisseurs on été mis a jour");
        }, function(err) {
            LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
        })
    }
    _this.remove = function(obj) {
        $scope.pl.splice(_.findIndex($scope.pl, '_id', obj._id), 1);
    }
}

angular.module('edison').controller('editFournisseur', editFournisseur);