var editRaison = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, $filter, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Raisons');

    edisonAPI.raison.list().then(function(resp) {
        $scope.pl = resp.data
        $scope.pl = $filter('orderBy')($scope.pl, 'raison')
    })

    _this.save = function() {
        edisonAPI.raison.saveRaison($scope.pl).then(function(resp) {
            LxNotificationService.success("Les Raisons ont été mise à jour");
        }, function(err) {
            LxNotificationService.error("Une erreur est survenu (" + JSON.stringify(err.data) + ')');
        })
    }
    _this.remove = function(obj) {
        $scope.pl.splice(_.findIndex($scope.pl, '_id', obj._id), 1);
    }
}

angular.module('edison').controller('editRaison', editRaison);