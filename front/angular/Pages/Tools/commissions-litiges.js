var ComLitiges = function(MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope, $location, LxProgressService, socket, config) {
    "use strict";

    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Coms.');

    _this.xcalc = function(e) {
        return _.round((e.compta.reglement.montant || e.compta.paiement.base || e.prixFinal) * 0.01, 2);
    }

    _this.getTotal = function() {
        var rtn = {
            com: 0,
            all: 0
        }
        _.each($scope.list, function(x) {
            rtn.com += _this.xcalc(x);
            rtn.all += x.compta.reglement.montant || 0
        })
        return rtn;
    }
    var end = new Date();
    var start = new Date(2013, 8, 1)
    _this.dateSelect = MomentIterator(start, end).range('month').map(function(e) {
        return {
            t:e.format('MMM YYYY'),
            m:e.month() + 1,
            y:e.year(),
        }
    }).reverse()

    var dateTarget = _.pick(_this.dateSelect[0], 'm', 'y');

    var actualise = _.debounce(function() {
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        edisonAPI.intervention.commissions(_.merge($scope.selectedDate)).then(function(resp) {
            LxProgressService.circular.hide();
            $scope.list = resp.data
	    console.log($scope.list);
            $scope.total = _this.getTotal()
        })
    }, 50)
    $scope.$watch("selectedDate", function(curr, prev) {
        $location.search('m', curr.m);
        $location.search('y', curr.y);
        actualise();
    })
    if ($location.search().m)  {
        dateTarget.m = parseInt($location.search().m)
    }
    if ($location.search().y)  {
        dateTarget.y = parseInt($location.search().y)
    }
    $scope.selectedDate = _.find(_this.dateSelect, dateTarget)
}
angular.module('edison').controller('ComLitiges', ComLitiges);
