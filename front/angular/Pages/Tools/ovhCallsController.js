var ovhCallsController = function(config, edisonAPI, MomentIterator, $scope, dialog, NgTableParams, LxProgressService, $window) {
    var _this = this;
    $scope.startLimit = 0

    _this.numbersData = {}

    var arr_sort = function (o1, o2) {
	return new Date(o1.date) - new Date(o2.date)
    }

    var loadData = function (array, n)
    {
	if (n < array.length && !_this.numbersData[$scope.filteredData[n]])
	{
	    edisonAPI.ovh.getSingle({tel: $scope.filteredData[n]}).then(function(resp) {
		resp.data.convOvh.sort(arr_sort);
		_this.numbersData[$scope.filteredData[n]] = resp.data
		loadData(array, n + 1);
	    })
	}
    }

    $scope.$watch('filteredData', function () {
	if ($scope.filteredData && $scope.filteredData.length == 0 && $scope.startLimit != 0)
	    $scope.startLimit -= 10;
	if ($scope.filteredData)
	    loadData($scope.filteredData, 0);
    })

    _this.inc = function () {
	$scope.startLimit += 10;
    }

    _this.dec = function () {
	if ($scope.startLimit != 0)
	    $scope.startLimit -= 10;
    }

    edisonAPI.ovh.getAll().then(function (resp) {
	_this.numberList = []
	Object.keys(resp.data).forEach(function(key, index) {
	    _this.numberList.push(resp.data[key].number)
	})
    })
}

angular.module('edison').controller('ovhCallsController', ovhCallsController);
