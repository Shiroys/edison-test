var chiffreGrandComptes = function(config, edisonAPI, MomentIterator, $scope) {
    "use strict";
    var _this = this;
    _this.config = config;

    var start = new Date("01/01/2013");
    var end = new Date();
    _this.yearSelect = MomentIterator(start, end).range('year', {
	format: 'YYYY',
    }).map(function(e) {
	return parseInt(e)
    })

    $scope.selectedYear = _this.yearSelect[_this.yearSelect.length - 1];
    
    _this.loadData = function () {
	var obj = {};
	obj.year = $scope.selectedYear;
	edisonAPI.compte.list().then(function (compte) {
	    edisonAPI.compta.getCA(obj).then(function (resp) {
		_this.years = resp.data
		_this.info = [];
		for (var n in resp.data)
		{
		    var total = 0;
		    for (var l = 0; l < resp.data[n].length; l++)
			total += resp.data[n][l]
		    var ref = n;
		    for (var l = 0; l < compte.data.length; l++)
		    {
			if (compte.data[l].ref.toUpperCase() === ref)
			    ref = compte.data[l].nom
		    }
		    _this.info.push({name: ref, total: total, ref: n});
		}
	    })
	})
    }
    
    _this.xsort = function(sb) {
	_this.sb = sb
    }

    $scope.$watch("selectedYear", function() {
	_this.loadData();
    });

}

angular.module('edison').filter("toArray", function (){
    return function (obj) {
	var res = [];
	angular.forEach(obj, function (val, key){
	    res.push(val);
	})
	return res;
    }
}).controller('chiffreGrandComptes', chiffreGrandComptes);
