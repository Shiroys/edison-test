var ovhController = function(config, edisonAPI, MomentIterator, $scope, dialog, NgTableParams, LxProgressService, $window) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.log = {}
    _this.late = {}
    _this.total = {}
    _this.excused = {}

    var sortNb = function (a, b) {
	return (a - b);
    }

    var filter = function (array, taken) {
	var ret = {
	    anM: 0,
	    anR: 0,
	    rec: 0,
	    recU: 0,
	    man: 0,
	    manU: 0,
	    manH: 0,
	    manUH: 0
	}
	var uniqueRec = [];
	var uniqueMan = [];
	for (var n = 0; n < array.length; n++)
	{
	    if (array[n].duration > 0)
	    {
		ret.rec += 1;
		if (array[n].an === true)
		    ret.anR += 1;
		if (uniqueRec.indexOf(array[n].number) === -1)
		{
		    ret.recU += 1
		    uniqueRec.push(array[n].number)
		}
	    }
	}
	for (var n = 0; n < array.length; n++)
	{
	    var date = new Date(array[n].date);
	    if (array[n].duration === 0 && taken.indexOf(array[n].number) === -1)
	    {
		if (date.getHours() >= 7 && date.getHours() <= 18)
		    ret.man += 1;
		else
		    ret.manH += 1;
		if (array[n].an === true)
		    ret.anM += 1;
		if (uniqueMan.indexOf(array[n].number) === -1 && uniqueRec.indexOf(array[n].number) === -1)
		{
		    if (date.getHours() >= 7 && date.getHours() <= 18)
			ret.manU += 1
		    else
			ret.manUH += 1
		    uniqueMan.push(array[n].number)
		}
	    }
	}
	return (ret);
    }

    var filterMissed = function(object) {
	var taken = []
	Object.keys(object).forEach(function(key, index) {
	    for (var n = 0; n < object[key].length; n++)
	    {
		if (object[key][n].duration > 0)
		    taken.push(object[key][n].number)
	    }
	})
	return (taken);
    }

    _this.updateStats = function () {
	var table = []
	var taken = filterMissed(_this.lines.logs);
	Object.keys(_this.lines.logs).forEach(function(key, index) {
	    var obj = filter(_this.lines.logs[key], taken)
	    if (_this.interLink[key])
	    {
		_this.interLink[key].total.sort(sortNb);
		_this.interLink[key].val.sort(sortNb);
		_this.interLink[key].ann.sort(sortNb);
		_this.interLink[key].prog.sort(sortNb);
	    }
	    if (_this.devisLink[key])
	    {
		_this.devisLink[key].total.sort(sortNb);
		_this.devisLink[key].env.sort(sortNb);
		_this.devisLink[key].tran.sort(sortNb);
	    }
	    table.push({
		name: key,
		rec: obj.rec,
		recU: obj.recU,
		man: obj.man,
		manU: obj.manU,
		anR: obj.anR,
		anM: obj.anM,
		manH: obj.manH,
		manUH: obj.manUH,
		at: _this.linesFinal[key] ? _this.linesFinal[key][0] : 0,
		atN: _this.linesFinal[key] ? _this.linesFinal[key][1].toFixed(2) : 0.00,
		atL: _this.interLink[key] ? _this.interLink[key].total : [],
		osv: _this.linesFinal[key] ? _this.linesFinal[key][2] : 0,
		osvN: _this.linesFinal[key] ? _this.linesFinal[key][3].toFixed(2) : 0.00,
		osvL: _this.interLink[key] ? _this.interLink[key].val : [],
		osa: _this.linesFinal[key] ? _this.linesFinal[key][4] : 0,
		osaN: _this.linesFinal[key] ? _this.linesFinal[key][5].toFixed(2) : 0.00,
		osaL: _this.interLink[key] ? _this.interLink[key].ann : [],
		osp: _this.linesFinal[key] ? _this.linesFinal[key][6] : 0,
		ospN: _this.linesFinal[key] ? _this.linesFinal[key][7].toFixed(2) : 0.00,
		ospL: _this.interLink[key] ? _this.interLink[key].prog : [],
		dev: _this.linesFinal[key] ? _this.linesFinal[key][8] : 0,
		devN: _this.linesFinal[key] ? _this.linesFinal[key][9].toFixed(2) : 0.00,
		devL: _this.interLink[key] ? _this.devisLink[key].total : [],
		devE: _this.linesFinal[key] ? _this.linesFinal[key][10] : 0,
		devEN: _this.linesFinal[key] ? _this.linesFinal[key][11].toFixed(2) : 0.00,
		devEL: _this.interLink[key] ? _this.devisLink[key].env : [],
		devT: _this.linesFinal[key] ? _this.linesFinal[key][12] : 0,
		devTN: _this.linesFinal[key] ? _this.linesFinal[key][13].toFixed(2) : 0.00,
		devTL: _this.interLink[key] ? _this.devisLink[key].tran : [],
		total: _this.linesFinal[key] ? (_this.linesFinal[key][1] + _this.linesFinal[key][9]).toFixed(2) : 0.00
	    })
	})
	_this.prStats = new NgTableParams({
	    count: table.length,
	    sorting: {
		rec: 'desc'
	    }
	}, {
	    counts: [],
	    data: table
	});
	/*var table2 = []
	Object.keys(_this.lines2.logs).forEach(function(key, index) {
	    var obj = filter(_this.lines2.logs[key], taken)	
	    if (_this.interLink[key])
	    {
		_this.interLink[key].total.sort(sortNb);
		_this.interLink[key].val.sort(sortNb);
		_this.interLink[key].ann.sort(sortNb);
		_this.interLink[key].prog.sort(sortNb);
	    }
	    if (_this.devisLink[key])
	    {
		_this.devisLink[key].total.sort(sortNb);
		_this.devisLink[key].env.sort(sortNb);
		_this.devisLink[key].tran.sort(sortNb);
	    }    
	    table2.push({
		name: key,
		number: _this.numbers.number[key] ? _this.numbers.number[key].replace('0033', '0') : '',
		rec: obj.rec,
		recU: obj.recU,
		man: obj.man,
		manU: obj.manU,
		anR: obj.anR,
		anM: obj.anM,
		manH: obj.manH,
		manUH: obj.manUH,
		at: _this.linesFinal[key] ? _this.linesFinal[key][0] : 0,
		atN: _this.linesFinal[key] ? _this.linesFinal[key][1].toFixed(2) : 0,
		atL: _this.interLink[key] ? _this.interLink[key].total : [],
		osv: _this.linesFinal[key] ? _this.linesFinal[key][2] : 0,
		osvN: _this.linesFinal[key] ? _this.linesFinal[key][3].toFixed(2) : 0.00,
		osvL: _this.interLink[key] ? _this.interLink[key].val : [],
		osa: _this.linesFinal[key] ? _this.linesFinal[key][4] : 0,
		osaN: _this.linesFinal[key] ? _this.linesFinal[key][5].toFixed(2) : 0.00,
		osaL: _this.interLink[key] ? _this.interLink[key].ann : [],
		osp: _this.linesFinal[key] ? _this.linesFinal[key][6] : 0,
		ospN: _this.linesFinal[key] ? _this.linesFinal[key][7].toFixed(2) : 0.00,
		ospL: _this.interLink[key] ? _this.interLink[key].prog : [],
		dev: _this.linesFinal[key] ? _this.linesFinal[key][8] : 0,
		devN: _this.linesFinal[key] ? _this.linesFinal[key][9].toFixed(2) : 0.00,
		devL: _this.interLink[key] ? _this.devisLink[key].total : [],
		devE: _this.linesFinal[key] ? _this.linesFinal[key][10] : 0,
		devEN: _this.linesFinal[key] ? _this.linesFinal[key][11].toFixed(2) : 0.00,
		devEL: _this.interLink[key] ? _this.devisLink[key].env : [],
		devT: _this.linesFinal[key] ? _this.linesFinal[key][12] : 0,
		devTN: _this.linesFinal[key] ? _this.linesFinal[key][13].toFixed(2) : 0.00,
		devTL: _this.interLink[key] ? _this.devisLink[key].tran : [],
		total: _this.linesFinal[key] ? (_this.linesFinal[key][1] + _this.linesFinal[key][9]).toFixed(2) : 0.00
	    })
	})
	_this.prStats2 = new NgTableParams({
	    count: table2.length,
	    sorting: {
		rec: 'desc'
	    }
	}, {
	    counts: [],
	    data: table2
	});*/
    }


    var start = new Date("08/01/2016");
    start.setHours(0,0,0,0);
    var end = new Date();
    end.setHours(1,0,0,0);
    _this.dateSelect = MomentIterator(start, end).range('weeks', {
	format: 'DD-MM-YYYY',
    }).map(function(e) {
	return e
    })

    _this.dateSelect2 = MomentIterator(start, end).range('months', {
	format: 'DD-MM-YYYY',
    }).map(function(e) {
	return e
    })    

    $scope.selectedWeek = _this.dateSelect[_this.dateSelect.length - 1];
    $scope.selectedMonth = _this.dateSelect2[_this.dateSelect2.length - 1];

    _this.changeLink = function (mode, nb) {
	$window.open('/' + mode + '/' + nb);
    }

    _this.ovhChargeMultiple = function(type, selected) {
	_this.loadedAcc = _this.account;
	_this.loadedNum = _this.number;
	LxProgressService.circular.show('#5fa2db', '#globalProgress');
	var q = {
	    t: type,
	    d: selected,
	    a: _this.account,
	    n: _this.logins[_this.number.replace("0033", "0")] ? _this.logins[_this.number.replace("0033", "0")] : '',
	}
	edisonAPI.ovh.getComptes(q).then(function (resp) {
	    _this.lines = resp.data;
	    q.n = _this.numberInfo[_this.number]
	    edisonAPI.ovh.demStats(q).then(function (resp2) {
		_this.numbers = resp2.data;
		Object.keys(_this.numbers.logs).forEach(function (key, index) {
		    Object.keys(_this.numbers.logs[key]).forEach(function (key2, index2) {
			_this.lines.logs[key2] = []
			for (var n = 0; n < _this.numbers.logs[key][key2].length; n++)
			    _this.lines.logs[key2].push(_this.numbers.logs[key][key2][n])
		    })
		})
		q.l = _this.logins[_this.number.replace("0033", "0")] ? _this.logins[_this.number.replace("0033", "0")] : '';
		edisonAPI.ovh.getInterStatus(q).then(function (resp3) {
		    _this.linesFinal = resp3.data.count;
		    _this.interLink = resp3.data.inter;
		    _this.devisLink = resp3.data.devis;
		    _this.updateStats();
		    LxProgressService.circular.hide();
		})
	    })
	})

    }

    _this.ovhCharge = function () {
	_this.loadedAcc = _this.account;
	_this.loadedNum = _this.number;
	LxProgressService.circular.show('#5fa2db', '#globalProgress');
	var check = new Date(_this.date2);
	var check2 = new Date();
	check2.setHours(0, 0, 0, 0);
	if (check.getTime() > check2.getTime())
	{
	    return ;
	}
	var q = {
	    t: 'single',
	    d: _this.date2,
	    a: _this.account,
	    n: _this.logins[_this.number.replace("0033", "0")] ? _this.logins[_this.number.replace("0033", "0")] : '',
	}
	edisonAPI.ovh.getComptes(q).then(function (resp) {
	    _this.lines = resp.data;
	    q.n = _this.numberInfo[_this.number]
	    edisonAPI.ovh.demStats(q).then(function (resp2) {
		_this.numbers = resp2.data;
		Object.keys(_this.numbers.logs).forEach(function (key, index) {
		    Object.keys(_this.numbers.logs[key]).forEach(function (key2, index2) {
			_this.lines.logs[key2] = []
			for (var n = 0; n < _this.numbers.logs[key][key2].length; n++)
			    _this.lines.logs[key2].push(_this.numbers.logs[key][key2][n]);
		    })
		})
		q.l = _this.logins[_this.number.replace("0033", "0")] ? _this.logins[_this.number.replace("0033", "0")] : '';
		edisonAPI.ovh.getInterStatus(q).then(function (resp3) {
		    _this.linesFinal = resp3.data.count;
		    _this.interLink = resp3.data.inter;
		    _this.devisLink = resp3.data.devis;
		    _this.updateStats();
		    LxProgressService.circular.hide();
		})
	    })
	})
    }

    _this.keys = function(obj) {
	if (!obj)
	    return []
	var arr = Object.keys(obj);
	for (var n = 0; n < arr.length; n++)
	{
	    for (var l = n + 1; l < arr.length; l++)
	    {
		if (obj[arr[n]].length < obj[arr[l]].length)
		{
		    var tmp = arr[n];
		    arr[n] = arr[l]
		    arr[l] = tmp;
		}
	    }
	}
	return arr;
    }

    _this.account = "";
    _this.number = "";
    _this.loadedAcc = "";
    _this.loadedNum = "";

    var naturalCompare = function (a, b) {
	var ax = [], bx = [];

	a.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
	b.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });

	while(ax.length && bx.length) {
	    var an = ax.shift();
	    var bn = bx.shift();
	    var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
	    if(nn) return nn;
	}

	return ax.length - bx.length;
    }

    _this.resetNumber = function () {
	_this.number = "";
    }

    _this.search = function (str) {
	var results = str ? _this.fullInfo.filter(function (elem) {
	    return (elem.toLowerCase().indexOf(str.toLowerCase()) >= 0);
	}) : _this.fullInfo;
	return results;
    }

    _this.onSelect = function(num) {
	var index = -1
	if (num)
	{
	    for (var n = 0; n < _this.numberFullList.length && index == -1; n++)
	    {
		if (num.indexOf(_this.numberFullList[n]) >= 0)
		    index = n;
	    }
	    if (index != -1)
	    {
		_this.number = _this.numberFullList[index];
		_this.account = _.findKey(_this.numberList, function(o) {
		    return o.indexOf(_this.numberFullList[index]) >= 0
		})
	    }
	}
    }

    _this.counter = 0;
    _this.loading = true;

    LxProgressService.circular.show('#5fa2db', '#timedProgress');
    edisonAPI.ovh.getAccounts().then(function (resp) {
	edisonAPI.ovh.getPhone().then(function (resp2) {
	    _this.logins = resp2.data;
	    _this.accountList = resp.data.accounts;
	    _this.numberList = resp.data.numbers;
	    _this.numberInfo = {};
	    _this.numberFullList = [].concat.apply([], Object.keys(resp.data.numbers).map(function (key) {
		return resp.data.numbers[key]
	    }))
	    _this.fullInfo = _this.numberFullList.slice();

	    var keys = Object.keys(_this.numberList);
	    var total = keys.length;
	    var val = keys.splice(0, 1)[0]
	    
	    var load = function (arr) {
		var obj = {
		    key: arr,
		    val: _this.numberList[arr],
		}
		edisonAPI.ovh.getInfo(obj).then(function (resp) {
		    _this.counter = Math.round(((total - keys.length) / total) * 100)
		    _this.numberInfo = Object.assign(_this.numberInfo, resp.data);
		    val = keys.splice(0, 1)[0];
		    if (val)
			load(val)
		    else
		    {
			for (var n = 0; n < _this.fullInfo.length; n++)
			{
			    _this.fullInfo[n] = _this.numberInfo[_this.fullInfo[n]] + " " + _this.fullInfo[n]
			}
			_this.loading = false;
			LxProgressService.circular.hide();
		    }
		})
	    }

	    var load2 = function () {
		edisonAPI.ovh.getBill({ val: _this.accountList}).then(function (resp) {
		    _this.accountName = resp.data;
		})
	    }

	    load2();
	    load(val);
	    _this.accountList.sort(naturalCompare);
	})
    })



/*    $scope.$watch("selectedWeek", function() {
	_this.reload();
    });*/

}

angular.module('edison').controller('ovhController', ovhController);
