var commissionsPartenariat = function(MomentIterator, TabContainer, $routeParams, edisonAPI, $rootScope, $scope,$location, LxProgressService, socket, openPost,  LxNotificationService) {
    "use strict";
  var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.dateSelectList = MomentIterator(new Date(2016, 1, 0),moment().add(1, 'months').toDate()).range('month').map(function(e) {
	return {
	    date: new Date(e),
	    name: moment(e).format("MM[/]YYYY")
	}
    })

    _this.xsort = function(sb) {
	_this.sb = sb
    }
    _this.onChanges = function () {
	if ($scope.choice)
	{
	var path = "/CommissionsPartenariat/" + $scope.choice;
	openPost('/api/document/see?name=' + path ,{
	    html: true
	})
	}
	else
	{
	    LxNotificationService.error("Veuillez mettre un fichier à telecharger");	    
	}
//	$location.path('/api/document/see?name=' + path)
    }
    
    _this.changeSelectedDate = function() {
	LxProgressService.circular.show('#5fa2db', '#globalProgress');
	edisonAPI.artisan.tableauCom(_this.selectedDate).then(function(resp) {
	    LxProgressService.circular.hide()
	    _this.data = resp.data
	    
	})
    }
    edisonAPI.artisan.filesCom().then(function(resp) {
	_this.files = resp.data
    })

    _this.validateArt = function () {
	edisonAPI.artisan.validateArt();
	LxNotificationService.success("Operation reussie");
    }
    
    _this.Csv = function (){
	var result = []
	var tab =  _this.data;
	var res = ["Société","Status","SubStatus","Nombre d'inters total","Total Paye","Intervention avec comission à payer","Comission","montant"];
	var prixtot = 0;
	var comtot = 0;
	result.push(res);
	for (var i = 0;tab.length > i ;i++)
	{
	    var inter = []
	    var nbr = Math.floor(tab[i].totalImpaye / 10)
	    if (nbr != 0)
	    {
		
		inter.push(tab[i].nomSociete)
		inter.push(tab[i].status)
		inter.push(tab[i].subStatus)
		inter.push(tab[i].nbrIntervention)
		var calc = Math.floor(tab[i].totalPaye)
		inter.push(Math.floor(tab[i].totalPaye / 10))
		inter.push(Math.floor(tab[i].nbrIntervention - calc))
		inter.push(nbr) 
		inter.push(nbr * 15)
		prixtot = prixtot + nbr * 15
		comtot = comtot + nbr
		result.push(inter)
	    }
	}
	result.push( [" "," "," "," "," "," "," "," "],["Total"," "," "," "," "," ",comtot, " "],["Montant"," "," "," "," " ," "," ",prixtot + "€"])
	edisonAPI.artisan.pdfcom(result).then(function(res){
	})
	LxNotificationService.success("Exportation Réussie");
    }
    _this.selectedDate = new Date(_this.dateSelectList[_this.dateSelectList.length - 1].date)
    _this.changeSelectedDate();
    _this.tab.setTitle('Coms.');
    //   edisonAPI.artisan.tableauCom().then(function(resp) {
    //    LxProgressService.circular.hide()
    //    _this.data = resp.data
    //  })
}
angular.module('edison').controller('commissionsPartenariat', commissionsPartenariat);
