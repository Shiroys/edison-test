var fournitureMonthly = function(config, edisonAPI, MomentIterator, $scope) {
    "use strict";
    var _this = this;
    _this.config = config;

    var start = new Date("01/01/2013");
    var end = new Date();
    _this.yearSelect = MomentIterator(start, end).range('year', {
	format: 'YYYY',
    }).map(function(e) {
	return parseInt(e)
    })

    $scope.selectedYear = _this.yearSelect[_this.yearSelect.length - 1];
    
    _this.loadData = function () {
	var obj = {};
	obj.year = $scope.selectedYear;
	edisonAPI.compta.getFourniture(obj).then(function (resp) {
	    _this.years = resp.data;
	})
    }
    _this.loadData();
    
    _this.xsort = function(sb) {
	_this.sb = sb
    }

    $scope.$watch("selectedYear", function() {
	_this.loadData();
    });

}

angular.module('edison').controller('fournitureMonthly', fournitureMonthly);
