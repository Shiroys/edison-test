var editUsers = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, $filter, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Utilisateurs');

    editUsers.prototype.$scope = $scope;

    edisonAPI.users.list().then(function(resp) {
		$scope.usrs = $filter('orderBy')(resp.data, 'login');
    })

    _this.save = function() {
    	$scope.usrs = $filter('orderBy')($scope.usrs, 'login');
    	edisonAPI.users.save($scope.usrs).then(function(usersSaved) {
    		LxNotificationService.success("Les utilisateurs ont été mis à jour");
    	}, function(err) {
    		LxNotificationService.error("Une erreur est survenue (" + JSON.stringify(err.data) + ')');
    	})
    }

    _this.remove = function(obj) {
        $scope.usrs.splice(_.findIndex($scope.usrs, '_id', obj._id), 1);
    }

    $scope.changeImage = function (obj, file) {
    	var reader = new FileReader();
    	/*reader.onload = function(e) {
    	    var bytes = new Uint8Array(e.target.result);
    	    var binary = ""
    	    for (var n = 0; n < bytes.byteLength; n++)
    		binary += String.fromCharCode(bytes[n]);
    	    edisonAPI.users.upload({login: obj, file: binary});
    	}
    	reader.readAsArrayBuffer($scope.file)*/
    	reader.onloadend = function(e) {
    	    var r = this.result;
    	    var hex = [];
    	    for (var n = 0; n < this.result.length; n++)
    	    {
        		var byteStr = r.charCodeAt(n);
        		hex.push(byteStr);
    	    }
    	    edisonAPI.users.upload({login: obj, file: hex});
    	}
    	reader.readAsBinaryString($scope.file);
        }
}

editUsers.prototype.changeFile = function (elem) {
    var $scope = this.$scope;
    $scope.$apply(function () {
	$scope.file = elem.files[0];
	$scope.changeImage(elem.name);
    });
};

angular.module('edison').controller('editUsers', editUsers);
