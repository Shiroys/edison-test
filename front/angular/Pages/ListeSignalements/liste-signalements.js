var listeSignalements = function(TabContainer, edisonAPI, MomentIterator, $rootScope, $filter, $scope, dialog, $location, LxNotificationService, socket) {
    "use strict";
    var _this = this;
    _this.tab = TabContainer.getCurrentTab();
    _this.tab.setTitle('Liste Signalements');
    //_this.activeTab = parseInt($location.search().level || 0)
	/* Initialisation des variables de base */
	var dateNow = new Date();
	_this.selectMonth = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet",
						"Août", "Septembre", "Octobre", "Novembre", "Décembre"];
	_this.selectService = ["RECOUVREMENT", "INTERVENTION", "COMPTABILITE", "PARTENARIAT"];
	_this.selectYear = MomentIterator(new Date(2016, 1 , 1), moment(new Date()).add(1, 'year')).range('year', { format: 'YYYY' }).map(function(resp) {
		return parseInt(resp);
	})
	$scope.dataUrl = $location.search();
	_this.selectedService = $scope.dataUrl.service;
	_this.selectedUser = $scope.dataUrl.user;
	_this.selectedMonth = _this.selectMonth[dateNow.getMonth()];
	_this.selectedYear = parseInt(dateNow.getFullYear());
	$scope.dataUrl.selectedUser = _this.selectedUser;
	$scope.dataUrl.selectedMonth = _this.selectMonth.indexOf(_this.selectedMonth);
	$scope.dataUrl.selectedYear = _this.selectedYear;
	$scope.dataUrl.selectedService = _this.selectedService;
	
	/* List tous les utilisateurs ainsi que les signalements de l'utilisateur loggé */
	edisonAPI.users.list().then(function(resp) {
		for (var ind = 0; ind < resp.data.length; ind++)
		{
			if (resp.data[ind].login == $scope.dataUrl.user)
			{
				if (resp.data[ind].root)
					$scope.dataUrl.root = resp.data[ind].root;
				if (resp.data[ind].admin)
					$scope.dataUrl.admin = resp.data[ind].admin;
				$scope.dataUrl.service = resp.data[ind].service;
			}
		}
		$scope.users = $filter('orderBy')(resp.data, 'login');
		$scope.users.unshift({ login: 'all', service: $scope.dataUrl.service });
    	edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
        	$scope.pl = resp.data;
    	})
	})

	/* Affichage pop up destinataires du signalement */
	_this.showDest = function(signal) {
		dialog.pop_up_showDest_raison(signal, 0, null)
	}

	/* Affichage pop up résolution signalement */
	_this.resolveSig = function(signal) {
		dialog.pop_up_showDest_raison(signal, 1, function(resp, cancel) {
			if (cancel === true && resp == "")
				LxNotificationService.error("Pas de raison donnée.");
			else if (cancel === true && resp != "")
			{
				edisonAPI.signalement.resolve({ _id: signal._id, text: resp, inter_id: signal.inter_id}).then(function(resp) {
					edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
        				$scope.pl = resp.data;
						LxNotificationService.success("Le signalement a été résolu.");	
    				})
				})	
			}
		})
	}

	/* Afficher la resolution */
	_this.showResolve = function(signal) {
		dialog.pop_up_showDest_raison(signal, 2, null)
	}

	/* Liste les signalements du nouvel utilisateur selectionner */
	_this.changeSelected = function() {
		$scope.dataUrl.selectedUser = _this.selectedUser;
		edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
        	$scope.pl = resp.data;
    	})
	}

	/* Menu change service on change */
	_this.changeService = function() {
		$scope.dataUrl.selectedService = _this.selectedService;
		edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
			$scope.pl = resp.data;
		})
	}

	/* Menu change month on change */
	_this.changeMonth = function() {
		$scope.dataUrl.selectedMonth = _this.selectMonth.indexOf(_this.selectedMonth);
		edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
			$scope.pl = resp.data;
		})
	}

	/* Menu change year on change */
	_this.changeYear = function() {
		$scope.dataUrl.selectedYear = _this.selectedYear;
		edisonAPI.signalement.list($scope.dataUrl).then(function(resp) {
			$scope.pl = resp.data;
		})
	}
}
angular.module('edison').controller('listeSignalements', listeSignalements);
