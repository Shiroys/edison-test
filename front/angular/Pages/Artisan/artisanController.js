var ArtisanCtrl = function(IBAN, $timeout, $rootScope, $scope, edisonAPI, $location, $routeParams, ContextMenu, LxProgressService, LxNotificationService, TabContainer, config, dialog, artisanPrm, Artisan, user) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.dialog = dialog;
    _this.moment = moment;
    _this.user = $rootScope.user;
    _this.contextMenu = new ContextMenu('artisan')
    var tab = TabContainer.getCurrentTab();
    if (!tab.data) {
        var artisan = new Artisan(artisanPrm.data)
        tab.setData(artisan);
        if ($routeParams.id.length > 12) {
            _this.isNew = true;
            artisan.tmpID = $routeParams.id;
            tab.setTitle('SST  ' + moment((new Date(parseInt(artisan.tmpID))).toISOString()).format("HH:mm").toString());
        } else {
            tab.setTitle('SST  ' + $routeParams.id);
            if (!artisan) {
                LxNotificationService.error("Impossible de trouver les informations !");
                $location.url("/dashboard");
                TabContainer.remove(tab);
                return 0;
            }
        }
    } else {
        var artisan = tab.data;
    }
    _this.data = tab.data;
    var star = tab.data.star;
    var one = tab.data.oneShot;
    var acp = tab.data.address.cp;
    var ar = tab.data.address.r;
    var av = tab.data.address.v;
    var tel1 = tab.data.telephone.tel1
    var tel2 = tab.data.telephone.tel2
    var mail = tab.data.email
    var nom = tab.data.representant.nom
    var IBAN = tab.data.IBAN
    var nomS = tab.data.nomSociete
    var an = tab.data.address.n;    
    var cat = [];
    var doc = []
    for(var index in tab.data.document) {
	doc.push(tab.data.document[index].ok);
    }
    for (var n = 0; n < _this.data.categories.length; n++)
	   cat.push(_this.data.categories[n]);
    var pm = tab.data.pourcentage.maindOeuvre;
    _this.saveArtisan = function(options) {
        // artisan = _this.data;
        // edisonAPI.artisan.checkExist({ email: artisan.email, tel: artisan.telephone.tel1 }).then(function(check) {
        //     console.log("------>", check);
        //     if (check.data == "NO")
        //     {
        //         console.log("IN NO");
        //         LxNotificationService.error("Erreur: L'email ou le téléphone existe déjà.");
        //     }
        //     else if (check.data == "YES")
        //     {
        	    artisan.save(function(err, resp) {
                    if (err)
                    {
                        return false
                    }
                    else if (options.contrat)
                    {
                        artisan = new Artisan(resp);
                        artisan.envoiContrat.bind(resp)(options, function(err, res) {
                            if (!err) {
                                TabContainer.close(tab);
                            }
                        });
                    }
                    else
                    {
                		if (nom != _this.data.representant.nom)
                		    edisonAPI.artisan.activite(_this.data._id, "Le nom de société a été changé de " + nom + " en " + _this.data.representant.nom)
                		if (IBAN != _this.data.IBAN)
                		{
                		    var messIBAN = "Le numero d' IBAN a changé de " + IBAN + " en " + _this.data.IBAN
                		    if (IBAN === undefined)
                			var messIBAN = "Ajout d'un numero d' IBAN: " + _this.data.IBAN;
                		    edisonAPI.artisan.activite(_this.data._id, messIBAN)
                		}
                		if (nomS != _this.data.nomSociete)
                		    edisonAPI.artisan.activite(_this.data._id, "Le nom de société a été changé de " + nomS + " en " + _this.data.nomSociete)
                		if (mail != _this.data.email)
                		    edisonAPI.artisan.activite(_this.data._id, "Le mail  a changé de " + mail + " en " + _this.data.email)
                		if (tel1 != _this.data.telephone.tel1)
                		    edisonAPI.artisan.activite(_this.data._id, "Le numero de téléphone n°1 a changé de " + tel1 + " en " + _this.data.telephone.tel1)
                		if (tel2 != _this.data.telephone.tel2)
                		{
                		    var messtel = "Le numero de téléphone n°2 a changé de " + tel2 + " en " + _this.data.telephone.tel2
                		    if (tel2 === undefined)
                			var messtel = "Ajout d'un numero de telephone n°2: " + _this.data.telephone.tel2;
                		    edisonAPI.artisan.activite(_this.data._id, messtel)
                		}
                		if (pm != _this.data.pourcentage.maindOeuvre)
                		{
                		    var sent = "Le pourcentage de main d'oeuvre a été changé de " + pm + " à " + _this.data.pourcentage.maindOeuvre
                		    edisonAPI.artisan.activite(_this.data._id, sent)
                		}
                		if (one != _this.data.oneShot)
                		{
                		    if (_this.data.oneShot === false)
                			var mess = "L' artisan n' a plus le statut OneShot"
                		    else
                			var mess = "L'artisan a le statut OneShot"
                		 
                		    edisonAPI.artisan.activite(_this.data._id, mess)
                		}
                		if (star != _this.data.star)
                		{
                		 
                		    if (_this.data.star === false)
                        		var mes = "L'artisan n' a plus le statut Star";
                        	else
                        		var mes = "L'artisan a le statut Star";
                		    edisonAPI.artisan.activite(_this.data._id, mes)
                		}
                		if (acp != _this.data.address.cp || ar != _this.data.address.r || av != _this.data.address.v || an != _this.data.address.n)
                		{
                		    var sent = "L' adresse a été changé de \n" + an + " " + ar + " " + av + " " + acp + " en \n" +  _this.data.address.n + " " + _this.data.address.r + " " + _this.data.address.v + " " + _this.data.address.cp; 
                		    edisonAPI.artisan.activite(_this.data._id, sent)
                		}
                		var doc2 = []
                        var check = []
                        if (_this.data.document)
                        {
                            var documents = Object.keys(_this.data.document)
                            for(var index in _this.data.document) {
                                doc2.push(tab.data.document[index].ok);
                            }
                            for (var d = 0;doc.length > d; d++)
                            {
                                if (doc[d] != doc2[d])
                                {
                                    if (doc2[d])
                                        edisonAPI.artisan.activite(_this.data._id, documents[d] + " a été mis")
                                    else
                                        edisonAPI.artisan.activite(_this.data._id, documents[d] + " a été enlevé")
                                }
                            }
                        }
                		for (var t = 0;cat.length > t;t++)
                		{
                		    if(_this.data.categories.indexOf(cat[t]) === -1)
                		    {
                			 edisonAPI.artisan.activite(_this.data._id,_this.config.categories[cat[t]].long_name  + " a été enlevé")
                		    }
                		    else
                		    {
                			 check.push(cat[t])
                		    }
                		}
                		
                		for (var y = 0; _this.data.categories.length > y;y++)
                		{
                		    if (check.indexOf(_this.data.categories[y]) === -1)
                		    {
                			edisonAPI.artisan.activite(_this.data._id,_this.config.categories[_this.data.categories[y]].long_name  + " a été ajouté")
                		    }
                		}
                        TabContainer.close(tab);
                    }
                })
        //     }
        // })
    }

    _this.checkConflict = function(check) {
	if (check === 'one' && _this.data.star === true)
	    _this.data.star = false;
	if (check === 'star' && _this.data.oneShot === true)
	    _this.data.oneShot = false;
    }
    
    _this.onArtisanFileUpload = function(file, name) {
        LxProgressService.circular.show('#5fa2db', '#fileUploadProgress');
        edisonAPI.artisan.upload(file, name, artisan.id).then(function() {
            LxProgressService.circular.hide()
            _this.loadFilesList();
        })
    }

    _this.artisanClickTrigger = function(elem) {
        setTimeout(function() {
            angular.element("#file_" + elem + ">input").trigger('click');
        }, 0)

    }
    _this.rightClick = function($event) {
        _this.contextMenu.setPosition($event.pageX, $event.pageY)
        _this.contextMenu.setData(artisan);
        _this.contextMenu.open();
    }

    _this.validIBAN = function(iban) {
        return !iban || IBAN.isValid(iban);
    }

    _this.fileExist = function(name) {
        if (!artisan.file)
            return false;
        return _.find(artisan.file, function(e) {
            return _.startsWith(e, name)
        });
    }
    _this.smart = [{
	label:"Smartphone"
    },{
	label:"Mobile"
    },{
	label:"Non demandé"
    }]
    
    if (!_this.data.smartphone)
    {
	_this.data.smartphone = "Non demandé"
    }
    _this.sendIns = function () {
	var opt = {}
	opt.to = _this.data.email;
	opt.mail = "instructionLydia.html"
	opt.obj = "Instruction Lydia"
	var id = _this.data.id
	edisonAPI.artisan.sendIns(id, opt)
	LxNotificationService.success("Les instructions Lydia ont bien été envoyés");
    }
    _this.loadFilesList = function() {
        edisonAPI.artisan.getFiles(artisan.id).then(function(result) {
            artisan.file = result.data;
        })
    }
    if (artisan.id) {
        _this.loadFilesList();
    }
    _this.checklimit = function (){
		edisonAPI.intervention.checklimit({id: _this.data._id}).then(function(resp)
    		{
	       		_this.seuilResp = Math.round(parseFloat(resp.data));
		})
	}

	_this.checklimitRupture = function () {
		edisonAPI.intervention.checklimit({id: _this.data._id}).then(function(resp)
	    	{
        		_this.seuilRupt = Math.round(parseFloat(resp.data));
		})
	}
    
    _this.addComment = function() {

        artisan.comments.push({
            login: $rootScope.user.login,
            text: _this.commentText,
            date: new Date()
        })
        if (artisan.id) {
            edisonAPI.artisan.comment(artisan.id, _this.commentText)
        }
        _this.commentText = "";
    }
    var updateTmpArtisan = _.after(5, _.throttle(function() {
        edisonAPI.artisan.saveTmp(artisan);

	
    }, 30000))

    if (!artisan.id) {
        $scope.$watch(function() {
            return artisan;
        }, updateTmpArtisan, true)
    }
}
angular.module('edison').controller('ArtisanController', ArtisanCtrl);
