var ProspectCtrl = function($window, edisonAPI, ContextMenu, $routeParams, $rootScope, $q, $location, dialog, config, Map, LxNotificationService, user, mapAutocomplete) {
    "use strict";
    var _this = this;
    _this.data = {};
    _this.contextMenuSST = new ContextMenu('artisan')
    _this.contextMenu = _this.contextMenuSST;
    _this.config = config;
    _this.hasCalled = false;
    _this.autocomplete = mapAutocomplete

    var refreshProspect = function() {
	var cont = true;
	for (var n = 0; n < _this.nearestArtisans.length && cont === true; n++)
	{
	    if (_this.nearestArtisans[n].id === _this.data.artisan.id)
	    {
		_this.nearestArtisans[n].statutProspect = _this.data.artisan.statutProspect;
		cont = false;
	    }
	}
    }

    var removeProspect = function() {
	var cont = true;
	for (var n = 0; n < _this.nearestArtisans.length && cont === true; n++)
	{
	    if (_this.nearestArtisans[n].id === _this.data.artisan.id)
	    {
		_this.nearestArtisans.splice(n, 1);
		cont = false;
	    }
	}
    }
    
    _this.callSst = function(tel, id)
    {
	edisonAPI.artisan.exist(_this.data.artisan.email, _this.data.artisan.telephone.tel1).then(function(resp) {
	    if (resp.data === "exist")
	    {
		removeProspect();
		edisonAPI.prospect.remove(_this.data.artisan.id);
		var message = _.template("L'artisan étant déjà dans la base, le prospect va être supprimé.")(resp.data)
		LxNotificationService.success(message);
		_this.data.artisan = _this.data.sst = null;
	    }
	    else
	    {
		_this.hasCalled = true;
		if (tel) {
		    $window.open('callto:' + tel, '_self', false);
		}
		if (id)
		{
		    edisonAPI.prospect.callLog(id).then(function (resp) {
			_this.data.artisan.calls = resp.data;
			var message = _.template("L'appel a été ajouté à l'historique.")(resp.data)
			LxNotificationService.success(message);
		    }, function (error) {
			LxNotificationService.error(error.data);
		    });
		}
	    }
	});
    }

    _this.showmap = window.innerWidth > 1655
    $(window).resize(function() {
	_this.showmap = window.innerWidth > 1655
    })
    
    _this.searchArtisans = function(intervention) {
	if (_.get(intervention, 'client.address.lt')) {
	    edisonAPI.prospect.getNearest(intervention.client.address, intervention.categorie)
	        .success(function(result) {
		    _this.nearestArtisans = result
		});
	}
    }

    _this.validIBAN = function(iban) {
	return !iban || IBAN.isValid(iban);
    }

    _this.addComment = function() {

	_this.data.artisan.comments.push({
	    login: $rootScope.user.login,
	    text: _this.commentText,
	    date: new Date()
	})
	if (_this.data.artisan.id) {
	    edisonAPI.prospect.comment(_this.data.artisan.id, _this.commentText)
	}
	_this.commentText = "";
    }

    var latLng = function(add) {
	return add.lt + ', ' + add.lg
    }

    var artisanSwitch = function(sst, first) {
	_this.hasCalled = false;
	if (!sst) {
	    _this.data.artisan = null
	    return false;
	}
	$q.all([
	    edisonAPI.prospect.get(sst.id, {
		cache: false
	    }),
/*	    edisonAPI.prospect.getStats(sst.id, {
		cache: true
	    })*/
	]).then(function(result) {
	    _this.data.sst = _this.data.artisan = result[0].data;
            _this.data.sst.stats = {};
	    edisonAPI.getDistance(latLng(sst.address), latLng(_this.inter.client.address))
	        .then(function(dir) {
		    _this.data.sst.stats.direction = dir.data;
		})
	    _this.recapFltr = {
		ai: _this.data.sst.id
	    }
	});
    }

    _this.selectArtisan = function(sst, first) {
	if (_this.hasCalled == true && !_this.data.artisan.statutProspect)
	{
	    dialog.prospectCall(_this.data.artisan, function (statut) {
		if (statut !== undefined)
		{
		    _this.data.artisan.statutProspect = config.statutProspect[statut];
		    edisonAPI.prospect.save(_this.data.artisan).then(function (resp) {
			if (_this.data.artisan)
			    refreshProspect();
			artisanSwitch(sst, first);
		    });
		}
	    });
	}
	else
	    artisanSwitch(sst, first);
    }

    if ($routeParams.id)
    {
	edisonAPI.intervention.get($routeParams.id).then(function (resp) {
	    _this.inter = resp.data;
	    _this.searchArtisans(_this.inter);
	});
    }
    else if ($routeParams.addr && $routeParams.cate)
    {
	_this.inter = {}
	_this.inter.client = {}
	_this.inter.client.address = {v: $routeParams.addr};
	_this.inter.categorie = $routeParams.cate;
	_this.autocomplete.getPlaceAddress({description: $routeParams.addr}).then(function (addr) {
	    _this.inter.client.address.lt = addr.lt;
	    _this.inter.client.address.lg = addr.lg;
	    _this.searchArtisans(_this.inter);
	})
    }

    _this.findColor = function(categorie) {
	if (_this.data.artisan.statutProspect && _this.data.artisan.statutProspect.short_name === categorie.short_name)
	    return (categorie.color);
	return ("white");
    }

    _this.toggleCategorie = function(categorie) {
	if (!_this.data.artisan.statusProspect ||
	    _this.data.artisan.statutProspect.categorie.short_name != categorie.short_name)
	    _this.data.artisan.statutProspect = categorie;
	edisonAPI.prospect.save(_this.data.artisan).then(function (resp) {
	    refreshProspect();
	    var message = _.template("Les changements ont été enregistrés")(resp.data)
	    LxNotificationService.success(message);
	}, function (error) {
	    LxNotificationService.error(error.data);
	});

    }

    _this.save = function () {
	edisonAPI.prospect.save(_this.data.artisan).then(function (resp) {
	    refreshProspect();
	    var message = _.template("Les changements ont été enregistrés")(resp.data)
	    LxNotificationService.success(message);
	}, function (error) {
	    LxNotificationService.error(error.data);
	});
    }

    _this.move = function () {
	var id = _this.data.artisan.id;
	delete _this.data.artisan.id;
	delete _this.data.artisan._id;
	_this.data.artisan.transferedBy = user.login
	edisonAPI.artisan.save(_this.data.artisan).then(function (resp) {
	    _this.hasCalled = false;
	    removeProspect();
	    edisonAPI.prospect.remove(id);
	    var message = _.template("La fiche artisan à bien été crée")(resp.data)
	    LxNotificationService.success(message);
	    $location.url("/artisan/" + resp.data.id.toString());
	}, function (error) {
	    _this.data.artisan.id = id;
	    _this.data.artisan._id = id;
	    LxNotificationService.error(error.data);
	});
    }

}

angular.module('edison').controller('ProspectController', ProspectCtrl);
