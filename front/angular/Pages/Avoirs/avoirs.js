var AvoirsController = function(TabContainer, openPost, edisonAPI, $rootScope, LxProgressService, LxNotificationService, FlushList) {
    "use strict";
    var _this = this
    var tab = TabContainer.getCurrentTab();
    _this.offsetX = 1;
    _this.offsetY = -5;
    tab.setTitle('Avoirs')
    _this.loadData = function(prevChecked) {
        LxProgressService.circular.show('#5fa2db', '#globalProgress');
        edisonAPI.compta.avoirs().then(function(result) {
            $rootScope.avoirs = result.data
            LxProgressService.circular.hide()
        })
    }
    if (!$rootScope.avoirs)
        _this.loadData()

    _this.reloadAvoir = function() {
        _this.loadData()
    }

    _this.print = function(type) {
        openPost('/api/intervention/printAvoir', {
            data: $rootScope.avoirs
        });
    }

    _this.printChq = function(type) {
        var checked = [];
        var check = 0;
        for (var ind = 0; ind < $rootScope.avoirs.length; ind++)
        {
            if ($rootScope.avoirs[ind].checked === true)
            {
                check++;
                checked.push($rootScope.avoirs[ind]);
            }
        }
        if (check >= 1)
        {
            openPost('/api/intervention/printAvoirChq', {
                data: checked,
                offsetX: _this.offsetX || 0,
                offsetY: _this.offsetY || 0
            });
        }
        else
        {
            openPost('/api/intervention/printAvoirChq', {
                data: $rootScope.avoirs,
                offsetX: _this.offsetX || 0,
                offsetY: _this.offsetY || 0
            });
        }
    }

    _this.flush = function() {
        var list = _.filter($rootScope.avoirs, {
            checked: true
        })
        edisonAPI.compta.flushAvoirs(list).then(function(resp) {
            LxNotificationService.success(resp.data);
            _this.reloadAvoir()
        }).catch(function(err) {
            LxNotificationService.error(err.data);
        })
    }

}


angular.module('edison').controller('avoirsController', AvoirsController);
