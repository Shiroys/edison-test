var ImportController = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, socket, $window, mapAutocomplete, Artisan, dialog, user) {
    "use strict";
    var _this = this;

    _this.saveAllArtisan = function() {
	edisonAPI.artisan.saveAll();
    }

    var geocoder = new google.maps.Geocoder();

    ImportController.prototype.$scope = $scope;

    var csvToJSON = function (file) {
	var lines = file.split("\n");
	var result = [];
	var header = lines[0].split(";");
	header.pop();

	for (var n = 1; n < lines.length; n++)
	{
	    var obj = {};
	    var current = lines[n].split(";");

	    for (var l = 0; l < header.length; l++)
		obj[header[l]] = current[l];

	    result.push(obj);
	}

	return (result);
    }

    var convertToArtisan = function (elem) {
	var tmp = {};

	tmp.email = elem.email;
	tmp.representant = {
	    nom: elem.nom,
	    prenom: elem.prenom,
	    civilite: "M."
	};
	tmp.categories = elem.categories.split(',');
	tmp.nomSociete = elem.societe;
	tmp.telephone = {
	    tel1: (elem.tel1 ? elem.tel1.replace(/\s/g,'') : ''),
	    tel2: (elem.tel2 ? elem.tel2.replace(/\s/g,'') : ''),
	};
	tmp.infoSup = {
	    siret: elem.siret,
	    Site1: elem.site1,
	    Site2: elem.site2,
	    Site3: elem.site3
	};

	tmp.zoneChalandise = "30";
	tmp.pourcentage = {
	    fourniture: "30",
	    maindOeuvre: "30",
	    deplacement: "50",
	};

	tmp.address = {
	    cp: elem.cp,
	    n: 0,
	    r: elem.rue,
	    v: elem.ville,
	};

	return (tmp);
    }

   var parseData = function(elems) {
       var finals = [];

       for (var n = 0; n < elems.length; n++)
       {
	   var tmpList = []
	   Object.keys(elems[n]).forEach(function(key, index) {
	       tmpList.push(key);
	       tmpList.push(elems[n][key]);
	   })
	   for (var l = n+1; l < elems.length; l++)
	   {
	       if ((elems[n].tel1 && elems[l].tel1 && elems[n].tel1 === elems[l].tel1) ||
		   (elems[n].tel1 && elems[l].tel2 && elems[n].tel1 === elems[l].tel2) ||
		   (elems[n].tel2 && elems[l].tel1 && elems[n].tel2 === elems[l].tel1) ||
		   (elems[n].tel2 && elems[l].tel2 && elems[n].tel2 === elems[l].tel2))
	       {
		   var copyList = []

		   Object.keys(elems[l]).forEach(function(key, index) {
		       copyList.push(key);
		       copyList.push(elems[l][key]);
		   })
		   for (var m = 0; m < copyList.length; m += 2)
		   {
		       var i = tmpList.indexOf(copyList[m]);
		       if (tmpList[i + 1].length === 0 && copyList[m + 1].length !== 0)
		       {
			   tmpList[i + 1] = copyList[m + 1];
			   elems[n][copyList[m]] = copyList[m + 1];
		       }
		   }
		   elems.splice(l--, 1)
	       }
	   }
	   finals.push(elems[n]);
       }
       return (finals);
   }

    _this.history = function () {
	edisonAPI.history.list().then(function (resp) {
	    _this.hist = resp.data;
	})
    }

    _this.history();

    _this.geoloc = function() {
	if ($scope.fileGeoloc)
	{
	    var name = $scope.fileGeoloc.name;
	    var read = new FileReader();
	    read.onload = function(e) {
		var elems = csvToJSON(e.target.result);
		elems = parseData(elems.slice(0));
		edisonAPI.history.add({login: user.login, name: name});
//		edisonAPI.prospect.checkDuplicates(elems).then(function (resp) {
		    var finals = [];
		    for (var n = 0; n < elems.length; n++)
			finals.push(convertToArtisan(elems[n]));
		    edisonAPI.prospect.geolocNotBDD(finals);
//		})
	    }
	    read.readAsText($scope.fileGeoloc, 'ISO-8859-1');
	}
    }
}


ImportController.prototype.changeFileGeoloc = function (elem) {
    var $scope = this.$scope;
    $scope.$apply(function () {
	$scope.fileGeoloc = elem.files[0];
    });
};

angular.module('edison').controller('ImportController', ImportController);
