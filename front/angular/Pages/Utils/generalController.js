var general = function(TabContainer, edisonAPI, $rootScope, $scope, $location, LxNotificationService, socket, $window, mapAutocomplete, Artisan, Intervention, dialog, user) {
    "use strict";
    var _this = this;

    _this.saveGrandsComptes = function() {
		edisonAPI.intervention.saveGrandsComptes();
    }
    
    _this.Geolocalisation = function () {
		edisonAPI.prospect.geoloc();
    }
    _this.CP = function () {
		edisonAPI.prospect.CP();
    }
    _this.Merge = function () {
		edisonAPI.prospect.Merge();
    }

	_this.relanceAuto7h = function() {
		edisonAPI.intervention.relanceAuto();
	}

	_this.changePriseEnCharge = function() {
		edisonAPI.intervention.priseEnChargeTimer().then(function(resp) {
			console.log("------>", resp);
		})
	}

	_this.listInter = function() {
		if ($scope.fileId)
		{
			/* Read local file */
			var name = $scope.fileId.name;
			var read = new FileReader($scope.fileId);
			read.onload = function(e) {
				edisonAPI.intervention.listInter({fileId: JSON.parse(e.target.result)}).then(function(resp) {
				});
			}
			read.readAsText($scope.fileId, 'ISO-8859-1');
		}
	}

	/* Fonction de transformation de status à partir d'un fichier JSON (Outils Admin) */
	_this.statusTrans = function() {
		if ($scope.fileId)
		{
			/* Read local file */
			var name = $scope.fileId.name;
			var read = new FileReader($scope.fileId);
			read.onload = function(e) {
				/* Appel de la fonction de transformation du status /server/models/intervention/methods/intervention.transStatus.js */
				edisonAPI.intervention.transSav({fileId: JSON.parse(e.target.result), nStatus: $scope.typeSt}).then(function() {
					LxNotificationService.success("Les transformations sont finies.");
				});
			}
			read.readAsText($scope.fileId, 'ISO-8859-1');
		}
	}

	_this.switchTest = function() {
		edisonAPI.intervention.switchTest().then(function(resp) {
		});
	}

	_this.mailAverif = function() {
		edisonAPI.intervention.mailAverif().then(function(resp) {
		});
	}

	_this.getDatasOnFactureAndClientNames = function() {
		edisonAPI.intervention.getDatasOnFactureAndClientNames().then(function(resp) {
		})
	}

	_this.transfo = function(typeSig) {
		edisonAPI.signalement.transfo({typeSig: typeSig}).then(function(resp) {
		});
	}

	_this.ReloadCache = function() {
		edisonAPI.intervention.reloadCache();
	}

	_this.flushTrue = function() {
		edisonAPI.intervention.flushTrue().then(function(resp) {
		});
	}

    _this.relAll = function () {
		edisonAPI.intervention.relanceAll();
    }

    general.prototype.$scope = $scope;

    _this.importRIB = function () {
		var read = new FileReader();
		read.onload = function(e) {
	   		var elems = toObject(csvToJSON(e.target.result));
	    	edisonAPI.artisan.addIBAN(elems).then(function(resp){
	    	})
		}
		read.readAsText($scope.file, 'ISO-8859-1');
    }

	/* Prototype de sauvegarde du fichier via onchange angular */
   	general.prototype.changeFile = function(elem) {
		var $scope = this.$scope;
		$scope.$apply(function () {
	    	$scope.fileId = elem.files[0];
		});
    };
}

/*
**
** This Controller serves for a large variety of
** functions linked to database manipulation
**
*/


angular.module('edison').controller('GeneralController', general);
