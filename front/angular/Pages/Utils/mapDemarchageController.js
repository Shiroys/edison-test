var MapDemarchageController = function($rootScope, edisonAPI, $scope, config) {
    var _this = this;
    $scope._ = _;
    $scope.root = $rootScope;

    _this.cat = config.categoriesHash();
    
    _this.changeCat = function () {
	_this.confirmedArtisans = []
	for (var n = 0; n < _this.complete.length; n++)
	{
	    if (_this.complete[n].cate === parseInt($scope.categorie))
		_this.confirmedArtisans.push(_this.complete[n]);
	}
    }

    edisonAPI.demarchage.list().then(function (resp) {
	_this.complete = []
	for (var n = 0; n < resp.data.length; n++)
	{
	    if (resp.data[n].foundOne === false)
		_this.complete.push(resp.data[n]);
	}
    });
}

angular.module('edison').controller('MapDemarchageController', MapDemarchageController);
