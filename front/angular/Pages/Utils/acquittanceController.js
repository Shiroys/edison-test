var acquittanceController = function(config, edisonAPI, MomentIterator, $scope, dialog) {
    "use strict";
    var _this = this;
    _this.config = config;
    _this.log = {}
    _this.late = {}
    _this.total = {}
    _this.excused = {}

    var start = new Date("08/01/2016");
    start.setHours(0,0,0,0);
    var end = new Date();
    end.setHours(1,0,0,0);
    _this.dateSelect = MomentIterator(start, end).range('week', {
	format: 'DD-MM-YYYY',
    }).map(function(e) {
	return e
    })

    $scope.selectedWeek = _this.dateSelect[_this.dateSelect.length - 1];
    
    _this.loadData = function () {
	var obj = {};
	obj.year = $scope.selectedYear;
	edisonAPI.compta.getFourniture(obj).then(function (resp) {
	    _this.years = resp.data;
	})
    }
    _this.loadData();

    _this.weekDays = function () {
	return (['Logins', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche', 'Retards Accumulés', 'Temps de travail'])
    }
    
    _this.cancel = function (login, day, elem, excused) {
	if (elem != 0 && excused === false)
	{
	    dialog.cancelLate({login: login, day: _this.weekDays()[day + 1], nb: day, week: $scope.selectedWeek}, function (data) {
		edisonAPI.users.cancel(data);
	    })
	}
    }

    _this.weekLog = function (login, hourse, hoursf) {
	edisonAPI.log.getLate({login: login, weekStart: $scope.selectedWeek}).then(function (resp) {
	    _this.late[login] = resp.data.late;
	    _this.excused[login] = resp.data.excused;
	    _this.total[login] = 0;
	    for (var n = 0; n < resp.data.late.length; n++)
		_this.total[login] += resp.data.late[n] > 0 ? resp.data.late[n] : 0;
	    edisonAPI.log.getWeekLog({login: login, weekStart: $scope.selectedWeek}).then(function (resp) {
		_this.log[login] = resp.data;
		var time = 0
		for (var j = 0;hourse.length > j;j++)
		{
		    var d1 = new Date(hourse[j])
		    var d1h = d1.getHours()
		    var d1m = d1.getMinutes()
		    var d2 =  new Date(hoursf[j])
		    var d2h = d2.getHours()
		    var d2m = d2.getMinutes()
		    var tot1 = (d2h - d1h) * 3600000
		    var tot2 = (d2m - d1m) *60000
		    var tot = tot1 + tot2
		    if (_this.log[login][j] !== "Abs")
		    {
			if (d1.getHours() != 0)
			    time = tot + time;
		    }
		}
		time = time - (_this.total[login] * 60000)
		var x = time / 1000
		var seconds = x % 60
		x /= 60
		var minutes = x % 60
		x /= 60
		var hours = Math.floor(x);
		time = hours + "h" + minutes + "min"
		_this.time[login] = time
	    });
	});
	
	
	
    }

    _this.reload = function () {
	if (_this.userList)
	{
	    for (var n = 0; n < _this.userList.length; n++)
		_this.weekLog(_this.userList[n].login);
	}
    }
    
    _this.getUserList = function () {
	edisonAPI.users.list().then(function (resp) {
	    _this.userList = []
	    _this.time = {}
	    for (var n = 0; n < resp.data.length; n++)
	    {
		if (resp.data[n].activated === true)
		{
		   _this.time[resp.data[n].login] = ""
		    _this.weekLog(resp.data[n].login, resp.data[n].hours, resp.data[n].hoursf);
		    _this.userList.push({login: resp.data[n].login, service: resp.data[n].service});
		    
		}
	    }
	})
    }
    _this.getUserList();

    $scope.$watch("selectedWeek", function() {
	_this.reload();
    });

}

angular.module('edison').controller('acquittanceController', acquittanceController);
