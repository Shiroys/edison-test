angular.module('edison').directive('signalement', function(edisonAPI, LxNotificationService, dialog) {
    "use strict";
	/* Directive pour faire un signalement dans une intervention */
    return {
        replace: false,
        restrict: 'E',
        templateUrl: '/Templates/signalement.html',
        scope: {
            data: '=',
            exit: '&',
        },
        link: function(scope, elem) {
            scope.setSelectedSubType = function(subType) {
                scope.selectedSubType = scope.selectedSubType === subType ? null : subType
            }
            edisonAPI.signal.list().then(function(resp) {
                scope.signalementsGrp = _.groupBy(resp.data, 'subType');
            })
            scope.hide = function(signal) {
				/* Ouverture et appel du pop up commentaires/raison */
		    	dialog.pop_up_level2_comment(signal.nom, function(resp, cancel){
					if (!resp.commentaire)
					    resp.commentaire = "Aucune"
					var check = resp.commentaire;
					/* Si on clique sur valider cancel = true */
					if (cancel)
					{
						var arrayDest = [];
						/* Creation du tableau des destinataires */
						for (var ind = 0; ind < signal.dest.users.length; ind++)
						{
							/* Tri pour le spécial */
							if (signal.dest.special && signal.dest.special.ajoutI && signal.dest.special.ajoutI === true && scope.data.login.ajout
								&& scope.data.login.ajout == signal.dest.users[ind].login)
								arrayDest.push(signal.dest.users[ind].email);
							else if (signal.dest.special && signal.dest.special.envoiI && signal.dest.special.envoiI === true && scope.data.login.envoi
								&& scope.data.login.envoi == signal.dest.users[ind].login)
								arrayDest.push(signal.dest.users[ind].email);
							else if (signal.dest.special && signal.dest.special.verifI && signal.dest.special.verifI === true && scope.data.login.verification
								&& scope.data.login.verification == signal.dest.users[ind].login)
								arrayDest.push(signal.dest.users[ind].email);
							else if (signal.dest.special && signal.dest.special.annulI && signal.dest.special.annulI === true && scope.data.login.annulation
								&& scope.data.login.annulation == signal.dest.users[ind].login)
								arrayDest.push(signal.dest.users[ind].email);
							else if (signal.dest.special && signal.dest.special.ajoutA && signal.dest.special.ajoutA === true && scope.data.artisan.login.ajout
								&& scope.data.artisan.login.ajout == signal.dest.users[ind].login)
								arrayDest.push(signal.dest.users[ind].email);
							/* Tri pour les services */
							else if ((signal.dest.service && signal.dest.service.commerc === true && signal.dest.users[ind].service == "INTERVENTION")
										|| (signal.dest.service && signal.dest.service.partena === true && signal.dest.users[ind].service == "PARTENARIAT")
										|| (signal.dest.service && signal.dest.service.compta === true && signal.dest.users[ind].service == "COMPTABILITE")
										|| (signal.dest.service && signal.dest.service.recouv === true && signal.dest.users[ind].service == "RECOUVREMENT"))
								arrayDest.push(signal.dest.users[ind].email);
							/* Tri pour les users cochés */
							else if (signal.dest.users[ind].send === true)
								arrayDest.push(signal.dest.users[ind].email);
							/* Tri pour l'émitteur */
							else if (signal.dest.users[ind].login == scope.data.login.ajout)
								arrayDest.push(signal.dest.users[ind].email);
						}
				 	   	signal.isStar = scope.data.sst.star;
						/* Appel de la fonction d'envoi du signalement */
						edisonAPI.signalement.add(_.merge(signal, {
							addBy: scope.data.login.ajout || 'Non ajoutée',
							verifBy: scope.data.login.verification || 'Non vérifiée',
							inter_id: scope.data.id || scope.data.tmpID,
							raison: check || "Raison: Aucune",
							arrayDest: arrayDest,
							sst_id: scope.data.sst && scope.data.sst.id,
							sst_nom: scope.data.sst && scope.data.sst.nomSociete,
					    })).then(function() {
							LxNotificationService.success("Le signalement a été envoyé.")
				  	  	})
					}
		    	})
                return scope.exit && scope.exit()
            }
        }
    }
 });
