var Controller = function($timeout, TabContainer, FiltersFactory, user, ContextMenu, LxProgressService, LxNotificationService, edisonAPI, DataProvider, $routeParams, $location, $rootScope, $filter, config, ngTableParams, MomentIterator, openPost) {
    var _this = this;
    _this._ = _;
    _this.displayUser = app_session;
    LxProgressService.circular.show('#5fa2db', '#globalProgress');
    var currentFilter;
    var currentHash = $location.hash();
    var dataProvider = new DataProvider(_this.model, $routeParams.hashModel)
    var filtersFactory = new FiltersFactory(_this.model)
    _this.see = 0;
    if ($routeParams.fltr) {
        currentFilter = filtersFactory.getFilterByUrl($routeParams.fltr)
    }
    if ($routeParams.fltr && $routeParams.fltr === "grandComptes")
    {
	edisonAPI.compte.list().then(function (resp) {
	    _this.compteList = [];
	    for (var n = 0; n < resp.data.length; n++)
	    {
		_this.compteList.push(resp.data[n]);
	    }
	});
    }

    if ($routeParams.fltr && $routeParams.fltr === "map")
    {
	edisonAPI.artisan.starList().then(function (resp) {
	    _this.confirmedArtisans = resp.data;
	});
    }

    _this.interSwipe = function (id) {
	$location.url("/intervention/" + id);
    }

    _this.devisSwipe = function (id) {
	$location.url("/devis/" + id);
    }

    _this.changeCompte = function () {
		$location.url("/intervention/list/grandComptes?nc=" + this.payeurToShow);
		_this.payeurToShow = this.payeurToShow;
    }
    var now = new Date()
    var end = new Date(now.getFullYear(), now.getMonth()+1, 1);
    var start = moment().add(-13, 'month').toDate()
    _this.dateSelectList = MomentIterator(start, end).range('month').map(function(e) {
		return {
	            ts:e.unix(),
	            t: e.format('MMM YYYY'),
	            m: e.month() + 1,
	            y: e.year(),
	        }
    })

    _this.selectAll = function () {
		for (var n = 0; n < _this.tableParams.data.length; n++)
		{
	    	_this.tableParams.data[n].Action = true;
			_this.tableParams.data[n].ActionTshirt = true;
		}
		_this.keep();
    }

    _this.keep = function () {
		var elems = [];
		for (var n = 0; n < _this.tableParams.data.length; n++)
		{
	    	if ((_this.tableParams.data[n].Action && _this.tableParams.data[n].Action === true) || (_this.tableParams.data[n].ActionTshirt && _this.tableParams.data[n].ActionTshirt))
	   		{
				elems.push(_this.tableParams.data[n]);
			}
		}
		if (elems.length === 0)
	    	_this.see = 0;
		else
	    	_this.see = 1;
    }

    _this.ArcCheck = function () {
		_this.arc = 1;
    };
    _this.RelCheck = function () {
		_this.rel = 1;
    };
    _this.EnvCheck = function () {
		_this.env = 1;
    };
    _this.FacCheck = function () {
		_this.fac = 1;
    }
    _this.RefCheck = function () {
		_this.ref = 1;
    };
    _this.ActionArt = function () {
		var arc = _this.arc;
		var rel = _this.rel;
		var env = _this.env;
		var fac = _this.fac;
		var ref = _this.ref;
		var elems = [];
		for (var n = 0; n < _this.tableParams.data.length; n++)
		{
			if (_this.tableParams.data[n].ActionTshirt && _this.tableParams.data[n].ActionTshirt === true)
	   		{
				var check = 0;
				if (elems.length != 0)
				{
					var ind = 0;
					while (elems[ind] != null)
					{
						if (_this.tableParams.data[n].id == elems[ind].id)
						{
							elems[ind].ActionTshirt = true;
							check = 1;
							ind = -2;
						}
						ind++;
					}
					if (check == 0)
						elems.push(_this.tableParams.data[n]);
				}
				else
					elems.push(_this.tableParams.data[n]);
	    	}
			if (_this.tableParams.data[n].Action && _this.tableParams.data[n].Action === true)
			{
				var check = 0;
				if (elems.length != 0)
				{
					var ind = 0;
					while (elems[ind] != null)
					{
						if (_this.tableParams.data[n].id == elems[ind].id)
						{
							elems[ind].Action = true;
							check = 1;
							ind = -2;
						}
						ind++;
					}
					if (check == 0)
						elems.push(_this.tableParams.data[n]);
				}
				else
					elems.push(_this.tableParams.data[n]);
			}
		}
		for (var j = 0;j < elems.length;j++)
		{
	    	var artisan = elems[j];
	    	edisonAPI.artisan.get(artisan.id).then(function(resp) {
				var ind = 0;
				while (elems[ind] != null)
				{
					if (elems[ind].id == resp.data.id)
					{
						var factu = elems[ind].Action;
						var tshirt = elems[ind].ActionTshirt;
						ind = -2;
					}
					ind++;
				}
				var mes = "Bonjour Monsieur " + resp.data.representant.nom + "\n" + "\n" + "Suite à notre dernière échange téléphonique concernant une proposition de partenariat entre nos deux entreprises,\n" + "je me permet de vous rappeler que certains documents sont essentiels à notre collaboration.\n" + "\n" +"Vous trouverez donc ci-joint la déclaration de sous-traitance à remplir.\n" + "\n" +"Merci de joindre également à cette déclaration les éléments suivants :\n" + "\n" + "<strong>• Extrait KBIS ou INSEE</strong>\n" + "<strong>• Photocopie R/V de la pièce d'identité du gérant</strong>\n" + "\n" +"Vous pouvez nous transmettre ces pièces administratives par mail à :\n" + "\n" +"yohann.rhoum@edison-services.fr\n" + "\n" + "\n" + "Ou par voie postal :\n" + "\n" + "<u><b>" + "Edison Services\n" + "Service Partenariat\n" + "32 rue Fernand Pelloutier -921107ClichyS\n" + "</b></u>" + "\n" + "\n" + "Dès réception de ces documents et validation par nos services, vous recevrez par voie postal:\n" + "\n" + "• Un bloc facture Edison Services\n" + "• Un bloc devis Edison Services\n" + "• Un accès à tous nos fournisseurs\n" + "\n" +"Je reste à votre entière disposition pour toutes les questions ou les remarques que vous pourriez avoir.\n" + "\n" + "Dans l'attente d'une réponse favorable de votre part,\n" + "\n" + "Cordialement\n" + "\n" + "<b>Yohann RHOUM</b>\n" + "Service Partenariat\n" + "Tel : 09.72.45.27.10 Fax : 09.72.39.33.46\n" + "yohann.rhoum@edison-services.fr\n" + "\n" + "<b>Edison Services</b>\n" + "Dépannage - Entretien - Installation - Rénovation\n" + "Siège social : 32 rue Fernand Pelloutier, 92110,Clichys\n" + "contact@edison-services.fr - www.edison-services.fr\n"
				if (arc === 1)
				{
					resp.data.status = "ARC";
					var message = "L'artisan n " + resp.data.id +  " a été archivé";
					LxNotificationService.success(message);
		    		edisonAPI.artisan.save(resp.data).then(function(resp2){
						edisonAPI.artisan.activite(resp.data.id, "cause d'Archivage: Archivage Multiple")
		    		})
				}
				else if (env === 1)
				{
		   			edisonAPI.artisan.envoiContrat(resp.data.id, {
					text: mes,
					signe: true
		    		})
		    		var message = "Envoi de contrat pour l'artisan n " + resp.data.id;
		    		LxNotificationService.success(message);
				}
				else if (fac === 1)
				{
					if (tshirt == true && (factu == false || !factu))
					{
						window.open('/api/artisan/' + resp.data.id + '/tshirt');
						edisonAPI.artisan.sendFacturier(resp.data.id, false, false, "tshirt");
					}
					else if (factu == true && (tshirt == false || !tshirt))
					{
						window.open('/api/artisan/'+ resp.data.id + '/facturier')
		    			edisonAPI.artisan.sendFacturier(resp.data.id, "facturier", "deviseur", false);
					}
					else if (tshirt == true && factu == true)
					{
						window.open('/api/artisan/'+ resp.data.id + '/facturier')
						window.open('/api/artisan/' + resp.data.id + '/tshirt');
						edisonAPI.artisan.sendFacturier(resp.data.id, "facturier", "deviseur", "tshirt");
					}
				}
				else if (ref === 1)
				{
		    		resp.data.demandeFacturier.status = 'NO';
		    		edisonAPI.artisan.save(resp.data).then(function(resp2){
		    		})
		    		var message = "Le facturier a été refusé pour Mr." + resp.data.representant.nom
		    		LxNotificationService.success(message);
				}
				else
				{
		    		edisonAPI.artisan.envoiContrat(resp.data.id, {
					text: mes,
					signe: true,
					rappel: true
		    		})
		    		var message = "Relance de documents pour l'artisan n " + resp.data.id
		    		LxNotificationService.success(message);
				}
	    	})
		}
		_this.fac = 0;
		_this.rel = 0
		_this.env = 0;
		_this.arc = 0;
		_this.tshirt = 0;
    }

    _this.removeDemarch = function (elem) {
        edisonAPI.demarchage.remove(elem._id).then(function (resp) {
            _this.tableParams.data.forEach(function (element, i, array) {
                if (element._id === elem._id)
                    array.splice(i, 1);
            });
            var message = _.template("La démarche a bien été retirée de la liste de démarchage.")(resp.data)
            LxNotificationService.success(message);
        }, function (error) {
            LxNotificationService.error(error.data);
        });
    }

    _this.found = function (elem) {
		var dateStr = new Date();
		var dArr = [];
		dArr = _.words(dateStr), /[^- ]+/;
		dArr[1] = dateStr.getMonth() + 1;
		var datev = dArr[2] + "/" + dArr[1] + "/" + dArr[3];
		edisonAPI.demarchage.validedemarche({id: elem._id,login: user.login,date: datev}).then(function (resp) {
		    _this.tableParams.data.forEach(function (element, i, array) {
			if (resp.data._id === element._id)
			{
			    array[i].demarchedate = resp.data.demarchedate
			    array[i].demarchepar = resp.data.demarchepar
			}
		    });
		})
		edisonAPI.demarchage.map({id: elem._id}).then(function (resp) {
		    _this.tableParams.data.forEach(function (element, i, array) {
			if (resp.data._id === element._id)
			{
			    array[i].foundOne = resp.data.foundOne;
			}
		    });
		})
    }
    _this.validate = function (elem) {
		var dateStr = new Date();
		var dArr = [];
		dArr = _.words(dateStr), /[^- ]+/;
		dArr[1] = dateStr.getMonth() + 1;
		var datev = dArr[2] + "/" + dArr[1] + "/" + dArr[3];
		edisonAPI.demarchage.validate({id: elem._id,login: user.login,date: datev}).then(function (resp) {
		    _this.tableParams.data.forEach(function (element, i, array) {
			if (resp.data._id === element._id)
			{
			    array[i].validedate = resp.data.validedate
			    array[i].validepar = resp.data.validepar
			}
		    });

		})
    }

    _this.routeParamsFilter = $routeParams.fltr;
    if (_this.embedded) {
        _this.$watch('filter', function() {
            if (_.size(_this.filter)) {
                _this.customFilter = function(inter) {
                    for (var i in _this.filter) {
                        if (_this.filter[i] !== inter[i])
                            return false
                    }
                    return true
                }
                if (_this.tableParams) {
                    dataProvider.applyFilter({}, _this.tab.hash, _this.customFilter);
                    _this.tableParams.reload();
                }
            }
        })
    }
    _this.displaySubRow = function(inter) {
        return _this.expendedRow && _this.expendedRow === inter.id;
    }

    _this.smallWin = window.innerWidth < 1400
    _this.phoneWin = window.innerWidth < 480
    $(window).resize(function() {
        _this.smallWin = window.innerWidth < 1400
		_this.phoneWin = window.innerWidth < 480
    })

    _this.tab = TabContainer.getCurrentTab();
    _this.tab.hash = currentHash;
    _this.config = config;
    var title = currentFilter ? currentFilter.long_name : _this.model;
    if ($routeParams.sstid) {
        var id = parseInt($routeParams.sstid)
        _this.customFilter = function(inter) {
            return inter.ai === id;
        }
    } else {
        _this.tab.setTitle(title, currentHash);
    }
    if ($routeParams.sstids_in) {
        _this.customFilter = function(inter) {
            return _.includes($routeParams.sstids_in, inter.id);
        }
    }
    if ($routeParams.ids_in) {
        var tab = JSON.parse($routeParams.ids_in)
        _this.customFilter = function(inter) {
            return _.includes(tab, inter.id);
        }
    }

    _this.$watch(function() {
        return $location.search()
    }, _.after(2, function(nw, old) {
        _this.tableParams.filter(_.omit(nw, 'hashModel', 'page', 'sstid', 'ids_in'))
    }), true)

    _this.$watch(function() {
        return $location.hash()
    }, function(nw, old) {
        if (_this.tableParams) {
            dataProvider.applyFilter(currentFilter, nw, _this.customFilter);
            _this.tableParams.reload()
        }
    }, true)

    var actualiseUrl = function(fltrs, page) {
        $location.search('page', page !== 1 ? page : undefined);
        _.each(fltrs, function(e, k) {
            if (!e) e = undefined;
            if (e !== "hashModel") {
                $location.search(k, e);

            } else {}
        })
    }

    var sortBy = (currentFilter && currentFilter.sortBy) ||  {
        id: 'desc'
    }

    dataProvider.init(function(err, resp) {
        dataProvider.applyFilter(currentFilter, _this.tab.hash, _this.customFilter);
        var tableParameters = {
            page: $location.search()['page'] ||  1,
            total: dataProvider.filteredData.length,
            filter: _this.embedded ? {} : _.omit($location.search(), 'hashModel', 'page', 'sstid', 'ids_in'),
            sorting: sortBy,
            count: _this.limit || 100
        };
        var tableSettings = {
            total: dataProvider.filteredData,
            getData: function($defer, params) {
                var data = dataProvider.filteredData;
                if (!_this.embedded) {
                    data = $filter('tableFilter')(data, params.filter());
                }
                _this.currentFilter = _.clone(params.filter());
                params.total(data.length);
				if (_this.routeParamsFilter === "recouvrementArtn" || _this.routeParamsFilter === "recouvrementArt")
                	data = $filter('orderBy')(data, 'a');
				else
					data = $filter('orderBy')(data, params.orderBy());
                $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            },
            filterDelay: 100
        }
        _this.tableParams = new ngTableParams(tableParameters, tableSettings);
        LxProgressService.circular.hide()
    })

    var lastChange = 0;
    $rootScope.$on(_this.model.toUpperCase() + '_CACHE_LIST_CHANGE', function(event, newData) {
        if (TabContainer.getCurrentTab() && _this.tab.fullUrl === TabContainer.getCurrentTab().fullUrl) {
            dataProvider.applyFilter(currentFilter, _this.tab.hash, _this.customFilter);
	    _this.tableParams.reload();
        }
    })
    _this.contextMenu = new ContextMenu(_this.model)

    if (user.service === 'COMPTABILITE') {
        var subs = _.findIndex(_this.contextMenu.list, "title", "Appels");
        if (subs) {
            var tmp = _this.contextMenu.list[subs]
            _this.contextMenu.list.splice(subs, 1);
            _this.contextMenu.list.push(tmp);
        }
    }
    _this.rowRightClick = function($event, inter) {
        edisonAPI[_this.model].get(inter.id, {
                populate: 'sst'
            })
            .then(function(resp) {
                _this.contextMenu.setData(resp.data);
                _this.contextMenu.setPosition($event.pageX - (($routeParams.sstid ||  _this.embedded) ? 50 : 0), $event.pageY + ($routeParams.sstid ||  _this.embedded ? 0 : 200))
                _this.contextMenu.open();
            })
    }

    _this.rowClick = function($event, inter) {
        if (_this.contextMenu.active)
            return _this.contextMenu.close();
        if ($event.metaKey || $event.ctrlKey) {
            TabContainer.addTab('/' + _this.model + '/' + inter.id, {
                title: ('#' + inter.id),
                setFocus: false,
                allowDuplicates: false
            });
        } else {
            if (_this.expendedRow === inter.id) {
                _this.expendedRow = undefined;
            } else {
                _this.expendedRow = inter.id
            }
        }
    }

    _this.absence  = {};
    _this.absence.start  = "";
    _this.absence.end  = "";
    _this.select = {};
    _this.select.start = "";
    _this.select.end = "";

	_this.$watch('select.end', function() {
	    if (_this.select.start != "")
	    {
		var inter = _this.select.end.split(" ");
		var Mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
		for (i = 0;i < Mois.length;i++)
		{
		    if (inter[1] === Mois[i])
			var nbrMonth = i;
		}
		var end = new Date(inter[2],nbrMonth,inter[0]);
		edisonAPI.artisan.datefacturier({
		    start: _this.absence.start.toISOString(),
		    end: end.toISOString()
		}).then(function(resp) {
		    _this.delai = resp;
		})
	    }
	})

    _this.$watch('select.start', function() {
	if (_this.select.end != "")
	{
	    var inter = _this.select.start.split(" ");
	    var Mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
	    for (i = 0;i < Mois.length;i++)
	    {
		if (inter[1] === Mois[i])
		    var nbrMonth = i;
	    }
	    var start = new Date(inter[2],nbrMonth,inter[0]);
	    edisonAPI.artisan.datefacturier({
		start: start.toISOString(),
		end: _this.absence.end.toISOString()
	    }).then(function(resp) {
		_this.delai = resp;
	    })
	}
    })

    _this.prospectModif = function (cate, ville) {
	$location.url('/prospect/' + ville + '/' + cate);
    }
}

angular.module('edison').directive('lineupIntervention', function($timeout, TabContainer, FiltersFactory, user, ContextMenu, LxProgressService, LxNotificationService, edisonAPI, DataProvider, $routeParams, $location, $rootScope, $filter, config, ngTableParams, MomentIterator, openPost) {
    "use strict";
    var arg = arguments;
    return {
        replace: false,
        restrict: 'E',
		templateUrl: function(){

	    	if (arg[9].fltr === "SAV")
				var Temp = '/Templates/lineup-sav.html';
	    	else if (arg[9].fltr === "litst")
				var Temp = '/Templates/lineup-litstatus.html';
			else if (arg[9].fltr === "recouvrementArt" || arg[9].fltr === "recouvrementArtn")
				var Temp = '/Templates/lineup-recouvrement.html';
			else if (arg[9].fltr === "recLitClos" || arg[9].fltr === "recLitNonPris" || arg[9].fltr === "recLitPris"
				|| arg[9].fltr === "recRelLit" || arg[9].fltr === "recLitAutres" || arg[9].fltr === "recLitOpposition"
				|| arg[9].fltr === "recLitRisqueOpposition" || arg[9].fltr === "recLitQualite"
				|| arg[9].fltr === "recLitTarif" ||  arg[9].fltr === "recLitPlaintes" || arg[9].fltr === "relRec"
				|| arg[9].fltr === "recClientClos"|| arg[9].fltr === "recClientNonPris" || arg[9].fltr === "recClientPris"
				|| arg[9].fltr === "recPasDeContact" || arg[9].fltr === "recPlaintes" || arg[9].fltr === "recFactureNonRecu"
				|| arg[9].fltr === "recRetEnvoi" || arg[9].fltr === "recImpaye" || arg[9].fltr === "recChequePerdu"
				|| arg[9].fltr === "recTarif" || arg[9].fltr === "recQualite" || arg[9].fltr === "recAutres"
        || arg[9].fltr === "recClientEncaiss"|| arg[9].fltr === "recLitigeEncaiss"
        || arg[9].fltr === "recClientEncaiss1"|| arg[9].fltr === "recLitigeEncaiss1"
        || arg[9].fltr === "recClientEncaiss2"|| arg[9].fltr === "recLitigeEncaiss2"
        || arg[9].fltr === "recClientEncaiss3"|| arg[9].fltr === "recLitigeEncaiss3"
        || arg[9].fltr === "recClientEncaiss4"|| arg[9].fltr === "recLitigeEncaiss4"
        || arg[9].fltr === "recClientEncaiss5"|| arg[9].fltr === "recLitigeEncaiss5"
        || arg[9].fltr === "recClientEncaiss6"|| arg[9].fltr === "recLitigeEncaiss6"
        || arg[9].fltr === "recClientEncaiss7"|| arg[9].fltr === "recLitigeEncaiss7"
        || arg[9].fltr === "recClientEncaiss8"|| arg[9].fltr === "recLitigeEncaiss8"
        || arg[9].fltr === "recClientEncaiss9"|| arg[9].fltr === "recLitigeEncaiss9"
        || arg[9].fltr === "recClientEncaiss10"|| arg[9].fltr === "recLitigeEncaiss10"
        || arg[9].fltr === "recClientEncaiss11"|| arg[9].fltr === "recLitigeEncaiss11"
        || arg[9].fltr === "recClientEncaiss12"|| arg[9].fltr === "recLitigeEncaiss12"
      )
				var Temp = '/Templates/lineup-recouvrementClient.html';
			else if (arg[9].fltr === "attenteVerif")
				var Temp = '/Templates/lineup-attenteVerif.html';
	    	else
				var Temp = '/Templates/lineup-intervention.html';
	    	return Temp
		},
        scope: {
            limit: '=',
            embedded: '=',
            filter: '=',
        },
        controller: function($scope) {
            $scope.model = 'intervention'
            Controller.apply($scope, arg)
        }
    }
});

angular.module('edison').directive('lineupDevis', function($timeout, TabContainer, FiltersFactory, user, ContextMenu, LxProgressService, LxNotificationService, edisonAPI, DataProvider, $routeParams, $location, $rootScope, $filter, config, ngTableParams, MomentIterator, openPost) {
    "use strict";
    var arg = arguments;
    return {
        replace: false,
        restrict: 'E',
        templateUrl: '/Templates/lineup-devis.html',
        scope: {
            filter: '=',
        },
        controller: function($scope) {
            $scope.model = 'devis'
            Controller.apply($scope, arg)
        }
    }
});

angular.module('edison').directive('lineupArtisan', function($timeout, TabContainer, FiltersFactory, user, ContextMenu, LxProgressService, LxNotificationService, edisonAPI, DataProvider, $routeParams, $location, $rootScope, $filter, config, ngTableParams, MomentIterator, openPost) {
    "use strict";
    var arg = arguments;
    return {
        replace: false,
        restrict: 'E',
        templateUrl: function(){
	    if (arg[9].fltr === "needFacturier" || arg[9].fltr === "histEnvoi")
		var Temp = '/Templates/lineup-facturier.html'
	    else
		var Temp = '/Templates/lineup-artisan.html'
	    return Temp
	},
        scope: {
	    filter: '=',
        },
        controller: function($scope) {
            $scope.model = 'artisan'
            Controller.apply($scope, arg)
        }
    }
 });

angular.module('edison').directive('lineupDemarchage', function($timeout, TabContainer, FiltersFactory, user, ContextMenu, LxProgressService, LxNotificationService, edisonAPI, DataProvider, $routeParams, $location, $rootScope, $filter, config, ngTableParams, MomentIterator, openPost) {
    "use strict";
    var arg = arguments;
    return {
        replace: false,
        restrict: 'E',
        templateUrl: '/Templates/lineup-demarchage.html',
        scope: {
            filter: '=',
        },
        controller: function($scope) {
            $scope.model = 'demarchage'
            Controller.apply($scope, arg)
        }
    }
});
