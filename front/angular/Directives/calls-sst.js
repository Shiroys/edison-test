angular.module('edison').directive('callsSst', function(edisonAPI) {
    "use strict";

    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/Templates/calls-sst.html',
        scope: {
            data: "=",
            exit: '&',
	    ispro: '@'
        },
        link: function(scope, element, attrs) {
            var reload = function() {
                if (!scope.data || !scope.data.id) {
                    return 0;
                }
		if (scope.ispro && scope.ispro === "true")
		{
		    edisonAPI.prospect.callHistory(scope.data.id).then(function(resp) {
			scope.hist = resp.data;
		    })
		}
		else
		{
                    edisonAPI.artisan.callHistory(scope.data.id).then(function(resp) {
			scope.hist = resp.data;
                    })
		}
            }

	    scope.$watch('data.calls', reload);
            scope.$watch('data.id', reload)
            scope.checkCall = function(sign) {
                var state = !sign.ok;
		if (scope.ispro && scope.ispro === "true")
		{
		    edisonAPI.prospect.checkCall(sign.id_artisan, sign._id, sign.text, state).then(function(resp) {
			var data;
			var n;
			for (n in resp.data.calls)
			{
                            if (resp.data.calls[n]._id == sign._id)
				data = resp.data.calls[n];
			}
			sign = _.merge(sign, data);
			reload();
                    })
                    scope.exit && scope.exit();
		}
		else
		{
                    edisonAPI.artisan.checkCall(sign.id_artisan, sign._id, sign.text, state).then(function(resp) {
			var data;
			var n;
			for (n in resp.data.calls)
			{
                            if (resp.data.calls[n]._id == sign._id)
				data = resp.data.calls[n];
			}
			sign = _.merge(sign, data);
			reload();
                    })
                    scope.exit && scope.exit();
		}
            }

        }
    };
});
