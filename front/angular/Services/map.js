angular.module('edison').factory('Map', function() {
    "use strict";

    var Map = function() {
        this.display = false;
    }

    Map.prototype.setCenter = function(address) {
        var myLatLng = new google.maps.LatLng(address.lt, address.lg);
        this.center = address;
        if (window.map)
            window.map.setCenter(myLatLng)
    }

    Map.prototype.setZoom = function(value) {
       // throw new Error('lol')
         if (window.map)
            window.map.setZoom(value)
        this.zoom = value
    }
    Map.prototype.show = function() {
        this.display = true;
    }

    var circleList = [];
    var cate = -1;

    Map.prototype.setPoly = function (circles) {
	if (window.map)
	{
	    circles.setMap(window.map);
	    circleList = circleList.concat(circles);
	}
    }

    Map.prototype.clearPoly = function (cat) {
	if (window.map)
	{
	    if (cate == -1)
		cate = cat
	    else if (cate != cat)
	    {
		for (var n = 0; n < circleList.length; n++)
		    circleList[n].setMap(null);
		circleList = [];
		cate = cat;
	    }
	}
    }
    
    return Map;
});
