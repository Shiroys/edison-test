angular.module('edison').factory('socket', function(socketFactory) {
	"use strict";
	return socketFactory({
		ioSocket: io.connect(location.protocol + "//" + location.hostname + ':1995')
	});
});
