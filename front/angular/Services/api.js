angular.module('edison').factory('edisonAPI', ['$http', '$location', 'Upload', function($http, $location, Upload) {
    "use strict";
    return {
	bug: {
	    declare: function(params) {
		return $http.post('/api/bug/declare', params)
	    },
	},
	raison: {
		saveRaison: function(params) {
			return $http.post('/api/raison/saveRaison', params);
		},
		list: function() {
		return $http.get('/api/raison/list');
	    },
	},
	fournisseur: {
		saveFournisseur: function(params) {
			return $http.post('/api/fournisseur/saveFournisseur', params);
		},
		list: function() {
		return $http.get('/api/fournisseur/list');
	    },
	},
	demarchage: {
        add: function(params) {
		return $http.post('/api/demarchage/add', params)
            },
	    map: function(params) {
		return $http.post('/api/demarchage/map', params);
	    },
	    validate: function(params) {
		return $http.post('/api/demarchage/validate', params);
	    },
	    validedemarche: function(params) {
		return $http.post('/api/demarchage/validedemarche', params);
	    },
        list: function() {
		return $http.get('/api/demarchage/list');
            },
        remove: function(id) {
		return $http.post('/api/demarchage/remove', {
            id: id,
		});
            },
	},
	marker: {
	    list: function (cat) {
		return $http.get('/api/marker/list', {
		    params: cat
		});
	    }
	},
	user: {
	    history: function(login) {
		return $http.get('/api/user/' + login + '/history')
	    },
	    loginuser: function(data) {
		return $http.get('/api/user/loginuser', data)
	    },
	},
	product: {
	    list: function() {
		return $http.get('/api/product/list');
	    },
	    save: function(data) {
		return $http.post('/api/product/__save', data);
	    }
	},
	signal: {
		saveDest: function(data) {
		return $http.post('/api/signal/saveDest', data);
	    },
	    list: function() {
		return $http.get('/api/signal/list');
	    },
	    update: function(data) {
		return $http.post('/api/signal/update', data);
	    },
	    save: function(data) {
		return $http.post('/api/signal/__save', data);
	    }
	},
	compte: {
	    list: function() {
		return $http.get('/api/compte/list');
	    },
	    save: function(data) {
		return $http.post('/api/compte/__save', data);
	    }
	},
	combo: {
	    list: function() {
		return $http.get('/api/combo/list');
	    },
	    save: function(data) {
		return $http.post('/api/combo/__save', data);
	    }
	},
	stats: {
	    telepro: function() {
		return $http.get('/api/stats/telepro');
	    },
	    day: function() {
		return $http.get('/api/stats/day');
	    }
	},
	users: {
	    logout: function() {
		return $http.get('/logout');
	    },
	    save: function(data) {
		return $http.post('/api/user/__save', data);
	    },
	    list: function() {
		return $http.get('/api/user/list');
	    },
	    upload: function(data) {
		return $http.post('/api/user/upload', data)
	    },
	    cancel: function (data) {
		return $http.post('/api/user/cancel', data)
	    }
	},
	bfm: {
	    get: function() {
		return $http.get('/api/bfm');
	    }
	},
	compta: {
	    getCA: function (year) {
			return $http.get('/api/intervention/getCA', {
			    params: year
			});
	    },
	    remise: function(data) {
			return $http.get('/api/intervention/remise?d=' + (data.d ||  ''));
	    },
	    lpa: function(data) {
			return $http.get('/api/intervention/lpa?d=' + (data.d ||  ''));
	    },
	    getFourniture: function(year) {
			return $http.get('/api/intervention/getFourniture', {
			    params: year
			});
	    },
	    flush: function(data) {
		return $http.post('/api/intervention/flush', data);
	    },
	    flushMail: function(data) {
		return $http.post('/api/intervention/flushMail', data);
	    },
	    listRemises: function() {
		return $http.get('/api/remise/listRemises');
	    },
	    addCheques: function() {
		return $http.get('/api/remise/addCheques');
	    },
	    validateRemise: function() {
		return $http.get('/api/remise/validateRemise');
	    },
	    archivesPaiement: function() {
		return $http.get('/api/intervention/archivePaiement');
	    },
	    archivesReglement: function() {
		return $http.get('/api/intervention/archiveReglement');
	    },
	    avoirs: function() {
		return $http.get('/api/intervention/avoirs')
	    },
	    flushAvoirs: function(data) {
		return $http.post('/api/intervention/flushAvoirs', data);
	    },

	},
	devis: {
		relanceAuto7h: function() {
		return $http.post('/api/devis/relanceAuto7h');
		},
	    getInfo: function (user) {
		return $http.get('/api/devis/getInfo', {
		    params: user
		});
	    },
	    saveTmp: function(data) {
		return $http.post('/api/devis/temporarySaving', data);
	    },
	    getTmp: function(id) {
		return $http.get('/api/devis/temporarySaving?id=' + id);
	    },
	    get: function(id, options) {
		return $http({
		    method: 'GET',
		    cache: false,
		    url: '/api/devis/' + id,
		    params: options || {}
		}).success(function(result) {
		    return result;
		});
	    },
	    save: function(params) {
		if (!params.id) {
		    return $http.post("/api/devis", params)
		} else {
		    return $http.post("/api/devis/" + params.id, params)

		}
	    },
	    envoi: function(id, options) {
		return $http.post("/api/devis/" + id + "/envoi", options);
	    },
	    annulation: function(id, causeAnnulation) {
		return $http.post("/api/devis/" + id + "/annulation");
	    },
	    list: function() {
		return $http.get('api/devis/getCacheList')
	    },
	},
	log: {
	    getWeekLog: function(options) {
		return $http.get('/api/log/getWeekLog', {
		    params: options
		});
	    },
	    getLate: function(options) {
		return $http.get('/api/log/getLate', {
		    params: options
		});
	    },
	},
	ovh: {
	    getAll: function() {
		return $http.get('/api/callovh/getAll');
	    },
	    getNumber: function(options) {
		return $http.get('/api/callovh/getNumber', {
		    params: options
		});
	    },
	    getSingle: function(options) {
		return $http.get('/api/callovh/getSingle', {
		    params: options
		});
	    },
	    getStats: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/ovh/ovhStats",
		    params: options
		});
	    },
	    getInfo: function (obj) {
		return $http.get('/api/ovh/getInfo', {
		    params: obj
		})
	    },
	    getBill: function (obj) {
		return $http.get('/api/ovh/getBill', {
		    params: obj
		})
	    },
	    getAccounts: function () {
		return $http.get('/api/ovh/getAccounts')
	    },
	    getPhone: function () {
		return $http.get('/api/ovh/getPhone')
	    },
	    getComptes: function (obj) {
		return $http.post('/api/ovh/ovhAccount', {
		    params: {
			dayStart: obj.d,
			type: obj.t,
			account: obj.a,
			line: obj.n
		    }
		})
	    },
	    demStats: function (obj) {
		return $http.post('/api/ovh/ovhDemStats', {
		    params: {
			dayStart: obj.d,
			type: obj.t,
			account: obj.a,
			line: obj.n
		    }
		})
	    },
	    getInterStatus: function (obj) {
		return $http.get('/api/ovh/interStatus', {
		    params: {
			dayStart: obj.d,
			type: obj.t,
			account: obj.a,
			line: obj.n,
			line2: obj.l
		    }
		})
	    },
	},
	intervention: {
		statsRecou: function(options) {
			return $http({
				method: 'GET',
				url: "/api/intervention/statsRecou",
				params: options
			});
		},
		saveEncaissement: function(params) {
			return $http.post('/api/intervention/saveEncaissement', params);
		},
		saveDecaissement: function(params) {
			return $http.post('/api/intervention/saveDecaissement', params);
		},
		comptaDemande: function() {
			return $http.post('/api/intervention/comptaDemande');
		},
		switchTest: function(params) {
			return $http.post('/api/intervention/switchTest', params);
		},
		getDatasOnFactureAndClientNames: function() {
			return $http.get('/api/intervention/getDatasOnFactureAndClientNames');
		},
		getDataOneInter: function(params) {
			return $http.post('/api/intervention/getDataOneInter', params);
		},
		demandeSav: function(params) {
			return $http.post('/api/intervention/demandeSav', params);
		},
		remiseCom: function(params) {
			return $http.post('/api/intervention/remiseCom', params);
		},
		lettreDesist: function(params) {
			return $http.post('/api/intervention/lettreDesist', params);
		},
		lettreOppo: function(params) {
			return $http.post('/api/intervention/lettreOppo', params);
		},
		protAcc: function(params) {
			return $http.post('/api/intervention/protAcc', params);
		},
		injTribCom: function(params) {
			return $http.post('/api/intervention/injTribCom', params);
		},
		injTribProx: function(params) {
			return $http.post('/api/intervention/injTribProx', params);
		},
		demeure: function(params) {
			return $http.post('/api/intervention/demeure', params);
		},
		envRec: function(params) {
			return $http.post('/api/intervention/envRec', params);
		},
		priseEnCharge: function(params) {
			return $http.post('/api/intervention/priseEnCharge', params);
		},
		resetRec: function(params) {
			return $http.post('/api/intervention/resetRec', params);
		},
		saveRec: function(params) {
			return $http.post('/api/intervention/saveRec', params);
		},
		createLitige: function(params) {
			return $http.post('/api/intervention/createLitige', params);
		},
		getRaisonData: function() {
			return $http.get('/api/intervention/getRaisonData');
		},
		getFournisseurData: function() {
			return $http.get('/api/intervention/getFournisseurData');
		},
		verifInter: function(params) {
			return $http.post('/api/intervention/verifInter', params);
		},
		updateVerifInter: function(params) {
			return $http.post('/api/intervention/updateVerifInter', params);
		},
		updateRefusInter: function(params) {
			return $http.post('/api/intervention/updateRefusInter', params);
		},
		sendRefus: function(params) {
			return $http.post('/api/intervention/sendRefus', params);
		},
		listInter: function(params) {
			return $http.post('/api/intervention/listInter', params);
		},
		mailAverif: function() {
			return $http.post('/api/intervention/mailAverif');
		},
		flushTrue: function() {
			return $http.post('/api/intervention/flushTrue');
		},
		transSav: function(params) {
			return $http.post('/api/intervention/transSav', params);
		},
		reloadCache: function() {
			return $http.post('/api/intervention/reloadCache');
		},
	    litigeInjonc: function(id){
		return $http.post('api/intervention/' + id + '/litigeInjonc')
	    },
	    printQ: function(id, check){
		return $http.post('api/intervention/' + id + '/printQ', check)
	    },
	    relanceAuto : function(){
			return $http.post('api/intervention/relanceAuto');
	    },
	    relanceAll : function(){
			return $http.post('api/intervention/relanceAll');
	    },
	    getLitiges : function(){
		return $http.post('api/intervention/getLitiges');
	    },
	    CountRecu: function (params){
		return $http.post('/api/intervention/CountRecu', params);
	    },
	    ComptaStats: function (params){
		return $http.post('/api/intervention/ComptaStats', params);
	    },
	    CountFacture: function (params){
		return $http.post('/api/intervention/CountFacture', params);
	    },
	    canGetFacturier: function (params){
		return $http.post('/api/intervention/canGetFacturier', params);
	    },
	    getSeuilMaxArtisan: function(params){
		return $http.post('/api/intervention/getSeuilMaxArtisan', params);
	    },
	    getSeuilRuptArtisan: function(params) {
		return $http.post('/api/intervention/getSeuilRuptArtisan', params);
	    },
	    checklimit: function(params){
		return $http.post('/api/intervention/checklimit', params);
	    },
	    validatefourn: function(params) {
		return $http.post('/api/intervention/validatefourn', params);
	    },
	    ptStats: function(options) {
		return $http.get('/api/intervention/ptStats', {
		    params: options
		});
	    },	    
	    dashboardStats: function(options) {
		return $http.get('/api/intervention/dashboardStats', {
		    params: options
		});
	    },
	    GetDayWeek: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/GetDayWeek",
		    params: options
		}); 
	    },
	    GetStatUser: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/GetStatUser",
		    params: options
		});
	    },
	    getHourlyStat: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/getHourlyStat",
		    params: options
		});
	    },
	    saveGrandsComptes: function () {
		return $http.post('/api/intervention/saveGrandsComptes');
	    },
	    getTelMatch: function(text) {
		return $http.post('/api/intervention/telMatches', text);
	    },
	    saveTmp: function(data) {
		return $http.post('/api/intervention/temporarySaving', data);
	    },
	    getTmp: function(id) {
		return $http.get('/api/intervention/temporarySaving?id=' + id);
	    },
	    demarcher: function(id) {
		return $http({
		    method: 'POST',
		    url: '/api/intervention/' + id + '/demarcher'
		})
	    },
	    favoris: function(params) {
		return $http.post('/api/intervention/favoris', params);
	    },
	    priseEnCharge: function(params) {
		return $http.post('/api/intervention/priseEnCharge', params);
	    },
	    reactivation: function(id) {
		return $http.post('api/intervention/' + id + '/reactivation')
	    },
	    list: function() {
		return $http.get('api/intervention/getCacheList')
	    },
	    get: function(id, options) {
		return $http({
		    method: 'GET',
		    cache: false,
		    url: '/api/intervention/' + id,
		    params: options || {}
		}).success(function(result) {
		    return result;
		});
	    },
	    getCB: function(id) {
		return $http.get("/api/intervention/" + id + "/CB");
	    },
	    save: function(params) {
		if (!params.id) {
		    return $http.post("/api/intervention", params)
		} else {
		    return $http.post("/api/intervention/" + params.id, params)

		}
	    },
	    getFiles: function(id) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/" + id + "/getFiles"
		});
	    },
	    verification: function(id, options) {
		return $http.post("/api/intervention/" + id + "/verification", options);
	    },
	    annulation: function(id, options) {
		return $http.post("/api/intervention/" + id + "/annulation", options);
	    },
	    envoi: function(id, options) {
		return $http.post("/api/intervention/" + id + "/envoi", options);
	    },
	    sendFacture: function(id, options) {
		return $http.post("/api/intervention/" + id + "/sendFacture", options);
	    },
	    sendFactureAcquitte: function(id, options) {
		return $http.post("/api/intervention/" + id + "/sendFactureAcquitte", options);
	    },
	    statsBen: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/statsBen",
		    params: options
		});
	    },
	    gstats: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/gstats",
		    params: options
		});
	    },
	    statsBenYear: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/statsBenYearly",
		    params: options
		});
	    },
	    commissions: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/intervention/commissions",
		    params: options
		});
	    },
	    scan: function(id) {
		return $http.post("/api/intervention/" + id + "/scan");
	    }
	},
	prospect: {
	    checkDuplicates: function (elems) {
		return $http.get('/api/prospect/checkDuplicates', {
		    params: {
			q: JSON.stringify({
			    list: elems
			})
		    }
		})
	    },
	    geoloc: function(options) {
		return $http.post('/api/prospect/geoloc');
	    },
	    geolocNotBDD: function(options) {
		return $http.post('/api/prospect/geolocNotBDD', {
		    params: {
			q: JSON.stringify({
			    list: options
			})
		    }
		});
	    },
	    Merge: function(options) {
		return $http.post('/api/prospect/Merge');
	    },
	    CP: function(options) {
		return $http.post('/api/prospect/CP');
	    },
	    getNearest: function(address, categorie) {
		return $http({
		    method: 'GET',
		    url: "/api/prospect/rank",
		    cache: false,
		    params: {
			categorie: categorie,
			lat: address.lt,
			lng: address.lg,
			limit: 150,
			maxDistance: 30
		    }
		});
	    },
	    save: function(params) {
		if (!params.id) {
		    return $http.post("/api/prospect", params)
		} else {
		    return $http.post("/api/prospect/" + params.id, params)
		}
	    },
	    get: function(id, options) {
		return $http({
		    method: 'GET',
		    cache: false,
		    url: '/api/prospect/' + id,
		    params: options || {}
		}).success(function(result) {
		    return result;
		});
	    },
	    comment: function(id, text) {
		return $http.post('/api/prospect/' + id + '/comment', {
		    text: text
		})
	    },
	    remove: function(id) {
		return $http.post('api/prospect/' + id + '/removePro')
	    },
	    callLog: function (id) {
		return $http.post('/api/prospect/' + id + '/callLog')
            },
	    checkCall: function(id_artisan, id, text, state) {
		return $http.post('/api/prospect/' + id_artisan + '/checkCall', {
                    text: text,
                    sst: id,
                    state: state
		})
            },
	    callHistory: function(id) {
		return $http.get('/api/prospect/' + id + '/callHistory');
            },
	    getStats: function(id_sst) {
		return $http({
		    method: 'GET',
		    url: "/api/prospect/" + id_sst + "/stats"
		});
	    },
	},
	artisan: {
		changeNumber: function() {
			return $http.get('/api/artisan/changeNumber');
		},
		checkExist: function (params) {
			return $http.post('/api/artisan/checkExist',params);
	    },
	    sendIns: function (id, options) {
		return $http.post('/api/artisan/' + id + '/sendIns', {
		    params: options
		});
	    },
	    CheckFacturier: function (id,params) {
		return $http.post('/api/artisan/'+ id +'/CheckFacturier',params);
	    },
	    validateArt: function () {
		return $http.get('/api/validateArt');
	    },
	    addIBAN: function (data) {
		return $http.post('/api/artisan/addIBAN', data);
	    },
	    pdfcom: function(data) {
		return $http.get('/api/artisan/pdfcom', {
		    params: data
		});
	    },
	    starList: function() {
		return $http.get('/api/artisan/starList');
	    },
	    ptStats: function(options) {
		return $http.get('/api/artisan/ptStats', {
		    params: options
		});
	    },	    
	    exist: function(email, num) {
		return $http({
		    method: 'GET',
		    cache: false,
		    url: '/api/artisan/exist',
		    params: {
			email: email,
			num: num
		    }
		});
	    },
	    tableauCom: function() {
		return $http.get('/api/artisan/getStep');
	    },
	    filesCom: function(date) {
		return $http.get('/api/artisan/getComissionFiles');
	    },
	    manage: function(id) {
		return $http.post('/api/artisan/' + id + '/manage')
	    },
	    activite: function(id, text) {
		return $http.post('/api/artisan/' + id + '/activite', {
		    text: text
		})
	    },
	    comment: function(id, text) {
		return $http.post('/api/artisan/' + id + '/comment', {
		    text: text
		})
	    },
	    absence: function(id, absence) {
		return $http.post('/api/artisan/' + id + '/absence', absence)
	    },
	    needFacturier: function(id) {
		return $http.post('/api/artisan/' + id + '/needFacturier')
	    },
	    sendFacturier: function(id, facturier, deviseur, tshirt) {
		return $http.post('/api/artisan/' + id + '/sendFacturier', {
		    facturier: facturier,
		    deviseur: deviseur,
			tshirt: tshirt,
		});
	    },
	    saveTmp: function(data) {
		return $http.post('/api/artisan/temporarySaving', data);
	    },
            callLog: function (id) {
		return $http.post('/api/artisan/' + id + '/callLog')
            },
            callHistory: function(id) {
		return $http.get('/api/artisan/' + id + '/callHistory');
            },
	    ActiviteHistory: function(id) {
		return $http.get('/api/artisan/' + id + '/ActiviteHistory');
	    },
	    fullHistory: function(id) {
		return $http.get('/api/artisan/' + id + '/fullHistory');
	    },
        checkCall: function(id_artisan, id, text, state) {
		return $http.post('/api/artisan/' + id_artisan + '/checkCall', {
                    text: text,
                    sst: id,
                    state: state
		})
        },
	    getTmp: function(id) {
		return $http.get('/api/artisan/temporarySaving?id=' + id);
	    },
	    getCompteTiers: function(id_sst) {
		return $http.get(['/api/artisan', id_sst, 'compteTiers'].join('/'));
	    },
	    envoiContrat: function(id, options) {
		return $http.post("/api/artisan/" + id + '/sendContrat', options)
	    },
	    upload: function(file, name, id) {
		return Upload.upload({
		    url: '/api/artisan/' + id + "/upload",
		    fields: {
			name: name,
			id: id
		    },
		    file: file
		})
	    },
	    getFiles: function(id) {
		return $http({
		    method: 'GET',
		    url: "/api/artisan/" + id + "/getFiles"
		});
	    },
	    extendedStats: function(id) {
		return $http.get('/api/artisan/' + id + "/extendedStats")
	    },
	    statsMonths: function(id) {
		return $http.get('/api/artisan/' + id + "/statsMonths")
	    },
	    save: function(params) {
		if (!params.id) {
		    return $http.post("/api/artisan", params)
		} else {
		    return $http.post("/api/artisan/" + params.id, params)

		}
	    },
	    list: function(options) {
		return $http.get('api/artisan/getCacheList')
	    },
	    lastInters: function(id) {
		return $http({
		    method: 'GET',
		    url: '/api/artisan/' + id + '/lastInters',
		})
	    },
	    get: function(id, options) {
		return $http({
		    method: 'GET',
		    cache: false,
		    url: '/api/artisan/' + id,
		    params: options || {}
		}).success(function(result) {
		    return result;
		});
	    },
	    getNearest: function(address, categorie) {
		return $http({
		    method: 'GET',
		    url: "/api/artisan/rank",
		    cache: false,
		    params: {
			categorie: categorie,
			lat: address.lt,
			lng: address.lg,
			limit: 150,
			maxDistance: 100
		    }
		});
	    },
	    datefacturier: function(options) {
		return $http({
		    method: 'GET',
		    url: "/api/artisan/datefacturier",
		    params: options
		});
	    },
	    getStats: function(id_sst) {
		return $http({
		    method: 'GET',
		    url: "/api/artisan/" + id_sst + "/stats"
		});
	    },
	},
	tasklist: {
	    get: function(date, login) {
		return $http.get('/api/tasklist/' + date + '/' + login);
	    },
	    check: function(listid, taskid) {
		return $http.post('/api/tasklist/' + listid + '/check/' + taskid);
	    },
	    update: function(task) {
		return $http.post('/api/tasklist/', {
		    task: task
		});
	    }
	},
	task: {
	    add: function(params) {
		return $http.post('/api/task/add', params)
	    },
	    check: function(id) {
		return $http.post('/api/task/' + id + '/check')
	    },
	    listRelevant: function(options) {
		return $http.get('/api/task/relevant', {
		    params: options
		});
	    }
	},
	signalement: {
		transfo: function(params) {
		return $http.post('/api/signalement/transfo', params)
		},
		resolve: function(params) {
		return $http.post('/api/signalement/resolve', params)
		},
	    check: function(id, text) {
		return $http.post('/api/signalement/' + id + '/check', {
		    text: text
		})
	    },
	    add: function(params) {
		return $http.post('/api/signalement/add', params)
	    },
	    list: function(params) {
		return $http.get('/api/signalement/list', {
		    params: params
		})
	    },
	    stats: function() {
		return $http.get('/api/signalement/stats')
	    }
	},
	file: {
	    uploadScans: function(file, options) {
		return Upload.upload({
		    url: '/api/document/uploadScans',
		    fields: options,
		    file: file
		})
	    },
	    upload: function(file, options) {
		return Upload.upload({
		    url: '/api/document/upload',
		    fields: options,
		    file: file
		})
	    },
	    scan: function(options) {
		return $http.post('/api/document/scan', options);
	    }
	},
	history: {
	    add: function(params) {
		return $http.post('/api/history/add', params);
	    },
	    list: function() {
		return $http.get('/api/history/listAll', {})
	    },
	},
	call: {
	    get: function(origin, link) {
		return $http({
		    method: 'GET',
		    url: '/api/calls/get',
		    params: {
			q: JSON.stringify({
			    link: link
			})
		    }
		})
	    },
	    save: function(params) {
		return $http.post('/api/calls', params);
	    },
	},
	sms: {
	    get: function(origin, link) {
		return $http({
		    method: 'GET',
		    url: '/api/sms/get',
		    params: {
			q: JSON.stringify({
			    link: link,
			    origin: origin
			})
		    }
		})
	    },
	    send: function(params) {
		return $http.post("/api/sms/send", params)
	    },

	},
	getDistance: function(origin, destination) {
	    return $http({
		method: 'GET',
		cache: true,
		url: '/api/mapGetDistance',
		params: {
		    origin: origin,
		    destination: destination
		}
	    })
	},
	request: function(options) {
	    return $http({
		method: options.method || 'GET',
		url: '/api/' + options.fn,
		params: options.data
	    });
	},
	searchPhone: function(tel) {
	    return $http({
		method: 'GET',
		url: "api/arcep/" + tel + "/search"
	    });
	},
	getUser: function() {
	    return $http({
		method: 'GET',
		cache: true,
		url: "/api/whoAmI"
	    });
	},
	searchText: function(text, options) {
	    return $http({
		method: 'GET',
		params: options,
		url: ['api', 'search', text].join('/')
	    })
	},
	bigSearch: function(text, options) {
	    return $http({
		method: 'GET',
		params: options,
		url: ['api', 'bigSearch', text].join('/')
	    })
	}
    }
}]);
