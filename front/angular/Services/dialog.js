angular.module('edison').factory('dialog', function(openPost, $mdDialog, $rootScope, edisonAPI, config, $window, $filter, LxNotificationService) {
    "use strict";

    return {
	callArtisan: function (data, cb) {
	    $mdDialog.show({
		controller: function($scope, $mdDialog, config)
		{
		    $scope.data = data
		    $scope.tel1 = true;
		    $scope.tel2 = false;
		    $scope.unselect = function (nb) {
			if (nb == 1)
			{
			    $scope.tel1 = false;
			    $scope.tel2 = true;
			}
			else
			{
			    $scope.tel1 = true;
			    $scope.tel2 = false;
			}
		    }
		    $scope.answer = function(cancel) {
			$mdDialog.hide();
			if (!cancel) {
			    if ($scope.tel1 == true)
				return cb($scope.data.tel1);
			    else
				return cb($scope.data.tel2);
			}
		    }
		},
		templateUrl: '/DialogTemplates/callArtisan.html',
	    })
	},

	cancelLate: function (data, cb) {
	    $mdDialog.show({
		controller: function($scope, $mdDialog, config)
		{
		    $scope.data = data
		    $scope.answer = function(cancel) {
			$mdDialog.hide();
			if (!cancel) {
			    return cb($scope.data);
			}
		    }
		},
		templateUrl: '/DialogTemplates/cancelLate.html',
	    });	
	},

	
	changeImage: function(link, cb) {
	    $mdDialog.show({
		controller: function($scope, $mdDialog, config)
		{
		    $scope.link = link;
		    $scope.answer = function(cancel) {
			$mdDialog.hide();
			if (!cancel) {
			    return cb($scope.link);
			}
		    }
		},
		templateUrl: '/DialogTemplates/changeImage.html',
	    });	
	},
	
	applyComment: function (obj, cb) {
	    $mdDialog.show({
		controller: function($scope, $mdDialog, config)
		{
		    $scope.history = obj.comments;
		    $scope.answer = function(cancel) {
			$mdDialog.hide();
			if (!cancel) {
			    return cb($scope.comment);
			}
		    }
		},
		templateUrl: '/DialogTemplates/comment.html',
	    });
	},
	
	prospectCall: function(elems, cb) {
	    $mdDialog.show({
		controller: function($scope, $mdDialog, config)
		{
		    $scope.statutPro = elems.statutProspect;
		    $scope.config = config;
		    $scope.answer = function(cancel) {
			$mdDialog.hide();
			if (!cancel) {
			    return cb($scope.statutPro);
			}
		    }
		},
		templateUrl: '/DialogTemplates/prospectCall.html',
	    });
	},
	
        declareBug: function(tabs, cb) {
            $mdDialog.show({
                controller: function($scope, $mdDialog, config) {
                    $scope.resp = {
                        location: tabs.getCurrentTab().path,
                    }
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel) {
                            return cb(null, $scope.resp)
                        }
                    }
                },
                templateUrl: '/DialogTemplates/declare-bug.html',
            });
        },
		/* Fonction pour la pop up de la résolution */
		pop_up_showDest_raison : function(signal, typeT, cb)
		{
			$mdDialog.show({
				controller: function DialogController($scope, $mdDialog) {
					$scope.signal = signal;
					$scope.typeT = typeT;
					if (signal.text && typeT == 2)
						$scope.resolve = signal.text;
					if (typeT == 0)
					{
						edisonAPI.users.list().then(function(resp) {
							$scope.showDest = [];
							for (var ind = 0; ind < resp.data.length; ind++)
							{
								for (var index = 0; index < signal.arrayDest.length; index++)
								{
									if (resp.data[ind].email == signal.arrayDest[index])
                                    {
                                        var check = 0;
                                        for (var i = 0; i < $scope.showDest.length; i++)
                                        {
                                            if ($scope.showDest[i] == resp.data[ind].login)
                                                check++;
                                        }
                                        if (check == 0)
                                            $scope.showDest.push(resp.data[ind].login);
                                    }
								}
							}
							$scope.showDest = $filter('orderBy')($scope.showDest, 'login');
						})
					}
					$scope.answer = function(cancel, resolve) {
						$mdDialog.hide();
						if (typeT == 1 && cancel === true && resolve)
						{
							return cb (resolve, cancel);
						}
					}
				},
				templateUrl: '/DialogTemplates/pop_up_showDest_raison.html',
			});
		},
		/* Fonction pour la pop up des destinataires dans Outils->Signalements */
		pop_up_dest: function(signal, indPl, cb)
		{
			$mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
					edisonAPI.users.list().then(function(resp) {
                        resp.data = $filter('orderBy')(resp.data, 'login');
                        $scope.signal = signal;
                        if (!$scope.signal.dest)
                            $scope.signal.dest = {};
						if (!$scope.signal.dest.special)
							$scope.signal.dest.special = {};
						if (!$scope.signal.dest.service)
							$scope.signal.dest.service = {};
                        if (!$scope.signal.dest.users)
                            $scope.signal.dest.users = resp.data;
                        else
                        {
                            edisonAPI.signal.update({ users: resp.data, signal: $scope.signal }).then(function(updatedSignal) {
                                $scope.signal = updatedSignal.data;
                            })
                        }
		    			$scope.answer = function(cancel) {
							$mdDialog.hide();
							if (cancel === true)
							{
								edisonAPI.signal.saveDest({ nom: $scope.signal.nom, signal: $scope.signal }).then(function(resp) {
                                    return cb (resp, cancel);
                                })
							}
                            else
                                return cb (null, cancel);
						}
					});
		    	},
                templateUrl: '/DialogTemplates/pop_up_dest.html',
            });

		},
	/* Fonction pour la pop up signalement/commentaires dans une intervention */	
		pop_up_level2_comment: function(nom, cb)
        {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
		    		$scope.nomA = nom
					$scope.answer = function(cancel) {
						$mdDialog.hide();
                     	if (cancel && $scope.commentaire){
						}
							return cb ($scope,cancel);
		    		}
                },
                templateUrl: '/DialogTemplates/pop_up_level2.html',
            });
        },

    	getPassword: function(page, cb) {
            $mdDialog.show({
                controller: function($scope, $mdDialog, config) {

                    $scope.answer = function(cancel) {
                        if ((page == "new" && $scope.password === 'sacha42') ||
			    (page == "part" && $scope.password === 'yohann') ||
			    (page == "recouv" && $scope.password === 'edison')) {
                            $mdDialog.hide();
                            return cb(null, "OK")
			}
                    }
                },
                templateUrl: '/DialogTemplates/get-password.html',
            });
        },

        verification: function(inter, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog, config) {
                    $scope.data = inter
                    $scope.config = config;
                    $scope.answer = function(cancel) {
                        $scope.window = $window;
                        $scope.inter = inter;
                        if (!cancel) {
                            if (!$scope.inter.prixFinal) {
                                LxNotificationService.error("Veuillez renseigner un prix final");
                            } else {
                                $mdDialog.hide();
                                cb(inter);
                            }
                        } else {
                            $mdDialog.hide();
                        }
                    }
                },
                templateUrl: '/DialogTemplates/verification.html',
            });
        },
        recouvrement: function(inter, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog, config) {
                    $scope.data = inter
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel) {
                            cb(inter);
                        }
                    }
                },
                templateUrl: '/DialogTemplates/recouvrement.html',
            });
        },
        validationReglement: function(inter, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.data = inter

                    Mousetrap.bind(['command+k', 'ctrl+k', 'command+f1', 'ctrl+f1'], function() {
                        $window.open("appurl:", '_self');
                        edisonAPI.intervention.scan(inter.id).then(function() {
                            LxNotificationService.success("Le fichier est enregistré");
                        })
                        return false;
                    });


                    $scope.answer = function(resp) {
                        $scope.data = inter;
                        $mdDialog.hide();
                        if (resp === null) {
                            return cb('nope')
                        }
                        if ($scope.data.compta.reglement.montant !== 0) {
                            $scope.data.compta.reglement.recu = resp;
                        }
                        return cb(null, $scope.data);
                    }
                },
                templateUrl: '/DialogTemplates/validationReglement.html',
            });
        },
        encaissement: function(inter, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $rootScope, $mdDialog) {
                    $scope.tva = inter.tva.toString();
                    $scope.dateSaisie = new Date().getTime();
                    $scope.dateEncaissement = new Date().getTime();
                    if (inter.compta.encaissement.length > 0) {
                        $scope.data = inter;
                    } else {
                        $scope.data = inter;
                        $scope.data.compta.encaissement = [{ client: inter.client.nom, agent: $rootScope.displayUser.login, tva: inter.tva.toString() }];
                    }
                    $scope.add = function() {
                        $scope.data.compta.encaissement.unshift({ client: inter.client.nom, agent: $rootScope.displayUser.login, tva: inter.tva.toString() });
                    }
                    $scope.remove = function(index) {
                        $scope.data.compta.encaissement.splice(index, 1);
                    }
                    $scope.answer = function(resp) {
                        $mdDialog.hide();
                        if (resp === true) {
                            for (var ind = 0; ind < $scope.data.compta.encaissement.length; ind++) {
                                $scope.data.compta.encaissement[ind].dateEncaissement = new Date($scope.data.compta.encaissement[ind].dateEncaissement);
                            }
                            return cb(null, $scope.data);
                        }
                    }
                },
                templateUrl: '/DialogTemplates/encaissement.html',
            });
        },
        decaissement: function(inter, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $rootScope, $mdDialog) {
                    var count = 0;
                    $scope.dateSaisie = new Date();
                    $scope.tva = inter.tva.toString();
                    if (inter.compta.decaissement.length > 0) {
                        $scope.data = inter;
                    } else {
                        $scope.data = inter;
                        $scope.data.compta.decaissement = [{ client: inter.client.nom, agent: $rootScope.displayUser.login, tva: inter.tva.toString() }];
                    }
                    if (inter.compta.reglement.avoir.effectue === true) {
                        for (var ind = 0; ind < inter.compta.decaissement.length; ind++) {
                            if (inter.compta.decaissement[ind].motif == 'avoir') {
                                count++;
                            }
                        }
                        if (count == 0) {
                            $scope.data.compta.decaissement.unshift({ num: inter.compta.reglement.avoir.numeroCheque, dateSaisie: inter.compta.reglement.avoir.date,
                            montant: inter.compta.reglement.avoir.montant, tva: inter.tva.toString(), motif: 'avoir' });
                        }
                    }
                    $scope.add = function() {
                        $scope.data.compta.decaissement.unshift({ client: inter.client.nom, agent: $rootScope.displayUser.login, tva: inter.tva.toString() });
                    }
                    $scope.remove = function(index) {
                        $scope.data.compta.decaissement.splice(index, 1);
                    }
                    $scope.answer = function(resp) {
                        $mdDialog.hide();
                        if (resp === true) {
                            return cb(null, $scope.data);
                        }
                    }
                },
                templateUrl: '/DialogTemplates/decaissement.html',
            });
        },
        validationPaiement: function(inter, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog, textTemplate) {
                    $scope.data = inter
                    Mousetrap.bind(['command+k', 'ctrl+k', 'command+f1', 'ctrl+f1'], function() {
                        $window.open("appurl:", '_self');
                        edisonAPI.intervention.scan(inter.id).then(function() {
                            LxNotificationService.success("Le fichier est enregistré");
                        })
                        return false;
                    });
                    $scope.textTemplate = textTemplate;
                    $scope.data.compta.paiement.ready = true;
                    $scope.preview = function() {
                        openPost('/api/intervention/autofacture', {
                            data: JSON.stringify($scope.data),
                            html: true
                        });
                    }
                    $scope.answer = function(resp) {
                        $scope.data = inter;
                        $mdDialog.hide();
                        if (resp === null) {
                            return cb('nope')
                        }
                        return cb(null, $scope.data);
                    }
                },
                templateUrl: '/DialogTemplates/validationPaiement.html',
            });
        },
        facturierDeviseur: function(artisan, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog, config) {
                    	$scope.sst = artisan
			$scope.facturier = true;
			$scope.deviseur = true;
			$scope.tshirt = true;
			$scope.doublePrint = function(id) {
				window.open('/api/artisan/' + id + '/tshirt');
				window.open('api/artisan/' + id + '/facturier');
			}
			$scope.printTshirt = function(id) {
				window.open('/api/artisan/' + id + '/tshirt');
			}
			$scope.printFact = function(id) {
				window.open('/api/artisan/' + id + '/facturier');
			}
			$scope.answer = function(choice) {
                        	$mdDialog.hide();
                        	if (choice == "facturier")
				{
                            		cb($scope.facturier, $scope.deviseur, $scope.tshirt);
				}
				else if (choice == "tshirt")
				{
					cb($scope.facturier, $scope.deviseur, $scope.tshirt);
				}
				else if (choice == "factTshirt")
				{
					cb($scope.facturier, $scope.deviseur, $scope.tshirt);
				}
                  	}
                },
                templateUrl: '/DialogTemplates/facturierDeviseur.html',
            });
        },
        envoiFacture: function(inter, text, showAcquitte, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.text = text
                    $scope.date = new Date();
                    $scope.showAcquitte = showAcquitte;
                    $scope.acquitte = showAcquitte;
                    $scope.saveFiles = [];
                    $scope.showName = [];
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel) {
                            cb(null, $scope.text, $scope.acquitte, $scope.date, $scope.saveFiles);
                        } else {
                            cb('nope')
                        }
                    }
                    $scope.joinFile = function(elem) {
                        var $scope = this;
                        $scope.$apply(function () {
                            var name = elem.files[0].name;
                            $scope.showName.push(elem.files[0].name);
                            var fileExt = elem.files[0].name.split('.');
                            if (fileExt[fileExt.length - 1] == "json")
                                var typeFile = 'application/json';
                            else
                                var typeFile = elem.files[0].type;
                            if (fileExt[fileExt.length - 1] == "pdf")
                            {
                                var read = new FileReader(elem.files[0]);
                                read.onload = function(e) {
                                    $scope.saveFiles.push({ ContentType: typeFile, Name: elem.files[0].name, Content: e.target.result.replace('data:application/pdf;base64,', '') });
                                }
                                read.readAsDataURL(elem.files[0], 'ISO-8859-1');
                            }
                            else
                            {
                                var read = new FileReader(elem.files[0]);
                                read.onload = function(e) {
                                    $scope.saveFiles.push({ ContentType: typeFile, Name: elem.files[0].name, Content: e.target.result });
                                }
                                read.readAsText(elem.files[0], 'ISO-8859-1');
                            }
                        });
                        $scope.deletePJ = function(index) {
                            $scope.showName.splice(index, 1);
                            $scope.saveFiles.splice(index, 1);
                        }
                    }
                },
                templateUrl: '/DialogTemplates/envoiFacture.html',
            });
        },
        recap: function(inters) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog, config) {
                    $scope.inters = inters;
                    $scope.config = config
                    $scope.answer = function() {
                        $mdDialog.hide();
                    }
                },
                templateUrl: '/DialogTemplates/recapList.html',
            });
        },
        callsList: function(sst) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.sst = sst;
                    $scope.answer = function() {
                        $mdDialog.hide();
                    }
                },
                templateUrl: '/DialogTemplates/callsList.html',
            });
        },
        smsList: function(sst) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.sst = sst;
                    $scope.answer = function() {
                        $mdDialog.hide();
                    }
                },
                templateUrl: '/DialogTemplates/smsList.html',
            });
        },
        choiceText: function(options, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.options = options;
                    $scope.answer = function(resp, text) {
                        $mdDialog.hide();
                        return cb(resp, text);
                    }
                },
                templateUrl: '/DialogTemplates/choiceText.html',
            });
        },
        addProd: function(cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog, $window) {
                    $scope.pu = 0;
                    $scope.window = $window
                    $scope.quantite = 1;
                    $scope.prod = {
                        quantite: 1,
                        pu: 0,
                        title: "",
                        ref: ""
                    }
                    $scope.$watch('prod', function() {
                        $scope.prod.ref = $scope.prod.ref.toUpperCase();
                        $scope.prod.title = $scope.prod.title.toUpperCase();
                        $scope.prod.desc = $scope.prod.title;
                        $scope.prod.pu = $scope.prod.pu;
                        $scope.prod.quantite = $scope.prod.quantite;
                    }, true)
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel) {
                            return cb($scope.prod);
                        }
                    }
                },
                templateUrl: '/DialogTemplates/getProd.html',
            });
        },
	archiver: function(artisan, cb) {
	    $mdDialog.show({
		controller: function($scope, config) {
		    $scope.nomA = artisan.nomSociete
		    $scope.causeArchive = config.cause;
		    $scope.answer = function(resp) {
			if (resp && !$scope.causeArchive) {
			    return LxNotificationService.error("Veuillez renseigner une raison pour archiver l'artisan");
			}
			$mdDialog.hide();
			if (resp)
			{
			    edisonAPI.artisan.save(artisan);
			    return cb(true, $scope.causeArchive);
			}
			return cb(false, 'nope');
		    }
		},
		templateUrl: '/DialogTemplates/archive.html',
	    });
	},
        getCauseAnnulation: function(inter, cb) {
            $mdDialog.show({
                controller: function($scope, config) {
                    $scope.causeAnnulation = config.causeAnnulation;
                    inter.datePlain = moment(inter.date.intervention).format('DD/MM/YYYY')
                    $scope.textSms = _.template("L'intervention {{id}} chez {{client.civilite}} {{client.nom}} à {{client.address.v}} le {{datePlain}} a été annulé. \nMerci de ne pas intervenir. \nEdison Services")(inter)
                    $scope.answer = function(resp) {
                        if (resp && !$scope.ca) {
                            return LxNotificationService.error("Veuillez renseigner une raison d'annulation");
                        }
                        $mdDialog.hide();
                        if (resp)
                            return cb(null, $scope.ca, $scope.reinit, $scope.sendSms, $scope.textSms);
                        return cb('nope');
                    }
                },
                templateUrl: '/DialogTemplates/causeAnnulation.html',
            });
        },
        sendContrat: function(options, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.options = options;
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel)
                            return cb($scope.options);
                    }
                },
                templateUrl: '/DialogTemplates/sendContrat.html',
            });
        },
        getText: function(options, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.options = options;
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel)
                            return cb($scope.options.text);
                    }
                },
                templateUrl: '/DialogTemplates/text.html',
            });
        },
        getTextDevis: function(previewFunction, options, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.options = options;
                    $scope.previewFunction = previewFunction;
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (!cancel)
                            return cb($scope.options.text);
                    }
                },
                templateUrl: '/DialogTemplates/textDevis.html',
            });
        },
        getFileAndText: function(data, text, files, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.Math = Math
                    $scope.xfiles = _.clone(files ||  []);
                    $scope.smsText = text;
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (cancel === false) {
                            return cb(null, $scope.smsText, $scope.addedFile);
                        } else {
                            return cb('nope');
                        }
                    }
                },
                templateUrl: '/DialogTemplates/fileAndText.html',
            });
        },
		demandeSmartphone: function(inter, cb) {
	    	$mdDialog.show({
			controller: function($scope, config) {
		    	edisonAPI.artisan.get(inter.artisan.id).then(function(resp){
					$scope.rep = resp.data.representant.civilite + " " + resp.data.representant.nom.charAt(0).toUpperCase() + resp.data.representant.nom.substring(1).toLowerCase() + " " + 
						resp.data.representant.prenom.charAt(0).toUpperCase() + resp.data.representant.prenom.substring(1).toLowerCase()
		    	})
		    	$scope.inter = inter
		    	$scope.answer = function(resp) {
				$mdDialog.hide();
				if (resp)
				    return cb(null, resp);
				return cb('nope');
		    	}
			},
				templateUrl: '/DialogTemplates/demandeSmartphone.html',
	    	});
		},
		/* Pop up phone envoi intervention */
        envoiIntervention: function(data, text, cb) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.data = data;
                    $scope.smsText = text;
                    $scope.answer = function(cancel) {
                        $mdDialog.hide();
                        if (cancel === false) {
                            return cb(null, $scope.smsText, $scope.addedFile);
                        } else {
                            return cb('nope');
                        }
                    }
                },
                templateUrl: '/DialogTemplates/envoi.html',
            });
        },
        editProduct: {
            open: function(produit, cb) {
                $mdDialog.show({
                    controller: function DialogController($scope, $mdDialog) {
                        $scope.produit = _.clone(produit);
                        $scope.mdDialog = $mdDialog;
                        $scope.answer = function(p) {
                            $mdDialog.hide(p);
                            return cb(p);
                        }
                    },
                    templateUrl: '/DialogTemplates/edit.html',
                });
            }
        },
        selectSubProduct: function(elem, callback) {
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog) {
                    $scope.elem = elem
                    $scope.answer = function(cancel, item) {
                        $mdDialog.hide();
                        if (!cancel) {
                            return callback(item)
                        }
                    }
                },
                templateUrl: '/DialogTemplates/selectSubProduct.html',
            });
        },

        alertDialog: function(dataScope){
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog){
                    $scope.answer = function(params) {
                        $mdDialog.hide();
                        var type = 0;
                        if (dataScope.dataVerif.reglementChoice)
                            type = 1;
                        else if (dataScope.dataVerif.date)
                            type = 2;
                        else if (dataScope.dataVerif.raison)
                            type = 3;
                        if(params === true){
                            document.getElementById('popupContainer').style.display = "none";
                            edisonAPI.intervention.updateVerifInter({ data: dataScope.data, type: type}).then(function(resp) {
                            if (resp.data == "OK")
                                LxNotificationService.success("Update de l'intervention effectuée.");
                            else
                                LxNotificationService.error("Erreur update de l'intervention.");
                            })
                        }
                    };
                },
                templateUrl: '/DialogTemplates/validateDialog.html',
            });
        },
        alertNoDialog: function(data){
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog){
                    console.log("data dialog", data);
                    $scope.answer = function(params) {
                        var refusText = document.getElementById('refusText').value;
                        $mdDialog.hide();
                        console.log(data.dataVerif);
                        var type = 0;
                        if (data.dataVerif.reglementChoice)
                            type = 1;
                        else if (data.dataVerif.date)
                            type = 2;
                        else if (data.dataVerif.raison)
                            type = 3;
                        if (refusText != "")
                        {   
                            if(params === true){
                                document.getElementById('popupContainer').style.display = "none";
                                edisonAPI.intervention.updateRefusInter({ data: data.dataVerif, type: type}).then(function(resp) {
                                    console.log("TEST")
                                    edisonAPI.intervention.sendRefus({ raisonRefus: refusText, data: data.dataVerif, type: 1}).then(function(resp) {
                                        if (resp.data == "OK")
                                                LxNotificationService.success("Mail de refus envoyé.");
                                        else
                                            LxNotificationService.error("Erreur envoi refus.");
                                    })
                                })

                            }
                        }   
                    };
                },
                templateUrl: '/DialogTemplates/refusValidDialog.html',
            });
        },
        alertNoReglementDialog: function(data){
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog){
                    $scope.answer = function(params) {
                        $mdDialog.hide();
                        console.log(data)
                        if(params === true){
                            document.getElementById('popupContainer').style.display = "none";
                            edisonAPI.intervention.updateRefusInter({ data: data.dataVerif, type: 1}).then(function(resp) {
                                edisonAPI.intervention.sendRefus({ raisonRefus: "", data: data.data, type: 2}).then(function(resp) {
                                    if (resp.data == "OK")
                                        LxNotificationService.success("Mail de refus envoyé.");
                                    else
                                        LxNotificationService.error("Erreur envoi refus.");
                                })
                            })
                        }   
                    };
                },
                templateUrl: '/DialogTemplates/noReglementDialog.html',
            });
        },
        priseEnCharge: function(data, cb){
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog){
                    $scope.data = data;
                    if ($scope.data.compta.recouvrement.litige.status == 0)
                        $scope.causeRec = { 'retEnvoi': false, 'chequePerdu': false, 'impaye': false, 'factNonRecu': false, 'tarif': false, 'qualite': false, 'pasDeContact': false, 'plaintes': false, 'autres': false };
                    else
                        $scope.causeRec = { 'tarif': false, 'qualite': false, 'opposition': false, 'riskOpposition': false, 'plaintes': false, 'autres': false, 'demandeSav': false };
                    if (data.compta.recouvrement.causeRec != 'none')
                        $scope.causeRec[data.compta.recouvrement.causeRec] = true;
                    $scope.societe = (data.tva == 0 || data.tva == 20) ? true : false;
                    $scope.particulier = (data.tva == 10) ? true : false;
                    if (data.devisOrigine)
                        $scope.devis = { 'yes': true, 'no': false };
                    else
                        $scope.devis = { 'yes': false, 'no': true };
                    if ($scope.devis['yes'] === true)
                        $scope.mailInter = { 'yes': true, 'no': false };
                    else
                        $scope.mailInter = { 'yes': false, 'no': false };
                    var addClient = $scope.data.client.address.n + $scope.data.client.address.r + $scope.data.client.address.cp + $scope.data.client.address.v;
                    if ($scope.data.facture)
                        var addPayeur = $scope.data.facture.address.n + $scope.data.facture.address.r + $scope.data.facture.address.cp + $scope.data.facture.address.v;
                    if (addClient == addPayeur)
                        $scope.coordIdent = { 'yes': true, 'no': false };
                    else
                        $scope.coordIdent = { 'yes': false, 'no': true };
                    $scope.changeBox = function(type, name) {
                        if (type == 0) {
                            for (var k in $scope.devis)
                                if (k != name)
                                    $scope.devis[k] = false;
                            if ($scope.devis['yes'] === true)
                                $scope.mailInter = { 'yes': true, 'no': false };
                            else
                                for (var k in $scope.mailInter)
                                    if (k != name)
                                        $scope.mailInter[k] = false;
                        } else if (type == 1) {
                            if ($scope.devis['yes'] === true)
                                $scope.mailInter = { 'yes': true, 'no': false };
                            else
                                for (var k in $scope.mailInter)
                                    if (k != name)
                                        $scope.mailInter[k] = false;
                        } else if (type == 2) {
                            for (var k in $scope.coordIdent)
                                if (k != name)
                                    $scope.coordIdent[k] = false;
                        } else if (type == 3) {
                            for (var k in $scope.causeRec)
                                if (k != name)
                                    $scope.causeRec[k] = false;
                        }
                    }
                    $scope.answer = function(params) {
                        var count = 0;;
                        for (var k in $scope.causeRec) {
                            if ($scope.causeRec[k] === true)
                                count++;
                        }
                        if (params === true) {
                            if (count != 1) {
                                LxNotificationService.error("Error: Aucunes ou plus d'une case cocher dans les causes");
                            }
                            else {
                                $mdDialog.hide();
                                for (var k in $scope.causeRec) {
                                    if ($scope.causeRec[k] === true)
                                        data.compta.recouvrement.causeRec = k;
                                }
                                if (data.compta.recouvrement.litige.status == 1)
                                    data.compta.recouvrement.litige.status = 2;
                                else
                                    data.compta.recouvrement.priseEnCharge = 1;
                                data.compta.recouvrement.devisSigne = ($scope.devis['yes'] === true) ? true : false;
                                data.compta.recouvrement.mailInter = ($scope.mailInter['yes'] === true) ? true : false;
                                data.compta.recouvrement.coordFactIdent = ($scope.coordIdent['yes'] === true) ? true : false;
                                cb (data);
                            }
                        }
                        else
                            $mdDialog.hide();
                    };
                },
                templateUrl: '/DialogTemplates/priseEnCharge.html',
            });
        },
        priseDeContact: function(data, cb){
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog){
                    $scope.data = data;
                    if ($scope.data.compta.recouvrement.litige.status == 0)
                        $scope.causeRec = { 'retEnvoi': false, 'chequePerdu': false, 'impaye': false, 'factNonRecu': false, 'tarif': false, 'qualite': false, 'pasDeContact': false, 'plaintes': false, 'autres': false };
                    else
                        $scope.causeRec = { 'tarif': false, 'qualite': false, 'opposition': false, 'riskOpposition': false, 'plaintes': false, 'autres': false, 'demandeSav': false };
                    $scope.status = { 'enc': false, 'reg': false, 'resolu': false, 'perte': false };
                    if ($scope.data.compta.recouvrement.litige.status == 0) {
                        $scope.conclu = { 'demandeSav': false, 'remiseCom': false, 'lettreDesist': false, 'factRenv': false, 'lettreOppo': false, 'remiseEnc': false,
                        'protAcc': false, 'demeure': false, 'injTribProx': false, 'injTribCom': false, 'negoc': false, 'regClient': false };
                    } else {
                        $scope.conclu = { 'protAcc': false, 'demeure': false, 'lettreOppo': false, 'demandeSav': false, 'remiseCom': false, 'negoc': false, 'regClient': false, 'envRec': false };
                    }
                    if ($scope.data.compta.recouvrement.priseDeContact.conclu != 'none')
                        $scope.conclu[$scope.data.compta.recouvrement.priseDeContact.conclu] = true;
                    if ($scope.data.compta.recouvrement.causeRec != 'none')
                        $scope.causeRec[$scope.data.compta.recouvrement.causeRec] = true;
                    if ($scope.data.compta.recouvrement.priseDeContact.conclu == 'remiseCom' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'protAcc'
                        || $scope.data.compta.recouvrement.priseDeContact.conclu == 'factRenv' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'lettreDesist'
                        || $scope.data.compta.recouvrement.priseDeContact.conclu == 'regClient' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'remiseEnc')
                        $scope.status['reg'] = true;
                    else
                        if ($scope.data.compta.recouvrement.priseDeContact.statusRec != 'none')
                            $scope.status[$scope.data.compta.recouvrement.priseDeContact.statusRec] = true;
                    $scope.changeBox = function(type, name) {
                        if (type == 0) {
                            for (var k in $scope.causeRec)
                                if (k != name)
                                    $scope.causeRec[k] = false;
                        } else if (type == 1) {
                            if (name == 'regClient' || name == 'factRenv' || name == 'lettreDesist' || name == 'protAcc' || name === 'remiseCom' || name === 'remiseEnc') {
                                for (var k in $scope.status)
                                    if (k != 'reg')
                                        $scope.status[k] = false;
                                    else if (k == 'reg' && $scope.status[k] === false)
                                        $scope.status[k] = true;
                            }
                            else {
                                $scope.status = { 'enc': false, 'reg': false, 'resolu': false, 'perte': false };
                            }
                            for (var k in $scope.conclu)
                                if (k != name)
                                    $scope.conclu[k] = false;
                        } else if (type == 2) {
                            if ($scope.conclu['regClient'] === true || $scope.conclu['factRenv'] === true
                            || $scope.conclu['lettreDesist'] === true || $scope.conclu['protAcc'] === true
                            || $scope.conclu['remiseCom'] === true || $scope.conclu['remiseEnc'] === true) {
                                for (var k in $scope.status)
                                    if (k != 'reg')
                                        $scope.status[k] = false;
                                    else if (k == 'reg' && $scope.status[k] === false)
                                        $scope.status[k] = true;

                            } else {
                                for (var k in $scope.status)
                                    if (k != name)
                                        $scope.status[k] = false;
                            }
                        }
                    }
                    $scope.answer = function(params) {
                        var count = 0;
                        for (var k in $scope.causeRec) {
                            if ($scope.causeRec[k] === true)
                                count++;
                        }
                        var count1 = 0;
                        for (var k in $scope.conclu) {
                            if ($scope.conclu[k] === true)
                                count1++;
                        }
                        var count2 = 0;
                        if ($scope.conclu['envRec'] === false) {
                            for (var k in $scope.status) {
                                if ($scope.status[k] === true)
                                    count2++;
                            }
                        }
                        else
                            count2 = 1;
                        if (params === true) {
                            if (count != 1 || count1 != 1 || count2 != 1) {
                                LxNotificationService.error("Error: Aucunes ou plus d'une case cocher dans les causes, conclusions ou status");
                            }
                            else {
                                for (var k in $scope.causeRec) {
                                    if ($scope.causeRec[k] === true)
                                        $scope.data.compta.recouvrement.causeRec = k;
                                }
                                for (var k in $scope.conclu) {
                                    if ($scope.conclu[k] === true)
                                        $scope.data.compta.recouvrement.priseDeContact.conclu = k;
                                }
                                if ($scope.data.compta.recouvrement.priseDeContact.conclu == 'remiseCom' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'protAcc'
                                    || $scope.data.compta.recouvrement.priseDeContact.conclu == 'factRenv' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'lettreDesist'
                                    || $scope.data.compta.recouvrement.priseDeContact.conclu == 'regClient' || $scope.data.compta.recouvrement.priseDeContact.conclu == 'remiseEnc') {
                                    $scope.data.compta.recouvrement.priseDeContact.statusRec = "reg";
                                } else if ($scope.data.compta.recouvrement.priseDeContact.conclu != 'envRec') {
                                    for (var k in $scope.status) {
                                        if ($scope.status[k] === true)
                                            $scope.data.compta.recouvrement.priseDeContact.statusRec = k;
                                    }
                                }
				var day = new Date().getDate();
				var month = new Date().getMonth() + 1;
				if (month < 10)
					month = '0' + month;
				var year = new Date().getFullYear();
                                if (!$scope.data.compta.recouvrement.priseDeContact.comment)
                                    $scope.data.compta.recouvrement.priseDeContact.comment = day + '/' + month + '/' + year + " Pas de réponse";
                                if ($scope.data.compta.recouvrement.priseDeContact.statusRec == "reg" && !data.compta.recouvrement.priseDeContact.dateRelance) {
                                    LxNotificationService.error("Error: Date de relance obligatoire pour ce status");
                                } else {
                                    $mdDialog.hide();
                                    if ($scope.data.compta.recouvrement.priseDeContact.statusRec != 'reg')
                                        delete $scope.data.compta.recouvrement.priseDeContact.dateRelance;
                                    if (($scope.data.compta.recouvrement.priseDeContact.statusRec == 'perte' || $scope.data.compta.recouvrement.priseDeContact.statusRec == 'resolu') && $scope.data.compta.recouvrement.priseEnCharge == 1) {
                                        $scope.data.compta.recouvrement.priseEnCharge = 2;
                                    } else if (($scope.data.compta.recouvrement.priseDeContact.statusRec == 'perte' || $scope.data.compta.recouvrement.priseDeContact.statusRec == 'resolu') && $scope.data.compta.recouvrement.litige.status == 2) {
                                        $scope.data.compta.recouvrement.litige.status = 3;
                                    } else if ($scope.data.compta.recouvrement.litige.status == 2) {
                                        $scope.data.compta.recouvrement.litige.status = 2;
                                    } else {
                                        $scope.data.compta.recouvrement.priseEnCharge = 1;
                                    }
                                    if (!$scope.comment || $scope.comment == "")
                                        $scope.data.compta.recouvrement.priseDeContact.comment.push(day + '/' + month + '/' + year + " Pas de réponse");
                                    else
                                        $scope.data.compta.recouvrement.priseDeContact.comment.push(day + '/' + month + '/' + year + ' ' + $scope.comment);
                                    cb ($scope.data);
                                }
                            }
                        } else {
                            $mdDialog.hide();
                        }
                    };
                    $scope.reset = function(params) {
                        if (params === true) {
                            $mdDialog.hide();
                            edisonAPI.intervention.resetRec({ data: $scope.data }).then(function(returnEnd) {
                                if (returnEnd.data == "YES") {
                                    LxNotificationService.success("Reset des données recouvrement");
                                } else {
                                    LxNotificationService.error("Une erreur est survenue");
                                }
                            })
                        }
                    }
                },
                templateUrl: '/DialogTemplates/priseDeContact.html',
            });
        },
        statusRec: function(data, cb){
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog){
                    $scope.data = data;
                    $scope.status = { 'enc': false, 'reg': false, 'resolu': false, 'perte': false };
                    if ($scope.data.compta.recouvrement.priseDeContact.statusRec != 'none')
                        $scope.status[data.compta.recouvrement.priseDeContact.statusRec] = true;
                    $scope.changeBox = function(name) {
                        for (var k in $scope.status)
                            if (k != name)
                                $scope.status[k] = false;
                    }
                    $scope.answer = function(params) {
                        var count = 0;
                        for (var k in $scope.status) {
                            if ($scope.status[k] === true)
                                count++;
                        }
                        if (params === true) {
                            if (count != 1) {
                                LxNotificationService.error("Error: Aucunes ou plus d'une case cocher");
                            }
                            else {
                                $mdDialog.hide();
                                for (var k in $scope.status) {
                                    if ($scope.status[k] === true)
                                        data.compta.recouvrement.priseDeContact.statusRec = k;
                                }
                                if ($scope.data.compta.recouvrement.priseDeContact.statusRec != 'reg')
                                    delete $scope.data.compta.recouvrement.priseDeContact.dateRelance;
                                if (($scope.data.compta.recouvrement.priseDeContact.statusRec == 'perte' || $scope.data.compta.recouvrement.priseDeContact.statusRec == 'resolu')
                                    && $scope.data.compta.recouvrement.priseEnCharge == 1)
                                    $scope.data.compta.recouvrement.priseEnCharge = 2;
                                else if (($scope.data.compta.recouvrement.priseDeContact.statusRec == 'perte' || $scope.data.compta.recouvrement.priseDeContact.statusRec == 'resolu')
                                    && $scope.data.compta.recouvrement.litige.status == 2)
                                    $scope.data.compta.recouvrement.litige.status = 3;
                                else if ($scope.data.compta.recouvrement.litige.status == 2 || $scope.data.compta.recouvrement.litige.status == 3){
                                    $scope.data.compta.recouvrement.priseEnCharge = 0;
                                    $scope.data.compta.recouvrement.litige.status = 2;
                                }
                                else
                                    $scope.data.compta.recouvrement.priseEnCharge = 1;
                                cb ($scope.data);
                            }
                        }
                        else
                            $mdDialog.hide();
                    };
                },
                templateUrl: '/DialogTemplates/statusRec.html',
            });
        },
        createLitige: function(cb){
            $mdDialog.show({
                controller: function DialogController($scope, $mdDialog){
                    console.log("Dialog litige");
                    $scope.answer = function(params) {
                        if (params === true) {
                            edisonAPI.intervention.getDataOneInter({ id: parseInt($scope.idLitige) }).then(function(resp) {
                                if (resp.data.compta.recouvrement.litige.status != 0) {
                                    LxNotificationService.error("Litige déjà éxistant");
                                } else {
                                    if (!$scope.idLitige)
                                        LxNotificationService.error("Case de l'id litige vide");
                                    else {
                                        $mdDialog.hide();
                                        cb (parseInt($scope.idLitige))
                                    }
                                }
                            })
                        }
                        else
                            $mdDialog.hide();
                    };
                },
                templateUrl: '/DialogTemplates/createLitige.html',
            });
        }
    }
});
