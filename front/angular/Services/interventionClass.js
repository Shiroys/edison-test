angular.module('edison')
    .factory('Intervention', function($location, $window, openPost, LxNotificationService, LxProgressService, dialog, user, config, edisonAPI, Devis, $rootScope, $routeParams, textTemplate) {
        "use strict";

        var Intervention = function(data) {
            if (!(this instanceof Intervention)) {
                return new Intervention(data);
            }
            for (var k in data) {
                this[k] = data[k];
            }
        };

        var appelLocal = function(tel) {
            if (tel) {
                $window.open('callto:' + tel, '_self', false);
            }
        }

        Intervention.prototype.callTel1 = function() {
            appelLocal(this.client.telephone.tel1)
        }
        Intervention.prototype.callTel2 = function() {
            appelLocal(this.client.telephone.tel2)
        }
        Intervention.prototype.callTel3 = function() {
            appelLocal(this.client.telephone.tel3)
        }

        Intervention.prototype.callSst1 = function() {
            appelLocal(this.sst.telephone.tel1)
	        edisonAPI.artisan.callLog(this.sst.id).then(function (resp) {
                var message = _.template("L'appel a été ajouté à l'historique.")(resp.data)
                LxNotificationService.success(message);
            }, function (error) {
                LxNotificationService.error(error.data);
	        });
        }
        Intervention.prototype.callSst2 = function() {
            appelLocal(this.sst.telephone.tel2)
            edisonAPI.artisan.callLog(this.sst.id).then(function (resp) {
                var message = _.template("L'appel a été ajouté à l'historique.")(resp.data)
                LxNotificationService.success(message);
            }, function (error) {
                LxNotificationService.error(error.data);
            });
        }

        Intervention.prototype.callPayeur1 = function() {
            appelLocal(this.facture.tel)
        }

        Intervention.prototype.callPayeur2 = function() {
            appelLocal(this.facture.tel2)
        }

        Intervention.prototype.typeOf = function() {
            return 'Intervention';
        };
        Intervention.prototype.envoiDevis = function(cb) {
            Devis().envoi.bind(this)(cb)
        };

        Intervention.prototype.validerReglement = function(cb) {
            var _this = this;
            dialog.validationReglement(this, function(err, resp) {
                if (err) {
                    return cb && cb(err);
                }
                edisonAPI.intervention.save(_this).then(function(resp) {
                    LxNotificationService.success("L'intervention " + _this.id + " est modifié");
                }, function(err) {
                    LxNotificationService.error("Une erreur est survenu (" + err.data + ")");
                });
            })
        };

        Intervention.prototype.encaissement = function(cb) {
            var _this = this;
            dialog.encaissement(this, function(err, newInter) {
                if (err) {
                    LxNotificationService.error("Une erreur est survenu");
                } else {
                    edisonAPI.intervention.saveEncaissement({ data: newInter }).then(function(resp) {
                        LxNotificationService.success("Encaissement valider");
                    }, function(err) {
                        LxNotificationService.error("Une erreur est survenu");
                    });
                }
            })
        };

        Intervention.prototype.decaissement = function(cb) {
            var _this = this;
            dialog.decaissement(this, function(err, newInter) {
                if (err) {
                    LxNotificationService.error("Une erreur est survenu");
                } else {
                    edisonAPI.intervention.saveDecaissement({ data: newInter }).then(function(resp) {
                        LxNotificationService.success("Decaissement valider");
                    }, function(err) {
                        LxNotificationService.error("Une erreur est survenu");
                    });
                }
            })
        };

	Intervention.prototype.comment = function() {
            var _this = this;
            dialog.applyComment({comments: _this.comments}, function(comment) {
        		_this.comments.push({
                    login: user.login,
                    text: comment,
                    date: new Date()
                })
        		edisonAPI.intervention.save(_this)
            });	    
        }

        Intervention.prototype.validerPaiement = function(cb) {
            var _this = this;
            dialog.validationPaiement(this, function(err, resp) {
                if (err) {
                    return cb && cb(err);
                }
                edisonAPI.intervention.save(_this).then(function(resp) {
                    LxNotificationService.success("L'intervention " + _this.id + " est modifié");
                }, function(err) {
                    LxNotificationService.error("Une erreur est survenu (" + err.data + ")");
                });
            })
        };

        Intervention.prototype.demarcher = function(cb) {
            edisonAPI.intervention.demarcher(this.id).success(function() {
                LxNotificationService.success("Vous demarchez l'intervention");
            }, function() {
                LxNotificationService.error("Une erreur est survenu");
            })
        };

        Intervention.prototype.favoris = function(cb) {
            edisonAPI.intervention.favoris({ id: this.id }).success(function(resp) {
                if (resp == "OUI")
                    LxNotificationService.success("Vous avez mis l'intervention en favoris");
                else if (resp == "NON")
                    LxNotificationService.success("Vous avez retiré l'intervention des favoris");
            }, function() {
                LxNotificationService.error("Une erreur est survenu");
            })
        };

        Intervention.prototype.facturePreview = function() {
            openPost('/api/intervention/facturePreview', {
                data: JSON.stringify(this),
                html: true
            })
        }

        Intervention.prototype.factureAcquittePreview = function() {
            openPost('/api/intervention/factureAcquittePreview', {
                data: JSON.stringify(this),
                html: true
            })
        }


        Intervention.prototype.sendFactureAcquitte = function(cb) {
            var _this = this;
            var datePlain = moment(this.date.intervention).format('LL');
            var template = textTemplate.mail.intervention.factureAcquitte.bind(_this)(datePlain)
            var mailText = (_.template(template)(this))
            dialog.envoiFacture(_this, mailText, true, function(err, text, acquitte, date, dataFiles) {
                if (err)
                    return cb('nope')
                LxProgressService.circular.show('#5fa2db', '#globalProgress');
                edisonAPI.intervention.sendFactureAcquitte(_this.id, {
                    text: text,
                    acquitte: acquitte,
                    date: date,
                    data: _this,
                    dataFiles: dataFiles
                }).success(function(resp) {
                    LxProgressService.circular.hide();
                    var validationMessage = _.template("La facture de l'intervention {{id}} à été envoyé")(_this)
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    LxProgressService.circular.hide();
                    var validationMessage = _.template("L'envoi de la facture {{id}} à échoué\n" + "(" + err.data + ")")(_this)
                    LxNotificationService.error(validationMessage);
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        }

        Intervention.prototype.sendFacture = function(cb) {
            var _this = this;
            var datePlain = moment(this.date.intervention).format('LL');
            var template = textTemplate.mail.intervention.envoiFacture.bind(_this)(datePlain)
            var mailText = (_.template(template)(this))
            dialog.envoiFacture(_this, mailText, false, function(err, text, acquitte, date, dataFiles) {
                if (err)
                    return cb('nope')
                LxProgressService.circular.show('#5fa2db', '#globalProgress');
                edisonAPI.intervention.sendFacture(_this.id, {
                    text: text,
                    dataFiles: dataFiles
                }).success(function(resp) {
                    LxProgressService.circular.hide();
                    var validationMessage = _.template("La facture de l'intervention {{id}} à été envoyé")(_this)
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    LxProgressService.circular.hide();
                    var validationMessage = _.template("L'envoi de la facture {{id}} à échoué\n" + "(" + err.data + ")")(_this)
                    LxNotificationService.error(validationMessage);
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        }



        Intervention.prototype.ouvrirFicheV1 = function() {
            $window.open('http://electricien13003.com/alvin/5_Gestion_des_interventions/show_res_bis_2.php?id_client=' + this.id)
        }
        Intervention.prototype.autoFacture = function() {
            $window.open('/api/intervention/' + this.id + '/autoFacture')
        }
        Intervention.prototype.ouvrirFiche = function() {
            $location.url('/intervention/' + this.id)
        }
        Intervention.prototype.ouvrirRecapSST = function() {
            $location.url(['/artisan', this.artisan.id, 'recap'].join('/') + '#interventions')
        }
        Intervention.prototype.smsArtisan = function(cb) {
            var _this = this;
            var text = textTemplate.sms.intervention.demande.bind(this)(user, config);
            text = _.template(text)(this)
            dialog.getFileAndText(_this, text, [], function(err, text) {
                if (err) {
                    return cb(err)
                }
                edisonAPI.sms.send({
                    dest: _this.sst.nomSociete,
                    text: text,
                    to: _this.sst.telephone.tel1,
                }).success(function(resp) {
                    var validationMessage = _.template("Un sms a été envoyé à M. {{sst.representant.nom}}")(_this)
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    LxNotificationService.success("L'envoi du sms a échoué");
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        };

        Intervention.prototype.callClient = function(cb) {
            var _this = this;
            var now = Date.now();
            $window.open('callto:' + _this.client.telephone.tel1, '_self', false)
            dialog.choiceText({
                subTitle: _this.client.telephone.tel1,
                title: 'Nouvel Appel Client',
            }, function(response, text) {
                edisonAPI.call.save({
                    date: now,
                    to: _this.client.telephone.tel1,
                    link: _this.id,
                    origin: _this.id || _this.tmpID || 0,
                    description: text,
                    response: response
                }).success(function(resp) {
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        }
        Intervention.prototype.callArtisan = function(cb) {
            var _this = this;
            var now = Date.now();
            $window.open('callto:' + _this.artisan.telephone.tel1, '_self', false)
            dialog.choiceText({
                subTitle: _this.artisan.telephone.tel1,
                title: 'Nouvel Appel',
            }, function(response, text) {
                edisonAPI.call.save({
                    date: now,
                    to: _this.artisan.telephone.tel1,
                    link: _this.artisan.id,
                    origin: _this.id || _this.tmpID || 0,
                    description: text,
                    response: response
                }).success(function(resp) {
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    if (typeof cb === 'function')
                        cb(err);
                })
            })
        };
        Intervention.prototype.save = function(cb) {
            var _this = this;
            var fournitureSansFournisseur = _.find(this.fourniture, function(e) {
                return !e.fournisseur;
            })
            var fournitureSansPU = _.find(this.fourniture, function(e) {
                    return !e.pu;
            })
            if (fournitureSansFournisseur) {
                LxNotificationService.error("Veuillez renseigner un fournisseur");
                return cb(fournitureSansFournisseur)
            }
            if (fournitureSansPU) {
                LxNotificationService.error("Veuillez renseigner un prix pour toutes les fournitures");
                return cb(fournitureSansPU)
            }
            edisonAPI.intervention.save(_this)
                .then(function(resp) {
                    var validationMessage = _.template("Les données de l'intervention {{id}} ont à été enregistré.")(resp.data)
                    if ((_this.tmpID && _this.sst) || (_this.sst__id && _this.sst && _this.sst__id !== _this.sst.id) && !_this.sst.tutelle) {
                        validationMessage += "\n\n Un sms à été envoyé";
                    }
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function')
                        cb(null, resp.data)
                }, function(error) {
                    LxNotificationService.error(error.data);
                    if (typeof cb === 'function')
                        cb(error.data)
                });
        };

		/* Prototype envoi intervention */
        Intervention.prototype.envoi = function(cb) {
            var _this = this;
	    	var pass = 0;
            if (!Intervention(_this).isEnvoyable()) {
                return LxNotificationService.error("Vous ne pouvez pas envoyer cette intervention");
            }
	    	edisonAPI.artisan.get(_this.artisan.id).then(function(resp2){
                _this.artisan.majInternet = resp2.data.majInternet;
                var defaultText = textTemplate.sms.intervention.envoi.bind(_this)(_.find(window.app_users, 'login', _this.login.ajout));
				if (!resp2.data.smartphone || resp2.data.smartphone === "Non demandé")
				{
		    		dialog.demandeSmartphone(_this, function(err, demande) {
						if (err)
						    return cb && cb(err)
						resp2.data.smartphone = demande;
						edisonAPI.artisan.save(resp2.data)
						dialog.envoiIntervention(_this, defaultText, function(err, text, file) {
			    			if (err)
								return cb && cb(err)
				  	  		LxProgressService.circular.show('#5fa2db', '#globalProgress');
				    		edisonAPI.intervention.envoi(_this.id, {
								sms: text,
								file: file
			    			}).then(function(resp) {
								LxProgressService.circular.hide();
								var validationMessage = _.template("L'intervention est envoyé")
								LxNotificationService.success(validationMessage);
								if (typeof cb === 'function')
						    		cb(null, resp.data)
			    			}, function(error) {
								LxProgressService.circular.hide();
								LxNotificationService.error(error.data);
								if (typeof cb === 'function')
						 		   cb(error.data);
			    			});
						})
		    		})
				}
				else
				{
					/* Pop up phone envoi sms */
		    		dialog.envoiIntervention(_this, defaultText, function(err, text, file) {
						if (err)
					    	return cb && cb(err)
						LxProgressService.circular.show('#5fa2db', '#globalProgress');
						edisonAPI.intervention.envoi(_this.id, {
						    sms: text,
						    file: file
						}).then(function(resp) {
						    LxProgressService.circular.hide();
						    var validationMessage = _.template("L'intervention est envoyé")
						    LxNotificationService.success(validationMessage);
						    if (typeof cb === 'function')
								cb(null, resp.data)
						}, function(error) {
						    LxProgressService.circular.hide();
						    LxNotificationService.error(error.data);
						    if (typeof cb === 'function')
								cb(error.data);
						});
		    		})
				}
	    	})
        };
	

        Intervention.prototype.reactivation = function(cb) {
            var _this = this;
            edisonAPI.intervention.reactivation(this.id).then(function() {
                LxNotificationService.success("L'intervention " + _this.id + " est à programmer");
            })
        };

        Intervention.prototype.annulation = function(cb) {
            var _this = this;
	    _this.config = config
            dialog.getCauseAnnulation(_this, function(err, causeAnnulation, reinit, sms, textSms) {
                if (err) {
                    return typeof cb === 'function' && cb('err');
                }
		if (causeAnnulation === "PS_SST" || causeAnnulation === "PS_SST_DISPO" || causeAnnulation === "PS_BON")
		{
		    var addr = _this.client.address.cp + ' ' + _this.client.address.v;
		    var dat = new Date();
		    var day = dat.getDate().toString();
		    var m = (dat.getMonth() + 1).toString();
		    var min = (dat.getMinutes()).toString();
		    var fulldate = dat.getHours() + "h" +
			(min[1] ? min : "0" + min[0]) + " " + (day[1] ? day : "0" + day[0]) + "/" +
			(m[1] ? m : "0" + m[0]) + "/" + dat.getFullYear();
		    var obj = {
			date: fulldate,
			login: $rootScope.user.login,
			cate: config.categories[_this.categorie].order,
			ad: addr,
			status: false,
		    };
		    edisonAPI.demarchage.add(obj).then(function (resp) {
			var message = _.template("Une nouvelle adresse a été ajoutée à la liste de démarchage.")(resp.data)
			LxNotificationService.success(message);
		    }, function (error) {
			LxNotificationService.error(error.data);
		    });
		}
                edisonAPI.intervention.annulation(_this.id, {
                    causeAnnulation: causeAnnulation,
                    reinit: reinit,
                    sms: sms,
                    textSms: textSms
                })
		    .then(function(resp) {
                        var msg = "L'intervention {{id}} est annulé";
                        if (sms) {
                            msg += "\nUn sms à été envoyé au SST";
                        }
                        var validationMessage = _.template(msg)(resp.data)
                        LxNotificationService.success(validationMessage);
                        if (typeof cb === 'function') {
                            cb(null, resp.data)
                        }
                    });
            });
        };


        Intervention.prototype.envoiFactureVerif = function(cb) {
            var _this = this;

            if (!this.produits.length) {
                LxNotificationService.error("Veuillez renseigner les produits");
                return cb('nope')
            }
            _this.sendFacture(function(err) {
                if (err)
                    return cb(err);
                _this.verificationSimple(cb)
            })
        }

        Intervention.prototype.verificationSimple = function(cb) {
            var _this = this;
            LxProgressService.circular.show('#5fa2db', '#globalProgress');
            if (!Intervention(this).isVerifiable()) {
                return LxNotificationService.error("Vous ne pouvez pas verifier cette intervention");
            }
            edisonAPI.intervention.verification(_this.id)
                .then(function(resp) {
                    LxProgressService.circular.hide()
                    var validationMessage = _.template("L'intervention {{id}} est vérifié")(resp.data)
                    LxNotificationService.success(validationMessage);
                    if (typeof cb === 'function') {
                        cb(null, resp.data);
                    }
                }, function(error) {
                    LxProgressService.circular.hide()
                    LxNotificationService.error(error.data);
                    cb(error.data);
                })
        }

        Intervention.prototype.verification = function(cb) {
            if (!Intervention(this).isVerifiable()) {
                return LxNotificationService.error("Vous ne pouvez pas verifier cette intervention");
            }
            var _this = this;
            if (!_this.reglementSurPlace) {
                return $location.url('/intervention/' + this.id)
            }
            dialog.verification(_this, function(inter) {
                Intervention(inter).save(function(err, resp) {
                    if (!err) {
                        return Intervention(resp).verificationSimple(cb);
                    }
                });
            });
        }


        Intervention.prototype.recouvrement = function(cb) {
            var _this = this;
            dialog.recouvrement(_this, function(inter) {
		inter.recouvrement.date = new Date()
		inter.recouvrement.login = user.login
                Intervention(inter).save(function(err, resp) {
                    return (cb || _.noop)()
                });
            });
        }

        Intervention.prototype.priseDeContact = function(cb) {
            edisonAPI.intervention.getDataOneInter({ id: this.id }).then(function(dataInter) {
                dialog.priseDeContact(dataInter.data, function(data) {
                    if (data.compta.recouvrement.priseDeContact.conclu == "demandeSav")
                        edisonAPI.intervention.demandeSav({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Demande sav envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "remiseCom")
                        edisonAPI.intervention.remiseCom({ data: data }).then(function(returnEnd) {
                            if (returnEnd.data == "YES")
                                LxNotificationService.success("Prix à négocier modifié");
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "lettreDesist")
                        edisonAPI.intervention.lettreDesist({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Lettre de désistement envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "remiseEnc")
                        edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                            if (returnEnd.data == "YES")
                                LxNotificationService.success("Remise à l'encaissement");
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "injTribProx")
                        edisonAPI.intervention.injTribProx({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Injonction tribunal de proximite envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "injTribCom")
                        edisonAPI.intervention.injTribCom({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Injonction tribunal de commerce envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "protAcc")
                        edisonAPI.intervention.protAcc({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Protocole d'accord envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "demeure")
                        edisonAPI.intervention.demeure({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Mise en demeure envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "lettreOppo")
                        edisonAPI.intervention.lettreOppo({ data: data }).then(function(resp) {
                            if (resp.data == "YES")
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Lettre d'opposition envoyée");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "factRenv")
                        Intervention(data).sendFacture(function(err) {
                            if (err)
                                LxNotificationService.error("Une erreur est survenue")
                            else
                            {
                                edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                                    if (returnEnd.data == "YES")
                                        LxNotificationService.success("Renvoi de la facture effectué");
                                    else
                                        LxNotificationService.error("Une erreur est survenue")
                                })
                            }
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "regClient")
                        edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                            if (returnEnd.data == "YES")
                                LxNotificationService.success("Règlement client en cours");
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "negoc")
                        edisonAPI.intervention.saveRec({ data: data }).then(function(returnEnd) {
                            if (returnEnd.data == "YES")
                                LxNotificationService.success("Négociation en cours");
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                    else if (data.compta.recouvrement.priseDeContact.conclu == "envRec")
                        edisonAPI.intervention.envRec({ data: data }).then(function(returnEnd) {
                            if (returnEnd.data == "YES")
                                LxNotificationService.success("Envoi en recouvrement");
                            else
                                LxNotificationService.error("Une erreur est survenue")
                        });
                })
            })
        }

        Intervention.prototype.priseEnCharge = function(cb) {
            edisonAPI.intervention.getDataOneInter({ id: this.id }).then(function(dataInter) {
                dialog.priseEnCharge(dataInter.data, function(data) {
                    edisonAPI.intervention.priseEnCharge({ data: data }).then(function(returnPrise) {
                        if (returnPrise.data == "YES")
                            LxNotificationService.success("Vous avez pris en charge l'intervention");
                    }, function() {
                        LxNotificationService.error("Une erreur est survenu");
                    })
                })
            })
        }

        Intervention.prototype.statusRec = function(cb) {
            edisonAPI.intervention.getDataOneInter({ id: this.id }).then(function(dataInter) {
                dialog.statusRec(dataInter.data, function(data) {
                    edisonAPI.intervention.saveRec({ data: data }).then(function(returnPrise) {
                        if (returnPrise.data == "YES")
                            LxNotificationService.success("Vous avez changé le status de recouvrement");
                    }, function() {
                        LxNotificationService.error("Une erreur est survenu");
                    })
                })
            })
        }

        Intervention.prototype.fileUpload = function(file, cb) {
            var _this = this;
            if (file) {
                LxProgressService.circular.show('#5fa2db', '#fileUploadProgress');
                edisonAPI.file.upload(file, {
                    link: _this.id || _this.tmpID,
                    model: 'intervention',
                    type: 'fiche'
                }).success(function(resp) {
                    LxProgressService.circular.hide();
                    if (typeof cb === 'function')
                        cb(null, resp);
                }).catch(function(err) {
                    LxProgressService.circular.hide();
                    if (typeof cb === 'function')
                        cb(err);
                })
            }
        }

        Intervention.prototype.editCB = function() {
            var _this = this;
            edisonAPI.intervention.getCB(this.id).success(function(resp) {
                _this.cb = resp;
            }, function(error) {
                LxNotificationService.error(error.data);
            })
        }

        Intervention.prototype.reinitCB = function() {
            this.cb = {
                number: 0
            }
        }
        function moneyStore()
        {
            this.limit = -1;
            this.seuil = -1;
            this.lswp = -1;
            this.sswp = -1;
            this.srwp = -1;
        }
        var moneyInstance = new moneyStore();
	//new Single Instance of moneyStore, acts like a C Static Variable

        Intervention.prototype.isEnvoyable = function()
		{
            if (this.status === "SAV")
            {
                return (true);
            }
			if (moneyInstance.limit == -1)
			{
				moneyInstance.limit = 0;
				edisonAPI.intervention.checklimit({id:this.sst._id}).then(function(resp)
				{
					moneyInstance.lswp = Number(resp.data);
				});
			}
			if (moneyInstance.seuil == -1)
			{
				moneyInstance.seuil = 0;
				edisonAPI.intervention.getSeuilMaxArtisan({id:this.sst._id}).then(function(resp)
				{
					moneyInstance.sswp = Number(resp.data);
				});
				edisonAPI.intervention.getSeuilRuptArtisan({id:this.sst._id}).then(function(resp)
				{
					moneyInstance.srwp = Number(resp.data);
				});
			}
			var isAdmin = typeof user.admin == 'undefined' ? false : user.admin;
	    	var isRoot = typeof user.root == 'undefined' ? false : user.root;
	    	
			if (!this.sst || (((moneyInstance.srwp < moneyInstance.lswp) == true) && isAdmin == false) || (((moneyInstance.sswp < moneyInstance.lswp) == true) && isRoot == false))
			{
				return (false);
			}
			else if (this.sst.subStatus === 'QUA' ||  this.sst.blocked)
			{
				return user.root;
			}
			else if (this.sst.subStatus === 'NEW' || this.sst.subStatus === 'TUT' || this.sst.status === 'POT')
			{
				return user.root || user.service === 'PARTENARIAT'
			}
            else if (!this.reglementSurPlace)
            {
                return true
            }
			return _.includes(["ANN", "APR", "ENC", undefined], this.status)
		}

        Intervention.prototype.isPayable = function() {
            return (this.status === 'ENC' || this.status === 'VRF') &&
                user.root ||  user.service === 'COMPTABILITE' || user.service === 'RECOUVREMENT'
        }

        Intervention.prototype.isVerifiable = function() {
	    if (!this.artisan) {
                return false;
            }
	    if (this.status === 'SAV')
		return this.status === 'SAV'
            return this.status === 'ENC'
        }
        return Intervention;
    });
