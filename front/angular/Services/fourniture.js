angular.module('edison').factory('fourniture', [function($rootScope) {
    "use strict";

    return {
        init: function(fourniture) {
            this.fourniture = fourniture;
            if (!this.fourniture)
                this.fourniture = [];
            return this;
        },
        remove: function(index) {
            this.fourniture.splice(index, 1);
        },
        moveTop: function(index) {
            if (index !== 0) {
		fourniture.updateDate(index);
                var tmp = this.fourniture[index - 1];
                this.fourniture[index - 1] = this.fourniture[index];
                this.fourniture[index] = tmp;
            }

        },
        moveDown: function(index) {
            if (index !== this.fourniture.length - 1) {
		fourniture.updateDate(index);
                var tmp = this.fourniture[index + 1];
                this.fourniture[index + 1] = this.fourniture[index];
                this.fourniture[index] = tmp;
            }
        },
        add: function() {
            this.fourniture.push({
                bl: '0',
                title: 'Fourniture',
                quantite: 1,
                pu: 0
            });
        },
        total: function() {
            var total = 0;
            if (this.fourniture) {
                this.fourniture.forEach(function(e) {
                    total += (e.pu * e.quantite);
                })
            }
            return total
        },
        updateDate: function(index) {
            var dat = new Date();
            var d = dat.getDate().toString();
            var m = (dat.getMonth() + 1).toString();
            var min = (dat.getMinutes()).toString();
            var fulldate = $rootScope.user.pseudo + " " + dat.getHours() + "h" +
                (min[1] ? min : "0" + min[0]) + " " + (d[1] ? d : "0" + d[0]) + "/" +
                (m[1] ? m : "0" + m[0]) + "/" + dat.getFullYear()
            this.fourniture[index].date = fulldate;
        }
    }

}]);
