angular.module('edison').directive('infoOvh', function(mapAutocomplete, edisonAPI, config, $sce) {
    "use strict";
    return {
        restrict: 'E',
        templateUrl: '/Templates/info-ovh.html',
        scope: {
            data: "=",
        },
        link: function(scope, element, attrs) {
            scope.embedded = !!attrs.embedded

	    scope.trustUrl = function (url) {
		return $sce.trustAsResourceUrl(url);
	    }
        },
    }

 });
