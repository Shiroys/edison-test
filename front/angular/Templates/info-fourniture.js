angular.module('edison').directive('infoFourniture', ['config', 'fourniture',function(config, fourniture, edisonAPI, user) {
        "use strict";
        return {
            restrict: 'E',
            templateUrl: '/Templates/info-fourniture.html',
            scope: {
                data: "=",
                display: "=",
                small:"="
            },
            link: function(scope, element, attrs) {
		scope.isAuto = false;
		if (scope.data.artisan)
		{
		    edisonAPI.artisan.get(scope.data.artisan.id).then(function (resp) {
			if (resp.data.formeJuridique == "AUT")
			    scope.isAuto = true;
		    })
		}
		
		scope.validefourni = function (){
		    var dateStr = new Date();
		    var dArr = [];
		    dArr = _.words(dateStr), /[^- ]+/;
		    dArr[1] = dateStr.getMonth() + 1;
		    var datev = dArr[2] + "/" + dArr[1] + "/" + dArr[3];
		    var ids = scope.data._id
		    edisonAPI.intervention.validatefourn({id: ids, login: user.login, date: datev}).then(function (resp){
			scope.data.fourndate = resp.data.fourndate
			scope.data.fournpar = resp.data.fournpar
			scope.datefour = resp.data.fourndate
			scope.parfour = resp.data.fournpar
		    })
		}
		scope.parfour = scope.data.fournpar
		scope.datefour = scope.data.fourndate
                scope.config = config
                scope.dsp = scope.display || false
                scope.data.fourniture = scope.data.fourniture || [];
                scope.fourniture = fourniture.init(scope.data.fourniture);
            },
        }

    }
]);
