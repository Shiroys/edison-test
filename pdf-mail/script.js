var fs = require('fs');
var pdf = require('html-pdf-wth-rendering');
var html = fs.readFileSync('./HTML/index.html', 'utf8')
var html2 = fs.readFileSync('./HTML/index2.html', 'utf8')
var html3 = fs.readFileSync('./HTML/tableau.html', 'utf8')
var html4 = fs.readFileSync('./HTML/miseEnDemeure.html', 'utf8')
var html5 = fs.readFileSync('./HTML/opposition.html', 'utf8')
var html6 = fs.readFileSync('./HTML/desistement.html', 'utf8')
var html7 = fs.readFileSync('./HTML/annexe1Client.html', 'utf8')
var html8 = fs.readFileSync('./HTML/protocoleDaccord.html', 'utf8')
var html9 = fs.readFileSync('./HTML/annexe2Client.html', 'utf8')
var options = { 
	format: 'Letter',
};
 
pdf.create(html, {
  format: 'Letter',
}).toFile('relance.pdf', function(err, res) {
  if (err) console.log(err);
});

pdf.create(html2, options).toFile('miseEnDemeure.pdf', function(err, res) {
  if (err)  console.log(err);
});

pdf.create(html3, options).toFile('tableau.pdf', function(err, res) {
  if (err)  console.log(err);
});

pdf.create(html4, options).toFile('miseEnDemeureClient.pdf', function(err, res) {
  if (err)  console.log(err);
});

pdf.create(html5, options).toFile('opposition.pdf', function(err, res) {
  if (err)  console.log(err);
});

pdf.create(html6, options).toFile('desistement.pdf', function(err, res) {
  if (err)  console.log(err);
});

pdf.create(html7, options).toFile('annexe1Client.pdf', function(err, res) {
  if (err)  console.log(err);
});

pdf.create(html8, options).toFile('protocoleAccord.pdf', function(err, res) {
  if (err)  console.log(err);
});

pdf.create(html9, options).toFile('annexe2Client.pdf', function(err, res) {
  if (err)  console.log(err);
});