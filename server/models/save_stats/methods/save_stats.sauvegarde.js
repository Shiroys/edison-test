
module.exports = function(schema) {

  var cron = require('node-schedule');

  var rule2 = new cron.RecurrenceRule();
  rule2.dayOfWeek = [1,2,3,4,5,6,0];
  rule2.hour = [10,11,12,13,14,15,16,17,18,19];
  rule2.minute = 45;
  cron.scheduleJob(rule2, function(){
  console.log("done");
  db.model("save_stats").collection.drop(function(){
  var model2=  db.model("save_stats",schema)
  var now = new Date();

  db.model("intervention").aggregate([
    {
      $match:{ "date.ajout": { $gte: new Date(2013, 8, 1),
        $lt: new Date()},
        "status": "VRF",
        "compta.reglement.recu" : true,
        'reglementSurPlace' : false}

      },
      {
        $group: {
        _id:/*{jour: {$dayOfMonth: "$date.ajout" }}*/'$date.ajout',

        recu: { "$sum": { $cond: [ { $eq: [ "$prixFinal", 0 ] } , "$prixAnnonce","$prixFinal" ] } },

      }
    },
    //trie
    {
      $sort:
      {
        _id: 1,
      }
    }
  ],function(err, res){
    if (err) console.log(err);
    else {

      for (var i in res){
      var model3 = new model2()
      //model3.id=res[i]._id,
      model3.montant= res[i].recu/2,
      model3.date = res[i]._id,
      model3.save(function(err){
        if(err){
          console.error(err);
        }
      });
    }
      console.log(res);
      //console.log('res =='+res[0].recu);
      //  db.close()
    }
  });
})
})
}
