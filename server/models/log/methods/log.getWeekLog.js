module.exports = function(schema) {
    var moment = require('moment');

    schema.statics.getWeekLog = function(req, res) {
	return new Promise(function(resolve, reject) {
	    db.model('log').findOne({login: req.query.login}).then(function (resp) {
		var ret = ['Abs', 'Abs', 'Abs', 'Abs', 'Abs', 'Abs', 'Abs'];
		if (resp !== null)
		{
		    var weekStart = new Date(moment(req.query.weekStart, "DD-MM-YYYY"));
		    var weekEnd = new Date(moment(req.query.weekStart, "DD-MM-YYYY"));
		    weekEnd.setDate(weekEnd.getDate() + 6);
		    for (var n = 0; n < resp.history.length; n++)
		    {
			if (resp.history[n].getTime() >= weekStart.getTime() &&
			    resp.history[n].getTime() <= weekEnd.getTime())
			{
			    ret[resp.history[n].getDay() - 1] = resp.history[n].getHours() + ":" + resp.history[n].getMinutes() + ":" + resp.history[n].getSeconds();
			}
		    }
		    resolve(ret);
		}
		else
		    resolve(ret);
	    })
	})
    }

}
