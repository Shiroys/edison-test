module.exports = function(schema) {
    var moment = require('moment');

    schema.statics.getLate = function(req, res) {
	return new Promise(function(resolve, reject) {
	    db.model('log').findOne({login: req.query.login}).then(function (resp) {
		var ret = {late: [0, 0, 0, 0, 0, 0, 0], excused: [false, false, false, false, false, false, false]};
		db.model('user').findOne({login: req.query.login}).then(function (user) {
		    if (resp !== null && user !== null)
		    {
			var weekStart = new Date(moment(req.query.weekStart, "DD-MM-YYYY"));
			var weekEnd = new Date(moment(req.query.weekStart, "DD-MM-YYYY").add(6, 'days'));
			for (var n = 0; n < resp.history.length; n++)
			{
			    var day = moment(new Date(resp.history[n])).year(2000).hours(0).minute(0).second(0).millisecond(0)
			    var hour = moment(user.hours[resp.history[n].getDay() - 1]).year(2000).date(1).month(1);
			    if (resp.history[n].getTime() >= weekStart.getTime() &&
				resp.history[n].getTime() <= weekEnd.getTime() &&
				!(hour.hours() == 0 && hour.minutes() == 0))
			    {
				for (var l = 0; l < user.excuseLate.length; l++)
				{
				    var date2 = moment(new Date(user.excuseLate[l])).year(2000).hours(0).minute(0).second(0).millisecond(0)
				    if (day.diff(date2, 'day') === 0)
				    {

					var limit2 = moment(user.hours[resp.history[n].getDay() - 1]).year(2000).date(1).month(1)
					var arrive2 = moment(resp.history[n]).year(2000).date(1).month(1)
					ret.late[(resp.history[n].getDay() - 1)] = arrive2.diff(limit2, 'minutes')
					ret.excused[(resp.history[n].getDay() - 1)] = true;
				    }
				    else
				    {
					var limit2 = moment(user.hours[resp.history[n].getDay() - 1]).year(2000).date(1).month(1)
					var arrive2 = moment(resp.history[n]).year(2000).date(1).month(1)
					ret.late[(resp.history[n].getDay() - 1)] = arrive2.diff(limit2, 'minutes')
				    }
				}
				for (var l = 0; l < user.countLate.length; l++)
				{
				    var date = moment(new Date(user.countLate[l])).year(2000).hours(0).minute(0).second(0).millisecond(0)
				    if (day.diff(date, 'day') === 0)
				    {

					var limit = moment(user.hours[resp.history[n].getDay() - 1]).year(2000).date(1).month(1)
					var arrive = moment(resp.history[n]).year(2000).date(1).month(1)
					ret.late[(resp.history[n].getDay() - 1)] = arrive.diff(limit, 'minutes')
					ret.excused[(resp.history[n].getDay() - 1)] = false;
				    }
				    else
				    {
					var limit = moment(user.hours[resp.history[n].getDay() - 1]).year(2000).date(1).month(1)
					var arrive = moment(resp.history[n]).year(2000).date(1).month(1)
					ret.late[(resp.history[n].getDay() - 1)] = arrive.diff(limit, 'minutes')
				    }
				}
			    }
			}
			resolve(ret);
		    }
		    else
			resolve(ret);
		})
	    })
	})
    }

}
