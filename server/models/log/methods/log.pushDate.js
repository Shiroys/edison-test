module.exports = function(schema) {

    schema.statics.pushDate = function(req, res) {
	return new Promise(function(resolve, reject) {
	    db.model('log').findOne({login: req}).then(function (resp) {
		if (resp === null)
		{
		    var defaultLog = {}
		    db.model('log').count({}).then(function (respC) {
			defaultLog.id = respC + 1;
			defaultLog._id = respC + 1;
			defaultLog.login = req
			defaultLog.history = [];
			defaultLog.history.push(new Date());
			db.model('log')(defaultLog).save().then(resolve, reject);
		    })
		}
		else
		{
		    var date = resp.history[resp.history.length - 1]
		    date.setHours(0,0,0,0);
		    var today = new Date();
		    today.setHours(0,0,0,0);
		    if (date.getTime() === today.getTime())
			resolve('ok')
		    else
		    {
			resp.history.push(new Date());
			resp.save().then(resolve, reject);;
		    }
		}
	    })
	    resolve('ok');
	})
    }
}

