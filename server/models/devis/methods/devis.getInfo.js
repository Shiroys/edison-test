module.exports = function(schema) {
    var async = require('async')
    var moment = require('moment')

    schema.statics.getInfo = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var ret = {}
	    async.waterfall([
		function (cb) {
		    var date = new Date()
		    date.setHours(0,0,0,0)
		    db.model('devis').find({'login.envoi': req.query.login, 'date.envoi': {$gt: date}}).lean().then(function (resp) {
			ret.env = resp.length;
			cb(null);
		    })
		},
		function (cb) {
		    var date = new Date(moment().startOf('isoweek').toDate())
		    date.setHours(0,0,0,0)
		    db.model('devis').find({'login.transfert': req.query.login, 'date.transfert': {$gt: date}}).lean().then(function (resp) {
			ret.acc = resp.length;
			cb(null);
		    })
		},
	    ], function (error){
		if (error)
		    reject(error)
		else
		    resolve(ret)
	    })
	})
    }
}
