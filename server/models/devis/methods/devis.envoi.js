module.exports = function(schema) {
    var _ = require('lodash')
    var moment = require('moment-timezone')
    var PDF = requireLocal('pdf-mail')

    var getDevisPdfObj = function(doc) {
        doc.facture = doc.client;
        doc.facture.tel = doc.client.telephone.tel1;
        doc.datePlain = moment.tz(doc.date.ajout, 'Europe/Paris').format('LL');
        doc.user = req.session;
        doc.acquitte = false;
        doc.type = "devis"
        return PDF([{
            model: 'facture',
            options: doc
        }, {
            model: 'conditions',
            options: doc
        }], 500)
    }

    schema.statics.envoi = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(devis, req, res) {
            return new Promise(function(resolve, reject) {
                if (!devis && !devis.produits || !devis.produits.length)
                    return reject('Le devis est vide')
                if (!isWorker) {
                    return edison.worker.createJob({
                        name: 'db_id',
                        model: 'devis',
                        method: 'envoi',
                        data: devis,
                        req: _.pick(req, 'body', 'session'),
                    }).then(function() {
                        devis.historique.push({
                            login: req.session.login,
                            date: new Date,
                            auto: req.body.auto
                        })
                        devis.status = 'ATT';
                        edison.event('DEVIS_ENVOI').login(req.session.login).id(devis.id).save();
						devis.date.envoi = new Date
						devis.login.envoi = req.session.login
                        devis.save().then(resolve, reject)
                    }, reject)
                }
                var pdf = getDevisPdfObj(devis);
                pdf.toBuffer(function(err, buffer) {
                    try {
                        var communication = {
                            mailDest: devis.client.email ? devis.client.email : ('contact@edison-services.fr'),
                            mailReply: (req.session.email ||  'noreply.edison@gmail.com')
                        }
                    } catch (e) {
                        console.log("Error devis.envoi: ", e.stack);
                    }
                    if (devis.historique.length == 0) {
                        var txt = "DEVIS n°" + devis.id;
                    } else if (devis.historique.length === 1) {
                        var txt = "Suite au devis n°" + devis.id;
                    } else if (devis.historique.length >= 2) {
                        var txt = "Relance concernant le devis n°" + devis.id;
                    }
        		    var mailRec = communication.mailDest;
        		    if (devis.client && devis.client.email2 && envProd)
        			mailRec += ";" + devis.client.email2;
                    mail.send({
                        From: "contact@edison-services.fr",
                        ReplyTo: communication.mailReply,
                        To: mailRec,
                        Subject: txt,
                        HtmlBody: req.body.text.replaceAll('\n', '<br>'),
                        Attachments: [{
                            Content: buffer.toString('base64'),
                            Name: "Devis n°" + devis.id + '.pdf',
                            ContentType: 'application/pdf'
                        }]
                    }).then(resolve, reject);
                })
            })
        }
    }
}
