module.exports = function(schema) {
    var moment = require('moment')
    
    schema.statics.cancel = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var day = moment(req.body.week, "DD-MM-YYYY").add(req.body.nb, 'day')
	    db.model('user').findOne({login: req.body.login}).then(function (resp) {
		for (var n = 0; n < resp.countLate.length; n++)
		{
		    var date = moment(new Date(resp.countLate[n]))
		    if (day.diff(date, 'day') === 0)
		    {
			var late = resp.countLate.splice(n--, 1)
			resp.excuseLate.push(late[0]);
			resp.excused += 1;
		    }
		}
		resp.save().then(resolve, reject);
	    })
	})
    }

}
