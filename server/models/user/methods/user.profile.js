module.exports = function(schema) {
    var fs = require('fs');
    
    schema.statics.upload = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var bytes = req.body.file;
	    var b = new Buffer(bytes.length)

	    for (var n = 0; n < bytes.length; n++)
		b[n] = bytes[n]
	    fs.writeFile(process.cwd() + '/front/assets/img/profile/' + req.body.login + '.png',
			 b, "binary", function (err) {
			     if (err)
				 reject(err)
			     else
				 resolve("ok");
			 })
	})
    }
    
    schema.statics.profile = {
	unique: true,
	findBefore: true,
	method: 'GET',
	fn: function(user, req, res) {
	    if (fs.existsSync(process.cwd() + '/front/assets/img/profile/' + user.login + '.png')) {
		res.sendFile(process.cwd() + '/front/assets/img/profile/' + user.login + '.png');
	    } else {
		res.sendFile(process.cwd() + '/front/assets/img/profile/default.jpg');
	    }
	}
    }
}
