module.exports = function(schema) {
    var _ = require('lodash')
    var template = requireLocal('config/textTemplate');
    var moment = require('moment');
    var PDF = requireLocal('pdf-mail');

    var dateFormat = function (date) {
	return (moment(date).format("DD/MM/YYYY"))
    }

    var dateString = function (array) {
	return dateFormat(array[array.length - 3]) +
	    ", " + dateFormat(array[array.length - 2]) +
	    ", " + dateFormat(array[array.length - 1])
    }

    var dateAv = function (array) {
	var ret = "Vous avez reçu également un avertissement à ce sujet – le ("
	if (((array.length + 1) / 4) % 3 === 1)
	    return "";
	else if (((array.length + 1) / 4) % 3 === 2)
	    return ret + dateFormat(array[array.length - 4]) + ").\n\n"
	else
	    return ret + dateFormat(array[array.length - 4]) +
	    ", " + dateFormat(array[array.length - 8]) + ").\n\n"
	    
    }

    var getDates = function (array) {
	var date = []
	date.push(dateFormat(array[array.length - 1]))
	date.push(dateFormat(array[array.length - 2]))
	if (((array.length + 1) / 4) % 3 === 2)
	    date.push(dateFormat(array[array.length - 4]))
	else if (((array.length + 1) / 4) % 3 === 0)
	{
	    date.push(dateFormat(array[array.length - 4]))
	    date.push(dateFormat(array[array.length - 8]))
	    var future = moment().add(11, 'days')
	    while (future.day() >= 6)
		future.add(1, 'days')
	    date.push(future.format("DD/MM/YYYY"))

	}
	return date;
    }

    var sendMail = function (resp, req) {
		var text = "L'utilisateur " + resp.login + " s'est connecté à " + moment().format('LLLL') + "<br>Nombre de retard(s): " + (resp.countLate.length + 1 + resp.excused)
		var obj = resp.login + "viens de se connecter";
		var option = {
		    From: "contact@edison-services.fr",
		    To: "connexion.edison@gmail.com",
		    Subject: obj,
		    HtmlBody: text,
		    Attachments: []
		}
		mail.send(option);
    }

    schema.statics.mailRetard = function(req, res) {
	return new Promise(function(resolve, reject) {
	    db.model('user').findOne({_id: req}).then(function (resp) {
		db.model('log').findOne({login: resp.login}).then(function (log) {
		    sendMail(resp, req)
		    var hour = moment(resp.hours[moment().day() - 1]).year(2000).date(1).month(1)
		    var now = moment().year(2000).date(1).month(1)
		    if (log && log.history.length > 0)
			var loggedin = moment(new Date(log.history[log.history.length - 1])).hours(0).minute(0).second(0).millisecond(0)
		    else
			var loggedin = moment("2015-12-25")
		    if (resp.activated === true && !(hour.hours() == 0 && hour.minutes() == 0) &&
			resp.late === true && now.diff(hour, 'minutes') > 15 &&
			moment().hours(0).minute(0).second(0).millisecond(0).diff(loggedin, 'days') !== 0)
		    {
			if (resp.countLate.length % 4 != 3)
			{
			    var text = template.mail.user.retard(resp.countLate.length % 4);
			    var obj = "Retard n°" + (resp.countLate.length + 1 + resp.excused) + " - " + moment().format('LLLL') + " - " + req;
			    text = _.template(text)(resp).replaceAll('\n', '<br>')
			    var option = {
				From: "contact@edison-services.fr",
				To: resp.email + ";comptabilite@edison-services.fr;contact@edison-services.fr",
				Subject: obj,
				HtmlBody: text,
				Attachments: []
			    }
			    mail.send(option);
			    resp.countLate.push(new Date());
			    resp.save().then(resolve, reject);
			}
			else
			{
			    var text = template.mail.user.avertissement(dateString(resp.countLate), dateAv(resp.countLate), ((resp.countLate.length + 1) / 4) % 3);
			    var obj = "Lettre d'avertissement pour cause de retards n°" + (((resp.countLate.length + 1) / 4) % 3 === 0 ? 3 : ((resp.countLate.length + 1) / 4) % 3)
			    text = _.template(text)(resp).replaceAll('\n', '<br>')
			    var textPDF = template.mail.user.avertissementPDF(getDates(resp.countLate), ((resp.countLate.length + 1) / 4) % 3);
			    textPDF = _.template(textPDF)(resp).replaceAll('\n', '<br>')
			    
			    var letter = {
				address: {
				    r: resp.address.r,
				    n: resp.address.n,
				    cp: resp.address.cp,
				    v: resp.address.v
				},
				dest: {
				    civilite: 'M.',
				    prenom: resp.prenom,
				    nom: resp.nom
				},
				text: textPDF,
				id: '',
				date: new Date(),
				factureQrCode: false,
				title: "Lettre d'avertissement pour retards injustifiés n°" + (((resp.countLate.length + 1) / 4) % 3 === 0 ? 3 : ((resp.countLate.length + 1) / 4) % 3)
			    }
			    PDF('letter', letter).buffer(function(err, buffer) {
				var option = {
				    From: "contact@edison-services.fr",
				    To: resp.email + ";comptabilite@edison-services.fr;contact@edison-services.fr",
				    Subject: obj,
				    HtmlBody: text,
				    Attachments: [{
					Content: buffer.toString('base64'),
					Name: obj,
					ContentType: 'application/pdf'
				    }]
				}
				mail.send(option);
				resp.countLate.push(new Date());
				resp.save().then(resolve, reject);
			    })   
			}
		    }
		})
	    })
	})
    }

    schema.statics.mailAbs = function(req, res) {
	return new Promise(function(resolve, reject) {
	    db.model('log').find({}).then(function (resp) {
		for (var n = 0; resp.length; n++)
		{
		    if (resp[n].history.length > 0 &&
			moment().diff(moment(new Date(resp[n].history[resp[n].history.length - 1])), 'days') === 1)
		    {
			db.model('user').findOne({login: resp[n].login}).then(function (user) {
			    var hour = moment(user.hours[moment().day() - 1]).date(1).month(1)
			    if (user.abs === true && user.activated === true &&
				!(hour.hours() == 0 && hour.minutes() == 0))
			    {
				var text = template.mail.user.absence(moment().format('LL'));
				text = _.template(text)(user).replaceAll('\n', '<br>')
				var option = {
				    From: "contact@edison-services.fr",
				    To: user.email + ";comptabilite@edison-services.fr;contact@edison-services.fr",
				    Subject: "Absence: " + user.email,
				    HtmlBody: text,
				    Attachments: []
				}
			    }
			})
		    }
		}
		resolve('ok');
	    })
	})
    }
}
