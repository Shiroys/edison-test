module.exports = function(db) {

    return new db.Schema({
    Action: Boolean,
    Send: Boolean,
	nom: String,
	prenom: String,
	portable: String,
	email: String,
	nom: String,
	prenom: String,
	pseudo: String,
	maxInterAverif: {
	    type: Number,
	    default: 100
	},
	login: {
	    type: String,
	    required: true
	},
	right: {
	    stats: Boolean,
	    grandComptes: Boolean,
	    envoiInterventions: Boolean,
	},
	oldLogin: String,
	id: Number,
	_id: String,
	service: String,
	dayVerif: {
	    type: Number,
	    default: 1
	},
	limit: {
	    type: Number,
	    default: 15
	},
	address: {
	    n: String,
	    r: String,
	    v: String,
	    cp: String
	},
	ligne: String,
	root: Boolean,
	admin: Boolean,
	password: String,
	priv: {
	    type: Boolean,
	    default: false
	},
	obs: {
	    type: Boolean,
	    default: false
	},
	passInit: {
	    type: Boolean,
	    default: false
	},
	activated: {
	    type: Boolean,
	    default: false
	},
	hours: [],
	hoursf: [],
	late: {
	    type: Boolean,
	    default: false 
	},
	countLate: [],
	excuseLate: [],
	excused: {
	    type: Number,
	    default: 0
	},
	abs: {
	    type: Boolean,
	    default: false 
	}
    });

}
