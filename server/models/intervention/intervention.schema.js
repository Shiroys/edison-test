module.exports = function(db) {

	var schema = new db.Schema({
		_id: { type: Number, index: true },
		id: { type: Number, index: true },
		status: { type: String, index: true, default: 'APR' },
		urgence: { type: Boolean, default: false },
		sav: { type: Boolean, default: false },
		oldid: Number,
		statusSAV: String,
		demandeClient: Number,
		demandeCommerciale: Number,
		dumpValue: {
			artisan: Number,
			edison: Number,
		},
		subStatus: String,
		needDevis: Boolean,
		causeAnnulation: String,
		validefourn: Boolean,
		fourndate: String,
		fournpar: String,
		fournfile: String,
		fourndata:[{
			login: String,
			statut: String,
			date: Date
		}],
		login: {
			ajout: { type: String, index: true },
			envoi: { type: String, index: true },
			envoiFacture: String,
			annulation: String,
			verification: String,
			verifMajInternet: String,
			refusMajInternet: String,
			demarchage: String,
		},
		date: {
			envoi: Date,
			ajout: { type: Date, index: true, default: Date.now },
			modifs: [{
				date: Date,
				login: String,
			}],
			commissionPartenariat: Date,
			envoiFacture: Date,
			intervention: Date,
			majInternet: Date,
			verifMajInternet: Date,
			refusMajInternet: Date,
			verification: Date,
			annulation: Date,
			demarchage: Date,
			dump: Date,
		},
		comments: [{
			login: String,
			text: String,
			date: Date
		}],
		historique: [],
		client: {
			civilite: { type: String, required: true },
			prenom: { type: String, default: "" },
			nom: { index: true, type: String, required: true },
			email: String,
			email2: String,
			telephone: {
				tel1: { type: String, required: true, index: true },
				tel2: { type: String, index: true },
				tel3: { type: String, index: true },
				origine: String,
				appel: String,
			},
			address: {
				n: { type: String, required: true },
				r: { type: String, required: true },
				v: { index: true, type: String, required: true },
				cp: { index: true, type: String, required: true },
				etage: String,
				code: String,
				batiment: String,
				lt: Number,
				lg: Number,
			},
			location: [],
		},
		facture: {
			compte: String,
			payeur: String,
			nom: String,
			prenom: String,
			tel: { type: String, index: true },
			tel2: { type: String, index: true },
			email: String,
			email2: String,
			papier: String,
			mailed: String,
			address: {
				n: String,
				r: String,
				v: String,
				cp: String,
			},
		},
		sav: {
			status: String,
			description: String,
			sst: Number,
		},
		sst: { type: Number, ref: 'artisan' },
		litige: {
			openedBy: String,
			closedBy: String,
			opened: Date,
			closed: Date,
			description: String,
			open: Boolean
		},
		categorie: { type: String, required: true },
		artisan: {
			id: { type: Number, index: true, ref: 'artisan' },
			subStatus: { type: String, index: true },
			nomSociete: String,
			login: {
				management: String,
			},
			moneyStore: {
				seuil: { type: Number, default: 0 },
				limit: { type: Number, default: 0 },
			}
		},
		description: { type: String, required: true },
		remarqueSms: { type: Boolean, default: true },
		remarque: { type: String, default: 'PAS DE REMARQUES' },
		produits: [{
			pu: Number,
			quantite: Number,
			title: String,
			ref: String,
			desc: String
		}],
		cb: {
			hash: String,
			preview: String,
			/* never clear*/
			cardType: String,
			number: String,
			expMonth: String,
			expYear: String,
			cvc: String,
		},
		fourniture: [{
			date: { type: String, default: 'Pas d\'historique' },
			bl: String,
			pu: Number,
			quantite: Number,
			title: String,
			fournisseur: String
		}],
		combo: String,
		modeReglement: { type: String, default: 'CHQ', index: true },
		prixAnnonce: { type: Number, default: 0 },
		prixFinal: { type: Number, default: 0 },
		prixInitial: { type: Number, default: 0 },
		acompte: Number,
		reglementSurPlace: { type: Boolean, default: true },
		recouvRenf: Boolean,
		aDemarcher: { type: Boolean, default: false },
		favoris: { type: Boolean, default: false },
		enDemarchage: { type: Boolean, default: false },
		demarchePar: String,
		devisOrigine: { type: Number, ref: 'devis' },
		fournisseur: String,
		coutFourniture: { type: Number, default: 0 },
		tva: { type: Number, default: 20 },
		compta: {
			recouvrement: {
				litige: {
					status: { type: Number, default: 0 },
					userCreate: { type: String },
				},
				priseEnCharge: { type: Number, default: 0 },
				datePrise: Date,
				devisSigne: { type: Boolean, default: false },
				mailInter: { type: Boolean, default: false },
				coordFactIdent: { type: Boolean, default: false },
				causeRec: { type: String, default: "none" },
				priseDeContact: {
					dateRelance: { type: Date },
					comment: { type: Array },
					conclu: { type: String, default: "none" },
					numCheque: { type: String },
					banque: { type: String },
					montant: { type: String },
					emisLe: { type: String },
					sommeGlob: { type: String },
					statusRec: { type: String, default: "none" },
				},
			},
			encaissement: [{
				num: { type: String },
				dateSaisie: { type: Date },
				dateEncaissement: { type: Date },
				mode: { type: String },
				montant: { type: Number },
				tva: { type: String },
				agent: { type: String },
				client: { type: String },
				end: { type: Boolean, default: false },
				mult: { type: Boolean, default: false }
			}],
			decaissement: [{
				num: { type: String },
				dateSaisie: { type: Date },
				mode: { type: String },
				montant: { type: Number },
				tva: { type: Number },
				motif: { type: String },
				agent: { type: String }
			}],
			reglement: {
				date: Date,
				login: { type: String, default: 'vincent_q' },
				recu: { type: Boolean, default: false, index: true },
				montant: { type: Number, default: 0 },
				avoir: { 
					_type: String,
					montant: Number,
					effectue: { type: Boolean, default: false },
					date: Date,
					numeroCheque: String,
				},
				historique: [{
					_type: { type: String, default: 'ENCAISSEMENT' },
					_typeAvoir: String,
					montant: Number,
					login: { type: String, default: 'vincent_q' },
					date: { type: Date, default: Date.now },
					numeroCheque: String,
				}]
			},
			paiement: {
				mode: String,
				tva: { type: Number, default: 0 },
				base: Number,
				montant: Number,
				pourcentage: { deplacement: Number, maindOeuvre: Number, fourniture: Number },
				dette: { type: Boolean, default: false },
				effectue: { index: true, type: Boolean, default: false },
				ready: { index: true, type: Boolean, default: false },
				date: Date,
				login: { type: String, default: 'vincent_q' },
				historique: [{
					tva:  { type: Number, default: 0 },
					fourniture: { artisan: Number, edison: Number, },
					dateAjout: Date,
					dateFlush: Date,
					loginAjout: { type: String, default: 'vincent_q' },
					loginFlush: { type: String, default: 'vincent_q' },
					_type: { type: String, default: 'AUTO-FACT' },
					pourcentage: {
						deplacement: { type: Number, default: 50 },
						maindOeuvre: { type: Number, default: 30 },
						fourniture: { type: Number, default: 30 }
					},
					mode: String,
					base: Number,
					montant: Number,
					payed: Number,
					'final': Number,
					numeroCheque: String
				}]
			},
			info: {
				factureNC: Boolean, // facture de l'intervention (si reglemenet est sur place)
				tvaNC: Boolean, // attestation de tva (si tva=10%)
				devisNC: Boolean, // devis (> 150)
				fournitureNC: Boolean,
				fournitureSMS:Boolean,
				fournitureSmsText:String
			},
		},
		relancecheque: {
			level: { type: Number, default: 0 }
		},
		litige: {
			level: { type: Number, default: 0 }
		},
		recouvrement: {
			level: { type: Number, default: 0 },
			date: Date,
			login: String
		},
		partenariat: { type: Boolean, default: false },
		newOs: { type: Boolean, index: true, default: false },
		litigeCompta: Boolean,
		conversations: [{
			io: String,
			status: String,
			withoperator: String,
			from: String,
			poste: String,
			to: String,
			dest: String,
			duration: Number,
			_id: Date
		}],
		appels: [{
			call_id: { type: String, index: true },
			date: { type: Date, default: Date.now },
			status: String,
			duration: Number,
			_type: String, //CONTACT/CALLBACK
		}],
		sms: { ref: 'sms', type: String },
		smsStatus: Number,
		file: [{
			name: String,
			mimeType: String,
			origin: String, //SCAN, UPLOAD, AUTO
		}],
		cache: {}
	}, {
		versionKey: false
	});
	return schema
}
