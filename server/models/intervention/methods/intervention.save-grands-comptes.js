module.exports = function(schema) {

    schema.statics.saveGrandsComptes = function(req, res) {
        return new Promise(function(resolve, reject) {

	    db.model('intervention').find({'facture.payeur' : 'GRN'}).then( function (resp) {
                for (var n = 0; n < resp.length; n++)
                {
                    resp[n].array = true;
                    resp[n].save();
                }
                resolve("ok");
            });
        })
    }
}
