module.exports = function(schema) {

    var fs = require('fs');
    /* Fonction de liste des inter via request fichier */
    schema.statics.listInter = function(req, res) {
        return new Promise(function(resolve, reject) {
            var ind = 0;
            fs.writeFileSync('/home/abel/edsx/listeInter.json', "", 'utf8');
            db.model('intervention').find(req.body.fileId.inter, req.body.fileId.champ).cursor()
                .on('error', function(err) {
                    console.log("Error listInter: ", err);
                    reject(err);
                })
                .on('data', function(elem) {
                    ind++;
                    fs.appendFile('/home/abel/edsx/listeInter.json', elem + '\n', 'utf8', function(err) {
                        if(err)
                            console.log("Error appendfile: ", err);
                    })
                })
                .on('end', function() {
                    resolve("ok");
                })
        })
    }
}