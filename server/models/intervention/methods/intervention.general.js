module.exports = function(schema) {

    var fs = require("fs");
    var _ = require("lodash");
    var moment = require('moment');
    var jsonfile = require('jsonfile');
    var fs = require('fs');
    var jsonfile = require('jsonfile');
    var fs = require('fs');
    moment.locale('fr');

    /* Fonction compta demande email */
    schema.statics.comptaDemande = function(req, res) {
        return new Promise(function(resolve, reject){
            fs.writeFileSync('/home/abel/edsx/email.json', "")
            var arraySst = [];
            var file = "/home/abel/edsx/email.json";
            var index = 0;
            db.model('intervention').find({
                "compta.paiement.historique.dateFlush": {$gte: new Date(2017,04,01)},
                "compta.paiement.effectue": true },
                { sst: 1 })
            .cursor()
                .on('error', function(err) {
                    console.log("Error cursor demande mail: ", err);
                })
                .on('data', function(elem) {
                    console.log("------", elem);
                    db.model('artisan').findOne({ "id": elem.sst }, { id: 1, email: 1 }).exec(function(err, artisan) {
                        if (err) {
                            console.log("Error find artisan: ", err)
                        } else {
                            var count = 0;
                            for (var ind = 0; ind < arraySst.length; ind++) {
                                if (artisan.id == arraySst[ind].id) {
                                    count++;
                                }
                            }
                            if (count == 0) {
                                jsonfile.writeFileSync(file, { id: artisan.id, email: artisan.email }, { flag: 'a' });
                                arraySst.push({id: artisan.id});
                                console.log("ARRAY: ", arraySst);
                            }
                        }
                    })
                    index++;
                })
                .on('end', function() {
                    console.log("INDEX: ", index);

                })
        })
    }

    /* Fonction pour recup donnée client ou adresse facture = adresse intervention */
    schema.statics.getDatasOnFactureAndClientNames = function(req, res) {
        return new Promise(function(resolve, reject){
            db.model('intervention').find({}, {id: 1, "reglement.login": 1, "reglement.reçu": 1, "cache.s": 1, "client.nom": 1, "facture.nom": 1, "facture.prenom": 1, "client.prenom": 1})
            .lean().exec(function(err, response){
                fs.writeFile('/home/abel/edsx/datas.json', "")
                for (var ind = 0; ind < response.length; ind++)
                {
                    var datas;
                    var file = '/home/abel/edsx/datas.json';
                    if(response[ind].facture && response[ind].client && response[ind].client.nom == response[ind].facture.nom && response[ind].client.prenom == response[ind].facture.prenom)
                    {
                        datas = response[ind];
                        jsonfile.writeFileSync(file, datas, {flag: 'a'});
                    }
                }
            })
        })
    }

    /* Fonction de lancement mail artisan pour les inter a vérifier*/
    schema.statics.mailAverif = function(req, res) {
        return new Promise(function(resolve, reject) {
            var start = new Date(2017, 1, 1);
            var end = new Date();
            db.model('artisan').find({ majInternet: true }).cursor()
                .on('error', function(err) {
                    console.log("Error mailAverif", err);
                    reject(err);
                })
                .on('data', function(elem) {
                    db.model('intervention').find({ "cache.s": 3, "reglementSurPlace": true, "artisan.id": elem.id }, { "id": 1, "artisan": 1, "date": 1, "client": 1, "prixAnnonce": 1, "description": 1 })
                    .lean().exec(function(err, resp) {
                        if (err)
                        {
                            console.log("Erreur find mail averif: ", err);
                            reject(err);
                        }
                        else
                        {
                            var count = 0;
                            var artiInter = {};
                            artiInter.list = [];
                            for (var index = 0; index < resp.length; index++)
                            {
                                if (elem.id == resp[index].artisan.id || elem.nomSociete == resp[index].artisan.nomSociete)
                                {
                                    artiInter.list.push(resp[index]);
                                    count++;
                                }
                            }
                            if (count == 1)
                            {
                                artiInter.list[0].newDate = moment(artiInter.list[0].date.intervention).format("dddd D MMMM YYYY à HH:mm");
                                fs.readFile(process.cwd() + '/pdf-mail/template/mailSolo.html', 'utf8', function(err, tempMail) {
                                    mail.send({
                                        From: "contact@edison-services.fr",
                                        ReplyTo: "noreply.edison@gmail.com",
                                        To: elem.email.toString(),
                                        Bcc: "noreply.edison@gmail.com",
                                        Subject: "Vous devez mettre à jour votre intervention.",
                                        HtmlBody: _.template(tempMail)(artiInter.list[0]),
                                    }).then(function(success){})
                                })
                            }
                            else if (count > 1)
                            {
                                for (var ind = 0; ind < artiInter.list.length; ind++)
                                    artiInter.list[ind].newDate = moment(artiInter.list[ind].date.intervention).format("dddd D MMMM YYYY à HH:mm");
                                fs.readFile(process.cwd() + '/pdf-mail/template/mailMulti.html', 'utf8', function(err, tempMail) {
                                    mail.send({
                                        From: "contact@edison-services.fr",
                                        ReplyTo: "noreply.edison@gmail.com",
                                        To: elem.email.toString(),
                                        Bcc: "noreply.edison@gmail.com",
                                        Subject: "Vous devez mettre à jour vos interventions.",
                                        HtmlBody: _.template(tempMail)(artiInter),
                                    }).then(function(success){})
                                })
                            }
                        }
                    })
                })
                .on('end', function() {
                    resolve("END");
                })
        })
    }

    /* Methods switch test */
    schema.statics.switchTest = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('signalement').find({}).cursor()
            .on('error', function(err) {
                console.log("ERROR: ", err);
                reject(err);
            })
            .on('data', function(elem) {
                db.model('intervention').findOne({ id: elem.inter_id }).exec(function(err, resp) {
                    if (resp.login.ajout)
                        elem.addBy = resp.login.ajout;
                    else
                        elem.addBy = 'Non ajoutée';
                    if (resp.login.verification)
                        elem.verifBy = resp.login.verification;
                    else
                        elem.verifBy = 'Non vérifiée';
                    console.log("ELEM id inter", elem.inter_id, "addBy: ", elem.addBy, "Verifby: ", elem.verifBy);
                    elem.save(function(err) {
                        if (err) {
                            console.log("ERROR SAVE: ", err);
                        }
                    })
                })
            })
            .on('end', function() {
                console.log("END");
                resolve('OK');
            })
        })
    }

    /* Fonction de liste des inter via request fichier */
    schema.statics.listInter = function(req, res) {
        return new Promise(function(resolve, reject) {
            var ind = 0;
            fs.writeFileSync('/home/abel/edsx/listeInter.json', "", 'utf8');
            db.model('intervention').find(req.body.fileId.inter, req.body.fileId.champ).cursor()
                .on('error', function(err) {
                    console.log("Error listInter: ", err);
                    reject(err);
                })
                .on('data', function(elem) {
                    ind++;
                    fs.appendFile('/home/abel/edsx/listeInter.json', elem + '\n', 'utf8', function(err) {
                        if(err)
                            console.log("Error appendfile: ", err);
                    })
                })
                .on('end', function() {
                    resolve("ok");
                })
        })
    }

    /* Fonction Reload du cache + update des interventions En cours en Vérifiées */
    schema.statics.reloadCache = function(req, res) {
        console.log("Begin reload cache");
        db.model('artisan').fullReload().then(function() {});
        db.model('intervention').fullReload().then(function() {});
        db.model('devis').fullReload().then(function() {});
    }

    /* Fonction de transformation de status (Outils Admin) */
    schema.statics.transSav = function(req, res) {
        return new Promise (function(resolve, reject) {
            var fileId = req.body.fileId;
            var numStatus = -1;
            if (req.body.nStatus == "ENC")
                numStatus = 0;
            else if (req.body.nStatus == "VRF")
                numStatus = 1;
            else if (req.body.nStatus == "APR")
                numStatus = 2;
            else if (req.body.nStatus == "AVR")
                numStatus = 3;
            else if (req.body.nStatus == "ANN")
                numStatus = 4;
            else if (req.body.nStatus == "SAV")
                numStatus = 5;
            else if (req.body.nStatus == "DVG")
                numStatus = 6;
            else if (req.body.nStatus == "PRT")
                numStatus = 7;
            else if (req.body.nStatus == "RGL")
                numStatus = 8;
            fileId.id.forEach(function(element) {
                if (req.body.nStatus == "SAV")
                {
                    db.model('intervention').find({_id:element}).then(function(resp) {
                        var saveStatus = resp[0].status;
                        db.model('intervention').update({_id:element}, {$set: {status:req.body.nStatus, "cache.s":numStatus, statusSAV:saveStatus, "cache.sa":saveStatus }}, function(err, resp) {
                            if (err)
                                console.log("Erreur tranfo status: ", err);
                        })
                    })
                }
                else
                {
                    db.model('intervention').update({_id:element}, {$set: {status:req.body.nStatus, "cache.s":numStatus, statusSAV:"", "cache.sa":"" }}, function(err, resp) {
                        if (err)
                            console.log("Erreur tranfo status: ", err);
                    })
                }
            })
            db.model('intervention').fullReload().then(function(err) {
                return (resolve("Ok"));
            });
        })
    }
}