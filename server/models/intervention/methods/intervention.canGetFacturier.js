module.exports = function(schema){
	schema.statics.canGetFacturier = function (req, res){
		return new Promise(function(resolve, reject){
			db.model('intervention').find({"artisan.id":req.body.id, "compta.reglement.recu":true}).limit(6).then(function(resp){
				resolve(resp.length > 5);
			});
		});
	}
}
