module.exports = function(schema) {

    var moment = require('moment');
    moment.locale('fr');
    /* Fonction d'update pour les interventions a vérifier*/
    schema.statics.updateVerifInter = function(req, res) {
        return new Promise(function(resolve, reject) {
            if (req.body.type == 1)
            {
                db1.mongoose1.model('made').findOne({ id: req.body.data.id.toString() }).lean().then(function(resp) {
                    db.model('intervention').findOne({ id: req.body.data.id }).then(function(updatedInter) {
                        if (resp.reglementChoice == "oui")
                            updatedInter.reglementSurPlace = true;
                        else
                            updatedInter.reglementSurPlace = false;
                        updatedInter.modeReglement = resp.reglement;
                        updatedInter.prixFinal = parseFloat(resp.priceHT).toFixed(2);
                        updatedInter.date.verifMajInternet = moment();
                        updatedInter.login.verifMajInternet = req.session.login;
                        updatedInter.cache.vf = req.session.login;
                        updatedInter.cache.s = 1;
                        updatedInter.status = "VRF";
                        if (resp.fourniturePrice != 0)
                        {
                            updatedInter.fourniture.push({
                                date: "artisan " + moment().format("HH") + "h" + moment().format("mm DD/MM/YYYY"),
                                bl: "0",
                                title: "Artisan Verif",
                                fournisseur: (resp.payedBy === "byEdison") ? resp.fournisseur : "ARTISAN",
                                pu: parseFloat(resp.fourniturePrice).toFixed(2),
                                quantite: 1
                            })
                        }
                        updatedInter.save(function(err, resp) {
                            if (err)
                            {
                                console.log("Error save updatedInter: ", err);
                                reject(err);
                            }
                            else
                            {
                                resolve("OK");
                            }
                        })
                    })
                })
            }
            else if (req.body.type == 2)
            {
                db1.mongoose1.model('pushedBack').findOne({ id: req.body.data.id.toString() }).lean().then(function(resp) {
                    db.model('intervention').findOne({ id: req.body.data.id }).then(function(updatedInter) {
                        var splitDate = resp.date.split(" ");
                        var momentObj = moment(splitDate[3] + "-" + splitDate[2] + "-" + splitDate[1] + " " + splitDate[4] + ":00", 'YYYY-MM-DD HH:mm');
                        updatedInter.date.intervention = momentObj;
                        updatedInter.login.intervention = req.session.login;
                        updatedInter.cache.s = 0;
                        updatedInter.status = "ENC";
                        updatedInter.cache.f.i_avr = 0;
                        updatedInter.save(function(err, resp) {
                            if (err)
                            {
                                console.log("Error save updatedInter: ", err);
                                reject(err);
                            }
                            else
                            {
                                db1.mongoose1.model('pushedBack').remove({ id: req.body.data.id.toString() }).then(function(respPush) {});
                                resolve("OK");
                            }
                        })
                    })
                })
            }
            else if (req.body.type == 3)
            {
                db1.mongoose1.model('canceled').findOne({ id: req.body.data.id.toString() }).lean().then(function(resp) {
                    db.model('intervention').findOne({ id: req.body.data.id }).then(function(updatedInter) {
                        updatedInter.date.annulation = moment();
                        updatedInter.login.annulation = req.session.login;
                        updatedInter.cache.s = 4;
                        updatedInter.cache.vf = req.session.login;
                        updatedInter.status = "ANN";
                        updatedInter.save(function(err, resp) {
                            if (err)
                            {
                                console.log("Error save updatedInter: ", err);
                                reject(err);
                            }
                            else
                            {
                                resolve("OK");
                            }
                        })
                    })
                })
            }
        })
    }
}