module.exports = function(schema) {
    schema.statics.printQ = {
	unique: true,
	findBefore: true,
	method: 'POST',
	fn: function(inter, req, res) {
	    return new Promise(function(resolve, reject) {
		var PDF = requireLocal('pdf-mail')
		var template = requireLocal('config/textTemplate');
		var _ = require('lodash')
		var moment = require('moment')
		var _this = this;
		var inter2 = JSON.parse(JSON.stringify(inter))
		var now = new Date()
		var month = now.getMonth() + 1
		var day = now.getDate()
		var year = now.getFullYear()
		var options = req.body
		var calcul = ((inter2.compta.reglement.montant * inter2.tva) / 100)
		var test = parseFloat(inter2.compta.reglement.montant + calcul)
		var test2 = parseFloat(inter2.compta.reglement.montant)
		inter2.compta.reglement.montantTTC = test.toFixed(2)
		inter2.compta.reglement.montant = test2.toFixed(2)
		if (req.body.params === "desis"){
		    var name = " LETTRE DESISTEMENT " + inter.id
		    PDF("lettre-desist",inter2).buffer(function(err, buffer) {
			document.upload({
			    filename: "/PrintQueue/" + [moment().format('L').replace(/\D/g, '-'), name, inter.login.ajout].join(' - ') + '.pdf',
			    data: buffer
			})

		    })
		}
		if (req.body.params === "desispers")
		{
		    var name = " LETTRE DESISTEMENT PERSONNALISÉ " + inter.id
		    var mmt = moment.tz(new Date(options.date), "Europe/Paris")
		    options.date = mmt.format("DD/MM/YYYY")
		    inter2.optionsC = options
		    PDF("lettre-desist-pers",inter2).buffer(function(err, buffer) {
			document.upload({
			    filename: "/PrintQueue/" + [moment().format('L').replace(/\D/g, '-'), name, inter.login.ajout].join(' - ') + '.pdf',
			    data: buffer
			})

		    })
		}
		if (req.body.params === "ssprovision")
		{
		    var name = " LETTRE CHEQUE SANS PROVISION " + inter.id
		    PDF("lettre-cheque-sspro",inter2).buffer(function(err, buffer) {
			document.upload({
			    filename: "/PrintQueue/" + [moment().format('L').replace(/\D/g, '-'), name, inter.login.ajout].join(' - ') + '.pdf',
			    data: buffer
			})

		    })
		}
		if (req.body.params === "opposition")
		{
		    var name = " LETTRE CHEQUE OPPOSITION " + inter.id
		    PDF("lettre-cheque-oppo",inter2).buffer(function(err, buffer) {
			document.upload({
			    filename: "/PrintQueue/" + [moment().format('L').replace(/\D/g, '-'), name, inter.login.ajout].join(' - ') + '.pdf',
			    data: buffer
			})

		    })
		}
		resolve("ok")
	    })
	}
    }
}
