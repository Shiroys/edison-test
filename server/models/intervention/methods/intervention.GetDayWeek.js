module.exports = function(schema) {
    var _ = require('lodash')
    var async = require('async');
    var moment = require('moment');
    schema.statics.GetDayWeek = function(req, res) {
	return new Promise(function(resolve, reject) {
	    db.model("intervention").find({
		'login.ajout': {$in: req.query.name},
		'login.envoi': {$exists: true},
		'status': 'ANN',
		'date.ajout': {$gt: req.query.start,
			       $lt: req.query.end}
	    },{'prixAnnonce': 1})
		.lean()
		.then(function(resp) {
		    var res = 0;
		    for (i = 0;i < resp.length;i++)
			res += resp[i].prixAnnonce;
		    resolve({elem: res});
		})
	});
    }
}
