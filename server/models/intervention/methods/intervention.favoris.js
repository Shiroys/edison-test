module.exports = function(schema) {

    schema.statics.favoris = function(req, res) {
        var _ = require('lodash')
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.id }).then(function(intFav) {
                if (intFav.favoris)
                    intFav.favoris = !intFav.favoris;
                else
                    intFav.favoris = true;
                edison.event('INTER_FAV').login(req.session.login).id(intFav.id)
                    .broadcast(intFav.login.ajout)
                    .color('orange')
                    .message(_.template("L'intervention {{id}} chez {{client.civilite}} {{client.nom}} ({{client.address.cp}}) a été ajoutée aux favoris")(intFav))
                    .send()
                    .save()
                intFav.save().then(function(resp) {
                    if (intFav.favoris === true)
                        resolve("OUI");
                    else if (intFav.favoris === false)
                        resolve("NON")
                    else
                        reject();
                });
            })
        })
    }
}