module.exports = function(schema) {
    var fs = require("fs");
    var _ = require("lodash");
    var moment = require('moment');
    moment.locale('fr');
    /* Fonction de lancement mail artisan pour les inter a vérifier*/
    schema.statics.mailAverif = function(req, res) {
        return new Promise(function(resolve, reject) {
            var start = new Date(2017, 1, 1);
            var end = new Date();
            db.model('artisan').find({ majInternet: true }).cursor()
                .on('error', function(err) {
                    console.log("Error mailAverif", err);
                    reject(err);
                })
                .on('data', function(elem) {
                    db.model('intervention').find({ "cache.s": 3, "reglementSurPlace": true, "artisan.id": elem.id }, { "id": 1, "artisan": 1, "date": 1, "client": 1, "prixAnnonce": 1, "description": 1 })
                    .lean().exec(function(err, resp) {
                        if (err)
                        {
                            console.log("Erreur find mail averif: ", err);
                            reject(err);
                        }
                        else
                        {
                            var count = 0;
                            var artiInter = {};
                            artiInter.list = [];
                            for (var index = 0; index < resp.length; index++)
                            {
                                if (elem.id == resp[index].artisan.id || elem.nomSociete == resp[index].artisan.nomSociete)
                                {
                                    artiInter.list.push(resp[index]);
                                    count++;
                                }
                            }
                            if (count == 1)
                            {
                                artiInter.list[0].newDate = moment(artiInter.list[0].date.intervention).format("dddd D MMMM YYYY à HH:mm");
                                fs.readFile(process.cwd() + '/pdf-mail/template/mailSolo.html', 'utf8', function(err, tempMail) {
                                    mail.send({
                                        From: "contact@edison-services.fr",
                                        ReplyTo: "noreply.edison@gmail.com",
                                        To: elem.email.toString(),
                                        Subject: "Vous devez mettre à jour votre intervention.",
                                        HtmlBody: _.template(tempMail)(artiInter.list[0]),
                                    }).then(function(success){})
                                })
                            }
                            else if (count > 1)
                            {
                                for (var ind = 0; ind < artiInter.list.length; ind++)
                                    artiInter.list[ind].newDate = moment(artiInter.list[ind].date.intervention).format("dddd D MMMM YYYY à HH:mm");
                                fs.readFile(process.cwd() + '/pdf-mail/template/mailMulti.html', 'utf8', function(err, tempMail) {
                                    mail.send({
                                        From: "contact@edison-services.fr",
                                        ReplyTo: "noreply.edison@gmail.com",
                                        To: elem.email.toString(),
                                        Subject: "Vous devez mettre à jour vos interventions.",
                                        HtmlBody: _.template(tempMail)(artiInter),
                                    }).then(function(success){})
                                })
                            }
                        }
                    })
                })
                .on('end', function() {
                    resolve("END");
                })
        })
    }
}