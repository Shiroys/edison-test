module.exports = function(schema) {

    schema.statics.saveEncaissement = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.data.id }).exec(function(err, saveInter) {
                if (err) {
                    console.log("Error find saveEncaissement: ", err);
                    reject(err);
                } else {
                    saveInter.compta.encaissement = req.body.data.compta.encaissement;
                    saveInter.save(function(err) {
                        if (err) {
                            console.log("Error save saveEncaissement: ", err);
                            reject(err);
                        } else {
                            resolve('OK');
                        }
                    })
                }
            })
        })
    }

    schema.statics.saveDecaissement = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.data.id }).exec(function(err, saveInter) {
                if (err) {
                    console.log("Error find saveDecaissement: ", err);
                    reject(err);
                } else {
                    saveInter.compta.decaissement = req.body.data.compta.decaissement;
                    console.log("LENGTH", saveInter.compta.decaissement.length);
                    for (var ind = 0; ind < saveInter.compta.decaissement.length; ind++) {
                        console.log("in boucle");
                        if (saveInter.compta.decaissement[ind].motif == 'opposition') {
                            console.log("IN OPPOSITION")
                            saveInter.compta.recouvrement.causeRec = "opposition";
                            saveInter.compta.recouvrement.litige.status = 1;
                        } else if (saveInter.compta.decaissement[ind].motif == 'sansProvision') {
                            console.log("IN INMPAYE")
                            saveInter.compta.recouvrement.causeRec = "impaye";
                            saveInter.compta.recouvrement.litige.status = 4;
                            saveInter.compta.recouvrement.priseEnCharge = 0;
                        }
                    }
                    saveInter.save(function(err) {
                        if (err) {
                            console.log("Error save saveDecaissement: ", err);
                            reject(err);
                        } else {
                            resolve("OK");
                        }
                    })
                }
            })
        })
    }
}