module.exports = function(schema) {
    var moment = require('moment');
    var fs = require('fs');
    var _ = require('lodash');
    var htmlpdf = require('html-pdf-wth-rendering');
    var PDF = requireLocal('pdf-mail');

    schema.statics.demandeSav = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.data.id }).exec(function(err, intSave) {
                mail.send({
                    From: "contact@edison-services.fr",
                    ReplyTo: "noreply.edison@gmail.com",
                    To: "secretariat.edison@gmail.com",
                    Subject: "Demande de SAV n°" + req.body.data.id,
                    HtmlBody: "Demande de SAV n°" + req.body.data.id,
                }).then(function(resp, err) {
                    if (err) {
                        console.log("Error send lettre de desistement: ", err);
                        reject(err);
                    }
                    resolve("YES");
                });
            })
        })
    }

    schema.statics.remiseCom = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.data.id }).exec(function(err, intSave) {
                intSave.compta.recouvrement = req.body.data.compta.recouvrement;
                intSave.prixInitial = req.body.data.compta.recouvrement.priseDeContact.montant;
                intSave.save(function(err, resp) {
                    if (err) {
                        console.log("Error save priseDeContact");
                        resolve ("NO")
                    } else
                        resolve ("YES");
                })
            })
        })
    }

    schema.statics.lettreDesist = function(req, res) {
        return new Promise(function(resolve, reject) {
            req.body.data.faitLe = moment().format("DD/MM/YY");
            var html = fs.readFileSync(process.cwd() + '/pdf-mail/template/desistementClient.html', 'utf8');
            html = _.template(html)(req.body.data);
            htmlpdf.create(html, { format: 'Letter' }).toFile(process.cwd() + '/templates/desistementClient.pdf', function(err, resp) {
                if (err) {
                    console.log("Error pdf create lettre desistement: ", err);
                    reject(err);
                }
                fs.readFile(process.cwd() + '/templates/desistementClient.pdf', 'base64', function(err, buffer) {
                    mail.send({
                        From: "contact@edison-services.fr",
                        ReplyTo: "noreply.edison@gmail.com",
                        To: "contentieux.edison@gmail.com;contentieux2.edison@gmail.com;contentieux3.edison@gmail.com",
                        Subject: "Lettre de désistement n°" + req.body.data.id,
                        HtmlBody: "Lettre de désistement n°" + req.body.data.id,
                        Attachments: [{
                            Content: buffer,
                            Name: "Lettre de désistement n°" + req.body.data.id + ".pdf",
                            ContentType: 'application/pdf'
                        }]
                    }).then(function(resp, err) {
                        if (err) {
                            console.log("Error send lettre de desistement: ", err);
                            reject(err);
                        }
                        resolve("YES");
                    });
                });
            })
        })
    }

    schema.statics.lettreOppo = function(req, res) {
        return new Promise(function(resolve, reject) {
            req.body.data.faitLe = moment().format("DD/MM/YY");
            req.body.data.interv = moment(req.body.data.date.intervention).format("DD/MM/YY");
            var html = fs.readFileSync(process.cwd() + '/pdf-mail/template/oppositionClient.html', 'utf8');
            html = _.template(html)(req.body.data);
            var html2 = fs.readFileSync(process.cwd() + '/pdf-mail/template/annexe1Client.html', 'utf8');
            html2 = _.template(html2)(req.body.data);
            var html3 = fs.readFileSync(process.cwd() + '/pdf-mail/template/annexe2Client.html', 'utf8');
            html3 = _.template(html3)(req.body.data);
            htmlpdf.create(html, { format: 'Letter' }).toFile(process.cwd() + '/templates/oppositionClient.pdf', function(err) {
                if (err) {
                    console.log("Error pdf create Lettre d'opposition: ", err);
                    reject(err);
                }
                html = fs.readFileSync(process.cwd() + '/templates/oppositionClient.pdf', 'base64');
                htmlpdf.create(html2, { format: 'Letter' }).toFile(process.cwd() + '/templates/annexe1Client.pdf', function(err) {
                    if (err) {
                        console.log("Error pdf create Lettre d'opposition annexe1: ", err);
                        reject(err);
                    }
                    html2 = fs.readFileSync(process.cwd() + '/templates/annexe1Client.pdf', 'base64');
                    htmlpdf.create(html3, { format: 'Letter' }).toFile(process.cwd() + '/templates/annexe2Client.pdf', function(err) {
                        if (err) {
                            console.log("Error pdf create Lettre d'opposition annexe2: ", err);
                            reject(err);
                        }
                        html3 = fs.readFileSync(process.cwd() + '/templates/annexe2Client.pdf', 'base64');
                        var pdfFile = PDF([{ model: 'facture', options: _.merge(req.body.data, { printable: true })}]);
                        pdfFile.toBuffer(function(err, resp) {
                            if (err) {
                                console.log("Error pdf create Injonction tribunal de commerce: ", err);
                                reject(err);
                            }
                            mail.send({
                                From: "contact@edison-services.fr",
                                ReplyTo: "noreply.edison@gmail.com",
                                To: "contentieux.edison@gmail.com;contentieux2.edison@gmail.com;contentieux3.edison@gmail.com",
                                Subject: "Lettre d'opposition n°" + req.body.data.id,
                                HtmlBody: "Lettre d'opposition n°" + req.body.data.id,
                                Attachments: [{
                                    Content: html,
                                    Name: "Lettre d'opposition n°" + req.body.data.id + ".pdf",
                                    ContentType: 'application/pdf'
                                },{
                                    Content: html2,
                                    Name: "Annexe 1.pdf",
                                    ContentType: 'application/pdf'
                                },{
                                    Content: html3,
                                    Name: "Annexe 2.pdf",
                                    ContentType: 'application/pdf'
                                },{
                                    Content: resp.toString('base64'),
                                    Name: "Facture' n°" + req.body.data.id + ".pdf",
                                    ContentType: 'application/pdf'
                                }]
                            }).then(function(resp, err) {
                                if (err) {
                                    console.log("Error send lettre d'opposition: ", err);
                                    reject(err);
                                }
                                resolve("YES");
                            });
                        })
                    })
                })
            })
        })
    }

    schema.statics.envRec = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.data.id }).exec(function(err, intSave) {
                if (err) {
                    console.log("Error find intSave");
                    reject(err);
                }
                intSave.compta.recouvrement = {};
                intSave.compta.recouvrement.priseEnCharge = 0;
                intSave.compta.recouvrement.litige.status = 4;
                intSave.save(function(err, resp) {
                    if (err) {
                        console.log("Error save saveRec");
                        reject(err);
                    }
                    else
                        resolve("YES");
                });
            })
        })
    }

    schema.statics.protAcc = function(req, res) {
        return new Promise(function(resolve, reject) {
            req.body.data.faitLe = moment().format("DD/MM/YY");
            req.body.data.interv = moment(req.body.data.date.intervention).format("DD/MM/YY");
            var html = fs.readFileSync(process.cwd() + '/pdf-mail/template/protocoleDaccordClient.html', 'utf8');
            html = _.template(html)(req.body.data);
            htmlpdf.create(html, { format: 'Letter' }).toFile(process.cwd() + '/templates/protocoleDaccordClient.pdf', function(err, resp) {
                if (err) {
                    console.log("Error pdf create protocole d'accord: ", err);
                    reject(err);
                }
                fs.readFile(process.cwd() + '/templates/protocoleDaccordClient.pdf', 'base64', function(err, buffer) {
                    mail.send({
                        From: "contact@edison-services.fr",
                        ReplyTo: "noreply.edison@gmail.com",
                        To: "contentieux.edison@gmail.com;contentieux2.edison@gmail.com;contentieux3.edison@gmail.com",
                        Subject: "Protocole d'accord n°" + req.body.data.id,
                        HtmlBody: "Protocole d'accord n°" + req.body.data.id,
                        Attachments: [{
                            Content: buffer,
                            Name: "Protocole d'accord' n°" + req.body.data.id + ".pdf",
                            ContentType: 'application/pdf'
                        }]
                    }).then(function(resp, err) {
                        if (err) {
                            console.log("Error send protocole d'accord: ", err);
                            reject(err);
                        }
                        resolve("YES");
                    });
                });
            })
        })
    }

    schema.statics.demeure = function(req, res) {
        return new Promise(function(resolve, reject) {
            req.body.data.faitLe = moment().format("DD/MM/YY");
            req.body.data.interv = moment(req.body.data.date.intervention).format("DD/MM/YY");
            var html = fs.readFileSync(process.cwd() + '/pdf-mail/template/miseEnDemeureClient.html', 'utf8');
            html = _.template(html)(req.body.data);
            var html2 = fs.readFileSync(process.cwd() + '/pdf-mail/template/annexe1Client.html', 'utf8');
            html2 = _.template(html2)(req.body.data);
            htmlpdf.create(html, { format: 'Letter' }).toFile(process.cwd() + '/templates/miseEnDemeureClient.pdf', function(err) {
                if (err) {
                    console.log("Error pdf create Mise en demeure: ", err);
                    reject(err);
                }
                html = fs.readFileSync(process.cwd() + '/templates/miseEnDemeureClient.pdf', 'base64');
                htmlpdf.create(html2, { format: 'Letter' }).toFile(process.cwd() + '/templates/annexe1Client.pdf', function(err) {
                    if (err) {
                        console.log("Error pdf create Mise en demeure annexe1: ", err);
                        reject(err);
                    }
                    html2 = fs.readFileSync(process.cwd() + '/templates/annexe1Client.pdf', 'base64');
                    var pdfFile = PDF([{ model: 'facture', options: _.merge(req.body.data, { printable: true })}]);
                    pdfFile.toBuffer(function(err, resp) {
                        if (err) {
                            console.log("Error pdf create Injonction tribunal de commerce: ", err);
                            reject(err);
                        }
                        mail.send({
                            From: "contact@edison-services.fr",
                            ReplyTo: "noreply.edison@gmail.com",
                            To: "contentieux.edison@gmail.com;contentieux2.edison@gmail.com;contentieux3.edison@gmail.com",
                            Subject: "Mise en demeure n°" + req.body.data.id,
                            HtmlBody: "Mise en demeure n°" + req.body.data.id,
                            Attachments: [{
                                Content: html,
                                Name: "Mise en demeure' n°" + req.body.data.id + ".pdf",
                                ContentType: 'application/pdf'
                            },{
                                Content: html2,
                                Name: "Annexe 1.pdf",
                                ContentType: 'application/pdf'
                            },{
                                Content: resp.toString('base64'),
                                Name: "Facture' n°" + req.body.data.id + ".pdf",
                                ContentType: 'application/pdf'
                            }]
                        }).then(function(resp, err) {
                            if (err) {
                                console.log("Error send Mise en demeure: ", err);
                                reject(err);
                            }
                            resolve("YES");
                        });
                    })
                })
            })
        })
    }

    schema.statics.injTribProx = function(req, res) {
        return new Promise(function(resolve, reject) {
            req.body.data.faitLe = moment().format("DD/MM/YY");
            req.body.data.interv = moment(req.body.data.date.intervention).format("DD/MM/YY");
            fs.readFile(process.cwd() + '/templates/injonctionTribunalProximite.pdf', 'base64', function(err, buffer) {
                mail.send({
                    From: "contact@edison-services.fr",
                    ReplyTo: "noreply.edison@gmail.com",
                    To: "contentieux.edison@gmail.com;contentieux2.edison@gmail.com;contentieux3.edison@gmail.com",
                    Subject: "Injonction de tribunal de proximite n°" + req.body.data.id,
                    HtmlBody: "Injonction de tribunal de proximite n°" + req.body.data.id,
                    Attachments: [{
                        Content: buffer,
                        Name: "Injonction de tribunal de proximite' n°" + req.body.data.id + ".pdf",
                        ContentType: 'application/pdf'
                    }]
                }).then(function(resp, err) {
                    if (err) {
                        console.log("Error send Injonction de tribunal de proximite: ", err);
                        reject(err);
                    }
                    resolve("YES");
                });
            });
        })
    }

    schema.statics.injTribCom = function(req, res) {
        return new Promise(function(resolve, reject) {
            req.body.data.datePlain = moment().format("DD/MM/YY");
            req.body.data.printable = true;
            var pdfFile = PDF([{ model: 'injonction', options: req.body.data },{ model: 'facture', options: _.merge(req.body.data, {printable: true })}]);
            pdfFile.toBuffer(function(err, resp) {
                if (err) {
                    console.log("Error pdf create Injonction tribunal de commerce: ", err);
                    reject(err);
                }
                mail.send({
                    From: "contact@edison-services.fr",
                    ReplyTo: "noreply.edison@gmail.com",
                    To: "contentieux.edison@gmail.com;contentieux2.edison@gmail.com",
                    Subject: "Injonction tribunal de commerce n°" + req.body.data.id,
                    HtmlBody: "Injonction tribunal de commerce n°" + req.body.data.id,
                    Attachments: [{
                        Content: resp.toString('base64'),
                        Name: "Injonction tribunal de commerce' n°" + req.body.data.id + ".pdf",
                        ContentType: 'application/pdf'
                    }]
                }).then(function(resp, err) {
                    if (err) {
                        console.log("Error send Injonction tribunal de commerce: ", err);
                        reject(err);
                    }
                    resolve("YES");
                });
            })
        })
    }

    /* Methods de save prise en charge */
    schema.statics.priseEnCharge = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.data.id }).exec(function(err, intPriseEnCharge) {
                if (err) {
                    console.log("Error find inter prise en charge");
                    reject(err);
                }
                intPriseEnCharge.compta.recouvrement = req.body.data.compta.recouvrement;
                intPriseEnCharge.compta.recouvrement.datePrise = moment();
                intPriseEnCharge.save(function(err, resp) {
                    if (err) {
                        console.log("Error save inter prise en charge");
                        reject(err);
                    } else if (resp.compta.recouvrement.priseEnCharge == 1)
                        resolve("YES");
                });
            })
        })
    }

    /* Methods reset data recouvrement */
    schema.statics.resetRec = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.data.id }).exec(function(err, intSave) {
                if (err) {
                    console.log("Error find intSave");
                    reject(err);
                }
                if (intSave.compta.recouvrement.litige.status == 2) {
                    var tmpUser = intSave.compta.recouvrement.litige.userCreate;
                    intSave.compta.recouvrement = {};
                    intSave.compta.recouvrement.litige.status = 1;
                    intSave.compta.recouvrement.litige.userCreate = tmpUser;
                } else {
                    intSave.compta.recouvrement = {};
                    intSave.compta.recouvrement.priseEnCharge = 0;
                }
                intSave.save(function(err, resp) {
                    if (err) {
                        console.log("Error save resetRec");
                        reject(err);
                    } else
                        resolve("YES");
                });
            })
        })
    }

    /* Methods save data recouvrement */
    schema.statics.saveRec = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.data.id }).exec(function(err, intSave) {
                if (err) {
                    console.log("Error find intSave");
                    reject(err);
                }
                intSave.compta.recouvrement = req.body.data.compta.recouvrement;
                intSave.save(function(err, resp) {
                    if (err) {
                        console.log("Error save saveRec");
                        reject(err);
                    } else
                        resolve("YES");
                });
            })
        })
    }

    /* Methods add litige recouvrement */
    schema.statics.createLitige = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.data }).exec(function(err, intSave) {
                if (err) {
                    console.log("Error find intSave");
                    reject(err);
                }
                if (req.body.type) {
                    if (req.body.type == 1) {
                        intSave.compta.recouvrement.causeRec = "chequePerdu";
                        intSave.compta.recouvrement.litige.status = 4;
                        intSave.compta.recouvrement.priseEnCharge = 0;

                    } else if (req.body.type == 2) {
                        intSave.compta.recouvrement.causeRec = "impaye";
                        intSave.compta.recouvrement.litige.status = 4;
                        intSave.compta.recouvrement.priseEnCharge = 0;

                    } else if (req.body.type == 3) {
                        intSave.compta.recouvrement.causeRec = "opposition";
                        intSave.compta.recouvrement.litige.status = 1;
                    }
                    intSave.compta.recouvrement.litige.userCreate = req.body.user;

                } else {
                    intSave.compta.recouvrement.litige.status = 1;
                    intSave.compta.recouvrement.litige.userCreate = req.body.user;
                }
                intSave.save(function(err, resp) {
                    if (err) {
                        console.log("Error save saveRec", err);
                        reject(err);
                    } else
                        resolve("YES");
                });
            })
        })
    }

    /* Methods timer two time each month reglement recu recouvrement => résolu */
    schema.statics.timerRecouv = function(req, res) {
        console.log("IN timerRecouv");
        return new Promise(function(resolve, reject) {
            var ind = 0;
            db.model('intervention').find({
                $or: [{
                    "compta.recouvrement.priseEnCharge": 1,
                    "compta.reglement.recu": true
                },{
                    "compta.recouvrement.litige.status": { $in: [1, 2] },
                    "compta.reglement.recu": true
                }]}).cursor()
                .on('error', function(err) {
                    console.log("Error cursor timerRecouv: ", err);
                    reject(err);
                })
                .on('data', function(elem) {
                    ind++;
                    console.log("Test: ", elem.id);
                    if (elem.compta.recouvrement.litige.status == 1 || elem.compta.recouvrement.litige.status == 2) {
                        elem.compta.recouvrement.litige.status = 3;
                        elem.compta.recouvrement.priseDeContact.statusRec = 'resolu';
                        if (elem.compta.recouvrement.priseDeContact.dateRelance)
                            delete elem.compta.recouvrement.priseDeContact.dateRelance;
                    } else {
                        elem.compta.recouvrement.priseEnCharge = 2;
                        elem.compta.recouvrement.priseDeContact.statusRec = 'resolu';
                        if (elem.compta.recouvrement.priseDeContact.dateRelance)
                            delete elem.compta.recouvrement.priseDeContact.dateRelance;
                    }
                    elem.save(function(err) {
                        if (err) {
                            console.log("Error save timerRecouv: ", err);
                            reject(err);
                        }
                    })
                })
                .on('end', function() {
                    console.log("----Ind: ", ind);
                    resolve('END');
                })
        })
    }

    /* Methods get data one inter */
    schema.statics.getDataOneInter = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').findOne({ id: req.body.id }).exec(function(err, dataInter) {
                if (err) {
                    console.log("Error find inter prise en charge");
                    reject(err);
                } else
                    resolve(dataInter);
            })
        })
    }
}