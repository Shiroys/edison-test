module.exports = function(schema) {


    schema.statics.annulation = {
        unique: true,
        findBefore: true,
        populateArtisan: true,
        method: 'POST',
        fn: function(inter, req, res) {

            var _ = require('lodash')
            return new Promise(function(resolve, reject) {
                if (inter.status === 'ENC' && inter.sst) {
                    var textTemplate = requireLocal('config/textTemplate');
                    mail.send({
                        noBCC: true,
                        From: "intervention@edison-services.fr",
                        ReplyTo: req.session.email,
                        To: inter.sst.email,
                        Subject: "[ANNULATION] - OS" + inter.id,
                        HtmlBody: _.template(textTemplate.sms.intervention.annulation)(inter)
                    });
                }
                inter.date.annulation = new Date;
                inter.date.envoi = undefined;
                inter.compta.paiement.ready = false;
                inter.login.annulation = req.session.login;
		if (inter.status === "SAV")
		    inter.statusSAV = "ANN"
		else
		    inter.status = "ANN";
                inter.causeAnnulation = req.body.causeAnnulation;
                if (req.body.reinit) {
                    inter.artisan = undefined;
                    inter.sst = undefined;
		    if (inter.status === "SAV")
			inter.statusSAV = "APR"
		    else
			inter.status = "APR";
                }
                edison.event('INTER_ANNULATION')
                    .login(req.session.login)
                    .id(inter.id)
                    .broadcast(inter.login.ajout)
                    .color('red')
                    .message(_.template("L'intervention {{id}} chez {{client.civilite}} {{client.nom}} ({{client.address.cp}}) à été annulé par {{login.annulation}}")(inter))
                    .send()
                    .save()
		var objFourniture = {};
		var listeFourniture = [];
		for (var n = 0; n < inter.fourniture.length; n++)
		{
		    if (inter.fourniture[n].fournisseur != "ARTISAN")
		    {
			if (listeFourniture.indexOf(inter.fourniture[n].fournisseur) == -1)
			    listeFourniture.push(inter.fourniture[n].fournisseur);
			if (!objFourniture[inter.fourniture[n].fournisseur])
			    objFourniture[inter.fourniture[n].fournisseur] = 0;
			objFourniture[inter.fourniture[n].fournisseur] += inter.fourniture[n].pu * inter.fourniture[n].quantite
		    }
		}
		if (listeFourniture.length > 0)
		{
		    var textMail = "Attention !<br>L'intervention n°<a href='46.101.137.217/intervention/" + inter.id + "'>" + inter.id + "</a> a été annulé par "
			+ req.session.login + " alors qu'elle contient des fournitures EDISON SERVICES.<br><br>"
		    for (var l = 0; l < listeFourniture.length; l++)
		    {
			textMail += "Montant H.T Fourniture: " + objFourniture[listeFourniture[l]] + "<br>";
			textMail += "Nom du Fournisseur: " + listeFourniture[l] + "<br>";
		    }
		    mail.send({
			From: "comptabilite@edison-services.fr",
			To: "fournitureannulation@edison-services.fr;achat.edison@gmail.com;comptabilite@edison-services.fr",
			Subject: "Intervention n°" + inter.id + " annulée avec coût de fourniture",
			HtmlBody: textMail,
		    })
		}
                inter.save().then(resolve, reject)
                if (req.body.sms) {
                    sms.send({
                        type: "ANNULATION",
                        dest: inter.sst.nomSociete,
                        to: inter.sst.telephone.tel1,
                        text: req.body.textSms,
                    })
                }

            })
        }
    }
}
