module.exports = function(schema) {
    var fs = require("fs");
    var _ = require("lodash");
    var moment = require('moment');
    moment.locale('fr');
    /* Fonction de lancement mail artisan pour les inter a vérifier*/
    schema.statics.sendRefus = function(req, res) {
        return new Promise(function(resolve, reject) {
            console.log('patatas', req.body)
            if (req.body.type == 1)
            {   
                console.log('here');
                db.model('intervention').findOne({ id: req.body.data.id }).exec(function(err, updatedInter) {
                    console.log('here 2 ')
                    db.model('artisan').findOne({id: updatedInter.sst}).exec(function(err, arti) {
                        console.log('here 3 ')
                        updatedInter.cache.s = 3;
                        updatedInter.cache.f.i_avr = 0;
                        updatedInter.status = "ENC";
                        updatedInter.save(function(err, resp) {
                            if (err)
                            {
                                console.log("Error save updatedInter: ", err);
                                reject(err);
                            }
                            else
                            {   
                                console.log('before datas')
                                req.body.newDate = moment(updatedInter.date.intervention).format("dddd D MMMM YYYY à HH:mm");
                                fs.readFile(process.cwd() + '/pdf-mail/template/mailRefus.html', 'utf8', function(err, tempMail){
                                    var allDatas = {
                                        dataInter: updatedInter,
                                        dataRaison: req.body
                                    };
                                    console.log('datas', allDatas)
                                    console.log('arti', arti.email)
                                    mail.send({
                                    From: "contact@edison-services.fr",
                                    ReplyTo: "noreply.edison@gmail.com",
                                    To: arti.email,
                                    Bcc: "noreply.edison@gmail.com",
                                    Subject: "Votre mise à jour de l'intervention n°" + req.body.data.id + " a été refusée.",
                                    HtmlBody:  _.template(tempMail)(allDatas), 
                                    }).then(function(success){
                                        resolve("OK");
                                    })
                                })
                            }
                        })
                    })
                })
            }
            else if (req.body.type == 2)
            {   
                console.log('there')
                db.model('user').findOne({ login: req.body.data.login.ajout }, { ligne: 1, login: 1, pseudo: 1 }).lean().exec(function(err, logUser) {
                    db.model('intervention').findOne({ id: req.body.data.id }).exec(function(err, updatedInter) {
                        db.model('artisan').findOne({id: updatedInter.sst}).exec(function(err, arti) {
                            updatedInter.cache.s = 3;
                            updatedInter.cache.f.i_avr = 0;
                            updatedInter.status = "ENC";
                            updatedInter.save(function(err, resp) {
                                if (err)
                                {
                                    console.log("Error save updatedInter: ", err);
                                    reject(err);
                                }
                                else
                                {   
                                    req.body.newDate = moment(updatedInter.date.intervention).format("dddd D MMMM YYYY à HH:mm");
                                    fs.readFile(process.cwd() + '/pdf-mail/template/mailRefusNoReglement.html', 'utf8', function(err, tempMail){
                                        var datas = {
                                            logUser: logUser,
                                            dataInter: req.body
                                        };
                                        mail.send({
                                        From: "contact@edison-services.fr",
                                        ReplyTo: "noreply.edison@gmail.com",
                                        To: arti.email,
                                        Bcc: "noreply.edison@gmail.com",
                                        Subject: "Votre mise à jour de l'intervention n°" + req.body.data.id + " a été refusée.",
                                        HtmlBody:  _.template(tempMail)(datas), 
                                        }).then(function(success){
                                            resolve("OK");
                                        })
                                    })
                                }
                            })
                        })
                    })
                })
            }
        })
    }
}