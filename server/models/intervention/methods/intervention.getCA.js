module.exports = function(schema) {
  var config = requireLocal('config/dataList')

    schema.statics.getCA = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var year = req.query.year;

	    var date = new Date("01/01/" + year);
	    var date2 = new Date("01/01/" + (parseInt(year) + 1));
	    db.model('compte').find({}).lean().then(function (comptes) {
		var nameArray = []
		for (var n = 0; n < comptes.length; n++)
		    nameArray.push(comptes[n].ref.toUpperCase());
		db.model('intervention').find({"facture.compte": {$in: nameArray}, "date.envoiFacture": {$gt: date, $lt: date2}}).lean().then(function (resp) {
		    var elems = {}
		    for (var n = 0; n < resp.length; n++)
			elems[resp[n].facture.compte] = new Array(12).fill(0);
		    for (var n = 0; n < resp.length; n++)
			elems[resp[n].facture.compte][resp[n].date.envoiFacture.getMonth()] += resp[n].prixFinal || resp[n].prixAnnonce
		    resolve(elems);
		})
	    })
	})
    };
}
