module.exports = function(schema) {

    var _ = require('lodash')
    var async = require('async');

    var getIn = function(field, telList) {
        var x = {}
        x[field] = {
            $in: telList
        }
        return x;
    }

    var reduce = function(rtn, fltr) {
        var total = 0;
        var x = _.filter(rtn, fltr);
        _.each(x, function(e) {
            total += e.prix;
        })
        return total
    }

    schema.statics.telMatches = function(req, res) {

        if (!isWorker) {
            return edison.worker.createJob({
                name: 'db',
                model: 'intervention',
                method: 'telMatches',
                req: _.pick(req, 'body', 'session')
            }).then(function(resp) {
                io.sockets.emit('telephoneMatch', resp);
                res.send('ok')
            })
        }
        return new Promise(function(resolve, reject) {

            var q = req.body.q.split('\n');
            q = _.map(q, function(e) {
                var sp = e.split(/\t| /);
                if (sp.length === 2) {
                    return {
                        origin: sp[0].replace('33', '0'),
                        client: sp[1].replace('33', '0')
                    }
                }
            })
            q = _.filter(q, function(e) {
                return e && e.client && e.client.length == 10;
            })
            q = _.groupBy(q, 'origin');
            var artn = [];
            var i = 0;
            async.each(q, function(e, cb) {
                var query = {
                    $or: []
                };
                _.each(_.map(e, 'client'), function(tel) {
                    if (!tel) {
                        return 0
                    }
                    query.$or.push(
                        // $or: [
                             getIn('client.telephone.origine', tel),
                             getIn('client.telephone.tel1', tel),
                             getIn('client.telephone.tel2', tel),
                             getIn('client.telephone.tel3', tel),
                             getIn('client.telephone.origine', tel),
                             getIn('client.telephone.appel', tel)
                         //]
                    )
                });
                db.model('intervention').find(query, {
                    id: true,
                    status: true,
                    prixAnnonce: true,
                    prixFinal: true,
                    'compta.reglement.recu': true
                }, function(err, resp) {
                    var rtn = _.map(resp, function(e) {
                            return {
                                id: e.id,
                                prix: (e.prixFinal ||  e.prixAnnonce ||  0),
                                status: e.status,
                                paiementRecu: e.compta.reglement.recu
                            }
                        })
                        ++i
                    var st = {
                        ligne: e[0].origin,
                        ANN: reduce(rtn, {
                            'status': 'ANN'
                        }),
                        ENC: reduce(rtn, {
                            'status': 'ENC'
                        }),
                        VRF: reduce(rtn, {
                            'status': 'VRF'
                        }),
                        PAY: reduce(rtn, {
                            paiementRecu: true
                        }),
                        list: _.map(rtn, 'id').join(','),
                        calls: e.length
                    }
                    artn.push(st)
                    cb(null);
                })

            }, function(err, resp) {
                return resolve(artn)
            })
        })
        return 0
        var query = {
            $or: []
        };
        _.each(q, function(tel) {
            if (!tel) {
                return 0
            }
            query.$or.push({
                $or: [
                    getIn('client.telephone.tel1', tel),
                    getIn('client.telephone.tel2', tel),
                    getIn('client.telephone.tel3', tel),
                    getIn('client.telephone.origine', tel),
                    getIn('client.telephone.appel', tel),
                ]
            })
        });
        db.model('intervention').find(query, {
            id: true
        }).exec(function(err, resp) {
            if (err) {
                return res.status.send("nope")
            }
            var rtn = _.map(resp, 'id');
            res.json(rtn)
        })
    }
}
