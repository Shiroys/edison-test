module.exports = function(schema) {
    var moment = require('moment');
    var async = require('async');

    schema.statics.ptStats = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var ret = {}
	    for (var n = 0; n < req.query.array.length; n++)
			ret[req.query.array[n]] = {news: 0, pot: 0, ann: 0, ver: 0}
	    var start = new Date(req.query.start);
	    var end = new Date(req.query.end);
	    async.waterfall([
		function (cb) {
		    db.model('intervention').find({"date.envoi": {$gt: start, $lt: end}, "login.envoi": {$in: req.query.array}}, {"login.envoi": 1, 'artisan': 1}).lean().then(function (resp) {
			for (var n = 0; n < resp.length; n++)
			{
			    if (resp[n].artisan && resp[n].artisan.subStatus === 'NEW')
					ret[resp[n].login.envoi].news += 1;
			    else if (resp[n].artisan && resp[n].artisan.subStatus === 'POT')
					ret[resp[n].login.envoi].pot += 1;
			}
			cb(null);
		    });
		}, function (cb) {
		    db.model('intervention').find({"date.annulation": {$gt: start, $lt: end}, "login.envoi": {$in: req.query.array}}, {"login.envoi": 1, 'status': 1}).lean().then(function (resp) {
			for (var n = 0; n < resp.length; n++)
			{
			    if (resp[n].status === 'ANN')
					ret[resp[n].login.envoi].ann += 1;
			}
			cb(null);
		    })
		}, function (cb) {
		    db.model('intervention').find({"date.verification": {$gt: start, $lt: end}, "login.envoi": {$in: req.query.array}}, {"login.envoi": 1, 'status': 1}).lean().then(function (resp) {
			for (var n = 0; n < resp.length; n++)
			{
			    if (resp[n].status === 'VRF')
					ret[resp[n].login.envoi].ver += 1;
			}
			cb(null);
		    })
		}], function (error) {
		    if (error)
				reject(error);
		    else
				resolve(ret);
		})
	})
    }

}
