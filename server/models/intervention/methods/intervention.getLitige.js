module.exports = function(schema) {
    schema.statics.getLitiges = function(req, res) {
		return new Promise(function(resolve, reject){
			var loginArr = [];
			var mainArr = {};
			var now = new Date()
			db.model('intervention').find({'recouvrement.level':2}, {'date.intervention':1, 'login.ajout':1}).then(function(resp){
			    var array = {}
			    var array2 = []
			    for (var i = 0; i < resp.length; i++)
			    {
				if (resp[i].date.intervention.getFullYear() === now.getFullYear())
				{
				    var inter = array2.indexOf(resp[i].login.ajout)
				    if (inter === -1)
				    {
					array[resp[i].login.ajout] = {janvier: 0, fevrier: 0,mars: 0, avril: 0,mai: 0,juin: 0, juiillet: 0, aout: 0,septembre: 0,octobre: 0,novembre: 0, decembre: 0}
					array2.push(resp[i].login.ajout)
				    }
				    if (resp[i].date.intervention.getMonth() === 0)
					array[resp[i].login.ajout].janvier++
				    if (resp[i].date.intervention.getMonth() === 1)
					array[resp[i].login.ajout].fevrier++
				    if (resp[i].date.intervention.getMonth() === 2)
					array[resp[i].login.ajout].mars++
				    if (resp[i].date.intervention.getMonth() === 3)
					array[resp[i].login.ajout].avril++
				    if (resp[i].date.intervention.getMonth() === 4)
					array[resp[i].login.ajout].mai++
				    if (resp[i].date.intervention.getMonth() === 5)
					array[resp[i].login.ajout].juin++
				    if (resp[i].date.intervention.getMonth() === 6)
					array[resp[i].login.ajout].juillet++
				    if (resp[i].date.intervention.getMonth() === 7)
					array[resp[i].login.ajout].aout++
				    if (resp[i].date.intervention.getMonth() === 8)
					array[resp[i].login.ajout].septembre++
				    if (resp[i].date.intervention.getMonth() === 9)
					array[resp[i].login.ajout].octobre++
				    if (resp[i].date.intervention.getMonth() === 10)
					array[resp[i].login.ajout].novembre++
				    if (resp[i].date.intervention.getMonth() === 11)
					array[resp[i].login.ajout].decembre++
				}
			    }
			    resolve(array);
			});
		});
	}
}
