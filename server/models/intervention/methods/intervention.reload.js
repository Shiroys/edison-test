module.exports = function(schema) {

	/* Fonction Reload du cache + update des interventions En cours en Vérifiées */
	schema.statics.reloadCache = function(req, res) {
		db.model('intervention').fullReload().then(function() {});
		db.model('devis').fullReload().then(function() {});
	}

}
