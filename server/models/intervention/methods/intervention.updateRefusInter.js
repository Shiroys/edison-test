module.exports = function(schema) {

    var moment = require('moment');
    moment.locale('fr');
    /* Fonction de refus d'update pour les interventions a vérifier*/
    schema.statics.updateRefusInter = function(req, res) {
        return new Promise(function(resolve, reject) {
            if (req.body.type == 1)
            {   
                db1.mongoose1.model('made').findOne({ id: req.body.data.id.toString() }).lean().then(function(resp) {
                    db.model('intervention').findOne({ id: req.body.data.id }).then(function(updatedInter) {
                        console.log('in findOne')
                        updatedInter.date.refusMajInternet = moment();
                        updatedInter.login.refusMajInternet = req.session.login;
                        updatedInter.cache.s = 3;
                        updatedInter.status = "AVR";
                        updatedInter.save(function(err, resp) {
                            if (err)
                            {
                                console.log("Error save updatedInter: ", err);
                                reject(err);
                            }
                            else
                            {
                                resolve("OK");
                                console.log("saved well")
                            }
                        })
                    })
                })
            }
            else if (req.body.type == 2)
            {
                db1.mongoose1.model('pushedBack').findOne({ id: req.body.data.id.toString() }).lean().then(function(resp) {
                    db.model('intervention').findOne({ id: req.body.data.id }).then(function(updatedInter) {
                        updatedInter.date.refusMajInternet = moment();
                        updatedInter.login.refusMajInternet = req.session.login;
                        updatedInter.cache.s = 3;
                        updatedInter.status = "AVR";
                        updatedInter.save(function(err, resp) {
                            if (err)
                            {
                                console.log("Error save updatedInter: ", err);
                                reject(err);
                            }
                            else
                            {
                                resolve("OK");
                            }
                        })
                    })
                })
            }
            else if (req.body.type == 3)
            {
                db1.mongoose1.model('canceled').findOne({ id: req.body.data.id.toString() }).lean().then(function(resp) {
                    db.model('intervention').findOne({ id: req.body.data.id }).then(function(updatedInter) {
                        updatedInter.date.refusMajInternet = moment();
                        updatedInter.login.refusMajInternet = req.session.login;
                        updatedInter.cache.s = 3;
                        updatedInter.status = "AVR";
                        updatedInter.save(function(err, resp) {
                            if (err)
                            {
                                console.log("Error save updatedInter: ", err);
                                reject(err);
                            }
                            else
                            {
                                resolve("OK");
                            }
                        })
                    })
                })
            }
        })
    }
}