module.exports = function(schema) {
    var _ = require('lodash')
    var async = require('async');
    var moment = require('moment');
    schema.statics.GetStatUser = function(req, res) {
	var dt = req.body.date;
	var dtprec = req.body.dateprec;
	return new Promise(function(resolve, reject) {
	    var q = {};
	    var options = {};
	    req.query.year = parseInt(req.query.year);
	    //date
	    if (req.query.group === 'day') {
		options.divider = " en cours"
		var date = new Date(req.query.year, req.query.month);
		options.dateRange = {
		    $gte: new Date(date.getFullYear(), date.getMonth() - 1, 1, -1),
		    $lt: new Date(date.getFullYear(), date.getMonth(), 1)
		}
		options.groupId = {
		    $dayOfMonth: "$date.ajout"
		}
		options.maxRange = 32

	    } else if (req.query.group == 'month') {
		options.divider = ' par mois'
		options.dateRange = {
		    $gte: new Date(req.query.year, 0, 1),
		    $lt: new Date(req.query.year + 1, 0, 1)
		}
		options.groupId = {
		    $month: "$date.ajout"
		}
		options.maxRange = 13

	    } else if (req.query.group == 'week') {
		options.divider = 'semaine'
		options.dateRange = {
		    $gte: new Date(req.query.year, 0, 1),
		    $lt: new Date(req.query.year + 1, 0, 1)
		}
		options.groupId = {
		    $week: "$date.ajout"
		}
		options.maxRange = 53
	    }




	    options.match = {
		'status': {
		    $in: ['AVR','ENC', 'VRF','ANN']
		}
	    }

	    var divider = {
		chiffre: {
		    post: function(resp) {
			var telepro = edison.users.service('INTERVENTION');
			var obs = edison.users.serviceObs('INTERVENTION');
			var i = 0;
			var series = _.map(telepro, function(e) {
			    return {
				name: e,
				obs: obs.indexOf(e) >= 0,
				data: db.utils.pluck(resp, e, options.maxRange),
				linkedTo: ':previous',
				color: "#FF0000",
			    }
			})
			return {
			    title: "% chiffre" + options.divider,
			    series: series,
			    categories: _.map(_.range(1, options.maxRange), String)
			}
		    },
		    project: function() {
			var telepro = edison.users.service('INTERVENTION');
			var rtn = {
			    _id: options.groupId,
			}
			_.each(telepro, function(e) {
			    rtn[e] = db.utils.sumAnn('$login.ajout', e, '$prixAnnonce','ANN')
			});
			return rtn;
		    }
		},
		chiffreVRF: {
		    post: function(resp) {
			
			var telepro = edison.users.service('INTERVENTION');
			var obs = edison.users.serviceObs('INTERVENTION');
			var series = _.map(telepro, function(e) {
			    return {
				name: e,
				obs: obs.indexOf(e) >= 0,
				data: db.utils.pluck(resp, e, options.maxRange),
			    }
			})
			return {
			    title: "% annulation" + options.divider,
			    series: series,
			    categories: _.map(_.range(1, options.maxRange), String)
			}
		    },
		    project: function() {
			var telepro = edison.users.service('INTERVENTION');
			var rtn = {
			    _id: options.groupId,
			}
			_.each(telepro, function(e) {
			    rtn[e] = db.utils.sumBoth(e)
			});
			return rtn;
		    }
		},
		annulation: {
		    post: function(resp) {
			var telepro = edison.users.service('INTERVENTION');
			var obs = edison.users.serviceObs('INTERVENTION');
			var series = _.map(telepro, function(e) {
			    return {
				name: e,
				obs: obs.indexOf(e) >= 0,
				data: db.utils.pluck(resp, e, options.maxRange),
				color: "#FF0000",
				linkedTo: ':previous'
			    }
			})
			return {
			    title: "% annulation" + options.divider,
			    series: series,
			    categories: _.map(_.range(1, options.maxRange), String)
			}
		    },
		    project: function() {
			var telepro = edison.users.service('INTERVENTION');
			var rtn = {
			    _id: options.groupId,
			}
			_.each(telepro, function(e) {
			    rtn[e] = db.utils.sumAnn('$login.ajout', e, 1, 'ANN');
			})
			    return rtn;
		    }
		},
		nbrVRF: {
		    post: function(resp) {
			var telepro = edison.users.service('INTERVENTION');
			var obs = edison.users.serviceObs('INTERVENTION');
			var series = _.map(telepro, function(e) {
			    return {
				name: e,
				obs: obs.indexOf(e) >= 0,
				data: db.utils.pluck(resp, e, options.maxRange)
			    }
			})
			return {
			    title: "% annulation" + options.divider,
			    series: series,
			    categories: _.map(_.range(1, options.maxRange), String)
			}
		    },
		    project: function() {
			var telepro = edison.users.service('INTERVENTION');
			var rtn = {
			    _id: options.groupId,
			}
			_.each(telepro, function(e) {
			    rtn[e] = db.utils.sumBothNbr(e)
			});
			return rtn;
		    }
		},
	    }

	    options.match['date.ajout'] = options.dateRange
	    db.model('intervention').aggregate()
	        .match(options.match)
	        .group(divider[req.query.divider].project())
		.exec(function(err, resp) {
		    var rtn = divider[req.query.divider].post(resp);
		    resolve(rtn);
		})
	})
    }
}    
