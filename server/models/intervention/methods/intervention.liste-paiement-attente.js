module.exports = function(schema) {
    var _ = require('lodash')


    schema.statics.getFlushList = function(query) {
        return new Promise(function(resolve, reject) {
            db.model('intervention').find(query)
                .populate('sst')
                .select('id client description compta.paiement compta.reglement date.intervention sst artisan fourniture categorie')
                .exec(function(err, docs) {
                    var rtn = _(docs).groupBy('sst.id').values().map(function(e) {
                        return {
                            address: e[0].sst.address,
                            nomSociete: e[0].sst.nomSociete,
                            id: e[0].sst.id,
						    verif: e[0].sst.verifRIB,
						    IBAN: e[0].sst.IBAN,
							fj: e[0].sst.formeJuridique,
                            sub: e[0].sst.subStatus,
                            status: e[0].sst.status,
                            representant: e[0].sst.representant,
                            list: _.map(e, function(z) {
                                z.sst = undefined
                				var fourniture = _.reduce(z.fourniture, function(res, x) {
                				    res[x.fournisseur === 'ARTISAN' ? 'artisan' : 'edison'] += (x.pu * x.quantite);
                				    return res;
                				}, {
                				    artisan: 0,
                				    edison: 0
                				})
                				z.dumpValue.artisan = fourniture.artisan;
                				z.dumpValue.edison = fourniture.edison;
                                return z;
                            })
                        }
                    }).value()
                    resolve(rtn)
                })
        });
    }

    var getHisto = function(inter, date) {
		var ind = 0;
		while (inter.compta.paiement.historique[ind] != null)
		{
			if (toString(inter.compta.paiement.historique[ind].dateFlush) == toString(date))
				index = ind;	
			ind++;
		}
        console.log("IND: ", ind, "ID: ", inter.id);
        inter = JSON.parse(JSON.stringify(inter));
        inter.compta.paiement.base = inter.compta.paiement.historique[index].base
        inter.compta.paiement.montant = inter.compta.paiement.historique[index].montant
        inter.compta.paiement.pourcentage = inter.compta.paiement.historique[index].pourcentage
        inter.compta.paiement.tva = inter.compta.paiement.historique[index].tva
        inter.compta.paiement.numeroCheque = inter.compta.paiement.historique[index].numeroCheque;
        inter.compta.paiement.fourniture = inter.compta.paiement.historique[index].fourniture
        inter.compta.paiement.historique = inter.compta.paiement.historique.slice(0, index);
        inter.sst = undefined;
        return inter;
    }

    var gtf = function(ts) {
        return new Promise(function(resolve, reject) {
            var date = new Date(parseInt(ts));
            db.model('intervention')
                .find({
                    'compta.paiement.historique': {
                        $elemMatch: {
                            dateFlush: date
                        }
                    },
                })
                .populate('sst')
                .select('id client description compta.paiement compta.reglement date.intervention sst artisan fourniture categorie')
                .exec(function(err, docs) {
                    var rtn = _(docs).filter('sst').groupBy('sst.id').map(function(e) {
                            return {
                                address: e[0].sst.address,
                                nomSociete: e[0].sst.nomSociete,
                                id: e[0].sst.id,
                                status: e[0].sst.status,
								fj: e[0].sst.formeJuridique,
                                sub: e[0].sst.subStatus,
								verif: e[0].sst.verifRIB,
								IBAN: e[0].sst.IBAN,
                                representant: e[0].sst.representant,
                                list: _.map(e, _.partial(getHisto, _, date))
                            }
                    })
                    resolve(rtn)
                })
        })
    }

    schema.statics.lpa = function(req, res) {
        if (req.query.d) {
            return gtf(req.query.d)
        }
        return this.getFlushList({
            'compta.paiement.ready': true,
            'compta.paiement.dette': false
        })

    }

}
