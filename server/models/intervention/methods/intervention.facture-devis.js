module.exports = function(schema) {
    var ejs = require("ejs");
    var fs = require("fs")
    var moment = require('moment');
    var PDF = requireLocal('pdf-mail')
    moment.locale('fr');
    var textTemplate = requireLocal('config/textTemplate');
    var _ = require('lodash')
    var Facture = requireLocal('config/validate-facture');
	var request = require('request');

    var getFacturePdfObj = function(doc, date, acquitte, reverse) {
        doc.datePlain = moment(date).format('LL');
        doc.acquitte = acquitte;
        var text = textTemplate.lettre.intervention.envoiFacture();
        var lettre = {
                address: doc.facture.address,
				mobile_url: doc.mobile_url,
                dest: doc.facture,
                text: _.template(text)(doc),
                id: _.padLeft(doc.id, 6, '0'),
                date: doc.date,
                factureQrCode: true,
                title: "OBJET : Facture n°" + doc.id + " en attente de reglement"
            }
        doc.type = 'facture'
        if (!acquitte) {
            var l = [{
                model: 'letter',
                options: lettre
            }]
        } else {
            var l = [];
        }
        l.push({
            model: 'conditions',
            options: doc
        }, {
            model: 'facture',
            options: doc
        })
        if (reverse) {
            var last = l.shift()
            l.push(last);
        }
        return PDF(l)
    }

    schema.statics.bigJob = {
        unique: true,
        findBefore: true,
        method: 'GET',
        fn: function(inter, req, res) {
            return new Promise(function(resolve, reject) {

                if (!isWorker) {
                    return edison.worker.createJob({
                        priority: 'high',
                        name: 'db_id',
                        model: 'intervention',
                        method: 'bigJob',
                        data: inter,
                        req: _.pick(req, 'body', 'session')
                    }).then(resolve, reject)
                }

                inter.facture = inter.client;
                getFacturePdfObj(inter, inter.date.intervention).toBuffer(function(err, buff) {
                    if (!err && buff) {
                        resolve('ok')
                    } else {
                        reject('nope')
                    }
                })
            })
        }
    }
    schema.statics.facturePreview = function(req, res) {
        var _this = this;
        try {
            var doc = JSON.parse(req.body.data);
            if (!doc.facture || !doc.facture.nom) {
                doc.facture = doc.client;
            }
            var pdf = getFacturePdfObj(doc, doc.date.intervention);
            return res.send(pdf.html())
            pdf.toBuffer(function(err, buff) {
                return res.pdf(buff)
            })
        } catch (e) {
            __catch(e)
            return res.status(400).send('bad data')
        }

    }

    schema.statics.factureAcquittePreview = function(req, res) {
        var _this = this;
        try {
            var doc = JSON.parse(req.body.data);
        } catch (e) {
            return res.status(400).send('bad data')
        }

        res.send(getFacturePdfObj(doc, req.body.date, true).html())
    }

    schema.statics.devisPreview = function(req, res) {
        var _this = this;
        try {
            var doc = JSON.parse(req.body.data);
        } catch (e) {
            return res.status(400).send('bad data')
        }
        doc.id = doc.id ||  "00000"
        doc.type = 'devis'
        doc.facture = doc.client;
        doc.facture.tel = doc.client.telephone.tel1;
        doc.acquitte = false;
        doc.user = req.session;
        doc.type = "devis"
        var result = PDF([{
            model: 'facture',
            options: doc
        }, {
            model: 'conditions',
            options: {}
        }]).html()
        return res.send(result)
    }


    schema.statics.sendFactureAcquitte = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(dd, req, res) {
            return new Promise(function(resolve, reject) {
                var inter = req.body.data
                if (inter.reglementSurPlace) {
                    inter.facture = inter.client;
                }
                var f = inter.facture;
                if (!f.email ||  !f.nom || !f.address.r || !f.address.v ||  !f.address.cp || !f.address.n) {
                    return reject('Les Coordonnées de facturations sont incompletes')
                }
                f.prenom = f.prenom ||  "";
                if (!inter.produits || !inter.produits.length) {
                    return reject('Veuillez renseigner au moins 1 produits')
                }
                if (!isWorker) {
                    edison.event('INTER_SENDFACT_ACQ').login(req.session.login).id(inter.id).save();
                    return edison.worker.createJob({
                        priority: 'high',
                        name: 'db_id',
                        model: 'intervention',
                        method: 'sendFactureAcquitte',
                        data: inter,
                        req: _.pick(req, 'body', 'session')
                    }).then(resolve, reject)
                }
                if (inter.reglementSurPlace) {
                    inter.facture = inter.client;
                }
                inter.acompte = (inter.prixFinal * (inter.tva / 100 + 1));
				var pdf = getFacturePdfObj(inter, req.body.date, true, req.body.date);
					pdf.toBuffer(function(err, buffer) {
                        for(var ind = 0; ind < req.body.dataFiles.length; ind++)
                        {
                            if (req.body.dataFiles[ind].ContentType != 'application/pdf')
                            {
                                var buffTmp = new Buffer(req.body.dataFiles[ind].Content, 'utf8');
                                req.body.dataFiles[ind].Content = buffTmp.toString('base64');
                            }
                        }
                        req.body.dataFiles.push({ ContentType: 'application/pdf', Content: buffer.toString('base64'), Name: "Facture n°" + inter.id + ".pdf",});
                        var communication = {
                            mailDest: envProd ? inter.facture.email : (req.session.email ||  'contact@edison-services.fr'),
                            mailReply: (req.session.email ||  'comptabilite@edison-services.fr')
                        }
						var mailTo = communication.mailDest;
						if (inter.facture.email2 && envProd)
							mailTo += ";" + inter.facture.email2;
						mail.send({
                        	From: "contact@edison-services.fr",
                        	ReplyTo: communication.mailReply,
                        	To: communication.mailDest,
                        	Subject: "Facture n°" + inter.id + " acquitté",
                        	HtmlBody: req.body.text.replaceAll('\n', '<br>'),
                        	Attachments: req.body.dataFiles
                    	}).then(function(resp) {
                            db.model('intervention').findOne({
                                id: inter.id
                            }).then(function(doc) {
                                doc.date.envoiFacture = new Date();
                                doc.login.envoiFacture = req.session.login;
                                doc.save().then(resolve, reject)
                                getFacturePdfObj(doc, doc.date.intervention, false, true).toBuffer(function(err, buff) {
        				    		document.stack(buff, 'FACTURE ' + doc.id, req.session.login)
        							.then(function(resp) {})
                                })
                            })
                        }, reject).catch(__catch)
                	})
            })
        }

    }

    schema.statics.sendFacture = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(inter, req, res) {
            return new Promise(function(resolve, reject) {

                var f = inter.facture;
                if (!f.email || !f.nom || !f.address.r || !f.address.v || !f.address.cp || !f.address.n || !f.tel) {
                    return reject('Les Coordonnées de facturations sont incompletes')
                }
                f.prenom = f.prenom || "";
                if (!inter.produits || !inter.produits.length) {
                    return reject('Veuillez renseigner au moins 1 produits')
                }
                if (!isWorker) {
                    return edison.worker.createJob({
                        name: 'db_id',
                        model: 'intervention',
                        method: 'sendFacture',
                        priority: 'high',
                        data: inter,
                        req: _.pick(req, 'body', 'session')
                    }).then(function() {
                        edison.event('INTER_SENDFACT').login(req.session.login).id(inter.id).save();
                        inter.save().then(resolve, reject)
                    }, reject)
                }
				if (inter.client.civilite !== 'Soc.' && f.payeur !== 'SOC' && f.payeur !== 'GRN' &&
					(f.address.r === inter.client.address.r && f.address.v === inter.client.address.v &&
					f.address.cp === inter.client.address.cp && f.address.n === inter.client.address.n))
				{
			    var tel = "Téléphone n°1: " + inter.client.telephone.tel1 + "<br>"
			    if (inter.client.telephone.tel2)
				    tel += "Téléphone n°2: " + inter.client.telephone.tel2 + "<br>"
			    var textInter = "Les coordonnées de factures envoyé par " + req.session.login + " sont identiques pour l'intervention n°<a href='46.101.137.217/intervention/" + inter.id + "'>" + inter.id + "</a>:<br>" +
					"Nom: " + inter.client.nom + "<br>" +
		 		   	"Prénom: " + inter.client.prenom + "<br>" + tel +
					"Adresse: " + inter.client.address.n + " " + inter.client.address.r +
					" " + inter.client.address.cp + " " + inter.client.address.v + "<br>" +
					"Description: " + inter.description + "<br>" +
					"Date d'intervention: " + moment(inter.date.intervention).format("DD[/]MM[ à ]HH[h]mm") + "<br>" +
					"Prix Total HT : " + inter.prixFinal + "€"
			   	mail.send({
					From: "contact@edison-services.fr",
					To: "doublonfacture@edison-services.fr",
					Subject: "Coordonnées de facturation pour " + inter.id + " par " + req.session.login,
                   	HtmlBody: textInter,
		   		});
				}
				if (inter.reglementSurPlace)
					inter.facture = inter.client;
				if (inter.acompte)
					var amount = (_.round(inter.prixFinal * (inter.tva / 100 + 1), 2).toFixed(2) - inter.acompte);
				else
					var amount = _.round(inter.prixFinal * (inter.tva / 100 + 1), 2).toFixed(2);
				//Requete pour obtenir l'url de paiement lydia.
				request.post('https://lydia-app.com/api/request/do.json', {form:{
					vendor_token: "568e473f2d825946384861",
					recipient: inter.facture.email,
					message: inter.id,
					amount: amount,
					expire_time: '604800',
					currency: 'EUR',
					type: 'email',
					notify: 'no'
				}}, function(err, http_resp, body) {
						if (err)
							console.log('Error callback Lydia: ' + err);
						else
						{
							body = JSON.parse(body);
							//Remplacement de la variable par l'adresse de paiement
							var text = req.body.text.replaceAll("adresseLydia", body.mobile_url);
							//Création du pdf
							var pdf = getFacturePdfObj(inter, req.body.date, req.body.acquitte, req.body.date);
							pdf.toBuffer(function(err, buffer) {
                                for(var ind = 0; ind < req.body.dataFiles.length; ind++)
                                {
                                    if (req.body.dataFiles[ind].ContentType != 'application/pdf')
                                    {
                                        var buffTmp = new Buffer(req.body.dataFiles[ind].Content, 'utf8');
                                        req.body.dataFiles[ind].Content = buffTmp.toString('base64');
                                    }
                                }
                                req.body.dataFiles.push({ ContentType: 'application/pdf', Content: buffer.toString('base64'), Name: "Facture n°" + inter.id + ".pdf",});
                                var communication = {
                                   mailDest: envProd ? inter.facture.email : (req.session.email ||  'contact@edison-services.fr'),
                                   mailReply: (req.session.email ||  'comptabilite@edison-services.fr')
                                }
								var mailTo2 = communication.mailDest;
								if (inter.facture.email2 && envProd)
									mailTo2 += ";" + inter.facture.email2;
							//Configuration et envoie du mail
								mail.send({
									From: "contact@edison-services.fr",
									ReplyTo: communication.mailReply,
									To: mailTo2,
									Subject: "Facture n°" + inter.id + " en attente de reglement",
									HtmlBody: text.replaceAll("\n", '<br>'),
									Attachments: req.body.dataFiles
								}).then(function(resp) {
									if (req.body.acquitte) {
										return resolve("OK");
									}
									db.model('intervention').findOne({
										id: inter.id
									}).then(function(doc) {
										doc.date.envoiFacture = new Date();
										doc.login.envoiFacture = req.session.login;
										doc.save().then(resolve, reject)
										getFacturePdfObj(doc, doc.date.intervention, false, true).toBuffer(function(err, buff) {
                                            document.stack(buff, 'FACTURE ' + doc.id, req.session.login)
                                                .then(function(resp) {})
										})
									})
								}, reject).catch(__catch)
							})
						}
					});//End callback
				 })
			}
		 }
}
