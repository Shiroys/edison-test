module.exports = function(schema) {
    schema.statics.ComptaStats = function(req, res) {
	var mongoose = require('mongoose');
	return new Promise(function(resolve, reject)  {
	    var moment = require("moment")
	    var current = new Date();
	    var fin = new Date();
	    var debut = new Date(moment().startOf('isoweek').toDate());
	    var listrecu = req.body
	    var keyarray = Object.keys(req.body)
		db.model('intervention').find({"recouvrement.date":{$gte: debut,$lt: fin}}).then(function (resp) {
		    var array = {}
		    var array2 = []
		    for (var j = 0;keyarray.length > j;j++)
		    {
			array[keyarray[j]] = {factrecu: 0, litige: 0,envoye: 0, reglrecu:listrecu[keyarray[j]]}
			array2.push(keyarray[j])
		    }
		    for (var i=0; resp.length > i; i++)
		    {
			var inter = array2.indexOf(resp[i].recouvrement.login)
			if (inter === -1)
			{
			    array[resp[i].recouvrement.login] = {factrecu: 0, litige: 0,envoye: 0, reglrecu:0}
			    array2.push(resp[i].recouvrement.login)
			}			
			if (resp[i].recouvrement.level === 1)
			{
			    array[resp[i].recouvrement.login].factrecu++ 
			}
			if (resp[i].recouvrement.level === 2)
			{
			    array[resp[i].recouvrement.login].litige++
			}
			if (resp[i].recouvrement.level === 3)
			{
			    array[resp[i].recouvrement.login].envoye++
			}
		    
		    }
		    resolve(array)
		})
	    
	});
    }
}
