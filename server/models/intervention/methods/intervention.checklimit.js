module.exports = function(schema) {
    schema.statics.checklimit = function (req, res) {
	return new Promise(function(resolve, reject) {
	    var id = req.body.id;
	    db.model('intervention').find({'artisan.id': id, 'reglementSurPlace': true, 'compta.reglement.recu': false}).then(function (resp) {
		var result = 0;
		for(var i = 0; resp.length > i; i++)
		{
		    result += resp[i].prixFinal;
		}
		res.status(200).send(result.toString());
	    }, reject);
	})
    }
}
