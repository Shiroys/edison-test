module.exports = function(schema) {

	/* Fonction de transformation de status (Outils Admin) */
	schema.statics.transSav = function(req, res) {
		return new Promise (function(resolve, reject) {
			var fileId = req.body.fileId;
			var numStatus = -1;
			if (req.body.nStatus == "ENC")
				numStatus = 0;
			else if (req.body.nStatus == "VRF")
				numStatus = 1;
			else if (req.body.nStatus == "APR")
				numStatus = 2;
			else if (req.body.nStatus == "AVR")
				numStatus = 3;
			else if (req.body.nStatus == "ANN")
				numStatus = 4;
			else if (req.body.nStatus == "SAV")
				numStatus = 5;
			else if (req.body.nStatus == "DVG")
				numStatus = 6;
			else if (req.body.nStatus == "PRT")
				numStatus = 7;
			else if (req.body.nStatus == "RGL")
				numStatus = 8;
			fileId.id.forEach(function(element) {
				if (req.body.nStatus == "SAV")
				{
					db.model('intervention').find({_id:element}).then(function(resp) {
						var saveStatus = resp[0].status;
						db.model('intervention').update({_id:element}, {$set: {status:req.body.nStatus, "cache.s":numStatus, statusSAV:saveStatus, "cache.sa":saveStatus }}, function(err, resp) {
							if (err)
								console.log("Erreur tranfo status: ", err);
						})
					})
				}
				else
				{
					db.model('intervention').update({_id:element}, {$set: {status:req.body.nStatus, "cache.s":numStatus, statusSAV:"", "cache.sa":"" }}, function(err, resp) {
						if (err)
							console.log("Erreur tranfo status: ", err);
					})
				}
			})
			db.model('intervention').fullReload().then(function(err) {
				return (resolve("Ok"));
			});
		})
	}
}
