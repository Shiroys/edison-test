module.exports = function(schema) {
    schema.statics.litigeInjonc = {
	unique: true,
	findBefore: true,
	method: 'POST',
	fn: function(inter, req, res) {
	    return new Promise(function(resolve, reject) {
		var RelanceClient = requireLocal('config/_relances-client');
		var relance = RelanceClient(inter, 'relance-client-3', inter.email)
		relance.send(function(){})
		resolve("ok")
	    })
	}
    }
}
