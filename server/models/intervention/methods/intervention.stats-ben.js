module.exports = function(schema) {
    var moment = require('moment')
    var _ = require('lodash')
    schema.statics.statsBen = function(req, res) {

        return new Promise(function(resolve, reject) {
            var q = {};
            var options = {};
            req.query.year = parseInt(req.query.year);
            //date
//POUR INTERVENTION
            if (req.query.group === 'day') {
                options.divider = 'jours'
                var date = new Date(req.query.year, req.query.month);
                options.dateRange = {
                    $gte: new Date(date.getFullYear(), date.getMonth() - 1, 1, -1),
                    $lt: new Date(date.getFullYear(), date.getMonth(), 1)
                }

                options.groupId = {
                    $dayOfMonth: "$date.ajout"
                }
                options.maxRange = 32

            } else if (req.query.group == 'month') {
                options.divider = 'mois'
                options.dateRange = {
                    $gte: new Date(req.query.year, 0, 1),
                    $lt: new Date(req.query.year + 1, 0, 1)
                }
                options.groupId = {
                    $month: "$date.ajout"
                }
                options.maxRange = 13

            } else if (req.query.group == 'week') {
                options.divider = 'semaine'
                options.dateRange = {
                    $gte: new Date(req.query.year, 0, 1),
                    $lt: new Date(req.query.year + 1, 0, 1)
                }
                options.groupId = {
                    $week: "$date.ajout"
                }
                options.maxRange = 53
            }



	    if (req.query.partenariat == 'true')
	    {
		options.match = {
                    'status': {
			$in: ['ENC', 'VRF']
                    },
		    'partenariat': true
		}
	    }
	    else
	    {
		options.match = {
                    'status': {
			$in: ['ENC', 'VRF']
                    }
		}
	    }

            var divider = {
               chiffre: {
                    post: function(resp) {
                        return {
                            title: req.query.model + ' / ' + options.divider,
                            series: [{
                                name: 'potentiel',
                                data: db.utils.pluck(resp, 'potentiel', options.maxRange),
                                color: "#26C6DA"
                            }, {
                                name: 'recu',
                                data: db.utils.pluck(resp, 'recu', options.maxRange),
                                color: "#4CAF50"
                            }],
                            categories: _.map(_.range(1, options.maxRange), String)
                        }
                    },
                    project: function() {
                        return {
                            _id: options.groupId,
                            recu: db.utils.sumCond('$compta.reglement.recu', true, db.utils.prix()),
                            potentiel: db.utils.sumCond('$compta.reglement.recu', false, db.utils.prix()),
                        }
                    }
                },
                chiffre2: {
                    post: function(resp) {
                        return {
                            title: req.query.model + ' / ' + options.divider,
                            series: [
                              {
                                 name: 'en cours',
                                 name2:'enCours',
                                 data: db.utils.pluck(resp, 'enCours', options.maxRange),
                                 color: "#FF9800"
                             },
                              {
                                 name: 'a verifier',
                                 name2:'enCours',
                                 data: db.utils.pluck(resp, 'aVerifier', options.maxRange),
                                 color: "#4E342E"
                             },
                              {
                                name: 'verifié',
                                name2:'potentiel',
                                data: db.utils.pluck(resp, 'verifier', options.maxRange),
                                color: "#26C6DA"
                            }, {
                                name: 'encaissé',
                                name2:'recu',
                                data: db.utils.pluck(resp, 'encaisser', options.maxRange),
                                color: "#4CAF50"
                            }
                          ],
                            categories: _.map(_.range(1, options.maxRange), String)
                        }
                    },
                    project: function() {
                        return {
                            _id: options.groupId,
                            encaisser: db.utils.sumCondCond('$compta.reglement.recu', true,'$status','VRF', db.utils.prix()),
                            verifier: db.utils.sumCondCond('$compta.reglement.recu', false,'$status','VRF', db.utils.prix()),
                            aVerifier: db.utils.sumCondCond('$compta.reglement.recu', false,'$cache.s',3, db.utils.prix()),
                            enCours: db.utils.sumCondCond('$compta.reglement.recu', false,'$cache.s',0, db.utils.prix())

                        }
                    }
                },
                categorie: {
                    post: function(resp) {
                        var categories = requireLocal('config/dataList').categories;
                        var series = _.map(categories, function(e) {
                            e.name = e.short_name;
                            e.data = db.utils.pluck(resp, e.name, options.maxRange)
                            e.color = e.color_hex;
                            return _.pick(e, 'name', 'color', 'data');
                        })

                        return {
                            title: req.query.model + ' / ' + options.divider,
                            series: series,
                            categories: _.map(_.range(1, options.maxRange), String)
                        }
                    },
                    project: function() {
                        var categories = requireLocal('config/dataList').categories;
                        var rtn = {
                            _id: options.groupId,
                        }
                        _.each(categories, function(e) {
                            rtn[e.short_name] = db.utils.sumCond('$categorie', e.short_name, db.utils.prix())
                        })
                        return rtn;
                    }
                },
                telepro: {
                    post: function(resp) {
                        var telepro = edison.users.service('INTERVENTION');
                        var series = _.map(telepro, function(e) {
                            return {
                                name: e,
                                data: db.utils.pluck(resp, e, options.maxRange)
                            }
                        })
                        return {
                            title: req.query.model + ' / ' + options.divider,
                            series: series,
                            categories: _.map(_.range(1, options.maxRange), String)
                        }
                    },
                    project: function() {
                        var telepro = edison.users.service('INTERVENTION');
                        var rtn = {
                            _id: options.groupId,
                        }
                        _.each(telepro, function(e) {
                            rtn[e] = db.utils.sumCond('$login.ajout', e, db.utils.prix())
                        })
                        return rtn;
                    }
                },


            }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//POUR SAVESTATS
            if (req.query.group === 'day') {
                options.divider = 'jours'
                var date = new Date(req.query.year, req.query.month);
                options.dateRange2 = {
                    $gte: new Date(date.getFullYear(), date.getMonth() - 1, 1, -1),
                    $lt: new Date(date.getFullYear(), date.getMonth(), 1)
                }

                options.groupId2 = {
                    $dayOfMonth: "$date"
                }
                options.maxRange = 32

            } else if (req.query.group == 'month') {
                options.divider = 'mois'
                options.dateRange2 = {
                    $gte: new Date(req.query.year, 0, 1),
                    $lt: new Date(req.query.year + 1, 0, 1)
                }
                options.groupId2 = {
                    $month: "$date"
                }
                options.maxRange = 13

            } else if (req.query.group == 'week') {
                options.divider = 'semaine'
                options.dateRange2 = {
                    $gte: new Date(req.query.year, 0, 1),
                    $lt: new Date(req.query.year + 1, 0, 1)
                }
                options.groupId2 = {
                    $week: "$date"
                }
                options.maxRange = 53
            }

            options.match2 = {
            }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            var divider2 = {
              suivi:  {
                  post: function(res, resp) {
                      return {
                          title: req.query.model + ' / ' + options.divider,
                          series: [
                            {
                              name: 'réglement sur place',
                              data: db.utils.pluck(resp, 'surPlace', options.maxRange),
                              color: "#37B933"
                          }, {
                              //type:'spline',
                              name: 'factures à envoyer',
                              data: db.utils.pluck(resp, 'ailleurs', options.maxRange),
                              color: "#B93333"
                          },{
                              type:'spline',
                              name: 'suivi des factures à envoyer',
                              data: db.utils.pluck(res, 'suivi', options.maxRange),
                              color: "#40A497"
                          }

                        ],


                          categories: _.map(_.range(1, options.maxRange), String)
                      }
                  },
                  project: function() {
                      return {
                          _id: options.groupId,
                          surPlace: db.utils.sumCondCond('$compta.reglement.recu', true,'$reglementSurPlace',true, db.utils.prix()),
                          ailleurs: db.utils.sumCondCond('$compta.reglement.recu', true,'$reglementSurPlace',false, db.utils.prix()),
                          suivi:    db.utils.sum("$montant")


                      }
                  },
                  project2: function() {
                      return {
                          _id: options.groupId2,
                          suivi:    db.utils.sum("$montant")


                      }
                  }
              }
              }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
              if(req.query.divider==='suivi'){
                options.match['date.ajout'] = options.dateRange
                db.model('intervention').aggregate()
                  .match(options.match)
                  .group(divider2[req.query.divider].project())
                  .exec(function(err, resp) {
                    options.match2['date'] = options.dateRange2
                    db.model('save_stats').aggregate()
                      .match(options.match2)
                      .group(divider2[req.query.divider].project2())
                      .exec(function(err, res) {
                          var rtn = divider2[req.query.divider].post(res, resp);
                          resolve(rtn);
                      })
                  })
                }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            else if(req.query.divider==='chiffre'||req.query.divider==='chiffre2'
              ||req.query.divider==='chiffre3'||req.query.divider==='telepro'||req.query.divider==='categorie'){
                  options.match['date.ajout'] = options.dateRange
                  db.model('intervention').aggregate()
                    .match(options.match)
                    //.lookup(options.lookup)
                    .group(divider[req.query.divider].project())
                    .exec(function(err, resp) {
                      var rtn = divider[req.query.divider].post(resp);
                      resolve(rtn);
                })
             }
        })
    }
}
