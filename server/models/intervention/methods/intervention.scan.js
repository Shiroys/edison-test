module.exports = function(schema) {

    var stampToFormat = function(moment, stamp)
    {
        return (moment(stamp).format('YYYY-MM-DD-HH-mm-ss') + '.pdf');
    }

	function waitSeconds(milliTimer)
	{
		var counter = 0;
		var start = new Date().getTime();
		var end = 0;

		while(counter < milliTimer)
		{
			end = new Date().getTime();
			counter = end - start;
		}
	}

    schema.statics.scan = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(inter, req, res) {
            var moment = require('moment-timezone');
            return new Promise(function(resolve, reject) {
				var from = moment.tz('Europe/Paris');
				var id = inter.id;
				var stop = false;
        		setTimeout(function() {
            		var start = from.clone();
            		document.list('/SCAN').then(function(dbl) {
                		for (var n = 0; n < 30 && stop == false; n++)
                		{
			    			if (n % 2 == 0)
								from.add((n / 2), 'seconds');
			    			else
								from.subtract((n / 2) + 0.5, 'seconds');
							dbl.forEach(function(element, index, array) {
								var nameFile = element;
                       			if (nameFile == stampToFormat(moment, from).toString())
                        		{
				    				stop = true;
                            		document.copy('/SCAN/' + element, '/SCAN_ARCHIVES/' + element);
                            		setTimeout( function() {
										document.move('/SCAN/' + element, '/V2_PRODUCTION/intervention/' + id + '/' + element);
									}, 5000);
				    				resolve("ok");
                        		}
								else if (nameFile.replaceAll('_', '-') == stampToFormat(moment, from).toString())
                        		{
				    				stop = true;
                            		document.copy('/SCAN/' + element, '/SCAN_ARCHIVES/' + element);
									setTimeout( function() {
										document.move('/SCAN/' + element, '/V2_PRODUCTION/intervention/' + id + '/' + element);
									}, 5000);
				    				resolve("ok");
                        		}

                			})
							from = start.clone();
                		}
            		})
       			}, 30000)
            })
        }
    }
}
