module.exports = function(schema) {
	schema.statics.getSeuilRuptArtisan = function (req, res) {
		return new Promise(function(resolve, reject) {
			var id = req.body.id;
			db.model('artisan').findOne({'id': id,}).then(function (resp) {
				res.status(200).send(resp.seuilRupture.toString());
			}, reject);
		})
	}
}
