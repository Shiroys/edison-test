module.exports = function(schema) {
    schema.statics.rappelDateIntervention = function(req, res) {
		return new Promise(function(resolve, reject) {
			var moment = require('moment')
			var _ = require('lodash')
			var now = moment().toDate();
			var inOneHour = moment().add('3', 'hours').toDate()
			var inTwoHour = moment().add('4', 'hours').toDate()
			var twoDaysAgo = moment().add('-1', 'days').toDate()
			var textTemplate = requireLocal('config/textTemplate');


			db.model('intervention').find({
				'date.intervention': db.utils.between(inOneHour, inTwoHour),
				'date.ajout': {
					$lt: twoDaysAgo
				},
				'status': 'ENC'
			}).lean().populate('sst').then(function(resp) {
			    _.each(resp, function(e) {
				
					if (!e.sst)
					    return 0
				moment.locale('fr')
					var text = _.template(textTemplate.sms.intervention.rappelArtisan())({
						e: e,
					    datePlain: moment(e.date.intervention).format("H[h]mm"),
					    date: moment(e.date.intervention).format('dddd')[0].toUpperCase() + moment(e.date.intervention).format('dddd').slice(1) + " " + e.date.intervention.getDate() + '/' + (e.date.intervention.getMonth() + 1) + '/' + e.date.intervention.getFullYear()
					})
					sms.send({
						type: "RAPPEL",
						dest: e.sst.telephone.nomSociete,
						to: e.sst.telephone.tel1,
						text: text,
					})
				})
				return resolve('ok')
			})
		})
	}
}
