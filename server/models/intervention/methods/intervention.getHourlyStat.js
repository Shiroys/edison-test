module.exports = function(schema) {
    var _ = require('lodash')
    var async = require('async');
    var moment = require('moment');
    
    schema.statics.getHourlyStat = function(req, res) {
	var dt = req.body.date;
	var dtprec = req.body.dateprec;
	return new Promise(function(resolve, reject) {
	    var q = {};
	    var options = {};
	    req.query.year = parseInt(req.query.year);
	    if (req.query.group === 'day')
	    {
			options.divider = " en cours"
			var date = new Date(req.query.year, req.query.month);
			options.dateRange = {
			    $gte: new Date(date.getFullYear(), date.getMonth() - 1, 1, -1),
			    $lt: new Date(date.getFullYear(), date.getMonth(), 1)
			}
			options.groupId = {
			    $dayOfMonth: "$date.ajout"
			}
			options.maxRange = 32
	    }
	    else if (req.query.group == 'hour')
	    {
			options.divider = ' par heure'
			var date = new Date();
			options.dateRange = {
			    $gte: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
			    $lt: new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1)
			}
			options.dateRange.$gte.setHours(7);
			options.dateRange.$lt.setHours(20);
			options.groupId = {
			    $hour: "$date.ajout"
			}
			options.maxRange = 20
	    }
	    options.match = {
			'status': {
			    $in: ['APR', 'AVR','ENC', 'VRF','ANN']
			}
	    }
	    var divider = {
			ajoutI: {
			    post: function(resp) {
					var telepro = edison.users.service('INTERVENTION');
					var obs = edison.users.serviceObs('INTERVENTION');
					var series = _.map(telepro, function(e) {
					    return {
						name: e,
						obs: obs.indexOf(e) >= 0,
						data: db.utils.pluck(resp, e, options.maxRange),
					    }
					})
					return {
					    title: "Ajout d'intervention/devis " + options.divider,
					    series: series,
					    categories: _.map(_.range(1, options.maxRange), String)
					}
			    },
			    project: function() {
					var telepro = edison.users.service('INTERVENTION');
					var rtn = {
					    _id: options.groupId,
					}
					_.each(telepro, function(e) {
					    rtn[e] = db.utils.sumMod(e, 1);
					})
					return rtn;
			    }
			},
			ajoutD: {
			    post: function(resp) {
					var telepro = edison.users.service('INTERVENTION');
					var obs = edison.users.serviceObs('INTERVENTION');
					var series = _.map(telepro, function(e) {
					    return {
						name: e,
						obs: obs.indexOf(e) >= 0,
						data: db.utils.pluck(resp, e, options.maxRange),
						color: "#FFA500",
						linkedTo: ':previous'
					    }
					})
					return {
					    title: "Ajout de devis" + options.divider,
					    series: series,
					    categories: _.map(_.range(1, options.maxRange), String)
					}
			    },
			    project: function() {
					var telepro = edison.users.service('INTERVENTION');
					var rtn = {
					    _id: options.groupId,
					}
					_.each(telepro, function(e) {
					    rtn[e] = db.utils.sumMod(e, 1);
					})
					return rtn;
			    }
			},
			ajoutAn: {
			    post: function(resp) {
				var telepro = edison.users.service('INTERVENTION');
				var obs = edison.users.serviceObs('INTERVENTION');
				var series = _.map(telepro, function(e) {
				    return {
					name: e,
					obs: obs.indexOf(e) >= 0,
					data: db.utils.pluck(resp, e, options.maxRange),
					color: "#FF0000",
					linkedTo: ':previous'
				    }
				})
				return {
				    title: "Ajout annulés et a programmer" + options.divider,
				    series: series,
				    categories: _.map(_.range(1, options.maxRange), String)
				}
			    },
			    project: function() {
				var telepro = edison.users.service('INTERVENTION');
				var rtn = {
				    _id: options.groupId,
				}
				_.each(telepro, function(e) {
				    rtn[e] = db.utils.sumAnn('$login.ajout', e, 1, 'ANN');
				})
				    return rtn;
			    }
			},
			ajoutPr: {
			    post: function(resp) {
					var telepro = edison.users.service('INTERVENTION');
					var obs = edison.users.serviceObs('INTERVENTION');
					var series = _.map(telepro, function(e) {
					    return {
						name: e,
						obs: obs.indexOf(e) >= 0,
						data: db.utils.pluck(resp, e, options.maxRange),
						color: "#0000FF",
						linkedTo: ':previous2'
					    }
					})
					return {
					    title: "Ajout annulés et a programmer" + options.divider,
					    series: series,
					    categories: _.map(_.range(1, options.maxRange), String)
					}
			    },
			    project: function() {
					var telepro = edison.users.service('INTERVENTION');
					var rtn = {
					    _id: options.groupId,
					}
					_.each(telepro, function(e) {
					    rtn[e] = db.utils.sumSt(e, 'APR');
					})
					return rtn;
			    }
			},
			CA: {
			    post: function(resp) {
				var telepro = edison.users.service('INTERVENTION');
				var obs = edison.users.serviceObs('INTERVENTION');
				var series = _.map(telepro, function(e) {
				    return {
					name: e,
					obs: obs.indexOf(e) >= 0,
					data: db.utils.pluck(resp, e, options.maxRange),
				    }
				})
				return {
				    title: "Chiffre d'affaire " + options.divider,
				    series: series,
				    categories: _.map(_.range(1, options.maxRange), String)
				}
			    },
			    project: function() {
				var telepro = edison.users.service('INTERVENTION');
				var rtn = {
				    _id: options.groupId,
				}
				_.each(telepro, function(e) {
				    rtn[e] = db.utils.sumBoth(e)
				})
				    return rtn;
			    }
			}
	    }
	    options.match['date.ajout'] = options.dateRange
	    if (req.query.divider != 'CA')
	    {
			db.model('intervention').aggregate()
	            .match(options.match)
	            .group(divider['ajoutI'].project())
		    	.exec(function(err, resp) {
					var rtn = []
					rtn.push(divider['ajoutI'].post(resp));
					if (req.query.group === "day")
					{
					    db.model('intervention').aggregate()
						.match(options.match)
						.group(divider['ajoutAn'].project())
						.exec(function(err, resp) {
						    rtn.push(divider['ajoutAn'].post(resp));
						    db.model('intervention').aggregate()
							.match(options.match)
							.group(divider['ajoutPr'].project())
							.exec(function(err, resp) {
							    rtn.push(divider['ajoutPr'].post(resp));
							    resolve(rtn);
							})
						})
					}
					else
					{
					    options.match.status = {
							$in: ['AEV','ANN', 'TRA','ATT']
					    }
					    db.model('devis').aggregate()
						.match(options.match)
						.group(divider['ajoutD'].project())
						.exec(function(err, resp) {
						    rtn.push(divider['ajoutD'].post(resp));
						    resolve(rtn);
						})
					}
			    })
		    }
	    else
	    {
			db.model('intervention').aggregate()
		        .match(options.match)
		        .group(divider['CA'].project())
			    .exec(function(err, resp) {
					resolve(divider['CA'].post(resp));
			    })
	    }
	})
    }
}    
