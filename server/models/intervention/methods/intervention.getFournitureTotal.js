module.exports = function(schema) {
  var config = requireLocal('config/dataList')

    schema.statics.getFourniture = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var year = req.query.year;
	    var listFournis = config.fournisseurEdison();

	    var date = new Date("01/01/" + year);
	    var date2 = new Date("01/01/" + (parseInt(year) + 1));
	    db.model('intervention').find({"fourniture.fournisseur": {$in: listFournis}, "date.ajout": {$gt: date, $lt: date2}}).lean().then(function (resp) {
		var elems = {}
		for (var n = 0; n < listFournis.length; n++)
		    elems[listFournis[n]] = new Array(12).fill(0);
		for (var n = 0; n < resp.length; n++)
		{
		    for (var l = 0; l < resp[n].fourniture.length; l++)
			elems[resp[n].fourniture[l].fournisseur][resp[n].date.ajout.getMonth()] += resp[n].fourniture[l].quantite * resp[n].fourniture[l].pu
		}
		resolve(elems);
	    })
	})
    };
}
