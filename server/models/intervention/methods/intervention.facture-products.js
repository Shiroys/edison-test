module.exports = function(schema) {
    schema.statics.getFact = function(req, res) {
        return new Promise(function(resolve, reject) {
            var _ = require('lodash');
            var async = require('async');
            var V1 = requireLocal('config/_convert_V1');

            if (!isWorker) {
                return edison.worker.createJob({
                    name: 'db',
                    model: 'intervention',
                    method: 'getFact',
                    req: _.pick(req, 'query', 'session')
                })
            }
            console.time('get');
            db.model('devis').find({
                status: {
                    $ne: 'TRA'
                },
                id: {
                    $gt: 13000
                },
                $where: 'this.produits.length'
            }).then(function(resp) {
                var i = 0;
                async.eachLimit(resp, 20, function(e, callback) {
                    var v1 = new V1(e);
                    v1.send(function(resp) {
                        callback(null);
                    });
                    v1 = null;
                })
            }, resolve)
            return 0;

            edison.v1.get("SELECT * FROM ligne_facture AS l JOIN objetfacture AS o ON o.id=l.id_objet_facture", function(err, x) {
                var lol = _(x.slice(0, 1)).map(function(e)  {
                        var w = {
                            devisTab: {
                                id_intervention: e.id_intervention,
                                quantite: e.quantite,
                                pu: parseFloat(e.puht),
                                ref: e.reference,
                                title: e.designation.toUpperCase().split(' ').slice(0, 3).join(' ').replaceAll("\r\n", " "),
                                desc: e.designation.replaceAll("\r\n", "<br>"),
                            },
                            dump: new Date
                        }
                        return w
                    }).groupBy('id_intervention').value()
            })
            resolve('ok')
        })
    }
}
