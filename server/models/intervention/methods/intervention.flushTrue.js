module.exports = function(schema) {

	/* Fonction de Flush true all interventions */
	schema.statics.flushTrue = function(req, res) {
		return new Promise(function(resolve, reject) {
			var ind = 0;
			var start = new Date(2016, 1, 1);
			var end = new Date();
			db.model('intervention').find({status: "VRF", "cache.ps": 2, "date.ajout": { $gte: start, $lt: end }}).cursor()
				.on('error', function(err) {
					console.log("----->Error: ", err);
					reject(err);
				})
				.on('data', function(elem) {
					ind++;
					if (elem.compta.paiement.ready == false)
					{
						elem.compta.paiement.ready = true;
						elem.save().then(function(resp) {})
					}
				})
				.on('end', function() {
					resolve("Ok");
				})
		})
	}
}
