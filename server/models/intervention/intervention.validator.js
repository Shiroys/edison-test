module.exports = function(schema) {
    var creditcard = require('creditcard');
    var key = requireLocal('config/_keys');
    var V1 = requireLocal('config/_convert_V1');
    var encryptor = require('simple-encryptor')(key.salt);
    var _ = require('lodash')
    var moment = require('moment');
    var UP = function(obj) {
        if (obj) {
            _.each(obj, function(e, k) {
                if (typeof e === 'string')
                    obj[k] = obj[k].toUpperCase();
            })
        }
    }
    var validatorPreSave = function(next) {
        var _this = this;
        UP(this.facture.address)
        UP(this.facture)
        UP(this.client.address)
        _this.coutFourniture = _.reduce(_this.fourniture, function(total, e) {
            return total += (e.quantite * e.pu)
        }, 0)
        _this.sst = _this.artisan.id
        _this.enDemarchage = _this.login.demarchage;
        _this.cache = db.model('intervention').Core.minify(_this);
        _this.conversations = _.uniq(_this.conversations, JSON.stringify);
        if (isWorker && _this.cb.number) {
            if (!creditcard.validate(_this.cb.number))
                return next(new Error('Numero de carte invalide'))
            _this.cb = {
                hash: encryptor.encrypt(JSON.stringify(_this.cb)),
                preview: "**** ".repeat(3) + _this.cb.number.slice(-4)
            }
        }
        return next();
    }

    var validatorPostSave = function(doc) {
	if (doc.array !== true)
	{
            if (!doc.client.address.lt) {
		db.model('intervention').geolocateAddress(doc);
            }
            if (!isWorker) {
		if (doc.artisan.id) {
                    db.model('artisan').findOne({
			id: doc.artisan.id
                    }).then(function(sst) {
			if (sst) {
                            sst.date.dump = Date.now();
                            sst.save().then();
			}
                    })
		}
		db.model('intervention').uniqueCacheReload(doc)
		if (envProd && (!doc.date.dump || moment().subtract(5000).isAfter(doc.date.dump))) {
                    var v1 = new V1(doc);
                    v1.send(function(resp) {
                    });
		}
            }
	}
    }

    schema.pre('save', function(next) {
        validatorPreSave.bind(this)(next)
    });


    schema.post('save', validatorPostSave)

    schema.post('findOneAndUpdate', validatorPostSave)
}
