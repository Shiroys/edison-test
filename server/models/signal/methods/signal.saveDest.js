module.exports = function(schema) {

    schema.statics.saveDest = function(req, res) {
        return new Promise(function(resolve, reject) {
			db.model('signal').findOne({ nom: req.body.nom }).exec(function(err, newSignal) {
				if(err) {
					console.log("Erreur find Signal (saveDest): ", err);
					reject(err);
				}
				else {
					if (newSignal) {
						newSignal.dest = req.body.signal.dest;
						newSignal.save(function(err, resp) {
							if(err) {
								console.log("Erreur save signal (saveDest): ", err)
								reject(err);
							}
							else {
								resolve(resp);
							}
						})
					}
					else {
						db.model('signal').create(req.body.signal, function(err, createdSig) {
							if (err) {
								console.log("Erreur create signal (saveDest): ", err)
								reject(err);
							}
							else {
								resolve(createdSig);
							}
						})
					}
				}
			})
        })
    }
}