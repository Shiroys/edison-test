module.exports = function(schema) {
/* Methods pour update les signalements quand on met à jour les users */
	var _ = require('lodash')
    schema.statics.update = function(req, res) {
        return new Promise(function(resolve, reject) {
			db.model('signal').findOne({ nom: req.body.signal.nom }, function(err, updatedSignal) {
				if (!updatedSignal)
				{
					console.log("Erreur update signal: ", err);
					reject(err);
				}
				/* Deep copy of the two array */
				var tmpDest = [];
				for (var index = 0; index < updatedSignal.dest.users.length; index++)
					tmpDest[index] = updatedSignal.dest.users[index];
				updatedSignal.dest.users = [];
				for (var index = 0; index < req.body.users.length; index++)
					updatedSignal.dest.users[index] = req.body.users[index];
				/* Check et actualise la variable send en comparant les deux tableaux précédant */
				for (var ind1 = 0; ind1 < updatedSignal.dest.users.length; ind1++)
				{
					for (var ind2 = 0; ind2 < tmpDest.length; ind2++)
					{
						if (updatedSignal.dest.users[ind1].login == tmpDest[ind2].login)
						{
							if (tmpDest[ind2].send === true || tmpDest[ind2].send === false)
								updatedSignal.dest.users[ind1].send = tmpDest[ind2].send;
						}
					}
				}
				resolve(updatedSignal);
			})
        })
    }
}
