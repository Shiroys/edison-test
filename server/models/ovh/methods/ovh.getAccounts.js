module.exports = function(schema) {
    var async = require('async');
    var moment = require('moment');

    var service = require('ovh')({
	appKey: 'vlVsZ2QAN1zUg1R8',
	appSecret: 'CrCyuD9t263hDesjne21fCjujZKWriIh',
	consumerKey: 'jhcb1da0Y7XL4VLVQTaSQFGzTqQdHb1I'
    });

    schema.statics.getPhone = function (req, res) {
	return new Promise(function (resolve, reject) {
	    db.model('user').find().then(function (users) {
		var userTel = []
		var indexTel = {}
		var ret = {}
		for (var n = 0; n < users.length; n++)
		{
		    if (users[n].ligne && users[n].activated === true && userTel.indexOf(users[n].ligne) === -1)
		    {
			ret[users[n].ligne] = users[n].login;
			userTel.push(users[n].ligne);
			indexTel[users[n].ligne] = n
		    }
		    else if (users[n].ligne && users[n].activated === true)
		    {
			ret[users[indexTel[users[n].ligne]].ligne] = users[indexTel[users[n].ligne]].service
		    }
		}
		resolve(ret);
	    })
	})
    }

    schema.statics.getBill = function (req, res) {
	return new Promise(function(resolve, reject) {
	    var ret = {}

	    var infoFc = function (num, cb) {
		service.request('GET', '/telephony/' + num, function (err, service) {
		    if (!err)
			ret[num] = service.description.replace(/\./g, ' ')
		    if (service.description.replace(/\./g, ' ') == "")
			ret[num] = num;
		    cb(null)
		})
	    }
	    
	    async.eachLimit(req.query.val, 4, infoFc, function (err) {
		resolve(ret)
	    })	    
	})
    }

    schema.statics.getInfo = function (req, res) {
	return new Promise(function(resolve, reject) {
	    var ret = {}

	    var infoFc = function (num, cb) {
		service.request('GET', '/telephony/' + req.query.key + '/service/' + num, function (err, service) {
		    if (!err)
			ret[num] = service.description.replace(/\./g, ' ')
		    cb(null)
		})
	    }
	    
	    async.eachLimit(req.query.val, 4, infoFc, function (err) {
		resolve(ret)
	    })
	})
    }
    
    schema.statics.getAccounts = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var ret = {accounts: [], numbers: {}, info: {}}
	    service.request('GET', '/telephony', function (err, tel) {
		if (!err)
		{		    
		    var infoFc = function (num, cb) {
			var elem = this.elem;
			service.request('GET', '/telephony/' + elem + '/service/' + num, function (err, service) {
			    if (!err)
			    {
				ret.info[num] = {
				    acc: elem,
				    name: service.description
				};
			    }
			    cb(null)
			})
		    }

		    var serviceFc = function (elem, cb) {
			service.request('GET', '/telephony/' + elem + '/service', function (err, num) {
			    if (!err)
			    {
				ret.accounts.push(elem);
				ret.numbers[elem] = num;
				//async.eachLimit(num, 1, infoFc.bind({elem: elem}), function (err) {
				cb(null)
				//})
			    }
			    else
				cb(null)
			})
		    }

		    async.eachLimit(tel, 4, serviceFc, function (err) {
			resolve(ret)
		    })
		}
	    })
	})
    }
}
