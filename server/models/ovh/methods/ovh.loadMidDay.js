module.exports = function(schema) {
    var async = require('async');
    var moment = require('moment');

    var service = require('ovh')({
	appKey: 'vlVsZ2QAN1zUg1R8',
	appSecret: 'CrCyuD9t263hDesjne21fCjujZKWriIh',
	consumerKey: 'jhcb1da0Y7XL4VLVQTaSQFGzTqQdHb1I'
    });

    schema.statics.loadMidDay = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var dayStart = new Date(req.body.params.dayStart)
	    service.request('GET', '/telephony', function (err, tel) {
		if (!err)
		{
		    var convFc = function (record, cb) {
			var elem = this.elem;
			var serv = this.serv;
			service.request('GET', '/telephony/' + elem + '/ovhPabx/' + serv + '/records/' + record, function (err, rec) {
			    if (!err)
			    {
				var date = new Date(rec.callStart);
				if (date.getDate() === dayStart.getDate() &&
				    date.getMonth() === dayStart.getMonth() &&
				    date.getFullYear() === dayStart.getFullYear() &&
				    date.getHours() >= req.body.params.intervalI &&
				    date.getHours() < req.body.params.intervalS)
				    db.model('ovh').linkToInter(rec);
			    }
			    cb(null)
			})
		    }

		    var recordFc = function (serv, cb) {
			var elem = this.elem;
			service.request('GET', '/telephony/' + elem + '/ovhPabx/' + serv + '/records', function (err, records) {
			    if (!err)
			    {
				async.eachLimit(records, 1, convFc.bind({serv: serv, elem: elem}), function (err) {
				    cb(null)
				})
			    }
			    else
				cb(null)
			})
		    }

		    var serviceFc = function (elem, cb) {
			service.request('GET', '/telephony/' + elem + '/ovhPabx', function (err, serv) {
			    if (!err)
			    {
				async.eachLimit(serv, 1, recordFc.bind({elem: elem}), function (err) {
				    cb(null)
				})
			    }
			    else
				cb(null)
			})
		    }
		    async.eachLimit(tel, 1, serviceFc, function (err) {
			db.model('ovh').findOne({year: dayStart.getFullYear(), month: dayStart.getMonth(), day: dayStart.getDate()}).then(function (ovh) {
			    if (ovh === null)
			    {
				ovh = {
				    year: dayStart.getFullYear(),
				    month: dayStart.getMonth(),
				    day: dayStart.getDate()
				}
			    }
			    if (req.body.params.intervalS >= 12)
				ovh.loaded_twelve = true;
			    if (req.body.params.intervalS >= 17)
				ovh.loaded_sixteen = true;
			    if (req.body.params.intervalS >= 20)
				ovh.loaded_late = true;
			    db.model('ovh')(ovh).save().then(resolve, reject);
			})
		    })
		}
	    })
	})
    }
}
