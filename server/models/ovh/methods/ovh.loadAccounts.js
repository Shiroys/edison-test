module.exports = function(schema) {
    var async = require('async');
    var moment = require('moment');

    var service = require('ovh')({
	appKey: 'vlVsZ2QAN1zUg1R8',
	appSecret: 'CrCyuD9t263hDesjne21fCjujZKWriIh',
	consumerKey: 'jhcb1da0Y7XL4VLVQTaSQFGzTqQdHb1I'
    });

    var containsObject = function (array, number, date) {
	for (var n = 0; n < array.length; n++)
	{
	    if (array[n].number === number && array[n].date === date)
		return (true);
	}
	return (false);
    }

    var containsNumber = function (array, number) {
	for (var n = 0; n < array.length; n++)
	{
	    if (array[n].number === number)
		return (true);
	}
	return (false);
    }


    schema.statics.loadDemStats = function(req, res) {
	return new Promise(function(resolve, reject) {
	    if (req.body.params.type === 'single')
	    {
		var dayStart = new Date(req.body.params.dayStart)
		var dayEnd = new Date(req.body.params.dayStart)
		dayEnd.setDate(dayStart.getDate() + 1);
	    }
	    else
	    {
		var dayStart = new Date(moment(req.body.params.dayStart, 'DD-MM-YYYY').add(1, 'h'))
		var dayEnd = new Date(moment(req.body.params.dayStart, 'DD-MM-YYYY').add(1, 'h'))
		if (req.body.params.type === 'week')
		    dayEnd.setDate(dayStart.getDate() + 7);
		else
		    dayEnd.setMonth(dayStart.getMonth() + 1);
	    }
	    if (req.body.params.type === 'single')
	    {
		db.model('ovh').findOne({year: dayStart.getFullYear(), month: dayStart.getMonth(), day: dayStart.getDate()}).then(function (ovh) {
		    if (!ovh.history_sec)
		    {
			service.request('GET', '/telephony', function (err, tel) {
			    if (!err)
			    {
				var ret = {logs: {}, number: {}}

				var convFc = function (record, cb) {
				    var elem = this.elem;
				    var serv = this.serv;
				    service.request('GET', '/telephony/' + elem + '/ovhPabx/' + serv + '/records/' + record, function (err, rec) {
					if (!err)
					{
					    var date = new Date(rec.callStart);
					    /*if (date.getDate() === dayStart.getDate() &&
						date.getMonth() === dayStart.getMonth() &&
						date.getFullYear() === dayStart.getFullYear() &&
						date.getHours() >= 16)
						db.model('ovh').linkToInter(rec);*/
					}
					cb(null)
				    })
				}

				var consumFc = function (voice, cb) {
				    var elem = this.elem;
				    var serv = this.serv;
				    var name = this.name;
				    var consum = new Date().getMonth() > dayEnd.getMonth() ? '/previousVoiceConsumption/' : '/voiceConsumption/'
				    service.request('GET', '/telephony/' + elem + '/service/' + serv + consum + voice, function (err, data) {
					if (!err && data && (data.wayType === 'incoming' || data.wayType === 'transfer'))
					{
					    var an = data.calling === serv
					    //if (containsObject(ret.logs[elem][name.replace(/\./g, ' ')], data.calling.replace("0033", "0"), data.creationDatetime) === false)
					    ret.logs[elem][name.replace(/\./g, ' ')].push({number: data.calling.replace("0033", "0"), date: data.creationDatetime, duration: data.duration, an: an})
					}
					cb(null)
				    })
				}

				var recordFc = function (elem, serv, cb) {
				    service.request('GET', '/telephony/' + elem + '/ovhPabx/' + serv + '/records', function (err, records) {
					if (!err)
					{
					    async.eachLimit(records, 1, convFc.bind({serv: serv, elem: elem}), function (err) {
						cb(null)
					    })
					}
					else
					    cb(null)
				    })
				}

				var voicesFc = function (serv, cb) {
				    var elem = this.elem;
				    var consum = new Date().getMonth() > dayEnd.getMonth() ? '/previousVoiceConsumption' : '/voiceConsumption'
				    service.request('GET', '/telephony/' + elem + '/service/' + serv, function (err, name) {
					if (!err)
					{
					    ret.number[name.description.replace(/\./g, ' ')] = serv
					    ret.logs[elem][name.description.replace(/\./g, ' ')] = []
					    service.request('GET', '/telephony/' + elem + '/service/' + serv + consum + '?creationDatetime.from=' + dayStart.toISOString() + '&creationDatetime.to=' + dayEnd.toISOString(), function (err, voice) {
						if (!err)
						{
						    async.eachLimit(voice, 1, consumFc.bind({serv: serv, elem: elem, name: name.description}), function (err) {
							cb(null);
						    })
						}
						else
						    cb(null);
					    })
					}
					else
					    cb(null);
				    })
				}


				var serviceFc = function (elem, cb) {
				    ret.logs[elem] = {}
				    service.request('GET', '/telephony/' + elem + '/number', function (err, serv) {
					if (!err)
					{
					    async.eachLimit(serv, 1, voicesFc.bind({elem: elem}), function (err) {
						cb(null)
					    })
					}
					else
					    cb(null)
				    })
				}
				async.eachLimit(tel, 4, serviceFc, function (err) {
				    ovh.history_sec = ret;
				    ovh.loaded = true;
				    ovh.save(function (err2, mod) {
					resolve(ret)
				    })
				})
			    }
			})
		    }
		    else
			resolve(ovh.history_sec);
		})
	    }
	    else
	    {
		var ret = {logs: {}, number: {}}
		var end = dayEnd.getTime()
		var recur = function () {
		    if (dayStart.getTime() < end)
		    {
			db.model('ovh').findOne({year: dayStart.getFullYear(), month: dayStart.getMonth(), day: dayStart.getDate()}, {history_sec: 1}).lean().then(function (ovh) {
			    if (ovh !== null)
			    {
				Object.keys(ovh.history_sec.logs).forEach(function(key, index) {
				    if (key in ret.logs)
				    {
					Object.keys(ovh.history_sec.logs[key]).forEach(function (key2, index2) {
					    if (key2 in ret.logs[key])
						ret.logs[key][key2] = ret.logs[key][key2].concat(ovh.history_sec.logs[key][key2])
					    else
						ret.logs[key][key2] = ovh.history_sec.logs[key][key2];
					})
				    }
				    else
					ret.logs[key] = ovh.history_sec.logs[key]
				})
				Object.keys(ovh.history_sec.number).forEach(function(key, index) {
				    if (!(key in ret.number))
					ret.number[key] = ovh.history_sec.number[key]
				})
			    }
			    dayStart.setDate(dayStart.getDate() + 1);
			    recur();
			})
		    }
		    else
			resolve(ret);
		}
		recur()
	    }

	})
    }			   
    
    schema.statics.loadAccount = function(req, res) {
	return new Promise(function(resolve, reject) {
	    if (req.body.params.type === 'single')
	    {
		var dayStart = new Date(req.body.params.dayStart)
		var dayEnd = new Date(req.body.params.dayStart)
		dayEnd.setDate(dayStart.getDate() + 1);
	    }
	    else
	    {
		var dayStart = new Date(moment(req.body.params.dayStart, 'DD-MM-YYYY').add(1, 'h'))
		var dayEnd = new Date(moment(req.body.params.dayStart, 'DD-MM-YYYY').add(1, 'h'))
		if (req.body.params.type === 'week')
		    dayEnd.setDate(dayStart.getDate() + 7);
		else
		    dayEnd.setMonth(dayStart.getMonth() + 1);
	    }
	    if (req.body.params.type === 'single')
	    {
		db.model('ovh').findOne({year: dayStart.getFullYear(), month: dayStart.getMonth(), day: dayStart.getDate()}).then(function (ovh) {
		    if (ovh === null)
		    {
			ovh = {
			    year: dayStart.getFullYear(),
			    month: dayStart.getMonth(),
			    day: dayStart.getDate()
			}
		    }
		    if (!ovh.history)
		    {
			service.request('GET', '/telephony', function (err, tel) {
			    if (!err)
			    {
				db.model('user').find().then(function (users) {

				    var userTel = []
				    var indexTel = {}
				    var ret = {logs: {}, number: {}}
				    for (var n = 0; n < users.length; n++)
				    {
					if (users[n].ligne && users[n].activated === true && userTel.indexOf(users[n].ligne) === -1)
					{
					    userTel.push(users[n].ligne);
					    indexTel[users[n].ligne] = n
					    ret.number[users[n].login] = users[n].ligne;
					    ret.logs[users[n].login] = []
					}
					else if (users[n].ligne && users[n].activated === true )
					{
					    ret.logs[users[indexTel[users[n].ligne]].service] = []
					    ret.number[users[indexTel[users[n].ligne]].service] = users[n].ligne
					    if (users[indexTel[users[n].ligne]].login in ret.logs)
					    {
						delete ret.logs[users[indexTel[users[n].ligne]].login]
						delete ret.number[users[indexTel[users[n].ligne]].login]
					    }
					}
				    }

				    var consumFc = function (voice, cb) {
					var elem = this.elem;
					var serv = this.serv;
					var consum = new Date().getMonth() > dayEnd.getMonth() ? '/previousVoiceConsumption/' : '/voiceConsumption/'
					service.request('GET', '/telephony/' + elem + '/service/' + serv + consum + voice, function (err, data) {
					    if (!err && data && serv === data.called && data.wayType === 'incoming')
					    {
						var an = data.calling === serv
						if (users[indexTel[serv.replace("0033", "0")]].login in ret.logs)
						{
//						    if (containsNumber(ret.logs[users[indexTel[serv.replace("0033", "0")]].login], data.calling.replace("0033", "0")) === false)
						    ret.logs[users[indexTel[serv.replace("0033", "0")]].login].push({number: data.calling.replace("0033", "0"), date: data.creationDatetime, duration: data.duration, an: an})
						}
						else
						{
//						    if (containsNumber(ret.logs[users[indexTel[serv.replace("0033", "0")]].service], data.calling.replace("0033", "0")) === false)
						    ret.logs[users[indexTel[serv.replace("0033", "0")]].service].push({number: data.calling.replace("0033", "0"), date: data.creationDatetime, duration: data.duration, an: an})
						}
					    }
					    cb(null)
					})
				    }

				    var voicesFc = function (serv, cb) {
					var elem = this.elem;
					var consum = new Date().getMonth() > dayEnd.getMonth() ? '/previousVoiceConsumption' : '/voiceConsumption'
					if (userTel.indexOf(serv.replace("0033", "0")) != -1)
					{
					    service.request('GET', '/telephony/' + elem + '/service/' + serv + consum + '?creationDatetime.from=' + dayStart.toISOString() + '&creationDatetime.to=' + dayEnd.toISOString(), function (err, voice) {
						if (!err)
						{
						    async.eachLimit(voice, 1, consumFc.bind({serv: serv, elem: elem}), function (err) {
							cb(null)
						    })
						}
						else
						    cb(null)
					    })
					}
					else
					    cb(null);
				    }


				    var serviceFc = function (elem, cb) {
					service.request('GET', '/telephony/' + elem + '/line', function (err, service) {
					    if (!err)
					    {
						async.eachLimit(service, 1, voicesFc.bind({elem: elem}), function (err) {
						    cb(null)
						})
					    }
					    else
						cb(null)
					})
				    }

				    async.eachLimit(tel, 4, serviceFc, function (err) {
					ovh.history = ret
					db.model('ovh')(ovh).save().then(function (save) {
					    resolve(ret)
					})
				    })
				})
			    }
			})
		    }
		    else
			resolve(ovh.history);
		})
	    }
	    else
	    {
		var ret = {logs: {}, number: {}}
		var end = dayEnd.getTime()
		var recur = function () {
		    if (dayStart.getTime() < end)
		    {
			db.model('ovh').findOne({year: dayStart.getFullYear(), month: dayStart.getMonth(), day: dayStart.getDate()}, {history: 1}).lean().then(function (ovh) {
			    if (ovh !== null)
			    {
				Object.keys(ovh.history.logs).forEach(function(key, index) {
				    if (key in ret.logs)
					ret.logs[key] = ret.logs[key].concat(ovh.history.logs[key])
				    else
					ret.logs[key] = ovh.history.logs[key]
				})
				Object.keys(ovh.history.number).forEach(function(key, index) {
				    if (!(key in ret.number))
					ret.number[key] = ovh.history.number[key]
				})

			    }
			    dayStart.setDate(dayStart.getDate() + 1);
			    recur();
			})
		    }
		    else
			resolve(ret);
		}
		recur();
	    }
	})
    }
}
