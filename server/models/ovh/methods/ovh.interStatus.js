module.exports = function(schema) {
    var async = require('async');
    var moment = require('moment');
    var exec = require('child_process').exec;
    var fs = require('fs');
    var _ = require('lodash');

    var key = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var OTP = function (pass, n) {
		if (pass.length < n)
		    return OTP(pass + key[_.random(0, key.length - 1)], n);
		else
		    return pass
    }

    schema.statics.linkToInter = function (req, res) {
	return new Promise(function (resolve, reject) {
	    var caller = req.callerIdNumber.replace("0033", "0");
	    if (!fs.existsSync(ovhPath + caller + "/"))
			fs.mkdirSync(ovhPath + caller + "/")
	    var otp = OTP("", 8);
	    var agent = req.agent.replace("0033", "0")
	    /*var addIt = true;
	    for (var n = 0; n < resp.convOvh.length; n++)
	    {
		if (resp.convOvh[n].duration == req.duration &&
		    resp.convOvh[n].date.getTime() == new Date(req.callStart).getTime())
		    addIt = false;
	    }
	    if (addIt == true)
	    {*/
	    db.model('callovh').count({number: caller, 'convOvh.duration': req.duration, 'convOvh.date': {$eq: new Date(req.callStart)}}).then(function (count) {
		if (count == 0)
		{
		    exec("wget -O " + ovhPath + caller + "/" + otp + ".mp3 \"" + req.fileUrl + "\"", function (error, stdout, stderr) {
			db.model('callovh').findOne({number: caller}).then(function (resp) {
			    db.model('user').findOne({ligne: agent}).then(function (user) {
				if (user)
				    agent = user.login
				if (!error)
				{
				    if (resp === null)
				    {
					var defaultObj = {}
					defaultObj.number = caller
					defaultObj.convOvh = []
					defaultObj.convOvh.push({
					    name: otp,
					    duration: req.duration,
					    date: new Date(req.callStart),
					    redir: req.callerIdName,
					    agent: agent,
					})
					db.model('callovh')(defaultObj).save();
				    }
				    else
				    {
					resp.convOvh.push({
					    name: otp,
					    duration: req.duration,
					    date: new Date(req.callStart),
					    redir: req.callerIdName,
					    agent: agent,
					});
					resp.save();
				    }
				}
				resolve("ok");
			    })
			})
		    })
		}
	    })
	})
    }
    
    schema.statics.interStatus = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var query = {
		body: {
		    params: {
			type: req.query.type,
			dayStart: req.query.dayStart,
			account: req.query.account,
			line: req.query.line2,
			loadAll: true
		    }
		}
	    }
	    db.model('ovh').ovhAccount(query).then(function (resp3) {
		obj = resp3.logs;
		for (var n = 0; n < obj.length; n++)
		{
		    if (obj[n].duration === 0)
			obj.splice(n--, 1);
		}
		query.body.params.line = req.query.line
		db.model('ovh').ovhDemStats(query).then(function (resp2) {
		    var numbers = resp2.logs;
		    obj2 = {}
		    Object.keys(numbers).forEach(function (key, index) {
			Object.keys(numbers[key]).forEach(function (key2, index2) {
			    obj2[key2] = []
			    for (var n = 0; n < numbers[key][key2].length; n++)
			    {
				if (numbers[key][key2][n].duration > 0)
				    obj2[key2].push(numbers[key][key2][n])
			    }
			})
		    })
		    var ret = {count: {}, inter: {}, devis: {}}
		    if (req.query.type === 'single')
		    {
			var dayStart = new Date(req.query.dayStart)
			var dayEnd = new Date(req.query.dayStart)
			dayEnd.setDate(dayStart.getDate() + 1);
		    }
		    else
		    {
			var dayStart = new Date(moment(req.query.dayStart, 'DD-MM-YYYY').add(1, 'h'))
			var dayEnd = new Date(moment(req.query.dayStart, 'DD-MM-YYYY').add(1, 'h'))
			if (req.query.type === 'week')
			    dayEnd.setDate(dayStart.getDate() + 7);
			else
			    dayEnd.setMonth(dayStart.getMonth() + 1);
		    }
		    Object.keys(obj2).forEach(function (key, index) {
			ret.count[key] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			ret.inter[key] = {total: [], val: [], ann: [], prog: []}
			ret.devis[key] = {total: [], env: [], tran: []}
		    })
		    db.model('user').find({}).then(function (users) {
			async.eachLimit(users, 1, function (user, cb) {
			    if (user.login in obj)
			    {
				ret.count[user.login] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				ret.inter[user.login] = {total: [], val: [], ann: [], prog: []}
				ret.devis[user.login] = {total: [], env: [], tran: []}
				var tmp = user.login
				var checked = []
				async.eachLimit(obj[user.login], 1, function (tel, cb) {
				    //
				    db.model('intervention').find({'login.ajout': tmp, 'date.ajout': {$gte: dayStart, $lte: dayEnd}, $or : [{'client.telephone.tel1': tel.number}, {'client.telephone.tel2': tel.number}, {'client.telephone.tel3': tel.number}]},
								  {id: 1, prixAnnonce: 1, status: 1, devisOrigine: 1})
					.then(function (resp) {
					    var ikey = "";
					    Object.keys(obj2).forEach(function (key, index) {
						for (var n = 0; n < obj2[key].length; n++)
						{
						    if (obj2[key][n].number === tel.number &&
							obj2[key][n].date === tel.date)
							ikey = key;
						}
					    })
					    for (var n = 0; n < resp.length; n++)
					    {
						if (resp[n].devisOrigine || checked.indexOf(resp[n].id) !== -1)
						    resp.splice(n--, 1);
					    }
					    ret.count[tmp][0] += resp.length;
					    if (ikey != "")
						ret.count[ikey][0] += resp.length;
					    for (var n = 0; n < resp.length; n++)
					    {
						ret.inter[tmp].total.push(resp[n].id)
						checked.push(resp[n].id)
						ret.count[tmp][1] += resp[n].prixFinal ? resp[n].prixFinal : resp[n].prixAnnonce
						if (ikey != "")
						{
						    ret.count[ikey][1] += resp[n].prixFinal ? resp[n].prixFinal : resp[n].prixAnnonce
						    ret.inter[ikey].total.push(resp[n].id)
						}
						if (resp[n].status === 'VRF' || resp[n].status === 'AVR' ||
						    resp[n].status === 'ENC')
						{
						    ret.inter[tmp].val.push(resp[n].id)
						    ret.count[tmp][2] += 1
						    ret.count[tmp][3] += resp[n].prixFinal ? resp[n].prixFinal : resp[n].prixAnnonce
						    if (ikey != "")
						    {
							ret.inter[ikey].val.push(resp[n].id)
							ret.count[ikey][2] += 1
							ret.count[ikey][3] += resp[n].prixFinal ? resp[n].prixFinal : resp[n].prixAnnonce
						    }
						}
						else if (resp[n].status === 'ANN')
						{
						    ret.inter[tmp].ann.push(resp[n].id)
						    ret.count[tmp][4] += 1
						    ret.count[tmp][5] += resp[n].prixFinal ? resp[n].prixFinal : resp[n].prixAnnonce
						    if (ikey != "")
						    {
							ret.inter[ikey].ann.push(resp[n].id)
							ret.count[ikey][4] += 1
							ret.count[ikey][5] += resp[n].prixFinal ? resp[n].prixFinal : resp[n].prixAnnonce
						    }
						}
						else if (resp[n].status === 'APR')
						{
						    ret.inter[tmp].prog.push(resp[n].id)
						    ret.count[tmp][6] += 1
						    ret.count[tmp][7] += resp[n].prixFinal ? resp[n].prixFinal : resp[n].prixAnnonce
						    if (ikey != "")
						    {
							ret.inter[ikey].prog.push(resp[n].id)
							ret.count[ikey][6] += 1
							ret.count[ikey][7] += resp[n].prixFinal ? resp[n].prixFinal : resp[n].prixAnnonce
						    }
						}
					    }
					    db.model('devis').find({'login.ajout': tmp, 'date.ajout': {$gte: dayStart, $lte: dayEnd}, $or : [{'client.telephone.tel1': tel.number}, {'client.telephone.tel2': tel.number}, {'client.telephone.tel3': tel.number}]},
								   {id: 1, prixAnnonce: 1, status: 1, date: 1})
						.then(function (resp2) {
						    for (var n = 0; n < resp2.length; n++)
						    {
							if (checked.indexOf(resp2[n].id) !== -1)
							    resp2.splice(n--, 1);
						    }
						    for (var n = 0; n < resp2.length; n++)
						    {
							ret.devis[tmp].total.push(resp2[n].id)
							checked.push(resp2[n].id)
							ret.count[tmp][8] += 1
							ret.count[tmp][9] += resp2[n].prixAnnonce
							if (ikey != "")
							{
							    ret.count[ikey][8] += 1;
							    ret.count[ikey][9] += resp2[n].prixAnnonce
							}
							if (resp2[n].date.envoi)
							{
							    ret.devis[tmp].env.push(resp2[n].id)
							    ret.count[tmp][10] += 1
							    ret.count[tmp][11] += resp2[n].prixAnnonce
							    if (ikey != "")
							    {
								ret.devis[ikey].env.push(resp2[n].id)
								ret.count[ikey][10] += 1;
								ret.count[ikey][11] += resp2[n].prixAnnonce
							    }
							}
							if (resp2[n].status === 'TRA')
							{
							    ret.devis[tmp].tran.push(resp2[n].id)
							    ret.count[tmp][12] += 1
							    ret.count[tmp][13] += resp2[n].prixAnnonce
							    if (ikey != "")
							    {
								ret.devis[ikey].tran.push(resp2[n].id)
								ret.count[ikey][12] += 1;
								ret.count[ikey][13] += resp2[n].prixAnnonce
							    }
							}
						    }
						    cb(null);
						})
					})
				}, function (error) {
				    cb(null);
				})
			    }
			    else
				cb(null);
			}, function (err) {
			    if (err)
				reject(err)
			    else
				resolve(ret);
			})
		    })
		})
	    })
	})
    }
}
									       
