module.exports = function(schema) {
    var fs = require('fs');

    schema.statics.clearOldCall = function (req, res) {
	return new Promise(function (resolve, reject) {
	    var date = new Date();
	    date.setDate(date.getDate() - 30);
	    db.model('callovh').find({'convOvh.date': {$lte: date}}).then(function (resp) {
		for (var n = 0; n < resp.length; n++)
		{
		    for (var l = 0; l < resp[n].convOvh.length; l++)
		    {
			if (new Date(resp[n].convOvh[l].date).getTime() <= date.getTime())
			{
			    if (fs.existsSync(ovhPath + resp[n].number + "/" + resp[n].convOvh[l].name + ".mp3"))
				fs.unlink(ovhPath + resp[n].number + "/" + resp[n].convOvh[l].name + ".mp3", function (err) {
				})
			    resp[n].convOvh.splice(l--, 1)
			}
		    }
		    resp[n].save();
		}
	    })
	})
    }
}
