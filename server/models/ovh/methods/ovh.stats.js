module.exports = function(schema) {
    var moment = require('moment')
    
    schema.statics.ovhStats = function(req, res) {
        return new Promise(function(resolve, reject) {
	    var ret = {
		categories: [],
		series: [],
	    }
	    ret.title = "TOP 20 / "
	    if (req.query.group === 'day') {
                var date = new Date(req.query.year, req.query.month);
                var dayStart = new Date(date.getFullYear(), date.getMonth() - 1, 1, -1)
                var dayEnd = new Date(date.getFullYear(), date.getMonth(), 1)
		var limit = 31
		for (var n = 0; n < limit; n++)
		    ret.categories.push(n + 1);
		ret.title += "Jour"

            } else if (req.query.group == 'month') {
                var dayStart = new Date(req.query.year, 0, 1)
                var dayEnd = new Date(parseInt(req.query.year) + 1, 0, 1)
		var limit = 12
		for (var n = 0; n < limit; n++)
		    ret.categories.push(n + 1);
		ret.title += "Month"
            } else if (req.query.group == 'week') {
                var dayStart = new Date(req.query.year, 0, 1)
                var dayEnd = new Date(parseInt(req.query.year) + 1, 0, 1)
		var limit = 53
		for (var n = 0; n < limit; n++)
		    ret.categories.push(n + 1);
		ret.title += "Week"
            }

	    var end = dayEnd.getTime()
	    var place = []
	    var week = 0;

	    var recur = function () {
		if (dayStart.getTime() < end)
		{
		    db.model('ovh').findOne({year: dayStart.getFullYear(), month: dayStart.getMonth(), day: dayStart.getDate()}).then(function (ovh) {
			if (ovh !== null)
			{
			    Object.keys(ovh.history_sec.logs).forEach(function(key, index) {
				if (place.indexOf(key) === -1)
				{
				    var data = []
				    for (var n = 0; n < limit; n++)
					data.push(0);
				    ret.series.push({
					name: key,
					data: data
				    })
				    place.push(key);
				    if (req.query.group === 'day')
					ret.series[place.indexOf(key)].data[dayStart.getDate()] = ovh.history_sec.logs[key].length;
				    else if (req.query.group == 'month')
					ret.series[place.indexOf(key)].data[dayStart.getMonth()] += ovh.history_sec.logs[key].length;
				    else if (req.query.group == 'week')
					ret.series[place.indexOf(key)].data[week] += ovh.history_sec.logs[key].length
				}
				else
				{
				    if (req.query.group === 'day')
					ret.series[place.indexOf(key)].data[dayStart.getDate()] = ovh.history_sec.logs[key].length;
				    else if (req.query.group == 'month')
					ret.series[place.indexOf(key)].data[dayStart.getMonth()] += ovh.history_sec.logs[key].length;
				    else if (req.query.group == 'week')
					ret.series[place.indexOf(key)].data[week] += ovh.history_sec.logs[key].length
				}
			    })
			}
			dayStart.setDate(dayStart.getDate() + 1);
			if (dayStart.getDay() === 1)
			    week += 1;
			recur();
		    })
		}
		else
		    resolve(ret);
	    }
	    recur();
        })
    }
}
