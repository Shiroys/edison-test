module.exports = function(schema) {
    
    schema.statics.mailDaily = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var date = new Date()
	    date.setHours(1, 0, 0, 0);
	    date.setDate(new Date().getDate() - 1)
	    var dateStr = date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
	    var text = "Voici le rapport des appels du: " + dateStr
	    var option = {
		From: "contact@edison-services.fr",
		To: "contact@edison-services.fr",
		Subject: "Rapport du: " + dateStr,
		HtmlBody: text,
		Attachments: []
	    }
	    mail.send(option).then(resolve, reject);
	})
    }
}
