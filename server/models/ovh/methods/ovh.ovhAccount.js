module.exports = function(schema) {
    var async = require('async');
    var moment = require('moment');

    var service = require('ovh')({
		appKey: 'vlVsZ2QAN1zUg1R8',
		appSecret: 'CrCyuD9t263hDesjne21fCjujZKWriIh',
		consumerKey: 'jhcb1da0Y7XL4VLVQTaSQFGzTqQdHb1I'
    });

    var containsObject = function (array, number, date) {
	for (var n = 0; n < array.length; n++)
	{
	    if (array[n].number === number && array[n].date === date)
		return (true);
	}
	return (false);
    }

    var containsNumber = function (array, number) {
	for (var n = 0; n < array.length; n++)
	{
	    if (array[n].number === number)
		return (true);
	}
	return (false);
    }


    schema.statics.ovhDemStats = function(req, res) {
	return new Promise(function(resolve, reject) {
	    if (req.body.params.type === 'single')
	    {
		var dayStart = new Date(req.body.params.dayStart)
		var dayEnd = new Date(req.body.params.dayStart)
		dayEnd.setDate(dayStart.getDate() + 1);
	    }
	    else
	    {
		var dayStart = new Date(moment(req.body.params.dayStart, 'DD-MM-YYYY').add(1, 'h'))
		var dayEnd = new Date(moment(req.body.params.dayStart, 'DD-MM-YYYY').add(1, 'h'))
		if (req.body.params.type === 'week')
		    dayEnd.setDate(dayStart.getDate() + 7);
		else
		    dayEnd.setMonth(dayStart.getMonth() + 1);
	    }
	    var match = {
		year: dayStart.getFullYear(),
		day: dayStart.getDate(),
		month: dayStart.getMonth(),
	    }
	    match["history_sec.logs." + req.body.params.account + (req.body.params.line ? "." + req.body.params.line : '')] = {$exists: true}
	    var proj = {}
	    proj["history_sec.logs." + req.body.params.account + (req.body.params.line ? "." + req.body.params.line : '')] = 1
	    proj["history_sec.number." + req.body.params.account + (req.body.params.line ? "." + req.body.params.line : '')] = 1
	    if (req.body.params.type === 'single')
	    {
		var q = db.model('ovh').aggregate([{"$match": match}, {$project: proj}]);
		q.exec().then(function(ovh) {
		    if (ovh[0] && ovh[0].history_sec)
			resolve(ovh[0].history_sec);
		    else
			resolve({logs: {}, number: {}})
		})
	    }
	    else
	    {
		var ret = {logs: {}, number: {}}
		var end = dayEnd.getTime()
		var recur = function () {
		    if (dayStart.getTime() < end)
		    {
			var q = db.model('ovh').aggregate([{"$match": match}, {$project: proj}]);
			q.exec().then(function(ovh) {
			    ovh = ovh[0]
			    if (ovh && ovh.history_sec)
			    {
				Object.keys(ovh.history_sec.logs).forEach(function(key, index) {
				    if (key in ret.logs)
				    {
					Object.keys(ovh.history_sec.logs[key]).forEach(function (key2, index2) {
					    if (key2 in ret.logs[key])
						ret.logs[key][key2] = ret.logs[key][key2].concat(ovh.history_sec.logs[key][key2])
					    else
						ret.logs[key][key2] = ovh.history_sec.logs[key][key2];
					})
				    }
				    else
					ret.logs[key] = ovh.history_sec.logs[key]
				})
				Object.keys(ovh.history_sec.number).forEach(function(key, index) {
				    if (!(key in ret.number))
					ret.number[key] = ovh.history_sec.number[key]
				})
			    }
			    dayStart.setDate(dayStart.getDate() + 1);
			    match.year = dayStart.getFullYear()
			    match.day = dayStart.getDate()
			    match.month = dayStart.getMonth()
			    recur();
			})
		    }
		    else
			resolve(ret);
		}
		recur()
	    }

	})
    }			   
    
    schema.statics.ovhAccount = function(req, res) {
	return new Promise(function(resolve, reject) {
	    if (req.body.params.type === 'single')
	    {
		var dayStart = new Date(req.body.params.dayStart)
		var dayEnd = new Date(req.body.params.dayStart)
		dayEnd.setDate(dayStart.getDate() + 1);
	    }
	    else
	    {
		var dayStart = new Date(moment(req.body.params.dayStart, 'DD-MM-YYYY').add(1, 'h'))
		var dayEnd = new Date(moment(req.body.params.dayStart, 'DD-MM-YYYY').add(1, 'h'))
		if (req.body.params.type === 'week')
		    dayEnd.setDate(dayStart.getDate() + 7);
		else
		    dayEnd.setMonth(dayStart.getMonth() + 1);
	    }
	    var match = {
		year: dayStart.getFullYear(),
		day: dayStart.getDate(),
		month: dayStart.getMonth(),
	    }
	    var proj = {
		day : 1,
	    }
	    db.model('ovh').getPhone().then(function (ovh) {
		service.request('GET', '/telephony/' + req.body.params.account + '/line', function (err, service) {
		    if (req.body.params.loadAll)
		    {
			proj["history.logs"] = 1
			proj["history.number"] = 1			
		    }
		    else if (req.body.params.line)
		    {
			match["history.logs." + req.body.params.line] = {$exists: true}
			proj["history.logs." + req.body.params.line] = 1
			proj["history.number." + req.body.params.line] = 1
		    }
		    else
		    {
			for (var n = 0; n < service.length; n++)
			{
			    service[n] = service[n].replace("0033", "0")
			    if (ovh[service[n]] != undefined)
			    {
				//match["history.logs"].$in.push({ovh[service[n]].login: {$exists: true}})
				proj["history.logs." + ovh[service[n]]] = 1
				proj["history.number." + ovh[service[n]]] = 1
			    }
			}
		    }
		    if (req.body.params.type === 'single')
		    {
			var q = db.model('ovh').aggregate([{"$match": match}, {$project: proj}]);
			q.exec().then(function (ovh) {
			    if (ovh[0] && ovh[0].history)
				resolve(ovh[0].history);
			    else
				resolve({logs: {}, number: {}})
			})
		    }
		    else
		    {
			var ret = {logs: {}, number: {}}
			var end = dayEnd.getTime()
			var recur = function () {
			    if (dayStart.getTime() < end)
			    {
				var q = db.model('ovh').aggregate([{"$match": match}, {$project: proj}]);
				q.exec().then(function (ovh) {
				    ovh = ovh[0]
				    if (ovh && ovh.history)
				    {
					Object.keys(ovh.history.logs).forEach(function(key, index) {
					    if (key in ret.logs)
						ret.logs[key] = ret.logs[key].concat(ovh.history.logs[key])
					    else
						ret.logs[key] = ovh.history.logs[key]
					})
					Object.keys(ovh.history.number).forEach(function(key, index) {
					    if (!(key in ret.number))
						ret.number[key] = ovh.history.number[key]
					})

				    }
				    dayStart.setDate(dayStart.getDate() + 1);
				    match.year = dayStart.getFullYear()
				    match.day = dayStart.getDate()
				    match.month = dayStart.getMonth()
				    recur();
				})
			    }
			    else
			    {
				resolve(ret);
			    }
			}
			recur();
		    }
		})
	    })
	})
    }
}
