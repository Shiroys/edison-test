module.exports = function(db) {

    return new db.Schema({
	day: Number,
	month: Number,
	year: Number,
	history: {},
	history_sec: {},
	loaded_twelve: {
	    type: String,
	    default: false
	},
	loaded_sixteen: {
	    type: Boolean,
	    default: false
	},
	loaded_late: {
	    type: Boolean,
	    default: false,
	},
	loaded: {
	    type: Boolean,
	    default: false,
	}
    });
}
