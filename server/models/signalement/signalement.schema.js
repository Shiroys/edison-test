module.exports = function(db) {

    return new db.Schema({
        login: { ajout: String, done: String },
        date: { ajout: Date, done: Date },
        inter_id: { type: Number, ref: 'intervention' },
        sst_id: { type: Number, ref: 'artisan' },
        addBy: { type: String },
        verifBy: { type: String },
		ajoutI : Boolean,
		envoiI : Boolean,
		verifI : Boolean,
		annulI : Boolean,
		ajoutA : Boolean,
		commerc : Boolean,
		partena : Boolean,
		compta : Boolean,
		recouv : Boolean,
		isStar: Boolean,
        sst_nom: String,
        text: String,
		raison: String,
		arrayDest: Array,
        _type: String,
		typeSig: String,
        subType: String,
        level: String,
        service: String,
        nom: String,
        ok: {
            type: Boolean,
            default: false
        }
    });
}
