module.exports = function(schema) {
/* Fonction de transformation de signalement */
    schema.statics.transfo = function(req, res) {
        return new Promise(function(resolve, reject) {
			db.model('signalement').find({typeSig: { $not: { $eq: "interneT"} }, typeSig: { $not: { $eq: "artisanT"} }}).cursor()
			.on('error', function(err) {
				console.log("--->Error: ", err);
				reject(err);
			})
			.on('data', function(elem) {

				elem.typeSig = req.body.typeSig;
				elem.save().then(function(resp) {});
			})
			.on('end', function()  {
				resolve("Ok");
			})
		})
    }
}