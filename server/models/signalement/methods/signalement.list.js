module.exports = function(schema) {

    schema.statics.list = function(req, res) {
        return new Promise(function(resolve, reject) {
			var start = new Date(req.query.selectedYear, req.query.selectedMonth, 1);
			db.model('user').findOne({login: req.query.selectedUser}).then(function(resp) {
				if (req.query.selectedMonth == 11)
					var end = new Date(parseInt(req.query.selectedYear) + 1, 1, 1);
				else
					var end = new Date(req.query.selectedYear, parseInt(req.query.selectedMonth) + 1, 1);
				if (req.query.type == 0) {
					if (req.query.selectedUser == 'all') {
						db.model('signalement').find({ "date.ajout": { $gte: start, $lt: end }}).then(resolve, reject);
					} else {
						db.model('signalement').find({ "login.ajout": req.query.selectedUser, "date.ajout": { $gte: start, $lt: end }}).then(resolve, reject);
					}
				} else if (req.query.type == 1) {
					if (req.query.selectedUser == 'all') {
						db.model('signalement').find({ ok: false }).then(resolve, reject);
					} else {
						db.model('signalement').find({ arrayDest: { $in: [resp.email] }, ok: false }).then(resolve, reject);
					}
				} else if (req.query.type == 2) {
					if (req.query.selectedUser == 'all') {
						db.model('signalement').find({ "date.ajout": { $gte: start, $lt: end }, ok: true }).then(resolve, reject);
					} else {
						db.model('signalement').find({ arrayDest: { $in: [resp.email] }, "date.ajout": { $gte: start, $lt: end }, ok: true }).then(resolve, reject);
					}
				} else if (req.query.type == 3) {
					db.model('signalement').find({ service: req.query.selectedService , "date.ajout": { $gte: start, $lt: end }}).then(resolve, reject);
				} else {
					db.model('signalement').find({}).then(resolve, reject);
				}
			})
        })
    }
}
