module.exports = function(schema) {

	schema.statics.resolve = function(req, res) {
		return new Promise (function(resolve, reject) {
			db.model('signalement').findOne({ _id: req.body._id, inter_id: req.body.inter_id }).then(function(resp) {
				resp.ok = true;
				resp.text = req.body.text;
				resp.login.done = req.session.login;
				resp.date.done = new Date();
				resp.save().then(function(resp) {
					resolve(resp);
				}, reject);
			});
		})
	}
}
