module.exports = function(schema) {
    var fs = require('fs');
    
    schema.statics.getConv = {
	unique: true,
	findBefore: false,
	method: 'GET',
	fn: function (ovh, req, res) {
		    var path = ovhPath + ovh + "/" + req.query.name + ".mp3"
		    if (res && fs.existsSync(path))
		    {
				return fs.readFile(path, function (err, file) {
			    	res.setHeader('Content-disposition', 'attachment; filename=' + req.query.name + '.mp3');
			    	return res.contentType("audio/mpeg").sendFile(path);
				})
		    }
		    else
				return res && res.status(404).send('not found');
		}
    }
}
