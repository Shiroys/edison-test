module.exports = function(db) {

    return new db.Schema({
	number: String,
	convOvh: [{
	    name: String,
	    duration: Number,
	    date: Date,
	    redir: String,
	    agent: String,
	}]
    });
}
