module.exports = function(db) {

    return new db.Schema({
	_id: Number,
	id: Number,
	nom: String,
	categorie: String,
	address: {
	    r: String,
	    v: String,
	    cp: String,
	    lt: Number,
	    lg: Number,
	    latLng: String,
	},
	loc: {
	    type: [Number],
	    index: '2dsphere'
	},
	telephone: String,
    });
}
