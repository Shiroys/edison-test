module.exports = function(schema) {
    var key = { key : "AIzaSyAoI5BIg91uwnLdp4pfOijlF_ZPjn-QBRY"};
    var geocoder = require('geocoder');
    var async = require('async');


    var location = function(req, pro, cb) {
	var address = pro.address.r + " " + pro.address.cp + " " + pro.address.v;
	geocoder.geocode(address, function(err, data) {
	    if (!err && !data.error_messsage && data.results[0]) {
		var obj = data.results[0].geometry.location;
		pro.address.lt = obj.lat;
		pro.address.lg = obj.lng;
		pro.address.latLng = obj.lat + ", " + obj.lng;
		pro.loc = [obj.lat, obj.lng];
		pro.save();
	    }
	    setTimeout(function () {
		cb(null);
	    }, 1000);
	}, key)
    }	

    var geo = function (req, res, resolve, reject, elem) {
	async.eachLimit(elem, 20, function(pro, cb) {
	    location(req, pro, cb)
	}, function (err) {
	    if (!err)
		reject(err);
	    else
		resolve("ok");
	})
    }

    schema.statics.geoloc = function(req, res) {
	return new Promise(function(resolve, reject)  {
	    db.model('marker').find({'address.lt' : {$exists: false}}).then( function (resp) {
		geo(req, res, resolve, reject, resp);
	    });
	    resolve("ok");
	})
    }
}
