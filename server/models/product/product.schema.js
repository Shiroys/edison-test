module.exports = function(db) {

    return new db.Schema({
        categorie: String,
	scategorie: String,
        pu: {
            type: Number,
            required: true
        },
	pa: Number,
	marge: Number,
	link: String,
        title: {
            type: String,
            required: true
        },
        desc: {
            type: String,
            required: true
        },
        ref: {
            type: String,
            required: true
        }
    });

}
