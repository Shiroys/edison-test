module.exports = function(db) {

    return new db.Schema({
        date: String,
        id: {
            type: Date,
        default: Date.now,
        },
        login: String,
        cate: Number,
        ad: String,
        status: Boolean,
	demarchepar: String,
	demarchedate: String,
	validepar: String,
	validedate: String,
	foundOne: {
	    type: Boolean,
	    default: false,
	},
	address: {
	    lt: Number,
	    lg: Number,
	}
    });
}
