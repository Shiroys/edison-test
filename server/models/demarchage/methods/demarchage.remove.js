module.exports = function(schema) {
    schema.statics.remove = function(req, res) {
        return new Promise(function(resolve, reject)  {
            var id = req.body.id;
            db.model('demarchage').findOne({_id: id}, function (err, model) {
                if (err)
                    console.log("Error demarchage.remove: ",err);
                else
                {
                    model.remove(function (err) {
                        console.log(err);
                    }).then(resolve, reject);
                }
            });
        })
    }
}