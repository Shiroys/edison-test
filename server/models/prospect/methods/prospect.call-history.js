module.exports = function(schema) {
    var _ = require('lodash')
    var normalize = function(type, dateKey) {
        return function(e) {
            e.type = type;
            e._date = new Date(_.get(e, dateKey ||  'date'));
        }
    }

    schema.statics.callHistory = {
        unique: true,
        findBefore: true,
        method: 'GET',
        fn: function(prospect, req, res) {
            return new Promise(function(resolve, reject) {
                var calls = _.each(prospect.toObject().calls, normalize('calls'));

		var rtn = [];
                rtn = rtn.concat(calls);
                rtn = _.sortBy(rtn, '_date').reverse()
                resolve(rtn)
            })
        }
    }
}
