module.exports = function(schema) {
    var key = { key : "AIzaSyAoI5BIg91uwnLdp4pfOijlF_ZPjn-QBRY"};
    var geocoder = require('geocoder');
    var async = require('async');


    var location = function(req, pro, cb) {
		var address = ""
		if (pro.address.r)
		    address = pro.address.r + " " + pro.address.cp + " " + pro.address.v;
		else
		    address = pro.address.cp + " " + pro.address.v;
		geocoder.geocode(address, function(err, data) {
		    if (!err && !data.error_messsage && data.results[0]) {
			var obj = data.results[0].geometry.location;
			pro.address.lt = obj.lat;
			pro.address.lg = obj.lng;
			pro.address.latLng = obj.lat + ", " + obj.lng;
			pro.loc = [obj.lat, obj.lng];
			var doc = db.model('prospect')(pro);
			var preSave = function(pro, session, callback) {
			    return callback(null, pro);
			}
			preSave(pro, req.session, function(err, resp) {
			    if (err) {
				return (err);
			    }
			    doc.save(function(err, resp) {
				if (err) {
				    return (err);
				}
			    });
			});
		    }
		    setTimeout(function () {
			cb(null);
		    }, 1000);
		}, key)
    }	

    var geo = function (req, res, resolve, reject, elem, nextID) {
	if (nextID != 0)
	{
	    for (var n = 0; n < elem.length; n++)
	    {
		elem[n].id = nextID;
		elem[n]._id = nextID++;
	    }
	}
	
	async.eachLimit(elem, 20, function(pro, cb) {
	    var query = [];
	    if (pro.telephone.tel1)
	    {
		query.push({'telephone.tel1': pro.telephone.tel1})
		query.push({'telephone.tel2': pro.telephone.tel1})
	    }
	    if (pro.telephone.tel2)
	    {
		query.push({'telephone.tel1': pro.telephone.tel2})
		query.push({'telephone.tel2': pro.telephone.tel2})
	    }
	    if (pro.telephone.tel1 || pro.telephone.tel2)
	    {
		db.model('prospect').count({$or: query}).then(function (cnt) {
		    if (cnt === 0)
			location(req, pro, cb)
		    else
			cb(null);
		});
	    }
	    else
		location(req, pro, cb)
	}, function (err) {
	    if (!err)
		reject(err);
	    else
		resolve("ok");
	})
    }

    schema.statics.geolocNotBDD = function (req, res) {
	return new Promise(function(resolve, reject)  {
	    var elems = JSON.parse(req.body.params.q).list
	    db.model('prospect').getNextID(elems[0], function(nextID) {		
		geo(req, res, resolve, reject, elems, nextID);
	    })
	    resolve("ok");
	})
    }

    schema.statics.geoloc = function(req, res) {
	return new Promise(function(resolve, reject)  {
	    db.model('prospect').find({'address.lt' : {$exists: false}}).then( function (resp) {
		geo(req, res, resolve, reject, resp, 0);
	    });
	    resolve("ok");
	})
    }
}
