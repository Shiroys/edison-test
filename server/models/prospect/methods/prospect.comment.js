module.exports = function(schema) {

    schema.statics.comment = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(prospect, req, res) {
            return new Promise(function(resolve, reject) {
                prospect.comments.push({
                    text: req.body.text,
                    login: req.session.login,
                    date: Date.now()
                })
                prospect.save().then(resolve, reject)
            })
        }
    }
}
