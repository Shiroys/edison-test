module.exports = function(schema) {

    schema.statics.checkCall = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(prospect, req, res) {
            return new Promise(function(resolve, reject) {
                db.model('prospect').update({'calls._id': req.body.sst},
                                            {$set: {'calls.$.text': req.body.text,
                                                    'calls.$.ok': req.body.state}}
                                           ).then(function (e) {
					       prospect.save().then(resolve, reject);
					   }, reject);
            })
        }
    }
}
