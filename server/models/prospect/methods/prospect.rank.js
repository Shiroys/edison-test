module.exports = function(schema) {

    var _ = require('lodash')

    var __filter = function(doc, i) {
	var d = doc.obj;
	return d.status !== 'ARC' && (!this.categorie || d.categories.indexOf(this.categorie) !== -1);
    }
    
    var __map = function(doc, i) {
	var d = doc.obj;
	return {
	    id: d.id,
	    nomSociete: d.nomSociete,
	    distance: doc.dis.round(1),
	    categories: d.categories,
	    zoneChalandise: d.zoneChalandise,
	    statutProspect: d.statutProspect,
	    address: {
		lt: d.address.lt,
		lg: d.address.lg,
	    },
	}
    }


    schema.statics.rankProspects = function(options) {
	var self = this;
	if (!isWorker) {
	    return edison.worker.createJob({
		name: 'db',
		model: 'prospect',
		priority: 'high',
		method: 'rankProspects',
		req: options
	    })
	}
	return new Promise(function(resolve, reject) {
	    var _ = require('lodash')

	    var query = {}

	    if (options.categorie) {
		query.categories = {
		    $in: [options.categorie]
		}
	    }

	    db.model('prospect').geoNear({
		type: "Point",
		coordinates: [parseFloat(options.lat), parseFloat(options.lng)]
	    }, {
		spherical: true,
		query: query,
		limit: options.categorie ? 300 : 25,
		distanceMultiplier: 0.001,
		maxDistance: (parseFloat(options.maxDistance) || 100) / 0.001
	    }).then(function(docs) {
		try {
		    resolve(_.map(docs, __map));
		} catch (e) {	
		    __catch(e);
		}
	    }, function (err) {
		console.log('-->', err)
	    })
	})
    }
    schema.statics.rank = function(req, res) {

	return this.rankProspects(req.query)
    }

}
