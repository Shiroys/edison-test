module.exports = function(schema) {

    schema.statics.callLog = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(prospect, req, res) {
            return new Promise(function(resolve, reject) {
                prospect.calls.push({
                    login: req.session.login,
                    date: Date.now(),
                    text: "",
                    id_artisan: prospect.id
                })
                prospect.save().then(function (resp) {
		    resolve(prospect.calls);
		}, reject)
            })
        }
    }
}
