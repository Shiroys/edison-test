module.exports = function(schema) {

    schema.statics.getNextID = function(data, cb) {
        db.model('prospect').findOne({}).sort("-id")
            .exec(function(err, latestDoc) {
		if (!latestDoc)
		    cb(1);
		else
                    cb(latestDoc.id + 1);
            })
    }
}
