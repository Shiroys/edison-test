module.exports = function(schema) {

    schema.statics.removePro = {
	   unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(prospect, req, res) {
            return new Promise(function(resolve, reject) {
                prospect.remove(function (err) {
                    console.log("Error prospect.removePro: ",err);
		        }).then(resolve, reject);
            })
        }
    }
}
