    var _ = require('lodash');
    module.exports = {
        name: 'prospect',
        Name: 'Prospect',
        NAME: 'PROSPECT',
        redisCacheListName: 'PROSPECT_CACHE_LIST',
        listChange: 'PROSPECT_CACHE_LIST_CHANGE'
    }

    module.exports.redisTemporarySaving = function(id) {
        return 'PROSPECT_TMP_SAVING___' + id;
    }
    module.exports.model = function() {
        return db.model('prospect');
    }
