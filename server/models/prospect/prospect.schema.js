module.exports = function(db) {

    return new db.Schema({
	_id: Number,
	id: {
	    type: Number,
	    index: true
	},
	nomSociete: String,
	formeJuridique: String,
	representant: {
	    civilite: String,
	    nom: String,
	    prenom: String,
	},
	address: {
	    n: String,
	    r: String,
	    v: String,
	    cp: String,
	    lt: Number,
	    lg: Number,
	},
	statutProspect: {
	    short_name: String,
	    long_name: String,
	    color: String,
	},
	pourcentage: {
	    deplacement: Number,
	    maindOeuvre: Number,
	    fourniture: Number,
	},
	loc: {
	    type: [Number],
	    index: '2dsphere'
	},
	zoneChalandise: Number,
	categories: [],
	email: String,
	telephone: {
	    tel1: String,
	    tel2: String
	},
	calls: [{
	    login: String,
	    id_artisan: Number,
	    date: {
		type: Date,
	    default: Date.now
	    },
	    text: {
		type: String,
	    default: ""
	    },
	    ok: {
		type: Boolean,
	    default: false
	    },
	}],
	infoSup: {
	    Multi: String,
	    Site1: String,
	    Site2: String,
	    Site3: String,
	},
	comments: [{
	    login: String,
	    text: String,
	    date: Date
        }],
	newOs: {
	    type: Boolean,
	    default: true
	},
	siret: String,
	IBAN: String,
	BIC: String,
	cache: {}
    });
}
