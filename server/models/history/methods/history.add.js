module.exports = function(schema) {

    schema.statics.add = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var defaultHist = {}
	    db.model('history').count({}).then(function (respC) {
		defaultHist.id = respC + 1;
		defaultHist._id = respC + 1;
		defaultHist.login = req.body.login
		defaultHist.name = req.body.name;
		defaultHist.date = new Date();
		db.model('history')(defaultHist).save().then(resolve, reject);
	    })
	})
    }
}
