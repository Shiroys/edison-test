module.exports = function(db) {

    return new db.Schema({
	_id: Number,
	id: {
	    type: Number,
	    index: true
	},
	login: String,
	name: String,
	date: Date,
    });
}
