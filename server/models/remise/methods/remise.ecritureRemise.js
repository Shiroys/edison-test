module.exports = function(schema) {
	var moment = require('moment');

	schema.statics.ecritureRemise = function(req, res) {
		return new Promise(function(resolve, reject) {
			console.time("End ecriture remise");
			db.model('remise').findOne({ dateRemise: req.query.d }).exec(function(err, remise) {
				if (err) {
					reject('Error find ecriture remise: ', err);
				}
				var rtn = [];
				if (remise.status == 'enc') {
					var dateRemise = moment(new Date()).format('L');
				} else {
					var dateRemise = moment(new Date(remise.dateRemise)).format('L');
				}
				var total = 0;
				for (var ind = 0; ind < remise.cheques.length; ind++) {
					if (remise.cheques[ind].tva == 10) {
						rtn.push(['BQ', dateRemise, '41101000',
							'CLT' + ((parseInt(remise.cheques[ind].os) < 100000 ) ? '0' + remise.cheques[ind].os : remise.cheques[ind].os),
							remise.cheques[ind].os, 'CHQ CLT n° ' + remise.cheques[ind].num + ' os ' + remise.cheques[ind].os,
							remise.cheques[ind].montant, '']);
						rtn.push(['BQ', dateRemise, '44571010', '',
							remise.cheques[ind].os, 'CHQ CLT n° ' + remise.cheques[ind].num + ' os ' + remise.cheques[ind].os,
							'', (parseFloat(remise.cheques[ind].montant) / 1.1 * 0.1).toFixed(2)]);
						rtn.push(['BQ', dateRemise, '44587010','',
							remise.cheques[ind].os, 'CHQ CLT n° ' + remise.cheques[ind].num + ' os ' + remise.cheques[ind].os,
							(parseFloat(remise.cheques[ind].montant) / 1.1 * 0.1).toFixed(2), '']);
					} else if (remise.cheques[ind].tva == 20) {
						rtn.push(['BQ', dateRemise, '41102000',
							'CLT' + ((parseInt(remise.cheques[ind].os) < 100000 ) ? '0' + remise.cheques[ind].os : remise.cheques[ind].os),
							remise.cheques[ind].os, 'CHQ CLT n° ' + remise.cheques[ind].num + ' os ' + remise.cheques[ind].os,
							remise.cheques[ind].montant, '']);
						rtn.push(['BQ', dateRemise, '44571020', '',
							remise.cheques[ind].os, 'CHQ CLT n° ' + remise.cheques[ind].num + ' os ' + remise.cheques[ind].os,
							'', (parseFloat(remise.cheques[ind].montant) / 1.2 * 0.2).toFixed(2)]);
						rtn.push(['BQ', dateRemise, '44587020', '',
							remise.cheques[ind].os, 'CHQ CLT n° ' + remise.cheques[ind].num + ' os ' + remise.cheques[ind].os,
							(parseFloat(remise.cheques[ind].montant) / 1.2 * 0.2).toFixed(2), '']);
					} else {
						rtn.push(['BQ', dateRemise, '41100000',
							'CLT' + ((parseInt(remise.cheques[ind].os) < 100000 ) ? '0' + remise.cheques[ind].os : remise.cheques[ind].os),
							remise.cheques[ind].os, 'CHQ CLT n° ' + remise.cheques[ind].num + ' os ' + remise.cheques[ind].os,
							remise.cheques[ind].montant, '']);
					}
					if (remise.cheques[ind].montant) {
						total += remise.cheques[ind].montant;
					}
					if ((ind % 50 == 0 && ind > 0) || ind + 1 == remise.cheques.length) {
						rtn.push(['BQ', dateRemise, '51210000', '', '', 'Remise de chèques', total, '']);
						rtn.push(['', '', '', '', '', '', '', '']);
						total = 0;
					}
				}
				console.timeEnd("End ecriture remise");
				if (err)
					reject(err);
				if (req.query.download) {
					res.sage(rtn)
				} else {
					res.table(rtn)
				}
			});
		})
	}
}