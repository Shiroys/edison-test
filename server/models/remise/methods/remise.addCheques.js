module.exports = function(schema) {
    var async = require('async');
    var moment = require('moment');

    schema.statics.addCheques = function(req, res) {
        return new Promise(function(resolve, reject) {
            console.time("End add cheques");
            db.model('remise').findOne({ status: 'enc' }).exec(function(err, encRemise) {
                db.model('remise').find({ status: 'val', dateRemise: { $exists: true }}).sort({ dateRemise: -1}).limit(1).exec(function(err, lastVal) {
                    if (err) {
                        console.log("Error find add cheque", err);
                        reject(err);
                    } else {
                        var dateNow = new Date();
                        var lastRem = lastVal[0] ? new Date(lastVal[0].dateRemise).setHours(0, 0, 0, 0) : new Date(2017,01,01).setHours(0, 0, 0, 0);
                        db.model('intervention').aggregate([
                            {
                                $match: {
                                    "compta.encaissement.mode": 'cheque',
                                    "compta.encaissement.dateEncaissement": { $gte: new Date(lastRem), $lte: new Date(dateNow) },
                                    "compta.encaissement.end": false
                                }
                            },{
                                $project: {
                                    "_id": 0,
                                    "id": 1,
                                    "compta.encaissement": 1
                                }
                            },{
                                $unwind: "$compta.encaissement",
                            },{
                                $sort: { "compta.encaissement.agent": 1 }
                            }
                            ]).exec(function(err, cheques) {
                                console.log("CHEQUES: ", cheques);
                            if (err) {
                                console.log("Error find cheques: ", err);
                                reject(err);
                            } else {
                                if (encRemise.cheques.length > 0)
                                    encRemise.cheques = [];
                                for (var ind = 0; ind < cheques.length; ind++) {
                                    if (new Date(cheques[ind].compta.encaissement.dateEncaissement) >= new Date(lastRem)
                                        && new Date(cheques[ind].compta.encaissement.dateEncaissement) <= new Date(dateNow)
                                        && cheques[ind].compta.encaissement.end === false) {
                                        if (cheques[ind].compta.encaissement.mult === true) {
                                            for (var index = ind; index < cheques.length; index++) {
                                                if (cheques[ind].compta.encaissement.num == cheques[index].compta.encaissement.num
                                                    && cheques[ind].compta.encaissement.client == cheques[index].compta.encaissement.client
                                                    && cheques[index].compta.encaissement.num === true) {
                                                    cheques[ind].compta.encaissement.montant += cheques[index].compta.encaissement.montant;
                                                    cheques.splice(index, 1);
                                                }
                                            }
                                        }
                                        encRemise.cheques.push({
                                            os: cheques[ind].id,
                                            num: cheques[ind].compta.encaissement.num,
                                            dateEncaissement: cheques[ind].compta.encaissement.dateEncaissement,
                                            montant: cheques[ind].compta.encaissement.montant,
                                            agent: cheques[ind].compta.encaissement.agent,
                                            client: cheques[ind].compta.encaissement.client.substring(0, 13),
                                            tva: cheques[ind].compta.encaissement.tva
                                        });
                                    }
                                }
                                encRemise.save(function(err, resp) {
                                    if (err) {
                                        console.log("Error save encRemise: ", err);
                                        reject(err);
                                    } else {
                                        console.timeEnd("End add cheques");
                                        resolve("OK");
                                    }
                                })
                            }
                        })
                    }
                })  
            })
        })
    }
}