module.exports = function(schema) {
    var htmlpdf = require('html-pdf-wth-rendering');
    var _ = require('lodash');
    var fs = require('fs');

    var lessThan50 = function(remise) {
        var tab = {};
        tab.tab1 = [];
        tab.tab2 = [];
        tab.total = 0;
        for (var ind = 0; ind < remise.cheques.length; ind++) {
            if (ind < 25) {
                tab.tab1.push(remise.cheques[ind]);
            } else {
                tab.tab2.push(remise.cheques[ind]);
            }
        }
        for (var ind = 0; ind < remise.cheques.length; ind++) {
            tab.total += remise.cheques[ind].montant;
        }
        tab.nbrCheques = remise.cheques.length;
        return (tab);
    }

    var moreThan50 = function(remise, index) {
        var tab = {};
        tab.tab1 = [];
        tab.tab2 = [];
        tab.total = 0;
        for (var ind = 0; (ind + (index * 50)) < remise.cheques.length && ind < 50; ind++) {
            if (ind < 25) {
                tab.tab1.push(remise.cheques[ind + (index * 50)]);
            } else {
                tab.tab2.push(remise.cheques[ind + (index * 50)]);
            }
        }
        tab.nbrCheques = ind;
        for (var ind = 0; (ind + (index * 50)) < remise.cheques.length && ind < 50; ind++) {
            tab.total += remise.cheques[ind + (index * 50)].montant;
        }
        return (tab);
    }

    schema.statics.printRemise = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('remise').findOne({ dateRemise: req.query.d }).exec(function(err, remise) {
                if (err) {
                    console.log("Error find printRemise: ", err);
                    reject(err);
                } else {
                    var tab = [];
                    var total = 0;
                    var nbrCheques = remise.cheques.length;
                    var nbrRemises = 0;
                    var calcTab = 0;
                    if (nbrCheques <= 50) {
                        tab.push(lessThan50(remise));
                        calcTab = 1;
                    } else {
                        if ((nbrCheques % 50) != 0) {
                            calcTab = Math.ceil(nbrCheques / 50);
                        } else {
                            calcTab = (nbrCheques / 50);
                        }
                        for (var ind = 0; ind < calcTab; ind++) {
                            tab.push(moreThan50(remise, ind));
                        }
                    }
                    try {
                    var html = fs.readFileSync(process.cwd() + '/pdf-mail/template/remiseCheque.html', 'utf8');
                    html = _.template(html)({ cheques: tab, nbrRemises: calcTab });
                    htmlpdf.create(html, { format: 'A4' }).toBuffer(function(err, buffer) {
                        if (err) {
                            console.log("Error pdf remise cheque: ", err);
                            reject(err);
                        } else {
                            res.contentType('application/pdf');
                            res.send(buffer);
                        }
                    })
                    } catch (e) {
                        console.log("ERROR: ", e);
                    }
                }
            })
        })
    }
}