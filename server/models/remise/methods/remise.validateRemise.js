module.exports = function(schema) {

    schema.statics.validateRemise = function(req, res) {
        return new Promise(function(resolve, reject) {
            console.time("End validate remise");
            db.model('remise').findOne({ status: 'enc' }).exec(function(err, encRemise) {
                if (err) {
                    console.log('Error find validateremise: ', err);
                    reject(err);
                }
                encRemise.status = 'val';
                encRemise.dateRemise = new Date();
                db.model('remise').create({ status: 'enc' }).then(function() {});
                encRemise.save(function(err) {
                    if (err) {
                        console.log('Error save validate remise: ', err);
                        reject(err);
                    } else {
                        var tab = [];
                        var tab2 = [];
                        for (var ind = 0; ind < encRemise.cheques.length; ind++) {
                            tab.push(encRemise.cheques[ind].num);
                            tab2.push(encRemise.cheques[ind].montant);
                        }
                        db.model('intervention').update({ $and: [{ "compta.encaissement.num": { $in: tab }}, { "compta.encaissement.montant": { $in: tab2 }}]},
                        { $set: { "compta.encaissement.$.end": true }}, { multi: true }).exec(function(err) {
                            if (err) {
                                console.log("Error update validateremise: ", err);
                                reject(err);
                            } else {
                                console.time("End validate remise");
                                resolve('OK');
                            }
                        })
                    }
                })
            })
        })
    }
}