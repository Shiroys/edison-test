module.exports = function(schema) {

    schema.statics.listRemises = function(req, res) {
        return new Promise(function(resolve, reject) {
            db.model('remise').findOne({ status: 'enc' }).exec(function(err, encRemise) {
                db.model('remise').find({ status: 'val', dateRemise: { $exists: true }}).sort({ dateRemise: -1}).lean().exec(function(err, list) {
                    if (err) {
                        console.log("Error find listRemise", err);
                        reject(err);
                    } else {
                        resolve({ list: list, enc: encRemise });
                    }
                })  
            })
        })
    }
}