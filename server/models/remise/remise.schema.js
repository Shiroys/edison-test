module.exports = function(db) {

    return new db.Schema({
        dateRemise: { type: Date },
        cheques: [{
        	os: { type: Number },
        	num: { type: String },
			dateEncaissement: { type: Date },
			montant: { type: Number },
            client: { type: String },
			agent: { type: String },
			tva: { type: String }
        }],
        status: { type: String }
    })
}