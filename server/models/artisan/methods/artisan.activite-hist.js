module.exports = function(schema) {
    var _ = require('lodash')
    schema.statics.ActiviteHistory = {
	unique: true,
	findBefore: true,
	method: 'GET',
	fn: function(artisan, req, res) {
	    return new Promise(function(resolve, reject) {
		db.model('artisan').find({
		    sst_id: artisan.id
		}).lean().then(function(resp) {
		    var comments = _.each(artisan.toObject().comments);
		    comments = _.sortBy(comments, 'date').reverse()
		    resolve(comments)
		})
	    })
	}
    }
}
