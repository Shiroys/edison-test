module.exports = function(schema) {

    schema.statics.checkExist = function(req, res) {
	return new Promise(function(resolve, reject) {
	    db.model('artisan').findOne({ $or: [{ email: req.body.email }, { "telephone.tel1": req.body.tel }] }).exec(function(err, checkArtisan) {
	    	if (err)
	    	{
	    		console.log("Erreur check artisan: ", err);
	    		reject(err);
	    	}
	    	else if (checkArtisan === null)
	    		resolve("YES");
	    	else
	    		resolve("NO");
	    });
	})
    }
}
