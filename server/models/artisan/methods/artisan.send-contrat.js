module.exports = function(schema) {
    var PDF = requireLocal('pdf-mail')
    var _ = require('lodash')

    schema.statics.sendContrat = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(artisan, req, res) {

            return new Promise(function(resolve, reject) {
                if (!isWorker) {
                    return edison.worker.createJob({
                        name: 'db_id',
                        model: 'artisan',
                        method: 'sendContrat',
                        data: artisan,
                        req: _.pick(req, 'query', 'session', 'body')
                    }).then(function(resp) {
                        artisan.historique.contrat.push({
                            login: req.session.login,
                            signe: req.body.signe,
                            date: Date.now(),
                            messageID: resp.MessageID,
                        })
                        artisan.save().then(_.partial(resolve, resp), reject);
                    })
                }
                try {
                    artisan.signe = req.body.rappel || req.body.signe;
                    var communication = {
                        mailDest: envProd ? artisan.email : (req.session.email ||  'intervention@edison-services.fr'),
                        mailReply: 'yohann.rhoum@edison-services.fr'
                    }
                    var template = req.body.rappel > 0 ? 'relanceDocuments' : 'envoiContrat';
                    var attachments = [];
                    var html = require('fs').readFileSync(process.cwd() + '/templates/' + template + '.html', 'utf8')
                    html = _.template(html)(artisan);
                    PDF('contract', artisan).buffer(function(err, buffer) {
                        if (!artisan.document.contrat.ok) {
                            attachments.push({
                                Content: buffer.toString('base64'),
                                Name: 'Declaration de sous-traitance.pdf',
                                ContentType: 'application/pdf'
                            })
                        }
                        mail.send({
                            From: "yohann.rhoum@edison-services.fr",
                            ReplyTo: communication.mailReply,
                            To: communication.mailDest,
                            Subject: req.body.rappel ? "En attente de vos documents" : "Proposition de partenariat",
                            HtmlBody: html,
                            Attachments: attachments
                        }).then(resolve, reject)
                    })
                } catch (e) {
                    console.log("Error artisan.send-contrat: ", e);
                }
            })

        }
    }
}
