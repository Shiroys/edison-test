module.exports = function(schema) {
    var moment = require('moment');

    schema.statics.ptStats = function(req, res) {
	return new Promise(function(resolve, reject) {
	    var ret = {}
	    for (var n = 0; n < req.query.array.length; n++)
		ret[req.query.array[n]] = {contact: 0, ajout: 0, actif: 0, complets: 0, archive: 0}
	    var start = new Date(req.query.start);
	    var end = new Date(req.query.end);
        db.model('prospect').find({"calls.date": {$gt: start, $lt: end}, "calls.login": {$in: req.query.array}}, {"calls": 1}).lean().exec(function (err, resp) {
		var ch = {}
			if (err)
			{
				console.log("Erreur ptStats dashboard: ", err);
				reject(err);
			}
			for (var n = 0; n < resp.length; n++)
			{
				var check= []
				var test = 0;
				for (var l = 0; l < resp[n].calls.length; l++)
				{
					if (resp[n].calls[l].date > start && resp[n].calls[l].date < end)
					{
						if (test === 0)
						{
							check.push(resp[n].calls[l].login)
							ret[resp[n].calls[l].login].contact++;
							test++;
						}
							else
							{
								for (var t = 0; t < check.length;t++)
								{
									if (!check[t].includes(resp[n].calls[l].login))
									{
										ret[resp[n].calls[l].login].contact++;
										check.push(resp[n].calls[l].login)
									}
								}
							}
					}
				}
				if (n === resp.length - 1)
				{
						ch = ret;
				}
			}
			db.model('artisan').find({"date.ajout": {$gt: start, $lt: end},
				"login.ajout": {$in: req.query.array}},
				{"login.ajout": 1})
			.lean().exec(function (err, resp) {
				if (err)
				{
					console.log("Erreur ptStats dashboard: ", err);
					reject(err);
				}
				for (var n = 0; n < resp.length; n++)
				{
					ret[resp[n].login.ajout].ajout += 1;
				}
				db.model('artisan').find({"login.ajout": {$in: req.query.array},
					$or: [{status: {$in: ['ACT', 'ARC']}},
					{$and: [{'document.cni.ok': true},
					{'document.kbis.ok': true},
					{'document.contrat.ok': true}]}]},
					{"status": 1, "login.ajout": 1, "document": 1})
				.lean().exec(function (err, resp) {
					if (err)
					{
						console.log("Erreur ptStats dashboard: ", err);
						reject(err);
					}
					for (var n = 0; n < resp.length; n++)
					{
						if (resp[n].status === 'ACT')
						    ret[resp[n].login.ajout].actif += 1;
						else if (resp[n].status === 'ARC')
						    ret[resp[n].login.ajout].archive += 1;
						if (resp[n].document &&
						    resp[n].document.cni && resp[n].document.cni.ok === true &&
						    resp[n].document.kbis && resp[n].document.kbis.ok === true &&
						    resp[n].document.contrat && resp[n].document.contrat.ok === true)
						    ret[resp[n].login.ajout].complets += 1;
				    }
				    resolve(ret);
			    });
		    });
		});
	})
    }
}
