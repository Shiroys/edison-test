module.exports = function(schema) {
    schema.statics.CheckFacturier = {
	unique: true,
	findBefore: true,
	method: 'POST',
	fn: function(artisan, req, res) {
	    var _ = require('lodash')
	    return new Promise(function(resolve, reject) {
		var pack = artisan.historique.pack
		if (pack.length > 0)
		    var interval = artisan.intervalfacture
		else
		    var interval = 5
		if (req.body.count >= interval)
		{
		    artisan.demandeFacturier = {
			status: 'PENDING',
			login: "Automatique",
			date: new Date()
		    }
		    artisan.save().then(resolve, reject)
		    edison.event('NEED_FACTURIER')
			.login(req.session.login)
			.id(artisan.id)
			.service('PARTENARIAT')
			.color('green')
			.message(_.template("La relance automatique vous signale une demande de facturier pour {{artisan.nomSociete}}(M. {{artisan.representant.nom}})")({
			    artisan: artisan,
			    login: req.session.login
			}))
			.send()
			.save()
		    resolve("ok")
		}
	    })
	}
    }
}
