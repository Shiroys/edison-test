module.exports = function(schema) {
    var _ = require('lodash')
    var PDF = requireLocal('pdf-mail')

    schema.statics.facturierMany = {
	
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(artisan, req, res) {
	    var textTemplate = requireLocal('config/textTemplate.js');
	    var params = {
                sst: artisan,
                text: _.template(textTemplate.lettre.artisan.envoiFacturier())(artisan),
                title: ""
	    }
	    if (req.query.html) {
                res.send(PDF('sst-letter', params).getHTML())
	    } else {
                PDF('sst-letter', params).buffer(function(err, resp) {
		    res.pdf(resp);
                })
	    }
	    
        }
    }

    schema.statics.facturier = {
        unique: true,
        findBefore: true,
        method: 'GET',
        fn: function(artisan, req, res) {
            var textTemplate = requireLocal('config/textTemplate.js');
            var params = {
                sst: artisan,
                text: _.template(textTemplate.lettre.artisan.envoiFacturier())(artisan),
                title: ""
            }
            if (req.query.html) {
                res.send(PDF('sst-letter', params).getHTML())
            } else {
                PDF('sst-letter', params).buffer(function(err, resp) {
                    res.pdf(resp);
                })
            }

        }
    }

	schema.statics.tshirt = {
        unique: true,
        findBefore: true,
        method: 'GET',
        fn: function(artisan, req, res) {
            var textTemplate = requireLocal('config/textTemplate.js');
            var params = {
                sst: artisan,
                text: _.template(textTemplate.lettre.artisan.envoiTshirt())(artisan),
                title: ""
            }
            if (req.query.html) {
                res.send(PDF('sst-letter', params).getHTML())
            } else {
                PDF('sst-letter', params).buffer(function(err, resp) {
                    res.pdf(resp);
                })
            }

        }
    }

    schema.statics.sendFacturier = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(artisan, req, res) {
            return new Promise(function(resolve, reject) {
			if (req.body.tshirt && req.body.facturier == false)
			{
				var opt = _.pick(req.body, false, false, 'tshirt');
				opt.dateTshirt = Date.now();
			}
			else if (req.body.facturier && req.body.tshirt == false)
			{
                		var opt = _.pick(req.body, 'facturier', 'deviseur', false);
				opt.date = Date.now();
				if (artisan.demandeFacturier) {
                	   		artisan.demandeFacturier.status = "OK";
                		}
			}
			else
			{
				var opt = _.pick(req.body, 'facturier', 'deviseur', 'tshirt');
				opt.dateTshirt = Date.now();
				opt.date = Date.now();
				if (artisan.demandeFacturier) {
                    			artisan.demandeFacturier.status = "OK";
                		}
			}
                	opt.login = req.session.login;
			artisan.historique.pack.push(opt);
                	edison.event('SEND_FACTURIER').login(req.session.login).id(artisan.id).save()
                	artisan.save().then(resolve, reject)
            })
        }
    }

}
