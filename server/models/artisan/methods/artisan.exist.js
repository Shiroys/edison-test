module.exports = function(schema) {
    schema.statics.exist = function (req, res) {
	return new Promise(function(resolve, reject) {
	    db.model('artisan').findOne({
		$or: [{
                    'telephone.tel1': req.query.num
		}, {
                    'email': req.query.email
		}]
            }).then(function (resp) {
		if (resp)
		    resolve("exist");
		else
		    resolve("free");
	    }, reject);
	})
    }
}
