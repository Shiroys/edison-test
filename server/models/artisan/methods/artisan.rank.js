module.exports = function(schema) {

  var _ = require('lodash')

  var __filter = function(doc, i) {
    var d = doc.obj;
    return d.status !== 'ARC' && (!this.categorie || d.categories.indexOf(this.categorie) !== -1);
  }

  var isAbsent = function(abs) {
    var moment = require('moment')
    if (!abs || !abs.length) {
      return false;
    }
    return moment().isAfter(abs[abs.length - 1].start) && moment().isBefore(abs[abs.length - 1].end)
  }

    var __map = function(doc, i) {
	var d = doc.obj;
	return {
	    id: d.id,
	    nomSociete: d.nomSociete,
	    distance: doc.dis.round(1),
	    blocked: d.blocked,
	    categories: d.categories,
	    ajout: d.date.ajout,
	    star: d.star,
	    status: d.status,
	    soir: d.soir,
	    samedi: d.samedi,
	    dimanche: d.dimanche,
	    ferie: d.ferie,
	    subStatus: d.subStatus,
	    quarantained: d.quarantained,
	    zoneChalandise: d.zoneChalandise,
	    absent: isAbsent(d.absence),
	    telephone: {
		tel1: d.telephone.tel1,
		tel2: d.telephone.tel2,
	    },
	    address: {
		lt: d.address.lt,
		lg: d.address.lg,
	    },
	}
    }


  schema.statics.rankArtisans = function(options) {
    var self = this;
    if (!isWorker) {
      return edison.worker.createJob({
        name: 'db',
        model: 'artisan',
        priority: 'high',
        method: 'rankArtisans',
        req: options
      })
    }
    return new Promise(function(resolve, reject) {
      var _ = require('lodash')

      var query = {
        status: {
          $ne: 'ARC'
        },
      }
      if (options.categorie) {
        query.categories = {
          $in: [options.categorie]
        }
      }

      db.model('artisan').geoNear({
        type: "Point",
        coordinates: [parseFloat(options.lat), parseFloat(options.lng)]
      }, {
          query: query,
	  spherical: "true",
          limit: options.categorie ? 100 : 25,
          distanceMultiplier: 0.001,
          maxDistance: (parseFloat(options.maxDistance) || 100) / 0.001
      }).then(function(docs) {
        try {
          docs = _.map(docs, __map);
          var l1 = _.filter(docs, function(e) {
            return e.star && e.distance < 50;
          })
          var l2 = _.reject(docs, function(e) {
            return e.star && e.distance < 50;
          })
          resolve(l1.concat(l2));
        } catch (e) {
          __catch(e);
        }
      }, reject)
    })
  }
  schema.statics.rank = function(req, res) {

    return this.rankArtisans(req.query)
  }

}
