module.exports = function(schema) {

    schema.statics.callLog = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(artisan, req, res) {
            return new Promise(function(resolve, reject) {
                artisan.calls.push({
                    login: req.session.login,
                    date: Date.now(),
                    text: "",
                    id_artisan: artisan.id
                })
                artisan.save().then(resolve, reject)
            })
        }
    }
}
