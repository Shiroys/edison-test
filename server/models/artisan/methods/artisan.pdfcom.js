module.exports = function(schema) {
    var _ = require('lodash')
    var xlsx = require('node-xlsx');
    var moment = require('moment');
    schema.statics.pdfcom = function(req, res) {
	return new Promise(function(resolve) {
	var _this = this;
	document.list('/CommissionsPartenariat').then(function(resp) {
	    var j = resp.length + 1
	    var diff = 'Commission du ' + moment().format('LL') + '.xlsx'
	    var diff2 = resp[resp.length - 1].slice(2,resp[resp.length - 1].length)
	    var rec = j +'.' +'Commission du ' + moment().format('LL') + '.xlsx'
	    if (diff2 === diff)
	    	j = resp.length;
	    var rtn = []
	    Object.keys(req.query).forEach(function(key, index) {
		rtn.push(req.query[key])
	    })
	    var name = j +'.' +'Commission du ' + moment().format('LL') + '.xlsx'
	    var file = xlsx.build([{
		name: name,
		data: rtn
	    }]);
	    document.upload({
		filename: '/CommissionsPartenariat/' + j +'.' +'Commission du ' + moment().format('LL') + '.xlsx',
		data: file
	    }).then(function(resp) {
	    }, function(err)  {
		console.log('==>', err)
	    })
	})
	    resolve("ok")
	})
    }
}
