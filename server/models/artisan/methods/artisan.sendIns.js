module.exports = function(schema) {
    var PDF = requireLocal('pdf-mail')
    var _ = require('lodash')

    schema.statics.sendIns = {
	unique: true,
	findBefore: true,
	method: 'POST',
	fn: function(artisan, req, res) {

	    return new Promise(function(resolve, reject) {
		var params = req.body.params
		    var communication = {
			mailDest: envProd ? artisan.email : (req.session.email ||  'intervention@edison-services.fr'),
			mailReply: 'yohann.rhoum@edison-services.fr'
		    }
		    var template = req.body.rappel > 0 ? 'relanceDocuments' : 'envoiContrat';
		    var attachments = [];
		    var html = require('fs').readFileSync(process.cwd() + '/templates/instructionLydia.html', 'utf8')
			html = _.template(html)(artisan);
			mail.send({
			    From: "yohann.rhoum@edison-services.fr",
			    ReplyTo: "yohann.rhoum@edison-services.fr",
			    To: params.to,
			    Subject: params.obj,
			    HtmlBody: html,
			    Attachments: attachments
			}).then(resolve, reject)
	    })
	}
    }
}
