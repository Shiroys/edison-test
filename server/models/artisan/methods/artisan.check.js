module.exports = function(schema) {

    schema.statics.checkCall = {
        unique: true,
        findBefore: true,
        method: 'POST',
        fn: function(artisan, req, res) {
            return new Promise(function(resolve, reject) {
                db.model('artisan').update({'calls._id': req.body.sst},
                                           {$set: {'calls.$.text': req.body.text,
                                                   'calls.$.ok': req.body.state}}
                                          ).then(function (e) {
					      artisan.save().then(resolve, reject);
					  }, reject);
            })
        }
    }
}
