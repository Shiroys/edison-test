module.exports = function(schema) {

	var _this = this;
	var fs = require('fs');
	var archiver = require('archiver');
	var moment = require('moment');
	var _ = require('lodash');

	/* Schema de téléchargement de vcf artisan */
	schema.statics.dllVcfArtisan = function(req, res) {	
		/* Formate data en data pour vcf artisan */
		var format = function(data) {
			var rtn = ""
			rtn +=	"BEGIN:VCARD\n";
			rtn +=	"VERSION:3.0\n" +
				_.template("N: {{id}} - {{nomSociete}} - {{representant.nom}} {{representant.prenom}} - {{address.cp}} {{address.v}}\n")(data) +
				_.template("FN: {{id}} - {{nomSociete}} - {{representant.nom}} {{representant.prenom}} - {{address.cp}} {{address.v}}\n")(data) +
				_.template("EMAIL: {{email}}\n")(data) +
				"TEL;WORK;VOICE: " + data.telephone.tel1 + "\n";
			if(data.telephone.tel2)
			{
				rtn +=	"TEL;WORK;VOICE: " + data.telephone.tel2 + "\n";
			}
			if(data.telephone.tel3)
			{
				rtn +=	"TEL;WORK;VOICE: " + data.telephone.tel3 + "\n";
			}
			rtn +=	"END:VCARD\n";
			return (rtn);
		}
		if (fs.existsSync('/home/abel/edsx/cache/vcf/artisans/' + 'vcf-artisan-' + moment().format("DD-MM-YYYY") + '.zip'))
		{
			/* Lancement download archive */
			if (req.query.download)
				res.download('/home/abel/edsx/cache/vcf/artisans/' + 'vcf-artisan-' + moment().format("DD-MM-YYYY") + '.zip', 'vcf-artisan-' + moment().format("DD-MM-YYYY") + '.zip', function(err) {
					if (err)
						console.log("Erreur download vcf artisan: ", err);
				})
		}
		else
		{
			var all = [];
			var fileTab = [];
			var CHUNK_SIZE = 2;
			var LIMIT = _.get(req, 'query.limit') && parseInt(req.query.limit);
			/* Recherche des données artisan trié par filtre spécifiés */
			db.model('artisan').find()
				.select('id nomSociete address telephone')
				.limit(LIMIT)
				.sort('-id')
				.then(function(resp) {
					for (var ind = 0; ind < resp.length; ind++)
						all.push(format(resp[ind]));
					/* Chunk en 2 fichiers les data artisan */
					var allChunked = _.chunk(all, parseInt(all.length / CHUNK_SIZE) + 2);
					var dirZip = "/home/abel/edsx/cache/vcf/artisans/";
					var nameZip = 'vcf-artisan-' + moment().format("DD-MM-YYYY") + '.zip';
					/* Créatoin du stream archiver */
					var output = fs.createWriteStream(dirZip + nameZip);
					var archive = archiver('zip');
					output.on('close', function() {
						/* Lancement download archive */
						if (req.query.download)
							res.download(dirZip + nameZip, nameZip, function(err) {
								if (err)
									console.log("Erreur download vcf artisan: ", err);
							})
					})
					archive.on('error', function(err) {
						if (err)
							console.log("Erreur archive: ", err);
					})
					archive.pipe(output);
					/* Création des fichiers vcf, stockage dans /cache/vcf et mise dans l'archive */
					_.each(allChunked, function(e, k) {
						var fileName = 'vcf-artisan-' + moment().format("DD-MM-YYYY") + '-part' + (k + 1) + '.vcf';
						var fileDest = dirZip + fileName;
						fileTab.push(fileDest);
						fs.writeFileSync(fileDest, e.join(''));
						archive.append(fs.createReadStream(fileDest), { name: fileName });
					})
					/* Finalisation de l'archive qui va lancer l'évent close et le download */
					archive.finalize();
				})
		}
	}

	/* Schema de téléchargement de vcf client */
	schema.statics.dllVcfClient = function(req, res) {	
		/* Formate data en data pour vcf client */
		var format = function(data) {
			var rtn = ""
			rtn +=	"BEGIN:VCARD\n";
			rtn +=	"VERSION:3.0\n" +
				_.template("N: {{id}} - {{client.civilite}} - {{client.nom}} {{client.prenom}} - {{client.address.cp}} {{client.address.v}}\n")(data) +
				_.template("FN: {{id}} - {{client.civilite}} - {{client.nom}} {{client.prenom}} - {{client.address.cp}} {{client.address.v}}\n")(data) +
				_.template("EMAIL: {{client.email}}\n")(data) +
				"TEL;WORK;VOICE: " + data.client.telephone.tel1 + "\n";
			if(data.client.telephone.tel2)
			{
				rtn +=	"TEL;WORK;VOICE: " + data.client.telephone.tel2 + "\n";
			}
			if(data.client.telephone.tel3)
			{
				rtn +=	"TEL;WORK;VOICE: " + data.client.telephone.tel3 + "\n";
			}
			rtn +=	"END:VCARD\n";
			return (rtn);
		}
		if (fs.existsSync('/home/abel/edsx/cache/vcf/clients/' + 'vcf-client-' + moment().format("DD-MM-YYYY") + '.zip'))
		{
			/* Lancement download archive */
			if (req.query.download)
				res.download('/home/abel/edsx/cache/vcf/clients/' + 'vcf-client-' + moment().format("DD-MM-YYYY") + '.zip', 'vcf-client-' + moment().format("DD-MM-YYYY") + '.zip', function(err) {
					if (err)
						console.log("Erreur download vcf client: ", err);
				})
		}
		else
		{
			var all = [];
			var fileTab = [];
			var CHUNK_SIZE = 3;
			var start = new Date();
			var end = new Date();
			start.setDate(start.getDate() - 30);
			/* Recherche des données artisan trié par filtre spécifiés */
			db.model('intervention').find({"date.ajout": {$gte: start, $lt: end}})
				.select('id address telephone client')
				.sort('-id')
				.then(function(resp) {
					for (var ind = 0; ind < resp.length; ind++)
						all.push(format(resp[ind]));
					/* Chunk en 2 fichiers les data artisan */
					var allChunked = _.chunk(all, parseInt(all.length / CHUNK_SIZE) + 2);
					var dirZip = "/home/abel/edsx/cache/vcf/clients/";
					var nameZip = 'vcf-client-' + moment().format("DD-MM-YYYY") + '.zip';
					/* Créatoin du stream archiver */
					var output = fs.createWriteStream(dirZip + nameZip);
					var archive = archiver('zip');
					output.on('close', function() {
						/* Lancement download archive */
						if (req.query.download)
							res.download(dirZip + nameZip, nameZip, function(err) {
								if (err)
									console.log("Erreur download vcf client: ", err);
							})
					})
					archive.on('error', function(err) {
						if (err)
							console.log("Erreur archive: ", err);
					})
					archive.pipe(output);
					/* Création des fichiers vcf, stockage dans /cache/vcf et mise dans l'archive */
					_.each(allChunked, function(e, k) {
						var fileName = 'vcf-client-' + moment().format("DD-MM-YYYY") + '-part' + (k + 1) + '.vcf';
						var fileDest = dirZip + fileName;
						fileTab.push(fileDest);
						fs.writeFileSync(fileDest, e.join(''));
						archive.append(fs.createReadStream(fileDest), { name: fileName });
					})
					/* Finalisation de l'archive qui va lancer l'évent close et le download */
					archive.finalize();
				})
			}
	}

}
