module.exports = function(schema) {
    var moment = require('moment');
    schema.statics.upload = function(req, res) {
        return new Promise(function(resolve, reject) {
            if (!req.files || !req.files.file || !req.files.file.buffer || !req.files.file.extension) {
                return reject("Invalid File");
            }
            if (req.files.file.size > 5000000)
                return reject("File is too big");
            document.upload({
                filename: '/V2_PRODUCTION/intervention/' + req.body.link + '/' + req.files.file.originalname,
                data: req.files.file.buffer,
            }).then(function(resp) {
                resolve('ok');
            }, reject);
        })
    }
}
