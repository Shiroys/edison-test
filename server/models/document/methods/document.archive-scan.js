module.exports = function(schema) {

  schema.statics.checkArchived = function(req, res) {
      res.json("v1 is dead");
  }

  schema.statics.archiveScan = function(req, res) {
      var _ = require('lodash');
      if (!isWorker) {
	  return edison.worker.createJob({
              name: 'db',
              model: 'document',
              method: 'archiveScan',
              req: _.pick(req, 'query', 'session')
	  })
      }

      return new Promise(function(resolve, reject) {
	  var request = require('request');
	  var requestP = require('request-promise');
	  var async = require('async');
	  var moment = require('moment');
	  var i = 0;
	  var limit = req && req.query && req.query.limit || 1000;
	  var archiveFile = function(file, cb) {
              document.move('/SCAN/' + file.name, '/SCAN_ARCHIVES/' + file.name)
		  .then(function(resp) {
		      db(null);
		  }, function(err) {
		      if (_.includes(String(err), '404')) {
			  document.move('/SCAN_ARCHIVES/' + file.name, '/SCAN/' + file.name).then(function(resp) {
			      cb(null)
			  }, function() {
			      cb(null);
			  })
		      }
		  });
	  }
	  resolve('ok');
    }).catch(__catch)
  }
}
