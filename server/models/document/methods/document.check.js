module.exports = function(schema) {
     var _ = require('lodash')
     var moment = require('moment-timezone')
     var request = require('request')
     var async = require('async')

     var convert = function(ts, offset) {
       var dt = ts.replace('.', '-').split('-').slice(0, -4).join('-');
       var hr = ts.replace('.', '-').split('-').slice(3, 6).join(':');
       return moment.tz(dt + " " + hr, 'Europe/Paris').unix() * 1000 + (offset || 0)
     }

     schema.statics.test = function(req, res) {
       try {
         var req = {}
         if (moment().hour() > 7 && moment().hour() < 21) {
           db.model('document').check(req).then(function() {
             db.model('document').archiveScan(req).then(function() {
               db.model('document').order(req).then(function() {})
             })
           })
         }
       } catch (e) {
         console.log("Error document.check: ",e)
       }
     }

     schema.statics.checkChecked = function(req, res) {
	 res.json("v1 is dead");
     }


     var findClosest = function(x, dbl) {
       var min = {
         diff: Math.pow(9, 9)
       }
       _.each(dbl, function(e) {
         e.diff = e.ts - x.start;
         if (Math.abs(e.diff) < Math.abs(min.diff)) {
           min = e;
         }
       })
       if ((min.diff > 0 && min.diff < 15000) || (min.diff < 0 && min.diff > -15000)) {
         return min
       }
       return null
     }

     var moveV1 = function(closest, cb) {
       request.get({
         url: 'http://electricien13003.com/alvin/5_Gestion_des_interventions/mvFile.php',
         qs: {
           file: '/SCAN/' + closest.name
         }
       }, function(err, resp, body) {
         cb(err, body)
       })
     }

     schema.statics.check = function(req, res) {
       if (!isWorker) {
         return edison.worker.createJob({
           name: 'db',
           model: 'document',
           method: 'check',
           req: _.pick(req, 'query', 'session')
         })
       }
       return new Promise(function(resolve, reject) {
           document.list('/SCAN').then(function(dbl) {
               dbl = _(dbl).filter(function(e) {
		   return e.length === 23 && _.endsWith(e, '.pdf') && e[4] === '-'
               }).map(function(e) {
		   return {
                       name: e,
                       ts: convert(e, 0)
		   }
               }).value();
               var i = 0;
               var limit = req && req.query &&  req.query.limit || 1000;
               try {
		   resolve('ok');
               } catch(e) {
		   console.log('=>', e)
               }
           }, function(err) {
               console.log('-->', err)
           })
       })
	     .catch(__catch);
     }
   }
