var ejs = require('ejs');
var fs = require("fs");
var bPromise = require('bluebird');
var _ = require('lodash');
require('nodeify').extend(bPromise);

var Mail = function(params) {

    var postmark = require("postmark");
    var key = requireLocal('config/_keys');

    this.client = new postmark.Client(key.postmark);
}

Mail.prototype.send = function(options, callback) {
    var _this = this;
    return new bPromise(function(resolve, reject) {
        if (!options.Bcc) {
            options.Bcc = "test.edison.simon@gmail.com";
        }
        _this.client.sendEmail(options, function(err, success) {
            if (err)  {
                console.log("Erreur mail: ", err);
				if (err.status === 422 && err.code === 406 && options.Subject != "Mail non envoyé")
				{
		    		var textMail = "Le mail suivant n'a pas pu être envoyé: " + options.Subject;
		    		mail.send({
						From: "contact@edison-services.fr",
						To: "contact@edison-services.fr",
						Subject: "Mail non envoyé",
						HtmlBody: textMail,
		    		})
				}
                return reject(err);
            }
            console.log("Success: ", success);
            return resolve(success)
        })
    }).nodeify(callback);
};
module.exports = Mail;
