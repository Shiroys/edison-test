var MD5 = require('md5')
var _ = require("lodash");
var requestp = require("request-promise")
var Mobyt = function(user, pass) {
    this.user = user;
    this.pass = pass;

}

Mobyt.prototype.getTicket = function(params) {
    return MD5(_.reduce(params, function(total, p) {
        return total += p;
    }))
}

Mobyt.prototype.getStatus = function(params) {
    var _this = this;
    return new Promise(function(resolve, reject) {
        var id;
        var requestStatus = function(doc) {
            var f = {
                user: _this.user,
                pass: _this.pass,
                id: doc.id,
                type: "notify",
                schema: 1
            }
            f.ticket = _this.getTicket([f.user, f.id, f.type, f.schema, MD5(f.pass)]);
            requestp.post({
                url: 'http://multilevel.mobyt.fr/sms/batch-status.php',
                form: f
            }).then(function(result) {
                var log = result.split(',');
                if (log.length === 9) {
                    resolve(parseInt(log[7]));
                } else {
                    reject(log);
                }
            })
        }

        if (typeof params === 'object')
            id = params.id
        else
            id = params;
        db.model('sms').findOne({
            id: id
        }).then(requestStatus, reject)
    })
}

Mobyt.prototype.send = function(params) {
    var _this = this;
    return new Promise(function(resolve, reject) {
        if (!params.text || !params.to) {
            return reject("Invalid Parameters");
        }
        if (!envProd) {
            params.to = "0633138868";
        }
        if(params.type != "RAPPEL")
        {
            var f = {
                user: _this.user,
                pass: _this.pass,
                data: params.text + "\nSTOP 36608",
                rcpt: params.to.length == 10 ? params.to.replace('0', '+33') : params.to,
                sender: 'Edison Srv.',
                qty: 'n',
                operation: 'MULTITEXT',
                domaine: "http://multilevel.mobyt.fr",
                return_id: "1",
            };
            f.ticket = _this.getTicket([f.user, f.rcpt, f.sender, f.data, f.qty, MD5(f.pass)])
            requestp.post({
                url: 'http://multilevel.mobyt.fr/sms/send.php',
                form: f
            }).then(function(response) {
                if (response.startsWith('OK')) {
                    params.id = response.substr(3);
                    mail.send({
                        From: "contact@edison-services.fr",
                        To: "noreply.edison+sms@gmail.com",
                        Subject: "[SMS] - " + "[" + (params.type ||  "INCONNU") + "] - " + "[" + (params.dest || "INCONNU") + "]",
                        HtmlBody: "Sent to:" + params.to + "<br>" + params.text.replaceAll('\n', '<br>'),
                    })
                    resolve(params);
                } else {
                    reject(response);
                }
            }, function(err) {
                reject(err);
            });
        }
    });
};

module.exports = Mobyt;
