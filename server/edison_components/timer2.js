var ms = require('milliseconds')
var _ = require('lodash')
var moment = require('moment')
var pm2 = require('pm2')
var Timer = module.exports = function() {
var schedule = require('node-schedule');
var DEFAULT_DELAY_VALUE = 10
var eventList = [];

var everydayAt = function(hour, delay, fn) {
	var moment = require('moment-timezone')
	if (!fn && _.isFunction(delay)) {
		fn = delay
		delay = DEFAULT_DELAY_VALUE
	}
	if (delay === false) {
		delay = 0;
	}
	var cronString = moment.tz('Europe/Paris').hour(hour).format('[0] H [* * *]');
	var parser = require('cron-parser');
	var interval = parser.parseExpression(cronString);
	eventList.push({
		nextDate: [
			interval.next().toString(),
			interval.next().toString(),
		],
		cronString: cronString,
		name: fn.name,
		fn: fn,
		delay: ms.minutes(delay)
	})
}

	var loadOvh = function () {
		var date = new Date()
		date.setHours(1, 0, 0, 0);
		date.setDate(1)
		date.setMonth(7)
		var dateE = new Date()
		dateE.setHours(1, 0, 0, 0);
		dateE.setDate(new Date().getDate() - 1)
		var repeat = function (dateStart, dateEnd) {
			var obj = {
				body: {
					params: {
						type: 'single',
						 dayStart: dateStart,
					}
				}
			}
			db.model('ovh').loadAccount(obj).then(function (resp) {
				db.model('ovh').loadDemStats(obj).then(function (resp2) {
					dateStart.setDate(dateStart.getDate() + 1)
					if (dateStart.getTime() < dateEnd.getTime() && moment().hour() < 23)
						repeat(dateStart, dateEnd);
				})
			})
		}
		repeat(date, dateE);
	}

	var everyMinutes = function(minute, fn) {
		var cronString = '*/' + minute + ' * * * *'
		var parser = require('cron-parser');
		var interval = parser.parseExpression(cronString);
		eventList.push({
			nextDate: [
				interval.next().toString(),
				interval.next().toString(),
			],
			name: fn.name,
			cronString: cronString,
			fn: fn,
			delay: 0
		})
	}

	everyStartOfMonths = function(fn) {
		var parser = require('cron-parser');
		var cronString = "00 02 1 * *";
		var interval = parser.parseExpression(cronString);
	}

	everyStartOfMonths(function() {
		db.model('artisan').sendComissionsRecap().then(function() {
			db.model('artisan').setStep()
		})
	})

	this.setTimer = function() {
		_.each(eventList, function(ev) {
			schedule.scheduleJob(ev.cronString, function() {
				setTimeout(ev.fn, ev.delay)
			});
		})
	}

	if (envProd) {
		everydayAt(8, function relancesDevisMatin() {
			if (moment().isoWeekday() !== 6 && moment().isoWeekday() !== 7) {
				db.model('devis').relanceAuto7h()
			}
		})

		everydayAt(14, function relancesDevisApresMidi() {
			if (moment().isoWeekday() !== 6 && moment().isoWeekday() !== 7) {
				db.model('devis').relanceAuto14h()
			}
		})

		everydayAt(18, function relancesClients() {
			if (moment().isoWeekday() !== 6 && moment().isoWeekday() !== 7) {
				db.model('intervention').relanceAuto();
			}
		})

		everydayAt(22, function timerRecouv() {
			console.log("----Begin Timer Recouvrement----");
			db.model('intervention').timerRecouv().then(function() {
				console.log("----End Timer Recouvrement----");
			})
		})

		everydayAt(14, function launchMailAverif() {
			db.model('intervention').mailAverif().then(function(resp) {})
		})

		everydayAt(1, function loadUp() {
			edison.worker.createJob({
				name: 'db',
				model: 'artisan',
				priority: 'high',
				method: 'validateBdd',
			})
			var date = new Date()
			date.setHours(1, 0, 0, 0);
			date.setDate(new Date().getDate() - 1)
			var obj = {
				body: {
					params: {
						type: 'single',
						dayStart: date,
					}
				}
			}
			db.model('ovh').clearOldCall()
			db.model('ovh').loadAccount(obj).then(function (resp) {
				db.model('ovh').loadDemStats(obj).then(function (resp2) {});
			})
		})

		everyMinutes(60, function rappelDateIntervention() {
			if (moment().hour() > 7 && moment().hour() < 21) {
				db.model('intervention').rappelDateIntervention()
			}
			if (moment().hour() >= 12 && moment().hour() % 2 == 0 && moment().hour() < 23)
			{
				var loadsixteen = moment().hour() >= 17
				var loadlate = moment().hour() >= 20
				var date = new Date()
				date.setHours(1, 0, 0, 0);
				var obj = {
					body: {
						params: {
							dayStart: date,
						}
					}
				}
				db.model('ovh').find({year: date.getFullYear(), month: date.getMonth(), day: date.getDate()}).then(function (ovh) {
					if (loadlate === true)
					{
						obj.body.params.intervalI = 6
						obj.body.params.intervalS = 20
					}
					else if (loadsixteen === true)
					{
						obj.body.params.intervalI = 6
						obj.body.params.intervalS = 17
					}
					else
					{
						obj.body.params.intervalI = 6
						obj.body.params.intervalS = 12
					}
					if (!ovh || (ovh && ((ovh.loaded_late == false && loadlate == true) || (ovh.loaded_sixteen == false && loadsixteen == true)
						|| (ovh.loaded_twelve == false))))
						db.model('ovh').loadMidDay(obj);
				})
			}
			if (moment().hour() < 22 && moment().hour() > 0)
			{
				pm2.connect(function (e) {    
					pm2.describe("web", function (err, desc) {
						if (desc.length > 0)
						{
							var now = new Date();
							var c = new Date(desc[0].pm2_env.created_at);
							if (now.getHours() - c.getHours() == 1 || now.getDate() != c.getDate())
								loadOvh();
						}
						pm2.disconnect();
					})
				})
			}
		})

		everyMinutes(5, function scanCheck() {
			if (moment().hour() > 7 && moment().hour() < 21) {
				db.model('document').check({}).then(function() {
				})
			}
		})

		everyMinutes(5, function scanArchive() {
			if (moment().hour() > 7 && moment().hour() < 21) {
				db.model('document').archiveScan({}).then(function() {
				})
			}
		})

		everyMinutes(5, function scanOrder() {
			if (moment().hour() > 7 && moment().hour() < 21) {
				db.model('document').order({}).then(function() {})
			}
		})

		everyMinutes(10, function conversationCheck() {
			if (moment().hour() > 7 && moment().hour() < 21) {
				db.model('conversation').refresh().then(function(resp) {})
			}
		})
	}

	everyMinutes(20, function artisanFullReload() {
		db.model('artisan').fullReload().then(function() {})
	})

	everyMinutes(20, function interventionFullReload() {
		console.log("FULL RELOAD BEGIN");
		db.model('intervention').fullReload().then(function() {
			console.log("FULL RELOAD END");
		})
	})

	everyMinutes(20, function devisFullReload() {
		db.model('devis').fullReload().then(function() {})
	})

	everyMinutes(5, function updateAVR() {
		if (moment().hour() > 7 && moment().hour() < 21)
		{
			db.model('intervention').update({ 'cache.f.i_avr': 1, 'cache.s': 0 }
				, { $set: { 'cache.s': 3 }}
				, { multi: true }
				, function(a, b) {})
		}
	})
}
