module.exports = function() {
    var mongoose = require("mongoose");
    var fs = require('fs');
    var path = require('path');
    var _ = require('lodash');

    mongoose.Promise = global.Promise
    mongoose.connect('mongodb://localhost/EDISON');
    function getDirectories(srcpath) {
        return fs.readdirSync(srcpath).filter(function(file) {
            return fs.statSync(path.join(srcpath, file)).isDirectory();
        });
    }

    var basePath = process.cwd() + '/server/models/'
    getDirectories(basePath).forEach(function(model) {
        var folder = basePath + model;
        var schema = require(folder + '/' + model + '.schema')(mongoose);
        require(folder + '/' + model + '.validator')(schema);
        fs.readdirSync(folder + '/methods').forEach(function(method) {
            if (_.endsWith(method, '.js') && !_.startsWith(method, '-')) {
                require(folder + '/methods/' + method)(schema)
            }
        });
        if (model === 'intervention' || model === 'devis' || model === 'artisan' || model === 'prospect' || model === 'save_stats') {
            requireLocal('server/core/core.index.js')(model, schema)
        }
        var model = mongoose.model(model, schema)
    })

    mongoose.utils = {
        round: function(field) {
            return {
                $divide: [{
                        $subtract: [{
                            $multiply: [field, 100]
                        }, {
                            $mod: [{
                                $multiply: [field, 100]
                            }, 1]
                        }]
                    },
                    100
                ]
            }
        },
        prix: function() {

            return {
              $cond: [{
                    $eq: ['$compta.reglement.recu', false]
                  }, '$prixAnnonce', {
			               $cond: [{
				                   $eq: ['$compta.reglement.avoir.effectue', false]
			                      }, '$prixFinal', { $subtract: ['$prixFinal', '$compta.reglement.avoir.montant']}]
		                        }]
                          }
                        },



        and:function(a, b) {
            return {
                $and:[a, b]
            }
        },
        or:function(a, b) {
            return {
                $or:[a, b]
            }
        },
        cond: function(field, value, rA, rB) {
            return {
                $cond: [{
                    $eq: [field, value]
                }, (rA || 1), (rB || 0)]
            }
        },
        sum:function(field) {
            return {
                $sum:field
            }
        },
        sumCond: function(field, value, rA, rB) {
            return {
                $sum: {
                    $cond: [{
                        $eq: [field, value]
                    }, (rA || 1), (rB || 0)]
                }
            }
        },
        test:function() {
            return {
                $sum:'montant'
            }
        },



      sumCondCond: function(field, value, field2, value2, rA, rB) {
          return {
                $sum: {
                  $cond: [{
                    $eq: [field2,value2]
                  },{$cond: [{
                        $eq: [field, value]
                    }, (rA || 1), (rB || 0)]},0]
                }
            }
      },
		sumAnn: function(field, value, num, status) {
	    	return {
				$sum: {
				    $cond: [{
					$and:[{
					    $eq: [field, value]
					}, {
					    $cond: [{
						$gt: ['$login.envoi', null]
					    }, true, false]
					}, {
					    $eq: ['$status', status]
					}, {
					    $cond: [{
						$gt: [num, 0]
					    }, true, false]
					}]
				    }, num, 0]
				}
			    }
			},
		sumSt: function(value, status) {
			return {
				$sum: {
				    $cond: [{
					$and:[{
					    $eq: ['$login.ajout', value],
					}, {
					    $eq: ['$status', status]
					}]
				    }, 1, 0],
				}
		    }
		},

		sumMod: function(value, ret) {
		    return {
				$sum: {
				    $cond: [{
					$and:[{
					    $eq: ['$login.ajout', value]
					}]
				    }, 1, 0],
				}
		    }
		},
		sumAnPr: function(value) {
		    return {
				$sum: {
				    $cond: [{
					$and:[{
					    $eq: ['$login.ajout', value]
					}, {
					    $eq: ['$status', 'APR']
					}]
				    }, 1, 0],
				    $cond: [{
					$and:[{
					    $eq: ['$login.ajout', value]
					}, {
					    $eq: ['$status', 'ANN']
					}]
				    }, 1, 0],
				}
		    }
		},
		sumBoth: function(value) {
		    return {
				$sum: {
				    $cond: [{
					$and:[{
					    $eq: ['$login.ajout', value]
					}, {
					    $cond: [{
						$gt: ['$login.envoi', null]
					    }, true, false]
					}, {
					    $eq: ['$status', 'ENC']
					}, {
					    $cond: [{
						$gt: ['$prixAnnonce', 0]
					    }, true, false]
					}]
				    }, '$prixAnnonce', 0],
				    $cond: [{
					$and:[{
					    $eq: ['$login.ajout', value]
					}, {
					    $cond: [{
						$gt: ['$login.envoi', null]
					    }, true, false]
					}, {
					    $eq: ['$status', 'VRF']
					}, {
					    $cond: [{
						$gt: ['$prixFinal', 0]
					    }, true, false]
					}]
				    }, '$prixFinal', 0],
				    $cond: [{
					$and:[{
					    $eq: ['$login.ajout', value]
					}, {
					    $cond: [{
						$gt: ['$login.envoi', null]
					    }, true, false]
					}, {
					    $cond: [{
						$gt: ['$status', 'AVR']
					    }, true, false]
					}, {
					    $cond: [{
						$gt: ['$prixAnnonce', 0]
					    }, true, false]
					}]
				    }, '$prixAnnonce', 0]
				}
		    }
		},
		sumBothNbr: function(value) {
		    return {
				$sum: {
				    $cond: [{
					$and:[{
					    $eq: ['$login.ajout', value]
					}, {
					    $cond: [{
						$gt: ['$login.envoi', null]
					    }, true, false]
					}, {
					    $eq: ['$status', 'ENC']
					}, {
					    $cond: [{
						$gt: ['$prixAnnonce', 0]
					    }, true, false]
					}]
				    }, 1, 0],
				    $cond: [{
					$and:[{
					    $eq: ['$login.ajout', value]
					}, {
					    $cond: [{
						$gt: ['$login.envoi', null]
					    }, true, false]
					}, {
					    $eq: ['$status', 'VRF']
					}, {
					    $cond: [{
						$gt: ['$prixFinal', 0]
					    }, true, false]
					}]
				    }, 1, 0],
				    $cond: [{
					$and:[{
					    $eq: ['$login.ajout', value]
					}, {
					    $cond: [{
						$gt: ['$login.envoi', null]
					    }, true, false]
					}, {
					    $cond: [{
						$gt: ['$status', 'AVR']
					    }, true, false]
					}, {
					    $cond: [{
						$gt: ['$prixAnnonce', 0]
					    }, true, false]
					}]
				    }, 1, 0]
				}
		    }
		},
        isDefined: function() {
            return {
                $exists: true
            }
        },
        pluck: function(data, value, rangeMax) {
            return _.map(_.range(1, rangeMax), function(day) {
                var fnd = _.find(data, '_id', day)
                return _.round((fnd && fnd[value]) || 0, 2);
            })
        },
        between: function(a, b) {
            return {
                $gt: a,
                $lt: b
            }
        }
    }
    return mongoose;
}
