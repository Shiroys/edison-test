  var moment = require('moment')
  var date = new Date(moment().toDate());
  process.env.FTP_PATH = process.env.FTP_PATH || "/Users/abelchalier/Desktop/ftp"
  var records = moment(date).format('[' + process.env.FTP_PATH + '/*/recordings/][record-]YYMMDD[*.wav]')
  var xml = moment(date).format('[' + process.env.FTP_PATH + '/*/calls/]YYMM[/calls-]YYMMDD[*.xml]')
  var glob = require('glob');
  var _ = require('lodash');
  var shell = require('shelljs')
  var ms = require('milliseconds');
  var md5 = require('md5');
  var fs = require('fs')
  var async = require('async')
  require('./shared.js')()
  
  setInterval(function() {
    var exec = require('child_process').exec;
    exec('pm2 restart ftp-files', function(error, stdout, stderr) {
    });
  }, ms.hours(1))

  var parseFile = function(fileName) {
    var XML = require('pixl-xml');
    var content = fs.readFileSync(fileName, 'utf-8');
    try {
      var parsed = XML.parse(content);
    } catch (e) {
      try {
        var parsed = XML.parse(content + '</calls>');
        return parsed;
      } catch (e) {
        return 0
      }
    }
  }
  var xmlFiles = glob.sync(xml)
  _.each(xmlFiles, function(e) {
      fs.watchFile(e, {
        interval: 3000
      }, (curr, prev) => {

        var getHash = function(call) {
          return call.time + ':0' + (call.to ||  call.from || "").slice(8, 10)
        }

        var filterContent = function(e) {
          return e.withoperator !== 'never' && parseInt(e.duration.split(':').join('')) > 10
        }

        var insertEach = function(call, callback)  {
          db.model('conversation').update({
            _id: call._id,
            archived: false
          }, {
            $set: call
          }, {
            upsert: true
          }).exec(function(err, resp) {
            callback(null)
          })
        }

        var mapContent = function(call) {
          if (call.from._Data) {
            call.from = call.from._Data
          }
          if (call.to._Data) {
            call.to = call.to._Data
          }
          call.to = call.to.replace(/^0033/, '0')
          call.from = call.from.replace(/^0033/, '0')
          call.poste = e.split('/')[e.split('/').findIndex(function(x) {
            return x === 'ftp'
          }) + 1]
          call.dest = call.to;
          call.origin = call.from;
          call.from = call.from.slice(0, 10);
          call.to = call.to.slice(0, 10)
          var d = call.duration.split(':').map(_.partial(parseInt, _, 10))
          call.duration = d[0] * 3600 + d[1] * 60 + d[2];
          call._id = moment(getHash(call), "DD/MM/YY HH:mm:ss:SSS").toDate()
          call.date = call._id
          call.archived = false;
          return call
        }


        var content = parseFile(e)
        if (content) {
          var upd = content.call.filter(filterContent).map(mapContent)
          async.eachLimit(upd, 10, insertEach, function(err, resp) {
          })
        }
      });
    })
