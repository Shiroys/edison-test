const Mongoose = require('mongoose').Mongoose;
const mongoose1 = new Mongoose();

mongoose1.connect('mongodb://localhost:27017/verifInter');

var Schema = mongoose1.Schema;
var madeSchema = require(process.cwd() + '/server/verifInter/models/made.schema')(mongoose1);
var pushedBackSchema = require(process.cwd() + '/server/verifInter/models/pushedBack.schema')(mongoose1);
var canceledSchema = require(process.cwd() +  '/server/verifInter/models/canceled.schema')(mongoose1);
var made = mongoose1.model('made', madeSchema);
var pushedBack = mongoose1.model('pushedBack', pushedBackSchema);
var canceled = mongoose1.model('canceled', canceledSchema);

 /* Schema pour intervention effectuée */

function madeFunc(req){
	const done = new made;
	mongoose1.model('made').findOne({ id: req.id }).then(function(resp) {
		if(resp)
		{
			mongoose1.model('made').remove({id: req.id}).then(function(response){
				for(var key in req){
					done[key] = req[key];
				}
				done.save()
			})
		}
		else
		{
			for(var key in req){
				done[key] = req[key];
			}
			done.save()
		}
		mongoose1.model('pushedBack').remove({id: req.id}).then(function(resp){});
		mongoose1.model('canceled').remove({id: req.id}).then(function(resp){});
		db.model('intervention').update({ 'id': req.id, 'cache.s': 3, 'reglementSurPlace': true }, { $set: { "cache.s": 9, 'status': "EVR", "date.majInternet": new Date()}}).exec(function(err, resp) {
			if (err)
				console.log("Erreur update EVR: ", err);
		})
	})
}

/* Schema pour intervention repoussée */

function pushedBackFunc(req){
	const done = new pushedBack;
	mongoose1.model('pushedBack').findOne({ id: req.id }).then(function(resp) {
		if(resp)
		{
			mongoose1.model('pushedBack').remove({id: req.id}).then(function(response){
				for(var key in req){
					done[key] = req[key];
				}
				done.save()
			})
		}
		else
		{
			for(var key in req){
				done[key] = req[key];
			}
			done.save()
		}
		mongoose1.model('canceled').remove({id: req.id}).then(function(resp){});
		mongoose1.model('made').remove({id: req.id}).then(function(resp){});
		db.model('intervention').update({ 'id': req.id, 'cache.s': 3, 'reglementSurPlace': true }, { $set: { "cache.s": 9, 'status': "EVR", "date.majInternet": new Date()}}).exec(function(err, resp) {
			if (err)
				console.log("Erreur update EVR: ", err);
		})
	})
}

/* Schema pour intervention annulée */

function canceledFunc(req){
	const done = new canceled;
	mongoose1.model('canceled').findOne({ id: req.id }).then(function(resp) {
		if(resp)
		{
			mongoose1.model('canceled').remove({id: req.id}).then(function(response){
				for(var key in req){
					done[key] = req[key];
				}
				done.save()
			})
		}
		else
		{
			for(var key in req){
				done[key] = req[key];
			}
			done.save()
		}
		mongoose1.model('pushedBack').remove({id: req.id}).then(function(resp){});
		mongoose1.model('made').remove({id: req.id}).then(function(resp){});
		db.model('intervention').update({ 'id': req.id, 'cache.s': 3, 'reglementSurPlace': true }, { $set: { "cache.s": 9, 'status': "EVR", "date.majInternet": new Date()}}).exec(function(err, resp) {
			if (err)
				console.log("Erreur update EVR: ", err);
		})
	})
}

module.exports = {
	mongoose1,
	madeFunc,
	pushedBackFunc,
	canceledFunc
};