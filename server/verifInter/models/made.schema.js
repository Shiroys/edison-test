module.exports = function(db){
	var schemaMade = new db.Schema({ 
		id: String,
		reglementChoice: String, // reglementSurPlace
		reglement: String, // modeReglement
		priceHT: String, //prixFinal
		fourniturePrice: String, //fourniture {}
		payedBy: String, // fournisseur
		fournisseur: String, // fournisseur
		remarque: String // en dehors
	});

	return schemaMade;
}