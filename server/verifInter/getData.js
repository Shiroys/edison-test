var getDataCtrl =  function(edisonAPI, $rootScope, $scope){
	"use strict";
	var _this = this;
	edisonAPI.intervention.getRaisonData().then(function(response){
		$scope.raisons = response.data;	
	});	
	edisonAPI.intervention.getFournisseurData().then(function(resp){
		$scope.fournisseurs = resp.data;
	});
};	

// angular.module('edison').controller('getDataController', getDataCtrl);