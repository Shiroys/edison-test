    module.exports = function(core) {
      var _ = require('lodash');
      var __throttle_cache_name = '__throttle_cache_reload' + core.name

      return function(doc) {
        core.model().throttleCacheReload([doc._id]).then(function(resp) {
          io.sockets.emit(core.listChange, {
            data: resp,
            ts: _.now()
          });
          edison.statsTelepro.reload();
        })
      }
    }
