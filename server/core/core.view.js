var _ = require('lodash');

module.exports = function(core) {
    /* 'client categorie tva -_id' */
    return function(id, req, res) {
        var _this = this;
        id = parseInt(id);
        if (id == 0 || isNaN(id))
            return Promise.reject(id + "DOES NOT EXIST")
        return new Promise(function(resolve, reject) {

            var prm = core.model().findOne({
                id: id
            });
            if (req.query.populate) {
                var pops = req.query.populate.removeAll(' ').split(',')
                _.each(pops, function(e) {
                    prm = prm.populate(e);
                })
            }
            if (req.query.select) {
                prm = prm.select(req.query.select);
            }
            prm.then(resolve, reject);

        })
    }
}
