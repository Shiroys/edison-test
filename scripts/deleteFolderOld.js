
/*
* Script delete the oldest files
*/

const fs = require('fs');
const path = require('path');

// set path at check
var pathDir = path.join('/home/abel/edsx/scripts/');
// set max file in folder
var maxFile = 3;

fs.readdir(pathDir, function(err, list) {
	if (err)
		throw err;
	if (list.length >= maxFile) {
		var tab = [];
		var i = 0;

		for (var i in list) {
			dir = path.join(pathDir + list[i]);
			if (fs.lstatSync(dir).isDirectory()) {
				tab.push({
					path: dir,
					time: fs.statSync(dir).birthtime.getTime()
				});
			}
		}
		tab.sort(function(a,b) {
		    return a.time - b.time;
		});
		for (var i=(list.length - maxFile)-1; i >= 0; i--) {
			fs.rmdirSync(tab[i].path);
		}
	}
});
